var gulp = require('gulp'),
    prefix = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    cssmin = require('gulp-cssmin');

gulp.task('styles', function () {
    gulp.src(["css/icon-font.css", "css/animation.css", "css/swiper.css", "include/nivo_slider/nivo-slider.css", "css/styles.css", "include/jcarousel/skin.css", "css/jquery.fancybox-1.3.4.css", "css/sw.css", "css/mobile.css", "css/ui/jquery-ui-1.8.15.custom.css", "css/metro/chosen.css", "js/slic/slick.css", "js/slic/slick-theme.css", "css/bg.css", "css/experiments.css"])
        .pipe(prefix('last 2 versions'))
        .pipe(concat('styles.min.css'))
        .pipe(cssmin())
        .pipe(gulp.dest('css'));
});

gulp.task('scripts', function () {
    gulp.src(["js/jquery.js", "js/jquery.cookie.js", "js/jquery.fancybox-1.3.4.pack.js", "include/jquery_ui_1.11/jquery-ui.min.js", "include/jcarousel/jquery.jcarousel.min.js", "include/jquery/jquery.php.js", "include/jquery/jquery.form.js", "js/jquery.tooltip.js", "js/ui/jquery.ui.datepicker-ru.js", "js/jquery.maskedinput.js", "js/functions.js", "js/layout.js", "js/dynamic.js", "js/metro/chosen.jquery.min.js", "js/metro/selector.js", "js/jquery.touchSwipe.min.js", "js/swiper.min.js", "js/onready.js", "js/onload.js", "js/experiments.js"])
        .pipe(concat('scripts.min.js'))
	    .pipe(gulp.dest('js'));
});

gulp.task('default', function() {
    gulp.start('styles', 'scripts');
});