<?php
global $_GLOBALS,$cur,$COOKIE_REPLACEMENT;

ini_set('display_errors', 0);
error_reporting(E_ALL & ~E_NOTICE);

//session_name("SF_SESS_NAME");
$host = strtolower($_SERVER['HTTP_HOST']);
if ($_SERVER['SERVER_ADDR']!='127.0.0.1'){
    $host = '.'.((stripos($host,'www.',0)===0)?substr($host, 4):$host);
}
define('_CROSS_DOMAIN', $host);
//session_set_cookie_params(0, '/', $host);

if ($_SERVER['REQUEST_URI'] == '/catalog/bouquet/rozy/') {
    $https = (!empty($_SERVER['HTTPS'])?'https':'http');
    header("HTTP/1.1 301 Moved Permanently");
    header('Location: '.$https.'://'._CROSS_DOMAIN.'/catalog/bouquet/flowers/rozy/');
    exit();
}

session_start();
ob_start();
header('Content-Type: text/html; charset=windows-1251');
define('SW', true);

include 'system/includes.php';

if (isset($_POST['cur'])) {
	include 'include/jquery/jQuery.php';
	switch ($_POST['cur']) {
		case 'euro': $cur_set = 'euro'; break;
		case 'usd': $cur_set = 'usd'; break;
		default: $cur_set = 'rub'; break;
	}
	setcookie('cur', $cur_set, null, '/',_CROSS_DOMAIN);
	if (_IS_USER) {
        db_query('UPDATE `sw_users` SET `cur` = '.quote($cur_set).' WHERE `id` = '.quote($_SESSION['user']['id']));
    }
	jQuery::evalScript(inUTF8('href.go("'.$_SERVER['REQUEST_URI'].'")'));
	jQuery::getResponse();
}
$module = !empty($_REQUEST['m']) ? filter(trim($_REQUEST['m']), 'nohtml') : _DEFAULT_MODULE;
$op = !empty($_REQUEST['op']) ? filter(trim($_REQUEST['op']), 'nohtml') : null;
$cur = get_currency();

if (!preg_match('#^[_a-z-]+$#i', $module)) { HeaderPage(); }
if (intval(dbone('COUNT(`id`)', '`link` = '.quote($module).' AND `active` = 1', 'modules')) == 0) { HeaderPage(_PAGE_ERROR404); }

if (file_exists('modules/'.$module.'/config.php')){
    include 'modules/'.$module.'/config.php';
}
include 'modules/'.$module.'/index.php';
include 'system/templates.php';

#$buffer = ob_get_clean();
#$buffer = str_replace('<script type="text/javascript" src="', '<script async type="text/javascript" src="', $buffer);
#echo $buffer;

ob_end_flush();