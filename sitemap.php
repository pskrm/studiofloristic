<?

ini_set('max_execution_time' , 0);

include_once('system/config.php');
include_once('system/db.php');
include_once('system/filter.php');

function checkCode($url){
	$httpCode = 200;
	$handle = curl_init($url);
	curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
	$response = curl_exec($handle);
	$httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
	curl_close($handle);
	return $httpCode;
}

$pages = 1;
$tags = array();
$hostname = 'www.studiofloristic.ru';
$proto = 'https://';
$err_log = 'sitemap_errors.txt';
$accepted_codes = array(200,301,302);

$xml = '<?xml version="1.0" encoding="UTF-8"?>'."\n";
$xml.= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";
$xml.= '	<url>'."\n";
$xml.= '		<loc>'.$proto.$hostname.'/</loc>'."\n";
$xml.= '		<lastmod>'.date('Y-m-d').'</lastmod>'."\n";
$xml.= '		<changefreq>daily</changefreq>'."\n";
$xml.= '		<priority>1.0</priority>'."\n";
$xml.= '	</url>'."\n";

$r = db_query('SELECT `link` FROM `sw_pages` WHERE `active` = 1 AND `link` != '.quote('error404').' AND `link` != '.quote('index'));
while ($p = mysql_fetch_array($r, MYSQL_ASSOC)) {
	$s_url = $proto.$hostname.'/'.$p['link'].'.html';
	$code = checkCode($s_url);
	if ( !in_array((int) $code, $accepted_codes))
	{
		file_put_contents($err_log, 'ERR: '.$code .' URL:' . $s_url. PHP_EOL, FILE_APPEND);
		continue;
	}
	$xml.= '	<url>'."\n";
	$xml.= '		<loc>'.$proto.$hostname.'/'.$p['link'].'.html</loc>'."\n";
	#$xml.= '		<lastmod>'.date('Y-m-d').'</lastmod>'."\n";
	$xml.= '		<changefreq>daily</changefreq>'."\n";
	$xml.= '		<priority>0.5</priority>'."\n";
	$xml.= '	</url>'."\n";
	$pages++;
}

$r = db_query('SELECT `url` FROM `sw_catalog_section` WHERE `first` = 1');
while ($c = mysql_fetch_array($r, MYSQL_ASSOC)) {
	$s_url = $proto.$hostname.$c['url'];
	$code = checkCode($s_url);
	if ( !in_array((int) $code, $accepted_codes))
	{
		file_put_contents($err_log, 'ERR: '.$code .' URL:' . $s_url . PHP_EOL, FILE_APPEND);
		continue;
	}

	$xml.= '	<url>'."\n";
	$xml.= '		<loc>'.$proto.$hostname.$c['url'].'</loc>'."\n";
	#$xml.= '		<lastmod>'.date('Y-m-d').'</lastmod>'."\n";
	$xml.= '		<changefreq>daily</changefreq>'."\n";
	$xml.= '		<priority>0.7</priority>'."\n";
	$xml.= '	</url>'."\n";
	$pages++;
}

$r = db_query('SELECT `pach` FROM `sw_section` WHERE `pach` IS NOT NULL');
while ($c = mysql_fetch_array($r, MYSQL_ASSOC)) {
	$s_url = $proto.$hostname.$c['pach'];
	$code = checkCode($s_url);
	if ( !in_array((int) $code, $accepted_codes))
	{
		file_put_contents($err_log, 'ERR: '.$code .' URL:' . $s_url . PHP_EOL, FILE_APPEND);
		continue;
	}

	$xml.= '	<url>'."\n";
	$xml.= '		<loc>'.$proto.$hostname.$c['pach'].'</loc>'."\n";
	#$xml.= '		<lastmod>'.date('Y-m-d').'</lastmod>'."\n";
	$xml.= '		<changefreq>daily</changefreq>'."\n";
	$xml.= '		<priority>0.5</priority>'."\n";
	$xml.= '	</url>'."\n";
	$pages++;
}

$r = db_query('SELECT `url` FROM `sw_blocks` WHERE `active` = 1');
while ($a = mysql_fetch_array($r, MYSQL_ASSOC)) {
	$s_url = $proto.$hostname.$a['url'];
	$code = checkCode($s_url);
	if ( !in_array((int) $code, $accepted_codes))
	{
		file_put_contents($err_log, 'ERR: '.$code .' URL:' . $s_url . PHP_EOL, FILE_APPEND);
		continue;
	}

	$xml.= '	<url>'."\n";
	$xml.= '		<loc>'.$proto.$hostname.$a['url'].'</loc>'."\n";
	#$xml.= '		<lastmod>'.date('Y-m-d').'</lastmod>'."\n";
	$xml.= '		<changefreq>daily</changefreq>'."\n";
	$xml.= '		<priority>0.3</priority>'."\n";
	$xml.= '	</url>'."\n";
	$pages++;
}

$xml.= '</urlset>';

$filename = 'sitemap.xml'; 

unlink($filename);
$f = fopen($filename, 'a+');
fwrite($f, $xml);
fclose($f);

//print '<p> - ���� ������������</p>';
//print '<p> - ������ sitemap: <b>'.sprintf("%01.2f", filesize($filename) / 1024).' ��</b></p>';
//print '<p> - ���-�� ������� � ����� sitemap: <b>'.$pages.'</b></p>';
header("Content-type: text/xml; charset=windows-1251");
readfile($filename);
?>