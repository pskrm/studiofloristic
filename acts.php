<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
        <link href='http://fonts.googleapis.com/css?family=Lobster&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
        <style type="text/css">
            body, html {margin: 0;padding: 0;font-family: Tahoma, Geneva, sans-serif;font-size: 12px;color: #000;position: relative;height:100%;}
            img {border:none;}
            p {padding:0; margin: 0px 0px 5px 0px;}

            table {border-collapse: collapse; width: 100%;}

            .html {width:700px;height:100%;margin:0px auto;}
            .html td {vertical-align: top;}
            .hint {color:#808080;}

            .header {height: 130px; }
            .header table {height: 107px; width: 99%; position:relative;}
            .header .hint {font-size: 22px;position:absolute;top:40px;right:0px;}
            .slogan {font-family: 'Lobster', cursive; display:block;font-size:16px;position:absolute;top:85px;left:85px;color:#555;}

            .recipient {position:relative;}
            .recipient p {margin: 3px 0px 8px 25px !important; font-size: 11px; width:490px;}
            .recipient .title {margin: 3px 0px 10px 0px !important; font-size: 12px;}

            .items {height: auto;}
            .items p {margin: 0px 0px 5px 0px;}
            .items .item-border {border: 1px solid #808080; padding: 10px; margin: 0px 0px 25px 0px;}
            .items .item {width: 99%; position:relative;}
            .items td {padding: 0px 10px;}
            .items .counts {position:absolute;bottom:0px;right:0px;color:#555;font-size: 14px;padding: 2px 10px;}
            .items .cBorder {border-left: 1px solid #808080;}

            .datetime {position:absolute; top:0px; right:25px; width: 150px; height: 80px; color: #555;}
            .datetime b {display: block; margin-top: 5px; font-size: 18px;}

            .autograph, .contacts {width: 99%;}
            .autograph td {border:1px solid #555; padding: 10px;}

            .footer {height: 215px;}
            .footer .autograph {height: auto; margin: 25px 0px 50px 0px;}
            .footer .contacts {height: 60px; color: #555; border-top: 2px solid #555;}
            .footer p {margin: 0px 0px 2px 0px; font-size: 11px; line-height: 18px;}
            .footer .h24 {background: url('/i/24.png') no-repeat; padding: 7px 34px;}

            .courier td {padding: 10px; border: 1px solid #808080;}
        </style>
        <title>[%ACT_TITLE%]</title>
    </head>
    <body>
        <table class="html [%ACT_TABLE_CLASS%]">
            [%ACT_TABLE_CONTENT%]
        </table>
    </body>
</html>