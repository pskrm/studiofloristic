<?php
require_once 'Action.php';
require_once 'Element.php';
class jQuery
{
    public static $jQuery;
    public $response = array(
                              'a' => array(),
                              'q' => array()
                            );
    function __construct() 
    {
    	
    }
    
    public static function init()
    {
        if (empty(jQuery::$jQuery)) {
            jQuery::$jQuery = new jQuery();
        }
        return true;
    }
    
    public static function addData ($key, $value, $callBack = null)
    {
        jQuery::init();

        $jQuery_Action = new jQuery_Action();
        $jQuery_Action ->add('k', $key);
        $jQuery_Action ->add('v', $value);
        
        if ($callBack) {
            $jQuery_Action ->add("callback", $callBack);
        }

        jQuery::addAction(__FUNCTION__, $jQuery_Action);

        return jQuery::$jQuery;
    }

    public static function addMessage ($msg, $callBack = null, $params = null)
    {
        jQuery::init();
        
        $jQuery_Action = new jQuery_Action();        
        $jQuery_Action ->add("msg", $msg);
        
        if ($callBack) {
            $jQuery_Action ->add("callback", $callBack);
        }
        
        if ($params) {
            $jQuery_Action ->add("params",  $params);
        }
        
        jQuery::addAction(__FUNCTION__, $jQuery_Action);
        
        return jQuery::$jQuery;
    }
    
    public static function addError ($msg, $callBack = null, $params = null)
    {
        jQuery::init();
        
        $jQuery_Action = new jQuery_Action();        
        $jQuery_Action ->add("msg", $msg);

        if ($callBack) {
            $jQuery_Action ->add("callback", $callBack);
        }
        
        if ($params) {
            $jQuery_Action ->add("params",  $params);
        }
        
        jQuery::addAction(__FUNCTION__, $jQuery_Action);
        
        return jQuery::$jQuery;
    }
    
    public static function evalScript ($foo)
    {
        jQuery::init();
        
        $jQuery_Action = new jQuery_Action();        
        $jQuery_Action ->add("foo", $foo);

        jQuery::addAction(__FUNCTION__, $jQuery_Action);
        
        return jQuery::$jQuery;
    }
    
    public static function getResponse()
    {
        jQuery::init();
        
        echo json_encode(jQuery::$jQuery->response);
        exit ();
    }
    
    public static function addQuery($selector)
    {
        jQuery::init();
        
        return new jQuery_Element($selector);
    }
    
    public static function addElement(jQuery_Element &$jQuery_Element)
    {
        jQuery::init();
        
        array_push(jQuery::$jQuery->response['q'], $jQuery_Element);
    }
    
    public static function addAction($name, jQuery_Action &$jQuery_Action)
    {
        jQuery::init();
        
        jQuery::$jQuery->response['a'][$name][] = $jQuery_Action;
    }
}

function jQuery($selector) 
{
    return jQuery::addQuery($selector);
}