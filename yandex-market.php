<?
	include_once('system/config.php');
	include_once('system/db.php');
	
	$datetime_gen = date('Y-m-d H:i');
	$shop_name = 'StudioFloristic.ru';
	$company_name = 'StudioFloristic.ru';
	$shop_url = 'http://studiofloristic.ru';
	
	$r = db_query('SELECT `value` FROM `sw_variables` WHERE `name` = \'pay-tochdos\' LIMIT 1');
	$v = mysql_fetch_assoc($r);
	$dostavka = !empty($v['value']) ? $v['value'] : 250;
	
	$data = '<?xml version="1.0" encoding="windows-1251"?>'."\n";
	$data.= '<!DOCTYPE yml_catalog SYSTEM "shops.dtd">'."\n";
	$data.= '<yml_catalog date="'.$datetime_gen.'">'."\n";
	$data.= '	<shop>'."\n";
	$data.= '		<name>'.$shop_name.'</name>'."\n";
	$data.= '		<company>'.$company_name.'</company>'."\n";
	$data.= '		<url>'.$shop_url.'</url>'."\n";
	$data.= '		<currencies>'."\n";
	
	$r = db_query('SELECT `name`, `value` FROM `sw_currency` WHERE `active` = 1');
	while ($c = mysql_fetch_assoc($r)) {
		switch ($c['name']) {
			case 'rub': $data.= '			<currency id="RUR" rate="'.$c['value'].'"/>'."\n"; break;
			case 'usd': $data.= '			<currency id="USD" rate="'.$c['value'].'"/>'."\n"; break;
			case 'euro': $data.= '			<currency id="EUR" rate="'.$c['value'].'"/>'."\n"; break;
		}
	}
	
	$data.= '		</currencies>'."\n";
	$data.= '		<categories>'."\n";
	
	$r = db_query('SELECT `id`, `sid`, `title` FROM `sw_section` WHERE `active` = 1');
	while ($s = mysql_fetch_assoc($r)) {
		$data.= '			<category id="'.$s['id'].'"'.(!empty($s['sid']) ? ' parentId="'.$s['sid'].'"' : null).'>'.$s['title'].'</category>'."\n";
	}
	
	$data.= '		</categories>'."\n";
	$data.= '		<local_delivery_cost>'.$dostavka.'</local_delivery_cost>'."\n";
	$data.= '		<offers>'."\n";
	
	$result = db_query('SELECT c.`id`, c.`title`, c.`text`, s.`url`, s.`sid`, c.`price`, c.`image` FROM `sw_catalog` as c LEFT JOIN `sw_catalog_section` as s ON s.`cid` = c.`id` AND s.`first` = 1 WHERE c.`active` = 1 AND c.`price` != 0 AND s.`sid` != 0 AND s.`url` LIKE \'/catalog/bouquet/%\' ORDER BY c.`id` DESC');
	while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
		$data.= '			<offer id="'.$row['id'].'">'."\n";
		$data.= '				<url>'.trim($shop_url.$row['url']).'</url>'."\n";
		$data.= '				<price>'.number_format($row['price'], 2, '.', '').'</price>'."\n";
		$data.= '				<currencyId>RUR</currencyId>'."\n";
		$data.= '				<categoryId>'.$row['sid'].'</categoryId>'."\n";
		
		if (!empty($row['image'])) {
			$images = explode(':', $row['image']);
			$data.= '				<picture>'.$shop_url.'/files/catalog/'.$row['id'].'/w168_'.$images[0].'</picture>'."\n";
		}
		
		$data.= '				<store>true</store>'."\n";
		$data.= '				<pickup>true</pickup>'."\n";
		$data.= '				<delivery>true</delivery>'."\n";
		$data.= '				<local_delivery_cost>'.$dostavka.'</local_delivery_cost>'."\n";
		$data.= '				<name>'.htmlspecialchars($row['title']).'</name>'."\n";
		$data.= '				<description>'.htmlspecialchars($row['mini_text']).'</description>'."\n";
		$data.= '			</offer>'."\n";
	}
	
	$data.= '		</offers>'."\n";
	$data.= '	</shop>'."\n";
	$data.= '</yml_catalog>'."\n";
	
	$fp = fopen('YML.xml', 'w');
	fwrite($fp, $data);
	fclose($fp);
?>