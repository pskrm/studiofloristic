<?php
class SF_tplScope {
	
	public function load_tpl($path=''){
		return (!empty($path) && file_exists($path))? file_get_contents($path) :$path;
	}
}
