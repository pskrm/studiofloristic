<?php

// ����� ��� ������ � ��������� �������
// ������� ��� ����, ��� ��� ��� ��� ������ � ����� �� �����, ��� � ����� ���� ���������� CMS

// ����� ����� 2 �������: ������� (������ ��������) � ������������ (��������� �� �������)

// �������� �������:
// ��������� ����� ����� ��� ��������� � �������� ���� ����� � �������
// ��������� ����� �����, ���� ��� ����������, ��� ����� ������� ���������, ��� ����� ��������� �������,
// �� ������� ��� ����� ����������
// �� ������� (active == 1),

class BonusCards
{
    public static function getCardValueByCode($code)
    {
        $query = 'SELECT * FROM `sw_cards` WHERE `title` = \''.mysql_real_escape_string($code).'\'';
        $result = db_query($query);
        if (mysql_num_rows($result) == 0)
        {
            return false; // ����� � ����� ������ ���
        }
        $card = mysql_fetch_array($result, MYSQL_ASSOC);
        return $card;
    }

    /**
     * ����� ����������� ����� � ������� � ���������� ID, ������������� ������ ������� �������� �������� ���� �����
     * @param $cardKey
     * @param $userID
     * @return array
     */
    public static function attachCardToUser($cardKey, $userID)
    {
        $card = BonusCards::getCardValueByCode($cardKey);
        $result = array();
        if (!$card)
        {
            $result['error'] = true;
            $result['message'] = '����� � ����� '.$cardKey.' �� �������';
        }
        else // ���� ����� �������
        {
            if (intval($card['active']) == 0)
            {
                $result['error'] = true;
                $result['message'] = '����� � ����� '.$cardKey.' (ID �����: '.$card['id'].') ���������� ��� ������������� (���������)';
            }
            elseif($card['activation'] == 1) // ���� ����� ������������
            {
                // ������, �� ������ ������������ ��� ����� ���� ������������
                if ($card['user-id'] != $userID)
                {
                    $user = SmsQueue::getUserDataByID($card['user-id']);
                    if (is_array($user))
                    {
                        $result['error'] = true;
                        $result['message'] = '����� � ����� '.$cardKey.' (ID �����: '.$card['id'].') ������������ ��� ������� '
                                            .$user['username'].'.\n '.'(ID �������: '.$user['id'].')';
                    }
                    else
                    {
                        $result['error'] = true;
                        $result['message'] = '����� ��� ������������, �� ������ �� ������ (������). (ID �����: '.$card['id'].')';
                        $result['message'] .= '�������� ��������� ����� � ������ ����� ������ � ��������� ��������� ���������.';
                    }
                }
                else // ��������� ����� ��� ������������ ������ ��� ����� �������, ����� ������� ������ ������ � ������� ���������� ������
                {
                    $result['error'] = false; // ������ ���
                    $user = SmsQueue::getUserDataByID($userID);
                    if ($user['discount'] < $card['value'])
                    {
                        $result['discount'] = $card['value'];
                    }
                    return $result;
                }
            }
        }
        // ���������� ��������� ������ �����. ���� � $result ���� ������, �� ���������� $result
        if (isset($result['error']) && $result['error'] == true)
        {
            return $result;
        }

        // ���� �������� ������ ����� ������ ��� ������, �� ����� ���������� �� � �������
        // ��� �������� ����� � ������� ����� ������� ������ � ������� sw_cards, � ����� ��������� ������� (�����) ������
        $user = SmsQueue::getUserDataByID($userID);
        $query = 'UPDATE `sw_cards` SET `activation` = 1, `activation-date` = NOW(), `user-discount` = '.quote($user['discount']).', `user-id` = '.quote($userID).' WHERE `id` = '.mysql_real_escape_string($card['id']);
        db_query($query);

        if ($user['discount'] < $card['value'])
        {
            db_query('UPDATE `sw_users` SET `discount` = '.mysql_real_escape_string($card['value']).' WHERE `id` = '.mysql_real_escape_string($user['id']));
            $result['discount'] = $card['value'];
        }

        $result['error'] = false;
        $result['message'] = '����� � ����� '.$cardKey.' ������� ������������ ��� ������� '. $user['username'].'\n ';
        $result['message'] .= '(ID �������: '.$user['id'].')';

        return $result;
    }

} // end of class BonusCards