<?php
/**
 * Created by PhpStorm.
 * User: sshaitan
 * Date: 03.08.17
 * Time: 17:17
 */
ini_set('display_errors', 0);
error_reporting(0);
$path = realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' ). DIRECTORY_SEPARATOR;
@include_once($path .'system/config.php');
@include_once($path .'system/db.php');
@include_once($path .'system/filter.php');


$hash = isset($_REQUEST['hash']) ? filter(trim($_REQUEST['hash']), 'nohtml') :  '';
$gif= 'https://www.studiofloristic.ru/i/pixel.png';
if (empty($hash)){
	exit;
}
$data = query_new("SELECT * FROM `sw_notify` WHERE crc = " . quote($hash ) . " LIMIT 0,1");
$data = $data[0];
if (is_array($data ) && isset($data['id'])){
	db_query("UPDATE `sw_notify` SET opened_count = opened_count + 1 WHERE id = ". quote($data['id']));
	db_query("UPDATE `sw_notify` SET opened = 1 WHERE id = ". quote($data['id']));

}

$filesize = filesize( $path .'i/pixel.png');

header( 'Pragma: public' );
header( 'Expires: 0' );
header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
header( 'Cache-Control: private',false );
header( 'Content-Disposition: attachment; filename="pixel' . ($hash != '' ? $hash : uniqid('f', true) ). '.png"' );
header( 'Content-Transfer-Encoding: binary' );
header( 'Content-Length: '.$filesize );
readfile( $gif );
exit;