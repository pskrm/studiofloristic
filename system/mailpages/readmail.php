<?php
/**
 * Created by PhpStorm.
 * User: sshaitan
 * Date: 01.08.17
 * Time: 15:13
 */

//error_reporting(E_ALL);
//ini_set('display_errors', 'on');
$path = realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' ). DIRECTORY_SEPARATOR;

@include_once($path .'system/config.php');
@include_once($path .'system/db.php');
@include_once($path .'system/filter.php');
@include_once($path .'system/mainfile.php');


if (isset($_REQUEST['opt']) && isset($_REQUEST['type'])){
	// �������� ����� � ������������ � ��������� �������
	$hash = isset($_REQUEST['opt']) ? filter(trim($_REQUEST['opt']), 'nohtml') : header('Location: /404.html');
	// ��� ����� a042361
	if (strlen($hash) < 6)
	{
		header('Location: /404.html');
	}
	$msg = query_new(sprintf("SELECT * FROM `sw_abandoned_carts` WHERE `hash` = '%s' LIMIT 0,1", $hash ), 1);
	// ���������� ������ �����������, ��� ������ ������� ���
	if ($_REQUEST['type'] === 'email') {
		//��������� ������
		$mailObj = new stdClass();
		$mailObj->hash = $msg['hash'];
		$mailObj->email = $msg['email'];
		$mailObj->name = $msg['name'];
		$datas = unserialize($msg['datas']);
		$mailObj->datas  = $datas;
		$mailObj->summ = $datas['discount_summ'];
		$mailObj->summ_discount = $msg['summ_discount'];
		$mailObj->discount = $datas['discounts']['user'];
		$mailObj->type = 0;
		// ��������� ��� ������
		foreach ($datas['products'] as $p)
		{
			$prod = new stdClass();
			$prod->id = $p['id'];
			$prod->�id = $p['cid'];
			$prod->title = $p['title'];
			$prod->text = $p['text'];
			$prod->count = $p['count'];
			$prod->amount = $p['mark_disc'];
			$prod->price = $p['price'];
			$prod->url = $p['url'];
			$prod->image = $p['image'];
			$prod->type = $p['type'];
			$mailObj->products[] = $prod;
		}

		echo inUTF8(renderAbandonedMail($mailObj));
	}

    if ($_REQUEST['type'] === 'sms') {
		//��������� SMS
		$mailObj = new stdClass();
		$mailObj->hash = $msg['hash'];
		$mailObj->phone = $msg['phone'];
		$mailObj->name = $msg['name'];
		$datas = unserialize($msg['datas']);
		$mailObj->summ = $datas['discount_summ'];
		$mailObj->discount = $datas['discounts']['user'];
		$mailObj->type = 1;
		// ��������� ��� ������
		foreach ($datas['products'] as $p)
		{
			$prod = new stdClass();
			$prod->id = $p['id'];
			$prod->title = $p['title'];
			$prod->text = $p['text'];
			$prod->count = $p['count'];
			$prod->amount = $p['mark_disc'];
			$prod->price = $p['price'];
			$prod->url = $p['url'];
			$prod->image = $p['image'];
			$mailObj->products[] = $prod;
		}
		echo 'SMS ON PHONE: ' . $msg['phone'] . '<br/>';
		echo  inUTF8(renderAbandonedSMS($mailObj)). '<br/>';
		echo '=================================';
	}

}
else
{


	$hash = isset($_REQUEST['hash']) ? filter(trim($_REQUEST['hash']), 'nohtml') : header('Location: /404.html');
	if (mb_strlen($hash) != 64)
	{
		header('Location: /404.html');
	}
	$data = query_new("SELECT * FROM `sw_notify` WHERE crc = " . quote($hash) . " LIMIT 0,1");
	$data = $data[0];
	if (is_array($data) && isset($data['id']))
	{
// ������ ������
		switch ($data['send_count'])
		{
			case '1':
				$template      = file_get_contents($path . 'modules/notify/tmpl/directmail7.html');
				$discount      = '10';
				$kupon         = 'god10';
				$orderDay      = date("d", strtotime($data['order_dt']));
				$orderMonthTxt = getRusMonth(date("m", strtotime($data['order_dt'])));
				$template      = str_replace('{%ORDER_DAY_MONTH_TXT%}', $orderDay . ' ' . $orderMonthTxt, $template);
				$template      = str_replace('{%DISCOUNT%}', $discount, $template);
				$template      = str_replace('{%KUPON%}', $kupon, $template);
				$template      = str_replace('{%CRC%}', $data['crc'], $template);
				$template      = str_replace('{%ORDER_DATE%}', date("d-m-Y", strtotime($preparedData['order_dt'])), $template);
				header('Content-Type: text/html; charset=windows-1251');
				echo $template;
				break;
			case '2':
			case '3':
			case '4':
				$template      = file_get_contents($path . 'modules/notify/tmpl/directmail2.html');
				$discount      = '15';
				$kupon         = 'god15';
				$orderDay      = date("d", strtotime($data['order_dt']));
				$orderMonthTxt = getRusMonth(date("m", strtotime($data['order_dt'])));
				$template      = str_replace('{%ORDER_DAY_MONTH_TXT%}', $orderDay . ' ' . $orderMonthTxt, $template);
				$template      = str_replace('{%DISCOUNT%}', $discount, $template);
				$template      = str_replace('{%KUPON%}', $kupon, $template);
				$template      = str_replace('{%CRC%}', $data['crc'], $template);
				$template      = str_replace('{%ORDER_DATE%}', date("d-m-Y", strtotime($preparedData['order_dt'])), $template);
				header('Content-Type: text/html; charset=windows-1251');
				echo $template;
				break;
			default:
				header('Location: /404.html');
				break;
		}
	}
	else
	{
		header('Location: /404.html');
	}
}
function getRusMonth($month_num){
	switch ($month_num){
		case 1: $m='������'; break;
		case 2: $m='�������'; break;
		case 3: $m='�����'; break;
		case 4: $m='������'; break;
		case 5: $m='���'; break;
		case 6: $m='����'; break;
		case 7: $m='����'; break;
		case 8: $m='�������'; break;
		case 9: $m='��������'; break;
		case 10: $m='�������'; break;
		case 11: $m='������'; break;
		case 12: $m='�������'; break;
		default:
			return false;
			break;
	}

	return $m;
}


exit;