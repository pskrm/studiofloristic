<?
include_once $_SERVER['DOCUMENT_ROOT'].'/include/PHPMailer/src/PHPMailer.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/include/PHPMailer/src/Exception.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$_POST = stripslashes_deep($_POST);
$_GET = stripslashes_deep($_GET);
$_COOKIE = stripslashes_deep($_COOKIE);

if (!empty($_POST)) {
	foreach ($_POST as $postkey=>$postvalue) !isset($postString) ? $postString = $postkey.'='.$postvalue : $postString.= '&'.$postkey.'='.$postvalue;
	str_replace("%09", "%20", $postString);
	$postString_64 = base64_decode($postString);
	if (stripos_clone($postString,'%20union%20') OR (stripos_clone($postString,'*%2f*')) OR stripos_clone($postString,'*/union/*') OR stripos_clone($postString,' union ') OR stripos_clone($postString_64,'%20union%20') OR stripos_clone($postString_64,'*/union/*') OR stripos_clone($postString_64,' union ') OR stripos_clone($postString_64,'+union+')) die(addBlackListData(1, $postString));
}

if (!empty($_GET)) {
	foreach ($_GET as $getkey=>$getvalue) !isset($getString) ? $getString = $getkey.'='.$getvalue : $getString.= '&'.$getkey.'='.$getvalue;
	str_replace("%09", "%20", $getString);
	$getString_64 = base64_decode($getString);
	if (stripos_clone($getString,'%20union%20') OR (stripos_clone($getString,'*%2f*')) OR stripos_clone($getString,'*/union/*') OR stripos_clone($getString,' union ') OR stripos_clone($getString_64,'%20union%20') OR stripos_clone($getString_64,'*/union/*') OR stripos_clone($getString_64,' union ') OR stripos_clone($getString_64,'+union+')) die(addBlackListData(2, $_SERVER['QUERY_STRING']));
	if (preg_match("/([OdWo5NIbpuU4V2iJT0n]{5}) /", rawurldecode($loc=$_SERVER['QUERY_STRING']), $matches)) die(addBlackListData(3, $_SERVER['QUERY_STRING']));
	if (stripos_clone(strtolower($_SERVER['QUERY_STRING']), "../")) die(addBlackListData(3, $_SERVER['QUERY_STRING']));
}

function stripslashes_deep($value) {
	if (is_array($value)) {
		$value = array_map('stripslashes_deep', $value);
	} elseif (!empty($value) && is_string($value)) {
		$value = stripslashes($value);
	}
	return $value;
}
function addBlackListData($type, $det = '') {
	$security = array();
	switch ($type) {
		case 1: {
			$title = 'SQL-�������� ��� POST �������� ������';
			$assessment = 3;
			$security['data_breach'] = quote_like($det);
			$time_block = mktime(date('H'), date('i'), date('s'), date('m') + 6, date('d'), date('Y'));
		} break;
		
		case 2: {
			$title = 'SQL-�������� ��� GET �������� ������';
			$security['data_breach'] = quote_like($det);
			$assessment = 3;
			$time_block = mktime(date('H'), date('i'), date('s'), date('m') + 6, date('d'), date('Y'));
		} break;
		
		case 3: {
			$title = '������� ����� � �������� �����';
			$security['data_breach'] = quote_like($_SERVER['QUERY_STRING']);
			$assessment = 3;
			$time_block = mktime(date('H'), date('i'), date('s'), date('m') + 6, date('d'), date('Y'));
		} break;
		
		case 4: {
			$title = '������ ��� ����������� � ������ ����������';
			$security['data_breach'] = $det;
			$assessment = 3;
			$time_block = mktime(date('H'), date('i') + 5, date('s'), date('m'), date('d'), date('Y'));
		} break;
		
		default: {
			$title = '��������� �� ������������';
			$assessment = 5;
			$time_block = mktime(date('H'), date('i'), date('s'), date('m'), date('d') + 1, date('Y'));
		} break;
	}
	
	$msg = '<p>��������� ������� ������������!</p><p>'.$title.'</p><p>IP ����������: '.$_SERVER['REMOTE_ADDR'].'</p>';
	if (!empty($security['data_breach'])) $msg.= '���������: '.$security['data_breach'].'<br />';

	$mail = new PHPMailer;
	$mail->setFrom('robot@'._HOSTNAME, _HOSTNAME);
	$mail->addAddress('info@skywebsystems.ru');
	$mail->Subject = '��������� ������������ �� '._HOSTNAME;
	$mail->isHTML(true);
	$mail->CharSet = 'windows-1251';
	$mail->Body = '<html>'.$msg.'</html>';
	$mail->AltBody = strip_tags($msg);
	$mail->send();
	
	unset($security);
	return $title;
}
function getRealIpAddr() {
	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		return $_SERVER['HTTP_CLIENT_IP'];
	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))	{
		return $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		return $_SERVER['REMOTE_ADDR'];
	}
}
?>