<?
function stripos_clone($haystack, $needle, $offset=0) {
	$return = strpos(strtoupper($haystack), strtoupper($needle), $offset);
	if ($return === false) {
		return false;
	} else {
		return true;
	}
}

function filter($what, $strip="", $save="", $type="") {
	if ($strip == "nohtml") {
		$what = check_html($what, $strip);
		if ($type != "preview" AND $save != 1) $what = html_entity_decode($what, ENT_QUOTES);
	}
	if ($save == 1) {
		$what = check_words($what);
		$what = check_html($what, $strip);
		if (!get_magic_quotes_gpc()) $what = addslashes($what);
	} else {
		$what = stripslashes(FixQuotes($what,$strip));
		$what = check_words($what);
		$what = check_html($what, $strip);
	}
	return($what);
}

function check_html ($str, $strip="") {
	if ($strip == "nohtml")	$AllowableHTML=array('');
	$str = stripslashes($str);
	$str = preg_replace("#<[[:space:]]*([^>]*)[[:space:]]*>#i",'<\\1>', $str);
	$str = preg_replace("#<a[^>]*href[[:space:]]*=[[:space:]]*\"?[[:space:]]*([^\" >]*)[[:space:]]*\"?[^>]*>#i",'<a href="\\1">', $str);
	$str = preg_replace("#<[[:space:]]* img[[:space:]]*([^>]*)[[:space:]]*>#i", '', $str);
	$str = preg_replace("#<a[^>]*href[[:space:]]*=[[:space:]]*\"?javascript[[:punct:]]*\"?[^>]*>#i", '', $str);
	$tmp = "";
	while (preg_match("#<(/?[[:alpha:]]*)[[:space:]]*([^>]*)>#i",$str,$reg)) {
		$i = strpos($str,$reg[0]);
		$l = strlen($reg[0]);
		if ($reg[1][0] == "/") $tag = strtolower(substr($reg[1],1));
		else $tag = strtolower($reg[1]);
		if (isset($a) && $a = $AllowableHTML[$tag])
		if ($reg[1][0] == "/") $tag = "</$tag>";
		elseif (($a == 1) || ($reg[2] == "")) $tag = "<$tag>";
		else {
			$attrb_list=delQuotes($reg[2]);
			$tag = "<$tag" . $attrb_list . ">";
		}
		else $tag = "";
		$tmp .= substr($str,0,$i) . $tag;
		$str = substr($str,$i+$l);
	}
	$str = $tmp . $str;
	return $str;
}

function check_words($Message) {
	global $CensorList;
	$EditedMessage = $Message;
	if (is_array($CensorList)) {
		for ($i = 0; $i < count($CensorList); $i++) {
			$EditedMessage = preg_replace("#$CensorList[$i]([^a-zA-Z0-9])#i","***",$EditedMessage);
		}
	}
	return $EditedMessage;
}

function FixQuotes ($what = "") {
	while (preg_match("#\\\\'#i", $what)) {
		$what = ereg_replace("\\\\'","'",$what);
	}
	return $what;
}

function in1251($text) {
	return iconv('utf-8', 'windows-1251', $text);
}

function inUTF8($text) {
	return iconv('windows-1251', 'utf-8', $text);
}
function strencode($str=''){
    $convmap = array(0, 0xffff, 0, 0xffff);
    return mb_encode_numericentity($str, $convmap, 'windows-1251');
}
?>