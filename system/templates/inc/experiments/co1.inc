<style>
async-hide { opacity: 0 !important}
/** header */
/*#topMenu {display: none!important}*/
#blinkelement div, #blinkelement p {display: none!important}
#header .instagram {display: none!important}
/*#openTopContactsBlockLestener {top:15px!important}*/
/*#header {height:134px!important}*/
#header .contacts__icons, #header .phone__white, #header .call_number {display: none!important}
#header .detailed__contacts {display: none!important}
#header .cart, #header .howMuchItem, #header .discount {display: none!important}
#header .whereCart {top: 60px !important}
/** panels */
#leftUh, #rightUh, .htmlblock_shadow {display: none!important}
/** sidebar */
#content .rightBlock {display: none!important}
#content .leftBlock, #content .contentBlock {width: auto !important; display: block!important}}
/** breadcrumbs */
.basket_breadcrumbs {display: table !important; width: 100% !important; margin: 10px 0 !important;}
/** cart */
#content .cartItem table {width: 95%!important}
#content .cartItem table .rightInfo {display:block !important; padding: 15px !important; margin: 20px 0 0 40px !important}
/*#content .cartItem table .rightInfo table td {white-space: nowrap}*/
#content .gifts {text-align: center !important}
#content .cup_block {display: table !important; width: 100% !important;}
#content .bigBlockWithButtonOk {margin: 0 auto !important}
#content .popup_link {text-align: center !important;} /** выравнивание */
/** checkout */
#content .payment, #content .link_wrap {display: none!important}
#footer .icons, #footer .left_footer {display: none!important}
/** confirm (payment) */
</style>