<?php

if (!defined('SW')) {
    define('SW', true);
}
include_once 'includes.php';
include_once 'vars_inc.php';
include_once 'google.php';

include_once $_SERVER['DOCUMENT_ROOT'].'/include/PHPMailer/src/PHPMailer.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/include/PHPMailer/src/Exception.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
/*
include_once('system/sms.php');

if (!empty($_GLOBALS['v_phone_meneger'])) {
    foreach (explode("\n", $_GLOBALS['v_phone_meneger']) as $ph) {
        $ph = trim($ph);
        $SMS4B->GetSOAP("AccountParams",array("SessionID" => $SMS4B->GetSID()));
        $destination = $SMS4B->parse_numbers($ph);
        $startUp = $SMS4B->GetFormatDateForSmsForm(date("d-m-Y H:i:s"));
        $SMS4B->SendSmsPack($recipientSMS, $destination, htmlspecialchars(stripslashes('SF')), $startUp, '', '');
    }
}

if (!empty($phoneSYS)) {
    $SMS4B->GetSOAP("AccountParams",array("SessionID" => $SMS4B->GetSID()));
    $destination = $SMS4B->parse_numbers($phoneSYS);
    $startUp = $SMS4B->GetFormatDateForSmsForm(date("d-m-Y H:i:s"));
    $SMS4B->SendSmsPack('������� ��� ������� ��� �������. ��� �������� ������ ��� � ������� 15 �����.', $destination, htmlspecialchars(stripslashes('SF')), $startUp, '', '');
}
*/

// ��� ���� ����� ������ ����������� ����������� �� ���������� �������� SMS

class SmsQueue
{
    const REGISTRATION_EVENT = 1;
    const ORDER_ONLINE_CASH  = 2;
    const ORDER_ONLINE_PAYMENT_SUCCESS = 3;
    const ORDER_DELIVERED = 4;
    const USER_PASSWORD_RECOVERY = 5;
    const FAST_ORDER_RECEIVED = 6;
    const ORDER_ONLINE_RECEIVED_NO_CASH = 7;
    const ORDER_FAST_BUY_RECEIVED = 8;
    const REGISTRATION_EVENT_DISCOUNT = 9;

    public $messages = array(); // ������� ���������

    public $events = array(); // ��������� ����������� ����� ���������

    public static $offline_payments = array('sbbank', 'terminals', 'yandex', 'wm', 'contact', 'euroset');
    public static $nodialog_payments = array('cash', 'cards-courier','cash-in-shop','card-in-shop');

    public function __construct()
    {
        $this->events[ SmsQueue::REGISTRATION_EVENT ] = '����������� �� �����';
        $this->events[ SmsQueue::REGISTRATION_EVENT_DISCOUNT ] = '������ ����� �����������';
        $this->events[ SmsQueue::ORDER_ONLINE_CASH ] = '����� �� ��������';
        $this->events[ SmsQueue::ORDER_ONLINE_PAYMENT_SUCCESS ] = '�������� ������ ������';
        $this->events[ SmsQueue::ORDER_DELIVERED ] = '����� ���������';
        $this->events[ SmsQueue::ORDER_FAST_BUY_RECEIVED ] = '������� ������� �� �����';

        // ������ ��������� ������� ��� ����������� �� �����
        $this->messages[ SmsQueue::REGISTRATION_EVENT ] = '������������, {name}{patronymic}! ���������� ��� �� �����������';

        // ������ ��������� ������� ����� �������� ����������� ��� ��������� ������
        $this->messages[ SmsQueue::REGISTRATION_EVENT_DISCOUNT ] = '';

        // ������ ��������� ������� (���������?) ��� ���������� ������ � ������� �� ��������
        $this->messages[ SmsQueue::ORDER_ONLINE_CASH ] = '';

        // ������ ��������� ������� (���������?) ��� ����������� ������ ������
        $this->message[ SmsQueue::ORDER_ONLINE_PAYMENT_SUCCESS ] = '';

        // ������ ��������� ������� ��� ���������� ������ ������� ���������
        $this->message[ SmsQueue::ORDER_DELIVERED ] = '';

        //  ������ ��������� ������� ��� ������� �������
        $this->message[ SmsQueue::ORDER_FAST_BUY_RECEIVED ] = '';
    }

    // ����� �������� ��������� � �������
    public function addMessageInQueue($params = array())
    {
        // � ���������� ���� ���������:
        // oid - ����� ������,
        // ����� ��������� (��� ��� �����. ���� � ������� ��������
        // ��� ��������� (������ ���� ����� �� ���������������� ��������
        $_TEST_MODE = false; // �� ���������� ��������� � �������� ������

        $msg_type = intval($params['msg_type']);

        switch($msg_type)
        {
            case SmsQueue::REGISTRATION_EVENT:
                $params = $this->prepareRegistrationEventMessage($params);
                break;

            case SmsQueue::REGISTRATION_EVENT_DISCOUNT:
                $params = $this->prepareRegistrationEventDiscountMessage($params);
                break;

            case SmsQueue::ORDER_ONLINE_CASH:
                $params = $this->prepareOrderOnlineCashMessage($params);
                break;

            case SmsQueue::ORDER_ONLINE_PAYMENT_SUCCESS:
                $params = $this->prepareOrderOnlinePaymentSuccess($params);
                break;

            case SmsQueue::ORDER_DELIVERED:
                $params = $this->prepareOrderOnlineDelivered($params);
                break;

            case SmsQueue::USER_PASSWORD_RECOVERY:
                $params = $this->prepareUserPasswordRecovery($params);
                break;

            case SmsQueue::FAST_ORDER_RECEIVED:
                $params = $this->prepareFastOrderMessage($params);
                break;

            case SmsQueue::ORDER_ONLINE_RECEIVED_NO_CASH:
                $params = $this->prepareOrderOnlineReceivedNoCash($params);
                break;

            case SmsQueue::ORDER_FAST_BUY_RECEIVED:
                $params = $this->prepareFastBuyMsgUser($params);
                break;

        }

        $record = false;
        if (!isset($params['error']) && isset($params['record'])) // ���� ��� ������� ������, ���� ��������� ��� ����������� ����
        {
            $record = $params['record'];
        }

        if (is_array($record))
        {
            // ������ ��������� � ������� ��������� (������ � ������� sw_smw_queue  �� ��������� 0 � ������� status)
            $query = 'INSERT INTO `sw_sms_queue` (`oid`, `created`, `status`, `uid`, `phone`, `msg_type`, `text`) '
                . 'VALUES ('.quote($record['oid']).', '.quote($record['created']).', '.quote($record['status']).', '
                    .quote($record['uid']).', '.quote($record['phone']).', '.quote($record['msg_type']).', '.quote($record['text']).')';
            $result = db_query($query);

            // ������ ��� ���������. �����-������
            $query = ('DELETE FROM `sw_sms_queue` WHERE status = 1');

            return $result;
        }

        return true;

    }

    // ���������� ��������� � ��������������
    private function prepareRegistrationEventMessage($params)
    {
        $params['record'] = array(
            'oid' => 0, // ������ ������ ���
            'created' => date("Y-m-d H:i:s"), // ���� � ����� �������� ������
            'status' => 0 , // �� ����������, ������� ��
            'uid' => 0, // User ID ��� ��� �� ��������
            'phone' => $params['destination'], // �������, �� ������� ��� SMS ����� ����������
            'msg_type' => SmsQueue::REGISTRATION_EVENT,  // ��� ���������
            'text' => $params['text']
        );

        return $params;
    }

    private function prepareRegistrationEventDiscountMessage($params)
    {
        $params['record'] = array(
            'oid' => 0, // ������ ������ ���
            'created' => date("Y-m-d H:i:s"), // ���� � ����� �������� ������
            'status' => 0 , // �� ����������, ������� ��
            'uid' => 0, // User ID ��� ��� �� ��������
            'phone' => $params['destination'], // �������, �� ������� ��� SMS ����� ����������
            'msg_type' => SmsQueue::REGISTRATION_EVENT_DISCOUNT,  // ��� ���������
            'text' => $params['text']
        );

        return $params;
    }

    // ���������� ��������� � ��������������
    private function prepareFastOrderMessage($params)
    {
        $params['record'] = array(
            'oid' => 0, // ������ ������ ���
            'created' => date("Y-m-d H:i:s"), // ���� � ����� �������� ������
            'status' => 0 , // �� ����������, ������� ��
            'uid' => 0, // User ID ��� ��� �� ��������
            'phone' => $params['destination'], // �������, �� ������� ��� SMS ����� ����������
            'msg_type' => SmsQueue::FAST_ORDER_RECEIVED,  // ������� �����
            'text' => '���������� �� �����! �� ��� ����������. 88003331291 StudioFloristic.ru'
        );

        return $params;
    }

    // ��������� ������� ��� ���������� ������ � ������� �� ��������
    private function prepareOrderOnlineCashMessage($params)
    {
        $oid = $params['oid']; // ����� ������
        $order = $this->getOrderDataByID($oid);

        // �������� �������� �������� ����� ����� sms-��������� ��� ���������� ������-������ �� ��������
        // � ������: 11111. �������� ������� ���. 88003331291 StudioFloristic.ru
        $params['record'] = array(
            'oid' => $oid, // ����� ������
            'created' => date("Y-m-d H:i:s"), // ���� � ����� �������� ������
            'status' => 0, // ������ - �� ���������� ���, ������� ��
            'uid' => $order['uid'], // ID ������������ �� ������
            'phone' => $order['phone-sys'], // ������� �� ������ ������, �������������� ��� sms-�������
            'msg_type' => SmsQueue::ORDER_ONLINE_CASH, // ��� ��������� - ������ ������-������ ���������
            'text' => '� ������: '.$oid.'. �� ��� ����������. 88003331291 StudioFloristic.ru' // ����� ���������
        );

        return $params;
    }

    // ��������� ������� ��� ����������� ������ ������
    private function prepareOrderOnlinePaymentSuccess($params)
    {
        // ����� id �� ������� sw_orders, ���������� ������ �
        // ���������� SMS

        // ����� ���� �������� ����
        $oid = $params['oid'];
        $order = $this->getOrderDataByID($oid);
        $itogo_format = number_format($order['itogo'], 0, '.', ' '); // ������� ����� � ������� 3 754
        // ������ ����� �����, �������
        $itogo_format .= ' �.';

        $params = array(
            'text' => 'StudioFloristic.ru ��� ����� �'.$oid.' �� ����� '.$itogo_format.' �������. 8-800-333-12-91',
            'msg_type' => SmsQueue::ORDER_ONLINE_PAYMENT_SUCCESS,
            'sender' => htmlspecialchars(stripslashes('SF')),
            'destination' => $order['phone-sys']  // ������ ��������� ��� ������
        );

        $params['record'] = array(
            'oid' => $oid, // ����� ������
            'created' => date("Y-m-d H:i:s"), // ���� � ����� �������� ������
            'status' => 0, // ������ - �� ���������� ���, ������� ��
            'uid' => $order['uid'], // ID ������������ �� ������
            'phone' => $order['phone-sys'], // ������� �� ������ ������, �������������� ��� sms-�������
            'msg_type' => SmsQueue::ORDER_ONLINE_CASH, // ��� ��������� - ������ ������-������ ���������
            'text' => 'StudioFloristic.ru ��� ����� �'.$oid.' �� ����� '.$itogo_format.' �������. 8-800-333-12-91', // ����� ���������
        );

        return $params;

    }

    private function prepareOrderOnlineDelivered($params)
    {
        // ����� id �� ������� sw_orders, ���������� ������ � ���������� SMS
        // 2 ���� SMS � ����������� �� ���� ������
        $oid = $params['oid'];

        // ��������, ��� �� sms, ������������ �� ����� ������ � ���� �� ��������
        $query = 'SELECT * FROM `sw_sms_queue` WHERE oid = '.$oid.' AND msg_type = '.SmsQueue::ORDER_DELIVERED;
        $result = db_query($query);
        if (mysql_num_rows($result) > 0)
        {
            return $params; // SMS ����� ���� (��� ��������) ��� ����� ������ ��� ������������, ����� � �������
        }

        // ��������� �����
        $order = $this->getOrderDataByID($oid);
        $discount = 10;
        $discount_tail = '';
        if (!empty($order['uid']))
        {
            $user = $this->getUserDataByID($order['uid']);
            if ($user['discount'] < 10)
            {
                db_query('UPDATE `sw_users` SET `discount` = 10 WHERE id = '.$order['uid']);
                $discount_tail = '���� ������ �� ��������� ������� - 10%';
            }
            else
            {
                $discount_tail = '���� ������ �� ��������� ������� - '.$user['discount'].'%';
            }
        }
        else
        {
            $discount_tail = '�� ����� ��� ����� �� ������ � ������� 10% �� ��������� ������. ��� ������ - sms10.';
        }

        // ������������� ������
        $smsForOnlinePayment = '��� ����� �'.$oid.' ���������. ��� �� ��� �����������? '.$discount_tail.' 8-800-333-12-91. StudioFloristic.ru';

        $params['record'] = array(
            'oid' => $oid,
            'created' => date("Y-m-d H:i:s"), // ���� � ����� �������� ������
            'status' => 0, // ������ - �� ���������� ���, ������� ��
            'uid' => $order['uid'], // ID ������������ �� ������
            'phone' => $order['phone-sys'], // ������� �� ������ ������, �������������� ��� sms-�������
            'msg_type' => SmsQueue::ORDER_DELIVERED, // ��� ��������� - ������ ������-������ ���������
            'text' => $smsForOnlinePayment
        );

        return $params;
    }

    public function prepareUserPasswordRecovery($params)
    {
        $host_name = $_SERVER['SERVER_NAME'];
        // ��� ����� ������ ID ������������, ��� ������ ���� � ����������
        $user = $this->getUserDataByID($params['uid']);
        if ($user)
        {
            $longUrl = 'https://studiofloristic.ru/login/op=restoration&key='.$user['activation'].'&lost=1';

            $shortLink = generatorShorLink($longUrl);

            $params['record'] = array(
                'oid' => 0,
                'created' => date("Y-m-d H:i:s"), // ���� � ����� �������� ������
                'status' => 0, // ������ - �� ���������� ���, ������� ��
                'uid' => $params['uid'], // ID ������������ �� ������
                'phone' => $user['sys-phone'],
                'msg_type' => SmsQueue::USER_PASSWORD_RECOVERY,
                'text' => 'Studio Floristic: ���� ������ ��� �������������� ������: '.$shortLink.'  �� ����� ���� ������ ����������� ;) 8-800-333-123-91'
            );
        }

        return $params;
    }

    // ���������� ���������� ��������� ������������ � ������� �������
    public function prepareFastBuyMsgUser($params){
        $oid = intval($params['oid']);

        $params['record'] = array(
            'oid' => $oid,
            'created' => date("Y-m-d H:i:s"), // ���� � ����� �������� ������
            'status' => 0, // ������ - �� ���������� ���, ������� ��
            'uid' => -1, // ID ������������ �� ������
            'phone' => $params['phone'],
            'msg_type' => SmsQueue::ORDER_FAST_BUY_RECEIVED,
            'text' => '���������� �� �����! �� ��� ����������.  880033312391'
        );

        return $params;
    }

    public function prepareOrderOnlineReceivedNoCash($params)
    {
        $oid = $params['oid'];

        // ��������, ��� �� sms, ������������ �� ����� ������ � ���� �� ��������
        $query = 'SELECT * FROM `sw_sms_queue` WHERE oid = '.$oid.' AND msg_type = '.SmsQueue::ORDER_ONLINE_RECEIVED_NO_CASH;
        $result = db_query($query);
        if (mysql_num_rows($result) > 0)
        {
            return $params; // SMS ����� ���� (��� ��������) ��� ����� ������ ��� ������������, ����� � �������
        }

        // ��������� �����
        $order = $this->getOrderDataByID($oid);

        $params['record'] = array(
            'oid' => $oid, // ����� ������
            'created' => date("Y-m-d H:i:s"), // ���� � ����� �������� ������
            'status' => 0, // ������ - �� ���������� ���, ������� ��
            'uid' => $order['uid'], // ID ������������ �� ������
            'phone' => $order['phone-sys'], // ������� �� ������ ������, �������������� ��� sms-�������
            'msg_type' => SmsQueue::ORDER_ONLINE_RECEIVED_NO_CASH, // ��� ��������� - ������ ������-������ ���������
            'text' => '� ������: '.$oid.'. �� ��� ����������. 88003331291 StudioFloristic.ru' // ����� ���������
        );

        return $params;

    }

    public function releaseMsgQueue()
    {
        // �������� �������� ��������� �����. �������� ��, � ������� ������ - 0
        // ����� �� ������ - �������� ���������, ����������� � ���� ������ 0

        require_once dirname(__FILE__).'/sms.php';
        $LOGIN = 'studiofloristic';
        $PASSWORD = 'Srloqj2098ed';
        $SMS4B = new Csms4b_general($LOGIN,$PASSWORD);

        $error_message = 'Error';

        $mysql_host = 'localhost';
        $mysql_login = 'root';
        $mysql_password = '123';
        $mysql_database = '123';
        $mysql_charset = 'cp1251';

        if (!@mysql_connect($mysql_host, $mysql_login, $mysql_password)) die ($error_message);
        if (!@mysql_select_db($mysql_database)) die ($error_message);
        mysql_query('SET NAMES '.$mysql_charset);

        $query = 'SELECT * FROM `sw_sms_queue` WHERE `status` = 0';
        error_reporting(E_ALL);
        $r = mysql_query($query);
        $e = mysql_error();
        var_dump($e);
        while ($msg_row = mysql_fetch_array($r, MYSQL_ASSOC))
        {
            $SMS4B->GetSOAP("AccountParams",array("SessionID" => $SMS4B->GetSID()));
            $destination = $SMS4B->parse_numbers($msg_row['phone']);
            $startUp = $SMS4B->GetFormatDateForSmsForm(date("d-m-Y H:i:s"));
            @$result = $SMS4B->SendSmsPack($msg_row['text'], $destination, htmlspecialchars(stripslashes('SF')), $startUp, '', '');
            echo $msg_row['text'].PHP_EOL;
            mysql_query('UPDATE `sw_sms_queue` SET status = 1 WHERE id = '.$msg_row['id']);
        }

        // ������ ��� ���������
        $query = ('DELETE FROM `sw_sms_queue` WHERE status = 1');
        // query_new($query); ���� ������� ������ �� �����
    }

    // ��������������� ������� ��� �������� Email ������� (���������� ��� ��������� �������)
    public function sendEmailWhenOrderDelivered($params)
    {

        global $_GLOBALS;
        $headers = "Content-type: text/html; charset=windows-1251 \r\n";
        $headers .= "From: �������� ������ <".$_GLOBALS['v_email_admin'] .">\r\n";

        // ��� ������� �����
        $order = $this->getOrderDataByID($params['oid']);
        if (!$order)
        {
            return false;
        }

        $user = $this->getUserDataByID($order['uid']); // ������� ������������, ���� ��� ��������

        // ������ ��� ������ ������, �������, ���� �� � ��� ����� ����������, ������� �� �������� ���� e-mail
        $email = false;
        if (isset($order['email']) && !empty($order['email']) && filter_var($order['email'], FILTER_VALIDATE_EMAIL ))
        {
            $email = $order['email'];
        }

        // ���� ��� ��� ��� email
        if ($email == false)
        {
            if ($user && isset($user['email']) && filter_var($user['email'], FILTER_VALIDATE_EMAIL ))
            {
                $email = $user['email'];
            }
        }

        // ���� ���� ����� ����� � ��� ��� email ������������, �� ����� ��� ������ ��������, ���������
        if ($email == false)
        {
            return false;
        }

        // ����� ������. ��� ������ ������� ������ � ��� ������ ������� �����
        $online_payments = array(
            'cards', // => '��������� ������: ROBOKASSA',
            'cards-assist', // => '��������� ������: Assist',
            'yandex', // => '������ ������',
            'wm'. // => 'Web money',
            'paypal', // => 'PayPal',
            'contact', // => '��������',
            'euroset', // => '�������',
            'sbbank', // => '��������',
            'courier', // => '����� ������� �� �������',
            'onlinebanks', // => '�������� ����',
            'mobileapp', // => '��������� ����������',
            'mobileoperators', // => '��������� ���������',
            'emoney', // => '����������� ������',
        );

        $cash_payments = array(
            'cash', // => '���������',
            'cards-courier', // => '��������� ������ �������'
            'terminals', // => '��������� ���������'
        );

        $online_pay = in_array($order['type'], $online_payments);
        $cash_pay = in_array($order['type'], $cash_payments);

        // ������ � �����������
        $hello = '������� ������� ����� ���, ';
        $userName = '';
        $is_registered_user = false;

        if($user) // ���� ������������������ ����, �������� ������ ���������
        {
            $userName .= ((int)$user['sex'] == 1) ? ' ��������� ' : ' ��������� '; // ���������� ������ �� ����� ��������� �� �������� ��������
            $userName .= $user['username'];
            $is_registered_user = true;
        }
        else // ������������ �� ���������������
        {
            $userName .= $order['uName'];
        }

        $hello = $this->clearDoubleSpace($hello . $userName); //

        $couponDiscount = 10;
        $host_name = $_SERVER['SERVER_NAME'];
        $message  = '<p style="text-align:center"><img src="http://'.$host_name.'/i/logo-studio-floristic-green.png" alt="'.$_GLOBALS['v_sitename'].'"></p>'.PHP_EOL;
        $message .= '<p>'.$hello.'</p>'.PHP_EOL;
        $message .= '<p>��� ����� � '.$order['id'].' <b>���������!</b></p>'.PHP_EOL;
        $message .= '<p>�� ����� ����� �����, ��� �� ��� �����������? :)</p>'.PHP_EOL;
        $message .= '<p>�� ����� ��� ����� ������������, ���� �� ������� ��������� ����� � <a href="http://www.studiofloristic.ru/reviews.html">�������� ���� ����� �� ����� �����</a> ��� �� <a href="https://market.yandex.ru/shop/56014/reviews?sort_by=date">������.�������</a>.</p>'.PHP_EOL;
        $message .= '<p></p>'.PHP_EOL;

        if ($is_registered_user)
        {
            if ($user['discount'] < $couponDiscount) // ���� ������� ������ ������������ ������ ������ �� ������, ��
            {
                $message .= '<p>����� � ������� ������������� �� ����� ���, '.$userName.', ����� �� ������ � ������� 10%.</p>'.PHP_EOL;
                $message .= '<p>��� ��������� ������ � ������ �������� ����� ������ ���:'.PHP_EOL;
                $message .= '<p><b>mail10</b></p>'.PHP_EOL;
                $message .= '<p>���� ����� ����� ����������� �� ��� ���� ����������� �������.</p>'.PHP_EOL;
            }
            else // ���� ������� ������ ������� ������ ��� ����� ������, �� ������ �������� ��� �� ����
            {
                $message .= '<p>� ������� �������� ���������� ���, '.$userName
                    .', ��� ��� ���������� �������� ��������� ������� ������</p>'.PHP_EOL;
                $message .= '<p>�� ������ ������ <b>���� ������ ���������� '.$user['discount'].'%</b>. '
                    .'��������� � ������� � ����� ��������-�������� �� ������ ������ �� <a href="http://www.studiofloristic.ru/discounts.html">���� ��������</a>.</p>'.PHP_EOL;
            }
        }
        else // ���� ������ �����������������, �� ������ � ���� ���
        {
            $message .= '<p>����� � ������� ������������� �� ����� ���, '.$userName.', ����� �� ������ � ������� 10 %.</p>'.PHP_EOL;
            $message .= '<p>��� ��������� ������ ����� ������������������ � � ������ �������� ����� ������ ��� <b>mail10</b></p>'.PHP_EOL;
            $message .= '<p>� ����� ���� ����� ����� ����������� �� ��� ���� ����������� �������.</p>'.PHP_EOL;
            $message .= '<p>��� ����������� ����� �� ������ ������������ ������ ��� ����� �������. ������������ ����� �� ������ �� ������ ��� ���������� ���������� ������, � ������� ����������</p>'.PHP_EOL;
        }

        $message .= '<p>�� ���� �������� ����������� �� �������� 8-800-333-12-91, <b>�������������</b>.'.PHP_EOL;
        $message .= '<p><b>������ ��� � ����� ������� ������� � �����������!</b></p>'.PHP_EOL;

        $message .= '<p><b style="color:green;">StudioFloristic.ru</b> <b>-</b> <b style="color:mediumpurple;">�����</b> <b style="color:orange">���</b> <b style="color:darkred;">�������</b></p>'.PHP_EOL;


        $theme = "��� ����� � ".$params['oid']." ���������";

        try {
            $mail = new PHPMailer;
            $mail->setFrom($_GLOBALS['v_email_admin'], '�������� ������');
            $mail->addAddress($email);
            $mail->Subject = $theme;
            $mail->isHTML(true);
            $mail->CharSet = 'windows-1251';
            $mail->Body = $message;
            $mail->AltBody = strip_tags($message);
            $mail->send();
        } catch (Exception $e) {
            unset($e);
        }

        $this->storeMailToFile(array('subject' => $theme,'body' => $message, 'headers' => $headers));

        return true;
    }


    public function sendEmailWhenOrderAcepted($params)
    {
        global $_GLOBALS;
        $headers = "Content-type: text/html; charset=windows-1251 \r\n";
        $headers .= "From: �������� ������ <".$_GLOBALS['v_email_admin'].">\r\n";
        // ��� ������� �����
        $order = $this->getOrderDataByID($params['oid']);
        if (!$order)
        {
            return false;
        }

        $user = $this->getUserDataByID($order['uid']); // ������� ������������, ���� ��� ��������

        // ������ ��� ������ ������, �������, ���� �� � ��� ����� ����������, ������� �� �������� ���� e-mail
        $email = false;
        if (isset($order['email']) && !empty($order['email']) && filter_var($order['email'], FILTER_VALIDATE_EMAIL ))
        {
            $email = $order['email'];
        }

        // ���� ��� ��� ��� email
        if ($email == false)
        {
            if ($user && isset($user['email']) && filter_var($user['email'], FILTER_VALIDATE_EMAIL ))
            {
                $email = $user['email'];
            }
        }

        // ���� ���� ����� ����� � ��� ��� email ������������, �� ����� ��� ������ ��������, ���������
        if ($email == false)
        {
            return false;
        }

        // ������ ����� ������
        // ����� ������. ��� ������ ������� ������ � ��� ������ ������� �����
        $online_payments = array(
            'cards', // => '��������� ������: ROBOKASSA',
            'cards-assist', // => '��������� ������: Assist',
            'yandex', // => '������ ������',
            'wm'. // => 'Web money',
            'paypal', // => 'PayPal',
            'contact', // => '��������',
            'euroset', // => '�������',
            'sbbank', // => '��������',
            'courier', // => '����� ������� �� �������',
            'onlinebanks', // => '�������� ����',
            'mobileapp', // => '��������� ����������',
            'mobileoperators', // => '��������� ���������',
            'emoney', // => '����������� ������',
        );

        $cash_payments = array(
            'cash', // => '���������',
            'cards-courier', // => '��������� ������ �������'
            'terminals', // => '��������� ���������'
        );

        $online_pay = in_array($order['type'], $online_payments);
        $cash_pay = in_array($order['type'], $cash_payments);

        // ������ � �����������
        $hello = '������� ������� ����� ���, ';
        $userName = '';
        $is_registered_user = false;

        if($user) // ���� ������������������ ����, �������� ������ ���������
        {
            $userName .= ((int)$user['sex'] == 1) ? ' ��������� ' : ' ��������� '; // ���������� ������ �� ����� ��������� �� �������� ��������
            $userName .= $user['username'];
            $is_registered_user = true;
        }
        else // ������������ �� ���������������
        {
            $userName .= $order['uName'];
        }

        $hello = $this->clearDoubleSpace($hello . $userName); //
        $host_name = $_SERVER['SERVER_NAME'];
        $message  = '<p style="text-align:center"><img src="http://'.$host_name.'/i/logo-studio-floristic-green.png" alt="'.$_GLOBALS['v_sitename'].'"></p>';
        $message .= '<p>'.$hello.'</p>';
        $message .= '<p>��� ����� � '.$order['id'].' <b>�����������</b>, � ��������� � ������ ���������.</b></p>';


        if(array_sum($discounts)>0){ $includs[] = '������ '.array_sum($discounts).'%';}
        $msg = '<p style="text-align:center"><img src="http://'._HOSTNAME.'/i/logo-studio-floristic-green.png" alt="'.$_GLOBALS['v_sitename'].'"></p>'.PHP_EOL;
        $msg.= '<p>��������� '.(_IS_USER ? $u['username']:'������!').'</p>'.PHP_EOL;
        $msg.= '<p>��� ����� ����� '.$oid.' �� '.date('d.m.Y', $time).' ������.</p>'.PHP_EOL;
        $msg.= !empty($unix_time)?'<p><b>����� ��������:</b> '.$order_title.'</p>'.PHP_EOL:''.PHP_EOL;
        $msg.= '<p>��������� ������: '.$itog.' '.$cur['reduction'].(!empty($includs) ? ' ������� '.implode(', ',$includs): null).'.</p>'.PHP_EOL;
        $msg.= '<p>������ ������:</p><ul>'.implode('',$itemsMail).'</ul>'.PHP_EOL;
        $msg.= '<p>����������, ��� ��������� � ����� ���������� ����������� ���������� ����� ������ ������ - '.$oid.'.</p>'.PHP_EOL;
        $msg.= '<p>�������: 8-800-333-12-91 (��������� �� ������)</p>'.PHP_EOL;
        $msg.= '<p>E-mail: info@studiofloristic.ru</p>'.PHP_EOL;
        $msg.= '<p>������� �� �������!</p>'.PHP_EOL;

        $theme = '��� ����� �� ����� �'.$_GLOBALS['v_sitename'].'�';
       // mail($u['email'], $theme, $msg, $headers);
        $message .= '<p>� ��������� ����� �������� �������� � ���� ��� ������������� � ��������� �������.</p>';
        $message .= '<p>�� ����� ��� ����� ������������, ���� �� ������� ��������� ����� � <a href="http://www.studiofloristic.ru/reviews.html">�������� ���� ����� �� ����� �����</a> ��� �� <a href="https://market.yandex.ru/shop/56014/reviews?sort_by=date">������.�������</a>.</p>'.PHP_EOL;
        $message .= '<p></p>'.PHP_EOL;

        if ($is_registered_user)
        {
            if ($user['discount'] < $couponDiscount) // ���� ������� ������ ������������ ������ ������ �� ������, ��
            {
                $message .= '<p>����� � ������� ������������� �� ����� ���, '.$userName.', ����� �� ������ � ������� 10%.</p>'.PHP_EOL;
                $message .= '<p>��� ��������� ������ � ������ �������� ����� ������ ���:'.PHP_EOL;
                $message .= '<p><b>mail10</b></p>'.PHP_EOL;
                $message .= '<p>���� ����� ����� ����������� �� ��� ���� ����������� �������.</p>'.PHP_EOL;
            }
            else // ���� ������� ������ ������� ������ ��� ����� ������, �� ������ �������� ��� �� ����
            {
                $message .= '<p>� ������� �������� ���������� ���, '.$userName
                    .', ��� ��� ���������� �������� ��������� ������� ������</p>'.PHP_EOL;
                $message .= '<p>�� ������ ������ <b>���� ������ ���������� '.$user['discount'].'%</b>. '
                    .'��������� � ������� � ����� ��������-�������� �� ������ ������ �� <a href="http://www.studiofloristic.ru/discounts.html">���� ��������</a>.</p>'.PHP_EOL;
            }
        }
        else // ���� ������ �����������������, �� ������ � ���� ���
        {
            $message .= '<p>����� � ������� ������������� �� ����� ���, '.$userName.', ����� �� ������ � ������� 10 %.</p>'.PHP_EOL;
            $message .= '<p>��� ��������� ������ ����� ������������������ � � ������ �������� ����� ������ ��� <b>mail10</b></p>'.PHP_EOL;
            $message .= '<p>� ����� ���� ����� ����� ����������� �� ��� ���� ����������� �������.</p>'.PHP_EOL;
            $message .= '<p>��� ����������� ����� �� ������ ������������ ������ ��� ����� �������. ������������ ����� �� ������ �� ������ ��� ���������� ���������� ������, � ������� ����������</p>'.PHP_EOL;
        }

        $message .= '<p>�� ���� �������� ����������� �� �������� 8-800-333-12-91, <b>�������������</b>.'.PHP_EOL;
        $message .= '<p><b>������ ��� � ����� ������� ������� � �����������!</b></p>'.PHP_EOL;

        $message .= '<p><b style="color:green;">StudioFloristic.ru</b> <b>-</b> <b style="color:mediumpurple;">�����</b> <b style="color:orange">���</b> <b style="color:darkred;">�������</b></p>'.PHP_EOL;

        $headers = "Content-type: text/html; charset=windows-1251 \r\n";
        $headers .= "From: �������� ������ <".$_GLOBALS['v_email_admin'].">\r\n";

        $theme = "��� ����� � ".$params['oid']." ������";

        $mail = new PHPMailer;
        try {
            $mail->setFrom($_GLOBALS['v_email_admin'], '�������� ������');
            $mail->addAddress($email);
            $mail->Subject = $theme;
            $mail->isHTML(true);
            $mail->CharSet = 'windows-1251';
            $mail->Body = $message;
            $mail->AltBody = strip_tags($message);
            $mail->send();
        } catch (Exception $e) {
            unset($e);
        }

        $this->storeMailToFile(array('subject' => $theme,'body' => $message, 'headers' => $headers));

        return true;
    }

    public function sendEmailWhenOrderReceived($params)
    {
        global $_GLOBALS;
        $headers = "Content-type: text/html; charset=windows-1251 \r\n";
        $headers .= "From: �������� ������ <".$_GLOBALS['v_email_admin'].">\r\n";
        // ��� ������� �����
        $order = $this->getOrderDataByID($params['oid']);
        if (!$order)
        {
            return false;
        }

        $user = $this->getUserDataByID($order['uid']); // ������� ������������, ���� ��� ��������

        // ������ ��� ������ ������, �������, ���� �� � ��� ����� ����������, ������� �� �������� ���� e-mail
        $email = false;
        if (isset($order['email']) && !empty($order['email']) && filter_var($order['email'], FILTER_VALIDATE_EMAIL ))
        {
            $email = $order['email'];
        }

        // ���� ��� ��� ��� email
        if ($email == false)
        {
            if ($user && isset($user['email']) && filter_var($user['email'], FILTER_VALIDATE_EMAIL ))
            {
                $email = $user['email'];
            }
        }

        // ���� ���� ����� ����� � ��� ��� email ������������, �� ����� ��� ������ ��������, ���������
        if ($email == false)
        {
            return false;
        }

        // ������ ����� ������
        // ����� ������. ��� ������ ������� ������ � ��� ������ ������� �����
        $online_payments = array(
            'cards', // => '��������� ������: ROBOKASSA',
            'cards-assist', // => '��������� ������: Assist',
            'yandex', // => '������ ������',
            'wm'. // => 'Web money',
            'paypal', // => 'PayPal',
            'contact', // => '��������',
            'euroset', // => '�������',
            'sbbank', // => '��������',
            'courier', // => '����� ������� �� �������',
            'onlinebanks', // => '�������� ����',
            'mobileapp', // => '��������� ����������',
            'mobileoperators', // => '��������� ���������',
            'emoney', // => '����������� ������',
        );

        $cash_payments = array(
            'cash', // => '���������',
            'cards-courier', // => '��������� ������ �������'
            'terminals', // => '��������� ���������'
        );

        $online_pay = in_array($order['type'], $online_payments);
        $cash_pay = in_array($order['type'], $cash_payments);

        // ������ � �����������
        $hello = '������� ������� ����� ���, ';
        $userName = '';
        $is_registered_user = false;

        if($user) // ���� ������������������ ����, �������� ������ ���������
        {
            $userName .= ((int)$user['sex'] == 1) ? ' ��������� ' : ' ��������� '; // ���������� ������ �� ����� ��������� �� �������� ��������
            $userName .= $user['username'];
            $is_registered_user = true;
        }
        else // ������������ �� ���������������
        {
            $userName .= $order['uName'];
        }

        $hello = $this->clearDoubleSpace($hello . $userName); //
        $host_name = $_SERVER['SERVER_NAME'];
        $message  = '<p style="text-align:center"><img src="http://'.$host_name.'/i/logo-studio-floristic-green.png" alt="'.$_GLOBALS['v_sitename'].'"></p>';
        $message .= '<p>'.$hello.'</p>';
        $message .= '<p>��� ����� � '.$order['id'].' <b>������, � ��������� � ������ ���������.</b></p>';


        if(array_sum($discounts)>0){ $includs[] = '������ '.array_sum($discounts).'%';}
        $msg = '<p style="text-align:center"><img src="http://'._HOSTNAME.'/i/logo-studio-floristic-green.png" alt="'.$_GLOBALS['v_sitename'].'"></p>'.PHP_EOL;
        $msg.= '<p>��������� '.(_IS_USER ? $u['username']:'������!').'</p>'.PHP_EOL;
        $msg.= '<p>��� ����� ����� '.$oid.' �� '.date('d.m.Y', $time).' ������.</p>'.PHP_EOL;
        $msg.= !empty($unix_time)?'<p><b>����� ��������:</b> '.$order_title.'</p>'.PHP_EOL:''.PHP_EOL;
        $msg.= '<p>��������� ������: '.$itog.' '.$cur['reduction'].(!empty($includs) ? ' ������� '.implode(', ',$includs): null).'.</p>'.PHP_EOL;
        $msg.= '<p>������ ������:</p><ul>'.implode('',$itemsMail).'</ul>'.PHP_EOL;
        $msg.= '<p>����������, ��� ��������� � ����� ���������� ����������� ���������� ����� ������ ������ - '.$oid.'.</p>'.PHP_EOL;
        $msg.= '<p>�������: 8-800-333-12-91 (��������� �� ������)</p>'.PHP_EOL;
        $msg.= '<p>E-mail: info@studiofloristic.ru</p>'.PHP_EOL;
        $msg.= '<p>������� �� �������!</p>'.PHP_EOL;

        $theme = '��� ����� �� ����� �'.$_GLOBALS['v_sitename'].'�';

        try {
            $mail = new PHPMailer;
            $mail->setFrom($_GLOBALS['v_email_admin'], '�������� ������');
            $mail->addAddress($u['email']);
            $mail->Subject = $theme;
            $mail->isHTML(true);
            $mail->CharSet = 'windows-1251';
            $mail->Body = $msg;
            $mail->AltBody = strip_tags($msg);
            $mail->send();
        } catch (Exception $e) {
            unset($e);
        }

        $message .= '<p>� ��������� ����� �������� �������� � ���� ��� ������������� � ��������� �������.</p>'.PHP_EOL;
        $message .= '<p>�� ����� ��� ����� ������������, ���� �� ������� ��������� ����� � <a href="http://www.studiofloristic.ru/reviews.html">�������� ���� ����� �� ����� �����</a> ��� �� <a href="https://market.yandex.ru/shop/56014/reviews?sort_by=date">������.�������</a>.</p>'.PHP_EOL;
        $message .= '<p></p>'.PHP_EOL;

        if ($is_registered_user)
        {
            if ($user['discount'] < $couponDiscount) // ���� ������� ������ ������������ ������ ������ �� ������, ��
            {
                $message .= '<p>����� � ������� ������������� �� ����� ���, '.$userName.', ����� �� ������ � ������� 10%.</p>'.PHP_EOL;
                $message .= '<p>��� ��������� ������ � ������ �������� ����� ������ ���:'.PHP_EOL;
                $message .= '<p><b>mail10</b></p>'.PHP_EOL;
                $message .= '<p>���� ����� ����� ����������� �� ��� ���� ����������� �������.</p>'.PHP_EOL;
            }
            else // ���� ������� ������ ������� ������ ��� ����� ������, �� ������ �������� ��� �� ����
            {
                $message .= '<p>� ������� �������� ���������� ���, '.$userName
                    .', ��� ��� ���������� �������� ��������� ������� ������</p>'.PHP_EOL;
                $message .= '<p>�� ������ ������ <b>���� ������ ���������� '.$user['discount'].'%</b>. '
                    .'��������� � ������� � ����� ��������-�������� �� ������ ������ �� <a href="http://www.studiofloristic.ru/discounts.html">���� ��������</a>.</p>'.PHP_EOL;
            }
        }
        else // ���� ������ �����������������, �� ������ � ���� ���
        {
            $message .= '<p>����� � ������� ������������� �� ����� ���, '.$userName.', ����� �� ������ � ������� 10 %.</p>'.PHP_EOL;
            $message .= '<p>��� ��������� ������ ����� ������������������ � � ������ �������� ����� ������ ��� <b>mail10</b></p>'.PHP_EOL;
            $message .= '<p>� ����� ���� ����� ����� ����������� �� ��� ���� ����������� �������.</p>'.PHP_EOL;
            $message .= '<p>��� ����������� ����� �� ������ ������������ ������ ��� ����� �������. ������������ ����� �� ������ �� ������ ��� ���������� ���������� ������, � ������� ����������</p>'.PHP_EOL;
        }

        $message .= '<p>�� ���� �������� ����������� �� �������� 8-800-333-12-91, <b>�������������</b>.'.PHP_EOL;
        $message .= '<p><b>������ ��� � ����� ������� ������� � �����������!</b></p>'.PHP_EOL;

        $message .= '<p><b style="color:green;">StudioFloristic.ru</b> <b>-</b> <b style="color:mediumpurple;">�����</b> <b style="color:orange">���</b> <b style="color:darkred;">�������</b></p>'.PHP_EOL;

        $headers = "Content-type: text/html; charset=windows-1251 \r\n";
        $headers .= "From: �������� ������ <".$_GLOBALS['v_email_admin'].">\r\n";

        $theme = "��� ����� � ".$params['oid']." ������";

        $mail = new PHPMailer;
        try {
            $mail->setFrom($_GLOBALS['v_email_admin'], '�������� ������');
            $mail->addAddress($email);
            $mail->Subject = $theme;
            $mail->isHTML(true);
            $mail->CharSet = 'windows-1251';
            $mail->Body = $message;
            $mail->AltBody = strip_tags($message);
            $mail->send();
        } catch (Exception $e) {
            unset($e);
        }

        $this->storeMailToFile(array('subject' => $theme,'body' => $message, 'headers' => $headers));

        return true;
    }

    // ������� ������ � ������� ������
    public static function getOrderDataByID($oid)
    {
        $query = 'SELECT * FROM `sw_orders` WHERE `id` = '.$oid;
        $result = db_query($query);
        if (mysql_num_rows($result))
        {
            return mysql_fetch_array($result, MYSQL_ASSOC);
        }

        return false;
    }

    public static function getUserDataByID($uid)
    {
        $result = db_query('SELECT * FROM `sw_users` WHERE `id` = '.$uid);
        if (mysql_num_rows($result))
        {
            return mysql_fetch_array($result, MYSQL_ASSOC);
        }

        return false;
    }

    public function clearDoubleSpace($str)
    {
        return str_replace('  ', ' ', $str);
    }

    public function storeMailToFile($mail_data)
    {
        $working_folder = dirname(__FILE__).'/tmp/spool/';
        $filename = $working_folder.$this->stringURLSafe($mail_data['subject'].'-'.date('Y-m-d H:i:s')).'.html';

        $debug_body  = '<!DOCTYPE html><html lang="ru-ru" ><head><meta http-equiv="content-language" content="ru-ru" />';
        $debug_body .= '<meta http-equiv="content-type" content="text/html; charset=windows-1251" />';
        $debug_body .= '</head><body>';
        $debug_body .= '<fieldset><legend>���������:</legend><br/>'.$mail_data['headers'].'</fieldset>';
        $debug_body .= $mail_data['body'];
        $debug_body .= '</body></html>';

        if (!is_dir($working_folder))
        {
            return false;
        }

        if (is_writable($working_folder))
        {
            $resource = fopen($filename, 'wb');
            if ($resource)
            {
                fwrite($resource, $debug_body);
                fclose($resource);
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public static function stringURLSafe($string)
    {
        $str = str_replace('-', ' ', $string);
        $str = trim(strtolower($str));
        $str = preg_replace('/(\s|[^A-Za-z0-9\-])+/', '-', $str);
        $str = trim($str, '-');

        return $str;
    }

}
