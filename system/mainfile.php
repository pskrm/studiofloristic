<?

if (!function_exists('http_response_code')) {

    function http_response_code($newcode = NULL) {
        static $code = 200;
        if ($newcode !== NULL) {
            header('X-PHP-Response-Code: ' . $newcode, true, $newcode);
            if (!headers_sent()) {
                $code = $newcode;
            }
        }
        return $code;
    }

}

function HeaderPage($page = '/', $code = 302) {
    global $_GLOBALS;
    if ($page == _PAGE_ERROR404) {
        if (function_exists('http_response_code')) {
            ob_end_clean();
            ob_start();
            http_response_code(404);
            include 'system/vars_inc.php';

            $html                   = query_new('SELECT `sid`, `title`, `text`, `html_title`, `keywords`, `description` FROM `sw_pages` WHERE `link` = \'error404\' AND `active` = 1', 1);
            $_GLOBALS['text']       = ($html !== false) ? stripcslashes($html['text']) : 'Error 404/Page not find';
            $_GLOBALS['title']      = $html['title'];
            $_GLOBALS['html_title'] = !empty($html['html_title']) ? $html['html_title'] : $html['title'] . ' - ' . $_GLOBALS['v_sitename'];

            $bg_colors = query_new('SELECT `bg_style`, `id`, `class` FROM `sw_colors` WHERE `active`=1 AND `bg_style`!=""', 0, 'id');

            include_once('system/templates.php');
            ob_end_flush();
            exit();
        }
    } else {
        ob_end_clean();
        ob_start();
        $host = stripos($page, $_GLOBALS['abs_url_host'], 0) !== false ? '' : $_GLOBALS['abs_url_host'];
        header('Location: ' . $host . $page, TRUE, $code);
        exit();
    }
}

function PageNavigator($count, $page_num, $per_page = 12, $pach = false, $first_page = false, $num = 0) {
    $pages = ceil($count/$per_page);
    $text_pages = array();
    for ($i=1;$i<=$pages;$i++){
        $link = ($i == 1) ? $first_page : $pach.$i;
        $style = '';
        $r_b = ($pages-$page_num)>3?3:$pages-$page_num;
        $r_b_add = (4-$page_num)<0?0:4-$page_num;
        if ($i>($page_num+$r_b+$r_b_add) || $i<($page_num+$r_b-6)){
            $style = 'display:none;';
        }
        $text_pages[] = '<li style="'.$style.'">'.($i==$page_num? '<span>'.$i.'</span>':'<a rel="nofollow" href="'.$link.'">'.$i.'</a>').'</li>';
    }
    $slider_left = 0;
    $slider_step = ceil(182/$pages);
    if ($page_num>3 && ($pages-$page_num)>3){
        $slider_left = ($page_num-4)*$slider_step;
    }
    if (($pages-$page_num)<4){
        $slider_left = ($pages-7)*$slider_step;
    }
    $pages_class = (count($text_pages)>7)? ' slider_run ':'';
    $slider_style = (($slider_step*7)>26)? ' width: '.($slider_step*7).'px;': 'width:26px;' ;
    $slider_style .= ' left:'.$slider_left.'px;';
    $text_pages_scroller = '<div class="slider"><span style="'.$slider_style.'"></span></div>';
    $prev_link = $pach.($page_num - 1).(!empty($num) ? '.html' : null);
    $text = '<div class="pages_nav  '.$pages_class.'">'
        . '<div class="prev_page">'.($page_num>1 ? '<a rel="nofollow" href="'.($page_num==2?$first_page:$prev_link).'">&larr; �����</a>' : '<span>&larr; �����</span>').'</div>'
        . '<div class="pages_sell">'
        . '<div class="pages"><ul>'.implode($text_pages).'</ul></div>'.(count($text_pages)>7? $text_pages_scroller :'')
        .'</div>'
        . '<div class="next_page">'.($page_num<$pages ? '<a rel="nofollow" href="'.$pach.($page_num + 1).(!empty($num) ? '.html' : null).'">����� &rarr;</a>' : '<span>����� &rarr;</span>').'</div>'
        . '</div>';
    $text_count_pages = '<div class="count_pages"><span>����� '.$pages.' �������';
    if (($pages>1 && $pages<5 ) || ($pages>20 && ($pages%10)>1 && ($pages%10)<5)){
        $text_count_pages .= '�';
    }elseif($pages>20 && ($pages%10)==1){
        $text_count_pages .= '�';
    }
    $text_count_pages .= '</span></div>';
    $text_see_all = '';
//    $text_see_all = '<div class="see_all"><a href="'.$first_page.'?pg=all">�������� ���</a></div>';

    return '<div class="pagging">'.$text.$text_count_pages.$text_see_all.'</div>';
}
function htmlNavigator($count, $page_num, $per_page = 12, $pach = false, $first_page = false, $html = true) {
    $pages = ceil($count/$per_page);
    $text_pages = array();
    for ($i=1;$i<=$pages;$i++){
        $style = '';
        $r_b = ($pages-$page_num)>3?3:$pages-$page_num;
        $r_b_add = (4-$page_num)<0?0:4-$page_num;
        if ($i>($page_num+$r_b+$r_b_add) || $i<($page_num+$r_b-6)){
            $style = 'display:none;';
        }
        $pg = ($html===true)?'pg':'&pg=';
        $pg_end = ($html===true)?'.html':'';
        $text_pages[] = '<li style="'.$style.'">'.($i==$page_num? '<span>'.$i.'</span>':'<a rel="nofollow" href="'.($i==1?$first_page:$pach.$pg.$i.$pg_end).'">'.$i.'</a>').'</li>';
    }
    $slider_left = 0;
    $slider_step = ceil(182/$pages);
    if ($page_num>3 && ($pages-$page_num)>3){
        $slider_left = ($page_num-4)*$slider_step;
    }
    if (($pages-$page_num)<4){
        $slider_left = ($pages-7)*$slider_step;
    }
    $pages_class = (count($text_pages)>7)? ' slider_run ':'';
    $slider_style = (($slider_step*7)>26)? ' width: '.($slider_step*7).'px;': 'width:26px;' ;
    $slider_style .= ' left:'.$slider_left.'px;';
    $text_pages_scroller = '<div class="slider"><span style="'.$slider_style.'"></span></div>';
    $text = '<div class="pages_nav  '.$pages_class.'">'
        . '<div class="prev_page">'.($page_num>1 ? '<a rel="nofollow" href="'.($page_num==2?$first_page:$pach.$pg.($page_num-1).$pg_end).'">&larr; �����</a>' : '<span>&larr; �����</span>').'</div>'
        . '<div class="pages_sell">'
        . '<div class="pages"><ul>'.implode($text_pages).'</ul></div>'.(count($text_pages)>7? $text_pages_scroller :'')
        .'</div>'
        . '<div class="next_page">'.($page_num<$pages ? '<a rel="nofollow" href="'.$pach.$pg.($page_num+1).$pg_end.'">����� &rarr;</a>' : '<span>����� &rarr;</span>').'</div>'
        . '</div>';
    $text_count_pages = '<div class="count_pages"><span>����� '.$pages.' �������';
    if (($pages>1 && $pages<5 ) || ($pages>20 && ($pages%10)>1 && ($pages%10)<5)){
        $text_count_pages .= '�';
    }elseif($pages>20 && ($pages%10)==1){
        $text_count_pages .= '�';
    }
    $text_count_pages .= '</span></div>';
//    if ($pg=='pg'){
//        $all = '?pg=all';
//    }else{
//        $all = '&pg=all';
//    }
//    $text_see_all = '<div class="see_all"><a href="'.$first_page.$all.'">�������� ��� ������</a></div>';
    $text_see_all = '';

    return '<div class="pagging">'.$text.$text_count_pages.$text_see_all.'</div>';
}
function FolderDelete($folderPath) {
    if (is_dir($folderPath)) {
        foreach (scandir($folderPath) as $value) {
            if ($value != "." && $value != "..") {
                $value = $folderPath . "/" . $value;
                if (is_dir($value))	{
                    FolderDelete($value);
                } elseif (is_file($value)) {
                    @unlink($value);
                }
            }
        }
        return rmdir($folderPath);
    } else {
        return FALSE;
    }
}
function CurrentPach($module, $id = 0) {
    $module = strtolower($module);
    $module_name = dbone("`title`", "`name` = ".quote($module), "pages");
    if ($id != 0) {
        $sid = $id;
        $bufer_array = array();
        while ($sid != 0) {
            $reurse = db("`id`, `sid`, `title`", "`id` = ".quote($sid), $module);
            $row = mysql_fetch_array($reurse);
            $sid = $row['sid'];
            $id != $row['id'] ? $bufer_array[] = '<a href="/'.$module.'/view/'.$row['id'].'">'.$row['title'].'</a>' : $bufer_array[] = $row['title'];
        }
        $bufer_array[] = '<a href="/'.$module.'">'.$module_name.'</a>';
    } else {
        $bufer_array[] = $module_name;
    }
    $bufer_array[] = '<a href="/" class="index">�������</a>';
    krsort($bufer_array);
    return implode('&nbsp/&nbsp;', $bufer_array);
}
function prepare_string($str) {
    $str = preg_replace("% +%", " ", $str);
    $str = trim($str);
    $str = str_replace("'", "", $str);
    $str = str_replace("\"", "&quot;", $str);
    return $str;
}
function PageData($link,$ret=false) {
    $r = query_new('SELECT `id`, `sid`, `title`, `text`, `html_title`, `keywords`, `description` FROM `sw_pages` WHERE `active` = 1 AND `link` = '.quote($link),1);
    return ($r===false && $ret===false) ? HeaderPage(_PAGE_ERROR404) : $r;
}
function unhtmlentities($string) {
    $trans_tbl = get_html_translation_table(HTML_ENTITIES);
    $trans_tbl = array_flip($trans_tbl);
    return strtr($string, $trans_tbl);
}
function generate_password($number) {
    $arr = array(
        'a','b','c','d','e','f',
        'g','h','i','j','k','l',
        'm','n','o','p','r','s',
        't','u','v','x','y','z',
        'A','B','C','D','E','F',
        'G','H','I','J','K','L',
        'M','N','O','P','R','S',
        'T','U','V','X','Y','Z',
        '1','2','3','4','5','6',
        '7','8','9','0'
    );
    $pass = "";
    for($i = 0; $i < $number; $i++) {
        $index = rand(0, count($arr) - 1);
        $pass .= $arr[$index];
    }
    return $pass;
}
function delivery_tabl($delivery=array(),$def_del=array(),$tab_id='',$hide=0){
    global $cur;
    $html = '';
    $style = ($hide==0)?'':'style="display: none;"';
    if (count($delivery) != 0 && (intval($def_del['min_city'])!=0 || intval($def_del['min_mkad'])!=0)) {
        $html.= '<div id="'.$tab_id.'-tab" class="sw-tabs" '.$style.'><table width="100%" border="0" cellspacing="0" cellpadding="0" class="data">';
        $html .= '<tr>'
            . '<td class="time_head" rowspan="2">�����</td>'
            . '<td class="city_head" colspan="2">� �������� ����</td>'
            . '<td class="mkad_head" colspan="2">�� ����</td>'
            . '</tr>'
            . '<tr>'
            . '<td class="city_head_do">�� '.ceil($def_del['min_city']/$cur['value']).$cur['reduction'].'</td>'
            . '<td class="city_head_ot">�� '.ceil($def_del['min_city']/$cur['value']).$cur['reduction'].'</td>'
            . '<td class="mkad_head_do">�� '.ceil($def_del['min_mkad']/$cur['value']).$cur['reduction'].'</td>'
            . '<td class="mkad_head_ot">�� '.ceil($def_del['min_mkad']/$cur['value']).$cur['reduction'].'</td>'
            . '</tr>';
        foreach ($delivery as $d_key=>$d_row){
            $td_time = (($d_key%2)==0)?'td_time_white':'td_time_dark';
            $td_c_do = (($d_key%2)==0)?'td_c_do_white':'td_c_do_dark';
            $td_c_ot = (($d_key%2)==1)?'td_c_ot_white':'td_c_ot_dark';
            $td_m_do = (($d_key%2)==0)?'td_m_do_white':'td_m_do_dark';
            $td_m_ot = (($d_key%2)==1)?'td_m_ot_white':'td_m_ot_dark';
            $html .= '<tr>'
                . '<td class="'.$td_time.'">'.$d_row['title'].'</td>'
                . '<td class="'.$td_c_do.'">'.ceil($d_row['center']/$cur['value']).$cur['reduction'].'</td>'
                . '<td class="'.$td_c_ot.'">'.(($d_row['min_city']==1)? '<span>���������</span>' :ceil($d_row['center']/$cur['value']).$cur['reduction']).'</td>'
                . '<td class="'.$td_m_do.'">'.ceil($d_row['okrayna']/$cur['value']).$cur['reduction'].'</td>'
                . '<td class="'.$td_m_ot.'">'.(($d_row['min_mkad']==1)? '<span>���������</span>' :ceil($d_row['okrayna']/$cur['value']).$cur['reduction']).'</td>'
                .'</tr>';
        }
        $html.= '</table></div>';
    }
    return $html;
}

function delivery_tabl_simple($delivery=array(),$def_del=array(),$tab_id='',$hide=0){
    global $cur;
    $html = '';
    $style = ($hide==0)?'':'style="display: none;"';
    if (count($delivery) != 0 && (intval($def_del['min_city'])!=0 || intval($def_del['min_mkad'])!=0)) {
        $html.= '<div id="'.$tab_id.'-tab" class="sw-tabs" '.$style.'><table class="delivery-table"><colgroup><col></col><col></col><col></col><col></col><col></col><col></col></colgroup>';
        $html .= '<tbody><tr>'
            . '<th rowspan="2">�����</th>'
            . '<th colspan="2">� �������� ����</th>'
            . '<th colspan="2">�� ���� (�� 8 ��)</th>'
            . '<th rowspan="2" class="no-mobile">�����������</th>'
            . '</tr>'
            . '<tr>'
            . '<th>�� '.ceil($def_del['min_city']/$cur['value']).$cur['reduction'].'</th>'
            . '<th>�� '.ceil($def_del['min_city']/$cur['value']).$cur['reduction'].'</th>'
            . '<th>�� '.ceil($def_del['min_mkad']/$cur['value']).$cur['reduction'].'</th>'
            . '<th>�� '.ceil($def_del['min_mkad']/$cur['value']).$cur['reduction'].'</th>'
            . '</tr>';
        foreach ($delivery as $d_key=>$d_row){
            if ($d_key == 0) {
                $podmosk = '<td rowspan="8" class="no-mobile">+ 35 �./�� ���� � ������ �� ���� �� 8 ��</td>';
            }else{
                $podmosk = '';
            }
            $title = explode("-", $d_row['title']);
            $tparts1 = explode(":", $title[0]);
            $tparts2 = explode(":", $title[1]);
            $html .= '<tr>'
                . '<td style="width: 16%;">'. $tparts1[0]. '<sup>'.$tparts1[1].'</sup>&mdash;' . $tparts2[0] . '<sup>'.$tparts2[1].'</sup>' .'</td>'
                . '<td style="width: 16%;">'.ceil($d_row['center']/$cur['value']).$cur['reduction'].'</td>'
                . '<td style="width: 16%;">'.(($d_row['min_city']==1)? '<span class="color-red"><strong>���������</strong></span>' :ceil($d_row['center']/$cur['value']).$cur['reduction']).'</td>'
                . '<td style="width: 16%;">'.ceil($d_row['okrayna']/$cur['value']).$cur['reduction'].'</td>'
                . '<td style="width: 16%;">'.(($d_row['min_mkad']==1)? '<span class="color-red"><strong>���������</strong></span>' :ceil($d_row['okrayna']/$cur['value']).$cur['reduction']).'</td>'
                . $podmosk .'</tr>';
        }
        $html .= '<tr><td colspan="5" class="for-mobile-td">�����������: + 35 �./�� ���� � ������ �� ���� �� 8 ��</td></tr>';
        $html.= '</tbody></table></div>';
    }
    return $html;
}

function geturlpage($url='www.google.com'){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0');
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $html = curl_exec($ch);
    curl_close($ch);
    return $html;
}
function url_resp_code($url=''){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_AUTOREFERER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_NOBODY, false);
    $res = curl_exec($ch);
    $info = curl_getinfo($ch,CURLINFO_HTTP_CODE);
    curl_close($ch);
    return intval(strval($info));
}
function arrsum($arr = array(),$ret = false){
    global $cur;
    foreach ($arr as $key=>$row){
        $arr[$key] = ceil($row/$cur['value']);
    }
    return $ret===false?array_sum($arr):$arr;
}

function strItems($n)
{
    $n = (int)$n;
    $text = $n%10==1&&$n%100!=11?_COUNT_TEXT:($n%10>=2&&$n%10<=4&&($n%100<10||$n%100>=20)?_COUNT_TEXT_1:_COUNT_TEXT_2);
    return $n.' '.$text;
}

function strItems_new($n)
{
    return strItems($n);
}

function get_currency($currency=false){
    $currency = empty($currency)?filter(trim($_COOKIE['cur']), 'nohtml'):(string)$currency;
    $cur = query_new('SELECT * FROM `sw_currency` WHERE `name` = '.quote($currency),1);
    if (empty($cur)) {
        $cur = query_new('SELECT * FROM `sw_currency` WHERE `name` = "rub"',1);
        setcookie('cur', 'rub' , null,'/',_CROSS_DOMAIN);
    }
    $cur['spanSymb'] = '<span class="cur-symb">'.$cur['reduction'].'</span>';
    switch ($cur['name']){
        case 'euro': $cur['ISO']='EUR'; break;
        case 'usd': $cur['ISO']='USD'; break;
        default: $cur['ISO']='RUB';
    }
    return $cur;
}
function set_currency($name=''){
    global  $cur;
    switch ($name) {
        case 'euro': $value = 'euro'; break;
        case 'usd': $value = 'usd'; break;
        default: $value = 'rub';
    }
    $c = query_new('SELECT * FROM `sw_currency` WHERE `name` = '.quote($value),1);
    if ($c !== false){
        setcookie('cur', $value, null, '/',_CROSS_DOMAIN);
        $cur = $c;
        if (_IS_USER) {
            query_new('UPDATE `sw_users` SET `cur` = '.quote($value).' WHERE `id` = '.quote($_SESSION['uid']));
        }
    }
    return $c;
}

function convert_currency($money, $from_currency, $to_currency){
    $from_currency = mb_strtoupper($from_currency);
    $to_currency = mb_strtoupper($to_currency);
    $c = query_new('SELECT `name`, `value` FROM `sw_currency`');
    $avalibleCur = array();
    foreach ($c as $num => $cc){
        $avalibleCur[mb_strtoupper( str_replace('euro', 'eur', $cc['name']) )] = $cc['value'];
    }
    if ((in_array($from_currency, array_keys($avalibleCur))) && (in_array($to_currency, array_keys($avalibleCur))) ){
        // ������� ��� � �����
        if ($from_currency !== 'RUB'){
            $tmpMoney = ceil( $money * $avalibleCur[$from_currency]);
            if ($to_currency != 'RUB'){
                return ceil($tmpMoney / $avalibleCur[$to_currency])  ;
            }else{
                return $tmpMoney;
            }
        }else{
            return ceil($money / $avalibleCur[$to_currency])  ;
        }
    }else{
        return $money;
    }
}

/***********************\
] GLOB BASKET FUNCTIONS [
\***********************/
function basket_breadcrumbs($op=false){
    $arr = array(
        1=>array('class'=>'basket_cart','text'=>'��� �����'),
        2=>array('class'=>'basket_issue','text'=>'����������'),
        3=>array('class'=>'basket_payment','text'=>'�������������'),
        4=>array('class'=>'basket_done','text'=>'������!')
    );
    switch ($op){
        case 'issue': $arr[2]['class'] .= ' active_page'; break;
        case 'payment': $arr[3]['class'] .= ' active_page'; break;
        case 'done': $arr[4]['class'] .= ' active_page'; break;
        default : $arr[1]['class'] .= ' active_page';
    }
    foreach ($arr as $key=>$row){
        $arr[$key] = '<div class="'.$row['class'].'"><span class="numb">'.$key.'</span><span class="icon"></span><span class="text">'.$row['text'].'</span></div>';
    }
    return '<div class="basket_breadcrumbs">'.implode('<div class="no_bg"></div>',$arr).'</div>';
}
function section_full_list($id=0){
    $ret = array();
    $sect = query_new('SELECT * FROM sw_section WHERE id='.$id,1);
    while ($sect!==false){
        $ret[$sect['id']] = $sect;
        $sect = query_new('SELECT * FROM sw_section WHERE id='.$sect['sid'],1);
    }
    return $ret;
}
function cookie_fields($u=array()){
    global $COOKIE_REPLACEMENT;
    $keys = query_new("SHOW COLUMNS FROM `sw_basket_order`");
    if (is_array($keys)){
        foreach ($keys as $field){
            if ((!isset($u[$field['Field']]) || $COOKIE_REPLACEMENT===true) && isset($_COOKIE['issue_'.$field['Field']])){
                $u[$field['Field']] = in1251($_COOKIE['issue_'.$field['Field']]);
            }
        }
    }
    $u['delivery'] = !empty($u['delivery']) ? $u['delivery']: 1;
    $courier_fields = array('cur-date', 'cur-adres', 'cur-time');
    foreach($courier_fields as $cf)
    {
        if (isset($_COOKIE['issue_'.$cf]))
        {
            $u[$cf] = in1251($_COOKIE['issue_'.$cf]);
        }
    }
    return $u;
}
function cookie_fields_clear($mask=false,$path='/'){
    if ($mask!==false && $mask!='' && is_string($mask)){
        foreach (array_keys($_COOKIE) as $val){
            if (stripos($val,$mask,0)===0){
                setcookie($val, null, 0, $path,_CROSS_DOMAIN);
            }
        }
        return true;
    }
    return false;
}
function del_datetime($u = array()){
    global $COOKIE_REPLACEMENT;
    $datetime = array();
    $dt_arr = array('date','time','interval');

    if (isset($u['datetime']) && $u['datetime']!=''){
        if (stripos($u['datetime'],' ')===false){
            $datetime['date'] = date('d.m.Y', $u['datetime']);
            $datetime['time'] = ($u['timeship']!='')?'': date('H:i', $u['datetime']);
            if ($u['timeship']==''){
                $interval = query_new("SELECT * FROM sw_delivery sd WHERE sd.start <= '".$datetime['time']."' AND sd.end > '".$datetime['time']."'",1);
                $datetime['interval'] = $interval['id'];
            }else{
                $interval = query_new("SELECT * FROM sw_delivery sd WHERE title REGEXP '".$u['timeship']."'",1);
                $datetime['interval'] = $interval['id'];
            }
        }else{
            $date_arr = explode(' ',str_replace('���������: ', '', $u['datetime']));
            $datetime['date'] = str_replace('-','.',$date_arr[0]);
            if (stripos($datetime['date'],'.')>3){
                $date = explode('.', $datetime['date']);
                $date = array_reverse($date);
                $datetime['date'] = implode('.', $date);
            }
            unset($date_arr[0]);
            $datetime['time'] = (count($date_arr)>1)?'':$date_arr[1];
            if ($datetime['time']==''){
                $interval = query_new("SELECT * FROM sw_delivery sd WHERE title REGEXP '".implode(' ',$date_arr)."'",1);
                $datetime['interval'] = $interval['id'];
            }else{
                $interval = query_new("SELECT * FROM sw_delivery sd WHERE sd.start <= '".$datetime['time']."' AND sd.end > '".$datetime['time']."'",1);
                $datetime['interval'] = $interval['id'];
            }
        }
    }
    foreach ($dt_arr as $dt_item){
        if (isset($_COOKIE['issue_'.$dt_item]) && (!isset($datetime[$dt_item]) || $COOKIE_REPLACEMENT===true)){
            $datetime[$dt_item] = $_COOKIE['issue_'.$dt_item];
        }
    }
    $datetime['interval'] = (empty($datetime['interval']))? 2 : $datetime['interval'];
    $datetime['diff_days'] = (!empty($datetime['date']))? floor(strtotime($datetime['date'])/86400)-floor(time()/86400) :0;

    return $datetime;
}
function timeto_interval($time){
    $interval = query_new("SELECT * FROM sw_delivery sd WHERE sd.start <= '".$time."' AND sd.end > '".$time."'",1);
    if ($interval===false){
        $interval = query_new("SELECT * FROM sw_delivery sd WHERE sd.title REGEXP '".$time."'",1);
    }
    if (isset($interval['id'])){
        $interval = $interval['id'];
    }
    return $interval;
}
function deliveres($price=false,$interval = 0,$date = false){
    global $cur;

    $interval_sql = (intval($interval) > 0) ? 'AND id = ' . quote($interval) : '';
    $date_sql     = ($date === false) ? '' : "WHERE STR_TO_DATE(" . quote($date) . ",'%d.%m.%Y') BETWEEN start AND end";
    $del_opt      = query_new("SELECT * FROM sw_delivery_holiday_options " . $date_sql, 1);
    if (!empty($del_opt)) {
        $delivery = query_new('SELECT * FROM `sw_delivery_holiday` WHERE `active` = "1" ' . $interval_sql . ' ORDER BY `start` ASC', intval($interval));
        $del_type = 'holl';
    } else {
        $delivery = query_new('SELECT * FROM `sw_delivery` WHERE `active` = "1" ' . $interval_sql . ' ORDER BY `start` ASC', intval($interval));
        $del_opt  = query_new("SELECT * FROM `sw_delivery_ships`", 1);
        $del_type = 'normal';
    }
    if ($delivery === false || $del_opt === false) {
        return false;
    }
    $del_opt['min_price'] = ceil($del_opt['min_price'] / $cur['value']);
    $del_opt['min_city']  = ceil($del_opt['min_city'] / $cur['value']);
    $del_opt['min_mkad']  = ceil($del_opt['min_mkad'] / $cur['value']);
    if ($price === false) {
        return array('dels' => $delivery, 'del_opt' => $del_opt, 'del_type' => $del_type);
    }
    if (intval($price) >= $del_opt['min_price']) {
        $center = (!empty($delivery['min_city']) && $price >= $del_opt['min_city']) ? 0 : ceil($delivery['center'] / $cur['value']);
        $mkad   = (!empty($delivery['min_mkad']) && $price >= $del_opt['min_mkad']) ? 0 : ceil($delivery['okrayna'] / $cur['value']);
        $_res = array(
            'center' => $center,
            'mkad' => $mkad,
            'mkad_over' => (!empty($delivery['min_mkad']) && $price >= $del_opt['min_mkad']) ? 0 : ceil($delivery['okrayna'] / $cur['value']),
            'del_type' => $del_type
        );
        return $_res;
    } else {
        return array('center' => 'disabled', 'mkad' => 'disabled', 'mkad_over' => 'disabled', 'del_type' => $del_type);
    }
}
function holl_markup($date=false){
    $holiday = query_new("SELECT * FROM sw_holiday WHERE `active`= '1'",1);
    $todayDate = strtotime($date);
    if ($todayDate!==false && $date!==false && $holiday!==false){
        $start = strtotime($holiday['start']);
        $end = strtotime($holiday['end']);
        if ($start <= $todayDate && $todayDate <= $end){
            return $holiday;
        }
    }
    return ($date===false)? $holiday : false;
}
function calc_prods_price($products=array(),$discount=0,$markup=0,$ret_summ = false){
    global $cur;
    $summ = array(
        'clear'=>0,
        'markup'=>0,
        'discount'=>0,
        'mark_disc'=>0,
    );
    $gifts = array();
    $count=0;
    $noAkziiTovars = array();
    $noAkziiTovarsHoll = array();
    $AkziiTovars = array();
    $AkziiTovarsHoll = array();
    foreach($products as $key=>$tovar){ // ���� �� ���� ���������, �� �� ����������!!! ������ �� ��������� � �������
        if ($tovar['type'] != 'gift'){
            $d = (empty($tovar['share']) && $tovar['type']!='my-order')? 1 : 0;
            $m = ($tovar['type']=='my-order')?0:$markup;
            $count += $tovar['count'];
            $tovar['price'] = ceil($tovar['price']/$cur['value']);
            $tovar['amount'] = ceil($tovar['amount']/$cur['value']);
            $summ['clear'] += $tovar['count']*$tovar['amount'];
            if ($d){
                // ���� ����� �� �� ����� �������� ����� ������ � ������
                $noAkziiTovars[] = round( $tovar['count']*$tovar['amount']); // ������ ��� ���������� float �� ������
                $noAkziiTovarsHoll[]= round($tovar['amount']*(1+$m/100));
            }else{
                $AkziiTovars[] = round( $tovar['count']*$tovar['amount']); // � ��� ����� ������ �� �����, �� ��� ��� ������
                $AkziiTovarsHoll[]= round($tovar['amount']*(1+$m/100));
            }

            $markup_amount = ceil($tovar['amount']*(1+$m/100));
            $discount_amount = ceil($tovar['amount']*(1-$discount*$d/100));
            $mark_disc = ceil($markup_amount*(1-$discount*$d/100));
            $summ['discount'] += $tovar['count']*$discount_amount;
            $summ['markup'] += $tovar['count']*$markup_amount;
            $summ['mark_disc'] += $tovar['count']*$mark_disc;
            $tovar['markup_amount'] = $markup_amount;
            $tovar['discount_amount'] = $discount_amount;
            $tovar['mark_disc'] = $mark_disc;
            $tovar['markup'] = $m;
            $tovar['disc'] = $d;
            $products[$key] = $tovar;
        } else{
            unset($products[$key]);
            $gifts[$key] = $tovar;
        }
    }
    // ������ ��������� ������ �� ����������� ��� ������� �� �� �����
    $fullSummNoAkzii = ((round(array_sum($noAkziiTovars))) - (round(round(array_sum($noAkziiTovars)) * $discount) / 100)) + round(array_sum($AkziiTovars)); // �������� ������
    $fullSummNoAkziiHoll = ((round(array_sum($noAkziiTovarsHoll))) - (round(round(array_sum($noAkziiTovarsHoll)) * $discount) / 100)) + round(array_sum($AkziiTovarsHoll)); // �������� ������
    $summ['discount'] =  round($fullSummNoAkzii, 0)  ;
    $summ['mark_disc']  = round($fullSummNoAkziiHoll, 0) ;
    $_SESSION['real_prices'] = array('discount' => $summ['discount'], 'mark_discount' => $summ['mark_disc']  );
    if ($ret_summ!==false){
        if (isset($summ[$ret_summ])){
            return $summ[$ret_summ];
        }else{
            return false;
        }
    }
    return array(
        'products'=>$products,
        'gifts'=>$gifts,
        'summ'=>$summ,
        'count'=>$count
    );
}
function dis_in_one($discounts = array()){
    global $_GLOBALS;
    if (is_array($discounts)){
        $discount = array_sum($discounts);
    }else{
        $discount = intval($discounts);
    }
    if ($discount > $_GLOBALS['v_max-discount']) {
        $discount = $_GLOBALS['v_max-discount'];
    }
    return $discount;
}

function basket_info($discounts = array(),$session='',$opt=array(), $products = array())
{
    global $_GLOBALS,$cur;

    $session   = empty($session) ? session_id() : $session;
    $prods_sql = (empty($opt['sql_products'])) ? 'SELECT b.*, s.`url`, c.`image`,c.`share` FROM `sw_basket` b LEFT OUTER JOIN `sw_catalog` c ON b.`cid`=c.`id` '
        . 'LEFT OUTER JOIN `sw_catalog_section` s ON s.`cid`=b.`cid` AND s.`first`=1 '
        . 'WHERE b.`session`=' . quote($session) . '  ORDER BY c.`views`,b.`id`' : $opt['sql_products'];
    // . 'WHERE b.`session`=' . quote($session) . ' AND b.`type`!=' . quote("gift") . '  ORDER BY c.`views`,b.`id`' : $opt['sql_products']; // old
    if (count($products) <= 0)
    {
        $products  = query_new($prods_sql);
    }

    $us        = empty($_SESSION['user']) ? false : $_SESSION['user'];
    $holiday   = holl_markup();
    $markup    = ($holiday !== false) ? intval($holiday['center']) : 0;
    // DISCOUNTS
    if (is_array($discounts))
    {
        $discounts['user'] = (_IS_USER && $us !== false) ? $us['discount'] : 0;
        if ((isset($_SESSION['cupon'])) && (intval($_SESSION['cupon']) > 0) && (intval($_SESSION['cupon']) > $discounts['user']))
        {
            $discounts['user'] = $_SESSION['cupon'];
        }
    }

    // CALCULATE BASE PRICE OF BASKET ITEMS
    $calc_totp                 = calc_prods_price($products, dis_in_one($discounts), $markup);
    $summ                      = $calc_totp['summ'];
    $results                   = array('clear_summ' => $summ['clear']);
    $results['markup_summ']    = $summ['markup'];
    $results['discount_summ']  = $summ['discount'];
    $results['mark_disc_summ'] = $summ['mark_disc'];
    $results['count']          = $calc_totp['count'];
    $results['markup']         = $markup;
    $results['products']       = $calc_totp['products'];
    $results['gifts']          = $calc_totp['gifts'];
    $results['user']           = $us;
    $results['holiday']        = $holiday;

    // MORE OPTIONS
    if (isset($opt['info']) && $opt['info']=='full')
    {
        $order_sql               = (empty($opt['sql_order'])) ? 'SELECT * FROM `sw_basket_order` WHERE `session` = ' . quote($session) : $opt['sql_order'];
        $u                       = query_new($order_sql, 1);
        $results['basket_order'] = $u;

        $u = ($u === false) ? cookie_fields($us) : cookie_fields($u);
        if (!empty($opt['basket_order']) && is_array($opt['basket_order']))
        {
            foreach ($opt['basket_order'] as $bo_key => $bo_val)
            {
                $u[$bo_key] = $bo_val;
            }
        }

        $results['basket_order_cookie'] = $u;
        $datetime                       = del_datetime($u);
        $results['datetime']            = $datetime;

        $del_disc = (isset($_GLOBALS['v_del_disc']) && intval($_GLOBALS['v_del_disc']) >= 0) ? intval($_GLOBALS['v_del_disc']) : 0;
        $price    = (empty($datetime['date']) || holl_markup($datetime['date']) === false) ? $results['discount_summ'] : $results['mark_disc_summ'];
        $del_add  = deliveres($price, $datetime['interval'], $datetime['date']);
        if ($del_add === false || $del_add['center'] === 'disabled' || $del_add['mkad'] === 'disabled')
        {
            $u['delivery']                      = 2;
//          setcookie('issue_delivery', 2, 0, '/basket/', _CROSS_DOMAIN);
            $results['deliveres']['del_active'] = false;
        }
        else
        {
            $results['deliveres']['del_active'] = true;
        }

        $del_OM    = (isset($_GLOBALS['v_start_over_MKAD']) && intval($_GLOBALS['v_start_over_MKAD']) > 0) ? intval($_GLOBALS['v_start_over_MKAD']) : 1;
        /* ��������� ������� ��� �������� ����� ����� */
        if ($u['delivery'] == 5){
            $sell_dell = array(5 => 'checked');
        }else{
            $sell_dell = ($u['delivery'] > 3) ? array(4 => 'checked') : array($u['delivery'] => 'checked');
        }

        if (isset($_COOKIE['issue_mkad_pass_lenght']) && intval($_COOKIE['issue_mkad_pass_lenght']) > $del_OM) {
            $lenght = $_COOKIE['issue_mkad_pass_lenght'];
        } else {
            $lenght = ($del_OM < ($u['delivery'] - 4)) ? $u['delivery'] - 4 : $del_OM;
        }
        if ($u['delivery'] == 5){
            //$lenght = 0;
            $u['delivery'] = 5;
            $u['delivery_metro'] = $_COOKIE['metro_deliver'];
            $_SESSION['delivery_metro__'] = $_COOKIE['metro_deliver'];
        }else {
            $u['delivery'] = ($u['delivery'] > 3) ? 4 : $u['delivery'];
        }
//        if (!empty($products)){
//            setcookie('issue_delivery', $u['delivery'], 0, '/basket/', _CROSS_DOMAIN);
//        }
        if ( $u['delivery'] != 5) {
            $mkad_len_price = isset($_GLOBALS['v_m_per_km']) ? ceil(intval($_GLOBALS['v_m_per_km']) / $cur['value']) : ceil(35 / $cur['value']);
            $results['deliveres']['mkad_len_price'] = $mkad_len_price;
            $results['deliveres']['mkad_boundary'] = $del_OM;
            $results['deliveres']['mkad_lenght'] = $lenght;
            $results['deliveres']['mkad_mkad'] = ($del_add['mkad_over']) != 'disabled' ? $del_add['mkad_over'] : 0;

            if ($results['deliveres']['del_active'] === true) {
                $lenght = $lenght - $del_OM;
                $del_4 = $mkad_len_price * $lenght;
                $results['deliveres']['mkad_over_clean'] = $del_4;
                $del_add['mkad_over'] += $del_4;
            }
        }else{
            /* TODO: ��� ���� �����������, � �� �� ������� ��� ��� ������, ����������� ��� � ���� 1 �������� � �������� ���� */
            $results['deliveres']['mkad_len_price'] = 0;
            $results['deliveres']['mkad_boundary'] = 8;
            $results['deliveres']['mkad_lenght'] = 8;
            $results['deliveres']['mkad_mkad'] = 0;
        }

        $results['deliveres']['delivery_2'] = array('value' => $del_disc, 'type' => 'disc', 'check' => $sell_dell[2]);
        $results['deliveres']['delivery_1'] = array('value' => $del_add['center'], 'type' => 'add', 'check' => $sell_dell[1]);
        $results['deliveres']['delivery_3'] = array('value' => $del_add['mkad'], 'type' => 'add', 'check' => $sell_dell[3]);
        $results['deliveres']['delivery_4'] = array('value' => $del_add['mkad_over'], 'type' => 'add', 'check' => $sell_dell[4]);

        /* ������� ��� �������� 5 - �� ������� ����� */
        $results['deliveres']['delivery_5'] = array('value' =>  $del_add['center'], 'type' => 'add', 'check' => $sell_dell[5]);
        /*********************************************/

        $results['deliveres']['delivery']   = $u['delivery'];
        $results['deliveres']['del_type']   = ($del_add['del_type'] !== 'holl') ? 'normal' : 'holl';

        $results['services']['photo']       = (!empty($_GLOBALS['v_photo'])) ? ceil($_GLOBALS['v_photo'] / $cur['value']) : 0;
        $results['services']['pay_tochdos'] = (!empty($_GLOBALS['v_pay-tochdos'])) ? ceil($_GLOBALS['v_pay-tochdos'] / $cur['value']) : 0;
        $results['services']['pay_courier'] = (!empty($_GLOBALS['v_pay-curer'])) ? ceil($_GLOBALS['v_pay-curer'] / $cur['value']) : 0;

        $results['addes_prices']['photo']       = (!empty($u['foto'])) ? $results['services']['photo'] : 0;
        $results['addes_prices']['pay_tochdos'] = (!empty($datetime['time'])) ? $results['services']['pay_tochdos'] : 0;
        $results['addes_prices']['pay_courier'] = ($u['type'] == 'courier') ? $results['services']['pay_courier'] : 0;

        if ($u['type'] === 'courier' && $u['curData'] > 0 && !empty($u['curAdres']))
        {
            $results['addes_prices']['pay_courier'] = $results['services']['pay_courier'];
        }
        if (($u['delivery'] != 2) )
        {
            $results['addes_prices']['delivery'] = $results['deliveres']['delivery_' . $u['delivery']]['value'];
        }
        if (is_array($discounts))
        {
            if ($u['delivery'] == 2 )
            {
                $discounts['delivery'] = $del_disc;

            }
            if ($u['delivery'] == 5 ){
                $discounts['delivery'] = 0; // ���� ������ ��� �������� �� �����
            }


            if ($datetime['diff_days'] >= 7)
            {
                $discounts['diff_days'] = ($datetime['diff_days'] < 14) ? intval($_GLOBALS['v_discount-7-days']) : intval($_GLOBALS['v_discount-14-days']);
            }
            else
            {
                $discounts['diff_days'] = 0;
            }
        }
        $calc_totp                      = calc_prods_price($products, dis_in_one($discounts), $markup);
        $summ                           = $calc_totp['summ'];
        $results['discount_summ_full']  = $summ['discount'];
        $results['mark_disc_summ_full'] = $summ['mark_disc'];
    }
    if (is_array($discounts))
    {
        $results['discounts'] = $discounts;
    }

    return $results;
}
/***************************\
] GLOB BASKET FUNCTIONS END [
\***************************/
/**********\
] MINICART [
\**********/
function mini_cart_item($c=array(), $class="",$holiday=false){
    global $cur;

    $html='';
    if (is_array($c) && count($c)>0 && $c['type']!='gift'){
        $images = !empty($c['image']) ? explode(':', $c['image']) : array();
        $image = '/files/catalog/'.$c['cid'].'/w120_'.$images[0];
        switch ($c['type']){
            case 'my-bouquet': $image = '/i/myBouquet.gif'; break;
            case 'my-order': $image = '/i/individ-100.jpg'; break;
        }
        $holi_text = '';
        if (($holiday)!==false){
            $holiday['title'] = str_replace('|',' ', $holiday['title']);
            $holi_text = '<div class="holiday_popup_cart"><table><tr>'
                . '<td><span class="text_item gray">'.$holiday['title'].'</span></td>'
                . '<td align="right"><span id="markup_amount_'.$c['id'].'" class="tovar_price_popup_cart text_color_orange cost">'.$c['markup_amount'].'<span class="reduction">'.$cur['reduction'].'</span></span></td></tr></table></div>';
        }
        $html .= '<div id="mc_item_cont_'.$c['id'].'" class="blockWithTopLine '.$class.'">'
            . '<input type="hidden" class="mc_item_prop" id="minicart_item_'.$c['id'].'" '
            . 'data-mc_count="'.$c['count'].'" data-mc_amount="'.$c['amount'].'" '
            . 'data-mc_disc="'.(empty($c['disc'])?0:1).'" '
            . 'value="'.$c['id'].'" data-mc_cid="'.$c['cid'].'" data-mc_markup="'.$c['markup'].'">'
            . '<div class="blockImg floatLeft"><img src="'.$image.'" /></div>'
            . '<div class="blockInfo floatRight">'
            . '<div class="title"><a href="'.$c['url'].'">'.$c['title'].'</a></div>'
            . '<div class="info">'.str_replace("\n", '<br />', $c['text']).'</div>'
            . '<div class="cost">'
            . '<span id="mc_amount_text_'.$c['id'].'">'.$c['amount'].'</span><span class="reduction">'.$cur['reduction'].'</span>'
            . '<span class="count_item_'.$c['count'].'"><span class="count_val">'.$c['count'].'</span> ��. x </span>'
            . '<span class="count_item_'.$c['count'].'"> = <span class="count_amount_price">'.($c['count']*$c['amount']).'</span>'
            . '<span class="reduction">'.$cur['reduction'].'</span></span>'
            . '</div>'.$holi_text.'</div><div class="clear"></div></div>';
    }
    return $html;
}
function mini_cart($basket_info=false){
    global $cur, $_GLOBALS;

    if ($basket_info===false){
        $basket_info = basket_info();
    }
    $ppInCart = array();

    $ppInCart[] = '<div class="gotoBasket"><a href="/basket.html" class="bigButton">�������� �����</a></div>';
    foreach ($basket_info['products'] as $p_key=>$prod){
        $style = $p_key==0?'noBackground':'';
        $style='';
        $ppInCart[] = mini_cart_item($prod,$style,$basket_info['holiday']);
    }
    $_GLOBALS['in_cart'] = empty($basket_info['count'])?0:$basket_info['count'];
    $_elClass = $basket_info['count'] > 0 ? 'someInCart_1' : 'someInCart_0';
    $minicart = '<div class="minicart_text"><div class="'.$_elClass.'">'
        . '<div class="ppInCart">'
        . '<span id="nowInCart">'.strItems_new($basket_info['count']).'</span>'
        . '<div id="popupNowInCart" class="'.$_elClass.'">'. implode(PHP_EOL, $ppInCart).PHP_EOL.'</div>'
        .'</div>'.PHP_EOL. '<span> �� ����� </span><span id="itog">'.$basket_info['clear_summ'].'</span>'.$cur['reduction'].'</div>'
        . '<div class="emptyCart '.$_elClass.'">������� �����</div></div>';
    return $minicart;
}

/**************\
] MINICART END [
\**************/
/****************\
] USER FUNCTIONS [
\****************/
function login_forms(){
    return implode('', file('system/templates/login_form.tpl'));
}
function unset_user_session(){
    unset($_SESSION['user']);
    unset($_SESSION['username']);
    unset($_SESSION['uid']);
    unset($_SESSION['discount']);
    setcookie('cur', null, null, '/',_CROSS_DOMAIN);
    setcookie('member',null,null,'/',_CROSS_DOMAIN);
    setcookie('soc_name',null,null,'/',_CROSS_DOMAIN);
    setcookie('soc_id',null,null,'/',_CROSS_DOMAIN);
}
function set_user_session($u=array()){
    if (!empty($u['cur'])) {
        setcookie('cur', $u['cur'], null, '/',_CROSS_DOMAIN);
    }
    $_SESSION['discount'] = $u['discount'];
    $_SESSION['user'] = $u;
    $_SESSION['uid'] = $u['id'];
    $_SESSION['username'] = $u['username'];
    $_SESSION['user']['login'] = (empty($u['email']))? $u['id'] : $u['email'];
}


function set_user_member($u=array(),$time=0,$more_opt=false){
    if (empty($u['activation'])){
        $activation = md5(session_id().time());
        query_new('UPDATE sw_users SET `activation`='. quote($activation).' WHERE id='.$u['id']);
    }else{
        $activation = $u['activation'];
    }
    setcookie('member', $activation , $time,'/',_CROSS_DOMAIN);
    $_COOKIE['member'] = $activation;
    if (is_array($more_opt)){
        foreach ($more_opt as $key=>$val){
            setcookie($key, $val , $time,'/',_CROSS_DOMAIN);
            $_COOKIE[$key] = $val;
        }
    }
}

function online() {
    if (is_user()) {
        define('_IS_USER', true);
    } else {
        define('_IS_USER', false);
    }
    if (_USER_SESSION_RECORD!==false){
        $ctime = time();
        $user = (_IS_USER) ? $_SESSION['user']['login'] : getRealIpAddr();
        $uid = (_IS_USER) ? $_SESSION['user']['id'] : 0;
        if (dbone('COUNT(`uid`)', '`name` = '.quote($user).' AND `PHP_SESSID` = '.quote(session_id()), 'session') != 0) {
            db_query('UPDATE `sw_session` SET `time` = '.quote($ctime).', `host_addr` = '.quote($_SERVER['REMOTE_ADDR']).' WHERE `name` = '.quote($user).' AND `PHP_SESSID` = '.quote(session_id()));
        } else {
            db_query('INSERT INTO `sw_session` (`uid`, `name`, `time`, `time_start`, `host_addr`, `PHP_SESSID`) VALUES ('.quote($uid).', '.quote($user).', '.quote($ctime).', '.quote($ctime).', '.quote($_SERVER['REMOTE_ADDR']).', '.quote(session_id()).')');
        }
    }
}
function is_user() {
    if (!empty($_COOKIE['member']) && empty($_SESSION['user'])){
        $u = query_new('SELECT * FROM sw_users WHERE activation = '.quote($_COOKIE['member']),1);
        set_user_session($u);
    }
    if (!empty($_SESSION['user']) && $_COOKIE['member'] === $_SESSION['user']['activation']) {
        return true;
    }
    return false;
}
/*******************\
] USER FUNCTIONS END[
\*******************/
/******************\
] HOVER POPUP INFO [
\******************/
function hover_popup($id='',$content='',$opt=array('class'=>array('how_to_fill','text_block'),'cont_class'=>'tb_hover_popup')){
    if (isset($opt['class'])){
        $class = (is_array($opt['class']))? implode(' ', $opt['class']):strval($opt['class']);
    }else{
        $class = '';
    }
    $class = str_replace('"', '', str_replace("'", '', $class));
    if (isset($opt['cont_class'])){
        $cont_class = (is_array($opt['cont_class']))? implode(' ', $opt['cont_class']):strval($opt['cont_class']);
    }else{
        $cont_class = '';
    }
    $cont_class = str_replace('"', '', str_replace("'", '', $cont_class));
    $content = (is_array($content))? implode('', $content):strval($content);
    $html = '<div id="'.$id.'_pop" class="'.$class.'" onmouseover="hoverPopup.onOver(this);" onmouseout="hoverPopup.onOut(this);">'
        . '<div class="'.$cont_class.'" id="'.$id.'_pop_cont">'
        . '<div class="triagle"></div>'
        . '<div class="text_block_cont">'.$content.'</div>'
        . '</div>'
        . '</div>';
    return $html;
}
/**********************\
] HOVER POPUP INFO END [
\**********************/
function return_file($filename=''){
    if (file_exists($filename)){
        if (ob_get_level()) {
            ob_end_clean();
        }
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename($filename));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($filename));
        readfile($filename);
        exit();
    }
}
function get_curr_bg($date=false){
    $date = empty($date)?date('Y-m-d'):$date;
    $bg = query_new('(SELECT * FROM `sw_bg_changer` WHERE `active`=1 AND `date_start`<='.quote($date).' AND `date_end`>='.quote($date).' AND `date_start`!="null" AND `date_end`!="null" ORDER BY `date_start` DESC LIMIT 1)'
        . ' UNION (SELECT * FROM `sw_bg_changer` WHERE `active`=1 AND `main`=1 ORDER BY `insert` DESC LIMIT 1) LIMIT 1',1);
    return empty($bg)?'':' style="background: url(\'/files/bg_changer/'.$bg['id'].'/'.$bg['background'].'\') center top;" ';
}
function block_nofollow($html=''){
    return '<!--noindex--><!--googleoff: all-->'.$html.'<!--googleon: all--><!--/noindex-->';
}
function dirItem($c=array()){
    global $cur;
    if (!empty($c)){
        $from = '<i class="from">�� </i>';
        if (!empty($c['min_sub_price'])) {
            $c['price-old'] = '';
            $c['price'] = $c['min_sub_price'];
            $from = '';
        }
        $cost = $c['price'];
        $image = explode(':', $c['image']);
        $miniGal = count($image)>1?'data-imgs="'.$c['image'].'" data-path="/files/catalog/'.$c['id'].'/w168_" id="catItemId_'.$c['id'].'" data-id="'.$c['id'].'" ':'';
        $icons = '<div class="item-stickers">'
            . ((!empty($c['hit']))?'<div class="catalog_item_hit"></div>':'')
            . ((!empty($c['season']))?'<div class="catalog_item_bouquet"></div>':'')
            . ((!empty($c['share']))?'<div class="catalog_item_share centered_v_hover_popup shared_items text_block" tb="TYPE_hover_popup--append--MULTIPLE_shared_items" tb_cont="share_text_block"></div>':'')
            . '</div>';
        $html = '<div class="catalogItem dimage-show-cont">'
            . '<div class="title">'.$c['title'].'</div>'
            . '<a href="'.$c['url'].'" class="imgs" '.$miniGal.' >'
            . '<!--<img src="/i/loader.gif" class="loader"/>-->'
            . '<img src="/files/catalog/'.$c['id'].'/w168_'.$image[0].'" alt="'.str_replace(array('"', "'"), '', $c['title']).'" class="producImage"/>'
            . $icons.'</a>'
            . '<div class="cost">'.(!empty($c['price-old'])
                ? '<span id="'.$c['id'].'" class="newCost"><i>'.(countSizes($c['id']) > 1 ? $from : '').ceil($cost / $cur['value']).'</i><span class="reduction">'.$cur['reduction'].'</span></span><span class="oldCost"><span class="price">'.ceil($c['price-old'] / $cur['value']).'</span><span class="reduction">'.$cur['reduction'].'</span></span>'
                : '<span id="'.$c['id'].'"  class="newCost"><i>'.ceil($cost / $cur['value']).'</i><span class="reduction">'.$cur['reduction'].'</span></span>')
            . '</div><div><a href="javascript: basket.insert('.$c['id'].','.$c['sid'].',0);" class="zakazButton order">� �������</a></div>'
            . '<div><a href="javascript: fast.init('.$c['id'].','.$c['sid'].')" class="zakazButton quickOrder"><span>������� �����</span></a></div></div>';
        return $html;
    }
    return '';
}

function tpl_tpluralForm($n, $form1, $form2, $form3)
{
    $n = abs($n) % 100;
    $n1 = $n % 10;

    if ($n > 10 && $n < 20) {
        return $form3;
    }

    if ($n1 > 1 && $n1 < 5) {
        return $form2;
    }

    if ($n1 == 1) {
        return $form1;
    }

    return $form3;
}

function cf_get_list(){
    global $_GLOBALS;

    if (empty($_GLOBALS['cf_list'])){
        $cf = empty($_SESSION['cf'])?array():$_SESSION['cf'];
        $filter_dirs = query_new('SELECT sws.*, ss.`title` as "sid_title" FROM `sw_section` ss LEFT JOIN `sw_section` sws ON sws.`sid`=ss.`id` WHERE ss.`active`=1 AND sws.`active`=1 AND ss.`filter`=1 ORDER BY ss.`pos`,sws.`title`',0,'sid',1);
        if (!empty($filter_dirs)){
            foreach ($filter_dirs as $fd_key=>$f_dir){
                foreach ($f_dir as $fs_key=>$f_sect){
                    $filter_dirs[$fd_key][$fs_key]['fltr_checked'] = in_array($f_sect['id'], $cf);
                }
            }
            $_GLOBALS['cf_list'] = $filter_dirs;
        }
    }
    return $_GLOBALS['cf_list'];
}


function cf_html($fltrs=array(),$tab_cont='',$pcont_class=''){
    global $_GLOBALS;

    $html = '';
    if (!empty($fltrs) && is_array($fltrs)){
        $form_id = 'cf_form'.(empty($pcont_class)?'':'_'.$pcont_class);
        $url = empty($_GLOBALS['cf_url'])?'/catalog/bouquet/':$_GLOBALS['cf_url'];
        $html .= '<form method="POST" id="'.$form_id.'" action="'.$url.'" onsubmit="formSubmit(this); return false;">'.PHP_EOL
            . '<div class="bc_popup_cont '.$pcont_class.'"><div class="bc_body"><div class="bc_head">'.$tab_cont.'</div>';
        $mlist = array();
        $walls = array();
        $slists = array();
        $i = 0;
        foreach ($fltrs as $mkey=>$mval){
            $show = empty($i)?' show ':'';
            $mlist[] = '<li class="'.$show.'" onmouseover="javascript: catalog.mfltr(this,'.$mkey.')" onclick="javascript: catalog.mfltr(this,'.$mkey.')"><div><span>'.$mval[0]['sid_title'].'</span></div></li>';
            $scols = (floor(count($mval)/count($fltrs))>=2)? 2 : 1;
            if (floor(count($mval)/count($fltrs))>=4){ $scols = 3;}
            $mval_list = array_chunk($mval, ceil(count($mval)/$scols));
            $wall = array_fill(0,(count($mval_list[0])<count($fltrs)?count($fltrs):count($mval_list[0])),'<li><i class="fltr_wall"></i></li>');
            $wall[$i] = '<li><i class="fltr_wall_arrow"></i></li>';

            $walls[] = '<ul class="wall_'.$mkey.$show.'">'.PHP_EOL.implode(PHP_EOL, $wall).PHP_EOL.'</ul>';
            foreach ($mval_list as $mvl){
                $slists[] = '<ul class="slist_'.$mkey.$show.'">'.PHP_EOL.cf_slist_html($mvl).PHP_EOL.'</ul>';
            }
            $i++;
        }
        $html .= '<div class="ul_cont"><ul class="mlist">'.implode(PHP_EOL,$mlist).'</ul>'.implode(PHP_EOL,$walls).implode(PHP_EOL,$slists).'</div>'
            . '<div class="bc_submit_btn"><span onclick="catalog.set_fltrs(\''.$form_id.'\')" class="butn_green">���������</span></div></div></div>'
            . '<input type="hidden" name="set_fltrs" value="true"></form>';
    }
    return $html;
}
function cf_slist_html($arr=array()){
    if (!empty($arr) && is_array($arr)){
        $html = '';
        foreach ($arr as $val){
            $chkd = !empty($_SESSION['cf']) && in_array($val['id'], $_SESSION['cf'])?' checked ':'';
            $html .= '<li id="li_'.$val['id'].'" class="'.$chkd.'"><div>'
                . '<input id="check_'.$val['id'].'" name="cf[]" type="checkbox" '.$chkd.' value="'.$val['id'].'" onclick="catalog.fltr_check(this,true)">'
                . '<span class="cfic_label" onclick="javascript:catalog.fltr_check(this,true);">'.$val['title'].'</span>'
                . '<span class="uncheck_filter fontico-cancel-circled-3" onclick="catalog.fltr_check(this,false)"></span></div>'
                . '</li>';
        }
        return $html;
    }
    return '';
}
function tplIncluder($rows) {
    $pth  = 'system/templates/inc/';
    $html = '';
    if (!empty($rows) && is_array($rows)) {
        foreach ($rows as $rv) {
            if (!empty($rv['incType'])) {
                switch (strtolower($rv['incType'])) {
                    case 'file':
                        if (file_exists($pth . $rv['incType'] . '/' . $rv['incName'] . '.inc')) {
                            $html .= PHP_EOL . file_get_contents($pth . $rv['incType'] . '/' . $rv['incName'] . '.inc');
                        }
                        break;
                    case 'tag':
                        if (!empty($rv['incTag'])) {
                            $link = empty($rv['incLink']) ? '' : ' ' . ($rv['incTag'] == 'script' ? 'src' : 'href') . '="' . $rv['incLink'] . '" ';
                            $attr = empty($rv['incAttr']) ? '' : ' ' . implode(' ', $rv['incAttr']);
                            $html .= PHP_EOL.'<' . $rv['incTag'] . $link . $attr . '>' . ($rv['incTag'] == 'script' ? '</script>' : '');
                        }
                        break;
                }
            }
        }
    }
    return $html;
}

function getUserCity(){
    $path = dirname(__FILE__);
    if (file_exists($path . "/SxGeo.php")) {
        include($path . "/SxGeo.php");
        if (file_exists($path . "/geodb/SxGeoCity.dat")){
            $SxGeo = new SxGeo($path . '/geodb/SxGeoCity.dat', SXGEO_BATCH | SXGEO_MEMORY);
            $ip = get_client_ip();
            //$ip = '94.25.177.34';
            //$ip = '81.92.176.34';
            if (mb_strpos($ip,".") !== false){
                return $SxGeo->getCityFull($ip);
            }else{
                return false;
            }

        }else{
            return false;
        }

    }else{
        return false;
    }


}

function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

function isIphone($user_agent=NULL) {
    if(!isset($user_agent)) {
        $user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
    }
    return (strpos($user_agent, 'iPhone') !== FALSE);
}

/**
 * �-�� ��� �������������� SEO �����������
 * SEO ASKED SHIT 2.0 :)
 */

// ����� ��������� ������� SEO ��� ����������� ������
// ���� �������� update - true, ����� ����� ���������� � �� �� ��� �� �� ������
// ���������� stdClass ���������� ��-�� title, description � keywords
function getSeoForItem($itemId, $update = false)
{
    $returnSeoData = new stdClass();
    $itemData      = query_new(sprintf("SELECT sc.*, ss.`link` AS `ss_link`, scs.`url` AS `cat_url`, scs1.`sid`, scs1.`first` 
                FROM `sw_catalog_section` scs1 LEFT JOIN `sw_catalog_section` scs ON scs.`cid` = scs1.`cid` 
                LEFT JOIN `sw_section` ss ON scs.`sid` = ss.`id` LEFT JOIN `sw_catalog` sc ON scs.`cid` = sc.`id` 
                WHERE sc.`id` = %s AND sc.`active` = 1 AND scs.`first`=1 LIMIT 1 ;", (int) $itemId), 1);
    // ���� ��������� ���������, ������ ��� - ���������� �� sw_catalog_prices
    if (empty($itemData['structure']))
    {
        // ��������� �������� ��������� � �����
        $tryStructure = query_new(sprintf("SELECT `composition` FROM `sw_catalog_price` WHERE cid = %s;", (int)$itemId), 1);
        if (!empty($tryStructure['composition']))
        {
            // ��������� ������ �������� <br> �� ������ ��� \n
            $itemData['structure'] = str_replace(array("<br>", "<br/>", "<br />"), "\n", $tryStructure['composition']);
        }
    }

    // �������� ���������� �� ������ SEO ��� � �����
    if (empty($itemData['html_title'])){
        // ���� � �� ���� ������ - ������� ���
        $returnSeoData->title = getSeoTitle($itemData['title'], $itemData['structure'], $itemData['cat_url'] );
    }else{
        // ���� � �� ��� ���-�� �������� � ����� �������� �� ���� �� ��� ���-�� ������������
        if (detectNeedSeoReload($itemData['html_title']))
        {
            // ���� ��� �� ���� ������������ - ������� ������ ����� � ������ ����� ���
            $returnSeoData->raw_title =  $itemData['title'];
            $returnSeoData->title = getSeoTitle($itemData['title'], $itemData['structure'], $itemData['cat_url'] );
            $returnSeoData->description = getSeoDescription($itemData['title'], $itemData['structure'] , $itemData['cat_url']);
            $returnSeoData->keywords = getSeoKeywords($itemData['structure']);
            if ($update)
            {
                // ������� ��������� � ��
                updateDBSeo($returnSeoData, $itemId);
            }
            return $returnSeoData;
        }
        else
        {
            // ���� ������������ �� ����, ������ ���� ����������� �� ��� ����
            $returnSeoData->title = changeExistingTitle($itemData['html_title']);
        }


    }
    $returnSeoData->raw_title = $itemData['title'];

    // ������ �������� description

    if (empty($itemData['description'])){
        $returnSeoData->description = getSeoDescription($itemData['title'], $itemData['structure'], $itemData['cat_url']);
    }else{
        // ���� ���� � �� �� ���������
        $returnSeoData->description = $itemData['description'];
    }

    // ������ keywords ��� �������
    if (empty($itemData['keywords'])){
        $returnSeoData->keywords = getSeoKeywords($itemData['structure']);
    }else{
        $returnSeoData->keywords = $itemData['keywords'];
    }

    if (($update) && ($itemData['html_title'] != $returnSeoData->title ) )
    {
        // ������� ��������� � ��
        updateDBSeo($returnSeoData, $itemId);
    }

    return $returnSeoData;
    // ����� :)
}

// �-� ������� ��������� ��������, ������ ������� ������ ������ �� ������
// � ��� ���� �������� �������� ������, ��� ��������� � ������ ��� �� sw_catalog_section
function getSeoTitle($title, $structure, $url = ''){
    $putToFirst = false;
    /*
     * ���� �������� ������ ���������� � �����, �� ����������, ����� ���������
     * ����� �����-�� (������ ��� ����������) � ��������� (- �� ���) | ���������������.��(�������� ����� �� ������� � ��.)
     */

    // �� ����� ��������� ����� ����� ������ ��� ��� �������� ��� ����
    // ���� url �� ������ �� ���������� ���� �� ��������� �����
    // � ���� ��� ������, �� ����� ��������� ������ �� �����, ��� ����� ����� 99% �� ������������
    $addBouquet = true; // �� ��� ����� ���������
    $exceptionUrl = array('otkrytki', 'shariki', 'konfety', 'elki', 'derevja', 'novogodnie', 'ulichnye-rastenija');
    if (!empty($url))
    {
        foreach ($exceptionUrl as $skip)
        {
            // ���� ����� ���� � ����
            if (mb_stripos($url, mb_convert_case($skip, MB_CASE_LOWER, "WINDOWS-1251"),
                    0, "WINDOWS-1251") !== false)
            {
                // ���� �� ���������� ��� � ������ ������ �� ���� ��������� �����
                $addBouquet = false;
                // ������ ������ �������� ���-�� ��� ���
                break;
            }
        }
    }
    else
    {
        // ���� ��� ������, �� ��� ������ � ��
        // ����� ���� ����������� $addBouquet ����� true;
    }


    // ��������� ��������� ���� ���� ������ ��� ��� ���� �����
    if ($addBouquet)
    {
        $struc = prepareSeoStructure($structure);
    }else{
        $struc = ''; // ������ ���������
    }

    // ��������� ���������, ������ ��������� title
    if ($addBouquet)
    {
        if (mb_stripos($title, '�����', 0, "WINDOWS-1251") === false )
        {
            // ���� � ����� ���� ���-�� �� ���������, �� ����� ������ �����, ����� ����� ������
            $addFlowers = false;
            if (!empty($struc))
            {
                $addFlowers = detectAddFlowers($title, $struc);
            }


            if ($addFlowers === true)
            {
                $title = '����� ������ ' . $title;
            }
            else
            {
                $title = '����� ' . $title;
            }

        }
        else
        {
            // ���� �� ��������� �����, ��� ���� ������, ��������� ��������� ��� ���
            $addFlowers = detectAddFlowers($title, $struc);
        }
    }
    else
    {
        $addFlowers = true;
        if (!empty($struc))
        {
            $addFlowers = detectAddFlowers($title, $struc);
        }

    }
    // ������ ������� ������ title


    return checkTitleAndGet($title, $struc, $addFlowers, $url) ;

}

// �-� �������� ������ � �������� ����� ��� ����
function checkTitleAndGet($title, $structure, $needAddCvetov, $url='')
{
    // 0. ���� ��������� ���
    if (empty($structure))
    {
        // ������ � ������
        //	if (stripos($url, 'bouquet') !== false)
//		{
//			return trim(preg_replace('!\s+!', ' ', $title . ' � ��������� | ���������������'));
//		}else{
        // ���� ��� �� �����
        if (!empty($url))
        {
            $getCatsTxt = getTextCategoriesSeo($url);
            return trim(preg_replace('!\s+!', ' ', $getCatsTxt . ' � ��������� | ���������������'));
        }
        else
        {
            return trim(preg_replace('!\s+!', ' ', $title . ' � ��������� | ���������������'));
        }
//		}

    }

    // 1. ���� � �������� �� ���������� ���-�� �� ���������
    if ($needAddCvetov === true)
    {
        // ����� ���������, �� � ������ �������
        $title = trim(preg_replace('!\s+!', ' ',  detectTitleLen($title, $structure) . ' � ��������� | ���������������')) ;
    }
    else
    {
        // 2. ���� ���-���� ���-�� �� ��������� ���� � ��������, ����� ��������� �� �����, � ����� ��! ����� �����
        if (mb_stripos($title, '����', 0, "WINDOWS-1251") !== false )
        {
            $title = trim(preg_replace('!\s+!', ' ',  detectTitleLen($title, $structure) . ' � ��������� | ���������������')) ;
        }
        else
        {
            //3. ������ ���, �� ����� ���� ������� ��� ��������, �������� URL
            $exceptionUrl = array('otkrytki', 'shariki', 'konfety', 'elki', 'derevja', 'novogodnie', 'ulichnye-rastenija');
            if (!empty($url))
            {
                $getCatsTxt = getTextCategoriesSeo($url);
                foreach ($exceptionUrl as $skip)
                {
                    // ���� ����� ���� � ����
                    if (mb_stripos($url, mb_convert_case($skip, MB_CASE_LOWER, "WINDOWS-1251"),
                            0, "WINDOWS-1251") !== false)
                    {
                        // ���� ���-�� ��������, �������� ��� ������
                        switch ($skip)
                        {
                            case 'otkrytki':
                                // ��� �������� ��������� �������� �������� - ��������
                                if (mb_stripos($title, '��������',0, "WINDOWS-1251") !== false){
                                    // ���� �������� ���� - �� �����������
                                    $title = trim(preg_replace('!\s+!', ' ',  $title . ' � ��������� | ���������������')) ;
                                }else{
                                    // ��������
                                    $title = trim(preg_replace('!\s+!', ' ',  '�������� '.$title . ' � ��������� | ���������������')) ;
                                }

                                break;
                            case 'shariki':
                                // ��� �������� ��������� �������� �������� - ���
                                if (mb_stripos($title, '���',0, "WINDOWS-1251") !== false){
                                    // ���� �������� ���� - �� �����������
                                    $title = trim(preg_replace('!\s+!', ' ',  $title . ' � ��������� | ���������������')) ;
                                }else{
                                    // ��������
                                    $title = trim(preg_replace('!\s+!', ' ',  '��������� ��� '.$title . ' � ��������� | ���������������')) ;
                                }

                                break;
                            case 'konfety':
                                // ��� �������� ��������� �������� �������� - �������
                                if (mb_stripos($title, '������',0, "WINDOWS-1251") !== false){
                                    // ���� �������� ���� - �� �����������
                                    $title = trim(preg_replace('!\s+!', ' ',  $title . ' � ��������� | ���������������')) ;
                                }else{
                                    // ��������
                                    $title = trim(preg_replace('!\s+!', ' ',  '������� '.$title . ' � ��������� | ���������������')) ;
                                }

                                break;
                            //TODO: ���������� ��� ������ ���������

                            default:
                                $title = trim(preg_replace('!\s+!', ' ',  $getCatsTxt . ' � ��������� | ���������������')) ;
                                break;
                        }
                        break;
                    }else{
                        $title = trim(preg_replace('!\s+!', ' ',  $title . ' � ��������� | ���������������')) ;
                        break;
                    }
                }
            }else{
                $title = trim(preg_replace('!\s+!', ' ',  $title . ' � ��������� | ���������������')) ;
            }

        }

    }

    return $title;

}

function detectTitleLen($titleStr, $structureStr)
{
    if ((empty($titleStr)) || (empty($structureStr)) )
    {
        // ���� ���-�� ������ �� �� ���� ���������
        return $titleStr;
    }
    // 1. ���������� ���������� �������� � �����
    $titleLen = mb_strlen($titleStr, "WINDOWS-1251");
    if ($titleLen >= 50) return $titleStr;
    // 2. ��������� ������ ���������
    $structArr = explode(",", $structureStr); // ������ ���������
    // 3. �������� ��� �������� ���������
    $strucNew = array();
    foreach ($structArr as $strucElem)
    {
        $strucNew[] = trim(str_replace('  ', ' ', $strucElem));
    }

    if (empty($strucNew)) return $titleStr;
    // ������ ����� ��������� � ������ �� ������ �������� ��������� � ��������� ������
    $titleLenNew = $titleLen ;
    $i = 0; // ���������� ��������
    $stucLen = count($strucNew);
    $newTitle = '';
    while ($titleLenNew < 40)
    {
        if ($i > $stucLen  ) break; // ��������� ���������, ������
        $newTitle = sprintf( $titleStr . " (%s)", implode(", ",array_slice($strucNew, 0, $i+1)))  ;
        $titleLenNew = mb_strlen($newTitle, "WINDOWS-1251");
        $i++;
    }

    return $newTitle ;

}

//������� ���������� ����� �� ��������� � ����� ����� �����
// ���� �������� � ��� ������ ������ � ��������������� ������ ��������� ����
// "����, �����, �����"
function detectAddFlowers($titleStr, $structureStr){

    if ((empty($titleStr)) || (empty($structureStr)) )
    {
        // ���� ���-�� ������ �� �� ���� ���������
        return false;
    }

    if (mb_stripos($titleStr, '����', 0, "WINDOWS-1251") !== false )
    {
        return false;
    }

    $return = true;

    $structArr = explode(",", $structureStr); // ������ ���������
    // ������� ���� �� � �������� ���� ���� ���������� �� ����������
    foreach ($structArr as $strucPart )
    {
        if (!empty($strucPart ))
        {
            if (mb_stripos($titleStr, mb_convert_case(mb_substr(strtolower(trim($strucPart)), 0, -1, "WINDOWS-1251"),
                    MB_CASE_LOWER, "WINDOWS-1251"), 0, "WINDOWS-1251") !== false)
            {
                // ���-�� �������� �������� ����� ���� 40�� (����) - � ���� ������ ������ ����� ������ �� ����
                $return = false;
                break;
            }
        }
    }

    return $return;
}


// ������� �������� ���������� ����� seo description
// ���� �������� ������ � ������ ��� �� sw_catalog_section
function getSeoDescription($title, $structure, $url = ''){
    /*
     * �������� ����� (������) �� ��� ��� � ����. {������ ������ �����|�������� ������� � ������ �����},
     * {������� � ����������|�����������|��������������}
     * �������� �� ������ {�������������|���������} - ���������������.��, 8 (800) 333-12-91
     */

    // �� ����� ��������� ��������� ������ ��� ��� ��� ���
    $addStruct = true; // �� ��� ��������� ���������
    $exceptionUrl = array('otkrytki', 'shariki', 'konfety', 'elki', 'derevja', 'novogodnie', 'ulichnye-rastenija');
    if (!empty($url))
    {
        foreach ($exceptionUrl as $skip)
        {
            // ���� ����� ���� � ����
            if (mb_stripos($url, mb_convert_case($skip, MB_CASE_LOWER, "WINDOWS-1251"),
                    0, "WINDOWS-1251") !== false)
            {
                // ���������� ��� � ������ ������ �� ���� ��������� ���������
                $addStruct = false;
                // ������ ������ �������� ���-�� ��� ���
                break;
            }
        }
    }


    $parts1 = array('������ ������ �����', '�������� ������� � ������ �����');
    $parts2 = array('������� � ����������', '�����������' , '��������������');

    $description = '�������� ����� ';

    if ($addStruct)
    {
        $struc = prepareSeoStructure($structure);
    }
    else
    {
        // ���� �� ���� ��������� ���������, �� ��� ����� ������
        $struc = '';
    }



    if (!empty($struc )){
        $description .= '(' . $struc . ')';
    }
    $description .= ' �� ��� ��� � ����. ' . $parts1[array_rand($parts1)] . ', '  ;
    // ���� ��������� ��� �� ������� �� ������ ����� �����
    $dos_index = $parts2[array_rand($parts2)];
    if (stripos($dos_index, '����������') === false){
        $tres_index = '���������';
    }else{
        $tres_index = '�������������';
    }
    $description .= $dos_index . ' �������� �� ������ ' . $tres_index . ' - ���������������, 8 (800) 333-12-91';
    // ������ ������ � ���������� �����
    $description  = trim(preg_replace('!\s+!', ' ', $description)) ;

    // ��������� ���� ��� ����������
    $appendDescTitle = false;
    $s = sprintf("SELECT `id` FROM `sw_catalog` WHERE `description` = %s LIMIT 0,1", quote($description));
    $subQuery = query_new($s );
    if (is_array($subQuery ))
    {
        if (count($subQuery ) > 0)
        {
            $appendDescTitle = true;
        }
    }

    if ($appendDescTitle === true)
    {
        $title = getSeoTitle($title, $structure, $url);
        $title = str_replace(' � ��������� | ���������������', '', trim(preg_replace('/\s*\([^)]*\)/', '', $title ))) ;
        $description  = $title . '. ' . $description;
    }

    return $description  ;
}


function getSeoKeywords($structure){
    $kw = '�������� �����';
    $struc = prepareSeoStructure($structure);
    if (!empty($struc )){
        $kw .= ' ' . $struc;
    }
    return trim(preg_replace('!\s+!', ' ', $kw));
}

// ����� ���������� ���������� ������
// ���� �������� ������
function prepareSeoStructure($structure){
    // ������ ���� ��� ���� �������� �� �������
    $needToRemove = array('�������', '�������', '��������', '��������', '�������', '�������', '���������',
        '���������� �����������', '�����������', '���������', '������', '�������� �������', '�������� �����',
        '��������� ��������', '������� ��������', '������ ������', '���������', '���������� ������');

    // ������� ����� ���������
    $structureData = !empty($structure) ? explode("\n", $structure) : array();
    if (!empty($structureData))
    {
        // ������ �� ��������� ��� ����������
        $newStructure = array();
        foreach ($structureData as $stru )
        {
            if (!empty($stru))
            {
                $parts = explode(':', $stru);
                if (!empty($parts[0]))
                {
                    if (!in_array($parts[0], $needToRemove))
                    {
                        // ���� ������� ������ ������ ���� ��� ���-�� ����
                        $newStructure[] = trim(preg_replace('/\s*\([^)]*\)/', '', $parts[0]));
                    }

                }
            }
        }
        $struc = implode(', ',array_slice($newStructure, 0,10));
        //TODO: ���������� ���� ��������, ���� ����� ���� �����
        //$struc = strlen($struc) > 55 ? implode(', ', array_slice($newStructure,0,2)).' � ��.' : $struc;

        // ���� �������� �����-�� ����� � ��������� - ������ ��

        $struc = preg_replace('/\d+/', '', $struc );
        // ���� �������� ���������� - ���� ������ �� �� �������
        $struc = str_replace('��', '', $struc );
        // ���� �������� ��������� - ���� ������ ��
        $struc = str_replace(':', '', $struc );
        // �������� ���� � ����� ������� � �������
        $struc = str_replace(' )', ')', $struc );
        // ������ ������ �������
        $struc = str_replace('  ', ' ', $struc );
        $struc =  trim($struc);

    }else{
        $struc = '';
    }
    return $struc;
}

// ����� ����������� ������������ � �� ���������
// ���������� ������������ �����
function changeExistingTitle($title)
{
    $needToRemove = array(' - StudioFloristic.ru');
    foreach ($needToRemove as $remover)
    {
        $title = str_replace($remover, '', $title);
    }

    // ���� � ������ � ����� ���� ����� �� ���� ������
    if  (substr($title, -1) == '.')
    {
        $title = rtrim($title, '.');
    }
    // ������� ����� ��� ��������������� ����� ��� ��� ����������
    if  (substr($title, -1) == '!')
    {
        $title = rtrim($title, '!');
    }

    // ���� ���, ������ ������� � ����� ��� �����
    if (strpos($title, '���������������') === false)
    {
        $title .= ' | ���������������';
    }

    return $title;

}

// ��������� ����������� ������������� ���������� ����������, description � keywords
// �� ���������� ������������ ������ ���� � �� ��� ���� �����-�� ����� title
// ��������� ����� �� ������� ������������ ����� ���� � ���� ��� ���� - ���������� true
// �� ���� ��� ��������� ���������� ������������
function detectNeedSeoReload($title){
    // ���� � ��������� ���� ��� �����, ����� ��� ���� ������������
    $stopWords = array('studio floristic', 'Studio Floristic');
    // 1. ����� ��������� ���� ���� � �������� ��� �����
    foreach ($stopWords as $stop)
    {
        if (strpos($title, $stop) !== false)
        {
            return true;
        }
    }

    //2. �������� ���������� ���� � ���������
    $stopWordsCount = 3;
    $titleCount = str_word_count($title, 0, '�����������娸����������������������������������������������������' );
    // ���� ���������� ���� � ��������� ������ �� ����� ������������
    if ($titleCount <= $stopWordsCount )
    {
        return true;
    }

    //3. ���� ���
    return false;

}

// ��������� ���������� ������ SEO � ��
// ���� �������� ������ stdClass � id �� sw_catalog
function updateDBSeo($seoObject, $id){
    if ($seoObject instanceof stdClass)
    {
        if (is_numeric($id))
        {
            if ((isset($seoObject->title)) && (isset($seoObject->description)) && (isset($seoObject->keywords)))
            {
                $query = sprintf("UPDATE `sw_catalog` SET `html_title` = '%s', `description` = '%s', `keywords` = '%s' WHERE id = %s;",
                    $seoObject->title, $seoObject->description, $seoObject->keywords, $id);

                db_query($query);
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

function getTextCategoriesSeo($url){
    $c = catItem_url($url);
    $sep = '@';
    $bc_text[] = array('title'=>$c['title'],'class'=>'tcol-orange','url'=>$c['url']);
    $dir = query_new('SELECT * FROM sw_section WHERE `active` = 1 AND `id` = '.$c['sid'].' LIMIT 1',1);

    while (!empty($dir)){
        $_GLOBALS['sections'][] = $dir['link'];
        $bc_text[] = array(
            'title'=>$dir['title'],
            'class'=>(empty($bc_text)?'tcol-orange':''),
            'url'=>$dir['pach']);
        $dir = query_new('SELECT * FROM sw_section WHERE `active` = 1 AND `id` = '.$dir['sid'].' LIMIT 1',1);
    }
    // ����� ������ ������ ��� ��������, �������� ����� ������ � ����������.



    if (isset($bc_text[1]))
    {
        if (stripos($url, 'otkrytki') !== false)
        {
            if (mb_stripos($bc_text[0]['title'], mb_substr($bc_text[2]['title'] , 0, -1, "WINDOWS-1251"),0,"WINDOWS-1251" ) !== false)
            {
                return  strip_tags( $bc_text[0]['title']);
            }
            else
            {
                return  strip_tags( $bc_text[2]['title'] .' '. $bc_text[0]['title']);
            }

        }
        elseif(stripos($url, 'novogodnie') !== false)
        {
            if (mb_stripos($bc_text[0]['title'], '��������',0,"WINDOWS-1251" ) !== false)
            {
                return  strip_tags( $bc_text[0]['title']);
            }
            else
            {
                return  strip_tags( $bc_text[1]['title'] .' '. $bc_text[0]['title']);
            }
        }
        else
        {
            if (mb_stripos($bc_text[0]['title'], mb_substr($bc_text[1]['title'] , 0, -2, "WINDOWS-1251"),0,"WINDOWS-1251" ) !== false)
            {
                return  strip_tags( $bc_text[0]['title']);
            }
            else
            {
                return  strip_tags( $bc_text[1]['title'] .' '. $bc_text[0]['title']);
            }
        }


    }
    else
    {
        return  strip_tags( $bc_text[0]['title']);
    }


}

//  �-�� ���������� ����� ��� ��������� ������.

// ������ SMS
function renderAbandonedSMS($obj)
{
    $pattern = query_new("SELECT `value` FROM `sw_variables` WHERE `name` = 'abandoned_sms' LIMIT 0,1 ", true);
    $pattern = $pattern['value'];
    if (empty($pattern))
    {
        return 'ERROR: empty pattern';
    }
    $smsPattern = $pattern;
    if (isset($obj->hash))
    {
        if (!empty($obj->hash))
        {
            return str_replace('{%HASH%}', $obj->hash, $smsPattern );
        }
        else
        {
            return 'ERROR: empty hash';
        }
    }else{
        return 'ERROR: empty hash';
    }
}
// ������ ������

function renderAbandonedMail($obj)
{
    $path = realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..'  ). DIRECTORY_SEPARATOR . 'modules/notify/tmpl/abandon.html';
    /*������ ������
     * <tr>
       <td valign="middle" style="padding-top:5px; text-align:center;"><img src="https://www.studiofloristic.ru/files/catalog/2476/w199_f36ad1557b3b643d8faf5d152f0200e3.jpg" width="140" /></td>
       <td valign="middle" style="padding-top:5px;"><div style="font-weight:bold; font-size: 18px; color: #272727; padding-bottom: 7px;">����� "������� �������"</div><div style="font-size: 18px; color: #424242;">�����������</div></td>
       <td valign="middle" style="padding-top:5px;"><div style="font-weight:bold; font-size: 18px; color: #ea1313; text-align:right;">2 399 �.</div></td>
       </tr>
     */

    /*
     * ���
     <tr>
     <td colspan="3" height="5" style="border-bottom: 1px solid #e2e2e2;">&nbsp;</td>
     </tr>
     */

    // ���� ����������� ����� ��� ������ ��� ������
    $tovars = '';
    $i = 0;
    // ��������� ����� �� ������� 15% ��� ���� ��������
    $productCount = 0;
    foreach ($obj->products as $p)
    {
        // ����� ����� ��� ������ 'c' ���� ��������� � �������� ������� $p = $p->�id;
        if ($i == 2) break;
        $priductOrigin = db_query("SELECT share FROM `sw_catalog` WHERE `id` = '$p->�id' LIMIT 1");
        $priductOrigin = mysql_fetch_array($priductOrigin, MYSQL_ASSOC);

        $share = ($priductOrigin['share'] == '1') ? '<img src="https://www.studiofloristic.ru/i/accii.png" style="float: right">' : '';

        $im = explode(":", $p->image );

        $image = "https://www.studiofloristic.ru/files/catalog/".$p->�id."/w199_".$im[0];
        switch ($p->type){
            case 'my-bouquet': $image = 'https://www.studiofloristic.ru/i/myBouquet.gif'; break;
            case 'my-order': $image = 'https://www.studiofloristic.ru/i/individ-100.jpg'; break;
        }

        $tovars .= '<tr>';
        $tovars .= '<td valign="middle" style="padding-top:5px; text-align:center;">
            <a href="'.$p->url.'" target="_blank" title="'.$p->title.'">
                <img src="'.$image.'" width="140" />
                '.$share.'    
            </a>
        </td>';

        if (mb_strpos($p->title, '(', 0, "WINDOWS-1251") !== false)
        {
            $titlePart = explode("(", $p->title);
            $titleFirst = trim($titlePart[0]);
            $titleSecond = '('.trim($titlePart[1]);
            $tovars .= '<td valign="middle" style="padding-top:5px; line-height: 18px;">
						<a href="'.$p->url.'" target="_blank" title="'.$p->title.'" style="text-decoration: none;">
						<div style="font-weight:bold; font-size: 18px; color: #272727; padding-bottom: 7px;">
						'.$titleFirst .'
						</div>
						<div style="font-size: 18px; color: #424242;">
						'.$titleSecond .'
						</div>
						</a>
						</td>';
        }else{
            $tovars .= '<td valign="middle" style="padding-top:5px; line-height: 18px;">
						<a href="'.$p->url.'" target="_blank" title="'.$p->title.'" style="text-decoration: none;">
						<div style="font-weight:bold; font-size: 18px; color: #272727; padding-bottom: 7px;">
						'.$p->title.'
						</div>
						</a>
						</td>';
        }

        $tovars .= '<td valign="middle" style="padding-top:5px; line-height: 24px;"><div style="font-weight:bold; font-size: 18px; color: #ea1313; text-align:right;">'.(int)$p->count.' x '.number_format($p->price, 0, '.', ' ').'�.</div></td>';
        $tovars .= '</tr>';
        $tovars .= '<tr><td colspan="3" height="5" style="border-bottom: 1px solid #e2e2e2;">&nbsp;</td></tr>';
        $productCount += $p->count;
        $i++;
    }

    $total = '<tr>';

    $total .= '<td valign="middle" style="padding-top:5px; text-align:center;"><img src="https://www.studiofloristic.ru/i/mail-cart-icon.png" width="71" /></td>';
    $total .= '<td valign="middle" style="padding-top:5px; line-height: 18px;">';
    if (count($obj->products ) > 2)
    {
        $total .= '<div style="font-size: 18px; color: #272727; padding-top: 10px; padding-bottom: 7px;">� ��� '.(count($obj->products ) - 2) .' '.sklonenie((count($obj->products ) - 2), array('�����', '������', '�������')).'</div>';

    }
    $total .= '<div class="mobilka" style="font-weight:bold; font-size: 18px; color: #272727;">����� '.$productCount .' '.sklonenie((count($obj->products )), array('�����', '������', '�������')).' �� �����</div>';

    $total .= '<div style="font-size: 14px; color: #272727; padding-bottom: 10px;">(� ������ <span style="color: #f38a1a; font-weight: bold;">15%</span> ������)</div></td>';
    $total .= '<td valign="middle" style="padding-top:5px; line-height: 24px;">
							<div style="font-weight:bold; font-size: 16px; text-align:right;"><s>'.number_format($obj->summ, 0, '.', ' ').'�.</s></div>
							<div style="font-weight:bold; font-size: 24px; color: #ea1313; text-align:right;">'.number_format($obj->summ_discount, 0, '.', ' ').'�.</div>
				   </td>';
    $total .= '</tr>';

    $html = file_get_contents($path, false);
    $html = str_replace("{%PRODUCT_LINES%}", $tovars, $html);
    $html = str_replace("{%TOTAL%}", $total, $html);
    $html = str_replace("{%HASH%}", $obj->hash, $html);
    $html = str_replace("{%NAME%}", $obj->name, $html);
    return $html;

}
function sklonenie($n, $forms) {
    return $n%10==1&&$n%100!=11?$forms[0]:($n%10>=2&&$n%10<=4&&($n%100<10||$n%100>=20)?$forms[1]:$forms[2]);
}

function countSizes($cid) {
    $count = query_new("SELECT COUNT(cid) AS count FROM `sw_catalog_price` WHERE cid=".$cid, true);
    return $count['count'];
}