<?php

$_GLOBALS['media_version'] = '?v=1.3';

$tpl_name = !empty($_GLOBALS['template']) ? $_GLOBALS['template'] : 'template';
if (file_exists('system/templates/'.$tpl_name.'.tpl')){
    $html = file_get_contents('system/templates/'.$tpl_name.'.tpl');
}else{
  die('������ ������� �� ������');
}

$_GLOBALS['head_inc'][] = '<base href="http'.(isset($_SERVER['HTTPS'])?'s':'').'://'.$_SERVER['HTTP_HOST'].'/" />';
$_GLOBALS['head_inc'][] = '<link href="https://fonts.googleapis.com/css?family=Roboto:300" rel="stylesheet">';
if (isset($_GET['pg']) || stripos($_SERVER['REQUEST_URI'],'?')!==false || stripos($_SERVER['REQUEST_URI'],'/op=')!==false){
    $_GLOBALS['head_inc'][] = '<meta name="robots" content="noindex,follow">';
}
$ADD_SCRIPTS = (!empty($_GLOBALS['js_preload']) && is_array($_GLOBALS['js_preload']))?$_GLOBALS['js_preload']:array();

if (empty($_GLOBALS['html_title'])) {
    $_GLOBALS['html_title'] = !empty($_GLOBALS['title']) ? $_GLOBALS['title'] : $_GLOBALS['v_title'];
}

$html = str_replace("[%TITLE%]", $_GLOBALS['html_title'], $html);

// ������� SEO ��� �������� �����
if (stripos($_SERVER['REQUEST_URI'], 'actions.html'))
{
  $html = str_replace("[%DESCRIPTION%]", "������� ������ �� ����� �� �������� � ������. �������� ���� � ������� �����������: 8 800 333-12-91", $html);
}else{
  $html = str_replace("[%DESCRIPTION%]", !isset($_GLOBALS['description']) ? $_GLOBALS['v_description'] : $_GLOBALS['description'], $html);
}

$html = str_replace("[%KEYWORDS%]", $_GLOBALS['keywords'], $html);
$html = str_replace("[%BODY_HEADER%]", file_get_contents('system/templates/header.tpl.html'), $html);
$html = str_replace("[%BODY_BG%]", get_curr_bg(), $html);
$tpl_tmp = file_get_contents('system/templates/leftShadow.tpl.html').PHP_EOL.file_get_contents('system/templates/rightShadow.tpl.html');
$html = str_replace("[%HTML_BLOCK%]", $tpl_tmp, $html);

// ��������� ������ �����-������
$html = str_replace("[%MEDIA_VERSION%]", $_GLOBALS['media_version'], $html);

$minicart = '<div class="howMuchItem">'.mini_cart().'</div>';
if (_IS_USER) {
  $udata = '<div class="links"><span>����� ����������<br /></span><a href="/office/op=my">'.$_SESSION['username'].'</a></div>';
  $udata.= '<div class="cart"><a href="'.(empty($_GLOBALS['in_cart'])?'javascript: void(0);" style="cursor:default;text-decoration:none;"':'/basket.html"').'>���� �������</a></div>';
  $udata.= $minicart.'<div class="discount">';
    $udata.= (!empty($_SESSION['discount']))?'���� ������ <span id="discount-value">'.getRealUserDiscount().'%</span><br />':'';
  $udata.= '<a href="javascript:login.exit()">�����</a></div>';
} else {
  $udata = '<div class="links"><a href="javascript:login.entr()" id="showLogin">����</a> | <a href="javascript:login.registration()" id="showRegistration">�����������</a></div>';
  $udata.= '<div class="cart"><a href="'.(empty($_GLOBALS['in_cart'])?'javascript: void(0);" style="cursor:default;text-decoration:none;"':'/basket.html"').'>���� �������</a></div>';
  $udata.= $minicart;
  $udata.= '<div class="discount">������ ������? <div class="disc_pop_link text_block centered_hover_popup" tb_cont="discPop_link" tb="TYPE_hover_popup--append--MULTIPLE_disc_pop_link"><a href="javascript:login.registration()">�����������������</a></div></div>';
    $_GLOBALS['body_pre_close'][] = '<div id="popupLogin">'.login_forms().'</div>';
}
$html = str_replace('[%USER-DATA%]', $udata, $html);

$filter_prices = array();
$fp_text = '';

$max_price = query_new('SELECT MAX(`price`) AS max FROM sw_catalog sc WHERE sc.`active`=1', 1);

if (!isset($cur['value'])){
    $cur = get_currency();
}

$fps = !empty($_GLOBALS['filter_price_set']) ? $_GLOBALS['filter_price_set'] : array(0,$max_price['max']);
for ($p=1;$p<8;$p++){
    $min = ($p<2)?0:$p*1000;
    $max = ($p<7)?($p+1)*1000: $max_price['max'];
    $active_range = ($min == $fps[0] && $max == $fps[1] && $_GLOBALS['cfp_count']>0)?' current ':'';
    $fp_text .= '<li class="'.$active_range.'"><a href="javascript: filterPrice.setPriceRange(\''.$min.'\',\''.$max.'\'); void(0);">';
    if ($min>=2000 && $min<7000){
        $fp_text .= '�� '.ceil($min / $cur['value']).'<br /> �� '.ceil($max / $cur['value']);
    }else{
        $fp_text .= ($min<2000)? '����� <br />'.ceil($max / $cur['value']) : '�����<br /> '.ceil($min / $cur['value']);
    }
    $fp_text .= '</a></li>';
}
$money_step = 100;
$slider_step = 36*$money_step/1000;
$top_arr = ($fps[0]<2000)? 0 : round(($fps[0]-2000)/$money_step)*$slider_step+27;
$bottom_arr = ($fps[1]>7000)? 235: round(($fps[1]-2000)/$money_step)*$slider_step+27;
if (($bottom_arr-$top_arr)<21.6 && $bottom_arr!=235 && $top_arr!=0){
    $top_arr = ($bottom_arr-21.6)<27 ? 0 : $bottom_arr-21.6;
    $fps[0] = ($top_arr<27)? 0 : ($top_arr-27)/$slider_step*$money_step + 2000;
}

$top_brdg = $top_arr+9;
$height_brdg = $bottom_arr-$top_arr;
$_GLOBALS['cfp_path'] = (empty($_GLOBALS['cfp_path']) || stripos($_GLOBALS['cfp_path'],'/catalog/',0)===false)? '/catalog/filter.html' : $_GLOBALS['cfp_path'];
$cur_img = array('euro'=>'ico1', 'usd'=>'ico2', 'rub'=>'ico3');
$filter = '<div class="rightMoney">';
foreach ($cur_img as $k=>$v) {
    $filter.= '<a href="javascript:cur.'.$k.'(); void(0);" class="'.$v.($k != $cur['name'] ? '' : 'active').'"></a>';
}
$filter.= '</div>'
        . '<div class="filter_price_cont">'
            . '<ul class="lavaLamp">'.$fp_text.'</ul>'
            . '<ul class="price_slider"><li>'
                . '<div id="top_price_slider" class="price_slider_arrow" style="top:'.$top_arr.'px;"></div>'
                . '<div id="sliders_bridge" style="top:'.($top_brdg).'px; height:'.($height_brdg).'px;"></div>'
                . '<div id="fp_popup_info" onmouseover="filterPrice.showPopup(false)" onmouseout="filterPrice.showPopup(true)">'
                    . '<div class="fppi_cont">'
                        . '<div class="fppi_range"><span>�� <span id="fppi_min">'.$fps[0].$cur['reduction'].'</span> �� <span id="fppi_max">'.$fps[1].$cur['reduction'].'</span></span></div>'
                        . '<div class="fppi_results"><span>������� �������: <span id="fppi_results" action_time="0">'.$_GLOBALS['cfp_count'].'</span></span></div>'
                        . '<div class="fppi_actions">'
                            . '<span class="fppi_loading"></span>'
                            . '<span class="fppi_see_results" onclick="filterPrice.seeResults();">��������</span>'
                            . '<span class="fppi_change_range">�������� ��������</span></div>'
                    . '</div>'
                    . '<div class="fppi_right_arr"></div>'
                . '</div>'
                . '<div id="bottom_price_slider" class="price_slider_arrow" style="top:'.$bottom_arr.'px;"></div>'
            . '</li></ul>'
        . '</div>';


include_once('t/menu.php');
include_once('system/templates/border_banners.php');
include_once('t/right.php');
// if (isset($_GLOBALS['product'])) {
//   $html = str_replace('[%RIGHT-BLOCK%]', '', $html);
// } else {
//   include_once('t/right.php');
// }

$html = str_replace('[%LEFT_BANNERS%]', empty($left_banners)?'':$left_banners, $html);
$html = str_replace('[%RIGHT_BANNERS%]', empty($right_banners)?'':$right_banners, $html);

$html = str_replace('[%FILTER%]', $filter, $html);
$html = str_replace('[%FOOTER-LEFT%]', $_GLOBALS['v_footer-left'], $html);
$html = str_replace('[%FOOTER-RIGHT%]', (!empty($_GLOBALS['v_bottom-menu']) ? '<div class="footer111">'.$_GLOBALS['v_bottom-menu'].'</div>' : null), $html);
$html = str_replace("[%MODULE_PACH%]", !empty($_GLOBALS['module_pach']) ? '<div class="kroshka">'.$_GLOBALS['module_pach'].'</div>' : '', $html);
if (isset($module) && $module=='basket' && $_GLOBALS['title']!='���� ������� �����'){
    $html = str_replace("[%MODULE_TITLE%]", basket_breadcrumbs($op), $html);
} elseif (isset($_GLOBALS['bcms'])){
  $html = str_replace("[%MODULE_TITLE%]", !empty($_GLOBALS['bcms']) ? '<h1>'.$_GLOBALS['title'].'</h1><div class="breadcrumbs item">'.$_GLOBALS['bcms'].'</div>' : '', $html);
} else {
    $html = str_replace("[%MODULE_TITLE%]", !empty($_GLOBALS['title']) ? '<h1>'.$_GLOBALS['title'].'</h1>' : '', $html);
}
$ADD_SCRIPTS = '<script type="text/javascript">'.PHP_EOL.implode(';'.PHP_EOL,$ADD_SCRIPTS).';'.PHP_EOL.'</script>';
$ADD_SCRIPTS .= PHP_EOL.tplIncluder($_GLOBALS['pre_opens_includes']).PHP_EOL;

// A/B-�����: ������
$__go = isset($_COOKIE["__go"]) ? explode('&', urldecode($_COOKIE["__go"])) : [];
$__ex = isset($_GET["__ex"]) ? $_GET["__ex"] : '';

// ����������: ������
if ( (stripos($_SERVER['REQUEST_URI'], 'basket') == 1 || parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH) == '/')
    && file_exists('system/templates/inc/experiments/async.inc')
) {
    $ADD_SCRIPTS .= file_get_contents('system/templates/inc/experiments/async.inc');
}
// ����������: �����

// ����������� CO1: ������
if ( //($__ex == 'co1' || in_array('co1=true', $__go))
    stripos($_SERVER['REQUEST_URI'], 'basket') == 1
    && file_exists('system/templates/inc/experiments/co1.inc')
) {
    // if (!in_array('co1=true', $__go)) {
    //   $__go[] = 'co1=true';
    // }
    $ADD_SCRIPTS .= "<!-- co1:start -->";
    $ADD_SCRIPTS .= file_get_contents('system/templates/inc/experiments/co1.inc');
    $ADD_SCRIPTS .= "<!-- co1:end -->";
}
// ����������� CO1: �����
//
// ����������� GB1: ������

//if ( ($__ex == 'gb1' || in_array('gb1=true', $__go))
if ( file_exists('system/templates/inc/experiments/gb1.inc')
    && ($__ex != 'gb1')
) {
    // if (!in_array('gb1=true', $__go)) {
    //   $__go[] = 'gb1=true';
    // }
    $ADD_SCRIPTS .= "<!-- gb1:start -->";
    $ADD_SCRIPTS .= file_get_contents('system/templates/inc/experiments/gb1.inc');
    $ADD_SCRIPTS .= "<!-- gb1:end -->";
}
setcookie('__go', implode('&', $__go), mktime() . time()+60*60*24*30,  '/');
// A/B-�����: �����

foreach ($bg_colors as $kc=>$value) {
    $_GLOBALS['add_styles'][] = 'body .bg_'.$value['class'].', body .bg_'.$value['class'].':hover {'.PHP_EOL.$value['bg_style'].PHP_EOL.'}'.PHP_EOL;
}
file_put_contents('css/bg.css', implode(PHP_EOL,$_GLOBALS['add_styles']));

$_GLOBALS['js_onready'][] = 'filterPrice.price_max = '.$max_price['max'];
$_GLOBALS['js_onready'][] = 'filterPrice.cur_min = '.$fps[0];
$_GLOBALS['js_onready'][] = 'filterPrice.cur_max = '.$fps[1];
$_GLOBALS['js_onready'][] = 'filterPrice.money_per_step = '.$money_step;
$_GLOBALS['js_onready'][] = 'filterPrice.reduction = "'.$cur['reduction'].'"';
$_GLOBALS['js_onready'][] = 'filterPrice.cur_value = '.$cur['value'];
$_GLOBALS['js_onready'][] = 'filterPrice.link = "'.$_GLOBALS['cfp_path'].'"';
$_GLOBALS['js_onready'][] = 'cur.symb = "' . $cur['reduction'] . '"';
$_GLOBALS['js_onready'][] = 'cur.value = '.$cur['value'];
$_GLOBALS['js_onready'][] = 'cur.reduction = "'.$cur['reduction'].'"';
$_GLOBALS['js_onready'][] = 'cur.spanReduction = "<span class=\'reduction\'>'.$cur['reduction'].'</span>"';
$_GLOBALS['js_onready'][] = 'basket.cook_opt.domain = "'._CROSS_DOMAIN.'"';

$onReady =!empty($_GLOBALS['js_onready']) ? PHP_EOL . implode(';' . PHP_EOL, $_GLOBALS['js_onready']) . ';' : '';
$onLoad =!empty($_GLOBALS['js_onload']) ? PHP_EOL . implode(';' . PHP_EOL, $_GLOBALS['js_onload']) . ';' : '';

$_GLOBALS['body_pre_close'][] = '<script type="text/javascript">' . PHP_EOL . '$(document).ready(function(){' . PHP_EOL . $onReady . PHP_EOL . '});' . PHP_EOL . '</script>';
$_GLOBALS['body_pre_close'][] = '<script type="text/javascript">' . PHP_EOL . '$(window).load(function(){' . PHP_EOL . $onLoad . PHP_EOL . '});' . PHP_EOL . '</script>';

$_GLOBALS['body_pre_close'][] = tplIncluder($_GLOBALS['pre_closes_includes']);
$_GLOBALS['after_body_includes'] = tplIncluder($_GLOBALS['after_body_includes']);
// ������� ���� expecto.me
$expectoCodeInclude = 'system/templates/inc/file/expecto.me.inc';
if (file_exists($expectoCodeInclude))
{
    #$_GLOBALS['body_pre_close'][] = file_get_contents($expectoCodeInclude); // for localhost
}

// ������� ���� getsale.io
$getsaleCodeInclude = 'system/templates/inc/file/getsale.io.inc';
if (file_exists($getsaleCodeInclude))
{
    $_GLOBALS['body_pre_close'][] = file_get_contents($getsaleCodeInclude);
}

$clearStrong_url = filter(trim($_SERVER['REQUEST_URI']), 'nohtml');
if (stripos($clearStrong_url, '/products/', 0) == 0 || stripos($clearStrong_url, '/blocks/') == 0 || stripos($clearStrong_url, '/articles/') == 0) {
   // $_GLOBALS['text'] = empty($_GLOBALS['text']) ? '' : str_replace('<strong>', '', $_GLOBALS['text']);
   // $_GLOBALS['text'] = empty($_GLOBALS['text']) ? '' : str_replace('</strong>', '', $_GLOBALS['text']);
  //  $_GLOBALS['text'] = empty($_GLOBALS['text']) ? '' : str_replace('strong', 'span', $_GLOBALS['text']);
//  $_GLOBALS['text'] = empty($_GLOBALS['text'])?'':str_replace('strong', '', $_GLOBALS['text']);
}

$html = str_replace("[%MODULE_TEXT%]", !empty($_GLOBALS['text']) ? $_GLOBALS['text'] : '', $html);
$html = str_replace("[%EMAILADMIN%]", !empty($_GLOBALS['v_email_admin']) ? $_GLOBALS['v_email_admin'] : '', $html);
$html = str_replace("[%BODY_PRE_CLOSE%]", implode(PHP_EOL,$_GLOBALS['body_pre_close']), $html);
$html = str_replace("[%ADD_SCRIPTS%]", $ADD_SCRIPTS, $html);
$html = str_replace("[%HEADER_INCLUDES%]", !empty($_GLOBALS['head_inc']) ? implode(PHP_EOL, $_GLOBALS['head_inc']) : '', $html);
$html = str_replace("[%HEADER_INCLUDES%]", !empty($_GLOBALS['head_inc']) ? implode(PHP_EOL, $_GLOBALS['head_inc']) : '', $html);
$matches = preg_split('/(<body.*?>)/i', $html, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
/* assemble the HTML output back with the iframe code in it */
$html = $matches[0] . $matches[1] . $_GLOBALS['after_body_includes'] . $matches[2];
echo $html;
