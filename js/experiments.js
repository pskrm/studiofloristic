$(document).ready(function() {
    var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    if (w >= 500) {
        $('#content .cup_block>div ').css('width', '33%');
    }
    /** ���������� */
    var msg = $("<div/>").append($('.delivery_only_by_phone').clone().css('display', 'block')).html();
    $('.by_phone_only_main label').before(msg);
    /** shipping */
    if (window.location.pathname !== "/delivery.html") {
        $('.delivery-table').hide();
        $('.sw-tab').hide();
        $('<div style="text-align:center; margin: 10px 0;"><a id="show_delivery_info_tbl">������� ������� ��������� ��������</a></div>')
            .insertAfter('.delivery_info_tbl')
            .click(function() {
                $(".delivery-table").toggle();
                $('.sw-tab').toggle();
            });        
    }

    var hideAllDelivery = function() {
        $('.delivery_info_tbl #courer').css('visibility', 'hidden');
        $('.delivery_info_tbl #metro_selector').css('visibility', 'hidden');
        $('.delivery_info_tbl #courer-okr').css('visibility', 'hidden');
        $('#del_4_cont .over_mkad_inf_cont').css('visibility', 'hidden');
    }

    hideAllDelivery();

    $('.delivery_info_tbl .check2 label').each(function() {
        var label = this;
        switch ($('input', label).val()) {
            case "1": // � �������� ����
                if ($('input', label).is(":checked")) {
                    $('.gray', label).css('visibility', 'visible');
                }
                $(label).click(function() {
                    hideAllDelivery();
                    $('.gray', label).css('visibility', 'visible');
                });
                break;
            case "5": // �� ������� �����:
                if ($('input', label).is(":checked")) {
                    $('#metro_selector', label).css('visibility', 'visible');
                }
                $(label).click(function() {
                    hideAllDelivery();
                    $('#metro_selector', label).css('visibility', 'visible');
                    $('.gray', label).css('visibility', 'visible');
                });
                break;
            case "3": // �� ���� (�� 8 ��)
                if ($('input', label).is(":checked")) {
                    $('.gray', label).css('visibility', 'visible');
                }
                $(label).click(function() {
                    hideAllDelivery();
                    $('.gray', label).css('visibility', 'visible');
                });
                break;
            case "4": // �� ���� (�� 8 ��)
                if ($('input', label).is(":checked")) {
                    $('#del_4_cont .over_mkad_inf_cont').css('visibility', 'visible');
                }
                $(label).click(function() {
                    hideAllDelivery();
                    $('#del_4_cont .over_mkad_inf_cont').css('visibility', 'visible');
                });
                break;
            case "2": // ���������
                $(label).click(function() {
                    hideAllDelivery();
                });
                break;
        }
    });

    if (w <= 500) {
        $('.payment_types>div').css('width', '100%');
        $('.payment_types>div:gt(3)').hide();
    } else {
        $('.payment_types>div').css('width', '25%');
        $('.payment_types>div:gt(7)').css('width', '25%').hide();
    }


    // $('.payment_types').append($('<div style="width: 100%"><a class="zakazButton" style="margin: 10px auto">��� ������� ������</a></div>'));
    $('.payment_types').append($('<div style="width: 100%; text-align:center; margin: 10px 0;"><a id="show_all_payments">��� ������� ������</a></div>'));

    $('.payment_types #show_all_payments').click(function() {
        $('.payment_types #show_all_payments').hide();
        $('.payment_types>div').show();
    });

    /** catalog */
    $('.catalogItem').each(function() {
        var context = this;
        $(context).on('mouseover', function() {
            $('.catalog_item_hit', context).show();
            $('.catalog_item_bouquet', context).show();
        });
        $(context).on('mouseout', function() {
            $('.catalog_item_hit', context).hide();
            $('.catalog_item_bouquet', context).hide();
        });

        if ($('.catalog_item_share', context).get(0) !== undefined) {
            $('.cost .oldCost', context).show();
            $('.cost .newCost', context).css('color', '#d10000');
        } else {
            $('.cost .newCost', context).css({
                'color': '#000',
                'width': '100%',
                'text-align': 'center'
            });
        }
    });
    /** product */
    if ($('.product .leftProd .catalog_item_share').get(0) !== undefined) {
        $('.product .kolvo .oldCost').show();
    } else {
        $('.product .kolvo .newCost').css('color', '#000');
    }
    /** quick order */
    if ($("#popup #quickOrderBlock").get(0) !== undefined) {
        var observer = new MutationObserver(function(mutations) {
            mutations.forEach(function(mutation) {
                var newNodes = mutation.addedNodes;
                if (newNodes !== null) {
                    newNodes = Array.prototype.slice.call(newNodes);
                    $(newNodes).each(function() {
                        if ($(this).hasClass('formOrder')) {
                            var context = this;
                            if ($('.item-stickers .catalog_item_share', context).get(0) !== undefined) {
                                $('.kolvo .oldCost', context).show();
                                $('.kolvo .newCost', context).css('color', '#d10000');
                            } else {
                                $('.kolvo .newCost', context).css('color', '#000');
                            }
                        }
                    });
                }
            });
        });
        observer.observe($("#popup #quickOrderBlock").get(0), {
            childList: true
        });
    }

    /** load more products */
    if ($("#content .catalog").get(0) !== undefined) {
        var observer = new MutationObserver(function(mutations) {
            mutations.forEach(function(mutation) {
                var newNodes = mutation.addedNodes;
                if (newNodes !== null) {
                    newNodes = Array.prototype.slice.call(newNodes);
                    $(newNodes).each(function() {
                        if ($(this).hasClass('catalogItem')) {
                            var context = this;
                            $(context).on('mouseover', function() {
                                $('.catalog_item_hit', context).show();
                                $('.catalog_item_bouquet', context).show();
                            });
                            $(context).on('mouseout', function() {
                                $('.catalog_item_hit', context).hide();
                                $('.catalog_item_bouquet', context).hide();
                            });

                            if ($('.catalog_item_share', context).get(0) !== undefined) {
                                $('.cost .oldCost', context).show();
                                $('.cost .newCost', context).css('color', '#d10000');
                            } else {
                                $('.cost .newCost', context).css('color', '#000');
                            }
                        }
                    });
                }
            });
        });
        observer.observe($("#content .catalog").get(0), {
            childList: true
        });
    }

    if (window.location.pathname !== '/') {
        var mainBanner = $("<div/>").append($('#baner_slider').clone()).html();
        var count = 0;
        if (w <= 500) {
            count = $('.catalog .catalogItem').length < 8 ? $('.catalog .catalogItem').length - 1 : 7;
            $('.catalog .catalogItem').eq(count).after(mainBanner);
        } else {
            count = $('.catalog .catalogItem').length < 8 ? $('.catalog .catalogItem').length - 1 : 7;
            $('.catalog .catalogItem').eq(count).after(mainBanner);
        }
        $('.ramka').attr('class', 'ramka swiper-container swiper-container-horizontal swiper-container-android').attr('style', 'margin: 0 0 15px 3px');
    } 


    $(".topMenu2 li.sub_level_1").last().find('.border-left').append('<li class="sub_level_2" zlevel="2" style="z-index: 119;"><a class="main " href="/individual-order.html" style="z-index: 122; box-shadow: none;">�������������� �����</a></li>');

    if (window.location.pathname.substring(0, 9) == '/catalog/') {

        var currency = '';
        $('#rightUh .rightMoney a').each(function(index) {
            if ($(this).hasClass('ico1active') || $(this).hasClass('ico2active') || $(this).hasClass('ico3active')) {
                currency = ['�', '$', '�.'][index];
            }
        });

        var ranges = [];
        var currentRange = '';
        $('#rightUh .lavaLamp li').each(function() {
            var range = $('a', this).attr('href').match(/[0-9]+','[0-9]+/gi) || ['0'];
            range = range[0].replace("','", '-');
            ranges.push(range);
            if ($(this).hasClass('current')) {
                currentRange = range;
            }
        });

        var newFilter = $('<div/>').attr('class', "seton_filters blockWithTopLine").attr('style', "display:block");

        if ($('#content .seton_filters').get(0) == undefined) {
            $(newFilter).append('<div><b class="text">���</b></div>');
        } else {
            $(newFilter).append('<div><a href="javascript: catalog.unset_fltr(\'price\');"><span class="text">���</span></a></div>');
        }

        $.each(ranges, function(index, value) {
            var range = value.split("-");
            var rangeText = '';
            if (range[0] == 0) {
                rangeText = '�� ' + range[1] + currency;
            } else if (range[1] == 199990) {
                rangeText = '�� ' + range[0] + currency;
            } else {
                rangeText = range[0] + '-' + range[1] + currency;
            }

            if (currentRange == value) {
                $(newFilter).append('<div><b class="text">' + rangeText + '</b></div>');
            } else {
                $(newFilter).append('<div><a href="javascript: void(0);" class="text"><span class="text">' + rangeText + '</span></a></div>')
                    .find('a')
                    .last()
                    .one('click', function() {
                        filterPrice.setPriceRange(range[0], range[1]);
                        setTimeout(filterPrice.seeResults, 500);
                        return false;
                    });

                //
                // $(newFilter).append('<div class="floatLeft"><a href="javascript: return false"><span class="text">'+ rangeText +'</span></a></div>')
                //   find('').on('one', function(){
                //     filterPrice.setPriceRange('+range[0]+','+range[1]+');
                //     void(0); setTimeout(filterPrice.seeResults, 500);
                //   });


            }
        });

        $('#content .seton_filters').remove();
        $(newFilter).insertBefore('#content-data .catalog');

    }
});