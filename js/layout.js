$.fn.addUgToActiveTopLink = function() {
    $(this).append("<span>&nbsp;</span>")
};
$.fn.createTop2Menu = function() {
    $(this).find("div").each(function() {
        var i = $(this).find("a").size();
        if (i > 0) {
            var c = $(this).find("a");
            var f = c.length;
            var d = Math.floor(c.length / 2) + 1;
            var a = Math.floor(c.length / 2);
            if (Math.floor(c.length / 2) * 2 == f) {
                d = Math.floor(c.length / 2);
                a = Math.floor(c.length / 2) - 1
            }
            $(this).find("a:lt(" + d + ")").wrapAll("<b>");
            $(this).find("a:gt(" + a + ")").wrapAll("<b>");
            var e = $(this).find("b:first").outerWidth();
            var b = $(this).find("b:last").outerWidth();
            $(this).width(e + b + 2);
            var h = $(this).find("b:first").height();
            var g = $(this).find("b:last").height();
            if (h > g) {
                $(this).find("b:last").height(h)
            }
        }
        $(this).hide()
    })
};
$.fn.showDarkBg = function(c, b) {
    var a = $(window).width();
    var d = $("body").height();
    if (a < 1000) {
        a = 1000
    }
    $(this).width(a).height(d);
    if (!b) {
        b = 0
    }
    if (c == "show") {
        $(this).show(b)
    }
    if (c == "hide") {
        $(this).hide(b)
    }
};
$.fn.closePopup = function(b, a) {
    $("#darkBack").click(function() {
        if ($(b).css("display") == "block") {
            $("#darkBack").showDarkBg("hide", 0);
            if (a == 0) {
                $(b).remove()
            } else {
                $(b).hide()
            }
        }
    })
};
$.fn.findMaxHeight = function() {
    var a = 0;
    $(this).each(function() {
        var b = $(this).height();
        if (b > a) {
            a = b
        }
    });
    $(this).each(function() {
        var b = a - $(this).height();
        $(this).css("padding-top", b)
    })
};
$.fn.calculatePositionCenter = function() {
    var e = $(window).width();
    var d = $(window).height();
    var c = $(this).outerWidth(true);
    var a = $(this).outerHeight(true);
    var b = (d - a) / 2 + $(document).scrollTop();
    if (b < 10) {
        b = 10
    }
    $(this).css({
        top: b,
        left: (e - c) / 2
    })
};
function link_phone() {
    if(window.innerWidth >= 481){
        $('.link-phone').addClass('link-phone-desabled');
    } else {
        $('.link-phone').removeClass('link-phone-desabled');
    }
    $('.link-phone-desabled').on('click', function() {
        return false;
    });
}
$(window).resize(function() {
    if ($("#darkBack").css("display") == "block") {
        $("#darkBack").showDarkBg("show", 0)
    }
    $("body > #popup > div").calculatePositionCenter();
    lrSideContPos();
    windows_info_repos();
    link_phone();
});
$(window).scroll(function() {
    if ($(".banners_cont").length > 0) {
        if ($(window).scrollTop() >= 450) {
            $(".banners_cont").addClass("fixed")
        } else {
            $(".banners_cont").removeClass("fixed")
        }
    }
    if ($(window).scrollTop() >= 100 && $("#slideTop").css("display") != "block") {
        $("#slideTop").css("opacity", "0.5").show()
    }
    if ($(window).scrollTop() >= 300 && $("#slideTop").css("display") == "block") {
        $("#slideTop").css("opacity", "1")
    }
    if ($(window).scrollTop() >= 550 && $(window).scrollTop() < 650) {
        $("#slideTop").css("opacity", "0.5")
    }
    if ($(window).scrollTop() >= 0 && $(window).scrollTop() < 550) {
        $("#slideTop").hide()
    }
});