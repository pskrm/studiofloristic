
var colors = {
    set: function (id, cid) {

        if (jQuery('span#color-list-' + id + ' a#c' + cid).hasClass('active')) {
            jQuery('span#color-list-' + id + ' a#c' + cid).removeClass('active');
            jQuery('span#color-list-' + id + ' a#c' + cid).children('input').prop('checked', false);
            jQuery('#size-data').trigger('click');
            jQuery('#size-data').click();

        } else {
            jQuery('span#color-list-' + id + ' a#c' + cid).addClass('active');
            jQuery('span#color-list-' + id + ' a#c' + cid).blur();
            jQuery('span#color-list-' + id + ' a#c' + cid).children('input').prop('checked', true);
            jQuery('body').trigger('click');
        }
    }
};

var lSideContPos = 0;
var rSideContPos = 0;
var deliveryCalc = {
    dcal: null,
    way: null,
    day: null,
    prc: null,
    tm: null,
    stm: null,
    sway: null,
    sday: null,
    inTime: null,
    init: function (win) {
        $('#' + win).length <= 0 ? initGET('/catalog/op=delinf') : windows_info(win, 'closeNew');
    },
    initVars: function () {
        this.dcal = $('.delcalc_text_info');
        this.way = this.dcal.find('select[name="way"]');
        this.day = this.dcal.find('select[name="day"]');
        this.tm = this.dcal.find('select[name="time"]');
        this.inTime = this.dcal.find('input[name="inTime"]');
        this.setSels();
    },
    setSels: function () {
        this.sway = this.way.find('option:selected');
        this.sday = this.day.find('option:selected');
        this.stm = this.tm.find('option:selected');
        this.dcal.find('.hday-informer > a').attr('class', this.sday.val());
        var top = this.sday.val() + this.sway.val();
        this.dcal.find('.toPrice').html(this.sway.data(top) + cur.symb);
        this.prc = this.dcal.find('.radio.' + this.sway.val() + ' input[name="price"]' + (this.sway.val() !== 'omkad' ? ':checked' : ''));
        this.sway.val() !== 'omkad' ? this.dcal.find('.radios').removeClass('omkad') : this.dcal.find('.radios').addClass('omkad');
        var clss = this.sway.val() + this.sday.val() + this.prc.data('radioset');
        this.tm.find('option').hide().filter('.' + clss).show();
        if (!this.stm.hasClass(clss)) {
            clss = this.tm.find('option[class="' + clss + '"]').length > 0 ? '[class="' + clss + '"]' : '.' + clss + ':eq(3)';
            this.tm.find('option' + clss).prop('selected', true);
            this.stm = this.tm.find('option:selected');
        }
        this.setPrc();
    },
    free: function () {
        if (this.sway.val() === 'omkad') {
            this.way.find('option[value="mkad"]').prop('selected', true);
            this.sway = this.way.find('option:selected');
        }
        if (this.prc.data('radioset') !== 'hip') {
            this.dcal.find('.radio.mkad input[value="hip"]').prop('checked', true);
            this.prc = this.dcal.find('.' + this.sway.val() + ' input[name="price"]:checked');
        }
        this.tm.find('option[class="' + this.sway.val() + this.sday.val() + this.prc.data('radioset') + '"]').prop('selected', true);
        this.stm = this.tm.find('option:selected');
        this.inTime.prop('checked', false);
        this.setSels();
    },
    setPrc: function () {
        var sprice = this.stm.data(this.sday.val() + this.sway.val()) + (this.inTime.prop('checked') ? this.inTime.data('price') : 0);
        sprice = sprice + (this.prc.data('km') * this.prc.data('km-price'));
        sprice > 0 ? $('.del-price .nopay').show() : $('.del-price .nopay').hide();
        sprice > 0 ? $('.del-price .free-text').hide() : $('.del-price .free-text').show();
        sprice > 0 ? this.dcal.find('span.is-free').hide() : this.dcal.find('span.is-free').show();
        $('.del-price .pay_info').hide();
        if (sprice == 0) {
            $('.del-price .pay_info.' + this.sday.val() + '_' + this.sway.val()).show();
        }
        this.dcal.find('span.pay').html(sprice + cur.spanSymb());
    },
    oMkad: function (opt) {
        var val = this.prc.data('km');
        switch (opt) {
            case false:
            case true:
                val = (opt === true) ? val + 1 : val - 1;
                val = val < 0 ? 0 : val;
                this.prc.data('km', val).val(val + this.prc.data('start-omkad'));
                break;
            default:
                val = $.isNumeric(this.prc.val()) ? parseInt(this.prc.val()) - this.prc.data('start-omkad') : val;
                val = val < 0 ? 0 : val;
                this.prc.data('km', val);
                if (opt !== 'kup') {
                    this.prc.val(val + this.prc.data('start-omkad'));
                }
        }
        this.setPrc();
    }
};
var instPhotos = {
    init: function (cid) {
        initGET('/catalog/op=instagram&cid=' + cid);
    }, carusel: function () {
        $('#inst_photos iframe').load(function () {
            $('#inst_photos').slideDown();
            jQuery("#instagram_carusel").jcarousel({wrap: null, scroll: 1, animation: "slow"});
        });
    }
};
var charityPhoto = {
    initCarusel: function (id) {
        jQuery(id).jcarousel({wrap: null, scroll: 1, animation: "slow"});
        $('.window_info .jcarousel-prev, .window_info .jcarousel-next').css('top', 36.5);
    }
};
var miniCart = {
    total_count: 0,
    total_amount: 0,
    total_markup: 0,
    total_discount: 0,
    total_mark_disc: 0,
    reloadMC: function () {
        initGET('/basket/op=refresh');
    },
    itemsRefresh: function () {
        miniCart.total_count = 0;
        miniCart.total_amount = 0;
        $('.mc_item_prop').each(function () {
            var id = $(this).val();
            var count = $(this).data('mc_count');
            var amount = $(this).data('mc_amount');
            var markup = $(this).data('mc_markup');
            if ($(this).data('mc_disc') == 1) {
                $('#discInfo_inBok').show();
            }
            var markup_amount = Math.ceil((1 + markup / 100) * amount);
            miniCart.total_count = miniCart.total_count + count;
            miniCart.total_amount = miniCart.total_amount + (count * amount);
            $('#mc_amount_text_' + id).html(amount);
            $('#mc_item_cont_' + id + ' div.cost span.cost_val').html(count).parent().removeAttr('class').addClass('count_item_' + count);
            $('#mc_item_cont_' + id + ' div.cost span.count_amount_price').html(count * amount).parent().removeAttr('class').addClass('count_item_' + count);
            if ($('#markup_amount_' + id).length > 0) {
                $('#markup_amount_' + id).html(markup_amount + cur.spanReduction);
            }
        });
        var empt = $('.emptyCart');
        if (miniCart.total_count > 0) {
            var _elClass = 'someInCart_1';
        } else {
            var _elClass = 'someInCart_0';
        }
        $('.minicart_text > div, #popupNowInCart').attr('class', _elClass);
        empt.addClass('emptyCart');
        $('#nowInCart').html(strItems(miniCart.total_count, '�����', '��', '�'));
        $('#count-items').html($('#nowInCart').html());
        $('#itog').html(miniCart.total_amount);
    },
    calcTotal: function (disc) {
        miniCart.total_count = 0;
        miniCart.total_amount = 0;
        miniCart.total_markup = 0;
        miniCart.total_discount = 0;
        miniCart.total_mark_disc = 0;
        if (disc !== undefined && !isNaN(disc) && parseInt(disc) > 0) {
            disc = parseInt(disc);
        } else {
            disc = 0;
        }
        $('.mc_item_prop').each(function () {
            var count = $(this).data('mc_count');
            var amount = $(this).data('mc_amount');
            var markup = $(this).data('mc_markup');
            var disc_onoff = $(this).data('mc_disc');
            if (disc_onoff == 1 && disc > 0) {
                $('#discInfo_inBok').show();
            }
            var markup_amount = Math.round((1 + markup / 100) * amount);
            var disc_amount = Math.round(amount * (1 - disc * disc_onoff / 100));
            var mark_disc_amount = Math.round(markup_amount * (1 - disc * disc_onoff / 100));
            miniCart.total_count = miniCart.total_count + count;
            miniCart.total_amount = miniCart.total_amount + (count * amount);
            miniCart.total_markup = miniCart.total_markup + (count * markup_amount);
            miniCart.total_discount = miniCart.total_discount + (count * disc_amount);
            miniCart.total_mark_disc = miniCart.total_mark_disc + (count * mark_disc_amount);
        });
    },
    hoverRefresh: function () {
    }
};
var basket = {
    mkad_mkad: 0,
    wayTrafic: 0,
    start_over: 1,
    max_disc: 0,
    start_disc: 0,
    cur_disc: 0,
    added_price: 0,
    cook_opt: {path: '/basket/', domain: window.location.hostname},
    insert: function (id, sid, pid) {
        if (jQuery('input[name="colors_id_' + id + '[]"]').length > 0) {
            if (jQuery('input[name="colors_id_' + id + '[]"]:checked').length > 0) {
                var color = [];
                jQuery('input[name="colors_id_' + id + '[]"]:checked').each(function () {
                    color.push($(this).val());
                });
                color = color.join('-');
            } else {
                initSysDialog('�������� ���� ������');
                return;
            }
        } else {
            var color = '0';
        }
        if (jQuery('input[name="pid"]:checked').length > 0 && parseInt(pid) === 0) {
            pid = parseInt(jQuery('input[name="pid"]:checked').val());
        }
        var count = parseInt(jQuery('#item-count-' + id).val());
        initGET('/basket/op=insert&id=' + id + '&sid=' + sid + '&pid=' + pid + '&color=' + color + '&count=' + count);
    },
    mkadOver: function (opt) {
        if ($('input[name=mkad_pass_lenght]').prop('disabled') === false) {
            var val = parseInt($('input[name=mkad_pass_lenght]').val());
            switch (opt) {
                case false:
                case true:
                    if (!isNaN(val)) {
                        val = (opt === true) ? val + 1 : val - 1;
                    }
                    val = (isNaN(val) || val < basket.start_over) ? basket.start_over : val;
                    $('input[name=mkad_pass_lenght]').val(val).trigger('change');
                    break;
                default:
                    if (opt !== 'kup' && (isNaN(val) || val < basket.start_over)) {
                        val = basket.start_over;
                        $('input[name=mkad_pass_lenght]').val(val).trigger('change');
                        break;
                    }
                    if (!isNaN(val)) {
                        var len = val < basket.start_over ? 0 : val - basket.start_over;
                        var clean = basket.wayTrafic * len;
                        $('#pop_calc').html(' (' + val + '-' + basket.start_over + ')*' + basket.wayTrafic);
                        $('#pop_calc_res').html(clean + cur.reduction);
                        var totp_val = clean + basket.mkad_mkad;
                        $('#del_4').attr('totp_value', totp_val);
                        var del_price = totp_val + (($('input[name=time]').val() == '') ? 0 : parseInt($('input[name=time]').attr('totp_value')));
                        $('#mkad-over').html('<b>' + del_price + cur.reduction + '</b>');
                    }
            }
        }
        basket.totPrice();
    },
    totPrice: function () {
        basket.added_price = 0;
        basket.cur_disc = basket.start_disc;
        $('.totPrice').each(function () {
            var value = 0;
            switch ($(this).attr('type')) {
                case'checkbox':
                    value = ($(this).prop('checked')) ? parseInt($(this).attr('totp_value')) : 0;
                    break;
                case'radio':
                    value = ($(this).prop('checked')) ? parseInt($(this).attr('totp_value')) : 0;
                    break;
                case'text':
                    value = ($(this).val() !== '') ? parseInt($(this).attr('totp_value')) : 0;
                    break;
                default:
                    value = (parseInt($(this).attr('totp_value')) !== NaN) ? parseInt($(this).attr('totp_value')) : 0;
            }
            if ($(this).attr('name') == 'time' && $('input[name="delivery"]:checked').val() == 2) {
                value = 0;
            }
            if ($(this).attr('name') == 'time' && $('input[name="delivery"]:checked').val() == 5) {
                value = 0;
            }
            switch ($(this).attr('totp_action')) {
                case'disc':
                    basket.cur_disc = basket.cur_disc + value;
                    break;
                case'add':
                    basket.added_price = basket.added_price + value;
                    break;
            }
        });
        if (basket.cur_disc > basket.max_disc) {
            basket.cur_disc = basket.max_disc;
        }
        basket.added_price > 0 ? $('#with_adds').show() : $('#with_adds').hide();
        miniCart.calcTotal(basket.cur_disc);
        var tot_price = miniCart.total_discount + basket.added_price;
        var tot_holl = miniCart.total_mark_disc + basket.added_price;
        jQuery.ajaxSetup({async: false});
        if ($('#itog-items').length > 0) {
            $('#itog-items').html(tot_price.formatMoney(0, 3, ' ', '.') + cur.spanReduction);
        }
        if ($('#itog2-items').length > 0) {
            $('#itog2-items').html(tot_holl.formatMoney(0, 3, ' ', '.') + cur.spanReduction);
        }
        $('#cur_disc_text').html(basket.cur_disc + '%');
    },
    fieldsSet: function () {
        $('#basket-issue, #additional-fields').find('input[type="text"], textarea, input[type=checkbox], input[type=radio], select').each(function () {
            $(this).on('change keyup blur click', function () {
                if ($(this).attr('type') == 'text' || $(this).prop('tagName') == 'TEXTAREA') {
                    $.cookie('issue_' + $(this).attr('name'), $(this).val(), basket.cook_opt);
                }
                if ($(this).attr('type') == 'checkbox') {
                    $.cookie('issue_' + $(this).attr('name'), ($(this).prop('checked') === true ? 1 : 0), basket.cook_opt);
                }
                if ($(this).attr('type') == 'radio') {
                    var name = $(this).attr('name');
                    if ($('input[name=' + name + ']:checked').length > 0) {
                        var val = $('input[name=' + name + ']:checked').val();
                        if (name == 'delivery') {
                            val = parseInt(val);
                            if (val == 4) {
                                val = val + parseInt($('input[name=mkad_pass_lenght]').val());
                            }
                            if (val == 5) {
                                $.cookie('metro_deliver', $("#mselect option:selected").val(), basket.cook_opt);
                            }
                        }
                        $.cookie('issue_' + name, val, basket.cook_opt);
                    }
                }
            });
        });
    },
    set: function (id, task) {
        $('.gift_reload').addClass('loading');
        var value = parseInt(jQuery('#' + id).val());
        switch (task) {
            case'-':
                if (value > 1 && value !== NaN) {
                    value -= 1;
                    jQuery('#' + id).val(value);
                }
                break;
            case'+':
                if (value !== NaN) {
                    value += 1;
                    jQuery('#' + id).val(value);
                }
                break;
        }
        if (value > 0 && value !== NaN) {
            $('#minicart_item_' + id).data('mc_count', value);
            miniCart.itemsRefresh();
            basket.totPrice();
            initGET('/basket/op=set&id=' + id + '&count=' + value);
            miniCart.itemsRefresh();
        }
    },
    basketGoon: function (stat) {
        var val = $('input[name="goon"]:checked').val();
        switch (val) {
            case'reg':
                login.registration();
                moveToDom('popupLogin');
                break;
            case'login':
                login.entr();
                moveToDom('popupLogin');
                break;
            default:
                if (stat === 'go') {
                    document.location = '/basket/op=issue';
                }
        }
    },
    by_phone_only: function () {
        if ($('input[name="by_phone_only"]').is(':checked')) {
            $('.delivery_obp_inputs .bigInput').slideDown(200);
            $('#recipient-block .regBlock input').prop('disabled', true).addClass('noActiveInput');
            $('#recipient-block .regBlock textarea').prop('disabled', true).addClass('noActiveInput');
            $('#recipient-block .regBlock input[name="recipient-phone"]').prop('disabled', false).removeClass('noActiveInput');
            $('#recipient-block .regBlock input[name="recipient-name"]').prop('disabled', false).removeClass('noActiveInput');
            $('#recipient-block .regBlock input[name="by_phone_only"]').prop('disabled', false).removeClass('noActiveInput');

        } else {
            $('.delivery_obp_inputs .bigInput').slideUp(200);
            $('#recipient-block .regBlock input').prop('disabled', false).removeClass('noActiveInput');
            $('#recipient-block .regBlock textarea').prop('disabled', false).removeClass('noActiveInput');
        }
    },
    proceed: function () {
        jQuery('#darkBack').fadeOut(300);
        jQuery('div#popupOrder').fadeOut(300, function () {
            jQuery('div#popupOrder').remove();
        });
    },
    courer: function (t) {
        if (t.value !== '__:__' && t.value !== '') {
            // ���������� �������� ��� �����
            var d = new Date();
            var setDateTime = function(date, str){
                var sp = str.split(':');
                date.setHours(parseInt(sp[0],10));
                date.setMinutes(parseInt(sp[1],10));
                date.setSeconds(parseInt(sp[2],10));
                return date;
            }

            metroDate = setDateTime(d, t.value + ":00");
            start = setDateTime(new Date(), '08:00:00');
            end = setDateTime(new Date(), '23:59:00');
            if (metroDate.getTime() < start.getTime() || metroDate.getTime() > end.getTime() ){
                // � ��� ����� �� ��������, �� � ���� �� ������ ���� :)
                $("#del_5_cont").css("opacity", "0.4");
                $("#del_5_cont").css("pointer-events", "none");
                $("#del_5_cont").css("color", "red");
                if ($('#del_5').is(':checked')) {
                  //  $("#del_1").prop("checked", true);
                    $("#metrotime").text(t.value);
                    windows_info('info_metro_not_working','close_del_info');
                    $("#del_1").trigger('click');
                }
            }else{
                $("#del_5_cont").css("opacity", "");
                $("#del_5_cont").css("pointer-events", "");
                $("#del_5_cont").css("color", "");
            }

            jQuery('span#courer').html('��������...');
            jQuery('span#courer-okr').html('��������...');
            $.cookie('issue_time', t.value, basket.cook_opt);
            $.cookie('issue_date', jQuery('input.datepicker').val(), basket.cook_opt);
            initGET('/basket/op=courer&time=' + t.value + '&date=' + jQuery('input.datepicker').val());
        } else jQuery("select[name='interval']").trigger("change");
    },
    interval: function () {
        var i = $('select[name="interval"] option:selected');
        var d = $('input[name=date].datepicker');
        $('span#courer, span#courer-okr').html('��������...');
        $('input[name=time]').val('');
        if ((parseInt(i.val()) == 7) || (parseInt(i.val()) == 8)) {
            $("#del_5_cont").css("opacity", "0.4");
            $("#del_5_cont").css("pointer-events", "none");
            $("#del_5_cont").css("color", "red");
            if ($('#del_5').is(':checked')) {
                //$("#del_1").prop("checked", true);
                $("#metrotime").text(i.text());
                windows_info('info_metro_not_working','close_del_info');
                $("#del_1").trigger('click');
            }
        } else {
            $("#del_5_cont").css("opacity", "");
            $("#del_5_cont").css("pointer-events", "");
            $("#del_5_cont").css("color", "");
        }
        $.cookie('issue_time', '', basket.cook_opt);
        $.cookie('issue_interval', i.val(), basket.cook_opt);
        $.cookie('issue_date', d.val(), basket.cook_opt);
        initGET('/basket/op=interval&id=' + i.val() + '&date=' + d.val());
    },
    interval2: function () {
        var i = $('select[name="interval"] option:selected');
        var d = $('input[name=date].datepicker');
        $('span#courer, span#courer-okr').html('��������...');
        $('input[name=time]').val('');
        $.cookie('issue_time', '', basket.cook_opt);
        $.cookie('issue_interval', i.val(), basket.cook_opt);
        $.cookie('issue_date', d.val(), basket.cook_opt);
        initGET('/basket/op=interval&id=' + i.val() + '&date=' + d.val());
    },
    delivery: function (c) {
        var val = $(c).val();
        $('.del_price').removeClass('totPrice');
        $('#delivery_' + val).addClass('totPrice');
        basket.totPrice();
    },
    noDelivery: function () {
        initSysDialog('� ��� ����� �������� �� ��������');
        jQuery("select[name='interval']").trigger("change")
        jQuery('form#basket-issue input[name$="time"]').val(null).select();
    },
    result: function (r) {
        switch (r) {
            case 1:
                initSysDialog('������ ������');
                break;
            case 2:
                initSysDialog('���� ���������� * ����������� � ����������!');
                break;
            case 3:
                initSysDialog('�� �� ������� ������ ������');
                break;
            case 4:
                initSysDialog('��������� ��� ���� ����� ���������');
                break;
            case 5:
                initSysDialog('��������� �� ��� ������������ ���� ����� ������������');
                break;
        }
    },
    payment: function (t) {
        if (jQuery(t).attr("data-type") == 'courier') {
            jQuery('div#additional-fields').show(250);
        } else {
            jQuery('div#additional-fields').hide(250);
        }
        basket.totPrice();
    },
    recipient: function (c) {
        if ($(c).prop('checked')) {
            jQuery('div#recipient-block').addClass('noActiveBlock');
            jQuery('div#recipient-block input').attr('disabled', 'disabled');
            jQuery('div#recipient-block textarea').attr('disabled', 'disabled');
            jQuery('div#me_address').show();
        } else {
            jQuery('div#recipient-block').removeClass('noActiveBlock');
            jQuery('div#recipient-block input').removeAttr('disabled');
            jQuery('div#recipient-block textarea').removeAttr('disabled');
            jQuery('div#me_address').hide();
        }
    },
    robo: function () {
        initGET('/basket/op=robo');
    },
    box: function (c, b) {
        basket.setAmount(c);
        $('.gift_reload').addClass('loading');
        initGET('/basket/op=box&c=' + c + '&b=' + b.value);
    },
    vase: function (c) {
        basket.setAmount(c);
        var b = ($('input[name="vase-' + c + '"]').prop('checked')) ? 1 : 0;
        $('.gift_reload').addClass('loading');
        initGET('/basket/op=vase&c=' + c + '&b=' + b);
    },
    vasenew: function (c) {
        var b = ($('input[name="vase-' + c + '"]').prop('checked')) ? 1 : 0;
        if (b == 1) {
            $(".vases-block-" + c).show();
        } else {
            $('#vase-result-' + c).html("");
            $(".vases-block-" + c).hide();
            basket.vaseremove(c, $('#vase-result-' + c).prop('data-mc_amount_add'));
        }
    },
    vaseappend: function (c, price, id) {
        var b = $('#vase-result-' + c);
        b.html("(+ " + price + cur.reduction + ")");
        b.prop('data-mc_amount_add', price);
        basket.setAnotherAmount(c, price);
        $('.gift_reload').addClass('loading');
        initGET('/basket/op=vase2&c=' + c + '&b=1&vaseid=' + id + '&vaseprice=' + price);
        $(".vases-block-" + c).hide();
    },
    vaseremove: function (c, price) {
        $('.gift_reload').addClass('loading');
        initGET('/basket/op=vase3&c=' + c + '&b=1&vaseprice=' + price);
        basket.removeAnotherAmount(c, price);
        $(".vases-block-" + c).hide();
    },
    setAmount: function (c) {
        var amount = 0;
        if ($('input[name="box-' + c + '"]:checked').length === 1) {
            amount = amount + $('input[name="box-' + c + '"]:checked').data('mc_amount');
        }
        if ($('input[name="vase-' + c + '"]').prop('checked')) {
            amount = amount + $('input[name="vase-' + c + '"]').data('mc_amount_add');
        }
        $('#minicart_item_' + c).data('mc_amount', amount);
        $('#price-item-' + c).html(amount);
        miniCart.itemsRefresh();
        basket.totPrice();
    },
    setAnotherAmount: function (c, price) {
        var amount = 0;
        if ($('input[name="box-' + c + '"]:checked').length === 1) {
            amount = amount + $('input[name="box-' + c + '"]:checked').data('mc_amount');
        }
        amount = amount + price
        $('#minicart_item_' + c).data('mc_amount', amount);
        $('#price-item-' + c).html(amount);
        miniCart.itemsRefresh();
        basket.totPrice();
    },
    removeAnotherAmount: function (c, price) {
        var amount = 0;
        if ($('input[name="box-' + c + '"]:checked').length === 1) {
            amount = amount + $('input[name="box-' + c + '"]:checked').data('mc_amount');
        }
        $('#minicart_item_' + c).data('mc_amount', amount);
        $('#price-item-' + c).html(amount);
        miniCart.itemsRefresh();
        basket.totPrice();
    },
    remove: function (c) {
        $('#item-' + c).slideUp(400);
        $('.gift_reload').addClass('loading');
        initGET('/basket/op=remove&id=' + c);
    }
};
var catalog = {
    set_ppage: function (ppage) {
        formSubmit('<form method="POST" id="set_per_page" action="' + location.href + '"><input type="hidden" name="per_page" value="' + ppage + '"></form>');
    }, unset_fltr: function (filt) {
        formSubmit('<form method="POST" id="unset_cf" action="' + location.href + '"><input type="hidden" name="unset_fltr" value="' + filt + '"></form>');
    }, set_otype: function (order) {
        formSubmit('<form method="POST" id="unset_cf" action="' + location.href + '"><input type="hidden" name="order_type" value="' + order + '"></form>');
    }, set_fltrs: function (id) {
        $('#' + id).submit();
    }, mfltr: function (el, id) {
        var ul_cont = $(el).parent().parent();
        ul_cont.find('ul.mlist>li').removeClass('show');
        $(el).addClass('show');
        ul_cont.find('ul[class^="wall_"]').removeClass('show');
        ul_cont.find('ul.wall_' + id).addClass('show');
        ul_cont.find('ul[class^="slist_"]').removeClass('show');
        ul_cont.find('ul.slist_' + id).addClass('show');
    }, fltr_check: function (el, type) {
        var ul_cont = $(el).parent().parent();
        var ch = ul_cont.find('input[type="checkbox"]');
        if (type === false) {
            ul_cont.removeClass('checked');
            ch.prop('checked', false);
        } else {
            if ($(el).prev('input[type="checkbox"]').length > 0) {
                if (ch.prop('checked')) {
                    ch.prop('checked', false);
                    ul_cont.removeClass('checked');
                } else {
                    ch.prop('checked', true);
                }
            }
            var pre_set = ch.prop('checked') ? 'pre_set' : '';
            ul_cont.removeClass('pre_set').addClass(pre_set);
        }
    }
};
var sf_ga = {
    sub_form: '', transaction: {}, products: [], init: function (form) {
        try {
            this.sub_form = $(form);
            var pageTracker = _gat._getTracker("UA-16178506-1");
            pageTracker._trackPageview();
            var gt = this.transaction;
            pageTracker._addTrans(gt.oid, gt.store, gt.total, gt.tax, gt.ship);
            $.each(this.products, function (i, e) {
                pageTracker._addItem(e.oid, e.sku, e.name, e.category, e.price, e.count, e.category);
            });
            pageTracker._trackTrans();
            miniCart.reloadMC();
        } catch (err) {
        } finally {
            this.sub();
        }
    }, sub: function () {
        if ($(this.sub_form).length > 0) {
            $(this.sub_form).submit();
        }
    }
};
var fastsearch = {
    closepop: '', init: function () {
        $('div.fast_search_popup a.fastsearch_fancybox').fancybox({overlayColor: "#000", overlayOpacity: "0.5"});
        $('.fast_search_popup td.description>div>a>span').hide();
        $('.fast_search_popup td.description>div>a>span').each(function (i) {
            $(this).show();
            $(this).css("display", "inline-block");
            if ($('div.fast_search_popup').width() > 480) {
                $(this).before('<br>');
            }
            if (i >= $('.fast_search_popup td.description>div>a>span').length - 1) {
                $('.fast_search_popup').show();
            }
        });
    }, goSearch: function (id) {
        var val = $(id).val().toString();
        if (val.length > 2 || $.isNumeric(val)) {

            initGET('/search/op=fsearch&query=' + val);
        }
    }, closeList: function (id) {
        fastsearch.closepop = $(id);
        fastsearch.closepop.animate({opacity: 0}, {
            complete: function () {
                fastsearch.closepop.remove();
            }
        });
    }, showConsist: function () {
        var words = $('div.find input[name="query"]');
        $.each(words,function(){
            if ($(this).val() != '') {
                location.href = '/?m=search&query=' + $(this).val();

                return true;
            }
        });



    }, hoverHolly: function (dom, task) {
        dom = $(dom).next('div');
        if (task === true) {
            dom.show();
        } else {
            dom.hide();
        }
    }
};
var dimageShow = {
    remClass: '', showClass: '', showItemClass: '.dimage-show-item', init: function (el, rem) {
        dimageShow.showClass = el;
        dimageShow.remClass = rem;
        dimageShow.scroll();
    }, imgShow: function (el) {
        if (el.attr('src') == undefined) {
            el.attr('src', el.attr('dimage_src')).load(function () {
                $(this).animate({opacity: 1}, 500);
                $(this).prev('div.loading-dot').animate({opacity: 0}, 500);
            });
        }
    }, scroll: function () {
        $(dimageShow.showClass).each(function () {
            $(this).removeClass(dimageShow.remClass);
            $(this).find(dimageShow.showItemClass).each(function () {
                dimageShow.imgShow($(this));
            });
        });
    }
};
var breadcrumbs = {
    link: '', showPopup: function (id, parent) {
    }, checkItem: function (id, set) {
        if (set === false) {
            $('#check_' + id).prop('checked', false);
        }
    }
};
var filterPrice = {
    price_min: 0,
    price_max: 1000000,
    cur_min: 0,
    cur_max: 1000000,
    cur_value: 1,
    min_max_ret: 17,
    money_per_step: 100,
    min_slider_step: 36 / 1000,
    slider_step: 36 * 100 / 1000,
    top_2k: 27,
    top_7k: 207,
    top_over7k: 235,
    reduction: '',
    min_sl: '',
    max_sl: '',
    brdg: '',
    popup: '',
    link: '/',
    preSend: false,
    init: function () {
        filterPrice.slider_step = filterPrice.min_slider_step * filterPrice.money_per_step, filterPrice.min_sl = $('#top_price_slider');
        filterPrice.max_sl = $('#bottom_price_slider');
        filterPrice.brdg = $('#sliders_bridge');
        filterPrice.popup = $('#fp_popup_info');
        filterPrice.min_sl.hover(function () {
            if ($('#fp_popup_info').hasClass('show_fp')) {
                filterPrice.showPopup(false);
            }
        }, function () {
            if ($('#fp_popup_info').hasClass('show_fp')) {
                filterPrice.showPopup(true);
            }
        });
        filterPrice.max_sl.hover(function () {
            if ($('#fp_popup_info').hasClass('show_fp')) {
                filterPrice.showPopup(false);
            }
        }, function () {
            if ($('#fp_popup_info').hasClass('show_fp')) {
                filterPrice.showPopup(true);
            }
        });
        filterPrice.min_sl.draggable({
            axis: "y", containment: "parent", start: function () {
                $('#fp_popup_info').addClass('show_fp');
                filterPrice.showPopup(false);
            }, drag: function (e, ui) {
                filterPrice.popupPrice(ui.position.top, true);
                filterPrice.fixPos(ui.position.top, true, false);
            }, stop: function (e, ui) {
                if (e.type === 'dragstop') {
                    filterPrice.fixPos(ui.position.top, true, true);
                    filterPrice.showPopup(true);
                }
            }
        });
        filterPrice.max_sl.draggable({
            axis: "y", containment: "parent", start: function () {
                $('#fp_popup_info').addClass('show_fp');
                filterPrice.showPopup(false);
            }, drag: function (e, ui) {
                filterPrice.popupPrice(ui.position.top, false);
                filterPrice.fixPos(ui.position.top, false, false);
            }, stop: function (e, ui) {
                if (e.type === 'dragstop') {
                    filterPrice.fixPos(ui.position.top, false, true);
                    filterPrice.showPopup(true);
                }
            }
        });
        filterPrice.resizeBrdg();
        filterPrice.popup.find('#fppi_results').attr('action_time', 1);
    },
    fixPos: function (top, rout, stop) {
        var top_min = 0;
        var top_max = 0;
        var cur_top = 0;
        if (rout === true && stop === false) {
            top_max = filterPrice.max_sl.position().top;
            if ((top_max - top) < 21.6) {
                cur_top = top + 21.6 < filterPrice.top_7k ? top + 21.6 : filterPrice.top_over7k;
                filterPrice.popupPrice(cur_top, false);
                filterPrice.max_sl.css('top', cur_top);
            }
        }
        if (rout === true && stop === true) {
            top = (top >= (filterPrice.top_2k - filterPrice.min_max_ret) && top < filterPrice.top_2k) ? filterPrice.top_2k : top;
            top_min = filterPrice.fixCurPos(0, filterPrice.top_7k, top);
            top_max = filterPrice.max_sl.position().top;
            top_max = filterPrice.fixCurPos((top_min + 21.6), filterPrice.top_over7k, top_max);
        }
        if (rout === false && stop === false) {
            top_min = filterPrice.min_sl.position().top;
            if ((top - top_min) < 21.6) {
                cur_top = top - 21.6 > filterPrice.top_2k ? top - 21.6 : 0;
                filterPrice.popupPrice(cur_top, true);
                filterPrice.min_sl.css('top', cur_top);
            }
        }
        if (rout === false && stop === true) {
            top = (top <= (filterPrice.top_over7k - filterPrice.min_max_ret) && top > filterPrice.top_7k) ? filterPrice.top_7k : top;
            top_max = filterPrice.fixCurPos(filterPrice.top_2k, filterPrice.top_over7k, top);
            top_min = filterPrice.min_sl.position().top;
            top_min = filterPrice.fixCurPos(0, (top_max - 21.6), top_min);
        }
        if (stop === true) {
            top_max = (top_max < filterPrice.top_2k) ? filterPrice.top_2k : top_max;
            top_min = (top_min > filterPrice.top_7k) ? filterPrice.top_7k : top_min;
            filterPrice.fixPrice(top_min, top_max);
            filterPrice.resizeBrdg(top_min, top_max);
            filterPrice.popupPrice(top_min, true);
            filterPrice.popupPrice(top_max, false);
            filterPrice.max_sl.animate({top: top_max});
            filterPrice.min_sl.animate({top: top_min});
            filterPrice.preView();
        } else {
            filterPrice.resizeBrdg();
        }
    },
    fixCurPos: function (min_top, max_top, cur_top) {
        var top = 0;
        cur_top = ((cur_top < (filterPrice.top_2k - filterPrice.min_max_ret) || max_top < filterPrice.top_2k) && min_top === 0) ? 0 : cur_top;
        cur_top = ((cur_top > (filterPrice.top_over7k - filterPrice.min_max_ret) || min_top > filterPrice.top_7k) && max_top === filterPrice.top_over7k) ? filterPrice.top_over7k : cur_top;
        if (cur_top === 0 || cur_top === filterPrice.top_over7k) {
            return cur_top;
        }
        top = Math.round((cur_top - filterPrice.top_2k) / filterPrice.slider_step) * filterPrice.slider_step + filterPrice.top_2k;
        if (top < min_top || top > max_top) {
            var delta_max = (max_top === filterPrice.top_over7k) ? max_top : Math.floor((max_top - filterPrice.top_2k) / filterPrice.slider_step) * filterPrice.slider_step + filterPrice.top_2k;
            var delta_min = (min_top === 0) ? min_top : Math.ceil((min_top - filterPrice.top_2k) / filterPrice.slider_step) * filterPrice.slider_step + filterPrice.top_2k;
            if (delta_max === delta_min) {
                top = delta_max;
                return top;
            }
            if (top > max_top && delta_max > min_top && delta_max <= max_top) {
                top = delta_max;
                return top;
            }
            if (top < min_top && delta_min < max_top && delta_min >= min_top) {
                top = delta_min;
                return top;
            }
        }
        return top;
    },
    fixPrice: function (top, bottom) {
        filterPrice.cur_max = filterPrice.posToPrice(bottom);
        filterPrice.cur_min = filterPrice.posToPrice(top);
    },
    priceToPos: function (price) {
        if (price < 2000) {
            return 0;
        }
        if (price > 7000) {
            return filterPrice.top_over7k;
        }
        return Math.round((price - 2000) / filterPrice.money_per_step) * filterPrice.slider_step + filterPrice.top_2k;
    },
    posToPrice: function (top) {
        if (top < filterPrice.top_2k) {
            return filterPrice.price_min;
        }
        if (top > filterPrice.top_7k) {
            return filterPrice.price_max;
        }
        return Math.round((top - filterPrice.top_2k) / filterPrice.slider_step) * filterPrice.money_per_step + 2000;
    },
    resizeBrdg: function (top_min, top_max) {
        if (top_min === false || top_max === false || top_min === undefined || top_max === undefined) {
            top_min = filterPrice.min_sl.position().top;
            var h = filterPrice.max_sl.position().top - top_min;
            filterPrice.brdg.css('top', top_min + 9).css('height', h);
            filterPrice.popup.css('top', top_min + h / 2 - 34);
        } else {
            filterPrice.brdg.animate({top: top_min + 9, height: top_max - top_min}, {
                step: function (now, fx) {
                    if (fx.prop === 'height') {
                        filterPrice.popup.css('top', $(fx.elem).position().top + now / 2 - 43);
                    }
                }
            });
        }
    },
    seeResults: function () {
        formSubmit('<form method="POST" id="set_fltr_price" action="' + filterPrice.link + '"><input type="hidden" name="cfp" value="' + filterPrice.cur_min + ':' + filterPrice.cur_max + '"></form>');
    },
    preView: function () {
        $('div.fppi_actions>span').hide();
        $('div.fppi_actions>span.fppi_loading').css('display', 'block');
        filterPrice.showPopup(false);
        formSubmit('<form method="POST" id="preview_fltr_price" action="' + filterPrice.link + '"><input type="hidden" name="cfp_preview" value="' + filterPrice.cur_min + ':' + filterPrice.cur_max + '"></form>');
    },
    noResults: function () {
        $('#fppi_results').html('0');
        $('div.fppi_actions>span').hide();
        $('div.fppi_actions>span.fppi_change_range').show();
        filterPrice.showPopup(true);
    },
    haveResults: function () {
        $('div.fppi_actions>span').hide();
        $('div.fppi_actions>span.fppi_see_results').show();
        filterPrice.showPopup(true);
    },
    setPriceRange: function (min, max) {
        if (parseInt(min) < 0 || parseInt(min) > 7000) {
            min = filterPrice.price_min;
        }
        if (parseInt(max) < 2000 || parseInt(max) < parseInt(min)) {
            max = filterPrice.price_max;
        }
        filterPrice.cur_min = min;
        filterPrice.cur_max = max;
        var top_min = filterPrice.priceToPos(filterPrice.cur_min);
        var top_max = filterPrice.priceToPos(filterPrice.cur_max);
        if (top_max - top_min < 21.6) {
            top_min = filterPrice.fixCurPos(0, top_max - 21.6, top_min);
            filterPrice.cur_min = filterPrice.posToPrice(top_min);
        }
        filterPrice.showPopup(true);
        filterPrice.resizeBrdg(top_min, top_max);
        filterPrice.min_sl.animate({top: top_min}, {
            step: function (now, ui) {
                $('#fp_popup_info').addClass('show_fp');
                filterPrice.popupPrice(now, true);
            }
        });
        filterPrice.max_sl.animate({top: top_max}, {
            step: function (now, ui) {
                $('#fp_popup_info').addClass('show_fp');
                filterPrice.popupPrice(now, false);
            }, complete: function () {
                filterPrice.preView();
            }
        });
    },
    showPopup: function (auto_hide) {
        if (filterPrice.popup.find('#fppi_results').attr('action_time') > 0) {
            clearTimeout(filterPrice.hide_timeout);
            filterPrice.popup.stop();
            filterPrice.popup.show();
            filterPrice.popup.css('opacity', 1);
            if (auto_hide === true) {
                filterPrice.hide_timeout = setTimeout(function () {
                    filterPrice.hidePopup();
                }, 3000);
            }
        }
    },
    hidePopup: function () {
        filterPrice.popup.animate({opacity: 0}, 1000, function () {
            filterPrice.popup.hide();
        });
    },
    popupPrice: function (top, min_max) {
        if (min_max === true) {
            filterPrice.popup.find('#fppi_min').html(Math.ceil(filterPrice.posToPrice(top) / filterPrice.cur_value) + '' + filterPrice.reduction);
        }
        if (min_max === false) {
            filterPrice.popup.find('#fppi_max').html(Math.ceil(filterPrice.posToPrice(top) / filterPrice.cur_value) + '' + filterPrice.reduction);
        }
        if (min_max !== true && min_max !== false) {
            filterPrice.popup.find('#fppi_min').html(Math.ceil(filterPrice.posToPrice(filterPrice.min_sl.position().top) / filterPrice.cur_value) + '' + filterPrice.reduction);
            filterPrice.popup.find('#fppi_max').html(Math.ceil(filterPrice.posToPrice(filterPrice.max_sl.position().top) / filterPrice.cur_value) + '' + filterPrice.reduction);
        }
    }
};
var pageNav = {
    sliderSpan: '',
    sliderStep: 1,
    sliderWidth: 26,
    sliderCurPage: 1,
    pagesCount: 1,
    pagesUl: '',
    init: function (dom) {
        pageNav.pagesUl = $(dom).find('div.pages>ul');
        pageNav.pagesCount = pageNav.pagesUl.find('li').length;
        pageNav.sliderCurPage = pageNav.pagesUl.find('li').index(pageNav.pagesUl.find('li:has(span)')) + 1;
        if ($(dom).children('div.slider').length > 0) {
            pageNav.sliderSpan = $(dom).find('div.slider>span');
            pageNav.sliderPrepare();
        }

        setTimeout(function() { 
            var strLastButton = $('.pages ul li').last().html(); 

            if (typeof strLastButton !== 'undefined') {
                strLastButton = strLastButton.indexOf('span');
                if(1 == strLastButton) {            
                    $('.next_page').html('<span>�����</span>');
                } 
            }

              
        }, 500);        
    },
    pagesPos: function (slide) {
        if (pageNav.sliderCurPage > pageNav.pagesCount) {
            pageNav.sliderCurPage = pageNav.pagesCount;
        }
        var pos = pageNav.sliderCurPage;
        var count = pageNav.pagesCount;
        if (slide === true) {
            var ul = pageNav.pagesUl;
            var tmp = (count - pos) > 3 ? 3 : count - pos;
            var start = (pos - 6 + tmp) < 0 ? 0 : pos - 6 + tmp;
            var end = pos + tmp;
            ul.find('li:lt(' + (start) + ')').hide();
            ul.find('li:gt(' + (end - 1) + ')').hide();
            for (var i = (start - 1); i < end; i++) {
                ul.find('li:eq(' + (i) + ')').show();
            }
        }
    },
    loadMore: function (e, p) {
        if ($(e).length && !$(e).parent().hasClass('loading')) {
            e = $(e);
            e.parent().addClass('loading');
            console.log("Loading page: " + p);
            initGET(location.href + '?ajax_ret=load-more&pg=' + p);
        }

        var count = pageNav.pagesUl.find('li').length;
        if (p == count) {
            $('.next_page').html('<span>�����</span>');
        }         
    },
    loadEx: function (p) {
        initGET(location.href + '?ajax_ret=load-more&pg=' + p + '&dontsetpage=1');
    },
    sliderPrepare: function () {
        var pos = pageNav.sliderCurPage;
        var count = pageNav.pagesCount;
        var dom = pageNav.sliderSpan;
        var parent_w = dom.parent().width();
        var tmp_width = parent_w / count;
        if ((tmp_width * 7) > 26) {
            pageNav.sliderWidth = tmp_width * 7;
        }
        dom.css('width', pageNav.sliderWidth);
        pageNav.sliderStep = tmp_width;
        if (pageNav.sliderWidth <= 26) {
            pageNav.sliderStep = (parent_w - 26) / (count - 7);
        }
        if (pos > 3 && (count - pos) > 3) {
            dom.css('left', (pos - 4) * pageNav.sliderStep);
        }
        if (pos < 4) {
            dom.css('left', 0);
        }
        if ((count - pos) < 4) {
            dom.css('left', (count - 7) * pageNav.sliderStep);
        }
        dom.draggable({
            axis: "x", containment: "parent", drag: function (e, ui) {
                var new_pos = Math.round(ui.position.left / pageNav.sliderStep) + 4;
                pageNav.sliderCurPage = new_pos;
                pageNav.pagesPos(true);
            }, stop: function (e, ui) {
                if (e.type === 'dragstop') {
                    var left_pos = (pageNav.sliderCurPage - 4) * pageNav.sliderStep;
                    pageNav.sliderSpan.animate({left: left_pos}, 200);
                }
            }
        });
    }
};
var top_Menu2 = {
    rightWall: 0, showSub: function (dom) {
        var zlevel = 100 + ($(dom).attr('zlevel') * 10);
        top_Menu2.setMenuZlevel($(dom).children('div.sub_cont'));
        var div_width = $(dom).children('div.sub_cont').width();
        var div_height = $(dom).children('div.sub_cont').height();
        $(dom).children('div.box_shadow').css('z-index', zlevel + 2).css('width', div_width).css('height', div_height);
        $(dom).children('a').css('z-index', zlevel + 2);
        $(dom).children('a.hovered').css('z-index', zlevel + 5);
        if ($(dom).hasClass('sub_level_1') === false) {
            var ul = $(dom).parent();
            ul.children('li').css('z-index', zlevel - 1);
            ul.next('ul').children('li').css('z-index', zlevel - 1);
            ul.prev('ul').children('li').css('z-index', zlevel - 1);
            if ($(dom).children('a.hovered').length > 0) {
                var w = Math.ceil($(dom).children('a.main').width());
                $(dom).children('a.hovered').css('width', w);
            }
            if ($(dom).children('div').length > 0) {
                var par_li = $(dom).parent().parent().parent();
                var delta = $(dom).width();
                if (par_li.hasClass('right-sub-menu')) {
                    $(dom).addClass('right-sub-menu').addClass('selected_side');
                }
                if (par_li.hasClass('left-sub-menu')) {
                    $(dom).addClass('left-sub-menu').addClass('selected_side');
                }
                if ($(dom).hasClass('selected_side') === false) {
                    if ((delta + $(dom).children('div').width() + $(dom).offset().left + 15) < top_Menu2.rightWall) {
                        $(dom).addClass('right-sub-menu').addClass('selected_side');
                    } else {
                        $(dom).addClass('left-sub-menu').addClass('selected_side');
                    }
                }
            } else {
                $(dom).children('a').css('box-shadow', 'none');
            }
        }
        $(dom).children('div.sub_cont').css('z-index', zlevel + 6);
        $(dom).css('z-index', zlevel);
    }, setMenuZlevel: function (dom) {
        dom.children('ul>li').each(function () {
            var zlevel = 100 + ($(this).attr('zlevel') * 10);
            $(this).css('z-index', zlevel);
            $(this).children('a').css('z-index', zlevel + 2);
            $(this).children('a.hovered').css('z-index', zlevel + 6);
        });
    }
};
var right_Menu = {
    showSub: function (dom) {
        var zlevel = 100 + ($(dom).attr('zlevel') * 10);
        right_Menu.setMenuZlevel($(dom).children('div.sub_cont'));
        var div_width = $(dom).children('div.sub_cont').width();
        var div_height = $(dom).children('div.sub_cont').height();
        $(dom).children('div.box_shadow').css('z-index', zlevel + 2).css('width', div_width).css('height', div_height);
        $(dom).children('a').css('z-index', zlevel + 2);
        $(dom).children('a.hovered').css('z-index', zlevel + 6);
        var ul = $(dom).parent();
        ul.children('li').css('z-index', zlevel - 1);
        ul.next('ul').children('li').css('z-index', zlevel - 1);
        ul.prev('ul').children('li').css('z-index', zlevel - 1);
        if ($(dom).children('a.hovered').length > 0) {
            var w = Math.ceil($(dom).children('a.main').width()) + 1;
            $(dom).children('a.hovered').css('width', w);
        }
        if ($(dom).children('div').length > 0) {
            $(dom).removeClass('right-sub-menu').removeClass('left-sub-menu').addClass('left-sub-menu');
        } else {
            $(dom).children('a').css('box-shadow', 'none');
        }
        $(dom).children('div.sub_cont').css('z-index', zlevel + 5);
        $(dom).css('z-index', zlevel);
    }, setMenuZlevel: function (dom) {
        dom.children('ul>li').each(function () {
            var zlevel = 100 + ($(this).attr('zlevel') * 10);
            $(this).css('z-index', zlevel);
            $(this).children('a').css('z-index', zlevel + 2);
            $(this).children('a.hovered').css('z-index', zlevel + 6);
        });
    }
};
var gifts = {
    show: function (s) {
        $('div#tabs-data').html('<p><span class="hint">���������� ���������, ��������� ������...</span></p>');
        $('#tabs ul.ui-tabs-nav > li').removeClass('ui-state-active').filter('#basket_gifts_tab_' + s).addClass('ui-state-active');
        initGET('/basket/op=gifts-load&id=' + s);
    }, carusel: function () {
        jQuery(".haveTooltip").easyTooltip();
    }, switchPage: function (id) {
        if (id !== undefined && $('.nav_bar_' + id).length > 0 && $('#new_slider_page_' + id).length > 0) {
            $('.nav_bar_switch').removeClass('active');
            $('.nav_bar_' + id).addClass('active');
            $('.new_slider_page').removeClass('active');
            $('#new_slider_page_' + id).addClass('active');
        } else {
            $('.new_slider_nav_bar').each(function () {
                $(this).find('.nav_bar_switch').removeClass('active').first().addClass('active');
            });
            $('.new_slider_page').removeClass('active').first().addClass('active');
        }
        dimageShow.showClass = 'div.new_slider_page.active>ul.dimage-show-cont';
        dimageShow.scroll();
    }, funbox: function () {
        $(".gifts a.cart_fancybox").fancybox({overlayColor: "#000", overlayOpacity: "0.5"});
    }, setgift: function (id) {
        initGET('/basket/op=setgift&id=' + id);
    }, goSwipe: function(){
        $(".page_slider").swipe( {

            swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
                current = $('.new_slider_nav_bar span.active').attr('class');
                active = current.match(/(\d)/g);
                totals = $('.new_slider_nav_bar').find('span.nav_bar_switch').length;
                totals = totals / 2;

                if (direction == 'right'){

                    if (parseInt(active) != 0 ){
                        gifts.switchPage(parseInt(active)-1);
                    }

                }
                if (direction == 'left'){

                    if (parseInt(active) != parseInt(totals -1)){
                        gifts.switchPage(parseInt(active)+1);
                    }
                }
            },
            allowPageScroll:"auto",
        });
    }
};
var item = {
    minus: function (id, min) {
        var count = parseInt(jQuery('#' + id).val());
        if (count > 1 && count > min) {
            count -= 1;
            jQuery('#' + id).val(count);
        }
    }, plus: function (id) {
        var count = parseInt(jQuery('#' + id).val());
        count += 1;
        jQuery('#' + id).val(count);
    }
};
var cur = {
    value: 1, reduction: '', spanReduction: '', symb: '', spanSymb: function () {
        return '<span class="cur-symb func">' + this.symb + '</span>';
    }, rub: function () {
        jQuery('body').append('<form action="' + window.location + '" id="cur-go"><input type="hidden" name="cur" value="rub"></form>');
        this.go();
    }, euro: function () {
        jQuery('body').append('<form action="' + window.location + '" id="cur-go"><input type="hidden" name="cur" value="euro"></form>');
        this.go();
    }, usd: function () {
        jQuery('body').append('<form action="' + window.location + '" id="cur-go"><input type="hidden" name="cur" value="usd"></form>');
        this.go();
    }, go: function () {
        formSubmit(jQuery('form#cur-go:first'));
    }, fast: function (e) {
        initGET('/catalog/op=cur-go&value=' + e.id);
    }, fastSet: function (e) {
        var v = {'euro': 'ico1', 'usd': 'ico2', 'rub': 'ico3'};
        var s = {'euro': '&euro;', 'usd': '&dollar;', 'rub': '�.'};
        for (var i in v)jQuery('div.infoMoney a#' + i).attr('class', v[i]);
        jQuery('div.infoMoney a#' + e.id).attr('class', v[e.id] + 'active');
        jQuery('div#quickOrderBlock .fast-order-price').each(function () {
            var span = $(this);
            span.html(Math.ceil(parseInt(span.attr('val')) / e.value));
            span.next('span.reduction').html(s[e.id]);
        });
    }
};
var reminder = {
    max: 30, count: function (t) {
        var value = reminder.max - parseInt(jQuery(t).val().length);
        jQuery('span#message-char font').html(value);
        if (value < 0) {
            jQuery('span#message-char font').css('color', 'red');
        } else {
            jQuery('span#message-char font').css('color', '#808080');
        }
    }
};
var imgcar = {
    pach: '', item: [], view: function (i, id) {
        $('.' + id + '_all').hide();
        $('#' + id + '_' + i).show();
        $('#' + id).html('<ul class="sliderSmall jcarousel-skin-tangoSm"></ul>');
        $.each(imgcar.item, function (e, ind) {
            if (e !== i) {
                var dis = '<li><a href="javascript:imgcar.view(' + e + ',\'' + id + '\'); void(0);"><img nopin="nopin" src="' + imgcar.pach + 'w70_' + ind + '" /></a></li>';
                jQuery('#' + id + ' ul.sliderSmall').append(dis);
            }
        });
        jQuery('#' + id + ' ul.sliderSmall').jcarousel({wrap: null, scroll: 1, animation: "slow"});
    }, nopin: function (id) {
        $('#' + id).find('img').attr('nopin', 'nopin');
    }
};
var deliveryPhoto = {
    init: function () {
        if ($('#delivery_photo_cont').length === 0) {
            initGET('/basket/op=delivery-photo');
        } else {
            deliveryPhoto.load();
        }
    }, load: function () {
        windows_info('delivery_photo_cont', 'closeNew', deliveryPhoto.initCarusel, '.window_info ul.sliderSmall');
    }, initCarusel: function (id) {
        jQuery(id).jcarousel({wrap: null, scroll: 1, animation: "slow"}).find('a').attr('rel', "delivery-photo");
        var h = ($(id).height() - 77) / 2 + 20;
        $('.window_info .jcarousel-prev, .window_info .jcarousel-next').css('margin-top', 'calc((192px - 77px)/2)');
        $(id).find('a').fancybox({overlayColor: "#000", overlayOpacity: "0.5"});
    }, overOut: function (el, w, h) {
        el = $(el).find('img');
        el.stop();
        el.animate({width: w, height: h}, {
            step: function (now, fx) {
                if (fx.prop === 'width') {
                    var h = $(fx.elem).height();
                    var left = (168 - now) / 2;
                    var top = (192 - h) / 2;
                    $(fx.elem).css('top', top).css('left', left);
                }
                if (fx.prop === 'height') {
                    var w = $(fx.elem).width();
                    var left = (168 - w) / 2;
                    var top = (192 - now) / 2;
                    $(fx.elem).css('top', top).css('left', left);
                }
            }, duration: 300
        });
    }
};
var no_call = {
    initShow: function () {
        var el = $('input[name="nocall"]');
        if (el.is(':checked')) {
            el.prop('checked', false);
            windows_info('no_call_cont', 'closeNew');
        }
        $('.window_info p.btns>span, .closeNew').click(function () {
            $('#window_info_bg').fadeOut(200);
            $('#window_info_bg').hide();
            $('#no_call_cont').hide();
            if ($(this).hasClass('yes_btn')) {
                no_call.chois(true);
            } else {
                no_call.chois(false);
            }
        });
    }, chois: function (ch) {
        if (ch === true) {
            $('input[name="nocall"]').prop('checked', true);
        } else {
            $('input[name="nocall"]').prop('checked', false);
        }
    }
};
var sendQuestion = {
    init: function () {
        initGET('/sendquestion/op=init');
    }, show: function () {
        windows_info('send_question_form', 'closeNew', sendQuestion.prepareWindow, '');
    }, refresh: function (dom) {
        $(dom).css("border", "1px solid #d6d6d6");
        $("#" + $(dom).prop("name") + "_after").html("<br/>");
    }, prepareWindow: function () {
    }, validateData: function () {
        var sq_name_i = $("#id_sq_name");
        var sq_phone_i = $("#id_sq_phone");
        var ret = true;
        if (sq_name_i.val() <= 0) {
            sq_name_i.css("border", "1px solid red");
            $("#sq_name_after").html("<br/><span style='color: red; font-size: 10px;'>����������, ������� ���� ���</span>");
            ret = false;
        }
        if (sq_phone_i.val() <= 0) {
            sq_phone_i.css("border", "1px solid red");
            $("#sq_phone_after").html("<br/><span style='color: red; font-size: 10px;'>����������, ������� ����� ��������</span>");
            ret = false;
        }
        return ret;
    }, completeRequest: function () {
        if (this.validateData()) {
            $("#sq_blk").html("<h2 style=\"color:#64960a; text-align: center;\">���� ��������� �������! � ��������� ����� �� � ���� ��������.</h2>");
            var name = $("#id_sq_name").val();
            var phone = $("#id_sq_phone").val();
            var email = $("#id_sq_mail").val();
            var text = $("#id_sq_text").val();
            var url = "&name=" + name + "&phone=" + phone + "&email=" + email + "&text=" + text;
            var div = $('#send_question_form');
            $('#window_info_bg').fadeOut(200);
            div.hide();
            div.removeClass('window_info').attr('id', div.data('id-winInfo'));
            initGET('/sendquestion/op=get' + url);
        }
    }
};
var fastBay = {
    init: function () {
        initGET('/fast_baying/op=init');
    }, show: function () {
        windows_info('fast_baying_form', 'closeNew', fastBay.prepareWindow, '');
    }, getCall: function () {
        if ($('.fast_baying_info input[name="fb_name"]').val() == '' || $('.fast_baying_info input[name="fb_phone"]').val() == '') {
            var set_col = $('.fast_baying_info input[name="fb_name"]').val() == '' ? '#f66' : '#6f6';
            $('.fast_baying_info input[name="fb_name"]').css('box-shadow', '0 0 3px 0 ' + set_col + ' inset').attr('placeholder', '������� ���� ���');
            set_col = $('.fast_baying_info input[name="fb_phone"]').val() == '' ? '#f66' : '#6f6';
            $('.fast_baying_info input[name="fb_phone"]').css('box-shadow', '0 0 3px 0 ' + set_col + ' inset').attr('placeholder', '������� ��� �������');
        } else {
            var url = '';
            $('#fast_baying_form').find('input[type="checkbox"], input[type="text"]').each(function () {
                if ($(this).attr('type') == 'checkbox') {
                    url += '&' + $(this).attr('name') + '=' + (($(this).is(':checked')) ? 1 : 0);
                } else {
                    url += '&' + $(this).attr('name') + '=' + $(this).val();
                }
            });
            initGET('/fast_baying/op=get' + url);
        }
    }, setTime: function (dom) {
        var el = $(dom).parent().children('input[type="text"]');
        var val = parseInt(el.val());
        var max = (el.hasClass('fb_m')) ? 59 : 23;
        if ($(dom).val() === '+') {
            val += (val < max) ? 1 : 0;
        } else {
            val -= (val > 0) ? 1 : 0;
        }
        el.val(preFx(val, "0", 2));
    }, changeTime: function (dom) {
        var val = parseInt($(dom).val());
        var max = ($(dom).hasClass('fb_m')) ? 59 : 23;
        if (val > max) {
            val = max;
        }
        $(dom).val(preFx(val, "0", 2));
    }, initTime: function () {
        var dt = new Date();
        var m = dt.getMinutes();
        var h = dt.getHours();
        $('.times_cont input.fb_h').val(preFx(h, "0", 2));
        $('.times_cont input.fb_m').val(preFx(m, "0", 2));
    }, prepareWindow: function () {
        $('#fast_baying_form').find('div.counter_cont input[type=text]').attr('maxlenght', 2);
        $('#fast_baying_form').find('input[type="checkbox"]').each(function () {
            fastBay.checkBox(this);
            $(this).on('click', function () {
                fastBay.checkBox(this);
            });
        });
    }, checkBox: function (dom) {
        var opt = ($(dom).attr('opt') === 'true') ? true : false;
        if ($(dom).is(':checked') === opt) {
            $('.' + $(dom).attr('name')).removeClass('disabled');
            $('.' + $(dom).attr('name')).find('input').prop('disabled', false);
            $('.' + $(dom).attr('name')).find('.disabled input').prop('disabled', true);
            if ($('#' + $(dom).attr('hide')).length > 0) {
                $('#' + $(dom).attr('hide')).hide();
            }
        } else {
            $('.' + $(dom).attr('name')).addClass('disabled');
            $('.' + $(dom).attr('name')).find('input').prop('disabled', true);
            if ($('#' + $(dom).attr('hide')).length > 0) {
                $('#' + $(dom).attr('hide')).show();
            }
        }
    }
};
var cupon = {
    Check: function () {
        var cuponval = $('input[name="cupon"]').val();
        initGET('/basket/op=set-cupon&cupon=' + cuponval);
    }
};
var text_blocks = {
    init: function () {
        var url = '/text_blocks/op=main';
        if ($('.text_block').length > 0) {
            $('.text_block').each(function () {
                if ($(this).attr('tb') !== undefined && ($(this).attr('tb_cont') !== undefined || $(this).attr('id') !== undefined)) {
                    url += '&' + $(this).attr(($(this).attr('tb_cont') !== undefined ? 'tb_cont' : 'id')) + '=' + $(this).attr('tb');
                }
            });
            initGET(url);
        }
        text_blocks.hoverPop();
    }, showMore: function (btn, block) {
        if ($(block).hasClass('show') === true) {
            $(block).removeClass('show');
        } else {
            $(block).addClass('show');
        }
        if ($(btn).hasClass('btn_hide')) {
            $(btn).hide();
        }
    }, hoverPop: function () {
        $('.text_block').hover(function () {
            var pop = ($(this).find('.tb_hover_popup').length > 0) ? $(this).find('.tb_hover_popup') : $('#' + $(this).attr('id') + '_cont.tb_hover_popup');
            if (pop.length <= 0) {
                return;
            }
            pop.show();
            lrSideContPos();
            if ($(this).hasClass('centered_hover_popup')) {
                var delta = (pop.parent().outerWidth(true) - pop.outerWidth(true)) / 2;
                if ((pop.parent().offset().left + delta) < lSideContPos) {
                    var deltaT = delta;
                    delta = lSideContPos - pop.parent().offset().left;
                    deltaT = deltaT - delta;
                    pop.find('div.triagle').css('left', deltaT);
                }
                if ((pop.parent().offset().left + delta + pop.outerWidth(true)) > rSideContPos) {
                    var deltaT = delta;
                    delta = rSideContPos - (pop.parent().offset().left + pop.outerWidth(true));
                    deltaT = deltaT - delta;
                    pop.find('div.triagle').css('left', deltaT);
                }
                if ($(this).hasClass('anonyms'))
                {
                 //   pop.css('left', '-150px');
                    pop.css('margin-top', 0);

                }
                else
                {
                    pop.css('left', delta);
                    pop.css('margin-top', 8);

                }

            }
            if ($(this).hasClass('centered_v_hover_popup')) {
                var delta = (pop.parent().outerHeight(true) - pop.outerHeight(true)) / 2;
                pop.css('top', delta);
            }
        }, function () {
            var pop = ($(this).find('.tb_hover_popup').length > 0) ? $(this).find('.tb_hover_popup') : $('#' + $(this).attr('id') + '_cont.tb_hover_popup');
            if (pop.length <= 0) {
                return;
            }
            pop.hide();
        });
    }
};
var hoverPopup = {
    onOver: function (e) {
        $('#' + $(e).attr('id') + '_cont').show();
    }, onOut: function (e) {
        $('#' + $(e).attr('id') + '_cont').hide();
    }
};
var fw = {
    open: function () {
        $("#darkBack").showDarkBg("show", 0);
        $("#quickOrderBlock").fadeIn(500);
        $("#quickOrderBlock").calculatePositionCenter();
    }, close: function (id) {
        jQuery("#darkBack").click(function () {
            if (jQuery(id).css('display') == 'block') {
                jQuery('#darkBack').showDarkBg('hide', 0);
                jQuery(id).hide().remove();
            }
        });
    }
};
var fast = {
    init: function (id, sid) {
        jQuery('div#quickOrderBlock').html('<span class="h2_alt">������� �����</span><p id="loading">����������� ���������, ���� �������� ������...</p>');
        this.initClose();
        jQuery('#darkBack').showDarkBg("show", 0);
        jQuery('#quickOrderBlock').fadeIn(500, function () {
            initGET('/catalog/op=fast&id=' + id + '&sid=' + sid);
        });
        jQuery('#quickOrderBlock').calculatePositionCenter();
    }, info: function (id, sid) {
        jQuery('div#quickOrderBlock').html('<span class="h2_alt">�������� �������</span><p id="loading">����������� ���������, ���� �������� ������...</p>');
        this.initClose();
        jQuery('#darkBack').showDarkBg("show", 0);
        jQuery('#quickOrderBlock').fadeIn(500, function () {
            initGET('/catalog/op=podarok&id=' + id + '&sid=' + sid);
        });
        jQuery('#quickOrderBlock').calculatePositionCenter();
    }, load: function () {
        jQuery('#quickOrderBlock').calculatePositionCenter();
        this.initClose();
        if (jQuery('input[name$="price"]').size() > 0) {
            jQuery('input[name$="price"]').change(function () {
                jQuery('input[name$="price-value"]').val(jQuery(this).val());
            });
        }
    }, initClose: function () {
        jQuery('.close').click(function () {
            jQuery('#darkBack').fadeOut(300);
            jQuery('div#quickOrderBlock').fadeOut(300, function () {
                jQuery('div#quickOrderBlock').html(null);
            });
        });
        jQuery('#darkBack').click(function () {
            jQuery('#darkBack').fadeOut(300);
            jQuery('div#quickOrderBlock').fadeOut(300, function () {
                jQuery('div#quickOrderBlock').html(null);
            });
        });
    }, pach: '', item: [], carousel: function (i, id) {
        $('.' + id + '_all').hide();
        $('#' + id + '_' + i).show();
        $('#' + id).html('<ul class="sliderSmall jcarousel-skin-tangoSm"></ul>');
        $.each(fast.item, function (e, ind) {
            if (e !== i) {
                var dis = '<li><a href="javascript:fast.carousel(' + e + ',\'' + id + '\'); void(0);"><img nopin="nopin" src="' + fast.pach + 'w70_' + ind + '" /></a></li>';
                $('#' + id + ' ul.sliderSmall').append(dis);
            }
        });
        jQuery('#' + id + ' ul.sliderSmall').jcarousel({wrap: null, scroll: 1, animation: "slow"});
        $(".img>a").fancybox({overlayColor: "#000", overlayOpacity: "0.5"});
    }, ok: function () {
        location = '/order-adopted.html';
    }, colorSet: function (id, cid) {
        if (jQuery('span#fastOrder_colorList_' + id + ' a#fo_c' + cid).hasClass('active')) {
            jQuery('span#fastOrder_colorList_' + id + ' a#fo_c' + cid).removeClass('active');
            jQuery('span#fastOrder_colorList_' + id + ' a#fo_c' + cid + ' input[name="fast_color_' + id + '[]"]').prop('checked', false);
        } else {
            jQuery('span#fastOrder_colorList_' + id + ' a#fo_c' + cid).addClass('active');
            jQuery('span#fastOrder_colorList_' + id + ' a#fo_c' + cid + ' input[name="fast_color_' + id + '[]"]').prop('checked', true);
        }
    }
};
var phone = {
    error: function (r) {
        switch (r) {
            case 1: {
                skyweb.alert({e: jQuery('#phone-number'), t: '����� ������ ����� ��������', top: '50px'});
                skyweb.alert({e: jQuery('#phone-name'), t: '����� ������ ���� ���', top: '99px'});
            }
                break;
        }
    }, result: function (r) {
        switch (r) {
            case 1:
                jQuery('div#darkBack').trigger('click');
                initSysDialog('����� ��������� ������ ������, �� ���������� ��� ����������� ������. ���������� ���!');
                console.log('ghbyzn');
                break;
        }
    }
};
var captcha = {
    refresh: function (img) {
        if (img === undefined) {
            var image = $('#review_form img');
        } else {
            image = $(img);
        }
        image.attr('src', image.attr('src') + '?' + Math.random());
    }
};
var reviews = {
    cid: 0, star: 0, initForm: function () {
        initGET('/reviews/op=init');
    }, showForm: function (cid) {
        reviews.cid = cid;
        $('#review_form input[name="cid"]').val(cid);
        windows_info('review_form', 'closeNew');
    }, clearSpans: function (el) {
        $(el).parent().find('span').remove();
    }, addReview: function () {
        var form = $('.review_form_info form');
        jQuery.ajax({
            url: form.attr('action'),
            type: "POST",
            data: form.formToArray(true),
            enctype: "multipart/form-data",
            dataType: "json",
            beforeSend: function () {
                return php.beforeSend();
            },
            success: function (data, textStatus) {
                return php.success(data, textStatus);
            },
            error: function (xmlEr, typeEr, except) {
                return false;
            },
            complete: function (XMLHttpRequest, textStatus) {
                return php.complete(XMLHttpRequest, textStatus);
            }
        });
    }, setStar: function (star) {
        if (parseInt(star) > 0) {
            reviews.star = parseInt(star);
        }
        $('.review_form_info input[name="star"]').val(reviews.star);
        var pos = 0;
        $('.review_form_info div.inputs>div.stars>span').removeAttr('style').stop().each(function () {
            if (pos < reviews.star) {
                $(this).addClass('setStar');
            } else {
                $(this).removeClass('setStar');
            }
            pos++;
        });
    }, starHover: function (star) {
        $(star).css('background-position', '0 0');
        $(star).nextAll('span').css('background-position', '0 -24px');
        $(star).prevAll('span').css('background-position', '0 0');
    }, itemAdd: function (item) {
        if (jQuery('div.item-reviews-list h2').size() != 0) jQuery('div.item-reviews-list').show();
        jQuery('div.item-reviews-list').append('<div class="otzivsBlock blockWithTopLine hidden"><span class="date">' + item.date + '</span><i>' + item.text + '<b class="name">' + item.name + '</b></i></div>');
        jQuery('div.item-reviews-list div.hidden').fadeIn(500);
        jQuery('div.createOtviz form').resetForm();
    }, insert: function (v) {
        jQuery('div#list-reviews').prepend('<div class="otzivsBlock blockWithTopLine hidden"><span class="date">' + v.date + '</span><i>' + htmlspecialchars_decode(v.text) + '<b class="name">' + v.name + '</b></i></div>');
        jQuery('div#list-reviews div.otzivsBlock:first').fadeIn(500);
        jQuery('div.createOtviz form').resetForm();
    }, error: function (r) {
        switch (r) {
            case 1:
                initSysDialog('��� ���� ����������� ��� ����������');
                break;
            case 2:
                initSysDialog('�� �� ����� ���������');
                break;
        }
    }
};
var order = {
    error: function (r) {
        switch (r) {
            case 1:
                initSysDialog('��� ���� ����������� ��� ����������');
                break;
        }
    }, result: function (r) {
        switch (r) {
            case 1: {
                jQuery('div#individual-order form').resetForm();
                jQuery('#darkBack').showDarkBg('show', 0);
                jQuery('body').append('<div id="popupOrder"><span class="h2_alt">��� ����� �������� � �������</span><div><a href="/basket.html" class="zakazButton">�������� �����</a><a href="javascript:basket.proceed()" class="zakazButton">���������� �������</a></div></div>');
                jQuery('div#popupOrder').calculatePositionCenter();
                jQuery('div#popupOrder').fadeIn(300);
                jQuery('#darkBack').click(function () {
                    basket.proceed();
                });
            }
                break;
        }
    }, view: function (id) {
        jQuery('#darkBack').showDarkBg('show', 0);
        jQuery('body').append('<div id="popupHistory"><span class="h2_alt">��������...</span></div>');
        initGET('/office/op=order&id=' + id);
        jQuery('div#popupHistory').calculatePositionCenter();
        jQuery('div#popupHistory').fadeIn(300);
        jQuery('#darkBack').click(function () {
            jQuery('#darkBack').fadeOut(300);
            jQuery('div#popupHistory').fadeOut(300, function () {
                jQuery('div#popupHistory').remove();
            });
        });
    }, load: function () {
        jQuery('div#popupHistory').calculatePositionCenter();
        jQuery('div#popupHistory .close').click(function () {
            jQuery('#darkBack').trigger('click');
        });
    }
};
var alerts = {
    init: function () {
        jQuery(document).ready(function () {
            jQuery('input.datepicker').datepicker({inline: true});
            jQuery('div.lCabBlock input[name$="time"]').mask("99:99");
        });
    }, insert: function (item) {
        if (jQuery('div.lCabBlock div.napomLi').size() == 0) jQuery('div#alerts-list').append('<span class="h2_alt">�����������</span>');
        jQuery('div#alerts-list').append('<div class="napomLi hidden"><span class="name">' + item.name + '</span><span class="date">' + item.alert + '</span><span class="info">' + htmlspecialchars_decode(item.text) + '</span><a href="/alerts/op=remove&id=' + item.id + '" class="closeLi">&nbsp;</a></div>');
        jQuery('div#alerts-list div.hidden').fadeIn(500);
        jQuery('form.addNap').resetForm();
        jQuery.fn.changeHeightContent();
    }, result: function (r) {
        switch (r) {
            case 1:
                initSysDialog('���� ���������� * ����������� ��� ����������');
                break;
            case 2:
                initSysDialog('�������� ������ �������');
                break;
            case 3:
                initSysDialog('�������� ������ ����');
                break;
            case 4:
                initSysDialog('����� ����������� ������� �������');
                break;
        }
    }
};
var designer = {
    itog: 0, execut: 0, minus: function (id) {
        var price = parseInt(jQuery('span#item_price_' + id).html());
        var count = parseInt(jQuery('span#item_count_' + id).html());
        if (count > 0) {
            count -= 1;
            jQuery('span#item_count_' + id).html(count);
            var itog = count * price;
            jQuery('span#item_summa_' + id).html(itog);
            jQuery('#create_item_' + id).val(count);
        }
        this.summa();
    }, plus: function (id) {
        var price = parseInt(jQuery('span#item_price_' + id).html());
        var count = parseInt(jQuery('span#item_count_' + id).html());
        count += 1;
        jQuery('span#item_count_' + id).html(count);
        var itog = count * price;
        jQuery('span#item_summa_' + id).html(itog);
        jQuery('#create_item_' + id).val(count);
        this.summa();
    }, execution: function (c) {
        designer.execut = parseInt($(c).attr('price'));
        this.summa();
    }, summa: function () {
        designer.itog = 0;
        jQuery('div.createBlock span.summa').each(function () {
            var summa = parseInt(jQuery(this).html());
            designer.itog += summa;
        });
        jQuery('span#create_itog').html(designer.itog + designer.execut);
    }, order: function () {
        formSubmit(jQuery('form#create-bouquet'));
    }, error: function (r) {
        switch (r) {
            case 1:
                initSysDialog('�������� ������� ���������� ������');
                break;
            case 2:
                initSysDialog('����� ������� ���� �� ���� ������');
                break;
            case 3:
                initSysDialog('�������� ������ 1 ���� ��� ������ ������');
                break;
        }
    }, result: function (r) {
        switch (r) {
            case 1: {
                designer.itog = 0;
                designer.execut = 0;
                jQuery('form#create-bouquet').resetForm();
                jQuery('div.createBlock span.count').html('0');
                jQuery('div.createBlock>input[type="hidden"]').val('0');
                jQuery('span#create_itog').html('0');
                jQuery('div.createBlock span.summa').html('0');
                jQuery('form#create-bouquet').find('div.colorChange').each(function () {
                    jQuery(this).find('a:first').trigger('click');
                });
                initSysDialog('��� ����� �������� � �������');
            }
                break;
        }
    }
};
var office = {
    result: function (r, f) {
        switch (r) {
            case 1: {
                initSysDialog('��������� ����� �������� ������ ������������������ �������������');
            }
                break;
            case 2: {
                initSysDialog('����� ������ ����� �����');
                jQuery('div.cardActiv input[name$="key"]').select();
            }
                break;
            case 3: {
                initSysDialog('���� �� ������');
            }
                break;
            case 4: {
                initSysDialog('����� ����� ������� �����������');
            }
                break;
            case 5: {
                initSysDialog('������ ������� ���������');
            }
                break;
            case 6: {
                initSysDialog('��������� ���� E-mail ��������������� �� ������� ������������');
            }
                break;
            case 61: {
                initSysDialog('��������� ���� ������� ��������������� �� ������� ������������');
            }
                break;
            case 7: {
                initSysDialog('�� ��� ���� ������� "��������� ������" ���������');
            }
                break;
            case 8: {
                initSysDialog('������������� ������ ������� �� �����');
            }
                break;
            case 9: {
                initSysDialog('������ ������ ������ �� �����');
            }
                break;
            case 10: {
                jQuery('form#change-password').resetForm();
                initSysDialog('������ ������� �������');
            }
                break;
            case 11: {
                initSysDialog('���� ������� ������ ('+f.disc_uno+') ��������� ������ ������ �� ��������� ���������� ����� ('+f.disc_dos+'). ������ �������� �������!');
            }
        }
    }, cards: function (retObj) {
        jQuery.getJSON("/office/op=cupcards", function (data, status) {
            if (status == "success") {
                retObj = data;
                console.log(retObj);
                if (retObj.type == 'card') {
                    jQuery("#cardResult").html('<span class="h3_alt">������ �������</span><table width="100%" border="0" cellspacing="0" cellpadding="0" class="data">' + '<tr><th>����� �����</th><th>������</th><th>���� ���������</th></tr>' + '<tr><td>' + retObj.name + '</td><td>' + retObj.val + '%</td><td>' + retObj.dt + '</td></tr>' + '</table>');
                    jQuery("#cardResult").slideDown('slow');
                } else if (retObj.type == 'cupon') {
                    jQuery("#cardResult").html('<table width="100%" border="0" cellspacing="0" cellpadding="0" class="data">' + '<tr><th>�����</th><th>������</th><th>���������</th></tr>' + '<tr><td>' + retObj.name + '</td><td>' + retObj.val + '%</td><td>�� ������� �����</td></tr>' + '</table>');
                    jQuery("#cardResult").slideDown('slow');
                }
            }
        });
    }
};

var typeItem = {
    check: function (id) {
        $('.holy').hide();
        $('#' + id).show();
        var el = $('#subprice_struc_' + id);
        if (el.length) {
            $('#item-size').html(el.data('size'));
            $('#composition').html(el.html());
        }
    }
};
var fastprice = {
    check: function (id) {
        $('.holy-fast').hide();
        $('#holy-' + id).show();
        $('input[name$="cost"]').val($('#fast-' + id).html());
        var el = $('#fast_subprice_struc_' + id);
        if (el.length) {
            $('#fast-item-size').html(el.data('size'));
            $('#fast-composition').html(el.html());
        }
    }
};
var corporate = {
    created: function () {
        jQuery('div.corpForm form').resetForm();
        initSysDialog('���� ������ ������� ���������. ��������, � ���� ��������.');
    }, error: function (r) {
        switch (r) {
            case 1:
                initSysDialog('���� ���������� * ����������� � ����������!');
                break;
        }
    }
};
var login = {
    login_stage: 0, initForm: function () {
        if ($('#popupLogin').length == 0 && $('input[name="goon"]').length > 0) {
            $('input[value="no_reg"]').prop('checked', true);
        }
    }, closeForms: function () {
        $('#popupLogin').hide().removeClass('window_info');
        $('#popupLogin>div').hide();
        jQuery('#darkBack').showDarkBg('hide', 0);
    }, showForm: function (form) {
        $('#darkBack').showDarkBg('show', 0);
        $("#darkBack").click(function () {
            login.closeForms();
        });
        $(form).show();
        $('#popupLogin').addClass('window_info').css('margin-top', '0').show();
        windows_info_repos();
    }, socLogin: function (soc) {
        initGET('/login/op=soc_login&soc=' + soc);
    }, entr: function () {
        login.closeForms();
        login.showForm('#login_cont');
        $('#login_cont input[name$="email"]').focus();
        $('#login_cont form').attr('action', window.location);
    }, recovery: function () {
        login.closeForms();
        login.showForm('#forget_pass');
        $('#forget_pass input[name$="email"]').focus();
    }, registration: function () {
        if ($('#popupRegistration').length > 0) {
            login.closeForms();
            login.showForm('#popupRegistration');
            $('#popupRegistration input[name$="name"]').focus();
            $(".tutPlus input").toggle(function () {
                $('.reg_butt').addClass('open');
                $('#popupLogin').animate({marginTop: "-200px"});
                $(this).parent().next().slideDown();
                $(this).val('-');
            }, function () {
                $('.reg_butt').removeClass('open');
                $('#popupLogin').animate({marginTop: "0px"});
                $(this).parent().next().slideUp();
                $(this).val('+');
            });
        }
    }, exit: function () {
        jQuery('body').prepend('<form action="' + window.location + '" id="login-exit"><input type="hidden" name="auth" value="logout"></form>');
        formSubmit(jQuery('form#login-exit:first'));
    }, result: function (r) {
        switch (r) {
            case 1: {
                skyweb.alert({e: jQuery('#registration-name'), t: '����� ������ ���', top: '6px'});
                skyweb.alert({e: jQuery('#registration-email'), t: '����� ������ e-mail', top: '6px'});
                skyweb.alert({e: jQuery('#registration-pass'), t: '����� ������ ������', top: '6px'});
                skyweb.alert({e: jQuery('#registration-rpass'), t: '������� ������ ��� ���', top: '6px'});
            }
                break;
            case 2:
                initSysDialog('������ �� ���������');
                break;
            case 3:
                initSysDialog('���� email ��� ���������������, ����������� ������');
                break;
            case 30:
                initSysDialog('��������� ���� email ��� ��������������� �� ���� �����. ���� ��� ��� e-mail, �� ������ ��������������� ���������� �������������� ������� � ������� ������. ���������� ���');
                break;
            case 4: {
                jQuery('div#darkBack').trigger('click');
                initSysDialog('����������� ���������. ��������� ����� � ����������� ���� �������');
            }
                break;
            case 5: {
                skyweb.alert({e: jQuery('div#recovery-email'), t: '����� ������ e-mail ��� �������', top: '26px'});
            }
                break;
            case 6:
                initSysDialog('��������� ���� email �� ���������������');
                break;
            case 61:
                initSysDialog('��������� ���� ������� �� ���������������');
                break;
            case 7:
                initSysDialog('���� ������� ������ ��� �� ������������');
                break;
            case 8:
                jQuery('div#darkBack').trigger('click');
                initSysDialog('�� e-mail, ��������� ���� ��� �����������, ���� ������� ������ � ����������� �� �������������� ������');
                break;
            case 81:
                jQuery('div#darkBack').trigger('click');
                initSysDialog('�� �������, ��������� ���� ��� �����������, ���� ���������� ��������� � ����������� �� �������������� ������');
                break;
            case 9: {
                skyweb.alert({e: jQuery('div#login-email'), t: '����� ������ e-mail'});
                skyweb.alert({e: jQuery('div#login-password'), t: '����� ������ ������', top: '6px'});
            }
                break;
            case 10:
                initSysDialog("������ ��� ����� ������ ��� ������");
                break;
            case 11:
                initSysDialog('���� ������� ��� ���������������, ����������� ������. ����� �� ������ ��������������� �������� �������������� ������');
                break;
            case 12:
                initSysDialog('������ ������� �������.');
                break;
        }
    }
};
var skyweb = {
    alert: function (v) {
        if (jQuery(v.e).find('input').size() != 0) {
            if (jQuery(v.e).find('input').val() == '') {
                jQuery(v.e).append('<span class="alert">' + v.t + '</span>');
                if (v.top == undefined) {
                    jQuery(v.e).find('span.alert').css('top', jQuery(v.e).find('input').offset().top - jQuery(v.e).find('input').height() - 20);
                } else jQuery(v.e).find('span.alert').css('top', v.top);
                if (v.left !== undefined) jQuery(v.e).find('span.alert').css('left', v.left);
                jQuery(v.e).find('input').focus(function () {
                    jQuery(v.e).find('span.alert').fadeOut(500, function () {
                        jQuery(this).remove();
                    });
                });
            }
        } else if (jQuery(v.e).find('textarea').size() != 0) {
            if (jQuery(v.e).find('textarea').val() == '') {
                jQuery(v.e).append('<span class="alert">' + v.t + '</span>');
                if (v.top == undefined) {
                    jQuery(v.e).find('span.alert').css('top', jQuery(v.e).find('textarea').offset().top - jQuery(v.e).find('textarea').height() - 20);
                } else jQuery(v.e).find('span.alert').css('top', v.top);
                if (v.left !== undefined) jQuery(v.e).find('span.alert').css('left', v.left);
                jQuery(v.e).find('textarea').focus(function () {
                    jQuery(v.e).find('span.alert').fadeOut(500, function () {
                        jQuery(this).remove();
                    });
                });
            }
        }
    }
};
var href = {
    go: function (u) {
        location = u;
    }
};
var phpFunc = {
    stripos: function (f_haystack, f_needle, f_offset) {
        var haystack = f_haystack.toLowerCase();
        var needle = f_needle.toLowerCase();
        var index = 0;
        if (f_offset == undefined) {
            f_offset = 0;
        }
        if ((index = haystack.indexOf(needle, f_offset)) > -1) {
            return index;
        }
        return false;
    }
};
var miniGal = {
    over: function (el) {
        $(el).addClass('MGover');
        var mg = $(el).find('div.miniGal>div');
        if (mg.length < 1) {
            var div = document.createElement('div');
            var imgsList = $(el).data('imgs').split(":");
            var link = $(el).data('path');
            imgsList.push(imgsList[0]);
            $(div).addClass('miniGal').css({
                position: 'absolute',
                overflow: 'hidden',
                width: 168,
                height: 192,
                left: 0,
                top: 0
            }).html('<div></div>').find('div').css({
                width: (168 * (imgsList.length)),
                height: 192,
                position: 'relative',
                left: 0
            });
            $(el).prepend(div);
            mg = $(el).find('div.miniGal>div');
            imgsList.forEach(function (e) {
                var img = $('<img  src="' + link + e + '" alt="" width="168" height="192" />').css({opacity: 0}).load(function () {
                    $(this).addClass('is_ready').css({opacity: 1, width: 168, height: 192});
                    if (mg.find('img').length === mg.find('img.is_ready').length && mg.parent().parent().hasClass('MGover')) {
                        setTimeout(function () {
                            miniGal.scroll(mg);
                        }, 300);
                    }
                });
                mg.append(img);
            });
        }
        miniGal.scroll(mg);
    }, out: function (el) {
        $(el).removeClass('MGover').find('div.miniGal > div').stop(true).animate({left: 0}, 300);
    }, scroll: function (mg) {
        if (mg.find('img').length === mg.find('img.is_ready').length && mg.parent().parent().hasClass('MGover')) {
            mg.animate({left: '-=168px'}, 300, function () {
                if ((mg.width() - 168 + mg.position().left) <= 0) {
                    mg.css({left: 0});
                }
                mg.animate({left: '-=0px'}, 1400, function () {
                    miniGal.scroll(mg);
                });
            });
        }
    }
};
Number.prototype.formatMoney = function (n, x, s, c) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')', num = this.toFixed(Math.max(0, ~~n));
    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};

// abandoned carts

var abandoned = {
    put: function (){
        Uname  = jQuery(".customer_name").val();
        Uemail = jQuery(".customer_email").val();
        Uphone = jQuery(".customer_phone").val();
        // save eidted data
        if (Uphone.length > 8 || Uemail.length > 5) {
            jQuery.ajax({
                type: "POST",
                url: "/basket/op=abandon",
                data: {name: Uname, email: Uemail, phone: Uphone}
            }).done(function (msg) {
                //console.log("abandoned saved: " + msg);
            });
        }
    }

};