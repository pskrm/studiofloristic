jQuery(function(){
    jQuery(".chosen-select").chosen({
        width: "170px"
    });

    jQuery('input:radio[name="delivery"]').change(function(){
        jQuery.cookie('metro_deliver' , jQuery("#mselect option:selected").val(), basket.cook_opt);

        if (jQuery(this).is(':checked') && jQuery(this).val() == '5') {
            // append goes here
            jQuery("#mselect").prop('disabled', false).trigger("chosen:updated");

        }else{
            jQuery("#mselect").prop('disabled', true).trigger("chosen:updated");
        }
    });

    jQuery('#mselect').on('change', function(evt, params) {
        jQuery.cookie('metro_deliver' , jQuery("#mselect option:selected").val(), basket.cook_opt);
    });

    //delivery_metro
});