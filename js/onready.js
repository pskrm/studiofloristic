var g_resp = false;
var loadCaptcha = function() {
    captchaContainer = grecaptcha.render('g-recaptcha', {
        'sitekey': '6Ldjcw8UAAAAADQ2C05xISlTKB6eyBs5q6cPbB0z',
        'callback': function(response) {
            if(response != '0'){
                   g_resp = true;
             }
        }
    });
};

$(document).ready(function() {
    lrSideContPos();
    filterPrice.init();
    login.initForm();
    fw.close(".zakazZvonkaTop");
    pageNav.init("div.pages_sell");
    basket.fieldsSet();
    basket.recipient($("input.polIsZak").get());
    text_blocks.init();

    /* ----- from onload -------- */
    lrSideContPos();

    $.fn.closePopup(".zakazZvonkaTop", 0);
    $.fn.closePopup("#popupRegistration");

    $(".haveTooltip").length ? $(".haveTooltip").easyTooltip() : void(0);
    $(".colorChange a").length ? $(".colorChange a").easyTooltip() : void(0);
    $(".noActiveBlock input, .noActiveBlock textarea, .noActiveBlock checkbox").attr("disabled", "disabled");
    $(".addNap .dateTime").next().addClass("floatRight1");
    $(".moremathods").click(function() {
        $(".hiddenmethods").show();
        $(this).hide();
        $(".more-methods-wrapper").hide();
    });
    $('.mycarousel').jcarousel({
        wrap: null,
        scroll: 1,
        animation: "slow"
    });
    $(".img>a, a.cart_fancybox").fancybox({
        overlayColor: "#000",
        overlayOpacity: "0.5"
    });
    /*- ������������ ��������� �������� -*/
    $(".close").click(function() {
        $("#darkBack").showDarkBg("hide", 0);
        $(this).parent().fadeOut(500);
    });
    $(".tutPlus input").toggle(
        function() {
            $(this).parent().next().slideDown();
            $(this).val("-");
        },
        function() {
            $(this).parent().next().slideUp();
            $(this).val("+");
        }
    );
    $('#slideTop').click(function() {
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
    $(".alert").click(function() {
        $(this).hide();
    });
    $('#openTopContactsBlockLestener').click(function() {
        var el = $(this).clone();
        $('#header .contactsTopLestener').append('<div class="zakazZvonkaTop"><form action="/phone/op=insert" onsubmit="formSubmit(this); return false;"><div class="zakaz1"></div><div class="zakaz2"><div id="phone-number">��� �������<br /><input name="phone" type="text" /></div><div id="phone-name">���� ���<br /><input name="name" type="text" /></div><div class="dopText">��� ���� ����������� ��� ����������</div><div class="textRight floatRight"><input type="submit" value="��������" onclick="ga(\'send\', \'event\', \'send7UA\', \'Click7UA\'); yaCounter10652362.reachGoal(\'ya7send\'); return true;" /></div><div class="clear"></div></div></form></div>');
        $('.zakaz1').prepend(el);
        $('#darkBack').showDarkBg('show', 0);
        $('.contactsTopLestener .zakazZvonkaTop').show();
    });

    //                                                            ����� JS
    //                                                            ������������� ����� � ����������� �������
    var timeout = null;
    $(".detailed__contacts").mouseenter(function(e) {
        $(".contact__info__modal_wrap").show();
        clearTimeout(timeout);
        timeout = null;
    });

    $(".contact__info__modal_wrap").mouseleave(function(e) {
        timeout = setTimeout(function() {
            $(".contact__info__modal_wrap").hide();
        }, 300);
    });

    $(".contact__info__modal_wrap").mouseenter(function(e) {
        clearTimeout(timeout);
        timeout = null;
    });
    //                                                            END OF ���������� ��������� ����� � ����������� �������
    //                                                            �������� �������
    $(document).ready(function() {
        var freqSecs = 0.6;
        setInterval(blink, freqSecs * 1000);

        function blink() {
            var inout = (freqSecs * 1000) / 0.5;
            $("#blinkelement").fadeIn(inout).fadeOut(inout);
        }
    });
    //                                                            END OF �������� �������

    $('ul.sw-tab li').click(function() {
        $('ul.sw-tab .active').removeClass('active');
        $(this).addClass('active');
        $('div.sw-tabs').hide();
        $('div#' + $(this).attr('id') + '-tab').show();
    });

    if ($(".catalog").length) {
        var col = $(".catalogItem .title").length;
        var col1 = Math.ceil(col / 3);
        for (i = 0; i < col1; i++) {
            $(".catalogItem .title").slice(3 * i, 3 * i + 3).findMaxHeight();
        }
    }
    /*- �������� � ������� -*/
    if ($(".oplataBlock").length) {
        var col1 = $(".oplataBlock").find("table:first td:eq(0)").width();
        $(".oplataBlock").find(".dopTable td:eq(0)").width(col1);
    }

    //                                                            ������ ��� ������������
    $(document).ready(function() {
        $('#one').click(function() {
            $('#one').toggleClass('display__none');
            $('#two').toggleClass('display__none');
            $('div.col__left p:nth-child(1)').toggleClass('display__none');
            $('div.col__left p:nth-child(2)').toggleClass('display__none');
            $('p.phone.letter-spacing').toggleClass('one_phone');

        });
        $('#two').click(function() {
            $('#two').toggleClass('display__none');
            $('#one').toggleClass('display__none');
            $('div.col__left p:nth-child(1)').toggleClass('display__none');
            $('div.col__left p:nth-child(2)').toggleClass('display__none');
            $('p.phone.letter-spacing').toggleClass('one_phone');
        });
    });
    //                                                            END OF ������ ��� ������������
    //                                                            END OF ����� JS

    if ($("#payButton").length) {
        if ((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i))) {
            $("#payButton").onTouchStart(function(e) {
                form_id = $(this).attr('form');
                if ($('#' + form_id).length) {
                    e.preventDefault();
                    initGET('/basket/op=sendMsg&oid=' + $(this).attr('data-source')); // ���������, ��� ����� ������ ��� ���, ��� ���������� ������
                    $('#' + form_id).submit();
                }
            });
        } else {
            $("#payButton").click(function(e) {
                form_id = $(this).attr('form');
                if ($('#' + form_id).length) {
                    e.preventDefault();
                    initGET('/basket/op=sendMsg&oid=' + $(this).attr('data-source')); // ���������, ��� ����� ������ ��� ���, ��� ���������� ������
                    $('#' + form_id).submit();
                }
            });
        }

    }

    top_Menu2.rightWall = rSideContPos;
    top_Menu2.setMenuZlevel($('div.topMenu2'));
    basket.by_phone_only();

    $(document).ready(function() {
        $(".hiddens").hide();
        $(".opens").click(function() {
            jQuery(this).toggleClass("opened");
            jQuery(this).next().slideToggle();
        });

        // ���� ��� �������� ������ �������

        if ($("#manual-pay").length) {
            $.ajax({
                    method: "POST",
                    url: "/basket/op=checkSbNeeds",
                    dataType: "JSON"
                })
                .done(function(data) {

                    if (data.result == 'true') {
                        $("#manual-type").append('<option value="sbbank">������ �� ��������� � ����� ����� ��</option>');
                    }
                });
        }

        $("#manual-pay").click(function(event) {
            event.preventDefault();
            if (g_resp == false) {
                $("#r3").show()
                return;
            }
            var isPa = false;

            $("#appender").html("");
            // block fields
            manoid = $("#manual-oid").val();
            mansumm = $("#manual-summ").val();
            proppay = $("#manual-type").val();

            if (manoid) {

                if ($("#manual-oid").val().length > 4) {
                    $.ajax({
                            method: "POST",
                            url: "/basket/op=getsummbyoid&oid=" + parseInt($("#manual-oid").val()),
                            dataType: "JSON"
                        })
                        .done(function(data) {
                            console.log(data);

                            if (parseInt(data.text) == 1) {
                                $("#manual-comment").html('<span style="color:darkorange;">&nbsp;&nbsp;&nbsp;����� �' + $("#manual-oid").val() + ' �������� �� ������� ������������</span>');
                                $("#manual-oid").css('background-color', '#d7d7d7;');
                                $("#manual-summ").css('background-color', '#d7d7d7;');
                                grecaptcha.reset();

                            }
                            if (parseInt(data.text) == 2) {
                                $("#manual-comment").html('<span style="color:green;">&nbsp;&nbsp;&nbsp;����� �' + $("#manual-oid").val() + ' ��� �������</span>');
                                $("#manual-oid").css('background-color', '#d7d7d7;');
                                $("#manual-summ").css('background-color', '#d7d7d7;');
                                grecaptcha.reset();
                            }
                            if (parseInt(data.text) == 3) {
                                $("#manual-comment").html('<span style="color:red;">&nbsp;&nbsp;&nbsp;����� �' + $("#manual-oid").val() + ' �� ������</span>');
                                $("#manual-oid").css('background-color', '#d7d7d7;');
                                $("#manual-summ").css('background-color', '#d7d7d7;');
                                grecaptcha.reset();
                            }
                            if ((parseInt(data.summ) != 0) && (parseInt(data.text) == 0)) {
                                $("#manual-summ").prop('disabled', true);
                                $("#manual-summ").val(data.summ);
                                $("#manual-oid").prop('disabled', true);
                                $("#manual-oid").css('background-color', '#d7d7d7;');
                                $("#manual-summ").prop('disabled', true);
                                $("#manual-summ").css('background-color', '#d7d7d7;');
                                $.ajax({
                                        method: "POST",
                                        url: "/basket/op=manual&oid=" + manoid + "&summ=" + mansumm + "&type=" + proppay,
                                        dataType: "html"
                                    })
                                    .done(function(data) {
                                        grecaptcha.reset();
                                        $("#r1").remove();
                                        $("#r2").remove();
                                        $("#r3").hide();
                                        $("#manual-oid").removeClass('error');
                                        $("#manual-summ").removeClass('error');
                                        $("#manual-oid").prop('disabled', false);
                                        $("#manual-summ").css('background-color', '');
                                        $("#manual-oid").css('background-color', '');
                                        $("#manual-summ").prop('disabled', false);
                                        $("#appender").html(data);
                                        $("#appender").find('form').submit();
                                        //
                                    });
                            }

                        });

                    // pay by oid
                } else {
                    $("#manual-oid").addClass('error');
                    $("#infomanual").append('<span class="error-message" id="r1"><span>������� ����� ������ ��� ������������ ����� ��� ������</span></span><span class="error-label" id="r2">����� �� ������</span>');
                    $("#manual-summ").addClass('error');
                    $("#manual-oid").prop('disabled', false);
                    $("#manual-summ").prop('disabled', false);
                    $("#manual-oid").css('background-color', '');
                    $("#manual-summ").css('background-color', '');
                    grecaptcha.reset();
                }

            } else {
                if (mansumm) {
                    // pay by summ
                    $.ajax({
                            method: "POST",
                            url: "/basket/op=manual&oid=0&summ=" + mansumm + "&type=" + proppay,
                            dataType: "html"
                        })
                        .done(function(data) {
                            grecaptcha.reset();
                            $("#r1").remove();
                            $("#r2").remove();
                            $("#r3").hide();
                            $("#manual-oid").removeClass('error');
                            $("#manual-summ").removeClass('error');
                            $("#manual-oid").prop('disabled', false);
                            $("#manual-summ").css('background-color', '');
                            $("#manual-oid").css('background-color', '');
                            $("#manual-summ").prop('disabled', false);
                            $("#appender").html(data);
                            $("#appender").find('form').submit();
                            //
                        });
                } else {
                    grecaptcha.reset();
                    $("#manual-oid").addClass('error');
                    $("#infomanual").append('<span class="error-message" id="r1"><span>������� ����� ������ ��� ������������ ����� ��� ������</span></span><span class="error-label" id="r2">����� �� ������</span>');
                    $("#manual-summ").addClass('error');
                    $("#manual-oid").prop('disabled', false);
                    $("#manual-summ").prop('disabled', false);
                    $("#manual-oid").css('background-color', '');
                    $("#manual-summ").css('background-color', '');
                    // error
                }
            }

        });

        $("#manual-type").change(function(e) {
            $("#r1").remove();
            $("#r2").remove();
            $("#r3").hide();
            $("#manual-oid").val('');
            $("#manual-summ").val('');
            $("#manual-oid").removeClass('error');
            $("#manual-summ").removeClass('error');
            $("#manual-oid").prop('disabled', false);
            $("#manual-summ").prop('disabled', false);
            $("#manual-summ").css('background-color', '');
            $("#manual-oid").css('background-color', '');
            $("#manual-pay").prop('disabled', false);
            $("#manual-pay").css('opacity', '1');
        });

        $("#manual-oid, #manual-summ").focus(function() {
            $("#r1").remove();
            $("#r2").remove();
            $("#r3").hide();
            $("#manual-oid").removeClass('error');
            $("#manual-summ").removeClass('error');
            $("#manual-oid").css('background-color', '');
            $("#manual-summ").css('background-color', '');
            $("#manual-comment").html('&nbsp;');
            $("#manual-oid").val('');
            $("#manual-summ").val('');
        });

        $("#manual-oid, #manual-summ").click(function() {
            $("#r1").remove();
            $("#r2").remove();
            $("#r3").hide();
            $("#manual-oid").removeClass('error');
            $("#manual-summ").removeClass('error');
            $("#manual-oid").css('background-color', '');
            $("#manual-summ").css('background-color', '');
            $("#manual-comment").html('&nbsp;');
            $("#manual-oid").val('');
            $("#manual-summ").val('');
        });

        if (window.location.href.indexOf("payment") > -1) {

            sendQuestion.init();
        }




        $(".set-banner-destination").click(function(e) {
            e.preventDefault();
            setCookie('ban-dest-id', $(this).prop('id'));
            window.location = $(this).prop('href');
        });


    });



    function setCookie(name, value, expires, path, domain, secure) {
        document.cookie = name + "=" + escape(value) +
            ((expires) ? "; expires=" + expires : "") +
            ((path) ? "; path=" + path : "") +
            ((domain) ? "; domain=" + domain : "") +
            ((secure) ? "; secure" : "");
    }
});