window.addEventListener("orientationchange", function() {
    location.reload();
}, false);
$(document).ready(function() {
    var a = window.matchMedia("only screen and (max-width: 480px)");
    if (a.matches) {
        if ($(".menu-mobile-link").length) {
            $(".menu-mobile").hide();
            $(".menu-mobile-link").click(function() {
                $(".menu-mobile").toggle();
            });
        }
    }
});
if (typeof Array.prototype.forEach != "function") {
    Array.prototype.forEach = function(b) {
        for (var a = 0; a < this.length; a++) {
            b.apply(this, [this[a], a, this]);
        }
    };
}

function crop_set_img(a) {
    void(0);
}

function moveToDom(b) {
    b = $("#" + b).length > 0 ? $("#" + b) : $(b);
    if (b.length) {
        var a = b.offset().top;
        a = a > 15 ? a - 15 : 0;
        $("body").animate({
            scrollTop: a
        }, 800);
        return false;
    }
}

function lrSideContPos() {
    lSideContPos = $("#content").offset().left + 20;
    rSideContPos = lSideContPos + $("#content").outerWidth() - 40;
}

function windows_info_repos() {
    var g = $(".window_info");
    if (g.length) {
        var d = $(window).height();
        var b = $(document).width();
        var c = g.height();
        var a = g.width();
        var f = $(document).scrollTop() + ((d - c) / 2);
        var e = ((b - a) / 2);
        f = (f < 35) ? 35 : f;
        g.css("top", f);
        g.css("left", e);
    }
}

function centered(g) {
    g = $(g);
    if (g.length) {
        var d = $(window).height();
        var b = $(document).width();
        var c = g.height();
        var a = g.width();
        var f = $(document).scrollTop() + ((d - c) / 2);
        var e = ((b - a) / 2);
        f = (f < 35) ? 35 : f;
        g.css("top", f);
        g.css("left", e);
    }
}

function capUpdate(a) {
    $(a).attr("src", $(a).attr("src") + "?" + Math.random());
}

function strItems(f, g, e, d) {
    var h = parseInt(f);
    var b = f + " " + g;
    var a = h % 10 == 1 && h % 100 != 11 ? "" : (h % 10 >= 2 && h % 10 <= 4 && (h % 100 < 10 || h % 100 >= 20) ? d : e);
    return b + a;
}

function megaHoverOver() {
    $(this).find("div").stop().fadeTo(200, 1).show();
    $(this).find("a:first").clone().insertAfter($(this).find("a:first")).addClass("active");
}

function megaHoverOut() {
    $(this).find("a.active").remove();
    $(this).find("a:first").removeClass("active").end().find("div").stop().fadeTo(0, 0, function() {
        $(this).hide();
    });
}

function initGET(a) {
    jQuery.ajax({
        url: a,
        type: "POST",
        data: "j=1",
        dataType: "json",
        beforeSend: function() {
            jQuery("div#loading").show();
            return php.beforeSend();
        },
        success: function(b, c) {
            jQuery("div#loading").hide();
            if (window.backURL !== undefined && a == window.backURL) {
                delete window.backURL;
            }
            return php.success(b, c);
        },
        error: function(d, c, b) {
            jQuery("div#loading").hide();
            return false;
        },
        complete: function(b, c) {
            jQuery("div#loading").hide();
            return php.complete(b, c);
        }
    });
    return false;
}

function formSubmit(b, c) {
    var a = true;
    if (jQuery(b).attr("id") == "issue-button") {
        c.preventDefault();
        a = false;
    }
    jQuery.ajax({
        url: jQuery(b).attr("action"),
        type: "POST",
        async: a,
        data: jQuery(b).formToArray(true),
        enctype: "multipart/form-data",
        dataType: "json",
        beforeSend: function() {
            if (a == false) {
                if (jQuery("#issue-button").length > 0) {
                    animatIt(jQuery("#issue-button"));
                }
            }
            return php.beforeSend();
        },
        success: function(d, e) {
            return php.success(d, e);
        },
        error: function(f, e, d) {
            return false;
        },
        complete: function(d, e) {
            return php.complete(d, e);
        }
    });
    return false;
}

function getForm(a) {
    initURL(jQuery(a).attr("action") + "&" + jQuery(a).serialize());
}

function initSysDialog(b, a) {
    a = $.isNumeric(a) ? parseInt(a) : 2000;
    jQuery("body").append('<table id="sys-dialog" class="popup"><tr><td id="d"><div class="dialog-box"><div class="title">Системное уведомление</div><div class="data">' + b + "</div></div></td></tr></table>");
    jQuery("table#sys-dialog").css("opacity", 0.9).fadeIn(500, function() {
        setTimeout(function() {
            jQuery("table#sys-dialog").fadeOut(500, function() {
                jQuery(this).remove();
            });
        }, a);
    }).click(function() {
        jQuery("table#sys-dialog").fadeOut(500, function() {
            jQuery(this).remove();
        });
    });
}

function loadCSS(b) {
    var a = document.createElement("link");
    a.setAttribute("rel", "stylesheet");
    a.setAttribute("type", "text/css");
    a.setAttribute("href", b);
    if (typeof a != "undefined") {
        document.getElementsByTagName("head")[0].appendChild(a);
    }
}

function htmlspecialchars_decode(b, f) {
    var d = 0,
        c = 0,
        e = false;
    if (typeof f === "undefined") {
        f = 2;
    }
    b = b.toString().replace(/&lt;/g, "<").replace(/&gt;/g, ">");
    var a = {
        ENT_NOQUOTES: 0,
        ENT_HTML_QUOTE_SINGLE: 1,
        ENT_HTML_QUOTE_DOUBLE: 2,
        ENT_COMPAT: 2,
        ENT_QUOTES: 3,
        ENT_IGNORE: 4
    };
    if (f === 0) {
        e = true;
    }
    if (typeof f !== "number") {
        f = [].concat(f);
        for (c = 0; c < f.length; c++) {
            if (a[f[c]] === 0) {
                e = true;
            } else {
                if (a[f[c]]) {
                    d = d | a[f[c]];
                }
            }
        }
        f = d;
    }
    if (f & a.ENT_HTML_QUOTE_SINGLE) {
        b = b.replace(/&#0*39;/g, "'");
    }
    if (!e) {
        b = b.replace(/&quot;/g, '"');
    }
    b = b.replace(/&amp;/g, "&");
    return b;
}

function content_window(b, g, e, f) {
    $("body").find("#window_info_bg").remove();
    var i = $(window).height();
    var l = $(document).width();
    var a = $("#" + b);
    a.data("id-winInfo", b).addClass("window_info");
    if (typeof e !== "undefined") {
        e(f);
    }
    var c = a.height();
    var j = a.width();
    var k = $(document).scrollTop() + ((i - c) / 2);
    var d = ((l - j) / 2);
    k = (k < 35) ? 35 : k;
    a.css("top", k);
    a.css("left", d);
    $("body").append('<div id="window_info_bg"></div>');
    if (g !== "") {
        a.find("." + g).on("click", function() {
            $("#window_info_bg").fadeOut(200);
            a.hide();
            a.removeClass("window_info").attr("id", a.data("id-winInfo"));
        });
    }
    if (a.hasClass("just_btn_close") === false) {
        $("#window_info_bg").on("click", function() {
            $("#window_info_bg").fadeOut(200);
            a.hide();
            a.removeClass("window_info").attr("id", a.data("id-winInfo"));
        });
    }
    $("#window_info_bg").fadeIn(200, function() {
        a.show();
    });
}

function windows_info(d, b, c, a) {
    if ($("#" + d).length) {
        content_window(d, b, c, a);
    }
}

function preFx(b, c, a) {
    if ($.isNumeric(b)) {
        b = b + "";
        a = $.isNumeric(a) ? parseInt(a) : 2;
        while (b.length < a) {
            b = c + "" + b;
        }
    }
    return b;
}

function animatIt(b) {
    var a = $(b).parent();
    if (a.find("i.animate-spin").length < 1) {
        a.append('<i class="fontico-spin1 animate-spin"></i>');
    }
    if (!a.hasClass("loading")) {
        a.addClass("loading");
    }
    return true;
}

function redirect(a) {
    location.href = a;
    window.location = a;
}
window.onload = function() {
    $("#list-images_0").css("pointer-events", "inherit");
};