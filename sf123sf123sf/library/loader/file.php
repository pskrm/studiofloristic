<?
ini_set("post_max_size","8192M");
ini_set("upload_max_filesize","8192M");
define('_SWS_ADMIN_CORE', true);

include_once('../../../system/config.php');
include_once('../../../system/db.php');
include_once('../../../system/filter.php');

$return = array('status'=>1);
$error = false;

if (empty($_GET['name']) || empty($_GET['field']) || empty($_GET['id'])) $error = '������ � �������';

if (!$error) {
	$module = filter(trim($_GET['name']), 'nohtml');
	$field = filter(trim($_GET['field']), 'nohtml');
	$id = abs(intval($_GET['id']));
}

if (!$error && $_FILES['Filedata']['size'] > 2097152) $error = '������ ����� �� ������ ��������� 2��!';

if (!$error) {
	$r = db_query('SELECT `id` FROM `sw_modules` WHERE `link` = '.quote($module).' AND `active` = 1');
	if (mysql_num_rows($r) != 0) {
		include_once('../../../modules/'.$module.'/admin/index.php');
		$pach = '../../../files/'.$module.'/'.$id.'/';
		$time = time();
		
		if (empty($_GLOBALS['field']['type'][$field])) $error = '������ � �������';
		
		if (!empty($_GLOBALS['field']['settings'][$field]['extension'])) {
			$extension = strtolower(substr(strrchr($_FILES['Filedata']['name'], '.'), 1));
			if (!in_array($extension, $_GLOBALS['field']['settings'][$field]['extension'])) $error = '���� �������� � ��������';
			
			if (!is_dir('../../../files/'.$module.'/')) mkdir('../../../files/'.$module.'/', 0755);
			if (!is_dir($pach)) mkdir($pach, 0755);
			
			$filename = md5($_FILES['Filedata']['name'].$time).'.'.$extension;
			copy($_FILES['Filedata']['tmp_name'], $pach.$filename);
		} else $error = '��� ����������� ��� �������� ����� ������';
	}
}

if ($error) {
	$return['status'] = 0;
	$return['error'] = iconv('windows-1251', 'utf-8', $error);
} else {
	if (empty($return['name'])) $return['name'] = $filename;
	$return['hash'] = md5_file($_FILES['Filedata']['tmp_name']);
	
	$r = db_query('SELECT `'.$field.'` FROM `sw_'.$module.'` WHERE `id` = '.quote($id));
	if (mysql_num_rows($r) != 0) {
		$v = mysql_fetch_array($r, MYSQL_NUM);
		$values = !empty($v[0]) ? explode(':', $v[0]) : array();
		$values[] = $filename;
	} else {
		$values = array($filename);
	}
	
	db_query('UPDATE `sw_'.$module.'` SET `'.$field.'` = '.quote(implode(':', $values)).' WHERE `id` = '.quote($id));
	
	$return['fName'] = '/sf123sf123sf/i/file-ico.png';
	$return['id'] = $id;
	$return['field'] = $field;
}

if (isset($_REQUEST['response']) && $_REQUEST['response'] == 'xml') {
	echo '<response>';
	foreach ($return as $key => $value) {
		echo "<$key><![CDATA[$value]]></$key>";
	}
	echo '</response>';
} else {
	echo json_encode($return);
}
?>