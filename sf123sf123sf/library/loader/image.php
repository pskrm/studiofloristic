<?
define('_SWS_ADMIN_CORE', true);

include_once('../../../system/config.php');
include_once('../../../system/db.php');
include_once('../../../system/filter.php');
include_once('class.upload.php');

$return = array('status'=>1);
$error = false;

if (empty($_GET['name']) || empty($_GET['field']) || empty($_GET['id'])) $error = '������ � �������';

if (!$error) {
	$module = filter(trim($_GET['name']), 'nohtml');
	$field = filter(trim($_GET['field']), 'nohtml');
	$id = abs(intval($_GET['id']));
}

if (!$error && $_FILES['Filedata']['size'] > 2097152) $error = '������ ����� �� ������ ��������� 2��!';

if (!$error) {
	$r = db_query('SELECT `id` FROM `sw_modules` WHERE `link` = '.quote($module).' AND `active` = 1');
	if (mysql_num_rows($r) != 0) {
		
		include_once('../../../modules/'.$module.'/admin/index.php');
		$pach = '../../../files/'.$module.'/'.$id.'/';
		$time = time();
		
		if (empty($_GLOBALS['field']['type'][$field])) $error = '������ � �������';
		
		$img = new Upload($_FILES['Filedata']['tmp_name']);
		if ($img->uploaded) {
			$img->file_new_name_body = md5($_FILES['Filedata']['name'].$time);
			$img->image_convert = 'jpeg';
			$img->Process($pach);
			
			$filename = $img->file_dst_name;
			
			if (!empty($_GLOBALS['field']['settings'][$field]['size'])) {
				foreach ($_GLOBALS['field']['settings'][$field]['size'] as $size) {
					if (!empty($size['width']) && !empty($size['height'])) {
						$fName = md5($_FILES['Filedata']['name'].$time);
						if (!empty($size['ratio'])) {
							$img->image_resize = true;
							$img->image_ratio_fill = 'C';
							$img->image_y = $size['height'];
							$img->image_x = $size['width'];
							$img->image_background_color = '#FFFFFF';
						} else {
							$img->image_resize = true;
							$img->image_ratio_crop = true;
							$img->image_y = $size['height'];
							$img->image_x = $size['width'];
						}
						$img->image_convert = 'jpeg';
						$img->file_new_name_body = $fName;
						$img->file_name_body_pre = 'w'.$size['width'].'_';
						
						if (!empty($size['watermark'])) {
							$img->image_watermark = 'watermark.png';
							$img->image_watermark_position = 'C';
						}
						
						$img->Process($pach);
						$return['rName'] = $fName.'.jpg';
					} elseif (!empty($size['width']) && empty($size['height'])) {
						$fName = md5($_FILES['Filedata']['name'].$time);
						$img->image_resize = true;
						$img->image_ratio_y = true;
						$img->image_x = $size['width'];
						$img->image_convert = 'jpeg';
						$img->file_new_name_body = $fName;
						$img->file_name_body_pre = 'w'.$size['width'].'_';
						$img->Process($pach);
						$return['rName'] = $fName.'.jpg';
					} elseif (empty($size['width']) && !empty($size['height'])) {
						$fName = md5($_FILES['Filedata']['name'].$time);
						$img->image_resize = true;
						$img->image_ratio_x = true;
						$img->image_y = $size['height'];
						$img->image_convert = 'jpeg';
						$img->file_new_name_body = $fName;
						$img->file_name_body_pre = 'w'.$size['width'].'_';
						$img->Process($pach);
						$return['rName'] = $fName.'.jpg';
					}
					if (!empty($size['width']) && $size['width'] == 120) $return['name'] = $img->file_dst_name;
				}
			}
			
			if (!$img->processed) $error = $img->error;
		} else $error = '������ ��� ��������';
	} else $error = '������ � �������';
}

if ($error) {
	$return['status'] = 0;
	$return['error'] = iconv('windows-1251', 'utf-8', $error);
} else {
	if (empty($return['name'])) $return['name'] = $filename;
	$return['hash'] = md5_file($_FILES['Filedata']['tmp_name']);
	
	$r = db_query('SELECT `'.$field.'` FROM `sw_'.$module.'` WHERE `id` = '.quote($id));
	if (mysql_num_rows($r) != 0) {
		$v = mysql_fetch_array($r, MYSQL_NUM);
		$values = !empty($v[0]) ? explode(':', $v[0]) : array();
		$values[] = $filename;
	} else {
		$values = array($filename);
	}
	
	db_query('UPDATE `sw_'.$module.'` SET `'.$field.'` = '.quote(implode(':', $values)).' WHERE `id` = '.quote($id));
	
	$return['fName'] = '/files/'.$module.'/'.$id.'/'.$return['name'];
	$return['id'] = $id;
	$return['field'] = $field;
	
	$img->Clean();
}

if (isset($_REQUEST['response']) && $_REQUEST['response'] == 'xml') {
	echo '<response>';
	foreach ($return as $key => $value) {
		echo "<$key><![CDATA[$value]]></$key>";
	}
	echo '</response>';
} else {
	echo json_encode($return);
}
?>