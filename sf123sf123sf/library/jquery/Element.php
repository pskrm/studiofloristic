<?php
class jQuery_Element
{
    public $s;
    public $m = array();
    public $a = array();
    public function __construct($selector)
    {
        jQuery::addElement($this); 
        $this->s = $selector;
    }
    
    public function __call($method, $args)
    {
        array_push($this->m, $method);
        array_push($this->a, $args);
        
        return $this;
    }
    
    public function end()
    {
        return new jQuery_Element($this->s);
    }
}