<?php
class inc_vars {
    static $types = array(
        'cards'=>'��������� ������: WalletOne',
        'cards-assist'=>'��������� ������: Assist',
        'cash'=>'���������',
        'cash-in-shop'=>'��������� � ��������',
        'card-in-shop'=>'������ � ��������',
        'yandex'=>'������ ������',
        'wm'=>'Web money',
        'paypal'=>'PayPal',
        'sbbank'=>'��������',
        'contact'=>'��������',
        'euroset'=>'�������',
        'courier'=>'����� ������� �� �������',
        'terminals'=>'��������� ���������',
        'onlinebanks'=>'�������� ����',
        'mobileapp'=>'��������� ����������',
        'mobileoperators'=>'��������� ���������',
        'emoney'=>'����������� ������',
        'cards-courier'=>'��������� ������ �������',
        'beznal'=>'������ �� �������'
    );

    static $offline_payments = array('sbbank', 'terminals', 'yandex', 'wm', 'contact', 'euroset');

    static $soc_nw_cols = array('facebook','vk','google','odnoklassniki','yandex','mailru');
}

