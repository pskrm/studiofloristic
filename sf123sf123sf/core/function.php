<?php
function treeSID($module = false, $sid = false, $ParentID = 0, $lvl = 0) {
	global $module_name, $module_id, $lvl, $_GLOBALS;
	
	$module = !$module ? $module_name : strtolower($module);
	if (!$sid) $sid = dbone("`sid`", "`id` = '".$module_id."'", $module_name);
	$lvl++;
	
	$result = db_query("SELECT `id`, `sid`, `title` FROM `"._MYSQL_PREFIX.$module."` WHERE `sid` = '".$ParentID."' AND `id` <> '".$module_id."' ORDER BY `title` ASC");
	if (mysql_num_rows($result) > 0) {
		while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
			$selected = $sid != $row['id'] ? '' : ' selected';
			$title = $lvl != 1 ? $row['title'] : $row['title'];
			$padding_left = $lvl > 1 ? $_GLOBALS['select_left'] * $lvl : 3;
			$HTML.= '<option style="padding-left: '.$padding_left.'px" value="'.$row['id'].'"'.$selected.'>'.$title.'</option>';
			$HTML.= treeSID(false, $sid, $row['id'], $lvl);
			$lvl--;
		}
	}
	return $HTML;
}

function treeSelectByID($fileld, $module = false, $current_id = false, $ParentID = 0, $lvl = 0, $max_lvl = false) {
	global $module_name, $module_id, $lvl, $_GLOBALS;
	
	$module = !$module ? $module_name : strtolower($module);
	if (!$current_id) $current_id = dbone("`".$fileld."`", "`id` = '".$module_id."'", $module_name);
	$lvl++;
	
	$result = db_query("SELECT `id`, `sid`, `title` FROM `"._MYSQL_PREFIX.$module."` WHERE `sid` = '".$ParentID."' ORDER BY `pos` DESC");
	if (mysql_num_rows($result) > 0) {
		while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
			$selected = $current_id != $row['id'] ? '' : ' selected';
			$title = $lvl != 1 ? $probel.$row['title'] : $row['title'];
			$padding_left = $lvl > 1 ? $_GLOBALS['select_left'] * $lvl : 3;
			$HTML.= '<option style="padding-left: '.$padding_left.'px" value="'.$row['id'].'"'.$selected.'>'.$title.'</option>';
			if ($max_lvl && $lvl < $max_lvl) $HTML.= treeSelectByID($fileld, $module, $current_id, $row['id'], $lvl, $max_lvl);
			$lvl--;
		}
	}
	return $HTML;
}

function GenerationContent ($id = 0, $module, $data = 'text') {
	foreach (explode(',', $data) as $value) {
		$field = substr(trim($value), 0, 1) == "`" ? trim($value) : "`".trim($value)."`";
		!isset($data_sql) ? $data_sql = $field : $data_sql .= ", ".$field;
	}
	$result = db_query("SELECT ".$data_sql." FROM `"._MYSQL_PREFIX.$module."` WHERE `id` = '".$id."'");
	if (mysql_num_rows($result) != 0) {
		$row = mysql_fetch_array($result, MYSQL_ASSOC);
		foreach ($row as $k=>$v) $row[$k] = stripslashes($v);
		return $row;
	}
}

// ���������, �� �� ��������
function GenerationTable($data) {
	global $_GLOBALS, $module_name, $module_id;
	
	$papa = $module_id != 0 ? dbone("`sid`", "`id` = '".$module_id."'", $module_name) : 0;
	
	$text  = '<form id="table_form_data" onsubmit="return false;">';
	$text .= '	<input type="hidden" name="module_name" value="'.$module_name.'">';
	$text .= '	<input type="hidden" name="module_id" value="'.$module_id.'">';
	$text .= '	<input type="hidden" name="op" value="delete_items_table">';
	$text .= '	<table id="table_data">';
	$text .= '		<tr>';
	$text .= '			<th width="20" class="th"><input type="checkbox" class="pointer" name="select_all" onclick="items_select_all(this)"></th>';
	foreach ($_GLOBALS['current_field']['table'] as $key=>$value) {
		switch ($key) {
            case 'active': { $text.= '<th class="th"></th>'; } break;
			default: { $text.= '<th class="th">'.$_GLOBALS['current_field']['title'][$key].'</th>'; } break;
		}
	}
	$text .= '</tr>';
	
	if ($module_id != 0) {
		$text .= '<tr>';
		$text .= '<td></td>';
		$text .= '<td colspan="'.count($_GLOBALS['current_field']['table']).'"><a href="javascript:void(0)" onclick="LoadData(\'load_dir\', \''.$module_name.'\', '.$papa.')"><img src="/'._ADMIN_DIR.'/i/back.png" align="left">&nbsp;<b>..</b></a></td>';
		$text .= '</tr>';
	}
	
	foreach ($data as $index=>$row) {
		$checkbox = $row['is_system'] != 0 ? '<input type="checkbox" disabled>' : '<input type="checkbox" id="chek_'.$index.'" class="pointer" name="table_data[]" value="'.$row['id'].'" onchange="Change(this)">';
		$text .= '<tr id="tr_'.$index.'">';
		$text .= '<td width="20">'.$checkbox.'</td>';
		foreach ($row as $key=>$value) {
			switch ($_GLOBALS['current_field']['table'][$key]) {
				case 'active': {
					$img_active = $row['active'] == 0 ? '<img id="active_'.$index.'" src="i/lock.gif" alt="LOCK">' : '<img id="active_'.$index.'" src="i/unlock.gif" alt="UNLOCK">';
					$text .= '<td class="active"><a href="javascript:void(0)" onclick="LoadData(\'active\', \''.$module_name.'\', '.$row['id'].')">'.$img_active.'</a></td>';
				} break;
				case 'position': {
					if (count($data) > 1) {
						$top = '<a href="javascript:void(0)" onclick="LoadData(\'pos_top\', \''.$module_name.'\', '.$row['id'].')"><img src="i/top.gif" align="left"></a>';
						$bottom = '<a href="javascript:void(0)" onclick="LoadData(\'pos_bottom\', \''.$module_name.'\', '.$row['id'].')"><img src="i/bottom.gif" align="left"></a>';
						if ($index == 0) $top = '<img src="i/top_block.gif" align="left">';
						if (count($data)-1 == $index) $bottom = '<img src="i/bottom_block.gif" align="left">';
					} else {
						$top = '<img src="i/top_block.gif" align="left">';
						$bottom = '<img src="i/bottom_block.gif" align="left">';
					}
					$text .= '<td width="35" align="center" valign="center">'.$top.$bottom.'</td>';
				} break;
				case 'title': {
					$ico = isset($row['count_sub_dir']) && $row['count_sub_dir'] != 0 ? '<a href="javascript:void(0)" onclick="LoadData(\'load_dir\', \''.$module_name.'\', '.$row['id'].')"><img align="left" src="i/folder.gif"></a>' : '<a href="javascript:void(0)" onclick="LoadData(\'edit_item\', \''.$module_name.'\', '.$row['id'].')"><img align="left" src="i/file.gif"></a>';
					$text.= '<td>'.$ico.'&nbsp;<a href="javascript:void(0)" onclick="LoadData(\'edit_item\', \''.$module_name.'\', '.$row['id'].')" class="title_link">'.$value.'</a></td>';
				} break;
                case 'text': { $text.= '<td>'.(!empty($value) ? $value : '<font color="#ccc">�� �������</fonr>').'</td>'; } break;
                case 'datetime': { $text.= '<td>'.date('d.m.Y � H:i:s', $value).'</td>'; break; }
                case 'textarea': { $text.= '<td>'.str_replace("\n", "<br />", $value).'</td>'; } break;
                case 'email': { $text.= '<td><a href="mailto:'.$value.'">'.$value.'</a></td>'; } break;
                case 'date': { $text.= '<td>'.((!empty($value) && preg_match("/^([0-9]{4})\-([0-9]{1,2})\-([0-9]{1,2})$/i", $value, $date))? $date[3].'.'.$date[2].'.'.$date[1] : $value).'</td>'; } break;
                case 'gl': { $text.= '<td>'.(intval($value) != 0 ? '<font color="red"><b>���.</b></font>' : '<font color="green" class="pointer" onclick="FastEditField(\''.$module_name.'\', '.$row['id'].', \''.$key.'\', 1)"><b>����.</b></font>').'</td>'; } break;
				case 'checkbox': {
					$text .= '<td>'.
                        (intval($value) != 0 
                        ? '<font color="red" class="pointer" onclick="FastEditField(\''.$module_name.'\', '.$row['id'].', \''.$key.'\', 0)"><b>���.</b></font>'
                        : '<font color="green" class="pointer" onclick="FastEditField(\''.$module_name.'\', '.$row['id'].', \''.$key.'\', 1)"><b>����.</b></font>')
                    .'</td>';
				} break;
                case 'datetime': { $text .= '<td style="color: #666666">'.date('d.m.Y � H:i:s', $value).'</td>'; } break;
			}
		}
		$text .= '</tr>';
		$index++;
	}
	$text .= '</table>';
	$text .= '</form>';
	return $text;
}

function save_file_name($pach, $name) {
	$type=substr($name, -3);
	$type=strtolower($type);
	$max_number=0;
	if (is_dir($pach)) {
		if ($dh=opendir($pach)) {
			while (($file=readdir($dh))!==false) {
				if (filetype($pach.$file)=="file") {
					if (ereg("file_([0-9]*).".$type,$file,$number)) {
						if ($max_number==0) $max_number=$number[1];
						elseif ($number[1]>$max_number) $max_number=$number[1];
					}
				}
			}
			closedir($dh);
		}
	}
	$max_number++;
	return 'file_'.$max_number.'.'.$type;
}

function FolderDelete($folderPath) {   
	if (is_dir($folderPath)) {
		foreach (scandir($folderPath) as $value) {
			if ($value != "." && $value != "..") {
				$value = $folderPath . "/" . $value;
				if (is_dir($value))	{
					FolderDelete($value);
				} elseif (is_file($value)) {
					@unlink($value);
				}
			}
		}
		return rmdir($folderPath);
	} else {
		return FALSE;
	}
}

function AdminPageNavigator($count_items, $pg, $items_on_page = 20) {
	global $module_name, $module_id, $op;
	
	$pages_to_show = 6;
	$page_coll = ceil($count_items / $items_on_page);
	$half_pages_to_show = round($pages_to_show/2);
	
	if ($page_coll > 1) {
		$text = '';
		if ($pg >= $pages_to_show - 1) $text .= '<a href="javascript:void(0)" onclick="DataPaging(\'load_dir\', \''.$module_name.'\', '.$module_id.', 1)">������ ��������</a> | ';
		for ($i = $pg - $half_pages_to_show; $i <= $pg + $half_pages_to_show; $i++) {
			$link = '<a href="javascript:void(0)" onclick="DataPaging(\'load_dir\', \''.$module_name.'\', '.$module_id.', '.$i.')">'.$i.'</a>';
			if ($i >= 1 && $i <= $page_coll) {
				if ($i == $pg) {
					if ($i != $page_coll) {
						$text.= $i <= $pg + $half_pages_to_show - 1 ? $i.' | ' : $i;
					} else $text.= $i;
				} else {
					if ($i != $page_coll) {
						$text.= $i <= $pg + $half_pages_to_show - 1 ? ' '.$link.' | ' : ' '.$link;
					} else $text.= ' '.$link;
				}
			}
		}
		if (($pg + $half_pages_to_show) < $page_coll) $text .= ' | <a href="javascript:void(0)" onclick="DataPaging(\'load_dir\', \''.$module_name.'\', '.$module_id.', '.$page_coll.')">��������� ��������</a>';
	} else $text = '';
	return '<div id="page_navigator" class="bottom">'.$text.'</div>';
}

function HeaderPage($page = '/') {
	header('location: '.$page);
	exit();
}

function RusToEng($st) {
	$st = strtolower(trim(strtr($st, ",.!@#$%^&()+=/:;<>?-��[]��", "                          ")));
	$st = preg_replace('/ +/', ' ', $st);
	$st = str_replace('"', '', $st);
	$st = strtr($st, ' ', '-');
	$st = strtr($st, "�����Ũ��������������������������", "��������������������������������");
	$st = strtr($st, "������������������������", "abvgdeeziyklmnoprstufh'ie");
	$st = strtr($st, array('�'=>'zh', '�'=>'ts', '�'=>'ch', '�'=>'sh', '�'=>'shch','�'=>'', '�'=>'yu', '�'=>'ya'));
	return $st;
}

function get_currency($currency=false){
    $currency = ($currency===false)?filter(trim($_COOKIE['cur']), 'nohtml'):$currency;
    $cur = query_new('SELECT * FROM `sw_currency` WHERE `name` = '.quote($currency),1);
    if ($cur===false) {
        $cur = query_new('SELECT * FROM `sw_currency` WHERE `name` = "rub"',1);
        setcookie('cur', 'rub' , null,'/',_CROSS_DOMAIN);
    }
    return $cur;
}

function get_user_orders($uid = 0, $tables = 'orders'){
    if (!empty($tables)){
        if (!is_array($tables)){
            $tables = array(strval($tables));
        }
        $queries = array();
        foreach ($tables as $table){
            $queries[] = 'SELECT `id`, '.quote($table).' as module FROM `'._MYSQL_PREFIX.$table.'` WHERE `uid` = '.quote($uid);
        }
        if ($queries>1){
            $start = '(';
            $glue = ') UNION (';
            $end = ')';
        }else{
            $start = '';
            $glue = '';
            $end = '';
        }
        $orders = query_new($start.implode($glue, $queries).$end.' ORDER BY `id` DESC');
        if (!empty($orders)){
            return $orders;
        }
    }
    return false;
}

// �������� �������� ������ ������������
function get_user_disc($uid = 0)
{
    $ret = query_new("(SELECT scu.`uid`, 'coup' AS `type`, sc.`value` AS `val`, sc.`title` FROM sw_coupons_users scu LEFT JOIN sw_coupons sc ON sc.`id`=scu.`cid` WHERE scu.uid = ".$uid.") UNION "
            . "(SELECT `user-id` AS `uid`, 'card' AS `type`, `value` AS `val`, `title` FROM sw_cards WHERE `user-id` = ".$uid." AND activation = 1) ORDER BY val DESC LIMIT 1",1);

    return $ret;
}

function get_user_soc_nw($uid = 0,$soc = ''){
    if (!empty($soc)){
        if (!is_array($soc)){
            $soc = array(strval($soc));
        }
        $sql_soc = array();
        foreach ($soc as $s){
            $sql_soc[] = 'SELECT `'.$s.'` AS `soc_id`, "'.$s.'" AS `type` FROM sw_users WHERE `id` = '.$uid.' AND `'.$s.'`!=""';
        }
        return query_new('('.  implode(') UNION (', $sql_soc).')');
    }
    return false;
}