<?
if (!empty($_POST['name']) && !empty($_POST['password'])) {
	$name = filter(trim($_POST['name']), 'nohtml');
	$pass = md5(filter(trim($_POST['password']), 'nohtml'));

	$r = db('`id`', '`title` = '.quote($name).' AND `password` = '.quote($pass).' AND `active` = 1', 'admin');
	if (mysql_num_rows($r) != 0) {
		$admin = mysql_fetch_array($r, MYSQL_ASSOC);
		$_SESSION['admin'] = base64_encode($admin['id'].':'.$name.':'.$pass);
	}
	
	header('location: /'._ADMIN_DIR.'/');
	exit();
}

define('_IS_ADMIN', is_admin() ? true : false);

function is_admin() {
	if (empty($_SESSION['admin'])) {
		return false;
	} elseif (!is_array($_SESSION['admin'])) {
		$_SESSION['admin'] = explode(':', addslashes(base64_decode($_SESSION['admin'])));
	}
	
	if (count($_SESSION['admin']) == 3) {
		if (empty($_SESSION['admin'][0]) || empty($_SESSION['admin'][1]) || empty($_SESSION['admin'][2])) return false;
		$pass = dbone('`password`', '`id` = '.quote($_SESSION['admin'][0]).' AND `title` = '.quote($_SESSION['admin'][1]), 'admin');
		if (!empty($pass) && $pass == $_SESSION['admin'][2]) {
			return true;
		}
	}
	
	return false;
}
?>