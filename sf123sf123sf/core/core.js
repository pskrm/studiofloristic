jQuery = jQuery.noConflict();

// For todays date;
Date.prototype.today = function () {
    return ((this.getDate() < 10)?"0":"") + this.getDate() +"/"+(((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1) +"/"+ this.getFullYear();
}

// For the time now
Date.prototype.timeNow = function () {
    return ((this.getHours() < 10)?"0":"") + this.getHours() +":"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes() +":"+ ((this.getSeconds() < 10)?"0":"") + this.getSeconds();
}

jQuery(document).on('ready', function() {


	jQuery('#header .log-off').click(function(){
		initGET('/go/op=exit');
	});
	jQuery('#header .help').click(function(){
		initSysDialog('� ����������');
	});

	jQuery('body').on('click', '.popup', function() {
		initDialogClose(jQuery(this).attr('id'));
	});

	jQuery('body').on('click', '.dialog-box', function(event){
	    event.stopPropagation();
	});

    initGET('/go/op=load-menu');
	// ��������� ���������� ����� �� ���������

	var newOrderTimer  = setInterval(function() {
		try {
			initGETquiet('/go/op=check-new-orders');
		}catch (e){
			console.log('�����-�� ��������... ������: ' + e.name);
		}

	}, 30000);

	// ��������� �������� ������� ����������

  jQuery.getJSON( "/go/op=getordersreloadtimer", function(result_timer) {

  	if (parseInt(result_timer )>0) {
        var reloadModuleTimer = setInterval(function () {
            if ((module.name == 'orders') && (module.state != 'edit')) {
                try {
                    var newDate = new Date();
                    if (window.scrollY <= 20) {
                        module.load(module.name);
                        console.log("module orders updated at " + newDate.today() + "@" + newDate.timeNow());
                    }

                } catch (e) {
                    console.log('�����-�� ��������... ������: ' + e.name);
                }
            }
        }, parseInt(result_timer )); // ���� ���� ������
    }
  });


});

function goUrl(url) {
	location = url;
}
function list_items_all(el){
    jQuery('fieldset.section_items table input[type="checkbox"]').prop('checked',jQuery(el).prop('checked'));
}

var usrnotify = {
	prepare: function (e){
        var url = '/go/m=notify&op=notify-prepare';
        initGET(url);

	},
	open: function (crc){
        var url = '/system/mailpages/readmail.php?hash=' + crc;
        var win = window.open(url, '_blank');
        win.focus();
	}

}

var sitemap = {
    fileCreate: function(){
        jQuery('td#sm_s form input').prop('disabled',true);
        jQuery('td#sm_s').removeClass('off').addClass('on');
        var url = '/go/m='+module.name+'&op=file-create';
        if (jQuery('td#sm_s form input[name="check_status"]').prop('checked')){
            url = url+'&check_status=on';
        }
        initGET(url);
    },
    runCheck: function(){
        window.smTimer = window.setInterval(function(){
            sitemap.statusCheck();
        },2000);
    },
    statusCheck: function (){
        initGET('/go/m='+module.name+'&op=get-status');
    },
    stopCheck: function (){
        window.clearInterval(window.smTimer);
        jQuery('td#sm_s').removeClass('on').addClass('off');
        jQuery('#sm_s form input').prop('disabled',false);
    }
};
var instagramm = {
    update: function(){
        initGET('/go/m='+module.name+'&op=inst_photo_update');
    }
}
var items = {
	counts: 0,
	init: function(e) {
		items.counts = e.length;
		for (i=0;i<items.counts;i++) {
			jQuery('table#list-items').append('');
		}
	},
	remove: function(id) {
		jQuery('body').append('<table id="remove-item" class="popup"><tr><td><div class="dialog-box"><div class="title">�������� ��������</div><div class="data"><p>�� ������������ ������ ������� �������?</p><p class="right buttons"><input id="remove" type="button" class="b-textfield" value="��, �������"><input id="cancel" type="button" class="b-textfield" value="������"></p></form></div></div></td></tr></table>');
		jQuery('table#remove-item input#cancel').click(function(){
			initDialogClose('remove-item');
		});
		jQuery('table#remove-item input#remove').click(function(){
			initGET('/go/m='+module.name+'&op=item-remove&id='+id);
		});
		jQuery('table#remove-item').fadeIn(500);
	},
    removeDone: function(id){
        initDialogClose("remove-item");
        if (id !== undefined){
            initSysDialog("<font size=\"3\">�������� ��������� ������</font>");
            jQuery('tr#i'+id).fadeOut(500,function(){
                jQuery(this).remove();
                items.odd("table#list-items");
            });
        } else{
            initSysDialog("<font size=\"3\">������ ��� ��������</font>");
            jQuery("table#list-items tr").show();
        }
    },
    odd: function(sel){
        jQuery(sel).find('tr:not(.title)').removeClass('odd green').filter(':odd').each(function(){
            jQuery(this).addClass('odd green');
        });
    }
}

var date = {
	init: function(exec) {
		// if (jQuery('link[href="include/ui/themes/base/jquery.ui.core.css"]').length==0){
		// 	loadCSS('/include/ui/themes/base/jquery.ui.core.css');
		// 	loadCSS('/include/ui/themes/base/jquery.ui.datepicker.css');
		// 	loadCSS('/include/ui/themes/base/jquery.ui.theme.css');
		// }
		
		// jQuery.getScript('/include/ui/jquery.ui.core.js', function() {
		// 	jQuery.getScript('/include/ui/jquery.ui.widget.js');
		// 	jQuery.getScript('/include/ui/jquery.ui.datepicker.js');
		// });
	},
	load: function(e) {
        jQuery(function (jQuery) {
            jQuery.datepicker.regional['ru'] = {
                closeText: '�������',
                prevText: '&#x3c;����',
                nextText: '����&#x3e;',
                currentText: '�������',
                monthNames: ['������', '�������', '����', '������', '���', '����',
                    '����', '������', '��������', '�������', '������', '�������'],
                monthNamesShort: ['������', '�������', '����', '������', '���', '����',
                    '����', '������', '��������', '�������', '������', '�������'],
                dayNames: ['�����������', '�����������', '�������', '�����', '�������', '�������', '�������'],
                dayNamesShort: ['���', '���', '���', '���', '���', '���', '���'],
                dayNamesMin: ['��', '��', '��', '��', '��', '��', '��'],
                weekHeader: '���',
                dateFormat: 'dd.mm.yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            };
            jQuery.datepicker.setDefaults(jQuery.datepicker.regional['ru']);
        });

		jQuery(e).datepicker({
            dateFormat: "dd.mm.yy"
		},  jQuery.datepicker.regional['ru']);
	}
}
var order = {
	box: ['����� (���������)', '�������� ����� (50 ���.)', '����������� �������� (200 ���.)', '���������� �������� (250 ���.)'],
	view: function(id) {
		module.setState('edit');
        var module_name = module.name;

        // ������� ��������
        if (module_name == 'analytics' || module_name == 'logevents' || module_name == 'notify'  )
        {
            module_name = 'orders';
        }

		jQuery('body').append('<table id="order-view" class="popup"><tr><td class="cData"><div class="overflow">\n\
    <div class="dialog-box" style="width:600px;">\n\
        <div class="title">���������� � ������: �'+id+'</div>\n\
        <div id="orderView_data" class="data" style="min-height:100px;">\n\
            <p>�������� ������...</p>\n\
        </div>\n\
        <div class="data_butn"><p class="right buttons"><input type="button" class="b-textfield" value="�������" id="cancel"></p></div>\n\
        <div class="acts">\n\
            <!--a href="/acts.php?t=client&id='+id+'" title="��������� � ����� ����" target="_blank">��� ��� �������</a>\n\
            <a href="/acts.php?t=courier&id='+id+'" title="��������� � ����� ����" target="_blank">��� ��� ��������</a-->\n\
            <a href="/go/m='+module_name+'&op=view-order&task=acts&t=client&id='+id+'" title="��������� � ����� ����" target="_blank">��� ��� �������</a>\n\
            <a href="/go/m='+module_name+'&op=view-order&task=acts&t=courier&id='+id+'" title="��������� � ����� ����" target="_blank">��� ��� ��������</a>\n\
        </div></div></div></td></tr></table>');
		jQuery('table#order-view input#cancel').click(function() {
			jQuery('table#order-view').fadeOut(500, function() {
				jQuery(this).remove();
			});
		});
		jQuery('table#order-view').fadeIn(500, function() {

			initGET('/go/m='+module_name+'&op=view-order&id='+id);
		});
	},
	display: function(v) {
		var html = '<p><b>���� � ����� ��������:</b> '+v.delivery+'</p>';
		html+= '<p><b>����� ������:</b> '+v.amount+' ���.</p>';
		html+= '<p><b>���-�� �������:</b> '+v.count+' ��.</p>';
		html+= '<p><b>��������:</b> '+v.dostavka+'</p>';
		html+= '<p><b>������:</b> '+v.discount+'%</p>';
        html+= '<p><b>�������:</b> '+v.markup+'%</p>';
        html+= v.other;
		html+= '<hr />';
		html+= htmlspecialchars_decode(v.recipient);
		html+= '<hr />';
		html+= htmlspecialchars_decode(v.uData);
		var c = v.item.length;
		if (c > 0) {
			html+= '<hr /><p><b>���������� ������:</b></p><p><table class="basket-items"><tr><th class="title">������������</th><th>���-��</th><th>����</th><th>�����</th><tr>';
			for(var i=0;i<c;i++){
				var summa = Math.ceil(v.item[i].price * (1 + v.markup / 100));
                summa = v.item[i].count * Math.ceil(summa * (1 - v.discount / 100));
				html+= '<tr><td class="title"><a href="'+v.item[i].url+'" '+(v.item[i].type == 'item'? 'target="_blank"':'')+'>'+v.item[i].name+'</a>'
                        +(v.item[i].box != 0 ? ' + '+order.box[v.item[i].box] : '')
                        +(v.item[i].vase != 0 ? ' + ����' : '')
                        +(v.item[i].type != 'item' && v.item[i].type != 'gift' ? ' | <a href="javascript:order.item('+v.item[i].id+')">��������</a>' : (v.item[i].type == 'gift'?' (�������)':''))
                        +'</td><td>'+v.item[i].count+'��</td><td>'+v.item[i].price+'�</td><td>'+summa+'�</td></tr>';
			}
			html+= '</table></p>';
		}
		
		if (v.comment) html+= '<p><b>�����������:</b> '+htmlspecialchars_decode(v.comment)+'</p>';
		
		jQuery('table#order-view div.data').html(html+'<p class="right buttons"><input type="button" class="b-textfield" value="�������" id="cancel"></p>');
		
		if (jQuery(window).height() > jQuery('table#order-view div.dialog-box').height()) {
			jQuery('table#order-view div.overflow').css('height', 'auto');
		} else {
			jQuery('table#order-view div.overflow').css('height', jQuery(window).height());
			jQuery('table#order-view div.dialog-box').css('margin', '20px auto');
		}
		
		jQuery('table#order-view input#cancel').click(function() {
			jQuery('body').css('overflow','auto');
			jQuery('table#order-view').fadeOut(500, function() {
				jQuery(this).remove();
			});
		});
	},
	item: function(id) {
		initGET('/go/m=orders&op=order-item&id='+id);
	},
	itemView: function(o) {
        console.log(o.text);
		jQuery('body').append('<table id="order-item-view" class="popup"><tr><td><div class="dialog-box"><div class="title">�������� ������ ������</div><div class="data">'+htmlspecialchars_decode(o.text)+'<p class="right buttons"><input id="cancel" type="button" class="b-textfield" value="�������"></p></div></div></td></tr></table>');
		jQuery('table#order-item-view').css('z-index', 1001);
		
		jQuery('table#order-item-view input#cancel').click(function() {
			jQuery('table#order-item-view').fadeOut(500, function() {
				jQuery(this).remove();
			});
		});
		
		jQuery('table#order-item-view').fadeIn(500);
	}
}

var pricing = {
	remove: function(id) {
		jQuery('body').append('<table id="pricing-remove" class="popup"><tr><td><div id="file-uploader" class="dialog-box"><div class="title">�������� �������</div><div class="data"><p>������� ��� ������?</p><p class="right buttons"><input id="remove" type="button" class="b-textfield" value="�������"><input id="cancel" type="button" class="b-textfield" value="������"></p></div></div></td></tr></table>');
		
		jQuery('table#pricing-remove input#cancel').click(function() {
			jQuery('table#pricing-remove').fadeOut(500, function() {
				jQuery(this).remove();
			});
		});
		
		jQuery('table#pricing-remove input#remove').click(function() {
			initGET('/go/m=pricing&op=pricing-remove&id='+id);
			jQuery('table#pricing-remove input#cancel').trigger('click');
			jQuery('tr#p'+id).fadeOut(500, function() {
				jQuery(this).remove();
			});
		});
		
		jQuery('table#pricing-remove').fadeIn(500);
	},
	clear: function() {
		jQuery('form#filter-pricing option').removeAttr('selected');
		formSubmit(jQuery('form#filter-pricing'));
	},
	back: function() {
		if (jQuery('form#back-pricing option:selected').size() == 0) {
			initSysDialog('�� �� ������� �� ������ �������');
			return;
		}
		
		jQuery('body').append('<table id="pricing-back" class="popup"><tr><td><div id="file-uploader" class="dialog-box"><div class="title">������� ����</div><div class="data"><p>���� ������� ������� ������� ��������� ����� ���������� � ����� ��� ����������! ���������?</p><p class="right buttons"><input id="yes" type="button" class="b-textfield" value="��, ���������"><input id="cancel" type="button" class="b-textfield" value="������"></p></div></div></td></tr></table>');
		
		jQuery('table#pricing-back input#cancel').click(function() {
			jQuery('table#pricing-back').fadeOut(500, function() {
				jQuery(this).remove();
			});
		});
		
		jQuery('table#pricing-back input#yes').click(function() {
			formSubmit(jQuery('form#back-pricing'));
			jQuery('table#pricing-back input#cancel').trigger('click');
			jQuery('form#back-pricing option').removeAttr('selected');
			initSysDialog('���� ����������');
		});
		
		jQuery('table#pricing-back').fadeIn(500);
	},
	start: function() {
		jQuery('body').append('<table id="pricing-start" class="popup"><tr><td><div class="dialog-box"><div class="title">��������� ��������� ����</div><div class="data"><p>�� ������������� ������ ��������� ����� ��������� ���� ��� ���� �������?</p><p class="right buttons"><input id="remove" type="button" class="b-textfield" value="��, ���������"><input id="cancel" type="button" class="b-textfield" value="������"></p></form></div></div></td></tr></table>');
		
		jQuery('table#pricing-start input#cancel').click(function(){
			initDialogClose('pricing-start');
		});
		
		jQuery('table#pricing-start input#remove').click(function(){
			initGET('/go/m=pricing&op=start');
			initDialogClose('pricing-start');
		});
		
		jQuery('table#pricing-start').fadeIn(500);
	}
}

var type = {
	insert: function(id) {
		jQuery('body').append('<table id="insert-type" class="popup"><tr><td><div class="dialog-box"><div class="title">���������� ������ ����</div><div class="data"><form action="/go/m=type&op=insert&id='+id+'" onsubmit="formSubmit(this); return false;"><p class="item"><label>������������*:</label><input type="text" name="title" class="f-field"></p><p class="item"><label>����*:</label><input type="text" name="price" class="f-field"></p><p class="item"><label>������ ����:</label><input type="text" name="price-old" class="f-field"></p><p class="item"><label>������:</label><input type="text" name="size" class="f-field"></p><p class="item"><label>������ ������:</label><textarea name="composition" rows="5" class="f-field"></textarea></p></form><p class="right buttons"><input type="button" class="b-textfield" value="���������" id="save"><input type="button" class="b-textfield" value="�������" id="cancel"></p></div></div></td></tr></table>');
		jQuery('table#insert-type input#cancel').click(function() {
			jQuery('table#insert-type').fadeOut(500, function() {
				jQuery(this).remove();
			});
		});
		jQuery('table#insert-type input#save').click(function() {
			formSubmit(jQuery('table#insert-type form'));
		});
		jQuery('table#insert-type').fadeIn(500);
	},
	edit: function(v) {
		jQuery('body').append('<table id="edit-type" class="popup"><tr><td><div class="dialog-box"><div class="title">�������������� ����</div><div class="data"><form action="/go/m=type&op=edit&id='+v.id+'" onsubmit="formSubmit(this); return false;"><p class="item"><label>������������*:</label><input type="text" name="title" class="f-field" value="'+v.title+'"></p><p class="item"><label>����*:</label><input type="text" name="price" class="f-field" value="'+v.price+'"></p><p class="item"><label>������ ����:</label><input type="text" name="price-old" class="f-field" value="'+v.priceOld+'"></p><p class="item"><label>������:</label><input type="text" name="size" class="f-field" value="'+v.size+'"></p><p class="item"><label>������ ������:</label><textarea name="composition" rows="5" class="f-field">'+v.composition+'</textarea></p></form><p class="right buttons"><input type="button" class="b-textfield" value="���������" id="save"><input type="button" class="b-textfield" value="�������" id="cancel"></p></div></div></td></tr></table>');
		jQuery('table#edit-type input#cancel').click(function() {
			jQuery('table#edit-type').fadeOut(500, function() {
				jQuery(this).remove();
			});
		});
		jQuery('table#edit-type input#save').click(function() {
			formSubmit(jQuery('table#edit-type form'));
		});
		jQuery('table#edit-type').fadeIn(500);
	},
	create: function(item) {
		jQuery('table#insert-type').fadeOut(500, function() {
			jQuery(this).remove();
		});
		jQuery('table.types').append('<tr id="type-'+item.id+'" class="hidden"><td class="title"><a href="javascript:void(0)" onclick="initGET(\'/go/m=type&op=edit&id='+item.id+'\')">'+item.title+'</a></td><td>'+htmlspecialchars_decode(item.composition)+'</td><td>'+item.price+' ���.</td><td>'+item.priceOld+' ���.</td><td>'+item.size+'</td><td class="options">'+(item.first!=0?'�� ���������':'<a href="javascript:type.first('+item.cid+','+item.id+')">�� ���������</a>')+' | <a href="javascript:type.remove('+item.cid+','+item.id+')">�������</a></td></tr>');
		jQuery('table.types tr#type-'+item.id).fadeIn(500);
	},
	first: function(cid,id) {
		initGET('/go/m=type&op=first&id='+id+'&cid='+cid);
	},
	remove: function(id,tid) {
		jQuery('table.types tr#type-'+tid).fadeOut(500, function() {
			jQuery(this).remove();
		});
		initGET('/go/m=type&op=remove&id='+id+'&tid='+tid);
	}
}

var htmlEditor = {
	init: function() {
		jQuery.getScript('/sf123sf123sf/library/tiny_mce/jquery.tinymce.js');
	},
	load: function(t,p) {
		console.log(p);
		jQuery('textarea#tiny-'+t).tinymce({
			script_url: '/sf123sf123sf/library/tiny_mce/tiny_mce.js',
			theme: 'advanced',
			language: 'ru',
			plugins: 'safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,filemanager',
			theme_advanced_buttons1: 'bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect',
			theme_advanced_buttons2: 'cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor',
			theme_advanced_buttons3: 'tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen',
			theme_advanced_buttons4: 'insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak',
			theme_advanced_toolbar_location: 'top',
			theme_advanced_toolbar_align: 'center',
			theme_advanced_statusbar_location: 'bottom',
			theme_advanced_resizing: true,
			extended_valid_elements: 'noindex',
			convert_urls: false,
			height: '400',
			filemanager_rootpath: '../../../../..' + p
		});
	}
}

var conv = {
	rus: [' ','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','\�','�','�','�', ' ', '\'', '\"', '\#', '\$', '\%', '\&', '\*', '\,', '\:', '\;', '\<', '\>', '\?', '\[', '\]', '\^', '\{', '\}', '\|', '\!', '\@', '\(', '\)', '\-', '\=', '\+', '\/', '\\', '�', '�', '�', '�'],
	eng: ['-','a','b','v','g','d','e','jo','zh','z','i','j','k','l','m','n','o','p','r','s','t','u','f','h','ch','c','sh','csh','e','ju','ja','y','', '', ' ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
	to: null,
	go: function(s) {
		this.to = '';
		s = s.replace(/^\s+/, '').replace(/\s+$/, '').replace(/[ \t]{2,}/g, ' ').toLowerCase();
		l = s.length;
		rc = this.rus.length;
		for (i=0;i<l;i++) {
			c = s.charAt(i,1);
			isRus = false;
			for (j=0;j<rc;j++) {
				if (c == this.rus[j]) {
					isRus = true;
					break;
				}
			}
			this.to+= (isRus) ? this.eng[j] : c;
		}
		
		return this.to;
	}
}

var alerts = {
	error: function(e) {
		switch (e) {
			case 1: initSysDialog('����� ��������� ��� ����'); break;
		}
	}
}

var sms = {
	view: function(i) {
		jQuery('form#fSendSMS input[name$="title"]').val(jQuery('tr#s'+i+' td.title a').html());
		jQuery('form#fSendSMS textarea[name$="text"]').val(jQuery('tr#s'+i+' td.text').html());
	},
	insert: function(s) {
		tr = jQuery('table#list-items tr:first').html();
		jQuery('table#list-items tr:first').remove();
		jQuery('table#list-items').prepend('<tr class="title">'+tr+'</tr><tr id="s'+s.id+'"><td class="title"><a href="javascript:sms.view('+s.id+')">'+s.title+'</a></td><td class="text">'+s.text+'</td><td class="btn"><div title="�������" class="delete" onclick="sms.remove('+s.id+')"></div></td></tr>');
		initSysDialog('��������� ���������� � ��������� � �������');
		jQuery('form#fSendSMS').resetForm();
	},
	count: function(t) {
		jQuery('font#count-str').html(jQuery(t).val().length);
	},
	remove: function(i) {
		initGET('/go/m=sms&op=remove&id='+i);
		jQuery('tr#s'+i).fadeOut(500, function() {
			jQuery(this).remove();
		});
	},
	myNumbers: function(t) {
		if (t) {
			jQuery('p#my-numers a').html('������ ������ �������').attr('href', 'javascript:sms.myNumbers(false)');
			jQuery('p#my-numers').append('<textarea name="numers" class="f-field" rows="5" style="width:100%;margin-top:4px;"></textarea>');
		} else {
			jQuery('p#my-numers a').html('�������� ������').attr('href', 'javascript:sms.myNumbers(true)');
			jQuery('p#my-numers textarea').remove();
		}
	},
	error: function(e) {
		switch (e) {
			case 1: initSysDialog('�� �� �������� ����� ���������'); break;
		}
	}
}

var email = {
	myNumbers: function(t) {
		if (t) {
			jQuery('p#my-numers a').html('������ ������ �������').attr('href', 'javascript:email.myNumbers(false)');
			jQuery('p#my-numers').append('<textarea name="emails" class="f-field" rows="5" style="width:100%;margin-top:4px;"></textarea>');
		} else {
			jQuery('p#my-numers a').html('�������� ������').attr('href', 'javascript:email.myNumbers(true)');
			jQuery('p#my-numers textarea').remove();
		}
	},
	remove: function(i) {
		initGET('/go/m=distribution&op=remove&id='+i);
		jQuery('tr#d'+i).fadeOut(500, function() {
			jQuery(this).remove();
		});
	},
	view: function(i) {
		initGET('/go/m=distribution&op=view&id='+i);
	},
	insert: function(s) {
		tr = jQuery('table#list-history tr:first').html();
		jQuery('table#list-history tr:first').remove();
		jQuery('table#list-history').prepend('<tr class="title">'+tr+'</tr><tr id="d'+s.id+'"><td class="title"><a href="javascript:email.view('+s.id+')">'+s.title+'</a></td><td class="date">'+s.date+'</td><td class="btn"><div title="�������" class="delete" onclick="email.remove('+s.id+')"></div></td></tr>');
		initSysDialog('��������� ���������� � ��������� � �������');
		jQuery('form#distribution-form').resetForm();
		jQuery("form#news-insert").resetForm();
	}
}

var module = {
	name: null,
	pg: 1,
	query: null,
	state: null,
	init: function(m) {
		this.load(jQuery('td#menu div.hover').attr('id'));
	},
	load: function(m) {
		this.pg = 1;
		this.query = null;
		this.name = m;
		this.menu();
		this.state = 'show';
		document.body.scrollTop = 0;
		initGET('/go/m='+m+'&op=load-module');
	},
	back: function() {
        this.state = 'show';
		initGET('/go/m='+this.name+'&op=load-module&pg='+this.pg+'&query='+this.query);
	},
	set: function(p,q) {
		this.pg = p;
		this.query = q;
	},
	search: function() {
		jQuery('div.search .query').select();
	},
    setState:function(s){
		this.state = s;
	},
	index: function() {
		initGET('/go/m='+this.name+'&op=load-module');
	},
    filter_data: function() {
    	this.setState('edit');
        var calendar_ot = document.getElementById('calendar_ot').value;
        var calendar_do = document.getElementById('calendar_do').value; 
        var tip_sortirovki = document.getElementById('tip_sortirovki').value;               
        var selected_shiping = document.getElementById('selected_shiping').value;
        var paytype = document.getElementById('pay-type').value;
        var timeranger = document.getElementById('timers-filter-id').value;
    //    var timefixed = document.getElementById('fixed-time').value;
      //  if (timefixed != '00:00') {
        //    initGET('/go/m=' + this.name + '&op=load-module&calendar_ot=' + calendar_ot + '&calendar_do=' +
          //      calendar_do + '&tip_sortirovki=' +
          ///      tip_sortirovki + '&filter_data=true&pay-type=' + paytype + '&fixed-time-filter=' + timefixed );
        //}else{
            initGET('/go/m=' + this.name + '&op=load-module&calendar_ot=' + calendar_ot + '&calendar_do=' +
                calendar_do + '&tip_sortirovki=' +
                tip_sortirovki + '&filter_data=true&pay-type=' + paytype  + '&time-range=' + timeranger +
                '&selected_shiping=' + selected_shiping
			);
		//}
    },
    filter_clear: function() {
        initGET('/go/m='+this.name+'&op=load-module&filter_clear=true');
        this.setState('view');
    },    
	menu: function() {
		jQuery('td#menu div.dir').removeClass('hover');
		jQuery('td#menu div#'+this.name).addClass('hover');
	},
	section: function(id,sid) {
		jQuery('div#current-topics span').html('��������...');
		initGET('/go/m='+module.name+'&op=section&id='+id+'&sid='+sid);
	},
    
}

var item = {
	field: null,
	title: function(i) {
		jQuery(i).blur(function() {
			if (jQuery('p#f-menu input:first').size() != 0 && jQuery('p#f-menu input:first').val().length == 0) jQuery('p#f-menu input:first').val(jQuery(this).val());
			if (jQuery('p#f-link input:first').size() != 0 && jQuery('p#f-link input:first').val().length == 0) jQuery('p#f-link input:first').val(conv.go(jQuery(this).val()));
		});
	},
	checked: function(i,e) {
		initGET('/go/m='+module.name+'&op=checkbox&id='+i+'&f='+jQuery(e).parent().attr('class'));
	},
	checkbox: function(i,e,v,t) {
		jQuery('tr#i'+i+' td.'+e).html('<span onclick="item.checked('+i+',this)" class="jLink '+(v!=0 ? ' off ' : '')+'">'+t+'</span>');
	},
	date: function() {
		
	}
}
var user = {
    setOrderH: function(){
        var h = 0;
        jQuery('table tr>td.user_fields>p').each(function(){
            h += jQuery(this).height() + 8;
        });
        jQuery('table tr>td.user_orders>div').height(h-50);
    }
}
var file = {
        list: [],
	id: 0,
	field: '',
	count: 0,
	max: 1,
	type: {},
	init: function(s) {
                file.list.push(s);
		file.id = s.id;
		file.field = s.field;
		file.type = s.type;
		if (s.max !== undefined) file.max = s.max;
//		delete s;
		
		switch (s.f) {
			case 'file': f = 'file.php'; break;
			case 'image': f = 'image.php'; break;
			default: return; break;
		}
		
		window.addEvent('domready', function() {
                    file.list.forEach(function(el){
                        var up = new FancyUpload2($(el.field+'-status'), $(el.field+'-list'), {
				verbose: true,
				url: '/sf123sf123sf/library/loader/'+f+'?name='+module.name+'&id='+el.id+'&field='+el.field,
				path: '/sf123sf123sf/library/loader/i/swiff_uploader.swf',
				typeFilter: el.type,
				target: el.field+'-browse',
				limitSize: 2097152,
				limitFiles: el.max,
				onLoad: function() {
					jQuery('#'+el.field+'-fallback').remove();
					this.target.addEvents({
						click: function() {
							return false;
						},
						mouseenter: function() {
							this.addClass('hover');
						},
						mouseleave: function() {
							this.removeClass('hover');
							this.blur();
						},
						mousedown: function() {
							this.focus();
						}
					});
					$(el.field+'-clear').addEvent('click', function() {
						el.count = 0;
						up.remove();
						return false;
					});
					$(el.field+'-upload').addEvent('click', function() {
						jQuery('div#'+el.field+'-status').slideDown('normal', function() {
							up.start();
							return false;
						});
					});
				},
				onComplete: function() {
					var timer = window.setInterval(function(){
						jQuery('div#'+el.field+'-status').slideUp('normal');
						window.clearInterval(timer);
					}, 1500);
				},
				onSelect: function(files) {
					el.count+= files.length;
				},
				onSelectFail: function(files) {
					files.each(function(file) {
						new Element('li', {
							'class': 'validation-error',
							html: file.validationErrorMessage || file.validationError,
							title: MooTools.lang.get('FancyUpload', 'removeTitle'),
							events: {
								click: function() {
									this.destroy();
								}
							}
						}).inject(this.list, 'top');
					}, this);
				},
				onFileSuccess: function(file, response) {
					var json = new Hash(JSON.decode(response, true) || {});
					if (json.get('status') == '1') {
						field = json.get('field');
						key = jQuery('ul#'+field+'-list li').size() - 1;
						jQuery(file.element).css('background', 'url('+json.get('fName')+') left top no-repeat').attr('id', 'file-'+key);
						file.element.set('html', '<a class="file-remove" href="javascript:void(0)" onclick="file.remove({id:'+json.get('id')+',key:'+key+',field:\''+field+'\'})" title="�������, ����� ������� ���� ����.">�������</a><span class="file-name">'+json.get('rName')+'</span><span class="file-info"></span>');
					} else {
						file.element.addClass('file-failed');
						file.info.set('html', '<strong>������:</strong> '+json.get('error'));
					}
				},
				onFail: function(error) {
					switch (error) {
						case 'hidden': alert('����� �������� ���������� ���������, ������������� ��� � �������� � ������� F5.'); break;
						case 'blocked': alert('����� �������� ���������� ���������, ����� ��������� ���������� Flash.'); break;
						case 'empty': alert('������ ���� �� ��� ������, ����������, ������ ���������, � �� ����� ��� ��������.'); break;
						case 'flash': alert('����� �������� ���������� ���������, ���������� ��������� ������ Adobe Flash ������.'); break;
					}
				}
			});
                    });
		});
	},
	image: function(id) {
		jQuery('body').append('<table id="uploader-popup" class="popup"><tr><td><div id="file-uploader" class="dialog-box"><div class="title">�������� ������</div><div class="data"><p id="loading-text"><img src="/sf123sf123sf/i/loading.gif" align="absmiddle" id="loading">���������� ���������, ���� �������� ������...</p><form action="image.php?m='+module.name+'&id='+id+'" method="post" enctype="multipart/form-data" id="uploader-form"><fieldset id="fallback"><label for="photoupload">��������� ����: <input type="file" name="Filedata" /></label></fieldset><div id="status" class="hide"><p><a href="javascript:void(0)" id="browse">�����</a> | <a href="javascript:void(0)" id="clear">��������</a> | <a href="javascript:void(0)" id="upload">���������</a></p><div><strong class="overall-title"></strong><br /><img src="i/bar.gif" class="progress overall-progress" /></div><div><strong class="current-title"></strong><br /><img src="i/bar.gif" class="progress current-progress" /></div><div class="current-text"></div></div><ul id="list"></ul></form><p class="right buttons"><input type="button" class="b-textfield" value="�������" onclick="file.close()"></p></div></div></td></tr></table>');
		jQuery('table#uploader-popup').fadeIn(500);
		jQuery.getScript('library/upload/mootools.js', function() {
			jQuery.getScript('library/upload/Swiff.Uploader.js', function() {
				jQuery.getScript('library/upload/Fx.ProgressBar.js');
				jQuery.getScript('library/upload/FancyUpload2.js', function() {
					initGET('/go/m='+module.name+'&op=image-load&id='+id);
				});
			});
		});
	},
	close: function() {
		jQuery('table#uploader-popup').fadeOut(500, function() {
			jQuery(this).remove();
		});
	},
	remove: function(v) {
		jQuery('body').append('<table id="file-remove" class="popup"><tr><td><div id="file-uploader" class="dialog-box"><div class="title">�������� ������: '+v.field+'</div><div class="data"><p>������� ���� ����?</p><p class="right buttons"><input id="remove" type="button" class="b-textfield" value="�������"><input id="cancel" type="button" class="b-textfield" value="������"></p></div></div></td></tr></table>');
		
		jQuery('table#file-remove input#cancel').click(function() {
			jQuery('table#file-remove').fadeOut(500, function() {
				jQuery(this).remove();
			});
		});
		
		jQuery('table#file-remove input#remove').click(function() {
			initGET('/go/m='+module.name+'&op=file-remove&id='+v.id+'&i='+v.key+'&field='+v.field);
			jQuery('table#file-remove input#cancel').trigger('click');
			jQuery('li#file-'+v.key).fadeOut(500, function() {
				jQuery(this).remove();
			});
		});
		
		jQuery('table#file-remove').fadeIn(500);
	}
}

var active = {
	on: function(id, m) {
		jQuery('tr#i'+id+' div.active').not('.status_verified').not('.status_delivered').not('.status').attr('title', '�������').attr('class', 'active');
	},
	off: function(id, m) {
		jQuery('tr#i'+id+' div.active').not('.status_verified').not('.status_delivered').not('.status').attr('title', '��������').attr('class', 'active active-off');
	}
}

var active_vases = {
    on: function(id, m) {
        jQuery('tr#i'+id+' div.active').not('.status_verified').not('.status_delivered').not('.status').attr('title', '�������').attr('class', 'active');
        jQuery('tr#i'+id+' td.status').text("1");
    },
    off: function(id, m) {
        jQuery('tr#i'+id+' div.active').not('.status_verified').not('.status_delivered').not('.status').attr('title', '��������').attr('class', 'active active-off');
        jQuery('tr#i'+id+' td.status').text("0");
    }
}

var activateStatusVerified = {
    on: function(id){
        jQuery('tr#i'+id+' div.status_verified').attr('title', '�����������').attr('class', 'status_verified active');
    },
    off: function(id){
        jQuery('tr#i'+id+' div.status_verified').attr('title', '�� �����������').attr('class', 'status_verified active active-off');
    }
}

var activateStatusPrint = {
    on: function(id){
        jQuery('tr#i'+id+' div.status_printed').attr('title', '�� �����������').attr('class', 'status_printed active');
    },
    off: function(id){
        jQuery('tr#i'+id+' div.status_printed').attr('title', '��� �����������').attr('class', 'status_printed active active-off');
    }
}

var activateStatusReady = {
    on: function(id){
        jQuery('tr#i'+id+' div.status_ready').attr('title', '�� ������').attr('class', 'status_ready active');
    },
    off: function(id){
        jQuery('tr#i'+id+' div.status_ready').attr('title', '������').attr('class', 'status_ready active active-off');
    }
}

var activateStatusDelivered = {
    on: function(id){
        jQuery('tr#i'+id+' div.status_delivered').attr('title', '���������').attr('class', 'status_delivered active');
    },
    off: function(id){
        jQuery('tr#i'+id+' div.status_delivered').attr('title', '�� ���������').attr('class', 'status_delivered active active-off');
    }
}

var activateStatusSend = {


    on: function(id, cstr){

        jQuery('tr#i'+id+' div.status_sended').attr('title', '���������. ' + cstr).attr('class', 'status_sended active');
    },
    off: function(id){
        jQuery('tr#i'+id+' div.status_sended').attr('title', '�� ���������').attr('class', 'status_sended active active-off');
    },

}


var changeOrderCashPaymentStatus = {
    on: function(id){
        jQuery('tr#i'+id+' div.status').attr('title', '�������').attr('class', 'status active');
        jQuery('tr#i'+id+' b.cashPayStatus').html('�������').css('color', 'green');
    },
    off: function(id){
        jQuery('tr#i'+id+' div.status').attr('title', '�� �������').attr('class', 'status active active-off');
        jQuery('tr#i'+id+' b.cashPayStatus').html('�� �������').css('color', 'red');
    }
}

// ��� ������������� ������������� �������� �� ��������� �������� �������� ��� �������
function confirmDeliveryStatusChange(id)
{
    var question = '�������� �������� ������� �������� ��� ������ � ' + id +' ?';
    var retVal = confirm(question);
    if (retVal == true) {
        initGET('/go/m=orders&op=activateStatusDelivered&id='+id);
        return true;
    } else {
        return false;
    }
}

function confirmSendStatusChange(id, courierstr, status)
{
	if (status == 1)
	{
        var question = '�� ������������� ������ �������� �������� ������ � ' + id +' ?';
        var retVal = confirm(question);
        if (retVal == true) {
            initGET('/go/m=orders&op=activateStatusSend&id='+id );
            // �������� ��� onClick
            var code = jQuery("#send_ch_" + id).attr("onclick");
            var newCode = code.replace(/,\s1/g, ", 0");
            jQuery("#send_ch_" + id).attr("onclick", newCode);
            return true;
        }else {
            return false;
		}


	}
	else
	{
        var question = '�������� �������� ������� �������� ��� ������ � ' + id +' ? �����, ������� ��� �������:';
        var retVal = prompt(question, courierstr);

        if (retVal !== null && retVal != '') {
            initGET('/go/m=orders&op=activateStatusSend&id='+id + '&setcur=' + retVal );
            var code = jQuery("#send_ch_" + id).attr("onclick");
            var newCode = code.replace(/,\s0/g, ", 1");
            jQuery("#send_ch_" + id).attr("onclick", newCode);
            return true;
        } else {
            return false;
        }
	}

}


function confirmPrintStatusChange(id)
{
    var question = '�� ����� ��������� ��������� ?! ��� ������ � ' + id +' ?';
    var retVal = confirm(question);
    if (retVal == true) {
        initGET('/go/m=orders&op=activateStatusPrinted&id='+id);
        return true;
    } else {
        return false;
    }
}

function confirmReadyStatusChange(id)
{
    var question = '����� ����� ������ ?! ��� ������ � ' + id +' ?';
    var retVal = confirm(question);
    if (retVal == true) {
        initGET('/go/m=orders&op=activateStatusReady&id='+id);
        return true;
    } else {
        return false;
    }
}

/*

 */
function confirmPaymentStatusChange(id)
{
    var dialog = jQuery('<div><h2>������ ������ �'+ id +' ����� ������� �� <font color="green">�������</font>' +
		'</h2><br/>�������� ��������:<br/><br/><hr width="90%"/>' +
		'<span style="font-size: 11px; font-style: italic;">' +
		'<p><span class="ui-icon ui-icon-comment"></span><b>������� + SMS</b> - �������� ������ �� <font color="green">"�������"</font> � ��������� SMS �������.</p>' +
		'<p><span class="ui-icon ui-icon-check"></span><b>�������</b> - �������� ������ �� <font color="green">"�������"</font>, ��� SMS.</p>' +
		'<p><span class="ui-icon ui-icon-closethick"></span><b>������</b> - ������ �� ������.</p>' +
		'</span></div>').dialog({

        height: 270,
		resizable: false,
		title: "��������� ������ ������ ",
        width: 500,
		draggable: false,
		position: { my: "top", at: "top", of: window},
        modal: true,
		closeText: "",
        buttons: [
			{
				text: "������� + SMS",
				id: "wellUIButton",
				icons: { primary: "ui-icon-comment" },
				click: function() {
					//������ ������ � ���������� ���
					jQuery("#chordsts"+ id).removeAttr("onclick");
					jQuery("#chordsts"+ id).attr("onclick", "confirmPaymentStatusChangeNotPayNoSMS('"+id+"')");
					initGET('/go/m=orders&op=changeOrderCashPaymentStatus&id=' + id + '&sendsms=1');
					dialog.dialog('close');
				}
			},
			{
				text: "�������",
				id: "greyUIButton",
				icons: { primary: "ui-icon-check" },
				click: function() {
					//	������ ������ ������
					jQuery("#chordsts"+ id).removeAttr("onclick");
					jQuery("#chordsts"+ id).attr("onclick", "confirmPaymentStatusChangeNotPayNoSMS('"+id+"')");
					initGET('/go/m=orders&op=changeOrderCashPaymentStatus&id=' + id + '&sendsms=0');
					dialog.dialog('close');
				}
			},
			{
				text: "������",
				id: "redUIButton",
				icons: { primary: "ui-icon-closethick" },
				click: function() {

					dialog.dialog('close');
				}
			}

		]
    });
	/*if (shdialog) {
		var question = '�������� �������� ������� ������ ��� ������ � ' + id + ' ? \r\n (������� ����� ���������� ��� �����������!)';
		var retVal = confirm(question);
		if (retVal == true) {
			initGET('/go/m=orders&op=changeOrderCashPaymentStatus&id=' + id);
			return true;
		} else {
			return false;
		}
	}else{
		initGET('/go/m=orders&op=changeOrderCashPaymentStatus&id=' + id);
	}*/

}

function confirmPaymentStatusChangeNotPayNoSMS(id) {
	var dialog = jQuery('<div><h2>������ ������ �'+ id +' ����� ������� �� <font color="red">�� �������</font>' +
		'</h2><br/>�������� ��������:<br/><br/><hr width="90%"/>' +
		'<span style="font-size: 11px; font-style: italic;">' +

		'<p><span class="ui-icon ui-icon-check"></span><b>�� �������</b> - �������� ������ ������ �� <font color="red">"�� �������"</font></p>' +
		'<p><span class="ui-icon ui-icon-closethick"></span><b>������</b> - ������ �� ������.</p>' +

		'</span></div>').dialog({

		height: 240,
		resizable: false,
		title: "��������� ������ ������ ",
		width: 500,
		draggable: false,
		position: { my: "top", at: "top", of: window},
		modal: true,
		closeText: "",
		buttons: [

			{
				text: "�� �������",
				id: "greyUIButton",
				icons: { primary: "ui-icon-check" },
				click: function() {
					//	������ ������ ������


					if (jQuery.inArray(jQuery("#chordsts"+ id).attr("rel"), getNoSMSPaymentsType()) != -1){
						// ������ ��� ��� �������� ���
						jQuery("#chordsts"+ id).removeAttr("onclick");
						jQuery("#chordsts"+ id).attr("onclick", "confirmPaymentStatusChangeNoSMS('"+id+"')");
					}else{
						// ���� ����� ���������� ���
						jQuery("#chordsts"+ id).removeAttr("onclick");
						jQuery("#chordsts"+ id).attr("onclick", "confirmPaymentStatusChange('"+id+"')");
					}
					console.log(getNoSMSPaymentsType());
					initGET('/go/m=orders&op=changeOrderCashPaymentStatus&id=' + id + '&sendsms=0');
					dialog.dialog('close');
				}
			},
			{
				text: "������",
				id: "redUIButton",
				icons: { primary: "ui-icon-closethick" },
				click: function() {

					dialog.dialog('close');
				}
			}

		]
	});
}

function confirmPaymentStatusChangeNoSMS(id)
{
	var dialog = jQuery('<div><h2>������ ������ �'+ id +' ����� ������� �� <font color="green">�������</font>' +
		'</h2><br/>�������� ��������:<br/><br/><hr width="90%"/>' +
		'<span style="font-size: 11px; font-style: italic;">' +

		'<p><span class="ui-icon ui-icon-check" ></span><b>�������</b> - �������� ������ ������ ��<font color="green">"�������"</font>, ��� SMS.</p>' +
		'<p><span class="ui-icon ui-icon-closethick" ></span><b></span>������</b> - ������ �� ������.</p>' +

		'</span></div>').dialog({

		height: 250,
		resizable: false,
		title: "��������� ������� ������ ",
		width: 500,
		draggable: false,
		position: { my: "top", at: "top", of: window},
		modal: true,
		closeText: "",
		buttons: [

			{
				text: "�������",
				icons: { primary: "ui-icon-check" },
				id: "greyUIButton",
				click: function() {
					//	������ ������ ������
					jQuery("#chordsts"+ id).removeAttr("onclick");
					jQuery("#chordsts"+ id).attr("onclick", "confirmPaymentStatusChangeNotPayNoSMS('"+id+"')");
					initGET('/go/m=orders&op=changeOrderCashPaymentStatus&id=' + id + '&sendsms=0');
					dialog.dialog('close');
				}

			},
			{
				text: "������",
				icons: { primary: "ui-icon-closethick" },
				click: function() {

					dialog.dialog('close');
				},
				id: "redUIButton"
			}

		]
	});
}

function getNoSMSPaymentsType(){
	// TODO: ��� �� �������� ������ �� ������� SmsQueue::$nodialog_payments
	return ['cash', 'cards-courier', 'courier'];
}


function initGETquiet(url) {
	console.log(url);
	jQuery.ajax({
		url: url,
		dataType : 'json',
		beforeSend: function() {
			startLoading();
			return php.beforeSend();
		},
		success: function(data, textStatus) {
			stopLoading();
			return php.success(data, textStatus);
		},
		error: function (xmlEr, typeEr, except) {
			stopLoading();
			console.log("���-�� ����� �� ���, ������: " + xmlEr.status + " ����� ������: " + except);
			return //php.error(xmlEr, typeEr, except);
		},
		complete: function (XMLHttpRequest, textStatus) {
			stopLoading();
			return php.complete(XMLHttpRequest, textStatus);
		}
	});
	return false;
}


function initGET(url) {
    console.log(url);
	jQuery.ajax({
		url: url,
		dataType : 'json',
		beforeSend: function() {
			startLoading();
			return php.beforeSend();
		},
		success: function(data, textStatus) {
			stopLoading();
			return php.success(data, textStatus);
		},
		error: function (xmlEr, typeEr, except) {
			stopLoading();
			return php.error(xmlEr, typeEr, except);
		},
		complete: function (XMLHttpRequest, textStatus) {
			stopLoading();
			return php.complete(XMLHttpRequest, textStatus);
		}
	});
	return false;
}

function getForm(f) {
	initGET(jQuery(f).attr('action') + '&' + jQuery(f).serialize());
}

function startLoading() {
	jQuery('div#loading').css('background', 'url(/sf123sf123sf/i/sys-loading.gif) center no-repeat');
}

function stopLoading() {
	jQuery('div#loading').css('background', 'none');
}

function formSubmit(form) {
	jQuery.ajax({
		url: jQuery(form).attr('action'),
		type: "POST",
		data: jQuery(form).formToArray(true),
		enctype: "multipart/form-data",
		dataType : "json",
		beforeSend: function() {
			startLoading();
			return php.beforeSend();
		},
		success: function(data, textStatus) {
			stopLoading();
			return php.success(data, textStatus);
		},
		error: function (xmlEr, typeEr, except) {
			stopLoading();
			return php.error(xmlEr, typeEr, except);                  
		},
		complete: function (XMLHttpRequest, textStatus) {
			stopLoading();
			return php.complete(XMLHttpRequest, textStatus);
		}
	});
	return false;
}

function initDialogData(url, id) {
	if (jQuery('table#'+id).length != 0) return false;
	createPopup(id);
	initGET(url);
}

function initDialogClose(id) {
	jQuery('body').css('overflow','auto');
	jQuery('table#' + id).fadeOut(500, function(){jQuery(this).remove()});
}

function createPopup(id) {
	jQuery('body').css('overflow','hidden').append('<table id="' + id + '" class="popup"><tr><td id="d">���������� ���������, ���� �������� ������...</td></tr></table>');
	jQuery('table#' + id).fadeIn(500);
}

function initSysDialog(t,time) {
	jQuery('body').append('<table id="sys-dialog" class="popup"><tr><td><div class="dialog-box"><div class="title">��������� �����������</div><div class="data">'+t+'</div></div></td></tr></table>');
	jQuery('table#sys-dialog').css('opacity', 1).fadeIn(500,function() {
        time = (time === undefined)? 1500 : time;
        if (!isNaN(time) && time!==false && time!==true){
            window.initsysdialog_timeout = window.setTimeout(function(){
                jQuery('table#sys-dialog').fadeOut(500, function(){jQuery(this).remove();});
            }, parseInt(time));
        }
	}).click(function(){
        if (window.initsysdialog_timeout !== undefined){
            window.clearTimeout(window.initsysdialog_timeout);
        }
		jQuery('table#sys-dialog').fadeOut(500, function(){jQuery(this).remove();});
	});
}

// ������ ������� ���� ��������

function initSysDialogWOClose(t,time) {
	try {
		jQuery('body').append('<table id="sys-dialog" class="popup"><tr><td><div class="dialog-box"><div class="title">��������� �����������</div><div class="data">' + t + '</div></div></td></tr></table>');
		jQuery('table#sys-dialog').css('opacity', 1).fadeIn(500, function () {
		}).click(function () {
			jQuery('table#sys-dialog').fadeOut(500, function () {
				jQuery(this).remove();
			});
		});
	}catch (e){
		console.log("���-�� ����� �� ��� :( ������: " + e.name);
	}
}


function loadCSS(src) {
	var fCSS = document.createElement('link');
	fCSS.setAttribute('rel', 'stylesheet');
	fCSS.setAttribute('type', 'text/css');
	fCSS.setAttribute('href', src);
	if (typeof fCSS != 'undefined') document.getElementsByTagName('head')[0].appendChild(fCSS);
}

function htmlspecialchars_decode(string, quote_style) {
	var optTemp = 0,
	i = 0,
	noquotes = false;
	if (typeof quote_style === 'undefined') {
		quote_style = 2;
	}
	string = string.toString().replace(/&lt;/g, '<').replace(/&gt;/g, '>');
	var OPTS = {
		'ENT_NOQUOTES': 0,
		'ENT_HTML_QUOTE_SINGLE': 1,
		'ENT_HTML_QUOTE_DOUBLE': 2,
		'ENT_COMPAT': 2,
		'ENT_QUOTES': 3,
		'ENT_IGNORE': 4
	};
	if (quote_style === 0) {
		noquotes = true;
	}
	if (typeof quote_style !== 'number') {
		quote_style = [].concat(quote_style);
		for (i = 0; i < quote_style.length; i++) {
			if (OPTS[quote_style[i]] === 0) {
				noquotes = true;
			} else if (OPTS[quote_style[i]]) {
				optTemp = optTemp | OPTS[quote_style[i]];
			}
		}
		quote_style = optTemp;
	}
	if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
		string = string.replace(/&#0*39;/g, "'");
	}
	if (!noquotes) {
		string = string.replace(/&quot;/g, '"');
	}
	
	string = string.replace(/&amp;/g, '&');
	
	return string;
}
