<?php
error_reporting(E_ERROR);
header('Content-Type: text/javascript; charset=Windows-1251');
define('_SWS_ADMIN_CORE', true);
$basepath = dirname(__FILE__)."/";

session_start();

require_once($basepath.'../library/jquery/jQuery.php');
include_once($basepath.'../../system/config.php');
include_once($basepath.'../../system/db.php');
include_once($basepath.'../../system/filter.php');
include_once($basepath.'../../system/smsqueue.php');
include_once($basepath.'../../system/bonuscards.php');
include_once($basepath.'inc_vars.php');
include_once($basepath.'authorization.php');
include_once($basepath.'function.php');
include_once($basepath.'../../system/instagram.class.php');

if (!_IS_ADMIN) {
    jQuery::evalScript('window.location.href = "/sf123sf123sf/index.php";');
    jQuery::getResponse();
}

$module = !empty($_REQUEST['m']) ? filter(trim($_REQUEST['m']), 'nohtml') : null;
$op = !empty($_REQUEST['op']) ? filter(trim($_REQUEST['op']), 'nohtml') : null;

// �������� �� ������ ������ � ��������
if (mb_strpos($module, '&')!== false)
{
	$ur = explode("&", $module);
	foreach ($ur as $prt)
	{
		if (mb_strpos($prt, "=") !== false)
		{
			$subprt = explode("=",$prt );
			if ($subprt[0] == 'op'){
				$op = $subprt[1];
			}elseif($subprt[0] == 'module'){
				$module = $subprt[1];
			}elseif($subprt[0] == 'query'){
				$query = iconv("UTF-8", "WINDOWS-1251", $subprt[1]);
			}
		}
		else{
			$module = $prt;
		}
	}
}

if (!empty($module)) {
	$r = db_query('SELECT `id`, `title` FROM `sw_modules` WHERE `link` = '.quote($module));
	if (mysql_num_rows($r) == 0) {
		jQuery::evalScript(inUTF8('initSysDialog("������ �� ����������")'));
		jQuery::getResponse();
	}
	$m = mysql_fetch_array($r, MYSQL_ASSOC);

	include_once($basepath.'../../modules/'.$module.'/admin/index.php');
}

switch ($op) {
	case 'load-menu': initMenu(); break;
	case 'load-module': initModule(); break; // ��������� ���
	case 'gen-link': genLink(); break;
	case 'top': posTop(); break;
	case 'bottom': posBottom(); break;
	case 'active': Active(); break;
    case 'activateStatusVerified' : activateStatusVerified(); break; // �������� ������ ������������� (��� ������ ������)
    case 'activateStatusDelivered' : activateStatusDelivered(); break; // �������� ������ �������� (��� ������ ������)
    case 'activateStatusSend' : activateStatusSend(); break; // �������� ������ �������� (��� ������ ������)
    case 'activateStatusPrinted' : activateStatusPrinted(); break; // �������� ������ �������� (��� ������ ������)
    case 'activateStatusReady' : activateStatusReady(); break;
    case 'changeOrderCashPaymentStatus' : changeOrderCashPaymentStatus(); break; // �������� ������ ������ ������ �� ��������
	case 'checkbox': checkbox(); break;
	case 'item': initItem(); break; // ������������� ������
//	case 'item-save': saveItem(); break;
	case 'item-remove': itemRemove(); break;
	case 'file-delete': deleteItem(); break;
	case 'image-load': loadImage(); break;
	case 'pos': posItem(); break;
	case 'radio': radioSet(); break;
	case 'section': section(); break;
	case 'file-remove': fileRemove(); break;
	case 'view-order': viewOrder(); break;
	case 'order-item': itemOrder(); break;
	case 'exit': exitAdmin(); break;
    // ����� ����� �������� � ��������� � ����� �������
    case 'check-new-orders': chkNewOrders(); break;
    case 'viewEvent': viewEvent(); break;
    case 'viewQuestion': viewQuestion(); break;
    case 'deleteQuestion': deleteQuestion(); break;
    case 'getordersreloadtimer': getSysOrdersTimer(); break;
    case 'notify-prepare': runPrepareNotifyMails(); break;



	default: {
		jQuery::evalScript(inUTF8('initSysDialog("�������� �� �������")'));
		jQuery::getResponse();
	} break;
}


function getCourierForOrder($id){

	$items = query_new('SELECT `courier` FROM `sw_orders` WHERE `id` = '.intval($id));
	echo print_r($items);
	die;
}

function runPrepareNotifyMails()
{
	$cliPath = realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..');

	$message=shell_exec('php ' . $cliPath . DIRECTORY_SEPARATOR . 'cli'. DIRECTORY_SEPARATOR . 'notify.php prepare');
	jQuery::evalScript(inUTF8('module.index();'));
	jQuery::getResponse();
}

function getSysOrdersTimer(){
    global $_GLOBALS;
    echo json_encode($_GLOBALS['v_admin_reload_orders_timer']);
    die;
}

function posItem() {
	global $module;

	if (empty($_GET['id'])) jQuery::getResponse();

	$id = abs(intval($_GET['id']));
	$pg = !empty($_GET['pg']) ? abs(intval($_GET['pg'])) : 1;
	$sid = !empty($_GET['sid']) ? abs(intval($_GET['sid'])) : 0;

	$items = query_new('SELECT `id` FROM `sw_'.$module.'` WHERE `sid` = '.quote($sid).' ORDER BY `pos`,`id` ASC');
	if (!empty($items) && isset($_GET['top'])){
		$top = intval($_GET['top']);
		foreach ($items as $npos => $item) {
			if (($top==0 && $item['id']==$id) || ($top==1 && $items[$npos+1]['id']==$id)){
				$npos++;
			}elseif(($top==0 && $items[$npos-1]['id']==$id) || ($top==1 && $item['id']==$id)){
				$npos--;
			}
			db_query('UPDATE `sw_'.$module.'` SET `pos` = '.quote($npos).' WHERE `id` = '.quote($item['id']));
		}
	}
	initModule($sid, $pg);
}

function fileRemove() {
	global $_GLOBALS, $module;

	if (empty($_GET['id']) || empty($_GET['field']) || !isset($_GET['i'])) jQuery::getResponse();

	$field = filter(trim(in1251($_GET['field'])), 'nohtml');
	$id = abs(intval($_GET['id']));
	$key = abs(intval($_GET['i']));

	$r = db_query('SELECT `'.$field.'` FROM `sw_'.$module.'` WHERE `id` = '.quote($id));
	if (mysql_num_rows($r) == 0) jQuery::getResponse();
	$f = mysql_fetch_array($r, MYSQL_ASSOC);

	if (!empty($f[$field])) {
		$files = explode(':', $f[$field]);
		foreach ($files as $k=>$v) {
			if ($k == $key) {
				if (!empty($_GLOBALS['field']['settings']['image']['size'])) {
					$pach = '../../files/'.$module.'/'.$id.'/';
					if (file_exists($pach.$v)) unlink($pach.$v);
					foreach ($_GLOBALS['field']['settings']['image']['size'] as $size) {
						if (file_exists($pach.'w'.$size['width'].'_'.$v)) unlink($pach.'w'.$size['width'].'_'.$v);
					}
				}
				unset($files[$k]);
			}
		}

		$value = implode(':', $files);

		db_query('UPDATE `sw_'.$module.'` SET `'.$field.'` = '.quote($value).' WHERE `id` = '.quote($id));
	}

	jQuery::getResponse();
}

function itemOrder() {
	if (empty($_GET['id'])) jQuery::getResponse();
	$id = abs(intval($_GET['id']));

	$r = db_query('SELECT `structure` FROM `sw_orders_item` WHERE `id` = '.quote($id));

	if (mysql_num_rows($r) == 0){
	    //���� ��� ����� ������
        $r = db_query('SELECT `structure` FROM `sw_basket` WHERE `id` = '.quote($id));
        if (mysql_num_rows($r) == 0) jQuery::getResponse(); // �� ���� ��� ����� ������ ���
    }
    //jQuery::getResponse();


	$o = mysql_fetch_array($r, MYSQL_ASSOC);

    $text = inUTF8(filter($o['structure']));


    $text = str_replace(array("'", '"'), '', $text);
    // ����, ����� ������ �� �����, ����� ��� ������

    if (empty($text) ){

        $text = inUTF8(filter('������ �� �������'));
    }
    $script = 'order.itemView({text:"'.str_replace("\n", '<br>', $text ).'"})';

	jQuery::evalScript($script);
	jQuery::getResponse();
}

function viewOrder() {
    global $module;
	$not_found = '���������� �� ������� ������� �� �������.';
    if (empty($_GET['id']) || empty($module) || function_exists('viewOrd_'.$module)===false){
		jQuery('#orderView_data')->html(inUTF8($not_found));
		jQuery::getResponse();
	}
	$html = call_user_func('viewOrd_'.$module);
	if (empty($html)){
		$html = $not_found;
	}
	jQuery('#orderView_data')->html(inUTF8($html));
	jQuery::getResponse();
}

function render_acts($data=array()){
    if (empty($data) || !is_array($data) || !file_exists('acts.php')){
       HeaderPage();
       exit();
    }
    $html = file_get_contents('acts.php');
    $html = str_replace('[%ACT_TITLE%]', $data['title'], $html);
    $html = str_replace('[%ACT_TABLE_CLASS%]', $data['class'], $html);
    $tbl = '';
    foreach ($data['content'] as $line){
        $tbl .= PHP_EOL.'<tr>'.PHP_EOL.implode(PHP_EOL, $line).PHP_EOL.'</tr>';
    }
    $html = str_replace('[%ACT_TABLE_CONTENT%]', $tbl, $html);
    header('Content-Type: text/html; charset=windows-1251');
    print_r($html);
    exit();
}

function viewOrd_orders(){
	$id = abs(intval($_GET['id']));
    $cur = get_currency();
	$box = array('����� (���������)', '�������� ����� (50 ���.)', '����������� �������� (200 ���.)', '���������� �������� (250 ���.)');
	$o = query_new('SELECT * FROM `sw_orders` WHERE `id` = '.quote($id),1);
	if (!empty($o)){
        $o['recipient'] = str_replace('<p><b>�������� �� ����</b></p>', '', $o['recipient']);
        $o['recipient'] = str_replace('<p><b>�� �������� �� ����</b></p>', '', $o['recipient']);
        $o['recipient'] = str_replace('<p><b>�������� ����� �������, �������������:</b> ��</p>', '', $o['recipient']);
        $o['recipient'] = str_replace('<p><b>�������� ����� �������, �������������:</b> ���</p>', '', $o['recipient']);
        $o['recipient'] = str_replace('<p><b>����:</b> ����</p>', '', $o['recipient']);
        $o['recipient'] = str_replace('<p><b>����:</b> �� ����</p>', '', $o['recipient']);
        $o['recipient'] = str_replace('<p><b>��� ������ ����������:</b> ��</p>', '', $o['recipient']);
        $o['recipient'] = str_replace('<p><b>��� ������ ����������:</b> ���</p>', '', $o['recipient']);
        $o['recipient'] = str_replace('<p><strong>�������� �� ����</strong></p>', '', $o['recipient']);
        $o['recipient'] = str_replace('<p><strong>�� �������� �� ����</strong></p>', '', $o['recipient']);
        $o['recipient'] = str_replace('<p><strong>�������� ����� �������, �������������:</strong> ��</p>', '', $o['recipient']);
        $o['recipient'] = str_replace('<p><strong>�������� ����� �������, �������������:</strong> ���</p>', '', $o['recipient']);
        $o['recipient'] = str_replace('<p><strong>����:</strong> ����</p>', '', $o['recipient']);
        $o['recipient'] = str_replace('<p><strong>����:</strong> �� ����</p>', '', $o['recipient']);
        $o['recipient'] = str_replace('<p><strong>��� ������ ����������:</strong> ��</p>', '', $o['recipient']);
        $o['recipient'] = str_replace('<p><strong>��� ������ ����������:</b> ���</p>', '', $o['recipient']);

        $recipient = explode('<p>', $o['recipient']);
        $oMessage = [];
        $oSignature = [];
        foreach ($recipient as $key => $value) {
            if (strpos($value, '������� ��� ��������:')) {
                $oMessage = [$key, $value];
            }
            if (strpos($value, '����� ��� ��������:')) {
                $oSignature = [$key, $value];
            }
        }
        if (!empty($oMessage) && !empty($oSignature)) {
            $recipient[$oMessage[0]] = $oSignature[1];
            $recipient[$oSignature[0]] = $oMessage[1];
        }
        $o['recipient'] = implode('<p>', $recipient);

		$mkad = query_new('SELECT * FROM sw_variables WHERE name='.quote('start_over_MKAD'),1);
		$foto = query_new('SELECT * FROM sw_variables WHERE name='.quote('photo'),1);
        /* ��������� �������� ������� �����, ���� ���������, ����� ������ ����� ��������� :) */
        if (mb_strpos($o['recipientSMS'],"$$") !== false) {
            $metro = isset($o['recipientSMS']) ? explode("$$", $o['recipientSMS']) : array();

            if (isset($metro[1])) {
                if (!empty($metro[1])) {
                    $metro = trim($metro[1]);
                }
            }
        }else{
            $metro = "";
        }
		$del_arr = array(
			1 => '� �������� ����',
			3 => '�� ���� (�� '.$mkad['value'].' ��)',
			4 => '�� ���� ',
            5 => trim( '�� ������� ����� ' . $metro)
        );
		if (intval($o['delivery'])>5){
			$dest = intval($o['delivery'])-4;
			$del_arr[4] = $del_arr[4].'('.$dest.' ��)';
			$o['delivery'] = 4;
		}
		$markup = empty($o['markup'])?0:$o['markup'];

        $items = query_new('SELECT i.`id`,i.`structure`, i.`count`, i.`type`, i.`title`,i.`cid`, i.`text`, i.`amount`, i.`boxPrice`, i.`box`, i.`vase`, i.`type`, i.`color`, c.`art`, c.`image`,c.`share`, s.`url`, so.`region`, so.`courier` FROM `sw_orders_item` as i LEFT JOIN `sw_catalog` as c ON i.`cid` = c.`id` LEFT JOIN `sw_catalog_section` as s ON i.`cid` = s.`cid` AND s.`first`=1 INNER JOIN sw_orders so ON so.`id` = i.`oid` WHERE i.`oid` = '.quote($id).' ORDER BY i.`id` DESC');
        if (!empty($_GET['task']) && $_GET['task']=='acts'){
            $data = array('title'=>'��� ��� '.($_GET['t']=='client'?'�������':'��������'),'class'=>($_GET['t']=='client'?'':'courier'));
            $tbl = array();
            if ($_GET['t']=='client'){
                $tbl[] = array('<td class="header"><table><tr><td><img src="/i/logo-studio-floristic-green1.jpg"></td><td align="right"><b class="hint">����� �'.$id.'</b></td></tr></table></td>');
            }else{
                $tbl[] = array('<td colspan="2">����� �'.$id.'</td>');
                $tbl[] = array('<td width="200">���� � ����� ��������:</td>','<td>'.$o['title'].'</td>');
            }
        }else{
			// ���� � ������� ������ (C�����)
           	$statusinfo = empty($o['status'])? '<b style="color:red">�� �������</b>':'<b style="color:green">�������</b>';
			//*************//
            $html = '<p><b>���� � ����� ��������:</b> '.$o['title'].'<span class="payment_state">'.$statusinfo.'</span></p>'
                . '<p><b>����� ������:</b> '.$o['amount'].' ���.</p>'
                . '<p><b>����� � ������:</b> '.$o['itogo'].' ���.</p>'
                . '<p><b>���-�� �������:</b> '.$o['count'].' ��.</p>'
                . '<p><b>��������:</b> '.($o['delivery']!=2 && isset($del_arr[$o['delivery']])? $del_arr[$o['delivery']].' + '.$o['dostavka'].$cur['reduction']: '���������').'</p>'
                . '<p><b>������:</b> '.$o['discount'].'%</p>'
                . '<p><b>�������:</b> '.$markup.'%</p>'
                . (!empty($foto['value']) && !empty($o['foto'])? '<p><b>���� ��� ��������:</b> '.$foto['value'].' ���.</p>' :'')
                . '<hr />'. str_replace("\n", '', $o['recipient']).'<hr />'
                . str_replace("\n", '', $o['uData']);
        }
		if (!empty($items)){
			$db_colors = query_new('SELECT `id`, `title`, `class` FROM `sw_colors` WHERE `active` = 1 ',0,'id');
            if (!empty($_GET['task']) && $_GET['task']=='acts'){
                $tbl_list = $_GET['t']=='client'?'<td class="items">':'<td>������ ������:</td><td>';
            }else{
                $html .= '<hr /><p><b>���������� ������:</b></p><p><table class="basket-items"><tr><th class="title">������������</th><th>���-��</th><th>����</th><th>�����</th></tr>';
            }
			foreach ($items as $c){
                $d = ($c['type']=='my-order' || !empty($c['share']))?0:1;
				$sum = ceil($c['amount']*(1+$markup/100));
                $summ = ceil($sum*(1-$o['discount']*$d/100));
				$sum = $c['count']*$summ;
                // ������� ������� ����
                $vase_title = "";
                $vase_price = "";
                $vase_img = "";
				if ($c['vase'] > 0){
                    $curvase = query_new("SELECT * FROM sw_vases WHERE `status` = 1 AND id = ". intval($c['vase']), 1);
                    if ($curvase['id'] > 0){
                        // ������ ���� ����
                        $vase_title = '"'.$curvase['title'].'"';
                        $vase_price = $curvase['price'];
                        $vase_img = '<br><img src="/files/vases/'.$curvase['id'].'/'.$curvase['picture'].'" height="100"/>';
                    }else{
                        $vase_title = "";
                        $vase_price = "300";
                    }
                }else{
                    $vase_title = "";
                    $vase_price = "300";
                }
				if (!empty($c['color'])){
                    $act_cols = array();
					$colors = explode('-', $c['color']);
					$color_text = '<br><span style="padding: 0px 0px 0px 15px; vertical-align:top;">����(�):</span>'
						. '<ul style="display: inline-block; list-style: outside none none; padding: 0px 0px 0px 5px; margin: 0px;">';
					foreach ($colors as $col_id){
						$color_text .= !empty($db_colors[$col_id]['title'])?'<li>'.$db_colors[$col_id]['title'].'</li>':'';
                        if (!empty($db_colors[$col_id]['title'])){
                            $act_cols[] = $db_colors[$col_id]['title'];
                        }
					}
                    if (!empty($_GET['task']) && $_GET['task']=='acts'){ $color_text = !empty($act_cols)?implode(', ',$act_cols):''; }
                    else{ $color_text .= '</ul>'; }
				}else{
					$color_text = '';
				}
                if (!empty($_GET['task']) && $_GET['task']=='acts'){
                    if ($_GET['t']=='client'){
                        $img = explode(':', $c['image']);
                        switch ($c['type']) {
                            case 'item': $ico = '<img src="/files/catalog/'.$c['cid'].'/w168_'.$img[0].'" width="110" style="max-height: 126px;">'; break;
                            case 'my-order': $ico = '<img src="/i/individ-100.jpg">'; break;
                            case 'my-bouquet': $ico = '<img src="/i/myBouquet.gif">'; break;
                            case 'gift':
                                $gift = query_new('SELECT * FROM `sw_gifts` WHERE `id`='.$c['cid'],1);
                                $img = explode(':', $gift['image']);
                                $ico = '<img src="/files/gifts/'.$c['cid'].'/w168_'.$img[0].'" width="110" style="max-height: 126px;">'; break;
                        }


                        $color_text = !empty($color_text)?'<p style="font-size: 15px; font-weight: bold;"><b>����: </b>'.$color_text.'</p>':'';
                        $struct = empty($c['structure'])?'':'<b>������:</b><br>'.$c['structure'];
                        $countText = '����������: '.$c['count'];
                        if ($c['count'] > 1) {
                            $countText = '<span style="font-size: 18px; font-weight: bold">'.$countText.'</span>';
                        }
                        $tbl_list.='<div class="item-border"><table class="item"><tr><td colspan="2"><p><b>'.$c['title'].'</b></p></td>'
                                . '<td rowspan="2" width="250" align="left" class="cBorder"><p class="title" style="margin: 3px 0px 3px 0px !important;">'.($c['type']!='gift'?'<b>������������� � ������:</b></p><p><b>����������:</b> '.$box[$c['box']].'</p>'
                                . '<p><b>����:</b> '.(!empty($c['vase']) ? $vase_title : '���').$vase_img.'</p>'.$color_text:'<b>���������� ������� � ������</b></p><p><b>��������:</b> '.$gift['text'].'</p>').'<div class="counts">'.$countText.'</div></td></tr>'
                                . '<tr><td style="text-align: center;" width="150">'.$ico.(!empty($c['art']) ? '<br><span class="hint">���.: '.$c['art'].'</span>' : null).'</td>'
                                . '<td style="line-height: 20px; padding: 0px 10px;" width="150">'.$struct.'</td></tr></table></div>';
                    }else{
                        $art = ($c['type']=='qift')?'�������: ':(empty($c['art'])? '':'���. '.$c['art'].': ');
                        $color_text = !empty($color_text)?'; ����: '.$color_text:'';
                        $tbl_list.='<p>'.$art.$c['title'].(!empty($c['structure'])?' � '.str_replace("\n", '; ', $c['structure']):'')
                            .'; ����������: '.$box[$c['box']].$color_text.'; ����: '.(!empty($c['vase']) ? $vase_title : '���').' / '.$c['amount'].' ���.'.($markup!=0?' + '.$markup.'%':'')
                            .(($o['discount']*$d)!=0?' - '.$o['discount'].'%':'').' = '.$summ.' ���.'.($c['count']>1?' x '.$c['count'].'��. = '.$sum.' ���.':'').'</p>';
                    }
                }else{

                    $html .= '<tr><td class="title"><a href="'.$c['url'].'" '.($c['type']=='item'? 'target="_blank"':'').'>'.$c['title'].'</a>'
                        . (!empty($c['box'])? ' + '.$box[$c['box']]: '').(!empty($c['vase'])?' + ���� ' . $vase_title . ' ('.$vase_price.' ���.)' :'').(!empty($c['share'])?'<span class="is_share">�����</span>':'').$color_text
                        . ($c['type']!='item' && $c['type']!='gift'?' | <a href="javascript:order.item('.$c['id'].')">��������</a>':($c['type']=='gift'?' (�������)':''))
                        . '</td><td>'.$c['count'].'��</td><td>'.$c['amount'].'�</td><td>'.$sum.'�</td></tr>';
                }
			}
            // ��������� �������� ������ � ��������� � ������� �������
            if (intval($o['dostavka']) > 0)
            {
                $html .= '<tr><td colspan="3" class="title">�������� '.$del_arr[$o['delivery']].'</td><td>'.intval($o['dostavka']).'�</td></tr>';
            }
            if (intval($o['courier_payment']) > 0)
            {
                $html .= '<tr><td colspan="3" class="title">����� ������� �� �������</td><td>'.intval($o['courier_payment']).'�</td></tr>';
            }
            $html .= '<tr style="background-color: yellowgreen"><td colspan="3" class="title"><b>����� � ������</b></td><td>'.intval($o['itogo']).'�</td></tr>';


            if (!empty($_GET['task']) && $_GET['task']=='acts'){
                if ($_GET['t']=='client'){
                    $o['extra'] = str_replace('<p><b>����:</b> ����</p>', '<p><b style="font-size: 18px; font-weight: bold">����:</b> <span style="font-size: 18px; font-weight: bold">���� + 50 ���. � �������� ��������</span></p>', $o['extra']);
                    $comments = '';
                    if ($o['comment'] != '') {
                        $comments = '<p style="border: 3px solid; width: 50%; min-height: 50px; padding: 5px;">����������� ���������: '.$o['comment'].'</p>';
                    }
                    $uData = explode('<p>', $o['uData']);
                    foreach ($uData as $value) {
                        if (strpos($value, '�����������:')) {
                            $value = str_replace('<strong>�����������:</strong>', '', $value);
                            $value = str_replace('<b>�����������:</b>', '', $value);
                            $value = str_replace('</p>', '', $value);
                            $comments .= '<p style="border: 3px solid; width: 50%; min-height: 50px; padding: 5px;">����������� ������������: '.$value.'</p>';
                        }
                    }
                    $tbl[] = array($tbl_list.'<div class="recipient"><p class="title"><b>������ ����������:</b></p>'.$o['recipient'].$comments.'<p>&nbsp;</p><p class="title"><b>�������������:</b></p>'.$o['extra']. '<p><b>��������:</b> '.($o['delivery']!=2 && isset($del_arr[$o['delivery']])? $del_arr[$o['delivery']].' + '.$o['dostavka'].$cur['reduction']: '���������').'</p>'.'<div class="datetime">���� � �����:<b>'.$o['title'].'</b></div></div></td>');
                    $tbl[] = array('<td class="footer"><table class="autograph"><tr><td width="330">��������� �� �������� ������������� �� ����:</td><td>_________________________________</td></tr><tr><td>�� ���� ����������:</td><td>_________________________________</td></tr></table>'
                        . '<table class="contacts"><tr><td width="230"><p><b>StudioFloristic.ru</b> - ����� ��� �������!</p><p>www.studiofloristic.ru</p></td><td><p>������� ��� ���������� ������� �� ������:<br /><b>8-800-333-12-91</b> (24 ���� / 7 ����)</p><p>������� � ������ <b>+7(495)255-12-91</b></p></td>'
                        . '<td width="200"><p>E-mail: <b>info@studiofloristic.ru</b></p><p>Skype: <b>studiofloristic</b></p><p>WhatsApp/Viber: <b>8 929 951-88-32</b></p></td></tr></table></td>');
                    $tbl[] = array('<style>.recipient p { margin: 3px 0px 3px 25px !important;}</style>');
                }else{
                    $tbl[] = array($tbl_list.'</td>');
                    $tbl[] = array('<td>��������:</td><td>'.$o['uData'].'</td>');
                    $tbl[] = array('<td>����������:</td><td>'.$o['recipient'].'</td>');
                    $tbl[] = array('<td>'.($o['type'] == 'cash' || $o['type'] == 'courier'?'� ������':'��������').':</td><td><b>'.$o['itogo'].' ���.</b></td>');
                    $courier_payment = ($o['type'] == 'courier' && intval($o['courier_payment'])!= 0) ? ' ('.$o['courier_payment'].' ���.)' : '';
                    $tbl[] = array('<td>����� ������:</td><td><b>'.inc_vars::$types[$o['type']].$courier_payment.'</b></td>');
                   // $tbl[] = array('<td>��������:</td><td><b>'.$o['dostavka'].' ���.</b></td>');
                    $tbl[] = array('<td>��������:</td><td><b>'.($o['delivery']!=2 && isset($del_arr[$o['delivery']])? $del_arr[$o['delivery']].' + '.$o['dostavka'].$cur['reduction']: '���������').'</b></td>');
                    $tbl[] = array('<td>������ �������:</td><td>'.$o['discount'].'%</td>');
                    $tbl[] = array('<td colspan="2" style="border: 0px;">&nbsp;</td>');
                    $tbl[] = array('<td style="border: 0px;">��������:</td><td style="border: 0px;">___________________________________________</td>');
                    $tbl[] = array('<td style="border: 0px;">�������:</td><td style="border: 0px;">___________________________________________</td>');
                    if ($o['delivery'] != 2) { $tbl[] = array('<td style="border: 0px;">������:</td><td style="border: 0px;">___________________________________________</td>'); }
                    $tbl[] = array('<td colspan="2" style="border: 0px;">&nbsp;</td>');
                    if (!empty($o['comment'])) { $tbl[] = array('<td style="border: 0px;">����������� ���������:</td><td>'.$o['comment'].'</td>'); }
                    $tbl[] = array('<td colspan="2" style="border: 0px;">&nbsp;</td>');
                }
                $data['content'] = $tbl;
                render_acts($data);
            }else{
                $html .= '</table>';
                $html .= '</p>';
                if (!empty($c['region'])){
                    $html .= '<br/><div><b>GeoIP:</b>&nbsp;'.$c['region'].'</div>';
                }
	            if (!empty($c['courier'])){
		            $html .= '<br/><div><b>������:</b>&nbsp;'.$c['courier'].'</div>';
	            }
                // ������� ����������� ���������
                if (isset($o['comment']) && !empty($o['comment']))
                {
                    $html .= '<p align="left"><b>����������� ���������:</b> '.$o['comment'].'</p>';
                }

            }
		}
		return $html;
	}
	return false;
}
function viewOrd_orders_fast(){
	$id = abs(intval($_GET['id']));
	$o = query_new('SELECT of.*, c.`art`, c.`image`,u.`discount`,c.`share` FROM `sw_orders_fast` as of LEFT JOIN `sw_catalog` as c ON of.`cid` = c.`id` LEFT OUTER JOIN `sw_users` as u ON u.`id`=of.`uid` WHERE of.`id` = '.quote($id),1);
	if (!empty($o)){
        if (!empty($_GET['task']) && $_GET['task']=='acts'){
            $data = array('title'=>'��� ��� '.($_GET['t']=='client'?'�������':'��������'),'class'=>($_GET['t']=='client'?'':'courier'));
            $tbl = array();
            $tbl_list = $_GET['t']=='client'?'<td class="items">':'<td>������ ������:</td><td>';
            if ($_GET['t']=='client'){
                $tbl[] = array('<td class="header"><table><tr><td><img src="/i/logo-studio-floristic-green1.jpg"></td><td align="right"><b class="hint">����� �'.$id.'</b></td></tr></table></td>');
            }else{
                $tbl[] = array('<td colspan="2">����� �'.$id.'</td>');
                $tbl[] = array('<td width="200">���� ��������:</td>','<td>'.$o['delivery_date'].'</td>');
            }
        }else{
            $html = '<p><b>��������:</b> '.$o['uName'].'</p>'
                . '<p><b>�������:</b> '.$o['uPhone'].'</p>'
                . '<p><b>���� ��������:</b> '.(!empty($o['delivery_date'])?$o['delivery_date']:'�� �������').'</p>'
                . '<p><b>����� ������:</b> '.$o['price'].' ���.</p>'
                . '<p><b>���-�� ������:</b> '.$o['count'].' ��.</p>'
                . '<p><b>���������� ���������:</b> <span>'.str_replace("\n", '', $o['uComment']).'</span></p>'
                . '<hr /><b>���������� ���������:</b> <span>'.str_replace("\n", '', $o['comment']).'</span></p>';
            $html .= '<hr /><p><b>���������� ������:</b></p><p><table class="basket-items"><tr><th class="title">������������</th><th>���-��</th><th>����</th><th>�����</th></tr>';
        }

        if (!empty($o['color'])){
            $db_colors = query_new('SELECT `id`, `title`, `class` FROM `sw_colors` WHERE `active` = 1 ',0,'id');
            $colors = explode('-', $o['color']);
            $act_cols = array();
            $color_text = '<br><span style="padding: 0px 0px 0px 15px; vertical-align:top;">����(�):</span>'
                . '<ul style="display: inline-block; list-style: outside none none; padding: 0px 0px 0px 5px; margin: 0px;">';
            foreach ($colors as $col_id){
                $color_text .= !empty($db_colors[$col_id]['title'])?'<li>'.$db_colors[$col_id]['title'].'</li>':'';
                if (!empty($db_colors[$col_id]['title'])){
                    $act_cols[] = $db_colors[$col_id]['title'];
                }
            }
            if (!empty($_GET['task']) && $_GET['task']=='acts'){ $color_text = !empty($act_cols)?implode(', ',$act_cols):''; }
            else{ $color_text .= '</ul>'; }
        }else{
            $color_text = '';
        }
//        $holiday = query_new('SELECT `center` FROM `sw_holiday` WHERE `active`=1 LIMIT 1',1);
//        $markup = empty($holiday)?0:$holiday['center'];
//        $d = ($o['type']=='my-order' || !empty($o['share']))?0:1;
//        $discount = empty($o['discount'])?0:$o['discount'];
//        $summ = ceil($o['price']*(1+$markup/100));
//        $summ = ceil($summ*(1-$discount*$d/100));
        $sum = $o['price']*$o['count'];
        if (!empty($_GET['task']) && $_GET['task']=='acts'){
            $u_data = '<p><b>��������: </b>'.$o['uName'].'</p><p><b>�������: </b>'.$o['uPhone'].'</p><p><b>����������� �����������:</b> <br><p>'.$o['uComment'].'</p></p>';
            if ($_GET['t']=='client'){
                $img = explode(':', $o['image']);
                $ico = '<img src="/files/catalog/'.$o['cid'].'/w168_'.$img[0].'" width="130">';
                $color_text = !empty($color_text)?'<p><b>����: </b>'.$color_text.'</p>':'';
                $struct = empty($o['structure'])?'':'<b>������:</b><br>'.$o['structure'];
                $tbl_list.='<div class="item-border"><table class="item"><tr><td colspan="2"><p><b>'.$o['title'].'</b></p></td>'
                        . '<td rowspan="2" width="250" align="left" class="cBorder"><p class="title"><b>������������� � ������:</b></p>'
                        . $color_text.'<div class="counts">����������: '.$o['count'].'</div></td></tr>'
                        . '<tr><td style="text-align: center; padding-bottom: 10px;" width="150">'.$ico.(!empty($o['art']) ? '<br><span class="hint">���.: '.$o['art'].'</span>' : null).'</td>'
                        . '<td style="line-height: 20px; padding: 0px 10px;" width="150">'.$struct.'</td></tr></table></div>';
                $tbl[] = array($tbl_list.'<div class="recipient"><p class="title"><b>������ ����������:</b></p>'.$u_data.'<div class="datetime">����:<b>'.$o['delivery_date'].'</b></div></div></td>');
                $tbl[] = array('<td class="footer"><table class="autograph" style="height:40px;"><tr><td width="330">��������� �� �������� ������������� �� ����:</td><td>_________________________________</td></tr></table>'
                    . '<table class="contacts"><tr><td width="230"><p><b>StudioFloristic.ru</b> - ����� ��� �������!</p><p>www.studiofloristic.ru</p></td><td><p>������� ��� ���������� ������� �� ������:<br /><b>8-800-333-12-91</b> (24 ���� / 7 ����)</p><p>������� � ������ <b>+7(495)255-12-91</b></p></td>'
                    . '<td width="200"><p>E-mail: <b>info@studiofloristic.ru</b></p><p>Skype: <b>studiofloristic</b></p><p>WhatsApp/Viber: <b>8 929 951-88-32</b></p></td></tr></table></td>');
            }else{
                $color_text = !empty($color_text)?'; ����: '.$color_text:'';
                $tbl_list.='<p>���. '.$o['art'].': '.$o['title'].(!empty($o['structure'])?' � '.str_replace("\n", '; ', $o['structure']):'')
                    .' / '.$o['price'].' ���.'.($o['count']>1?' x '.$o['count'].'��. = '.$sum.' ���.':'').'</p>';
                $tbl[] = array($tbl_list.'</td>');
                $tbl[] = array('<td>��������:</td><td>'.$u_data.'</td>');
                $tbl[] = array('<td>����������� ���������:</td><td>'.$o['comment'].'</td>');
                $tbl[] = array('<td>� ������:</td><td><b>'.$sum.' ���.</b></td>');
                $tbl[] = array('<td>������ �������:</td><td>'.(empty($o['discount'])?'0':$o['discount']).'%</td>');
                $tbl[] = array('<td colspan="2" style="border: 0px;">&nbsp;</td>');
                $tbl[] = array('<td style="border: 0px;">��������:</td><td style="border: 0px;">___________________________________________</td>');
                $tbl[] = array('<td style="border: 0px;">�������:</td><td style="border: 0px;">___________________________________________</td>');
                $tbl[] = array('<td colspan="2" style="border: 0px;">&nbsp;</td>');
                $tbl[] = array('<td colspan="2" style="border: 0px;">&nbsp;</td>');
            }
            $data['content'] = $tbl;
            render_acts($data);
        }else{
            $html .= '<tr><td class="title"><a href="'.$o['url'].'" target="_blank">'.$o['title'].'</a>'.$color_text
                . '</td><td>'.$o['count'].'��</td><td>'.$o['price'].'�</td><td>'.$sum.'�</td></tr></table></p>';
        }
		return $html;
	}
	return false;
}
function viewOrd_fast_baying(){
	$id = abs(intval($_GET['id']));
    $box = array('����� (���������)', '�������� ����� (50 ���.)', '����������� �������� (200 ���.)', '���������� �������� (250 ���.)');
    $o = query_new('SELECT * FROM `sw_fast_baying` WHERE `id` = '.quote($id),1);
	if (!empty($o)){
        $products = unserialize(base64_decode($o['tovar_list']));
        $basket_info = unserialize(base64_decode($o['basket_info']));
		$markup = ($basket_info['discount_summ']==$o['amount'])?0:$basket_info['markup'];
        if (!empty($_GET['task']) && $_GET['task']=='acts'){
            $data = array('title'=>'��� ��� '.($_GET['t']=='client'?'�������':'��������'),'class'=>($_GET['t']=='client'?'':'courier'));
            $tbl = array();
            if ($_GET['t']=='client'){
                $tbl[] = array('<td class="header"><table><tr><td><img src="/i/logo-studio-floristic-green1.jpg"></td><td align="right"><b class="hint">����� �'.$id.'</b></td></tr></table></td>');
            }else{
                $tbl[] = array('<td colspan="2">����� �'.$id.'</td>');
                $tbl[] = array('<td width="200">���� ��������:</td>','<td>'.$o['delivery_date'].'</td>');
            }
        }else{
            $html = '<p><b>��������:</b> '.$o['name'].'</p>'
                . '<p><b>�������:</b> '.$o['phone'].'</p>'
                . '<p><b>���� ��������:</b> '.(!empty($o['delivery_date'])?$o['delivery_date']:'�� �������').'</p>'
                . '<p><b>����� � ���������:</b> '.$o['current_time'].'</p>'
                . '<p><b>���������:</b> '.(!empty($o['call_now'])?'<font color="red">������</font>'
                    :(preg_match('^[:0]{8}$',$o['call_time_end'])===1?'� '.$o['call_time']:'� '.$o['call_time'].' �� '.$o['call_time_end'])).'</p>'
                . '<p><b>����� ������:</b> '.$o['amount'].' ���.</p>'
                . '<p><b>���-�� ������:</b> '.$basket_info['count'].' ��.</p>'
                . '<p><b>������:</b> '.$basket_info['discounts']['user'].'%</p>'
                . '<p><b>�������:</b> '.$markup.'%</p>'
                . '<hr /><b>���������� ���������:</b> <span>'.str_replace("\n", '', $o['comment']).'</span></p>';
        }
		if (!empty($products)){
			$db_colors = query_new('SELECT `id`, `title`, `class` FROM `sw_colors` WHERE `active` = 1 ',0,'id');
            if (!empty($_GET['task']) && $_GET['task']=='acts'){
                $tbl_list = $_GET['t']=='client'?'<td class="items">':'<td>������ ������:</td><td>';
            }else{
                $html .= '<hr /><p><b>���������� ������:</b></p><p><table class="basket-items"><tr><th class="title">������������</th><th>���-��</th><th>����</th><th>�����</th></tr>';
            }
            $get_amount = ($basket_info['discount_summ']==$o['amount'])?'discount_amount':'mark_disc';
			foreach ($products as $c){
//                if ($c['type'])
				$amount = $c[$get_amount];
				if (!empty($c['color'])){
                    $act_cols = array();
					$colors = explode('-', $c['color']);
					$color_text = '<br><span style="padding: 0px 0px 0px 15px; vertical-align:top;">����(�):</span>'
						. '<ul style="display: inline-block; list-style: outside none none; padding: 0px 0px 0px 5px; margin: 0px;">';
					foreach ($colors as $col_id){
						$color_text .= !empty($db_colors[$col_id]['title'])?'<li>'.$db_colors[$col_id]['title'].'</li>':'';
                        if (!empty($db_colors[$col_id]['title'])){
                            $act_cols[] = $db_colors[$col_id]['title'];
                        }
					}
                    if (!empty($_GET['task']) && $_GET['task']=='acts'){ $color_text = !empty($act_cols)?implode(', ',$act_cols):''; }
                    else{ $color_text .= '</ul>'; }
				}else{
					$color_text = '';
				}
                if (!empty($_GET['task']) && $_GET['task']=='acts'){
                    if ($_GET['t']=='client'){
                        $img = explode(':', $c['image']);

                        switch ($c['type']) {
                            case 'item': $ico = '<img src="/files/catalog/'.$c['cid'].'/w168_'.$img[0].'" width="130">'; break;
                            case 'my-order': $ico = '<img src="/i/individ-100.jpg">'; break;
                            case 'my-bouquet': $ico = '<img src="/i/myBouquet.gif">'; break;
                            case 'gift':
                                $gift = query_new('SELECT * FROM `sw_gifts` WHERE `id`='.$c['cid'],1);
                                $img = explode(':', $gift['image']);
                                $ico = '<img src="/files/gifts/'.$c['cid'].'/w168_'.$img[0].'" width="130">'; break;
                        }
                        $color_text = !empty($color_text)?'<p><b>����: </b>'.$color_text.'</p>':'';
                        $struct = empty($c['structure'])?'':'<b>������:</b><br>'.$c['structure'];
                        $tbl_list.='<div class="item-border"><table class="item"><tr><td colspan="2"><p><b>'.$c['title'].'</b></p></td>'
                                . '<td rowspan="2" width="250" align="left" class="cBorder"><p class="title">'.($c['type']!='gift'?'<b>������������� � ������:</b></p><p><b>����������:</b> '.$box[$c['box']].'</p>'
                                . '<p><b>����:</b> '.(!empty($c['vase']) ? '����' : '���').'</p>'.$color_text:'<b>���������� ������� � ������</b></p><p><b>��������:</b> '.$gift['text'].'</p>').'<div class="counts">����������: '.$c['count'].'</div></td></tr>'
                                . '<tr><td style="text-align: center; padding-bottom: 10px;" width="150">'.$ico.(!empty($c['art']) ? '<br><span class="hint">���.: '.$c['art'].'</span>' : null).'</td>'
                                . '<td style="line-height: 20px; padding: 0px 10px;" width="150">'.$struct.'</td></tr></table></div>';
                    }else{
                        $art = ($c['type']=='qift')?'�������: ':(empty($c['art'])? '':'���. '.$c['art'].': ');
                        $color_text = !empty($color_text)?'; ����: '.$color_text:'';
                        $tbl_list.='<p>'.$art.$c['title'].(!empty($c['structure'])?' � '.str_replace("\n", '; ', str_replace("<br />", '; ',$c['structure'])):'')
                            .'; ����������: '.$box[$c['box']].$color_text.'; ����: '.(!empty($c['vase']) ? '����' : '���').' / '.$c['amount'].' ���.'.($markup!=0?' + '.$markup.'%':'')
                            .($basket_info['discounts']['user']!=0?' - '.$basket_info['discounts']['user'].'%':'').' = '.$amount.' ���.'.($c['count']>1?' x '.$c['count'].'��. = '.($amount*$c['count']).' ���.':'').'</p>';
                    }
                }else{
                    $html .= '<tr><td class="title"><a href="'.$c['url'].'" '.($c['type']=='item'? 'target="_blank"':'').'>'.$c['title'].'</a>'.$color_text
                        . (!empty($c['box'])? ' + '.$box[$c['box']]: '').(!empty($c['vase'])?' + ����':'')
                        . ($c['type']!='item' && $c['type']!='gift'?' | <a href="javascript:order.item('.$c['id'].')">��������</a>':($c['type']=='gift'?' (�������)':''))
                        . '</td><td>'.$c['count'].'��</td><td>'.$amount.'�</td><td>'.($amount*$c['count']).'�</td></tr>';
                }
			}
            if (!empty($_GET['task']) && $_GET['task']=='acts'){
                if ($_GET['t']=='client'){
                    $tbl[] = array($tbl_list.'<div class="recipient"><p>&nbsp;</p><div class="datetime">����:<b>'.(!empty($o['delivery_date'])?$o['delivery_date']:'�� �������').'</b></div></div></td>');
                    $tbl[] = array('<td class="footer"><table class="autograph"><tr><td width="330">��������� �� �������� ������������� �� ����:</td><td>_________________________________</td></tr></table>'
                        . '<table class="contacts"><tr><td width="230"><p><b>StudioFloristic.ru</b> - ����� ��� �������!</p><p>www.studiofloristic.ru</p></td><td><p>������� ��� ���������� ������� �� ������:<br /><b>8-800-333-12-91</b> (24 ���� / 7 ����)</p><p>������� � ������ <b>+7(495)255-12-91</b></p></td>'
                        . '<td width="200"><p>E-mail: <b>info@studiofloristic.ru</b></p><p>Skype: <b>studiofloristic</b></p><p>WhatsApp/Viber: <b>8 929 951-88-32</b></p></td></tr></table></td>');
                }else{
                    $tbl[] = array($tbl_list.'</td>');
                    $tbl[] = array('<td>� ������:</td><td><b>'.$o['amount'].' ���.</b></td>');
                    $tbl[] = array('<td>������ �������:</td><td>'.$basket_info['discounts']['user'].'%</td>');
                    $tbl[] = array('<td>���������� ���������:</td><td>'.$o['text'].'</td>');
                    $tbl[] = array('<td colspan="2" style="border: 0px;">&nbsp;</td>');
                    $tbl[] = array('<td style="border: 0px;">��������:</td><td style="border: 0px;">___________________________________________</td>');
                    $tbl[] = array('<td style="border: 0px;">�������:</td><td style="border: 0px;">___________________________________________</td>');
                }
                $data['content'] = $tbl;
                render_acts($data);
            }else{
                $html .= '</table></p>';
            }
		}
		return $html;
	}
	return false;
}
function section() {
	if (!_IS_ADMIN || empty($_GET['id']) || empty($_GET['sid'])) jQuery::getResponse();

	$id = abs(intval($_GET['id']));
	$sid = abs(intval($_GET['sid']));

	db_query('UPDATE `sw_catalog_section` SET `first` = 0 WHERE `cid` = '.quote($id));
	db_query('UPDATE `sw_catalog_section` SET `first` = 1 WHERE `cid` = '.quote($id).' AND `sid` = '.quote($sid));

	$r = db_query('SELECT s.`id`, s.`title`, c.`first` FROM `sw_catalog_section` as c LEFT JOIN `sw_section` as s ON c.`sid` = s.`id` WHERE c.`cid` = '.quote($id));
	while ($c = mysql_fetch_array($r, MYSQL_ASSOC)) {
		$item = !empty($c['first']) ? $c['title'] : '<a href="javascript:module.section('.$id.','.$c['id'].')">'.$c['title'].'</a>';
		!empty($html) ? $html.= ' | '.$item : $html = $item;
	}

	jQuery('div#current-topics span')->html(inUTF8($html));
	jQuery::getResponse();
}

function loadImage() {
	jQuery::evalScript(inUTF8('file.init({"�����������": "*.jpg; *.jpeg; *.gif; *.png"})'));
	jQuery::getResponse();
}

/*
 * �������� ����� ������� ��� ����������� ������
 */
function chkNewOrders(){
    // TODO: �������� ������� �� �����


    // �������� �������
        if (!isset($_SESSION['admin'])){
            jQuery::getResponse();
        }

        $adm_uid = isset($_SESSION['admin'][0]) ?  intval($_SESSION['admin'][0]) : header('location: /');
        $query = "SELECT notification FROM sw_admin WHERE id = $adm_uid  LIMIT 0, 1;";
        $r = db_query($query);
        $uidHandle = mysql_fetch_array($r, MYSQL_ASSOC);
        $useNotify = (intval($uidHandle['notification']) > 0 ) ? true : false;

        if ($useNotify) {

            $hdr = "<h2><img src=\"/sf123sf123sf/i/active.gif\">������� ����� �����</h2>";
            // 1. ������� ��������� ����� �� ������� �������
            $query = "SELECT * FROM sw_orders ORDER BY `insert` DESC LIMIT 0, 1;";
            $r = db_query($query);
            $last_order = mysql_fetch_array($r, MYSQL_ASSOC);
            //2. ������ ��������� ����� � ����� �������
            $query = "SELECT * FROM sw_orders_fast ORDER BY `insert` DESC LIMIT 0, 1;";
            $r = db_query($query);
            $last_order_fast = mysql_fetch_array($r, MYSQL_ASSOC);
            //3. ������� ��������� ����� ���������� �� ������� �������
            $query = "SELECT * FROM sw_fast_baying ORDER BY `insert` DESC LIMIT 0, 1;";
            $r = db_query($query);
            $last_fast_buy = mysql_fetch_array($r, MYSQL_ASSOC);
            if (!isset($_SESSION['last_ordrs'])) {
                //������ ������
                $_SESSION['last_ordrs']['order'] = $last_order;
                $_SESSION['last_ordrs']['fast_order'] = $last_order_fast;
                $_SESSION['last_ordrs']['fast_buy'] = $last_fast_buy;
                jQuery::getResponse();
            } else {

                if (intval($last_order['id']) > $_SESSION['last_ordrs']['order']['id']) {
                    $close = "<br/><br/><br/><div align=\"center\"><span style=\"text-decoration: underline; cursor: pointer; color: #0A246A;\" onclick=\"module.load(\'orders\')\"><img src=\"/sf123sf123sf/i/orders.png\"/>&nbsp;[������� � ONLINE �������]</a></span>&nbsp;&nbsp;&nbsp;&nbsp;<span style=\"text-decoration: underline; cursor: pointer; color: #0A246A;\">[�������]</a></span></div><br/><audio autoplay=\"autoplay\"><source src=\"/files/sounds/martian-gun.mp3\" type=\"audio/mpeg\"><source src=\"/files/sounds/martian-gun.ogg\" type=\"audio/ogg; codecs=vorbis\"></audio>";
                    // ������� ����� ������� �����
                    $_SESSION['last_ordrs']['order'] = $last_order;
                    $html = $hdr . "<p><b>����� ������</b>: " . $last_order['id'] . "</p>";
                    $html .= "<p><b>��� ������:</b> ONLINE �����</p>";
                    $html .= "<p><b>�����:</b> " . $last_order['itogo'] . " </p>";
                    $html .= "<p><b>��������:</b> " . $last_order['uName'] . " </p>";
                    $html .= "<p><b>�������:</b> " . $last_order['uPhone'] . " </p>";
                    $recipInfo = explode("</p>",$last_order['recipient'] ) ;
                    $recip = array($recipInfo[0], $recipInfo[1]);

                    $html .= str_replace( array("'", '"', "`", "�", "|", "�", "\r", "\n", "\r\n"), "", ((implode("</p>", $recip)))   )  . $close;
                    $h = iconv("WINDOWS-1251", "UTF-8//IGNORE", $html);
                    jQuery::evalScript("initSysDialogWOClose('$h')");
                    jQuery::getResponse();
                } else {
                    if (intval($last_order_fast['id']) > $_SESSION['last_ordrs']['fast_order']['id']) {
                        $close = "<br/><br/><br/><div align=\"center\"><span style=\"text-decoration: underline; cursor: pointer; color: #0A246A;\" onclick=\"module.load(\'orders_fast\')\"><img src=\"/sf123sf123sf/i/order.gif\"/>&nbsp;[������� � ������� �������]</a></span>&nbsp;&nbsp;&nbsp;&nbsp;<span style=\"text-decoration: underline; cursor: pointer; color: #0A246A;\">[�������]</a></span></div><br/><audio autoplay=\"autoplay\"><source src=\"/files/sounds/martian-gun.mp3\" type=\"audio/mpeg\"><source src=\"/files/sounds/martian-gun.ogg\" type=\"audio/ogg; codecs=vorbis\"></audio>";
                        // ������� ����� ������� �����
                        $_SESSION['last_ordrs']['fast_order'] = $last_order_fast;
                        $html = $hdr . "<p><b>����� ������</b>: " . $last_order_fast['id'] . "</p>";
                        $html .= "<p><b>��� ������:</b> ������� �����</p>";
                        $html .= "<p><b>�����:</b> " . round(intval($last_order_fast['price']) * intval($last_order_fast['count']), 2) . " </p>";
                        $html .= "<p><b>���:</b> " . str_replace(array("'", '"', "`", "�", "|", "�", "\r", "\n" , "\r\n"), "", (($last_order_fast['uName']))  )  . " </p>";
                        $html .= "<p><b>�������:</b> " . str_replace(array("'", '"', "`", "�", "|", "�", "\r", "\n" , "\r\n"), "", (($last_order_fast['uPhone']))  )  . " </p>";
                        $html .= $close;
                        $h = iconv("WINDOWS-1251", "UTF-8//IGNORE", $html);
                        jQuery::evalScript("initSysDialogWOClose('$h')");
                        jQuery::getResponse();
                    } else {
                        if (intval($last_fast_buy['id']) > $_SESSION['last_ordrs']['fast_buy']['id']) {
                            $close = "<br/><br/><br/><div align=\"center\"><span style=\"text-decoration: underline; cursor: pointer; color: #0A246A;\" onclick=\"module.load(\'fast_baying\')\"><img src=\"/sf123sf123sf/i/basket_go.png\">&nbsp;[������� � ������� ��������]</a></span>&nbsp;&nbsp;&nbsp;&nbsp;<span style=\"text-decoration: underline; cursor: pointer; color: #0A246A;\">[�������]</a></span></div><br/><audio autoplay=\"autoplay\"><source src=\"/files/sounds/martian-gun.mp3\" type=\"audio/mpeg\"><source src=\"/files/sounds/martian-gun.ogg\" type=\"audio/ogg; codecs=vorbis\"></audio>";
                            // ������� �������
                            $_SESSION['last_ordrs']['fast_buy'] = $last_fast_buy;
                            $html = $hdr . "<p><b>����� ������</b>: " . $last_fast_buy['id'] . "</p>";
                            $html .= "<p><b>��� ������:</b> ������� �������</p>";
                            $html .= "<p><b>���:</b> " . str_replace(array("'", '"', "`", "�", "|", "�", "\r", "\n" , "\r\n"), "", (($last_fast_buy['name']))   )  . " </p>";
                            $html .= "<p><b>�������:</b> " . str_replace(array("'", '"', "`", "�", "|", "�", "\r", "\n" , "\r\n"), "", (($last_fast_buy['phone'] )) ). " </p>";
                            $html .= $close;
                            $h = iconv("WINDOWS-1251", "UTF-8//IGNORE", $html);
                            jQuery::evalScript("initSysDialogWOClose('$h')");
                            jQuery::getResponse();
                        } else {
                            // ������ �� ������
                            jQuery::getResponse();
                        }
                    }
                }
            }
        }else{
            jQuery::getResponse();
        }
}

function initMenu() {
	$query = 'SELECT ';
	$query.= '(SELECT COUNT(`id`) FROM `sw_users`) as `users`, ';
	$query.= '(SELECT COUNT(`uid`) FROM `sw_session` WHERE `uid` != 0) as `online`';
	$r = db_query($query);
	$count = mysql_fetch_array($r, MYSQL_ASSOC);

	$html = '<div id="info"><p>�������������: <b>'.$count['users'].'</b></p></div>';

    $html .= '<div id="chk-order-dialog" style="display: none;"><p>�������� �������...</p></div>';

	if (_IS_ADMIN) {
		$aid = abs(intval($_SESSION['admin'][0]));
		$r = db_query('SELECT `group` FROM `sw_admin` WHERE `id` = '.quote($aid));
		if (mysql_num_rows($r) == 0) jQuery::getResponse();
		$u = mysql_fetch_array($r, MYSQL_ASSOC);

		switch ($u['group']) {
			case 2: $where = ' AND `manager` = 1'; break;
			default: $where = null; break;
		}
	} else $where = null;

	$r = db_query('SELECT * FROM `sw_modules` WHERE `menu` = 1'.$where.' ORDER BY `pos` ASC');
	if (mysql_num_rows($r) != 0) {
		$html.= '<div class="title">�������� �������</div>';
		while ($m = mysql_fetch_array($r, MYSQL_ASSOC)) {
                    $style = (isset($m['icon']) && $m['icon']!='')? 'style="background-image:url(\'/sf123sf123sf/i/'.$m['icon'].'\');'
                        . ' background-position: 4px center;'
                        . ' background-repeat: no-repeat;"':'';
                    $html.= '<div id="'.$m['link'].'" class="dir'.($m['admin'] != 0 ? ' hover' : null).' " '.$style.' onclick="module.load(\''.$m['link'].'\')">'.$m['title'].'</div>';
		}
	}

	jQuery('#menu')->append(inUTF8($html));
	jQuery::evalScript('module.init()');
	jQuery::getResponse();
}

function checkbox() {
	global $_GLOBALS, $module;

	if (empty($_GET['id']) || empty($_GET['f'])) jQuery::getResponse();
	$id = abs(intval($_GET['id']));
	$f = filter(trim(in1251($_GET['f'])), 'nohtml');

	if (empty($_GLOBALS['field']['table'][$f])) jQuery::getResponse();

	$r = db_query('SELECT `'.$f.'` FROM `sw_'.$module.'` WHERE `id` = '.quote($id));
	if (mysql_num_rows($r) == 0) jQuery::getResponse();
	$i = mysql_fetch_array($r, MYSQL_NUM);

	$v = !empty($i[0]) ? 0 : 1;

	db_query('UPDATE `sw_'.$module.'` SET `'.$f.'` = '.$v.' WHERE `id` = '.quote($id));

	switch ($_GLOBALS['field']['table'][$f]){
        case 'yes/no': $t = (empty($v))?'���':'��';break;
        default: $t = (empty($v))?'��������':'���������';
    }
	jQuery::evalScript('item.checkbox('.$id.',"'.$f.'",'.$v.',"'.inUTF8($t).'")');
	jQuery::getResponse();
}

function radioSet(){
	global $_GLOBALS, $module;

	if (empty($_GET['id']) || empty($_GET['fild'])) jQuery::getResponse();

	$search = !empty($_GET['query']) ? filter(trim(in1251($_GET['query'])), 'nohtml'):'';
    $pg = !empty($_GET['pg']) ? abs(intval($_GET['pg'])) : 1;
    $sid = !empty($_GET['sid']) ? abs(intval($_GET['sid'])) : 0;
	$id = intval($_GET['id']);
	$fild = filter(trim(in1251($_GET['fild'])));

	query_new('UPDATE sw_'.$module.' SET `'.$fild.'` = 0 AND `id` != '.$id.', `active` = 0 AND `id` != '.$id.', `'.$fild.'` = 1 AND `id`='.$id.', `active` = 1 AND `id` = '.$id.' WHERE sid = '.$sid);
	$ret = initModule($sid,$pg,$search,true);

	jQuery('#content-data')->html(inUTF8($ret['html']));

	jQuery::evalScript('items.odd("table#list-items.items");');
	jQuery::evalScript('module.set('.$pg.',"'.htmlspecialchars(urlencode(inUTF8($search))).'")');
	if (!empty($_GLOBALS['tiny'])) jQuery::evalScript('htmlEditor.init()');
	if (!empty($_GLOBALS['date'])) jQuery::evalScript('date.init()');

	jQuery::getResponse();
}

function Active() {
	global $module;

	if (empty($_GET['id'])) jQuery::getResponse();
	$id = abs(intval($_GET['id']));

	if ($module == 'vases'){
        $r = db_query('SELECT `status` FROM `sw_'.$module.'` WHERE `id` = '.quote($id));
        if (mysql_num_rows($r) == 0) jQuery::getResponse();
        $i = mysql_fetch_array($r, MYSQL_NUM);

        if (!empty($i[0])) {
            $act = 0;
            jQuery::evalScript('active_vases.off('.$id.', "'.$module.'")');
        } else {
            $act = 1;
            jQuery::evalScript('active_vases.on('.$id.', "'.$module.'")');
        }

        db_query('UPDATE `sw_'.$module.'` SET `status` = '.$act.' WHERE `id` = '.quote($id));
    }else{
        $r = db_query('SELECT `active` FROM `sw_'.$module.'` WHERE `id` = '.quote($id));
        if (mysql_num_rows($r) == 0) jQuery::getResponse();
        $i = mysql_fetch_array($r, MYSQL_NUM);

        if (!empty($i[0])) {
            $act = 0;
            jQuery::evalScript('active.off('.$id.', "'.$module.'")');
        } else {
            $act = 1;
            jQuery::evalScript('active.on('.$id.', "'.$module.'")');
        }

        db_query('UPDATE `sw_'.$module.'` SET `active` = '.$act.' WHERE `id` = '.quote($id));
    }

	jQuery::getResponse();
}

function activateStatusVerified()
{
    global $module;

    // ���� �� ������� ID, ������ ������
    if (empty($_GET['id']))
    {
        jQuery::getResponse();
    }

    $id = abs(intval($_GET['id']));

    $r = db_query('SELECT `status_verified` FROM `sw_orders` WHERE `id` = '.quote($id));
    if (mysql_num_rows($r) == 0)
    {
        jQuery::getResponse();
    }
    $i = mysql_fetch_array($r, MYSQL_NUM);

    if (!empty($i[0])) {
        $action = 0;
        jQuery::evalScript('activateStatusVerified.off('.$id.')');
    } else {
        $action = 1;
        jQuery::evalScript('activateStatusVerified.on('.$id.')');
    }

    db_query('UPDATE `sw_'.$module.'` SET `status_verified` = '.$action.' WHERE `id` = '.quote($id));
    if ($action == 1)
    {
        // ��������� SMS ��������� � ��������
        if (!class_exists('SmsQueue'))
        {
            include_once '../../system/smsqueue.php';
        }

        $smsQueue = new SmsQueue();

        $params = array(
            'oid' => $id,
            'msg_type' => SmsQueue::ORDER_DELIVERED
        );

      //  $smsQueue->addMessageInQueue($params);
        $smsQueue->sendEmailWhenOrderAcepted($params);
    }
    jQuery::getResponse();

}

function activateStatusPrinted(){
    global $module;

    // ���� �� ������� ID, ������ ������
    if (empty($_GET['id']))
    {
        jQuery::getResponse();
    }
    $id = abs(intval($_GET['id']));
    $r = db_query('SELECT `status-printed`  FROM `sw_orders` WHERE `id` = '.quote($id));
    if (mysql_num_rows($r) == 0)
    {
        jQuery::getResponse();
    }
    $i = mysql_fetch_array($r, MYSQL_NUM);
    $i = $i[0];
    if ($i >= 1) {
        $i = 0;
        jQuery::evalScript('activateStatusPrint.off('.$id.')');
    }else{
        $i = 1;
        jQuery::evalScript('activateStatusPrint.on('.$id.')');

    }

    db_query('UPDATE `sw_orders` SET `status-printed` = '.$i .' WHERE `id` = '.quote($id));
    jQuery::getResponse();
}

function activateStatusReady(){
    global $module;

    // ���� �� ������� ID, ������ ������
    if (empty($_GET['id']))
    {
        jQuery::getResponse();
    }
    $id = abs(intval($_GET['id']));
    $r = db_query('SELECT `status_ready`  FROM `sw_orders` WHERE `id` = '.quote($id));
    if (mysql_num_rows($r) == 0)
    {
        jQuery::getResponse();
    }
    $i = mysql_fetch_array($r, MYSQL_NUM);
    $i = $i[0];
    if ($i >= 1) {
        $i = 0;
        jQuery::evalScript('activateStatusReady.off('.$id.')');
    }else{
        $i = 1;
        jQuery::evalScript('activateStatusReady.on('.$id.')');

    }

    db_query('UPDATE `sw_orders` SET `status_ready` = '.$i .' WHERE `id` = '.quote($id));
    jQuery::getResponse();
}

function activateStatusDelivered()
{
    global $module;

    // ���� �� ������� ID, ������ ������
    if (empty($_GET['id']))
    {
        jQuery::getResponse();
    }

    $id = abs(intval($_GET['id']));

    $r = db_query('SELECT `status_delivered` FROM `sw_orders` WHERE `id` = '.quote($id));
    if (mysql_num_rows($r) == 0)
    {
        jQuery::getResponse();
    }
    $i = mysql_fetch_array($r, MYSQL_NUM);

    if (!empty($i[0]))
    {
        $action = 0;
        jQuery::evalScript('activateStatusDelivered.off('.$id.')');
    }
    else
    {
        $action = 1;
        jQuery::evalScript('activateStatusDelivered.on('.$id.')');
    }

    db_query('UPDATE `sw_'.$module.'` SET `status_delivered` = '.$action.' WHERE `id` = '.quote($id));

    // �������� SMS ��� ��������
    if ($action == 1)
    {
        // ��������� SMS ��������� � ��������
        if (!class_exists('SmsQueue'))
        {
            include_once '../../system/smsqueue.php';
        }

        $smsQueue = new SmsQueue();

        $params = array(
            'oid' => $id,
            'msg_type' => SmsQueue::ORDER_DELIVERED
        );

        $smsQueue->addMessageInQueue($params);
        $smsQueue->sendEmailWhenOrderDelivered($params);
    }

    jQuery::getResponse();
}

function activateStatusSend()
{
	global $module;

	// ���� �� ������� ID, ������ ������
	if (empty($_GET['id']))
	{
		if (empty($_SERVER['QUERY_STRING'])){
			jQuery::getResponse();
		}else{

			preg_match_all('/id=([0-9]*)/', $_SERVER['QUERY_STRING'], $result, PREG_PATTERN_ORDER);
			$idarr = str_replace('id=', '', $result[0]) ;
			$id = $idarr[0];

		}

	}
	if (empty($id)){
		$id = abs(intval($_GET['id']));
	}

	// ������� �� ������� ������ ������ �������
	preg_match_all('/setcur=(.*)/', $_SERVER['QUERY_STRING'], $cr, PREG_PATTERN_ORDER);
	$cr = iconv("UTF-8", "WINDOWS-1251//IGNORE", str_replace('setcur=', '',  urldecode($cr[0][0])))  ;

	$r = db_query('SELECT `status_sended`, `courier` FROM `sw_orders` WHERE `id` = '.quote($id));
	if (mysql_num_rows($r) == 0)
	{
		jQuery::getResponse();
	}
	$i = mysql_fetch_array($r, MYSQL_NUM);

	if (!empty($i[0]))
	{
		$action = 0;
		jQuery::evalScript('activateStatusSend.off('.$id.')');
	}
	else
	{
		if (!empty($cr)){
			$c_str = "������: " . $cr;
			db_query('UPDATE `sw_orders` SET `courier` = '.quote($cr).' WHERE `id` = '.quote($id));
		}
		else
		{

			if (empty($i[1]))
			{
				$c_str = "������ �� ������";
			}
			else
			{
				$c_str = "������: " . $i[1];
			}
		}
		$action = 1;
	//	jQuery::evalScript('activateStatusSend.courier = \''. $c_str .'\';');
		jQuery::evalScript('activateStatusSend.on('.$id.', \''. inUTF8($c_str ) .'\')');
	}

	db_query('UPDATE `sw_'.$module.'` SET `status_sended` = '.$action.' WHERE `id` = '.quote($id));

	// �������� SMS ��� ��������
	if ($action == 1)
	{
		// ��������� SMS ��������� � ��������
		/*
		if (!class_exists('SmsQueue'))
		{
			include_once '../../system/smsqueue.php';
		}

		$smsQueue = new SmsQueue();

		$params = array(
			'oid' => $id,
			'msg_type' => SmsQueue::ORDER_DELIVERED
		);

		$smsQueue->addMessageInQueue($params);
		$smsQueue->sendEmailWhenOrderDelivered($params);
		*/
	}

	jQuery::getResponse();
}


function changeOrderCashPaymentStatus()
{
    global $module;
    // ���� �� ������� ID ������, ������ ������
    if (empty($_GET['id']))
    {
        jQuery::getResponse();
    }

    $send_sms = isset($_GET['sendsms']) ? boolval($_GET['sendsms']) : false;

    $id = abs(intval($_GET['id']));
    $r = db_query('SELECT `status`, `itogo`, `recipientSMS`, `type`, `uid`  FROM `sw_orders` WHERE `id` = '.quote($id));
    if (mysql_num_rows($r) == 0)
    {
        jQuery::getResponse();
    }

    $i = mysql_fetch_array($r, MYSQL_NUM);

    if (!empty($i[0]))
    {
        $action = 0;
        jQuery::evalScript('changeOrderCashPaymentStatus.off('.$id.')');
    }
    else
    {
        $action = 1;
        jQuery::evalScript('changeOrderCashPaymentStatus.on('.$id.')');
    }

    db_query('UPDATE `sw_'.$module.'` SET `status` = '.$action.' WHERE `id` = '.quote($id));

    logevent(8, $_SESSION['admin'][0], 2, array('order_id' => $id, 'itog' => $i[1], 'comment' =>$i[2], 'type' => $i[3], 'is_payed' =>$action) ); // ��������
     // ����������� ������ ������������
    if (!empty($i[4])){
        reloadUserDiscounts($i[4]);
    }
    // ��������� �������� ���������
    // ��, ��� �����

    // - 21.09.2016 ������ ��� ���� �������
    if ($send_sms) {
        $order = SmsQueue::getOrderDataByID($id);

        //if (in_array($order['type'], SmsQueue::$offline_payments))
        if (!in_array($order['type'], SmsQueue::$nodialog_payments)) {
            $smsQueue = new SmsQueue();
            $params['oid'] = $id;
            $params['msg_type'] = SmsQueue::ORDER_ONLINE_PAYMENT_SUCCESS;
            $smsQueue->addMessageInQueue($params);
        }
    }
    jQuery::getResponse();

}

// ��������� ���
function initModule($sid = 0, $pg = 1, $search = '', $ret = false)
{
	global $_GLOBALS, $m, $module, $op;

    if ($ret===false){
        if (!empty($_GET['query'])) { $search = filter(trim(in1251($_GET['query'])), 'nohtml'); }
        if (!empty($_GET['pg'])) { $pg = abs(intval($_GET['pg'])); }
        if (!empty($_GET['sid'])) { $sid = abs(intval($_GET['sid'])); }
    }

	$on_page = !empty($_GLOBALS['on-page']) ? $_GLOBALS['on-page'] : 10;
	$min = $on_page * ($pg - 1);
	$where = 'i.`sid` = '.$sid;

	if (!empty($search)) {
		if (substr($search, 0, 2) != 'id')
        {
			if (!empty($_GLOBALS['field']['search']))
            {
				$where = '';
				foreach ($_GLOBALS['field']['search'] as $f)
                {
                    $where .= ($where!=''?' OR ':'').' i.`'.$f.'` LIKE '.quote('%'.$search.'%').' ';
                }
			}
            else
            {
                $where = 'i.`title` LIKE '.quote('%'.$search.'%');
            }
		} else {
			$where = 'i.`id` = '.quote(abs(intval(substr($search, 2))));
		}
	}

	if ($module != 'notify')
	{
		$html = '<h1 style="float: left; margin-right: 13px;"><a href="javascript:module.index()">'.$m['title'].'</a>'.(!empty($_GLOBALS['add']) ? ' <img src="/sf123sf123sf/i/add.png" title="��������" align="absmiddle" onclick="initGET(\'/go/m='.$module.'&op=item\'); module.setState(\'edit\');">' : null).'</h1>';
	}

    if ($module == 'instagram_photos')
    {
        $conf = [
            'apiKey'=>INSTAGRAM_API_KEY,
            'apiSecret'=>INSTAGRAM_API_SECRET,
            'apiCallback'=>INSTAGRAM_API_CALLBACK
        ];
        $instagram = new Instagram($conf);
        $html.= '<a href="javascript:instagramm.update()" class="insta_task"><span>�������� ���� c</span><img src="/sf123sf123sf/i/insta.png"></a>&nbsp;&nbsp;&nbsp;&nbsp;';
        $html.= '<a href="'.$instagram->getLoginUrl().'" class="insta_task"><span>�������� access_token</span></a>';
    }

    // ������ ����
    if($module == 'orders'){
        // ����� ��������
        if(isset($_GET['filter_clear']) && $_GET['filter_clear'] == true){
            unset($_SESSION['calendar_do']);
            unset($_SESSION['calendar_ot']);
            unset($_SESSION['row_sort']);
            unset($_SESSION['selected_shiping']);
            unset($_SESSION['pay-type']);
            unset($_SESSION['time-range-code']);
            unset($_SESSION['time-range-start']);
            unset($_SESSION['time-range-end']);
        }

        // ���� ������ ������ ������������
        if(!empty($_GET['calendar_do']) && !empty($_GET['calendar_ot'])){
            $_SESSION['calendar_do'] = $_GET['calendar_do'];
            $_SESSION['calendar_ot'] = $_GET['calendar_ot'];

            $calendar_do = explode('-',$_GET['calendar_do']);
            //���������� 1 ����
            $y = $calendar_do[2];
            $m = $calendar_do[1];
            $d = $calendar_do[0]+1;
            $dt = mktime(0,0,0,$m,$d,$y);
            $calendar_do= date('Y-m-d',$dt);

            $calendar_ot = $_GET['calendar_ot'];
            $calendar_ot = explode('-',$_GET['calendar_ot']);
            $calendar_ot = $calendar_ot[2].'-'.$calendar_ot[1].'-'.$calendar_ot[0];

            if($_GET['tip_sortirovki'] == '1'){
                $selected_add = 'selected';
                $selected_delivery = '';
                $row_sor = 'insert';
            } else {
                $selected_add = '';
                $selected_delivery = 'selected';
                $row_sor = 'timeDelivery';
            }

            $_SESSION['row_sort'] = $_GET['tip_sortirovki'];

            $where = "`".$row_sor."` >= '".$calendar_ot."' AND `".$row_sor."` <= '".$calendar_do."'";

        } elseif (!empty($_SESSION['calendar_do']) && !empty($_SESSION['calendar_ot'])){
            // ������ ���������� ���� ������ � ������ ��������
                $calendar_do = explode('-',$_SESSION['calendar_do']);
                //���������� 1 ����
                $y = $calendar_do[2];
                $m = $calendar_do[1];
                $d = $calendar_do[0]+1;
                $dt = mktime(0,0,0,$m,$d,$y);
                $calendar_do= date('Y-m-d',$dt);
                $calendar_ot = explode('-',$_SESSION['calendar_ot']);
                $calendar_ot = $calendar_ot[2].'-'.$calendar_ot[1].'-'.$calendar_ot[0];

                if($_SESSION['row_sort'] == '1'){
                    $selected_add = 'selected';
                    $selected_delivery = '';
                    $row_sor = 'insert';
                } else {
                    $selected_add = '';
                    $selected_delivery = 'selected';
                    $row_sor = 'timeDelivery';
                }

                $where = "`".$row_sor."` >= '".$calendar_ot."' AND `".$row_sor."` <= '".$calendar_do."'";
        }
        // ������ �� ���� ������
        if ((!empty($_GET['pay-type'])) || (!empty($_SESSION['pay-type']))  ){
            if (!empty($_GET['pay-type'])) {
                $_SESSION['pay-type'] = $_GET['pay-type'];
            }
            if ($_SESSION['pay-type']  != 'all') {
                if (empty($where)) {
                    $where = " `type` = '" . $_SESSION['pay-type']  . "'";
                } else {
                    $where .= " AND `type` = '" . $_SESSION['pay-type']  . "' ";
                }
            }
        }

        // ������ �� ������� ������� ��� ����������
        if (!empty($_GET['fixed-time-filter'])){
            // ������ ������ �����
            $fx_time = date("H:i:s", strtotime($_GET['fixed-time-filter']));
            $_SESSION['fixed-time-filter'] = $_GET['fixed-time-filter'];
            if (empty($where)) {
                $where = " `s_end_time` = '" . $_SESSION['fixed-time-filter']  . "'";
            } else {
                $where .= " AND `s_end_time` = '" . $_SESSION['fixed-time-filter']  . "' ";
            }
        }elseif ((!empty($_GET['time-range'])) || (!empty($_SESSION['time-range-code'])) ){
            // ���������� ����, �������� �� ������ �����
            if (intval($_GET['time-range']) == '-1'){
                unset($_SESSION['time-range-code']);
                unset($_SESSION['time-range-start']);
                unset($_SESSION['time-range-end']);
            }else {

                if (!empty($_GET['time-range'])) {
                    $_SESSION['time-range-code'] = $_GET['time-range'];
                }
                $time_request = dbone("title", '`id` = ' . quote($_SESSION['time-range-code']), 'delivery');  //db_query("SELECT `title` FROM `sw_delivery` WHERE `id` = '".quote($_GET['time-range'])."' LIMIT 0,1;");
                $_GET['time-range'] = $time_request;
                $_SESSION['time-range'] = $_GET['time-range'];

                $fx_times = explode(" - ", $_SESSION['time-range']);
                $s_start = date("H:i:s", strtotime($fx_times[0]));
                $s_end = date("H:i:s", strtotime($fx_times[1]));
                $_SESSION['time-range-start'] = $s_start;
                $_SESSION['time-range-end'] = $s_end;
                if (empty($where)) {
                    $where = " `s_start_time` >= '" . $_SESSION['time-range-start'] . "' AND `s_end_time` <= '" . $_SESSION['time-range-end'] . "' ";
                } else {
                    $where .= " AND `s_start_time` >= '" . $_SESSION['time-range-start'] . "' AND `s_end_time` <= '" . $_SESSION['time-range-end'] . "' ";
                }
            }
        }

        // ������ �� ���� ��������
        if (!empty($_GET['selected_shiping'])) {
            $_SESSION['selected_shiping'] = (int)$_GET['selected_shiping'];
        }

        $selected_shiping_1 = '';
        $selected_shiping_2 = '';
        $selected_shiping_3 = '';
        $selected_shiping_4 = '';
        $selected_shiping_5 = '';
        if($_SESSION['selected_shiping'] && $_SESSION['selected_shiping'] == '1'){
            $selected_shiping_1 = 'selected';
        } elseif($_SESSION['selected_shiping'] && $_SESSION['selected_shiping'] == '2'){
            $selected_shiping_2 = 'selected';
        } elseif($_SESSION['selected_shiping'] && $_SESSION['selected_shiping'] == '3'){
            $selected_shiping_3 = 'selected';
        }  elseif($_SESSION['selected_shiping'] && $_SESSION['selected_shiping'] == '4'){
            $selected_shiping_4 = 'selected';
        }   elseif($_SESSION['selected_shiping'] && $_SESSION['selected_shiping'] == '5'){
            $selected_shiping_5 = 'selected';
        }

        if ($_SESSION['selected_shiping'] && !empty($_SESSION['selected_shiping'])) {
            if (empty($where)) {
                $where = " delivery = " . (int)$_SESSION['selected_shiping'] ." ";
            } else {
                $where .= " AND delivery = " . (int)$_SESSION['selected_shiping'] ." ";
            }
        }

        $selectControl = '<select name="pay-type" id="pay-type"><option value="all">--��� �������--</option>';
        foreach(inc_vars::$types as $type=>$description):
            if ($_SESSION['pay-type'] == $type){
                $selectControl .= '<option value="'.$type.'" selected="selected">'.$description.'</option>';
            }else{
                $selectControl .= '<option value="'.$type.'">'.$description.'</option>';
            }

        endforeach;
        $selectControl .= '</select>';

        // ������ �������

        $timers_sql = "SELECT `id`, `start`, `end`, `title` FROM `sw_delivery` WHERE `active` = 1;";
        $time_request = db_query($timers_sql );
        $timeSpis = "<select name='time-range' class='timers-filter' id='timers-filter-id'>";
        $timeSpis .= '<option value="-1">-- ����� ����� --</option>';
        while ($row = mysql_fetch_assoc($time_request )) {
            $timeSpis .= '<option value="'.$row['id'].'" '.(($row['id'] == $_SESSION['time-range-code'] ) ? "selected='selected'" : "").'>'.$row['title'] .'</option>';
        }
        $timeSpis .= '</select>';

        $timeScript = "<script>
                            jQuery(document).ready(function(){
                             //      jQuery(\"#timers-filter-id\").change(function(){
                                                         
                               //                              jQuery(\"#fixed-time\").val(\"00:00\");

                                 //              });

	jQuery('#pay-type').click(function(){
		module.setState('edit');
		
	});

	jQuery('#tip_sortirovki').click(function(){
        module.setState('edit');
	});

	jQuery('#timers-filter-id').click(function(){
        module.setState('edit');
	});

                            });
    
                        </script>";


        $html.=  $timeScript.'<form action="/go/m='.$module.'&op=load-module">
                <select id="selected_shiping" name="selected_shiping">
                  <option>�������� ������</option>
                  <option '.$selected_shiping_1.' value="1">� �������� ����</option>
                  <option '.$selected_shiping_2.' value="2">���������</option>
                  <option '.$selected_shiping_3.' value="3">�� ���� (�� 8 ��)</option>
                  <option '.$selected_shiping_4.' value="4">�� ���� (�� 8 ��)</option>
                  <option '.$selected_shiping_5.' value="5">�� ������� �����</option>
                </select> 
                <select id="tip_sortirovki" name="tip_sortirovki">
                  <option '.$selected_add.' value="1">�� ���� ����������</option>
                  <option '.$selected_delivery.' value="2">�� ���� ��������</option>
                </select>
                ���� ������: <span>c </span>
                <input id="calendar_ot" value="'.$_SESSION['calendar_ot'].'" type="text" onfocus="this.select();lcs(this);module.setState(\'edit\');" onclick="event.cancelBubble=true;this.select();lcs(this);module.setState(\'edit\');">
                <img src="/sf123sf123sf/i/date.gif" />
                <span>�� </span>
                <input id="calendar_do" value="'.$_SESSION['calendar_do'].'" type="text" onfocus="this.select();lcs(this);module.setState(\'edit\');" onclick="event.cancelBubble=true;this.select();lcs(this);module.setState(\'edit\');">
                <img src="/sf123sf123sf/i/date.gif" />
                <span id="label_pay-type">������ ������:</span> '.$selectControl .'
                <div id="a_button_filter">
                    <a href="javascript:module.filter_data()" id="a_select_fiter"><input type="button" id="showButton" value="��������"></a>
                    <a href="javascript:module.filter_clear()" id="a_clear_filter"><input type="button" value="�������� ������"></a><br/>
                </div>
                <div class="timesort">������ �� �������: '.$timeSpis . ' </div>
            </form>
        ';
    }

    if ($module != 'notify')
    {

	    if (!empty($_GLOBALS['search']) && $ret === false)
	    {
		    $html           .= '<div class="search">';
		    $html           .= '<form action="/go/m=' . $module . '&op=load-module" onsubmit="getForm(this); module.setState(\'edit\'); return false;">';
		    $html           .= '<input type="text" id="query-input" class="query" name="query" onkeyup="module.setState(\'edit\');" value="' . htmlspecialchars($search) . '">';
		    $search_message = '��� ������ �� id �������: id2';
		    if ($module == 'orders')
		    {
			    $search_message .= ', ��� ������ �� ������� "��������" ������� ��� (����� �����) ���������';
		    }
		    $html .= '<p class="info">' . $search_message . '</p>';
		    if (!empty($_GET['query']))
		    {
			    $html .= '<p class="info">�������� ���������� ������ �� <b>' . $search . '</b></p>';
		    }
		    $html .= '</form>';
		    $html .= '</div>';
		    jQuery::evalScript('module.search()');
	    }

    }else{
	    $html           .= '<div class="notify_info">
							
							<div class="data_butn"><p class="right buttons"><input type="button" class="b-textfield" value="�������� ������� ���������" title="������� ��������� ����������� �������������, �� �� ������ �������� �� �������" onclick="usrnotify.prepare();"></p></div>
							<br/></div>
							<h1 style="float: left; margin-right: 13px;"><a href="javascript:module.index()">������� ���������</a></h1>';
    }
	if ($sid != 0) {
		$pach = array();
		$s = $sid;
		$back = 0;
		while ($s != 0) {
			$r = db('`id`, `sid`, `title`', '`id` = '.$s, $module);
			$p = mysql_fetch_array($r);
			$s = $p['sid'];
			if ($back == 0) $back = $p['sid'];
			$sid != $p['id'] ? $pach[] = '<a onclick="initGET(\'/go/m='.$module.'&op=load-module&sid='.$p['id'].'\')">'.$p['title'].'</a>' : $pach[] = $p['title'];
		}
		$pach[] = '<a onclick="initGET(\'/go/m='.$module.'&op=load-module\')">'.$m['title'].'</a>';
		krsort($pach);
		$html.= '<div class="pach">'.implode('<span>></span>', $pach).'</div>';
	}
	$preTable = $html;
	$html = '<table id="list-items" class="items"><tr class="title">';

	foreach ($_GLOBALS['field']['table'] as $name=>$value) {
        if ($name!='soc_nw'){
            !isset($fields) ? $fields = 'i.`'.$name.'`' : $fields.= ', i.`'.$name.'`';
        }
        switch ($value){
            case 'nop': break;
            case 'user-disc':
                $html.= '<th class="'.$name.'" colspan="2">'.$_GLOBALS['field']['title'][$name].'</th>';
                break;
            default:
                $html.= '<th class="'.$name.'">'.$_GLOBALS['field']['title'][$name].'</th>';
        }
	}

	$tr = count($_GLOBALS['field']['table']);
	$html.= '<th class="options">��������</th></tr>';

	if ($sid != 0) {
        $html.= '<tr><td class="id"></td><td colspan="'.$tr.'" class="title dir" onclick="initGET(\'/go/m='.$module.'&op=load-module&sid='.$back.'\')">..</td></tr>';
    }


	$r = query_new('SELECT COUNT(i.`id`) as `c` FROM `sw_'. $module.'` as i WHERE '.$where,1);
    if ($module == 'sendquestion'){
        $r = query_new('SELECT COUNT(i.`id`) as `c` FROM `sw_questions` as i WHERE '.$where,1);
    }
    if ($module == 'logevents'){
        $r = query_new('SELECT COUNT(i.`id`) as `c` FROM `sw_adm_events` as i WHERE '.$where,1);
    }
    // ������� �������� ��� ������ ���
    if ($module == 'vases'){
        $r = query_new('SELECT COUNT(i.`id`) as `c` FROM `sw_vases` as i WHERE '.$where,1);
    }
    // ������ �����������
	if ($module == 'notify'){
		$r = query_new('SELECT COUNT(i.`id`) as `c` FROM `sw_notify` as i WHERE '.$where ,1);
		// ��� ���� ��������� ��� � ���-��
		$r2 = query_new('SELECT COUNT(i.`id`) as `c` FROM `sw_notify_sms` as i',1);
		//
		if (!empty($r2['c']))
		{
			$notifySMSCnt = $r2['c'];
			// ��� � ���
			if ((int)$r2['c'] > (int)$r['c'] )
			{
				$r['c'] =  $r2['c'];
			}


		}else{
			$notifySMSCnt = 0;
		}

	}
	if ($module == 'abandoned'){
		$r = query_new('SELECT COUNT(i.`id`) as `c` FROM `sw_abandoned_carts` as i WHERE '.$where,1);
	}



	$c = empty($r['c'])?0:$r['c'];
	$pgsCount = ceil($c/$on_page);
	$index = 1;

    if($row_sor == 'timeDelivery'){
        $row_sor = 'title';
    }
	$cal_do = !empty($_GET['calendar_do']) ? $_GET['calendar_do'] : $_SESSION['calendar_do'];
    $cal_ot = !empty($_GET['calendar_ot']) ? $_GET['calendar_ot'] : $_SESSION['calendar_ot'];
    if (!empty($cal_do) && !empty($cal_ot) && !empty($row_sor)){
        $order_by = 'TRIM(REPLACE(TRIM(REPLACE(i.`'.$row_sor.'`,\'���������:\', \'\')),\'���������\', \'\'))  DESC';
    }elseif(!empty($_GLOBALS['mod_order_by'])){
    	$order_by = $_GLOBALS['mod_order_by'];
    }else{
        $order_by = !empty($_GLOBALS['field']['table']['pos']) ? 'i.`pos` ASC' : '`dir` DESC, i.`id` DESC';
    }
    if (!empty($_GLOBALS['mod_filds'])){
    	$fields .= ', '.$_GLOBALS['mod_filds'];
    }

	$r = db_query('SELECT '.$fields.', i.`active`, i.`system`'.(!empty($_GLOBALS['nesting']) ? ', (SELECT COUNT(`id`) FROM `sw_'.$module.'` WHERE `sid` = i.`id`) as `dir`' : ', "0" as `dir`')
		. ' FROM `sw_'.$module.'` as i WHERE '.$where.' ORDER BY '.$order_by.' LIMIT '.$min.', '.$on_page);

	if ($module == 'orders'){
		$r = db_query('SELECT '.$fields.', i.`active`, i.`courier`, i.`system`'.(!empty($_GLOBALS['nesting']) ? ', (SELECT COUNT(`id`) FROM `sw_'.$module.'` WHERE `sid` = i.`id`) as `dir`' : ', "0" as `dir`')
			. ' FROM `sw_'.$module.'` as i WHERE '.$where.' ORDER BY '.$order_by.' LIMIT '.$min.', '.$on_page);
	}


    if ($module == 'sendquestion'){
        $r = db_query('SELECT '.$fields.' FROM `sw_questions` as i WHERE '.$where.' ORDER BY i.id DESC LIMIT '.$min.', '.$on_page);
    }

    //��� ������� ��� ���
    if ($module == 'vases'){
        $r = db_query('SELECT '.$fields.' FROM `sw_vases` as i WHERE '.$where.' ORDER BY i.id DESC LIMIT '.$min.', '.$on_page);
    }
	// ������ �����������
	if ($module == 'notify'){
		$r = db_query('SELECT '.$fields.' FROM `sw_notify` as i WHERE '.$where .' ORDER BY i.id DESC LIMIT '.$min.', '.$on_page);
		// ��������� ��� � ���
		$fields_notify_sms = 'CONCAT("SMS", i.`id`) as `id`, i.`name`, i.`phone` as `email`, i.`order`, i.`order_dt`, i.`send_dt`, i.`status` as `send_count`, i.`opened_count`';
		$r2 = db_query('SELECT '.$fields_notify_sms .' FROM `sw_notify_sms` as i ORDER BY i.id DESC LIMIT '.$min.', '.$on_page);
	}

	if ($module == 'abandoned'){

		$r = db_query('SELECT i.`id`, i.`name`, i.`email`, i.`phone`, i.`dt`, i.`status`, i.`type`, i.`datas`, i.`hash` FROM `sw_abandoned_carts` as i WHERE '.$where .' ORDER BY i.id DESC LIMIT '.$min.', '.$on_page);

	}


    if ($module == 'logevents'){
        if (!empty($search)){
            $where = "i.oid LIKE '%".trim($search)."%'";
        }
        $r = db_query('SELECT i.`id`, (SELECT `descr` FROM `sw_event_codes` WHERE `sw_event_codes`.`id`= i.`code`) as `code`, IF((SELECT username FROM sw_users WHERE `sw_users`.`id` = i.`uid`) <> \'\', (SELECT username FROM sw_users WHERE `sw_users`.`id` = i.`uid`) , IF ((SELECT title FROM sw_admin WHERE sw_admin.`id` = i.`uid`) <> \'\', (SELECT title FROM sw_admin WHERE sw_admin.`id` = i.`uid`),  \'�� ���������������\' )  ) AS `uid`, i.`oid`, IF(i.`status` = 0, \'�� �������\', \'�������\' ) as `status`, i.`place`, i.`itogo`, i.`active`, i.`system`, i.`date` , IF( i.`type` <> "", i.`type` , "none") as `type`, "0" as `dir` FROM `sw_adm_events` as i WHERE '.$where.' ORDER BY i.id DESC LIMIT '.$min.', '.$on_page);
    }
	if ($module == 'notify'){
		$notifySMSCnt = mysql_num_rows($r2);
		$cnr = mysql_num_rows($r) + $notifySMSCnt ;

	}else{
		$cnr = mysql_num_rows($r);
	}


    $table_rows = array();
notify_sms:
	while ($i = mysql_fetch_array($r, MYSQL_ASSOC))
    {
    	if ($module == 'abandoned')
	    {
		    $td = '<tr id="i'.$i['hash'].'">';
	    }
	    else
	    {
		    $td = '<tr id="i'.$i['id'].'">';
	    }


		foreach ($_GLOBALS['field']['table'] as $name=>$value)
        {
			switch ($value) {
                case 'nop': break;
				case 'title': $td.= '<td class="title'.($i['dir'] != 0 ? ' dir" onclick="initGET(\'/go/m='.$module.'&op=load-module&sid='.$i['id'].'\')"' : '" onclick="initGET(\'/go/m='.$module.'&op=item&id='.$i['id'].'&pg='.$pg.'&query='.htmlspecialchars(urlencode($search)).'\'); module.setState(\'edit\');"').'>'.$i[$name].'</td>'; break;
				case 'viewOrder': {
					if ($module == 'fast_baying')
                    {
						$fb_prods = unserialize(base64_decode($i[$name]));
						$fb_itm = $fb_prods;
						$itm = array_pop($fb_itm);
						$vo_text = (count($fb_prods)>1)? $itm['title'].', ...':$itm['title'];
					}
                    else
                    {
                        $vo_text = $i[$name];
                    }
					$td.= '<td class="title" style="min-width:100px;"><a href="javascript:order.view('.$i['id'].'); void(0);">'.$vo_text.'</a></td>';
				} break;
				case 'checkbox': $td.= '<td class="'.$name.'"><span onclick="item.checked('.$i['id'].',this)" class="jLink'.(!empty($i[$name]) ? ' off">���������' : '">��������').'</span></td>'; break;
                case 'yes/no': $td.= '<td class="'.$name.'"><span onclick="item.checked('.$i['id'].',this)" class="jLink'.(!empty($i[$name]) ? ' off">��' : '">���').'</span></td>'; break;
				case 'pos': $td.= '<td class="'.$name.'">'.(($pg == 1 && $index==1) ? '<img src="/sf123sf123sf/i/top-block.gif">': '<a href="javascript:void(0)" onclick="initGET(\'/go/m='.$module.'&op=pos&id='.$i['id'].'&top=1&sid='.$sid.'&pg='.($index==1?$pg-1:$pg).'\'); module.setState(\'edit\');"><img src="/sf123sf123sf/i/top.gif"></a>').(($pg == $pgsCount && $index == $cnr) ? '<img src="/sf123sf123sf/i/bottom-block.gif">' : '<a href="javascript:void(0)" onclick="initGET(\'/go/m='.$module.'&op=pos&id='.$i['id'].'&top=0&sid='.$sid.'&pg='.($index<$on_page?$pg:$pg+1).'\'); module.setState(\'edit\');"><img src="/sf123sf123sf/i/bottom.gif"></a>').'</td>'; break;
                case 'payment-type': { $td.= '<td class="'.$name.'">'.inc_vars::$types[$i[$name]].'</td>'; } break;
				case 'link': $td.= '<td class="'.$name.'"><a href="'.$i[$name].'" target="_blank">'.$i[$name].'</a></td>'; break;
				case 'datetime':
                    if (($module == 'logevents') || ($module == 'sendquestion') || ($module == 'notify') || ($module == 'abandoned') ){
	                    $td.= '<td class="'.$name.'">'.date('d.m.Y � H:i', strtotime($i[$name])).'</td>';
                    }
                    else{
                        $td.= '<td class="'.$name.'">'.date('d.m.Y � H:i', $i[$name]).'</td>';
                    }
                    break;
				case 'ctoc': $td.= '<td class="'.$name.'">'.($i[$name]==1?'���������� ��������� ����������':'�������� ����������').'</td>'; break;
                case 'uid_link': $td.= '<td width="250px" class="'.$name.'">'.(intval($i['uid'])>0
                    ? '<a href="javascript: initGET(\'/go/m=users&op=item&id='.intval($i['uid']).'\'); module.setState(\'edit\'); void(0);">'.$i[$name].'</a>'
                    : '<span>'.$i[$name].'</span>').'</td>';
                    break;
                case 'soc_nw':
                    $soc_nw = get_user_soc_nw($i['id'], inc_vars::$soc_nw_cols);
                    $td .= '<td class="'.$name.'">';
                    if ($soc_nw!==false){
                        foreach ($soc_nw as $s_val){
                            $td .= '<div class="soc_'.$s_val['type'].'" title="'.$s_val['soc_id'].'"></div>';
                        }
                    }
                    $td .= '</td>';
                    break;
                case 'inst_smalls':
                    $struc = unserialize($i[$name]);
					$iImg = '<a href="'.$struc->link.'" target="blank">'.(empty($struc->images) ? $struc->link
						: '<img src="'.$struc->images->thumbnail->url.'" width="'.$struc->images->thumbnail->width.'" height="'.$struc->images->thumbnail->height.'">').'</a>';
                    $td.= '<td class="'.$name.'" width="150px">'.$iImg.'</td>';
                    break;
                case 'cid_name':
					if (!empty($i[$name])){
						$cid_name = urlItem($i[$name]);
					}
                    $cid_name = empty($i[$name])?'�� ��������o':'<a href="'.$cid_name['url'].'" target="blank">'.$cid_name['title'].'</a>';
                    $td.= '<td class="'.$name.'">'.$cid_name.'</td>';
                    break;
                case 'inst_tags':
                    $td.= '<td class="'.$name.'">'.str_replace('#', ' ', $i[$name]).'</td>';
                    break;
                case 'radio':
                	$td.= '<td class="'.$name.'" ><input type="radio" name="'.$name.'" onclick="initGET(\'/go/m='.$module.'&op=radio&fild='.$name.'&id='.$i['id'].'&sid='.$sid.'&pg='.($pg).'\'); module.setState(\'edit\');" value="'.$i['id'].'" '.(!empty($i[$name])?'checked="checked"':'').'/></td>';
                	break;

                case 'status_verified':
                    $_divClass = (intval($i[$name]) == 0) ? 'status_verified active active-off' : 'status_verified active';
                    $_divTitle = (intval($i[$name]) == 0) ? '�� �����������' : '�����������';
                    $td.= '<td class="btn"><div class="'.$_divClass.'" title="'.
                        $_divTitle.'" onClick="initGET(\'/go/m='.$module.'&op=activateStatusVerified&id='.$i['id'].'\')"></div></td>';
                    break;

                case 'status_delivered':
                    $_divClass = (intval($i[$name]) == 0) ? 'status_delivered active active-off' : 'status_delivered active';
                    $_divTitle = (intval($i[$name]) == 0) ? '�� ���������' : '���������';
                    $td.= '<td class="btn"><div class="'.$_divClass.'" title="'.
                        $_divTitle.'" onClick="confirmDeliveryStatusChange(\''.$i['id'].'\')"></div></td>';
                    break;
				case 'status_sended':
					$_divClass = (intval($i[$name]) == 0) ? 'status_sended active active-off' : 'status_sended active';
					$_courierName = !empty($i['courier']) ? "������: " .$i['courier'] : "������ �� ������";
					$_divTitle = (intval($i[$name]) == 0) ? '�� ���������' : '���������. ' . $_courierName ;
					$td.= '<td class="btn"><div class="'.$_divClass.'" id="send_ch_'.$i['id'].'" title="'.
						$_divTitle.'" onClick="confirmSendStatusChange(\''.$i['id'].'\', \''.($i['courier']).'\', '.$i['status_sended'].')"></div></td>';
					break;

				case 'status_ready' :
                    $_divClass = (intval($i[$name]) == 0) ? 'status_ready active active-off' : 'status_ready active';
                    $_divTitle = (intval($i[$name]) == 0) ? '�� ������' : '������';
                    $td.= '<td class="btn"><div class="'.$_divClass.'" title="'.
                        $_divTitle.'" onClick="confirmReadyStatusChange(\''.$i['id'].'\')"></div></td>';
                    break;

                case 'status_printed' :
                    $_divClass = (intval($i[$name]) == 0) ? 'status_printed active active-off' : 'status_printed active';
                    $_divTitle = (intval($i[$name]) == 0) ? '�� �����������' : '��� �����������';
                    $td.= '<td class="btn"><div class="'.$_divClass.'" title="'.
                        $_divTitle.'" onClick="confirmPrintStatusChange(\''.$i['id'].'\')"></div></td>';
                    break;

                case 'status':
                    // ���� ������ ������� ��� ������ ��������, ��� ������, ��� ����� ���������
                    //if (in_array(trim($i['type']), inc_vars::$offline_payments))
                    //{
                        $_divClass = (intval($i[$name]) == 0) ? 'status active active-off' : 'status active';
                        $_divTitle = (intval($i[$name]) == 0) ? '�� �������' : '�������';
                        $color = (intval($i[$name]) == 0) ? 'red' : 'green';
                        if ((intval($i[$name]) == 0 )&& (in_array(trim($i['type']), SmsQueue::$nodialog_payments))){
                            // �� �������, � ��� ������� ��� ���
                            $td .= '<td class="btn '.$name.'">'.'<b class="cashPayStatus" style="color:'.$color.';vertical-align:middle;">'.$_divTitle.'</b>'.
                                '<div id="chordsts'.$i['id'].'" rel="'.$i['type'].'" class="'.$_divClass.'" style="vertical-align:middle" title="'.$_divTitle.
                                '" onClick="confirmPaymentStatusChangeNoSMS('.$i['id'].')"></div></td>';
                        }elseif(intval($i[$name]) == 0 && (!in_array(trim($i['type']), SmsQueue::$nodialog_payments))){
                            // �� ������� � ��� ���
                            $td .= '<td class="btn '.$name.'">'.'<b class="cashPayStatus" style="color:'.$color.';vertical-align:middle;">'.$_divTitle.'</b>'.
                                '<div id="chordsts'.$i['id'].'" rel="'.$i['type'].'" class="'.$_divClass.'" style="vertical-align:middle" title="'.$_divTitle.
                                '" onClick="confirmPaymentStatusChange('.$i['id'].')"></div></td>';
                        }elseif (intval($i[$name]) != 0){
                            $td .= '<td class="btn '.$name.'">'.'<b class="cashPayStatus" style="color:'.$color.';vertical-align:middle;">'.$_divTitle.'</b>'.
                                '<div id="chordsts'.$i['id'].'" rel="'.$i['type'].'" class="'.$_divClass.'" style="vertical-align:middle" title="'.$_divTitle.
                                '" onClick="confirmPaymentStatusChangeNotPayNoSMS('.$i['id'].')"></div></td>';
                        }

                        // ���� '" onClick="initGET(\'/go/m='.$module.'&op=changeOrderCashPaymentStatus&id='.$i['id'].'\')">'.


                        // � ���� ���� �����
                        // $td.= '<td class="'.$name.'">';
                        // $td.= '<b style="color:'.(!empty($i[$name]) ? 'green">�������' : 'red">�� �������').'</b></td>';
                    /*
                    }
                    else // ���� ������ �� ���, ����� ��� ��� ������
                    {
                        $td.= '<td class="'.$name.'">';
                        $td.= '<b style="color:'.(!empty($i[$name]) ? 'green">�������' : 'red">�� �������').'</b></td>';
                    }
                    */
                    break;

                // ��� ������ "������������"
                case 'user-disc':
                    $td .= '<td class="'.$name.'" style="white-space: pre; width:';
                    $u_disc = get_user_disc($i['id']); // ������� ������ � ���������������� �������
                    $td .= ($u_disc!==false
                            ? '50px">'.($u_disc['type'] == 'coup'? '�����': '�����').' '.$u_disc['val'].'%</td><td style="width:100px">'.$u_disc['title']
                            :'150px;" colspan="2">����������� '.$i[$name].'%').'</td>';
                    break;


				default:
                    if ($module == 'logevents'){
                        if ($name == 'oid'){
                            $td.= '<td class="'.$name.'"><a href="javascript:javascript:order.view('.$i[$name].'); void(0);" title="����������� �����">'.$i[$name].'</a></td>';
                        }else{
                            $td.= '<td class="'.$name.'">'.$i[$name].'</td>';
                        }

                    }elseif ($module == 'sendquestion') {
                        if ($name == 'status'){
                            $td.= '<td class="'.$name.'">'. (($i[$name] == 0) ? "<span style='color: red;'>�� �������</span>" : "<span style='color: grey;'>��� �������</span>") .'</td>';
                        }else{
                            $td.= '<td class="'.$name.'">'.$i[$name].'</td>';
                        }

                    }elseif ($module == 'notify'){
                    	switch ($name )
	                    {
		                    default:
			                    $td.= '<td class="'.$name.'">'.$i[$name].'</td>';
		                    	break;
		                    case 'order':
			                    $td.= '<td class="'.$name.'"><a href="javascript:order.view('.$i[$name] .'); void(0);" title="����������� �����">'.$i[$name] .'</a></td>';
		                    	break;
		                    case 'opened_count':
		                    	if ($i[$name] > 0) {
				                    $td.= '<td class="'.$name.'">���������</td>';
			                    }else{
				                    $td.= '<td class="'.$name.'">�� ���������</td>';
			                    }

		                    	break;
		                    case 'send_count':
			                    if ($i[$name] > 2) {
				                    $td.= '<td class="'.$name.'" style="color: red;">������</td>';
			                    }else{
				                    $td.= '<td class="'.$name.'">'.$i[$name].'</td>';
			                    }
		                    	break;
		                    case 'send_dt':

		                    	if ($i[$name] != '0000-00-00 00:00:00')
		                    	{
				                    $now = time(); // or your date as well
				                    $lastSend = strtotime($i[$name] );
				                    $lastSend_str = date('d.m.Y � H:i', strtotime($i[$name])) ;
				                    $datediff = $now - $lastSend ;
				                    $days = floor($datediff / (60 * 60 * 24));

				                    $days_str = tpl_tpluralForm($days , '����', '���', '����');
				                    if ($days < 180){
				                    	$spaColor = "red";
				                    }else{
					                    $spaColor = "green";
				                    }
				                    switch  ($i['send_count'] ){
					                    case '1':
						                    $prefix = '(�� 7 ���� �� ������)';
					                    	break;
					                    case '2':
						                    $prefix = '(�� 2 ��� �� ������)';
					                    	break;
					                    default:
					                    	$prefix = '';
					                    	break;
				                    }
				                    /*
				                    if ($days  == 0){
					                    $td.= '<td class="'.$name.'"> <span style="color: '.$spaColor.'">�������</span> '.$prefix .'</td>';
				                    }elseif($days  == 1){
					                    $td.= '<td class="'.$name.'"> <span style="color: '.$spaColor.'">�����</span> '.$prefix .'</td>';
				                    }else{
					                    $td.= '<td class="'.$name.'"> <span style="color: '.$spaColor.'">'. $days . ' ' . $days_str .'</span> ����� '.$prefix .'</td>';
				                    }
				                    */
				                    $td.= '<td class="'.$name.'"> <span style="color: '.$spaColor.'">'.$lastSend_str.'</span>&nbsp;'.$prefix .'</td>';


			                    }else{
		                    		if(($i['send_count'] > 2) )
		                    		{
					                    $td.= '<td class="'.$name.'" style="color:red;">������ ��������</td>';
				                    }
				                    else
				                    {
					                    $td.= '<td class="'.$name.'">������� (������ ������������)</td>';
					                }

			                    }
		                    	break;
	                    }

                    }elseif($module == 'abandoned'){
	                    switch ($name )
	                    {
		                    default:
			                    $td .= '<td class="' . $name . '">' . $i[$name] . '</td>';
			                    break;
		                    case 'status':
		                        $resultStatus = '�� ����������';
		                        if ($i['status'] == '0') {
                                    $resultStatus = '���������� �� email';
                                } elseif ($i['status'] == '1') {
                                    $resultStatus = '���������� ����� sms';
                                } elseif ($i['status'] == '2') {
                                    $resultStatus = '���������� �� email / ���������� ����� sms';
                                }
			                    $td .= '<td class="' . $name . '">' . $resultStatus . '</td>';
		                    	break;
		                    case 'type':
                                $resultMessage = '';
		                        if (!empty($i['email']) && !empty($i['phone'])) {
		                            $resultMessage = '������ � SMS';
                                } elseif(!empty($i['email'])) {
                                    $resultMessage = '������';
                                } elseif(!empty($i['phone'])) {
                                    $resultMessage = 'SMS';
                                }
			                    $td .= '<td class="' . $name . '">' . $resultMessage . '</td>';
			                    break;
		                    case 'cnt':
		                    	$datas = unserialize($i['datas'] );
			                    $td .= '<td class="' . $name . '">' . $datas['count']. '</td>';
		                    	break;
		                    case 'summ':
			                    $datas = unserialize($i['datas'] );
			                    $td .= '<td class="' . $name . '">' . $datas['discount_summ'] . ' ���.</td>';
			                    break;
		                    case 'email':
                                $s_contact = !empty($i['email']) ? $i['email'] : '';
                                if (!empty($i['phone']) && !empty($i['email'])) {
                                    $s_contact .= '<br>';
                                }
                                $s_contact .= !empty($i['phone']) ? $i['phone'] : '';

			                    $td .= '<td class="' . $name . '">'.$s_contact.'</td>';
		                    	break;
	                    }
                    }
                    else{
                        $td.= '<td class="'.$name.'">'.$i[$name].'</td>';
                    }

                    break;
			}
		}
		$index++;
        // ���������� ��������
        if ($module == 'logevents'){
            $td.= '<td class="btn"><div class="edit" title="�������� ������ ���������� � �������" onclick="initGET(\'/go/m='.$module.'&op=viewEvent&id='.$i['id'].'&pg='.$pg.'&query='.htmlspecialchars(urlencode($search), ENT_COMPAT | ENT_HTML401, 'cp1251').'\')"></div></td></tr>';
        }elseif ($module == 'sendquestion') {
            if ($i['status'] == 0) {
                $td .= '<td class="btn"><div class="edit" title="�������� �� ������" onclick="initGET(\'/go/m=' . $module . '&op=viewQuestion&id=' . $i['id'] . '&pg=' . $pg . '&query=' . htmlspecialchars(urlencode($search), ENT_COMPAT | ENT_HTML401, 'cp1251') . '\')"></div><div class="active-off" title="������� ������" onclick="initGET(\'/go/m=' . $module . '&op=deleteQuestion&id=' . $i['id'] . '&pg=' . $pg . '&query=' . htmlspecialchars(urlencode($search), ENT_COMPAT | ENT_HTML401, 'cp1251') . '\')"></div></td></tr>';
            }else{
                $td .= '<td class="btn"><div class="active-off" title="������� ������" onclick="initGET(\'/go/m=' . $module . '&op=deleteQuestion&id=' . $i['id'] . '&pg=' . $pg . '&query=' . htmlspecialchars(urlencode($search), ENT_COMPAT | ENT_HTML401, 'cp1251') . '\')"></div></td></tr>';
            }

        }elseif($module == 'vases'){
            $td.= '<td class="btn"><div class="active'.($i['status'] != 0 ? '" title="�������"' : ' active-off" title="��������"').' onclick="initGET(\'/go/m='.$module.'&op=active&id='.$i['id'].'\')"></div><div class="edit" title="�������������" onclick="initGET(\'/go/m='.$module.'&op=item&id='.$i['id'].'&pg='.$pg.'&query='.htmlspecialchars(urlencode($search)).'\')"></div><div title="�������" class="delete'.($i['system'] != 0 ? '-off"' : '" onclick="items.remove('.$i['id'].')"').'></div></td></tr>';
        }elseif($module == 'notify')
        {
	        $td.= '<td class="btn"><div title="������� ������" class="delete" onclick="items.remove('.$i['id'].')"></div></td></tr>';
        }elseif($module == 'abandoned'){
            $buttonSms = '<div title="����������� ���" class="edit" onclick="window.open(\'/system/mailpages/readmail.php?type=sms&opt='.$i['hash'] .'\', \'_blank\', \'menubar=no,location=no,resizable=no,scrollbars=no,status=no\');"></div>';
            $buttonEmail = '<div title="����������� �����������" class="edit" onclick="window.open(\'/system/mailpages/readmail.php?type=email&opt='.$i['hash'] .'\', \'_blank\', \'menubar=no,location=no,resizable=no,scrollbars=no,status=no\');"></div>';

			$td.='<td class="btn">'.$buttonSms.$buttonEmail.'<div title="������� �����������" class="delete" onclick="items.remove(\''.$i['hash'].'\')"></div></td></tr>';
        }else{
            $td.= '<td class="btn"><div class="active'.($i['active'] != 0 ? '" title="�������"' : ' active-off" title="��������"').' onclick="initGET(\'/go/m='.$module.'&op=active&id='.$i['id'].'\')"></div><div class="edit" title="�������������" onclick="initGET(\'/go/m='.$module.'&op=item&id='.$i['id'].'&pg='.$pg.'&query='.htmlspecialchars(urlencode($search)).'\'); module.setState(\'edit\');"></div><div title="�������" class="delete'.($i['system'] != 0 ? '-off"' : '" onclick="items.remove('.$i['id'].')"').'></div></td></tr>';
        }

        $table_rows[] = $td;
	}
	if ($module == 'notify')
	{
		if (!empty($r2))
		{
			$r = $r2;
			$r2 = null;
				if ($notifySMSCnt > 0)
				{
					$table_rows[] = '<tr class="title"><th colspan="9">SMS</th></tr>';
				}



			// � ��������� ������� � �������� ���� ����, ����� � ����� �����������
			// ����� ������������. ������������ ���� ���� ���� � �� ����.
			goto notify_sms;

		}
	}

    $paging = ($c > $on_page) ? paging($op, $c, $pg, $on_page, '/go/m='.$module.'&sid='.$sid.'&op=load-module&query='.htmlspecialchars(urlencode(inUTF8($search)), ENT_COMPAT | ENT_HTML401, 'cp1251')):'';
	$html = $preTable.(!empty($_GLOBALS['top_paging'])?$paging:'').$html.implode('', $table_rows).'</table>'.$paging;

    if (!empty($ret)){
        return array('rows'=>$table_rows,'paging'=>$paging,'html'=>$html);
    }

    if ($module == 'analytics'){
       $html = moduleAnalyticsGetAnalytics('startpage', $html);
    }



	jQuery('#content-data')->html(inUTF8($html));
    jQuery::evalScript('items.odd("table#list-items.items");');
	jQuery::evalScript('module.set('.$pg.',"'.htmlspecialchars(urlencode(inUTF8($search))).'")');
	if (!empty($_GLOBALS['tiny'])) jQuery::evalScript('htmlEditor.init()');
	if (!empty($_GLOBALS['date'])) jQuery::evalScript('date.init()');
	jQuery::getResponse();
}

// �������������� ������
function initItem()
{
	global $_GLOBALS, $module, $basepath;

	$pg = !empty($_GET['pg']) ? abs(intval($_GET['pg'])) : 1;
	$search = !empty($_GET['query']) ? filter(trim(in1251($_GET['query'])), 'nohtml') : null;

	if (!empty($_POST)) // ������, ����� ���� ���������� ������
    {
		$id = !empty($_GET['id']) ? abs(intval($_GET['id'])) : 0;
        if ($module == 'orders' && $id > 0){
            $raw = db_query('SELECT * FROM `sw_orders` WHERE `id` = '.quote($id) . ' LIMIT 0,1') ;
            $orderData = mysql_fetch_array($raw , MYSQL_ASSOC);
            logevent(16, $_SESSION['admin'][0], 2, array('order_id' => $id, 'itog' => $orderData['itogo'], 'comment' =>$orderData['recipientSMS'], 'type' => $orderData['type'], 'is_payed' =>$orderData['status']) ); // ��������
        }

        $value = array();
		$fields = $id != 0 ? $_GLOBALS['edit-field'] : $_GLOBALS['add-field'];

		if (!empty($_GLOBALS['field']['type']['link']))
        {
			if (empty($_POST['link']))
            {
				jQuery::evalScript(inUTF8("alert('���� \"".$_GLOBALS['field']['title']['link']."\" ����� ���������!')"));
				jQuery::getResponse();
			}
			$link = filter(trim($_POST['link']), 'nohtml');
		}

		foreach ($fields as $field) {
			if (isset($_POST[$field])) {
				switch ($_GLOBALS['field']['type'][$field]) {
					case 'insert': $value[$field] = time(); break;
					case 'textarea': $value[$field] = trim(in1251($_POST[$field])); break;
					case 'html': $value[$field] = trim(in1251($_POST[$field])); break;
                    case 'position': $value[$field] = intval($_POST[$field]); break;
                    case 'select': {
                        $value[$field] = filter(trim(in1251($_POST[$field])), 'nohtml');
                        if ($module=='modules') { $value[$field] = (file_exists($basepath.'../i/'.$_POST[$field]))? $_POST[$field] : 'plugin.png';}
                    } break;
                    case 'select-multiple': $value[$field] = is_array($_POST[$field])?implode(':',$_POST[$field]):''; break;
					case 'date': {
						$value[$field] = trim(in1251($_POST[$field]));
						if (!empty($value[$field])) {
							if (preg_match("/^([0-9]{2})\.([0-9]{2})\.([0-9]{4})$/i", $value[$field], $date)) {
								$value[$field] = $date[3].'-'.$date[2].'-'.$date[1];
							} else $value[$field] = null;
						}
					} break;
					case 'date2': {
						$value[$field] = trim(in1251($_POST[$field]));
						if (!empty($value[$field])) {
							if (preg_match("/^([0-9]{2})\.([0-9]{2})\.([0-9]{4})$/i", $value[$field], $date)) {
								$value[$field] = $date[1].'.'.$date[2].'.'.$date[3];
							} else $value[$field] = null;
						}
					} break;
					case 'd-time': {
						$value[$field] = '�� �������';
						if (!empty($_POST['start']) && !empty($_POST['end'])) {
							if (preg_match("/^([0-9]{2})\:([0-9]{2})$/i", $_POST['start'], $start) && preg_match("/^([0-9]{2})\:([0-9]{2})$/i", $_POST['end'], $end)) {
								$value[$field] = $start[1].':'.$start[2].' - '.$end[1].':'.$end[2];
							}
						}
					} break;
                    case 'time': { if (preg_match("/^([0-9]{2})\:([0-9]{2})$/i", $_POST[$field], $time)) $value[$field] = $time[1].':'.$time[2].':00'; } break;
					case 'password': {
						if (!empty($_POST[$field]) || !empty($_POST[$field.'-ret'])) {
							$pass = filter(trim(in1251($_POST[$field])), 'nohtml');
							$ret = filter(trim(in1251($_POST[$field.'-ret'])), 'nohtml');
							if ($pass != $ret) {
								jQuery::evalScript(inUTF8('initSysDialog("������ �� ���������")'));
								jQuery::getResponse();
							}
							$value[$field] = md5($pass);
							$value['password'] = $pass;
							if ($id != 0) {
								jQuery::evalScript(inUTF8('initSysDialog("������ �������")'));
								if ($module == 'admin' && $id == $_SESSION['admin'][0]) {
									$_SESSION['admin'] = base64_encode($_SESSION['admin'][0].':'.$_SESSION['admin'][1].':'.$value[$field]);
								}
							}
						}
					} break;
					case 'checkbox': $value[$field] = !is_numeric($_POST[$field]) ? filter(trim(in1251($_POST[$field])), 'nohtml') : abs(intval($_POST[$field])); break;
					case 'cid': $value[$field] = abs(intval($_POST[$field])); break;
					case 'sid': $value[$field] = abs(intval($_POST[$field])); break;
					case 'colors': {
						if (!empty($_POST[$field])) {
							$item = array();
							foreach ($_POST[$field] as $cid) {
								$cid = abs(intval($cid));
								if (!empty($cid)) $item[] = $cid;
							}
							$value[$field] = implode(':', $item);
						} else $value[$field] = null;
					} break;
					case 'pach': {
						if (empty($link)) {
							jQuery::evalScript(inUTF8("alert('���� \"��������� ���\" ����� ���������')"));
							jQuery::getResponse();
						}
						if (!empty($id)) {
							$r = db_query('SELECT s.`pach` FROM `sw_section` as s LEFT JOIN `sw_section` as d ON s.`id` = d.`sid` WHERE d.`id` = '.quote($id));
						} else {
							$r = db_query('SELECT `pach` FROM `sw_section` WHERE `id` = '.quote(abs(intval($_POST['sid']))));
						}
						if (mysql_num_rows($r) != 0) {
							$d = mysql_fetch_array($r, MYSQL_ASSOC);
							$value[$field] = $d['pach'].$link.'/';
						} else {
							$value[$field] = '/catalog/'.$link.'/';
						}
					} break;
					case 'interval': {
						if (empty($_POST['min']) || empty($_POST['max'])) {
							jQuery::evalScript(inUTF8('initSysDialog("������� � �������� �� ���������")'));
							jQuery::getResponse();
						}
						$min = abs(intval($_POST['min']));
						$max = abs(intval($_POST['max']));
						$value[$field] = '�� '.$min.' �� '.$max;
					} break;
					case 'catalog': break;
                    case 'tovar_list': break;
                    case 'section_items': {
                        $section_arr = (isset($_POST['item_list']))?$_POST['item_list']:array();
                        query_new("UPDATE `sw_catalog_section` SET `basket_show`=0 WHERE `sid`=".$_GET['id']);
                        foreach($section_arr as $s_key=>$s_item){
                            query_new("UPDATE `sw_catalog_section` SET `basket_show`=1 WHERE `sid`=".$_GET['id']." AND `cid`=".$s_key);
                        }
                    } break;
                    case 'filter_sellist': { $value[$field] = (is_array($_POST[$field]) && !empty($_POST[$field]))?implode(':', $_POST[$field]):''; } break;

					default: $value[$field] = filter(trim(in1251($_POST[$field])), 'nohtml'); break;
				}
			}
		}


        // ���������� ��� ������ Users ����� ������ ��� ��������� ������ ����� ��� ����������
        if ($module == 'users')
        {
            $message = '';
            if (!empty($value['user_card']))
            {
                $result = BonusCards::attachCardToUser($value['user_card'], $id);
                if (isset($result['error']) && $result['error'] == true)
                {
                    jQuery::evalScript(inUTF8('initSysDialog("'.$result['message'].'\n&nbsp;'.'")'));
                    jQuery::getResponse();
                }
                elseif($result['error'] == false)
                {
                    #jQuery::evalScript(inUTF8('initSysDialog("'.$result['message'].'\n&nbsp;'.'")'));
                    if (isset($result['discount']))
                    {
                        $value['discount'] = $result['discount'];
                    }
                }
            }
        }

        // ���������� ��� ������ Online ������, ��� ������ �� ���� ��������
        if ($module == 'orders')
        {
            $value['timeDelivery'] = $value['title'];
        }

		if (isset($_GLOBALS['field']['type']['colors']) && empty($value['colors'])) $value['colors'] = null;

		if (isset($_GLOBALS['edit-field']['link'])) {
			if (dbone('COUNT(`id`)', $id != 0 ? '`link` = '.quote($value['link']).' AND `id` != '.quote($id) : '`link` = '.quote($value['link']), $module) != 0) {
				jQuery::evalScript(inUTF8('initSysDialog("����� ��������� ��� ��� ������������. ���������� ������!")'));
				jQuery::getResponse();
			}
		}

		if (!empty($value)) {
			if ($id != 0) {
				if ($module == 'notify')
				{

					$updateRet =  db_update_data($value, $module, '`id` = '.quote(abs(intval($_GET['id']))));
					jQuery::evalScript('module.back()');

				}else{
					$updateRet =  db_update_data($value, $module, '`id` = '.quote(abs(intval($_GET['id']))));
					jQuery::evalScript('module.back()');
				}


			} else {
				if ($module == 'catalog') $value['price-start'] = $value['price'];
				if (db_insert_data($value, $module)===false){
                    $err = mysql_error();
                    jQuery::evalScript('initSysDialog("'.  inUTF8($err).'",5000)');
                }
				if (!empty($_GLOBALS['create_dir'])) {
					$id = dbone('MAX(`id`)', false, $module);
					$pach = '../../files/'.$module.'/'.$id.'/';
					if ($module == 'catalog' && empty($value['art'])) {
						db_query('UPDATE `sw_catalog` SET `art` = `id` WHERE `id` = '.quote($id));
					}
					if (!is_dir($pach)) {
						mkdir($pach, 0755);
					}
				}
				jQuery::evalScript('module.index()');
			}
		}
		if (isset($_GLOBALS['field']['type']['sid']) && $_GLOBALS['field']['type']['sid'] == 'catalog') {
			$sActive = 0;
			if ($id != 0) {
                $sids = query_new('SELECT `sid` FROM `sw_catalog_section` WHERE `cid` IN ('.  implode(',', $_POST['sid']).')');
				$r = db_query('SELECT `sid` FROM `sw_catalog_section` WHERE `cid` = '.mysql_real_escape_string($id).' AND `first` = 1');
				if (mysql_num_rows($r) != 0) {
					$s = mysql_fetch_array($r, MYSQL_NUM);
					if (in_array($s[0], $_POST['sid'])) $sActive = $s[0];
				}
				db_delete('catalog_section', '`cid` = '.quote($id));
			}

			foreach ($_POST['sid'] as $i=>$sid) {
				$r = db_query('SELECT `pach` FROM `sw_section` WHERE `id` = '.quote(abs(intval($sid))));
				if (mysql_num_rows($r) != 0) {
					$s = mysql_fetch_array($r, MYSQL_ASSOC);
					if ((empty($sActive) && $i == 0) || (!empty($sActive) && $sActive == $sid)) {
						db_query('INSERT INTO `sw_catalog_section` (`cid`, `sid`, `url`, `first`) VALUES ('.quote($id).', '.quote($sid).', '.quote($s['pach'].$link.'.html').', 1)');
					} else {
						db_query('INSERT INTO `sw_catalog_section` (`cid`, `sid`, `url`) VALUES ('.quote($id).', '.quote($sid).', '.quote($s['pach'].$link.'.html').')');
					}
				}
			}
		}

		unset($value);

		jQuery::getResponse();
	} // !empty($_POST)

	if (!empty($_GET['id'])) {
		$id = abs(intval($_GET['id']));

		if (empty($_GLOBALS['edit-field'])) {
			jQuery::evalScript(inUTF8('initSysDialog("���� ��� �������������� �� ������")'));
			jQuery::getResponse();
		}
        $item_list = false;
		$f = array();
        // ���������� ���� ��� ��������������
		foreach ($_GLOBALS['edit-field'] as $k=>$v)
        {
            if ($v!='item_list')
            {
                $f[$k] = '`'.$v.'`';
            }
            else
            {
                $item_list = $module;
            }
        }

		$field = implode(',', $f);
		$field .= ($module=='orders')?',`by_phone_only`':'';

        // TODO: ���� ��� �������, ����� ������� ���� ������� ���� � �� ���� �����
        if ($module == 'orders'){
            $raw = db_query('SELECT * FROM `sw_orders` WHERE `id` = '.quote($id) . ' LIMIT 0,1') ;
            $orderData = mysql_fetch_array($raw , MYSQL_ASSOC);

            logevent(15, $_SESSION['admin'][0], 2, array('order_id' => $id, 'itog' => $orderData['itogo'], 'comment' =>$orderData['recipientSMS'], 'type' => $orderData['type'], 'is_payed' =>$orderData['status']) ); // ��������
        }


		$r = db_query('SELECT '.$field.' FROM `sw_'.$module.'` WHERE `id` = '.quote($id));
		if (mysql_num_rows($r) == 0) {
            jQuery::evalScript(inUTF8('initSysDialog("<font size=\"3\">�� ������� ����� ���������� �� �������</font><br>(�������� ������ �������, ���������� ��� ��������)",5000)'));
            jQuery::getResponse();
        }
		$value = mysql_fetch_array($r, MYSQL_ASSOC);

        switch ($item_list){
            case 'section':
                $value['item_list'] = query_new("SELECT cs.`cid`, cs.`basket_show`,cs.`url`, c.`title` FROM sw_catalog_section cs LEFT JOIN sw_catalog c ON cs.`cid`=c.`id` WHERE cs.sid=".$id.' ORDER BY c.`views` DESC');
                break;
        }

		$sid = $value['sid'];
		$fields = $_GLOBALS['edit-field'];
	} else {
		$id = 0;
		$sid = 0;
		$fields = $_GLOBALS['add-field'];
		if (!empty($_GLOBALS['field']['value'])) $value = $_GLOBALS['field']['value'];
	}

	$html = '<form id="item-save" action="/go/m='.$module.'&op=item&id='.$id.'&pg='.$pg.'&query='.htmlspecialchars(urlencode($search) , ENT_COMPAT | ENT_HTML401, 'cp1251').'" onsubmit="formSubmit(this); return false;">';
	if ($module=='users' && !empty($id)) {
        $orders = get_user_orders($id, array('orders','orders_fast','fast_baying'));
    }
    if ($module=='users' && !empty($orders)){
        $html.= '<table width="100%"><tr><td class="user_fields">';
    }else{
        $html.= '<p class="submit"><a href="javascript:scroll(0,0)" class="noborder"><input type="button" value="���������" class="b-textfield" onclick="formSubmit(jQuery(\'form#item-save\')); module.setState(\'view\');"></a> <input onclick="module.back()" type="button" value="<< �����" class="b-textfield"></p>';
    }
	$tiny = false;
	foreach ($fields as $field) {
        $_type = $_GLOBALS['field']['type'][$field];
		switch ($_type) {
            case 'position': {
                $html.= '<p id="f-'.$field.'" class="item"><label>'.$_GLOBALS['field']['title'][$field].':</label><input type="text" name="'.$field.'" class="f-field"'.(intval($value[$field])!==false ? ' value="'.intval($value[$field]).'"' : null).'></p>';
            }break;
			case 'text': {
			    // ��������� ������������ ������� ��� ��������
			    if ($field == 'user_card')
			    {
			        $u_card = get_user_disc($id);
			        if ((empty($value[$field])) && (isset($u_card['type'])) )
			        {
			            if ((!empty($u_card['type'])) && ($u_card['type'] == 'card') )
			            {
                            $value[$field] = $u_card['title'];
                        }
                    }

                    $html.= '<p id="f-'.$field.'" class="item"><label>'.$_GLOBALS['field']['title'][$field].':</label><input type="text" name="'.$field.'" class="f-field"'.(!empty($value[$field]) ? ' value="'.$value[$field].'"' : null).'></p>';
                }
                else
                {
                    $html.= '<p id="f-'.$field.'" class="item"><label>'.$_GLOBALS['field']['title'][$field].':</label><input type="text" name="'.$field.'" class="f-field"'.(!empty($value[$field]) ? ' value="'.$value[$field].'"' : null).'></p>';
                }

			} break;
			case 'link': {
				$html.= '<p id="f-'.$field.'" class="item"><label>'.$_GLOBALS['field']['title'][$field].':</label><a href="'.$value[$field].'" target="_blank">'.$value[$field].'</a></p>';
			} break;
			case 'insert': {
				$html.= '<input type="hidden" name="insert" value="'.time().'"';
			} break;
			case 'd-time': {
				$html.= '<input type="hidden" name="'.$field.'"'.(!empty($value[$field]) ? ' value="'.$value[$field].'"' : null).'>';
			} break;
			case 'time': {
				$html.= '<p id="f-'.$field.'" class="item"><label>'.$_GLOBALS['field']['title'][$field].':</label><input type="text" name="'.$field.'" class="f-field time-field"'.(!empty($value[$field]) ? ' value="'.$value[$field].'"' : null).'></p>';
				jQuery::evalScript('jQuery("input[name$=\''.$field.'\']").mask("99:99")');
			} break;
			case 'pach': {
				$html.= '<input type="hidden" name="'.$field.'"'.(!empty($value[$field]) ? ' value="'.$value[$field].'"' : null).'>';
			} break;
			case 'interval': {
				$html.= '<input type="hidden" name="'.$field.'"'.(!empty($value[$field]) ? ' value="'.$value[$field].'"' : null).'>';
			} break;
			case 'image': {
				if (isset($_GLOBALS['field']['settings'][$field])) {
					if ($id != 0) {
						$img = null;
						$images = !empty($value[$field]) ? explode(':', $value[$field]) : array();
						$_GLOBALS['field']['settings'][$field]['count']-= count($images);
						$pref = null;
						if (!empty($_GLOBALS['field']['settings'][$field]['size'])) {
							foreach ($_GLOBALS['field']['settings'][$field]['size'] as $size) {
								if (empty($min) || $size['width'] < $min) $min = $size['width'];
							}
							$pref = 'w'.$min.'_';
						}
						foreach ($images as $key=>$file) {
							$img.= '<li id="file-'.$key.'" style="background: url(/files/'.$module.'/'.$id.'/'.$pref.$file.') left top no-repeat" class="file"><a class="file-remove" href="javascript:void(0)" onclick="file.remove({id:'.$id.',key:'.$key.',field:\''.$field.'\'})" title="�������, ����� ������� ��� ������.">�������</a><span class="file-name">'.$file.'</span><span class="file-info"></span></li>';
						}
						$html.= '<p id="f-'.$field.'" class="item"><label>'.$_GLOBALS['field']['title'][$field].': <a id="'.$field.'-browse" href="javascript:void(0)">�����</a><span> | <a href="javascript:void(0)" id="'.$field.'-upload">���������</a></span><span> | <a id="'.$field.'-clear" href="javascript:void(0)">��������</a></span></label><div id="'.$field.'-container"><form method="post" enctype="multipart/form-data"><fieldset id="'.$field.'-fallback"><legend>�������� ������</legend><label>�������� �����:<input type="file" name="Filedata" /></label></fieldset><div id="'.$field.'-status" class="loader-status hidden"><div><strong class="overall-title"></strong><img src="/sf123sf123sf/library/loader/i/progress-bar/bar.gif" class="progress overall-progress" /></div><div><strong class="current-title"></strong><img src="/sf123sf123sf/library/loader/i/progress-bar/bar.gif" class="progress current-progress" /></div><div class="current-text"></div></div><ul id="'.$field.'-list" class="file_load_list">'.$img.'</ul></form></div></p>';
						jQuery::evalScript(inUTF8('file.init({id:'.$id.',f:"image",field:"'.$field.'",max:'.$_GLOBALS['field']['settings'][$field]['count'].',type:{'.$_GLOBALS['field']['settings'][$field]['type'].'}})'));
					} else $html.= '<p id="f-'.$field.'" class="item hint"><b>���������!</b> �������� ����������� � ������ �������� ����� ���������� ��������</p>';
				}
			} break;
			case 'file': {
				if (isset($_GLOBALS['field']['settings'][$field])) {
					if ($id != 0) {
						$file = null;
						$files = !empty($value[$field]) ? explode(':', $value[$field]) : array();
						$_GLOBALS['field']['settings'][$field]['count']-= count($files);
						foreach ($files as $key=>$file) {
							$img.= '<li id="file-'.$key.'" style="background: url(/sf123sf123sf/i/file-ico.png) left top no-repeat" class="file"><a class="file-remove" href="javascript:void(0)" onclick="file.remove({id:'.$id.',key:'.$key.',field:\''.$field.'\'})" title="�������, ����� ������� ��� ������.">�������</a><span class="file-name">'.$file.'</span><span class="file-info"></span></li>';
						}
						$html.= '<p id="f-'.$field.'" class="item"><label>'.$_GLOBALS['field']['title'][$field].': <a id="'.$field.'-browse" href="javascript:void(0)">�����</a><span> | <a href="javascript:void(0)" id="'.$field.'-upload">���������</a></span><span> | <a id="'.$field.'-clear" href="javascript:void(0)">��������</a></span></label><div id="'.$field.'-container"><form method="post" enctype="multipart/form-data"><fieldset id="'.$field.'-fallback"><legend>�������� ������</legend><label>�������� �����:<input type="file" name="Filedata" /></label></fieldset><div id="'.$field.'-status" class="loader-status hidden"><div><strong class="overall-title"></strong><img src="/sf123sf123sf/library/loader/i/progress-bar/bar.gif" class="progress overall-progress" /></div><div><strong class="current-title"></strong><img src="/sf123sf123sf/library/loader/i/progress-bar/bar.gif" class="progress current-progress" /></div><div class="current-text"></div></div><ul id="'.$field.'-list" class="file_load_list">'.$img.'</ul></form></div></p>';
						jQuery::evalScript(inUTF8('file.init({id:'.$id.',f:"file",field:"'.$field.'",max:'.$_GLOBALS['field']['settings'][$field]['count'].',type:{'.$_GLOBALS['field']['settings'][$field]['type'].'}})'));
					} else $html.= '<p id="f-'.$field.'" class="item hint"><b>���������!</b> �������� ������ �������� ����� ���������� ��������</p>';
				}
			} break;
			case 'password': {
				$html.= '<p id="f-'.$field.'" class="item"><label>'.$_GLOBALS['field']['title'][$field].':</label><input type="password" name="'.$field.'" class="f-field"></p>';
				$html.= '<p id="f-'.$field.'" class="item"><label>������������� ������:</label><input type="password" name="'.$field.'-ret" class="f-field"></p>';
			} break;
			case 'date': {
				if (!empty($value[$field])) {
					if (preg_match("/^([0-9]{4})\-([0-9]{2})\-([0-9]{2})$/i", $value[$field], $date)) {
						$value[$field] = $date[3].'.'.$date[2].'.'.$date[1];
					}
				}
				$html.= '<p id="f-'.$field.'" class="item"><label>'.$_GLOBALS['field']['title'][$field].':</label><input id="date-'.$field.'" type="text" name="'.$field.'" class="f-field date"'.(!empty($value[$field]) ? ' value="'.$value[$field].'"' : null).'></p>';
				jQuery::evalScript('date.load(jQuery("input#date-'.$field.'"))');
			} break;
			case 'date2': {
				if (!empty($value[$field])) {
					if (preg_match("/^([0-9]{4})\-([0-9]{2})\-([0-9]{2})$/i", $value[$field], $date)) {
						$value[$field] = $date[3].'.'.$date[2].'.'.$date[1];
					}
				}
				$html.= '<p id="f-'.$field.'" class="item"><label>'.$_GLOBALS['field']['title'][$field].':</label><input id="date-'.$field.'" type="text" name="'.$field.'" class="f-field date"'.(!empty($value[$field]) ? ' value="'.$value[$field].'"' : null).'></p>';
				jQuery::evalScript('date.load(jQuery("input#date-'.$field.'"))');
			} break;
			case 'title': {
				$html.= '<p id="f-'.$field.'" class="item"><label>'.$_GLOBALS['field']['title'][$field].':</label><input type="text" name="'.$field.'" class="f-field"'.(!empty($value[$field]) ? ' value=\''. str_replace("'", '"' , $value[$field]) .'\'' : null).'></p>';
				jQuery::evalScript('item.title(jQuery("p#f-'.$field.' input:first"))');
			} break;
			case 'textarea': {
				$html.= '<p id="f-'.$field.'" class="item"><label>'.$_GLOBALS['field']['title'][$field].':</label><textarea rows="6" name="'.$field.'" class="f-field">'.(!empty($value[$field]) ? $value[$field] : null).'</textarea></p>';
			} break;
			case 'html': {
				$html.= '<p id="f-'.$field.'" class="item"><label>'.$_GLOBALS['field']['title'][$field].':</label><textarea id="tiny-'.$field.'" name="'.$field.'">'.(!empty($value[$field]) ? stripslashes($value[$field]) : null).'</textarea></p>';
				jQuery::evalScript('htmlEditor.load("'.$field.'","'.($id != 0 ? '/files/'.$module.'/'.$id.'/' : '/files/'.$module.'/').'")');
			} break;
			case 'checkbox': {
			    if ($module == 'orders' &&  $field == 'status'){
                    $html.= '<p id="f-'.$field.'" class="item"><input type="hidden" name="'.$field.'" value="'.intval($value[$field]).'"><input type="checkbox" disabled="disabled" name="'.$field.'" value="'.intval($value[$field]).'"'.(!empty($value[$field]) ? ' checked' : null).'> '.$_GLOBALS['field']['title'][$field].'</p>';
                }else{
                    $html.= '<p id="f-'.$field.'" class="item"><input type="hidden" name="'.$field.'" value="0"><input type="checkbox" name="'.$field.'" value="1"'.(!empty($value[$field]) ? ' checked' : null).'> '.$_GLOBALS['field']['title'][$field].'</p>';
                }

			} break;
			case 'select': {
				$html.= '<p id="f-'.$field.'" class="item"><label>'.$_GLOBALS['field']['title'][$field].':</label><br /><select name="'.$field.'" class="f-field width-100"><option value="0">�� �������</option>'.set_selected_opt($_GLOBALS['field']['value'][$field],$value[$field]).'</select></p>';
			} break;
			case 'group': {
				$html.= '<p id="f-'.$field.'" class="item"><label>'.$_GLOBALS['field']['title'][$field].':</label><br /><select name="'.$field.'" class="f-field width-100"><option value="0">�� �������</option>'.setGroup($value[$field]).'</select></p>';
			} break;
            case 'filter_sellist': {
                $html.= '<p id="f-'.$field.'" class="item"><label>'.$_GLOBALS['field']['title'][$field].':</label><br /><select name="'.$field.'[]" class="f-field width-100" multiple size="10">'.filter_sellist($value[$field]).'</select></p>';
            } break;
			case 'colors': {
				$values = !empty($value[$field]) ? explode(':', $value[$field]) : array();
				$html.= '<p id="f-'.$field.'" class="item"><label>'.$_GLOBALS['field']['title'][$field].': <span style="font-size:11px;"><a href="javascript:void(0)" onclick="jQuery(this).parent().parent().parent().find(\'option\').removeAttr(\'selected\')">����� ���������</a></span></label><select class="f-field width-100" multiple name="'.$field.'[]" size="10">';
				$r = db_query('SELECT `id`, `title` FROM `sw_colors` WHERE `active` = 1');
				while ($c = mysql_fetch_array($r, MYSQL_ASSOC)) {
					$html.= '<option value="'.$c['id'].'"'.(in_array($c['id'], $values) ? ' selected' : null).'>'.$c['title'].'</option>';
				}
				$html.= '</select></p>';
			} break;
			case 'sid': {
				$html.= '<p id="f-'.$field.'" class="item"><label>'.$_GLOBALS['field']['title'][$field].':</label><select name="'.$field.'" class="f-field"><option value="0">�� �������</option>'.sidList($module, $value[$field], $id).'</select></p>';
			} break;
			case 'catalog': {
				if ($id != 0) {
					$html.= '<p id="f-type" class="item"><label>��� ������: <a href="javascript:type.insert('.$id.')" class="insert-type">�������� ����� ���</a></label><table id="types-list" class="types"><tr><th class="title">������������</th><th class="composition">������</th><th class="price">����</th><th class="price-old">������ ����</th><th class="size">������</th><th class="options">&nbsp;</th></tr>';
					$r = db_query('SELECT `id`, `title`, `price`, `price-old`, `size`, `composition`, `first` FROM `sw_catalog_price` WHERE `cid` = '.quote($id));
					while ($t = mysql_fetch_array($r, MYSQL_ASSOC)) {
						$html.= '<tr id="type-'.$t['id'].'"><td class="title"><a href="javascript:void(0)" onclick="initGET(\'/go/m=type&op=edit&id='.$t['id'].'\')">'.$t['title'].'</a></td><td>'.(!empty($t['composition']) ? $t['composition'] : '�').'</td><td>'.$t['price'].' ���.</td><td>'.$t['price-old'].' ���.</td><td>'.$t['size'].'</td><td class="options">'.(!empty($t['first']) ? '�� ���������' : '<a href="javascript:type.first('.$id.','.$t['id'].')">�� ���������</a>').' | <a href="javascript:type.remove('.$id.','.$t['id'].')">�������</a></td></tr>';
					}
					$html.= '</table></p>';
				}
				$r = db_query('SELECT s.`id`, s.`title`, c.`first` FROM `sw_catalog_section` as c LEFT JOIN `sw_section` as s ON c.`sid` = s.`id` WHERE c.`cid` = '.quote($id));
				if (mysql_num_rows($r) != 0) {
					while ($c = mysql_fetch_array($r, MYSQL_ASSOC)) {
						$item = !empty($c['first']) ? $c['title'] : '<a href="javascript:module.section('.$id.','.$c['id'].')">'.$c['title'].'</a>';
						!empty($group) ? $group.= ' | '.$item : $group = $item;
					}
					$group = '<div id="current-topics">�������� ������: <span>'.$group.'</span></div>';
				} else $group = null;
				$html.= '<p id="f-'.$field.'" class="item catalog-section"><label>'.$_GLOBALS['field']['title'][$field].':</label><select multiple name="'.$field.'[]" size="10" class="f-field width-100">'.sectionList(0, $id).'</select>'.$group.'</p>';
			} break;
            case 'sectlist': {$html.= '<p id="f-'.$field.'" class="item catalog-section"><label>'.$_GLOBALS['field']['title'][$field].':</label><select name="'.$field.'" class="f-field width-100">'.sectionList(0,$id,0,null,$value[$field]).'</select></p>';}break;
            case 'sectionList_seoSections': {$html.= '<p id="f-'.$field.'" class="item catalog-section"><label>'.$_GLOBALS['field']['title'][$field].':</label><select name="'.$field.'" class="f-field width-100">'.sectionList(0,$id,0,null,$value[$field]).'</select></p>';}break;
            case 'cid': { $html.= '<p id="f-'.$field.'" class="item catalog-section"><label>'.$_GLOBALS['field']['title'][$field].':</label><select name="'.$field.'" size="10" class="f-field width-100">'.cidList(0, 0, 0, null, $value[$field]).'</select></p>';} break;
            case 'tovar_list':{
                $html .= '<p id="f-'.$field.'" class="item catalog-section"><label>'.$_GLOBALS['field']['title'][$field].':</label><ul>';
                $list = unserialize(base64_decode($value[$field]));
                if (is_array($list)){
					$db_colors = query_new('SELECT `id`, `title`, `class` FROM `sw_colors` WHERE `active` = 1 ',0,'id');
                    foreach ($list as $tovar){
                        $url = query_new("SELECT * FROM sw_catalog_section scs WHERE scs.cid = ".$tovar['cid']." AND scs.first = 1",1);
                        $url = $url['url'];
                        $title = $tovar['title'];
                        $count = $tovar['count'];
                        $html .= '<li><a href="'.$url.'" target="_blank">'.$title.'</a>: ���������� - '.$count.', ����: ';
						if (!empty($tovar['color'])){
							$colors = explode('-', $tovar['color']);
							foreach ($colors as $c_key=>$col_id){
								$colors[$c_key] = $db_colors[$col_id]['title'];
							}
							$html .= implode(', ',$colors).'</li>';
						}else{
							$html .= '���</li>';
						}
                    }
                }
                $html .= '</ul></p>';
            } break;
            case 'section_items': {
                $html .='<fieldset class="section_items"><legend>'.$_GLOBALS['field']['title'][$field].' <label><input type="checkbox" onclick="list_items_all(this)">���</label></legend><table>';
                foreach ($value[$field] as $si_key=>$si_item){
                    $si_class = ($si_key%2>0)?'grey':'white';
                    $check = (intval($si_item['basket_show'])>0)?' checked ':'';
                    $html .='<tr class="'.$si_class.'"><td><input type="checkbox" '.$check.' name="item_list['.$si_item['cid'].']"></td><td><a href="'.$si_item['url'].'" target="_blank">'.$si_item['title'].'</a></td></tr>';
                }
                $html .='</table></fieldset>';
            } break;
            case 'collist': { $html.= '<p id="f-'.$field.'" class="item"><label>'.$_GLOBALS['field']['title'][$field].':</label><br /><select name="'.$field.'" class="f-field width-100"><option value="0">�� �������</option>'.bg_clolor_list($value[$field]).'</select></p>'; } break;

            case 'payment_list':
                $selectControl = '<select name="'.$field.'">';
                foreach(inc_vars::$types as $type=>$description):
                    $selected = ($type == $value[$field]) ? ' selected' : '';
                    $selectControl .= '<option value="'.$type.'"'.$selected.'>'.$description.'</option>';
                endforeach;
                $selectControl .= '</select>';
                $html .= '<p id="f-'.$field.'" class="item"><label>'.$_GLOBALS['field']['title'][$field].':</label><br />';
                $html .= $selectControl;
                $html .= '</p>';
                break;
			default: {
				jQuery::evalScript(inUTF8('alert("���� �'.$field.'� �� �������")'));
			} break;

		}
	}
	if ($module == 'users' && !empty($orders)){
        $mod_name = array('orders'=>'ONLINE ������','orders_fast'=>'������� �����','fast_baying'=>'������� �������');
        $html .= '</td><td class="user_orders">'
                . '<p class="submit"><a href="javascript:scroll(0,0)" class="noborder"><input type="button" value="���������" class="b-textfield" onclick="formSubmit(jQuery(\'form#item-save\'))"></a> <input onclick="module.back()" type="button" value="<< �����" class="b-textfield"></p>'
                . '<div><table class="items"><tr class="title"><th class="id">� ������</th><th>��� ������<br>(����� '.count($orders).'��.)</th></tr>';
        foreach ($orders as $key=>$order){
            $html .= '<tr'.($key%2>0 ? ' class="odd green" ':'').'><td>'.$order['id'].'</td><td class="orders u_'.$order['module'].'"><a href="javascript: initGET(\'/go/m='.$order['module'].'&op=item&id='.$order['id'].'\'); void(0);">'.$mod_name[$order['module']].'</a></td></tr>';
        }
        $html .= '</table></div>'
                . '<p class="submit"><a href="javascript:scroll(0,0)" class="noborder"><input type="button" value="���������" class="b-textfield" onclick="formSubmit(jQuery(\'form#item-save\'))"></a> <input onclick="module.back()" type="button" value="<< �����" class="b-textfield"></p>'
                . '</td></tr></table>';
        jQuery::evalScript('user.setOrderH();');
    }else{
        $html.= '<p class="submit"><a href="javascript:scroll(0,0)" class="noborder"><input type="button" value="���������" class="b-textfield" onclick="formSubmit(jQuery(\'form#item-save\'))"></a> <input onclick="module.back()" type="button" value="<< �����" class="b-textfield"></p>';
    }
	$html.= '</form>';

	if ($tiny) jQuery::evalScript('html.tiny("'.($id != 0 ? '/files/'.$module.'/'.$id.'/' : '/files/'.$module.'/').'")');

	jQuery('#content-data')->html(inUTF8($html));
	jQuery::getResponse();
}


function deleteQuestion(){
    $qId = !empty($_GET['id']) ? abs(intval($_GET['id'])) : -1;
    query_new("DELETE FROM sw_questions WHERE id =  " . $qId );
    jQuery('#i' . $qId)->remove();
    jQuery::getResponse();
}

function viewQuestion(){
    $pg = !empty($_GET['pg']) ? abs(intval($_GET['pg'])) : 1;
    $search = !empty($_GET['query']) ? filter(trim(in1251($_GET['query'])), 'nohtml') : null;
    $qId = !empty($_GET['id']) ? abs(intval($_GET['id'])) : -1;
    if ($qId < 0){
        jQuery::evalScript(inUTF8("alert('������� � ������ ����� [".$qId ."] ����������� ')"));
        jQuery::getResponse();
    }
    $html = "";
    $html.= '<p class="submit"><input onclick="module.back()" type="button" value="<< �����" class="b-textfield"></p>';
    $sql = sprintf("
    SELECT * FROM sw_questions WHERE id = %d
    ", mysql_real_escape_string($qId ));

    $question = query_new($sql,1);

    $html .= '<fieldset style="overflow:auto;">
        <legend> <h2>'.$question['date'].' ������ �� ������������ '.$question['name'].'</h2> </legend>';
    $html .= '<p><strong>��� ������������:</strong>'.$question["name"].'</p>';
    $html .= '<p><strong>�������:</strong>'.$question["phone"].'</p>';
    $html .= '<p><strong>E-mail:</strong>'.$question["email"].'</p>';
    $html .= '<p><strong>����� �������:</strong><br/><i>'.$question["text"].'</i></p><hr/>';
    if ($question['email'] != ''){
        $html .= '<fieldset style="overflow:auto;">
        <legend> <h4>������� ����� �� ����������� ����� '.$question["email"].'</h4> </legend>';
        $html .= '<textarea rows="10" cols="45" name="text" id="questionTxt"></textarea><br/>';
        $html .= '<button id="sendEmialQuestion">��������� ������</button>';
        $html .= '</fieldset>';
    }else{
        $html .= '<fieldset style="overflow:auto;">
        <legend> <h4>��������� ������ ������������</h4> </legend>';
        $html .= '������������ �� ������ ����� ����������� �����, ��������� ��� ��� ������: <span style="font-size: 22; font-weight: bolder;">'.$question['phone'].'</span>';
        $html .= '</fieldset>';
    }
    $html .= '
    <script>
    jQuery("#sendEmialQuestion").click(function(){
		
		var ans = jQuery("#questionTxt").val();
		if (ans.length > 0){
			initGET(\'/sendquestion/op=email&txt=\' + ans + \'&id='.$question['id'].'\');
			jQuery("#questionTxt").prop(\'disabled\', \'disabled\');
			jQuery("#questionTxt").after(\'<br/><div style="color:green;">��������� ����������</div>\');
		}else{
			jQuery("#questionTxt").css(\'border-color\', \'red\');
		}
	});
    </script>
    ';

    jQuery('#content-data')->html(inUTF8($html));
    jQuery::getResponse();
}

/*
 * �-� ��������� ���� � ������� - ������ ����
 */
function viewEvent($id=0){


    $pg = !empty($_GET['pg']) ? abs(intval($_GET['pg'])) : 1;
    $search = !empty($_GET['query']) ? filter(trim(in1251($_GET['query'])), 'nohtml') : null;
    $eventId = !empty($_GET['id']) ? abs(intval($_GET['id'])) : -1;
    if ($eventId < 0){
        jQuery::evalScript(inUTF8("alert('������� � ������ ����� [".$eventId."] ����������� ')"));
        jQuery::getResponse();
    }


    $html = "";
    $html.= '<p class="submit"><input onclick="module.back()" type="button" value="<< �����" class="b-textfield"></p>';
    $sql = sprintf("
    SELECT * FROM sw_adm_events WHERE id = %d
    ", mysql_real_escape_string($eventId ));

    $event = query_new($sql,1);
    $codeSql = sprintf("SELECT * FROM sw_event_codes WHERE id = %d LIMIT 0,1", mysql_real_escape_string($event['code']));
    $codeData = query_new($codeSql ,1);

    $codeStr = "
        <p><strong>�������:</strong>{$codeData['descr']}</p>
        <p><strong>����� ������� � WEB-����������:</strong>{$event['place']}</p>
        <p><strong>���� �������:</strong>{$event['date']}</p>
        ";

    if ((!empty($event['uid'])) && ($codeData['status'] == 2) ){
        $sqlUser = sprintf("SELECT * FROM sw_admin WHERE id = %d LIMIT 0,1;", mysql_real_escape_string($event['uid']));
        $user = query_new($sqlUser ,1);
        $userStr = "
        <br/>
        <hr/>
        <fieldset>
        <legend> <h3>���������� �� ��������������</h3> </legend>
        <p><strong>ID:</strong>{$user['id']}</p>
        <p><strong>���:</strong>{$user['title']}</p>
        <p><strong>E-mail:</strong>{$user['email']}</p>
        </fieldset>
        <br/>
        <hr/>
        ";
    }elseif((!empty($event['uid'])) && ($codeData['status'] != 2) ) {
        $sqlUser = sprintf("SELECT * FROM sw_users WHERE id = %d LIMIT 0,1;", mysql_real_escape_string($event['uid']));
        $user = query_new($sqlUser ,1);

        $userStr = "
        <br/>
        <hr/>
        <fieldset>
        <legend> <h3>���������� � ������������</h3> </legend>
        <p><strong>ID:</strong>{$user['id']}</p>
        <p><strong>���:</strong>{$user['username']}</p>
        <p><strong>E-mail:</strong>{$user['email']}</p>
        <p><strong>�������:</strong>{$user['sys-phone']}</p>
        <p><strong>�������� �� ���:</strong>".($user['sms'] == 0 ? '���' : '��')."</p>
        <p><strong>�������� �� email:</strong>".($user['subscription'] == 0 ? '���' : '��')."</p>
        <p><strong>������:</strong>{$user['country']}</p>
        <p><strong>�����:</strong>{$user['city']}</p>
        </fieldset>
        <br/>
        <hr/>
    
    ";
    }else{
        $userStr = "
        <br/>
        <hr/>
        <fieldset>
        <legend> <h3>���������� � ������������</h3> </legend>
        <p><strong>�� ��������������� - ���������� �����������</strong></p>
        
        </fieldset>
        <br/>
        <hr/>
        ";
    }


    $sessionData = unserialize($event['session']);
    $sessionTmp = recurse_array($sessionData );

    $sessionStr = "
     <br/>
        <hr/>
        <fieldset>
        <legend> <h3>������ ������ ������������</h3> </legend>
        $sessionTmp 
        </fieldset>
        <br/>
        <hr/>
    ";
    $cookieData = unserialize($event['cookies']);
    $cookieTmp  = "";
    foreach ($cookieData as $k=>$v){
        $cookieTmp .= "<p><strong>$k:</strong>$v</p>";
    }
    $cookieStr = "
     <br/>
        <hr/>
        <fieldset>
        <legend> <h3>������ COOKIE ������������</h3> </legend>
        $cookieTmp 
        </fieldset>
        <br/>
        <hr/>
    ";

    $infoData = unserialize($event['info']);
    $infoTMP = "";
    foreach ($infoData as $k=>$v){
        $infoTMP .= "<p><strong>$k:</strong>$v</p>";
    }
    $infoStr = "
     <br/>
        <hr/>
        <fieldset>
        <legend> <h3>������ �������� ������������ � ��������� ����������</h3> </legend>
        $infoTMP 
        </fieldset>
        <br/>
        <hr/>
    ";


    $html .= "
   <fieldset style='max-height: none;'>
      <legend><h1>��������� ���������� � ������� {$eventId}</h1></legend>
      {$userStr}
      {$codeStr}
     {$sessionStr}
     {$cookieStr}
     {$infoStr}
     <br/>
     <hr/>
     <fieldset>
       <legend> <h3>������ ������</h3> </legend>
        <p><strong>� ������:</strong>{$event['oid']}</p>
        <p><strong>����� ������:</strong>{$event['itogo']}</p>
        </fieldset>
        <br/>
        <hr/>
     
  
    ";
    $html .= "</fieldset>";
    jQuery('#content-data')->html(inUTF8($html));
    jQuery::getResponse();
}

function urlItem ($id=0){
	if (!empty($id)){
		$item = query_new('SELECT scs.`url`, sc.* FROM sw_catalog sc LEFT JOIN sw_catalog_section scs ON sc.`id`=scs.`cid` WHERE `id`='.intval($id).' AND scs.`first`=1',1);
		return $item;
	}
	return false;
}
function filter_sellist($vals=''){
    $vals = !empty($vals)?explode(':', $vals):array();
    $filter_dirs = query_new('SELECT sws.*,ss.`title` as "parent_title" FROM `sw_section` ss LEFT JOIN `sw_section` sws ON sws.`sid`=ss.`id` WHERE ss.`active`=1 AND sws.`active`=1 AND ss.`filter`=1',0,'sid',1);
    $dis='';
    foreach ($filter_dirs as $sel_fil){
        $dis_titl = true;
        foreach ($sel_fil as $filt){
            if ($dis_titl){
                $dis .= '<option disabled>'.$filt['parent_title'].'</option>';
                $dis_titl = false;
            }
            $dis .= '<option value="'.$filt['id'].'" '.(in_array($filt['id'],$vals)?'selected':'').'>'.$filt['title'].'</option>';
        }
    }
    return $dis;
}

function bg_clolor_list($id = 0){
	$ret = '';
	$bgcols = query_new('SELECT `bg_style`, `id`, `title` FROM `sw_colors` WHERE `active`=1 AND `bg_style`!="" ');
	foreach ($bgcols as $bgc){
		$ret .= '<option style="border-radius:6px;margin:2px;padding:2px 6px 4px;color:#000;text-shadow:0 0 3px #000;'.$bgc['bg_style'].'" value="'.$bgc['id'].'" '.($id==$bgc['id']?'selected':'').'> '.$bgc['title'].'</option>';
	}
	return $ret;
}

function set_selected_opt($arr, $select)
{
    global $module;

    switch ($module)
    {
        case 'modules':
            foreach ($arr as $key=>$row)
            {
                if ($select==$key)
                {
                    $arr[$key] = str_replace('<option', '<option selected ', $row);
                }
            }
            return implode('', $arr);
            break;
        case 'users':
            // ��� � �����, ������ ���� ������� - ������� ������ � options ��� select
            $options = '';
            foreach($arr as $value=>$text)
            {
                $selected = ($select == $value) ? 'selected' : '';
                $options .= '<option value="'.$value.'" '.$selected.'>'.$text.'</option>';
            }
            return $options;
            break;
        default:
           return $arr;
    }
}

function sectionList($sid = 0, $id = 0, $level = 0, $h = null, $v = false) {
	global $level;

	$level++;
	$line = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	$pref = null;

	for ($i=1;$i<$level;$i++) {
		$pref.= $line;
	}

	$r = db_query('SELECT s.`id`, s.`title`, (IF(v.`sid` IS NULL,0,1)) as `selected`, (IF(d.`id` IS NULL,0,1)) as `dir` FROM `sw_section` as s LEFT JOIN `sw_catalog_section` as v ON v.`cid` = '.quote($id).' AND v.`sid` = s.`id` LEFT JOIN `sw_section` as d ON d.`sid` = s.`id` WHERE s.`sid` = '.quote($sid).' GROUP BY s.`id` ORDER BY s.`title` ASC');
	while ($p = mysql_fetch_array($r, MYSQL_ASSOC)) {
		$h.= '<option value="'.$p['id'].'"'.(!empty($p['selected']) || $v==$p['id'] ? ' selected' : null).'>'.$pref.$p['title'].'</option>';
		if (!empty($p['dir'])) {
			$h.= sectionList($p['id'], $id, $level,null,$v);
			$level--;
		}
	}

	return $h;
}

function sectionList_seoSections($sid = 0, $id = 0, $level = 0, $h = null, $v = false) {
	global $level;

	$level++;
	$line = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	$pref = null;

	for ($i=1;$i<$level;$i++) {
		$pref.= $line;
	}

	$r = db_query('SELECT s.`id`, s.`title`, (IF(d.`id` IS NULL,0,1)) as `dir` FROM `sw_section` as s LEFT JOIN `sw_catalog_section` as v ON v.`cid` = '.quote($id).' AND v.`sid` = s.`id` LEFT JOIN `sw_section` as d ON d.`sid` = s.`id` WHERE s.`sid` = '.quote($sid).' GROUP BY s.`id` ORDER BY s.`title` ASC');
	while ($p = mysql_fetch_array($r, MYSQL_ASSOC)) {
		$h.= '<option value="'.$p['id'].'"'.($v==$p['id'] ? ' selected' : null).'>'.$pref.$p['title'].'</option>';
		if (!empty($p['dir'])) {
			$h.= sectionList($p['id'], $id, $level,null,$v);
			$level--;
		}
	}

	return $h;
}

function cidList($sid = 0, $id = 0, $level = 0, $h = null, $v) {
	global $level;

	$level++;
	$line = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	$pref = null;

	for ($i=1;$i<$level;$i++) {
		$pref.= $line;
	}

	$r = db_query('SELECT s.`id`, s.`title`, (IF(v.`sid` IS NULL,0,1)) as `selected`, (IF(d.`id` IS NULL,0,1)) as `dir` FROM `sw_section` as s LEFT JOIN `sw_catalog_section` as v ON v.`cid` = '.quote($id).' AND v.`sid` = s.`id` LEFT JOIN `sw_section` as d ON d.`sid` = s.`id` WHERE s.`sid` = '.quote($sid).' GROUP BY s.`id` ORDER BY s.`title` ASC');
	while ($p = mysql_fetch_array($r, MYSQL_ASSOC)) {
		$h.= '<option value="'.$p['id'].'"'.(!empty($v) && $v == $p['id'] ? ' selected' : null).'>'.$pref.$p['title'].'</option>';
		if (!empty($p['dir'])) {
			$h.= cidList($p['id'], $id, $level, null, $v);
			$level--;
		}
	}

	return $h;
}

function sidList($m, $v, $id = 0, $h = null) {
	$r = db_query('SELECT `id`, `title` FROM `sw_'.$m.'` WHERE `id` != '.quote($id).' ORDER BY `sid` ASC, `title` ASC');
	while ($p = mysql_fetch_array($r, MYSQL_ASSOC)) $h.= '<option value="'.$p['id'].'"'.($v != $p['id'] ? null : ' selected').'>'.$p['title'].'</option>';
	return $h;
}

function itemRemove() {
	global $module;

	if (!_IS_ADMIN && empty($_GET['id'])) {
        jQuery::evalScript(inUTF8('initSysDialog("<font size=\"3\">������ ��� ��������</font><br>(������� ������� ������ ��� �������� � �������)",4000)'));
        jQuery::getResponse();
    }
    if ($module == 'abandoned')
    {
	    $id = trim(filter($_GET['id'], 'nohtml'));
	    if (!empty($id ))
	    {
	    	db_query("DELETE FROM `sw_abandoned_carts` WHERE `hash` = " . quote($id));
		    jQuery::evalScript('items.removeDone(\''.$id.'\');');
		    jQuery::getResponse();

	    }
    }
    else
    {

	    $id = abs(intval($_GET['id']));

	    if ($module == 'orders')
	    {
		    $r         = db_query(sprintf("SELECT * FROM sw_orders WHERE id  = %d", mysql_real_escape_string($id)));
		    $orderData = mysql_fetch_array($r, MYSQL_ASSOC);
		    logevent(14, $_SESSION['admin'][0], 2, array('order_id' => $id, 'itog' => $orderData['itogo'], 'comment' => $orderData['recipientSMS'], 'type' => $orderData['type'], 'is_payed' => $orderData['status'])); // ��������
	    }

	    if (deleteItems($module, $id) === true)
	    {
		    $sid         = !empty($_SESSION['init_module'][$module]['sid']) ? $_SESSION['init_module'][$module]['sid'] : 0;
		    $pg          = !empty($_SESSION['init_module'][$module]['pg']) ? $_SESSION['init_module'][$module]['pg'] : 1;
		    $search      = !empty($_SESSION['init_module'][$module]['search']) ? $_SESSION['init_module'][$module]['search'] : '';
		    $init_module = initModule($sid, $pg, $search, true);
		    $tr          = (!empty($init_module['rows']) && !empty($init_module['paging'])) ? array_pop($init_module['rows']) : '';
		    jQuery('table#list-items')->append(inUTF8($tr));
		    if (isset($init_module['paging']))
		    {
			    jQuery('div.paging')->replaceWith(inUTF8($init_module['paging']));
		    }
		    jQuery::evalScript('items.removeDone(' . $id . ');');
	    }
	    else
	    {
		    jQuery::evalScript('items.removeDone()');
	    }
	    jQuery::getResponse();
    }
}

function deleteItem($module, $id) {
	switch ($module) {
        case 'catalog': { db_delete('catalog_section', '`cid` = '.quote($id)); } break;
	}

	if (db_query('DELETE FROM `'._MYSQL_PREFIX.$module.'` WHERE `id` = '.quote($id))) {
		FolderDelete('../../'._DIR_USERS_FILES.$module.'/'.$id.'/');
		return true;
	}
    return false;
}

function deleteItems($module, $id) {
	$r = db_query('SELECT m.`id`, m.`sid`, (SELECT COUNT(`id`) FROM `'._MYSQL_PREFIX.$module.'` WHERE `sid` = m.`id`) as `dir` FROM `'._MYSQL_PREFIX.$module.'` as m WHERE m.`sid` = '.quote($id));
	while ($i = mysql_fetch_array($r, MYSQL_ASSOC)) {
		if ($i['dir'] != 0) {
			deleteItems($module, $i['id']);
		}
		deleteItem($module, $i['id']);
	}
	return deleteItem($module, $id);
}

function exitAdmin() {
	unset($_SESSION['admin']);
	jQuery::evalScript('goUrl("/'._ADMIN_DIR.'/")');
	jQuery::getResponse();
}

function paging($op, $counts, $pg, $on_page = 10, $url = null) {
	global $module;

	$pages_to_show = 6;
	$page_coll = ceil($counts / $on_page);
	$half_pages_to_show = round($pages_to_show / 2);
	$i_minus = $pg - $half_pages_to_show;
	$i_plus = $pg + $half_pages_to_show;
	$items = array();

	if (empty($url)) $url = '/go/m='.$module.'&op='.$op;

	if ($page_coll > 1) {
		if ($pg > 1) $items[]= '<a onclick="initGET(\''.$url.'&pg=1\'); module.setState(\'view\');">< �����</a>';
		for($i=$i_minus; $i <= $i_plus; $i++) {
			if ($i >= 1 && $i <= $page_coll) {
			    if ($i == 1){
                    $items[]= $i == $pg ? '<span>'.$i.'</span>' : '<a onclick="initGET(\''.$url.'&pg='.$i.'\'); module.setState(\'view\');">'.$i.'</a>';
                }else{
                    $items[]= $i == $pg ? '<span>'.$i.'</span>' : '<a onclick="initGET(\''.$url.'&pg='.$i.'\'); module.setState(\'edit\');">'.$i.'</a>';
                }

			}
		}
		if ($pg != $page_coll) $items[]= '<a onclick="initGET(\''.$url.'&pg='.$page_coll.'\'); module.setState(\'edit\');">������ ></a>';
	}

	return !empty($items) ? '<div class="paging">'.implode('&nbsp;', $items).'</div>' : null;
}


function tpl_tpluralForm($n, $form1, $form2, $form3)
{
	$n = abs($n) % 100;
	$n1 = $n % 10;

	if ($n > 10 && $n < 20) {
		return $form3;
	}

	if ($n1 > 1 && $n1 < 5) {
		return $form2;
	}

	if ($n1 == 1) {
		return $form1;
	}

	return $form3;
}


function recurse_array($values){
$content = '';
if( is_array($values) ){
    foreach($values as $key => $value){
        if( is_array($value) ){
            $content.="<strong>$key</strong> ------>>>><br />".recurse_array($value);
        }else{
            $content.="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>$key</strong> : $value<br />";
        }

    }
}
return $content;
}
?>