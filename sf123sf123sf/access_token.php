<?php

if ($_REQUEST['code'] != '') {
    $basepath = dirname(__FILE__)."/";

    session_start();

    include_once($basepath.'../system/config.php');
    include_once($basepath.'../system/db.php');
    include_once($basepath.'../system/instagram.class.php');

    $conf = [
        'apiKey'=>INSTAGRAM_API_KEY,
        'apiSecret'=>INSTAGRAM_API_SECRET,
        'apiCallback'=>INSTAGRAM_API_CALLBACK
    ];
    $instagram = new Instagram($conf);
    $data = $instagram->getOAuthToken($_REQUEST['code']);
    $instagram->setAccessToken($data);

    $_SESSION['instagram_access_token'] = $instagram->getAccessToken() ? $instagram->getAccessToken() : '';
    $_SESSION['instagram_user_id'] = $data->user->id ? $data->user->id : '';

    $url = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}/sf123sf123sf/";
    header('Location: '.$url);
}