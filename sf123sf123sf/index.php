<?
    session_start();

    header('Content-Type: text/html; charset=windows-1251');

    include_once('../system/config.php');
    include_once('../system/db.php');
    include_once('../system/filter.php');
    include_once('core/authorization.php');

    echo file_get_contents(!_IS_ADMIN ? 'core/login.html' : 'core/adminka.html');
?>