<?
	if (!defined('_SWS_ADMIN_CORE')) exit();

    $_GLOBALS['create_dir'] = true;
    $_GLOBALS['add'] = true;
    $_GLOBALS['search'] = false;
    $_GLOBALS['on-page'] = 20;
    $_GLOBALS['tiny'] = false;
    $_GLOBALS['date'] = false;

    $_GLOBALS['field']['title']['id'] = 'ID';
    $_GLOBALS['field']['title']['sid'] = '������/���������';
    $_GLOBALS['field']['title']['active'] = '���./����.';
    $_GLOBALS['field']['title']['home_active'] = '�� �������';
    $_GLOBALS['field']['title']['title'] = '��������:';
    $_GLOBALS['field']['title']['url'] = '������ ��� ���������� ��������:';
    $_GLOBALS['field']['title']['banner'] = '�������';
    $_GLOBALS['field']['title']['pos'] = '�������';

    $_GLOBALS['field']['type']['sid'] = 'sid';
    $_GLOBALS['field']['type']['active'] = 'checkbox';
    $_GLOBALS['field']['type']['home_active'] = 'checkbox';
    $_GLOBALS['field']['type']['title'] = 'text';
    $_GLOBALS['field']['type']['pos'] = 'position';
    $_GLOBALS['field']['type']['url'] = 'text';
    $_GLOBALS['field']['type']['banner'] = 'file';


    $_GLOBALS['field']['db']['sid'] = "int(11) NOT NULL default '0'";
    $_GLOBALS['field']['db']['active'] = "int(1) NOT NULL default '0'";
    $_GLOBALS['field']['db']['home_active'] = "int(1) NOT NULL default '0'";
    $_GLOBALS['field']['db']['title'] = "varchar(255) default NULL";
    $_GLOBALS['field']['db']['pos'] = "int(11) NOT NULL default '0'";
    $_GLOBALS['field']['db']['url'] = "text";
    $_GLOBALS['field']['db']['banner'] = "text";
    $_GLOBALS['field']['db']['insert'] = "timestamp NOT NULL default CURRENT_TIMESTAMP";
    $_GLOBALS['field']['db']['system'] = "int(1) NOT NULL default '0'";


    create_table($module, $_GLOBALS['field']['db']);
    check_tbl(array($module=>$_GLOBALS['field']['db']));
    $_GLOBALS['field']['table']['id'] = 'text';
    $_GLOBALS['field']['table']['pos'] = 'pos';
    $_GLOBALS['field']['table']['title'] = 'title';
    $_GLOBALS['field']['table']['home_active'] = 'checkbox';

    $_GLOBALS['field']['settings']['banner']['count'] = 2;
    $_GLOBALS['field']['settings']['banner']['type'] = '"�������": "*.jpg; *.jpeg; *.gif; *.png"';
    $_GLOBALS['field']['settings']['banner']['extension'] = array('jpg', 'jpeg', 'gif', 'png');

    $_GLOBALS['edit-field'] = array('active', 'home_active', 'title','pos', 'url', 'banner');
    $_GLOBALS['add-field'] = $_GLOBALS['edit-field'];