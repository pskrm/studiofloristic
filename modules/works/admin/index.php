<?
	if (!defined('_SWS_ADMIN_CORE')) exit();
	
	$_GLOBALS['create_dir'] = true;
	$_GLOBALS['add'] = true;
	$_GLOBALS['search'] = true;
	$_GLOBALS['on-page'] = 20;
	$_GLOBALS['tiny'] = true;
	$_GLOBALS['date'] = true;
	
	$_GLOBALS['field']['title']['id'] = 'ID';
	$_GLOBALS['field']['title']['sid'] = '������/���������';
	$_GLOBALS['field']['title']['active'] = '���./����.';
	$_GLOBALS['field']['title']['pos'] = '�������';
	$_GLOBALS['field']['title']['link'] = '��������� ���';
	$_GLOBALS['field']['title']['title'] = '���������';
	$_GLOBALS['field']['title']['notice'] = '�����';
	$_GLOBALS['field']['title']['text'] = '������ ����������';
	$_GLOBALS['field']['title']['date'] = '���� �������';
	$_GLOBALS['field']['title']['image'] = '�����������';
	$_GLOBALS['field']['title']['html_title'] = '��������� �������� (TITLE)';
	$_GLOBALS['field']['title']['keywords'] = '�������� ����� (KEYWORDS)';
	$_GLOBALS['field']['title']['description'] = '�������� (DESCRIPTION)';
	
	$_GLOBALS['field']['type']['sid'] = 'sid';
	$_GLOBALS['field']['type']['active'] = 'checkbox';
	$_GLOBALS['field']['type']['pos'] = 'position';
	$_GLOBALS['field']['type']['link'] = 'text';
	$_GLOBALS['field']['type']['title'] = 'title';
	$_GLOBALS['field']['type']['notice'] = 'html';
	$_GLOBALS['field']['type']['text'] = 'html';
	$_GLOBALS['field']['type']['date'] = 'date';
	$_GLOBALS['field']['type']['image'] = 'image';
	$_GLOBALS['field']['type']['html_title'] = 'textarea';
	$_GLOBALS['field']['type']['keywords'] = 'textarea';
	$_GLOBALS['field']['type']['description'] = 'textarea';
	
	$_GLOBALS['field']['table']['id'] = 'text';
	$_GLOBALS['field']['table']['title'] = 'title';
	
	$_GLOBALS['field']['value']['active'] = 1;
	
	$_GLOBALS['field']['settings']['image']['count'] = 30;
	$_GLOBALS['field']['settings']['image']['type'] = '"�����������": "*.jpg; *.jpeg; *.gif; *.png"';
	$_GLOBALS['field']['settings']['image']['size'] = array(array('width'=>120, 'height'=>90, 'ratio'=>true), array('width'=>128, 'height'=>116, 'ratio'=>true), array('width'=>600, 'height'=>400, 'ratio'=>true, 'watermark'=>true));
	
	$_GLOBALS['edit-field'] = array('active', 'title', 'link', 'notice', 'text', 'date', 'html_title', 'keywords', 'description', 'image');
	$_GLOBALS['add-field'] = $_GLOBALS['edit-field'];
?>