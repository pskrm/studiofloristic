$(document).ready(function(){
	$("ul.mycarousel a").fancybox({
		overlayColor:"#000",
		overlayOpacity:"0.5"
	});
});

var works = {
	open: function(w) {
		jQuery('div#quickOrderBlock').html('<span class="close">&nbsp;</span><h2>�������� ������</h2><p id="loading">����������� ���������, ���� �������� ������...</p>');
		
		works.initClose();
		
		jQuery('#darkBack').showDarkBg("show", 0);
		jQuery('#quickOrderBlock').fadeIn(500, function() {
			initGET('/works/op=view&id='+w);
		});
		
		jQuery('#quickOrderBlock').calculatePositionCenter();
	},
	initClose: function() {
		jQuery('div#quickOrderBlock .close').click(function(){
			jQuery('#darkBack').showDarkBg('hide', 0);
			jQuery(this).parent().fadeOut(500);
		});
		jQuery('#darkBack').click(function(){
			jQuery('#darkBack').showDarkBg('hide', 0);
			jQuery('#quickOrderBlock').fadeOut(500);
		});
	}
}