<?
if (!defined('SW')) die('���� ������� �� ���������');

switch($op) {
	
	case 'view': {
		include_once('include/jquery/jQuery.php');
		initViewWork();
	} break;
	
	default: getWorks(); break;
	
}

function initViewWork() {
	if (empty($_GET['id'])) jQuery::getResponse();
	
	$id = abs(intval($_GET['id']));
	
	$r = db_query('SELECT `text` FROM `sw_works` WHERE `id` = '.quote($id));
	if (mysql_num_rows($r) == 0) jQuery::getResponse();
	$w = mysql_fetch_array($r, MYSQL_ASSOC);
	
	$html = '<span class="close">&nbsp;</span><h1>�������� ���������� � ����:</h1>';
	$html.= stripslashes($w['text']);
	
	jQuery('div#quickOrderBlock')->html(inUTF8($html));
	jQuery::evalScript('works.initClose();jQuery("#quickOrderBlock").calculatePositionCenter();');
	jQuery::getResponse();
}

function getWorks() {
	global $_GLOBALS;
	
	$data = PageData('works');
	
	$html = null;
	$year = array(0=>'���');
	$yid = !empty($_GET['year']) ? abs(intval($_GET['year'])) : 0;
	$where = !empty($yid) ? ' AND YEAR(`date`) = '.quote($yid) : null;
	
	if (!empty($data['text'])) $html.= '<div class="mainTextInner">'.stripslashes($data['text']).'</div>';
	
	$r = db_query('SELECT YEAR(`date`) as `year` FROM `sw_works` WHERE `active` = 1 GROUP BY `year` ORDER BY `year` DESC');
	while ($y = mysql_fetch_array($r, MYSQL_NUM)) $year[$y[0]] = $y[0];
	
	$html.= '<p class="works-years">';
	foreach ($year as $k=>$y) $html.= $yid != $k ? '<a href="/'.(!empty($k) ? 'works/op=filter&year='.$k : 'works.html').'">'.$y.'</a>' : '<span>'.$y.'</span>';
	$html.= '</p>';
	
	$r = db_query('SELECT `id`, `title`, `notice`, `image` FROM `sw_works` WHERE `active` = 1'.$where.' ORDER BY `id` DESC LIMIT 0,10');
	while ($w = mysql_fetch_array($r, MYSQL_ASSOC)) {
		$img = explode(':', $w['image']);
		
		$html.= '<div class="mainTextInner works-item blockWithTopLine"><h2>'.$w['title'].'</h2>';
		$html.= stripslashes($w['notice']);
		$html.= '<ul class="mycarousel jcarousel-skin-tango">';
		foreach ($img as $item) $html.= '<li><a href="/files/works/'.$w['id'].'/w600_'.$item.'" class="haveTooltip" rel="example-'.$w['id'].'"><img src="/files/works/'.$w['id'].'/w128_'.$item.'" /></a></li>';
		$html.= '</ul><p class="right"><a class="zakazButton quickOrder" href="javascript:works.open('.$w['id'].')">��������� � ����</a></p></div>';
	}
	
	$html.= '<script type="text/javascript" src="/modules/works/works.js"></script><script type="text/javascript" src="/js/jquery.mousewheel-3.0.4.pack.js"></script><script type="text/javascript" src="/js/jquery.fancybox-1.3.4.pack.js"></script><link rel="stylesheet" type="text/css" href="/css/jquery.fancybox-1.3.4.css" media="screen" />';
	
	$_GLOBALS['section'] = 'works';
	$_GLOBALS['title'] = $data['title'];
	$_GLOBALS['text'] = $html;
	$_GLOBALS['html_title'] = !empty($data['html_title']) ? $data['html_title'] : $data['title'].' - '.$_GLOBALS['v_sitename'];
	if (!empty($data['description'])) $_GLOBALS['description'] = $data['description'];
	if (!empty($data['keywords'])) $_GLOBALS['keywords'] = $data['keywords'];
	$_GLOBALS['template'] = 'default';
}

?>