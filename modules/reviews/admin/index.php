<?
	if (!defined('_SWS_ADMIN_CORE')) exit();
	
	$_GLOBALS['create_dir'] = false;
	$_GLOBALS['add'] = true;
	$_GLOBALS['search'] = true;
	$_GLOBALS['on-page'] = 20;
	$_GLOBALS['tiny'] = false;
	$_GLOBALS['date'] = true;
	
	$_GLOBALS['field']['title']['id'] = 'ID';
	$_GLOBALS['field']['title']['sid'] = '������/���������';
	$_GLOBALS['field']['title']['active'] = '���./����.';
	$_GLOBALS['field']['title']['pos'] = '�������';
	$_GLOBALS['field']['title']['title'] = '���';
	$_GLOBALS['field']['title']['email'] = 'E-mail';
	$_GLOBALS['field']['title']['text'] = '������';
	$_GLOBALS['field']['title']['reply'] = '�����';
	$_GLOBALS['field']['title']['date'] = '����';
	
	$_GLOBALS['field']['type']['sid'] = 'sid';
	$_GLOBALS['field']['type']['active'] = 'checkbox';
	$_GLOBALS['field']['type']['pos'] = 'position';
	$_GLOBALS['field']['type']['title'] = 'text';
	$_GLOBALS['field']['type']['email'] = 'text';
	$_GLOBALS['field']['type']['text'] = 'textarea';
	$_GLOBALS['field']['type']['reply'] = 'textarea';
	$_GLOBALS['field']['type']['date'] = 'date';
	$_GLOBALS['field']['type']['insert'] = 'insert';
	
	$_GLOBALS['field']['db']['sid'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['active'] = "int(1) NOT NULL default '0'";
	$_GLOBALS['field']['db']['pos'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['title'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['email'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['text'] = "text";
	$_GLOBALS['field']['db']['reply'] = "text";
	$_GLOBALS['field']['db']['date'] = "text";
	$_GLOBALS['field']['db']['insert'] = "timestamp NOT NULL default CURRENT_TIMESTAMP";
	$_GLOBALS['field']['db']['system'] = "int(1) NOT NULL default '0'";
	
	$_GLOBALS['field']['table']['id'] = 'text';
	$_GLOBALS['field']['table']['title'] = 'title';
	
	$_GLOBALS['field']['value']['active'] = 1;
	$_GLOBALS['field']['value']['date'] = date('d.m.Y');
	
	$_GLOBALS['edit-field'] = array('active', 'title', 'email', 'text', 'reply', 'date');
	$_GLOBALS['add-field'] = array('active', 'title', 'email', 'text', 'reply', 'date', 'insert');
?>