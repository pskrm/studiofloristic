<?

if (!defined('SW'))
    die('���� ������� �� ���������');

include_once('include/jquery/jQuery.php');

switch ($op) {
    case 'add': addReview(); break;
    case 'init': formInit(); break;
    case 'item': itemReview(); break;
    case 'insert': insertReview(); break;
    default: getReviews(); break;
}

function addReview() {
    $post = array();
    $err  = 0;
    foreach ($_POST as $key => $val) {
        $post[$key] = filter(trim(in1251($val)), 'nohtml');
        if ($post[$key] == '' && $key != 'phone') {
            $err++;
            jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#review_' . $key . '"),t:"������ ���� ����������� ��� ����������",top:-15})'));
        }
    }
    if ($err > 0) {
        jQuery::getResponse();
    }
    if ($post['captcha'] == $_SESSION['captcha']) {
        $name  = $post['name'];
        $email = $post['email'];
        $text  = $post['text'];
        $url   = $_SERVER['HTTP_REFERER'];//$post['url'];
        $date  = date('d.m.Y');
        $uid   = _IS_USER ? abs(intval($_SESSION['uid'])) : 0;
        $id    = $post['cid'];
        $stars = $post['star'];
        $rid   = query_new('INSERT INTO `sw_catalog_reviews` (`uid`, `cid`, `name`, `email`, `text`, `date`, `url`, `insert`, `stars`) VALUES (' . quote($uid) . ', ' . quote($id) . ', ' . quote($name) . ', ' . quote($email) . ', ' . quote($text) . ', ' . quote($date) . ', ' . quote($url) . ', UNIX_TIMESTAMP(NOW()), ' . quote($stars) . ')', 1);
        if (!empty($rid)) {
            $rev = query_new('SELECT * FROM `sw_catalog_reviews` WHERE `cid` = ' . $id . ' AND `active` = 1 AND `id`=' . $rid, 1);
            if ($rev !== false) {
                $stars = '<div class="reviews_stars">';
                for ($i = 0; $i < 5; $i++) {
                    $class = ($rev['stars'] > $i) ? 'active_star' : '';
                    $stars .= '<span class="' . $class . '"></span>';
                }
                $stars .= '</div>';
                $html = '<div class="otzivsBlock blockWithTopLine" id="review_' . $rev['id'] . '"><span class="date">' . $rev['date'] . '</span>' . $stars . '<i>' . $rev['text'] . '<b class="name">' . $rev['name'] . '</b></i>' . (!empty($rev['response']) ? '<p>- ' . $rev['response'] . '</p>' : null) . '</div>';
                jQuery('.item-reviews-list')->append(inUTF8($html));
                jQuery('#review_captcha>span')->remove();
                jQuery::evalScript('$("#window_info_bg").trigger("click");');
                jQuery::evalScript(inUTF8('initSysDialog("����� ��������")'));
                jQuery::evalScript('capUpdate("#img_captcha")');
                jQuery::getResponse();
            }
        }
        jQuery::evalScript(inUTF8('initSysDialog("��� ����� �������� �� ������� ��������")'));
    } else {
        jQuery::evalScript(inUTF8('initSysDialog("������� ������ ��� ��������")'));
    }
    jQuery::evalScript('capUpdate("#img_captcha")');
    jQuery::getResponse();
}

function formInit() {
    global $module;
    $tplFile = _MODULS_PACH . $module . '/tpl/' . __FUNCTION__ . _TPL_EXT;
    $html    = file_exists($tplFile) ? file_get_contents($tplFile) : '';
    jQuery('body')->append(inUTF8($html));
    jQuery::getResponse();
}

function itemReview() {
    if (empty($_GET['id'])) jQuery::getResponse();
    
    $id = abs(intval($_GET['id']));
    $uid = _IS_USER ? abs(intval($_SESSION['uid'])) : 0;
    
    if (empty($_POST['name']) || empty($_POST['key']) || empty($_POST['text'])) {
        if (empty($_POST['name'])) jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#uName"),t:"���� ����� ���� ����� ���������",top:65,left:180})'));
        if (empty($_POST['key'])) jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#uKey"),t:"������� ����� �����",top:160,left:190})'));
        if (empty($_POST['text'])) jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#uText"),t:"������� ����� ���������",top:65,left:540})'));
        jQuery::getResponse();
    }
    
    if (empty($_SESSION['key']) || $_SESSION['key'] != $_POST['key']) {
        jQuery::evalScript(inUTF8('initSysDialog("�� �� ����� ���������")'));
        jQuery::getResponse();
    }
    
    $name = filter(trim(in1251($_POST['name'])), 'nohtml');
    $email = filter(trim(in1251($_POST['pochta'])), 'nohtml');
    $text = filter(trim(in1251($_POST['text'])), 'nohtml');
    $url = filter(trim(in1251($_POST['url'])), 'nohtml');
    
    db_query('INSERT INTO `sw_catalog_reviews` (`uid`, `cid`, `name`, `email`, `text`, `url`, `insert`) VALUES ('.quote($uid).', '.quote($id).', '.quote($name).', '.quote($email).', '.quote($text).', '.quote($url).', UNIX_TIMESTAMP(NOW()))');
    
    jQuery::evalScript(inUTF8('reviews.itemAdd({name:"'.htmlspecialchars($name).'",text:"'.htmlspecialchars($text).'"})'));
    jQuery::getResponse();
}

function insertReview() {
    
    if (!empty($_POST['email'])) jQuery::getResponse();
    
    if (empty($_POST['name']) || empty($_POST['key']) || empty($_POST['text']) || empty($_SESSION['key']) || $_SESSION['key'] != $_POST['key']) {
        if (empty($_POST['name']))
        {
             jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#uName"),t:"���� ����� ���� ����� ���������",top:65,left:180})'));
        }
         if (empty($_POST['text'])) 
        {
            jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#uText"),t:"������� ����� ���������",top:65,left:540})'));
        }
       
        if (empty($_POST['key'])) 
        {
            $v1 = rand(1,9);
            $v2 = rand(1,9);
            $_SESSION['key'] = $v1 + $v2;
            jQuery::evalScript("jQuery('#v1').html('".$v1."')");
            jQuery::evalScript("jQuery('#v2').html('".$v2."')");
            jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#uKey"),t:"������� ����� �����",top:160,left:190})'));
        }
        elseif (empty($_SESSION['key']) || $_SESSION['key'] != $_POST['key']) {
            $v1 = rand(1,9);
            $v2 = rand(1,9);
            $_SESSION['key'] = $v1 + $v2;
        jQuery::evalScript("jQuery('#v1').html('".$v1."')");
        jQuery::evalScript("jQuery('#v2').html('".$v2."')");
        jQuery::evalScript("jQuery('#inprogress').css({'display':'none'})"); // ������������ ���� ������ ���������� �� ������
        jQuery::evalScript('reviews.error(2)');
        
        }   
       
        jQuery::evalScript("jQuery('#inprogress').css({'display':'none'})");  // ������������ ���� ������ ���������� �� ������
        jQuery::getResponse();
    }
  
    
 // jQuery::evalScript("jQuery('#inprogress').css({'display':'block'})");//���� � ������ ��� ������� �� ���������� �� ��������� �����
    
    
    
    $name = filter(trim(in1251($_POST['name'])), 'nohtml');
    $email = filter(trim(in1251($_POST['pochta'])), 'nohtml');
    $text = filter(trim(in1251($_POST['text'])), 'nohtml');
    $time = time();
    
    $text = str_replace("\n", '<br />', $text);
    
    db_query('INSERT INTO `sw_reviews` (`active`, `title`, `email`, `text`, `date`, `insert`) VALUES (1, '.quote($name).', '.quote($email).', '.quote($text).', CURDATE(), '.quote($time).')');
            $v1 = rand(1,9);
            $v2 = rand(1,9);
            $_SESSION['key'] = $v1 + $v2;
    jQuery::evalScript("jQuery('#v1').html('".$v1."')");
    jQuery::evalScript("jQuery('#v2').html('".$v2."')");
    jQuery::evalScript(inUTF8('reviews.insert({name:"'.htmlspecialchars($name).'",text:"'.htmlspecialchars($text).'",date:"'.date('d.m.Y', $time).'"})'));
    jQuery::evalScript('initSysDialog("'.inUTF8('�������, ��� ����� ������').'")');
    jQuery::evalScript("jQuery('#inprogress').css({'display':'none'})");
    jQuery::getResponse();
}

function getReviews() {
    global $_GLOBALS, $module;
    
    $data = PageData($module);
    $html = null;
    
    $v1 = rand(1,9);
    $v2 = rand(1,9);
    $_SESSION['key'] = $v1 + $v2;
    
    $_GLOBALS['js_preload'][] = file_exists('modules/reviews/admin/reviews.js')?file_get_contents('modules/reviews/admin/reviews.js'):''; // podklu4enie file js
    
    $on_page = 20;
    $pg = !empty($_GET['pg']) ? abs(intval($_GET['pg'])) : 1;
    $min = $on_page * $pg - $on_page;
    $count = dbone('COUNT(`id`)', '`active` = 1', 'reviews');
    
    $html.= '<div class="mainTextInner">'.stripslashes($data['text']).'<input type="submit" style="margin: 0 auto;display: inherit;margin-bottom: 10px;" id="writeReview" value="�������� ���� �����" /></div>';

    $html.= '<div id="list-reviews">';
    $r = db_query('SELECT `title`, `text`, `reply`, UNIX_TIMESTAMP(`date`) as `insert` FROM `sw_reviews` WHERE `active` = 1 AND `date` <= CURDATE() ORDER BY `date` DESC, `id` DESC LIMIT '.$min.','.$on_page);
    while ($o = mysql_fetch_array($r, MYSQL_ASSOC)) $html.= '<div class="otzivsBlock blockWithTopLine"><span class="date">'.date('d.m.Y', $o['insert']).'</span><i>'.$o['text'].'<b class="name">'.$o['title'].'</b></i>'.(!empty($o['reply']) ? '<p>- '.$o['reply'].'</p>' : null).'</div>';
    $html.= '</div>';
    
    if ($count > $on_page) $html.= PageNavigator($count, $pg, $on_page, '/reviews/op=all&pg=', '/reviews.html');
    
    if (_IS_USER) {
        $r = db_query('SELECT `username`, `email` FROM `sw_users` WHERE `id` = '.quote($_SESSION['uid']));
        if (mysql_num_rows($r) == 0) jQuery::getResponse();
        $u = mysql_fetch_array($r, MYSQL_ASSOC);
    }
    //onclick="javascript:jQuery(\'#inprogress\').css({\'display\':\'block\'})"
      $html.= '<div class="createOtviz blockWithTopLine" id="reviewForm">'
            . '<form action="/reviews/op=insert" onsubmit="return checreviews(this); return false;">'
               . '<input type="text" name="email" class="hidden" >'
              . '<h2>�������� ���� �����</h2>'
               . '<div class="leftPart floatLeft">'
                  . '<p id="uName">���� ���:*<br /><input id="uN" name="name" type="text" value="'.(_IS_USER ? htmlspecialchars($u['username']) : null).'" /></p>'
                  . '<p>E-mail<br /><input name="pochta" type="text" value="'.(_IS_USER ? htmlspecialchars($u['email']) : null).'" /></p>'
                  . '<p id="uKey" class="code"><span id="v1">'.$v1.'</span>+<span id="v2">'.$v2.'</span> = <input id="uK" name="key" type="text" /></p>'
               . '</div>'
            . '<div class="rightPart floatRight">'
              . '<p id="uText">����� ���������:*<br /><textarea id="uT" name="text" cols="1" rows="1"></textarea></p>'
            . '</div>'
            . '<div class="clear"></div>'
            . '<div class="floatRight">'
            . '<span class="gray">����� ��������� ��� ����!</span> <input type="submit" value="���������" />'
            . '</div>'
            . '<div class="clear"></div>'
            . '<div id="inprogress" class="in_progress"></div>'
            . '</form>'
            . '</div>';
    
    $title_addition = ($pg > 1) ? ' (�������� �'.$pg.') / �������� �Studio Floristic�' : ' / �������� �Studio Floristic�';
    
    $_GLOBALS['title'] = $data['title'];
    $_GLOBALS['text'] = $html;
    $_GLOBALS['html_title'] = !empty($data['html_title']) ? $data['html_title'].$title_addition : $data['title'].$title_addition;
    if (!empty($data['keywords'])) $_GLOBALS['keywords'] = $data['keywords'];
    if (!empty($data['description'])) $_GLOBALS['description'] = $data['description'];
    $_GLOBALS['template'] = 'default';
    $_GLOBALS['menu'] = 'contact';
    $_GLOBALS['section'] = 'reviews';
}