<?
if (!defined('SW')) die('���� ������� �� ���������');

include_once('include/jquery/jQuery.php');

switch($op) {
	case 'item': itemReview(); break;
	case 'insert': insertReview(); break;
	default: getReviews(); break;
}

function itemReview() {
	if (empty($_GET['id'])) jQuery::getResponse();
	
	$id = abs(intval($_GET['id']));
	$uid = _IS_USER ? abs(intval($_SESSION['uid'])) : 0;
	
	if (empty($_POST['name']) || empty($_POST['key']) || empty($_POST['text'])) {
		if (empty($_POST['name'])) jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#uName"),t:"���� ����� ���� ����� ���������",top:65,left:180})'));
		if (empty($_POST['key'])) jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#uKey"),t:"������� ����� �����",top:160,left:190})'));
		if (empty($_POST['text'])) jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#uText"),t:"������� ����� ���������",top:65,left:540})'));
		jQuery::getResponse();
	}
	
	if (empty($_SESSION['key']) || $_SESSION['key'] != $_POST['key']) {
		jQuery::evalScript(inUTF8('initSysDialog("�� �� ����� ���������")'));
		jQuery::getResponse();
	}
	
	$name = filter(trim(in1251($_POST['name'])), 'nohtml');
	$email = filter(trim(in1251($_POST['pochta'])), 'nohtml');
	$text = filter(trim(in1251($_POST['text'])), 'nohtml');
	$url = filter(trim(in1251($_POST['url'])), 'nohtml');
	
	db_query('INSERT INTO `sw_catalog_reviews` (`uid`, `cid`, `name`, `email`, `text`, `url`, `insert`) VALUES ('.quote($uid).', '.quote($id).', '.quote($name).', '.quote($email).', '.quote($text).', '.quote($url).', UNIX_TIMESTAMP(NOW()))');
	
	jQuery::evalScript(inUTF8('reviews.itemAdd({name:"'.htmlspecialchars($name).'",text:"'.htmlspecialchars($text).'"})'));
	jQuery::getResponse();
}

function insertReview() {
	if (!empty($_POST['email'])) jQuery::getResponse();
	
	if (empty($_POST['name']) || empty($_POST['key']) || empty($_POST['text'])) {
		if (empty($_POST['name'])) jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#uName"),t:"���� ����� ���� ����� ���������",top:65,left:180})'));
		if (empty($_POST['key'])) jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#uKey"),t:"������� ����� �����",top:160,left:190})'));
		if (empty($_POST['text'])) jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#uText"),t:"������� ����� ���������",top:65,left:540})'));
		jQuery::getResponse();
	}
	
	if (empty($_SESSION['key']) || $_SESSION['key'] != $_POST['key']) {
		jQuery::evalScript('reviews.error(2)');
		jQuery::getResponse();
	}
	
	$name = filter(trim(in1251($_POST['name'])), 'nohtml');
	$email = filter(trim(in1251($_POST['pochta'])), 'nohtml');
	$text = filter(trim(in1251($_POST['text'])), 'nohtml');
	$time = time();
	
	$text = str_replace("\n", '<br />', $text);
	
	db_query('INSERT INTO `sw_reviews` (`active`, `title`, `email`, `text`, `date`, `insert`) VALUES (1, '.quote($name).', '.quote($email).', '.quote($text).', CURDATE(), '.quote($time).')');
	
	jQuery::evalScript(inUTF8('reviews.insert({name:"'.htmlspecialchars($name).'",text:"'.htmlspecialchars($text).'",date:"'.date('d.m.Y', $time).'"})'));
	jQuery::getResponse();
}

function getReviews() {
	global $_GLOBALS, $module;
	
	$data = PageData($module);
	$html = null;
	
	$v1 = rand(1,9);
	$v2 = rand(1,9);
	$_SESSION['key'] = $v1 + $v2;
	
	$on_page = 20;
	$pg = !empty($_GET['pg']) ? abs(intval($_GET['pg'])) : 1;
	$min = $on_page * $pg - $on_page;
	$count = dbone('COUNT(`id`)', '`active` = 1', 'reviews');
	
	if (!empty($data['text'])) $html.= '<div class="mainTextInner">'.stripslashes($data['text']).'</div>';
	
	$html.= '<div id="list-reviews">';
	$r = db_query('SELECT `title`, `text`, `reply`, UNIX_TIMESTAMP(`date`) as `insert` FROM `sw_reviews` WHERE `active` = 1 AND `date` <= CURDATE() ORDER BY `date` DESC, `id` DESC LIMIT '.$min.','.$on_page);
	while ($o = mysql_fetch_array($r, MYSQL_ASSOC)) $html.= '<div class="otzivsBlock blockWithTopLine"><span class="date">'.date('d.m.Y', $o['insert']).'</span><i>'.$o['text'].'<b class="name">'.$o['title'].'</b></i>'.(!empty($o['reply']) ? '<p>- '.$o['reply'].'</p>' : null).'</div>';
	$html.= '</div>';
	
	if ($count > $on_page) $html.= PageNavigator($count, $pg, $on_page, '/reviews/op=all&pg=', '/reviews.html');
	
	if (_IS_USER) {
		$r = db_query('SELECT `username`, `email` FROM `sw_users` WHERE `id` = '.quote($_SESSION['uid']));
		if (mysql_num_rows($r) == 0) jQuery::getResponse();
		$u = mysql_fetch_array($r, MYSQL_ASSOC);
	}
	
	$html.= '<div class="createOtviz blockWithTopLine"><form action="/reviews/op=insert" onsubmit="formSubmit(this); return false;"><input type="text" name="email" class="hidden" ><h2>�������� ���� �����</h2><div class="leftPart floatLeft"><p id="uName">���� ���:*<br /><input name="name" type="text" value="'.(_IS_USER ? htmlspecialchars($u['username']) : null).'" /></p><p>E-mail<br /><input name="pochta" type="text" value="'.(_IS_USER ? htmlspecialchars($u['email']) : null).'" /></p><p id="uKey" class="code">'.$v1.'+'.$v2.' = <input name="key" type="text" /></p></div><div class="rightPart floatRight"><p id="uText">����� ���������:*<br /><textarea name="text" cols="1" rows="1"></textarea></p></div><div class="clear"></div><div class="floatRight"><span class="gray">����� ��������� ��� ����!</span> <input type="submit" value="���������" /></div><div class="clear"></div></form></div>';
	
	$_GLOBALS['title'] = $data['title'];
	$_GLOBALS['text'] = $html;
	$_GLOBALS['html_title'] = !empty($data['html_title']) ? $data['html_title'] : $data['title'].' - '.$_GLOBALS['v_sitename'];
	if (!empty($data['keywords'])) $_GLOBALS['keywords'] = $data['keywords'];
	if (!empty($data['description'])) $_GLOBALS['description'] = $data['description'];
	$_GLOBALS['template'] = 'default';
	$_GLOBALS['menu'] = 'contact';
	$_GLOBALS['section'] = 'reviews';
}

?>