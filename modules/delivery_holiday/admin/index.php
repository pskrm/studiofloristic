<?
	if (!defined('_SWS_ADMIN_CORE')) exit();
	
	$_GLOBALS['create_dir'] = false;
	$_GLOBALS['add'] = true;
	$_GLOBALS['search'] = false;
	$_GLOBALS['on-page'] = 20;
	$_GLOBALS['tiny'] = true;
	$_GLOBALS['date'] = true;
        
    $mod_opt = query_new("SELECT * FROM "._MYSQL_PREFIX.$module.'_options', 1);
    if ($mod_opt===false){
        query_new('INSERT INTO '._MYSQL_PREFIX.$module.'_options (`active`) VALUES (0)');
    }
	
	$_GLOBALS['field']['title']['id'] = 'ID';
	$_GLOBALS['field']['title']['sid'] = '������/���������';
	$_GLOBALS['field']['title']['active'] = '���./����.';
	$_GLOBALS['field']['title']['pos'] = '�������';
	$_GLOBALS['field']['title']['title'] = '������������';
	$_GLOBALS['field']['title']['start'] = '��������� �����';
	$_GLOBALS['field']['title']['end'] = '�������� �����';
	$_GLOBALS['field']['title']['center'] = '������';
	$_GLOBALS['field']['title']['okrayna'] = '�����������';
    $_GLOBALS['field']['title']['min_city'] = '<span style="color:#f00;">���������� �������� �� '.htmlspecialchars($mod_opt['min_city'], ENT_COMPAT | ENT_HTML401, 'cp1251').' ���</span>';
    $_GLOBALS['field']['title']['min_mkad'] = '<span style="color:#f00;">���������� �������� �� '.htmlspecialchars($mod_opt['min_mkad'], ENT_COMPAT | ENT_HTML401, 'cp1251').' ���</span>';
	
	$_GLOBALS['field']['type']['sid'] = 'sid';
	$_GLOBALS['field']['type']['active'] = 'checkbox';
	$_GLOBALS['field']['type']['pos'] = 'position';
	$_GLOBALS['field']['type']['title'] = 'd-time';
	$_GLOBALS['field']['type']['start'] = 'time';
	$_GLOBALS['field']['type']['end'] = 'time';
	$_GLOBALS['field']['type']['center'] = 'text';
	$_GLOBALS['field']['type']['okrayna'] = 'text';
    $_GLOBALS['field']['type']['min_city'] = 'checkbox';
    $_GLOBALS['field']['type']['min_mkad'] = 'checkbox';
	
	$_GLOBALS['field']['db']['sid'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['active'] = "int(1) NOT NULL default '0'";
	$_GLOBALS['field']['db']['pos'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['title'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['start'] = "time default NULL";
	$_GLOBALS['field']['db']['end'] = "time default NULL";
	$_GLOBALS['field']['db']['center'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['okrayna'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['insert'] = "timestamp NOT NULL default CURRENT_TIMESTAMP";
	$_GLOBALS['field']['db']['system'] = "int(1) NOT NULL default '0'";
	$_GLOBALS['field']['db']['min_city'] = "int(1) NOT NULL default '0'";
    $_GLOBALS['field']['db']['min_mkad'] = "int(1) NOT NULL default '0'"; 
        
	$_GLOBALS['field']['table']['id'] = 'text';
	$_GLOBALS['field']['table']['title'] = 'title';
	$_GLOBALS['field']['table']['center'] = 'text';
	$_GLOBALS['field']['table']['okrayna'] = 'text';
	
	$_GLOBALS['field']['value']['active'] = 1;
	
	$_GLOBALS['edit-field'] = array('title', 'start', 'end', 'center', 'min_city', 'okrayna', 'min_mkad');
	$_GLOBALS['add-field'] = array('title', 'start', 'end', 'center', 'okrayna');
	
    $check_tbls_cols = array(
        $module=>$_GLOBALS['field']['db'],
        $module.'_options'=>array(
            'active' => 'tinyint(3) NOT NULL DEFAULT 0',
            'start' => 'date DEFAULT NULL',
            'end' => 'date DEFAULT NULL',
            'min_city' => "int(1) NOT NULL default 0",
            'min_mkad' => "int(1) NOT NULL default 0",
            'min_price' => "int(1) NOT NULL default 0",
            'tab' => "varchar(255) DEFAULT NULL"
        )
    );
    check_tbl($check_tbls_cols);
        
	switch ($op) {
		case 'load-module': initHoliday(); break;
		case 'save-holiday': saveHoliday(); break;
	}
	
	function saveHoliday() {
		$value = array();
		
		if (!empty($_GET['start'])) {
			$value['start'] = trim(in1251($_GET['start']));
			if (preg_match("/^([0-9]{2})\.([0-9]{2})\.([0-9]{4})$/i", $value['start'], $date)) {
				$value['start'] = $date[3].'-'.$date[2].'-'.$date[1];
			} 
		}
		
		if (!empty($_GET['end'])) {
			$value['end'] = trim(in1251($_GET['end']));
			if (preg_match("/^([0-9]{2})\.([0-9]{2})\.([0-9]{4})$/i", $value['end'], $date)) {
				$value['end'] = $date[3].'-'.$date[2].'-'.$date[1];
			}
		}
        $min_city = (!empty($_GET['min_city']))? intval($_GET['min_city']):0;
        $min_mkad = (!empty($_GET['min_mkad']))? intval($_GET['min_mkad']):0;
		$min_price = (!empty($_GET['min_price']))? intval($_GET['min_price']):0;
		$value['tab'] = filter(trim(in1251($_GET['tab'])), 'nohtml');
		$value['active'] = !empty($value['start']) && !empty($value['end']) ? 1 : 0;
		
		$value['start'] = empty($value['start']) ? 'NULL' :quote($value['start']);
		$value['end'] = empty($value['end']) ? 'NULL' :quote($value['end']);
		query_new('UPDATE `sw_delivery_holiday_options` SET '
                        . '`active` = '.$value['active'].', '
                        . '`start` = '.$value['start'].', '
                        . '`end` = '.$value['end'].', '
                        . '`tab` = '.quote($value['tab']).', '
                        . '`min_price` = '.$min_price.', '
                        . '`min_city` = '.$min_city.', '
                        . '`min_mkad` = '.$min_mkad.' WHERE `id` = 1');
		
		unset($value);
		
		jQuery::evalScript(inUTF8('alert("��������� ���������")'));
		jQuery::getResponse();
	}
	
	function initHoliday($sid = 0, $pg = 1, $search = null) {
		global $m, $module;
		
		$search = null;
		$pg = !empty($_GET['pg']) ? abs(intval($_GET['pg'])) : 1;
                
		$mod_opt = query_new("SELECT * FROM "._MYSQL_PREFIX.$module.'_options', 1);
		$html = '<h1><a href="javascript:module.index()">'.$m['title'].'</a></h1>';
		
		$r = db_query('SELECT `start`, `end`, `tab` FROM `sw_delivery_holiday_options` WHERE `id` = 1');
		$h = mysql_fetch_array($r, MYSQL_ASSOC);
		
		if (!empty($h['start'])) {
			if (preg_match("/^([0-9]{4})\-([0-9]{2})\-([0-9]{2})$/i", $h['start'], $date)) {
				$h['start'] = $date[3].'.'.$date[2].'.'.$date[1];
			}
		}
		
		if (!empty($h['end'])) {
			if (preg_match("/^([0-9]{4})\-([0-9]{2})\-([0-9]{2})$/i", $h['end'], $date)) {
				$h['end'] = $date[3].'.'.$date[2].'.'.$date[1];
			}
		}
		
                
                
		$html.= '<form action="/go/m='.$module.'&op=save-holiday" onsubmit="getForm(this); return false;">';
		$html.= '<p>������ ���������: <input id="start" type="text" name="start" value="'.htmlspecialchars($h['start'], ENT_COMPAT | ENT_HTML401, 'cp1251').'"></p>';
		$html.= '<p>����� ���������: <input id="end" type="text" name="end" value="'.htmlspecialchars($h['end'], ENT_COMPAT | ENT_HTML401, 'cp1251').'"></p>';
		$html.= '<p>����� ��� �������: <input type="text" name="tab" value="'.htmlspecialchars($h['tab'], ENT_COMPAT | ENT_HTML401, 'cp1251').'"></p>';
                $html.= '<p>����������� <u>����� ��� ��������� ��������</u>: <input id="min_price" type="text" name="min_price" value="'.htmlspecialchars($mod_opt['min_price'], ENT_COMPAT | ENT_HTML401, 'cp1251').'"></p>';
                $html.= '<p>����������� ����� ��� ���������� �������� �� ������: <input id="min_city" type="text" name="min_city" value="'.htmlspecialchars($mod_opt['min_city'], ENT_COMPAT | ENT_HTML401, 'cp1251').'"></p>';
		$html.= '<p>����������� ����� ��� ���������� �������� �� ����: <input id="min_mkad" type="text" name="min_mkad" value="'.htmlspecialchars($mod_opt['min_mkad'], ENT_COMPAT | ENT_HTML401, 'cp1251').'"></p>';
		$html.= '<p><input type="submit" value="���������"></p>';
		$html.= '</form>';
		
		$html.= '<table id="list-items" class="items"><tr class="title"><th>ID</th><th>������������</th><th>������</th><th>�����������</th><th class="options">��������</th></tr>';
		
		$r = db_query('SELECT `id`, `title`, `center`, `okrayna`, `active`, `system` FROM `sw_'.$module.'` ORDER BY `start` ASC');
		while ($i = mysql_fetch_array($r, MYSQL_ASSOC)) {
			$html.= '<tr id="i'.$i['id'].'">';
			$html.= '<td class="id">'.$i['id'].'</td>';
			$html.= '<td class="title" onclick="initGET(\'/go/m='.$module.'&op=item&id='.$i['id'].'&pg='.$pg.'&query='.htmlspecialchars(urlencode($search), ENT_COMPAT | ENT_HTML401, 'cp1251').'\')">'.$i['title'].'</td>';
			$html.= '<td class="center">'.$i['center'].'</td>';
			$html.= '<td class="okrayna">'.$i['okrayna'].'</td>';
			$html.= '<td class="btn"><div class="active'.($i['active'] != 0 ? '" title="�������"' : ' active-off" title="��������"').' onclick="initGET(\'/go/m='.$module.'&op=active&id='.$i['id'].'\')"></div><div class="edit" title="�������������" onclick="initGET(\'/go/m='.$module.'&op=item&id='.$i['id'].'&pg='.$pg.'&query='.htmlspecialchars(urlencode($search), ENT_COMPAT | ENT_HTML401, 'cp1251').'\')"></div><div title="�������" class="delete'.($i['system'] != 0 ? '-off"' : '" onclick="items.remove('.$i['id'].')"').'></div></td></td>';
			$html.= '</tr>';
		}
		
		$html.= '</table>';
		
		jQuery('#content-data')->html(inUTF8($html));
		jQuery::evalScript('date.load("input#start")');
		jQuery::evalScript('date.load("input#end")');
		jQuery::getResponse();
	}
?>