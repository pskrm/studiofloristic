<?
if (!defined('SW')) die('���� ������� �� ���������');

include_once('include/jquery/jQuery.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

switch($op) {
	case 'restoration': userRestoration(); break;
	case 'activation': userActivation(); break;
	case 'registration': userRegistration(); break;
	case 'recovery': userRecovery(); break;
	case 'soc_login': getSocParams(); break;
	case 'discount': discount(); break;
	default: loginDefault(); break;
}

function discount()
{
    if (empty($_SESSION['user']['id'])) {
        echo '�������������� � �������� ������';
        die;
    }

    $sqlUserDiscount = db_query('SELECT `user_id` FROM `sw_sms_register_discount` 
        WHERE `user_id` = ' . $_SESSION['user']['id'] . ' LIMIT 1');

    $userDiscount = mysql_fetch_assoc($sqlUserDiscount);
    $userDiscount = empty($userDiscount['user_id']) ? '' : $userDiscount['user_id'];

    if (!empty($userDiscount)) {
        $sqlVariable = db_query('SELECT `value` FROM `sw_variables` 
          WHERE `name` = \'message_registartion_discount\' LIMIT 1');
        $messageVariable = mysql_fetch_assoc($sqlVariable);
        $messageVariable = $messageVariable['value'];

        if (!empty($messageVariable)) {
            echo $messageVariable;
        }

        die;
    }

    header('Location: /');
}

function loginDefault(){
    if (empty($_GET['op']) && !empty($_REQUEST['code'])){
        getSocParams();
        if (!empty($_SESSION['last_visitet_page'])){
            $page = $_SESSION['last_visitet_page'];
            unset($_SESSION['last_visitet_page']);
            HeaderPage($page);
        }
    }
    HeaderPage();
}
function getSocParams(){
    global $soc_params;

    $soc = (!empty($_GET['soc']))?strtolower($_GET['soc']):$_COOKIE['soc_name'];

    $server = str_replace('www.','',$_SERVER['SERVER_NAME']);

    $params = $soc_params[$server][$soc];
    if (!empty($params)){
        if(isset($_REQUEST['code'])){
            $code = $_REQUEST['code'];
            $user = array('soc_name'=>$soc);
            switch ($soc){
                case 'vk': $user = vk_login($params,$code,$user); break;
                case 'facebook': $user = facebook_login($params,$code,$user); break;
                case 'odnoklassniki': $user = odnoklassniki_login($params, $code, $user); break;
                case 'mailru': $user = mailru_login($params, $code, $user); break;
                case 'yandex': $user = yandex_login($params, $code, $user); break;
                case 'google': $user = google_login($params, $code, $user); break;
            }
            if (is_array($user)){
                socLogin($user);
            }
            return;
        }
        $code_url = $params['code_url'];
        $code_params = $params['code_params'];
        if(isset($_GET['op']) && $_GET['op']=='soc_login'){
            setcookie('soc_name',$soc,0,'/',_CROSS_DOMAIN);
            $_SESSION['last_visitet_page'] = $_SERVER['HTTP_REFERER'];
            $url = urldecode(http_build_query($code_params));
            jQuery::evalScript('window.location.href="'.$code_url.'?'.$url.'"');
            jQuery::getResponse();
        }
    }
    if (isset($_GET['op'])){
        jQuery::evalScript(inUTF8('initSysDialog("���������� ��������������")'));
        jQuery::getResponse();
    }
    if (isset($_SESSION['last_visitet_page']) && $_SESSION['last_visitet_page']!=''){
        HeaderPage($_SESSION['last_visitet_page']);
    }else{
        HeaderPage();
    }
}
function socLogin($user = array()){
    if (isset($user['soc_id']) && isset($user['soc_name'])){
        $or = (!empty($user['email']))?' OR `email` REGEXP '.quote($user['email']):'';
        $u = query_new('SELECT * FROM sw_users WHERE `'.$user['soc_name'].'` = '.quote($user['soc_id'],false).$or,1);
        setcookie('deb_soc_query','SELECT * FROM sw_users WHERE `'.$user['soc_name'].'` = '.quote($user['soc_id'],false).$or,0,'/',_CROSS_DOMAIN);
        setcookie('deb_soc',print_r($u,1),0,'/',_CROSS_DOMAIN);
        if (empty($u)){
            $name = (!empty($user['name']))?filter(trim($user['name']), 'nohtml'):filter(trim($user['soc_id']));
            $family = filter(trim($user['family']), 'nohtml');
            $patronymic = '';
            $sex = (isset($user['sex']))? $user['sex']:0;
            $email = filter(trim($user['email']), 'nohtml');
            $activation = md5(session_id().time());
            $password = $activation.'_sf_pass';
            $pass = md5($password);
            $phone = (isset($user['phone']))? filter(trim($user['phone']), 'nohtml'): '';
            $sysPhone = preg_replace("/[^0-9]/", '', $phone);
            $subscription = 0;
            $sms = 0;
            $index = 0;
            $city = (isset($user['city']))? filter(trim($user['city']), 'nohtml'): '';
            $home = '';
            $housing = '';
            $hkey = '';
            $country = (isset($user['country']))? filter(trim($user['country']), 'nohtml'): '';
            $street = '';
            $structure = '';
            $apartment = '';
            $username = trim($family.' '.$name.' '.$patronymic);

            query_new('INSERT INTO `sw_users` (`active`,`username`, `email`, `name`, `family`, `patronymic`, `sex`, `pass`, `phone`, `sys-phone`, `subscription`, `sms`, `index`, `city`, `home`, `housing`, `hkey`, `country`, `street`, `structure`, `apartment`, `activation`, `'.$user['soc_name'].'`, `'.$user['soc_name'].'_scope`) VALUES '
                . '('.quote(1,false).','.quote($username).', '.quote($email).', '.quote($name).', '.quote($family).', '.quote($patronymic).', '.quote($sex).', '.quote($pass).', '.quote($phone).', '.quote($sysPhone,false).', '.quote($subscription).', '.quote($sms).', '.quote($index).', '.quote($city).', '.quote($home).', '.quote($housing).', '.quote($hkey).', '.quote($country).', '.quote($street).', '.quote($structure).', '.quote($apartment).', '.quote($activation).', '.quote($user['soc_id'],false).','.quote(serialize($user)).')');
        }else{
            $table_fields = query_new('DESCRIBE `sw_users`',0,'Field');
            $update_str = ' `'.$user['soc_name'].'`='.quote($user['soc_id'],false).', `'.$user['soc_name'].'_scope`='.quote(serialize($user)) . ', ';
            // foreach ($user as $key=>$var){
            //     if (isset($table_fields[$key])){
            //         $update_str .= ' `'.$key.'`='.quote($var).', ';
            //     }
            // }
            query_new('UPDATE sw_users SET '.$update_str.' `active`=1 WHERE id='.$u['id']);
        }
        $u = query_new('SELECT * FROM sw_users WHERE `'.$user['soc_name'].'` = '.quote($user['soc_id'],false).$or,1);
        setcookie('deb_soc2',print_r($u,1),0,'/',_CROSS_DOMAIN);
        $time = time()+(60*60*24*365);
        set_user_session($u);
        set_user_member($u, $time, array('soc_name'=>$user['soc_name'],'soc_id'=>$user['soc_id']));
    }
}
function vk_login($params=array(),$code='',$user=array()){
    if (is_array($params) && count($params)>0 && is_string($code) && $code!='' && is_array($user) && count($user)>0){
        $token_url = $params['token_url'];
        $token_params = $params['token_params'];
        $token_params['code'] = $code;
        $token = get($token_url ,$token_params, true);
        if (isset($token['access_token'])) {
            $user_params = array(
                'uids'         => $token['user_id'],
                'fields'       => 'uid,first_name,last_name,sex',
                'access_token' => $token['access_token']
            );
            $userInfo = get('https://api.vk.com/method/users.get',$user_params, true);
            if (isset($userInfo['response'][0]['uid'])) {
                $userInfo = $userInfo['response'][0];
                $user['email'] = $token['email'];
                $user['name'] = in1251($userInfo['first_name']);
                $user['family'] = in1251($userInfo['last_name']);
                $user['soc_id'] = $userInfo['uid'];
                if (isset($userInfo['sex'])){
                    $user['sex'] = (intval($userInfo['sex'])==2)?1:2;
                }
                return $user;
            }
        }
    }
    return false;
}
function facebook_login($params=array(),$code='',$user=array()){
    if (is_array($params) && count($params)>0 && is_string($code) && $code!='' && is_array($user) && count($user)>0){
        $token_url = $params['token_url'];
        $token_params = $params['token_params'];
        $token_params['code'] = $code.'#_=_';
        $token = get($token_url ,$token_params, false);
        $tmp = explode('&', $token);
        foreach ($tmp as $row){
            if (stripos($row,'access_token')!==false){
            	//fix old token to new at 02.06.2017
                $token=json_decode(str_replace('access_token=', '', $row), true) ;
            }
        }
        if (isset($token['access_token'])) {
            $user_params = array('access_token'=>$token['access_token'], 'fields' => 'id,first_name,last_name,email');
            $userInfo = get('https://graph.facebook.com/v2.9/me',$user_params, true);
            if (isset($userInfo['id'])){
                $user['email'] = $userInfo['email'];
                $user['name'] = in1251($userInfo['first_name']);
                $user['family'] = in1251($userInfo['last_name']);
                $user['soc_id'] = $userInfo['id'];
                return $user;
            }
        }
    }
    return false;
}
function odnoklassniki_login($params=array(),$code='',$user=array()){
    if (is_array($params) && count($params)>0 && is_string($code) && $code!='' && is_array($user) && count($user)>0){
        $token_url = $params['token_url'];
        $token_params = $params['token_params'];
        $token_params['code'] = $code;
        $token = post($token_url ,$token_params, true);
        if (isset($token['access_token'])) {
            $sign = md5("application_key=".$params['pablic_key']."format=jsonmethod=users.getCurrentUser" . md5($token['access_token'].$token_params['client_secret']));
            $user_params = array(
                'method'          => 'users.getCurrentUser',
                'access_token'    => $token['access_token'],
                'application_key' => $params['pablic_key'],
                'format'          => 'json',
                'sig'             => $sign
            );
            $userInfo = get('http://api.odnoklassniki.ru/fb.do',$user_params, true);
            if (isset($userInfo['uid'])) {
                $user['email'] = '';
                $user['name'] = in1251($userInfo['first_name']);
                $user['family'] = in1251($userInfo['last_name']);
                $user['soc_id'] = $userInfo['uid'];
                if (isset($userInfo['gender'])){
                    $user['sex'] = ($userInfo['gender']=='male')?1:2;
                }
                if (isset($userInfo['location'])){
                    $user['country'] = (isset($userInfo['location']['countryName']))?in1251($userInfo['location']['countryName']):'';
                    $user['city'] = (isset($userInfo['location']['city']))?in1251($userInfo['location']['city']):'';
                }
                return $user;
            }
        }
    }
    return false;
}
function mailru_login($params=array(),$code='',$user=array()){
    if (is_array($params) && count($params)>0 && is_string($code) && $code!='' && is_array($user) && count($user)>0){
        $token_url = $params['token_url'];
        $token_params = $params['token_params'];
        $token_params['code'] = $code;
        $token = post($token_url ,$token_params, true);
        if (isset($token['access_token'])) {
            $sign = md5("app_id=".$token_params['client_id']."method=users.getInfosecure=1session_key=" .$token['access_token'].$token_params['client_secret']);
            $user_params = array(
                'method'          => 'users.getInfo',
                'session_key'    => $token['access_token'],
                'secure'          => '1',
                'app_id'          => $token_params['client_id'],
                'sig'             => $sign
            );
            $userInfo = get('http://www.appsmail.ru/platform/api',$user_params, true);
            if (isset($userInfo[0]['uid'])) {
                $userInfo = $userInfo[0];
                $user['email'] = $userInfo['email'];
                $user['name'] = in1251($userInfo['first_name']);
                $user['family'] = in1251($userInfo['last_name']);
                $user['soc_id'] = $userInfo['uid'];
                if (isset($userInfo['sex'])){
                    $user['sex'] = (intval($userInfo['sex'])==2)?1:2;
                }
                if (isset($userInfo['location'])){
                    $user['country'] = (isset($userInfo['location']['country']['name']))?in1251($userInfo['location']['country']['name']):'';
                    $user['city'] = (isset($userInfo['location']['city']['name']))?in1251($userInfo['location']['city']['name']):'';
                }
                return $user;
            }
        }
    }
    return false;
}
function yandex_login($params=array(),$code='',$user=array()){
    if (is_array($params) && count($params)>0 && is_string($code) && $code!='' && is_array($user) && count($user)>0){
        $token_url = $params['token_url'];
        $token_params = $params['token_params'];
        $token_params['code'] = $code;
        $token = post($token_url ,$token_params, true);
        if (isset($token['access_token'])) {
            $user_params = array(
                'oauth_token'    => $token['access_token'],
                'format'          => 'json',
            );
            $userInfo = get('https://login.yandex.ru/info',$user_params, true);
            if (isset($userInfo['id'])) {
                $user['email'] = (isset($userInfo['default_email']))?$userInfo['default_email']:'';
                $user['name'] = in1251($userInfo['first_name']);
                $user['family'] = in1251($userInfo['last_name']);
                $user['soc_id'] = $userInfo['id'];
                if (isset($userInfo['sex'])){
                    $user['sex'] = ($userInfo['sex']=='male')?1:2;
                }
                return $user;
            }
        }
    }
    return false;
}
function google_login($params=array(),$code='',$user=array()){
    if (is_array($params) && count($params)>0 && is_string($code) && $code!='' && is_array($user) && count($user)>0){
        $token_url = $params['token_url'];
        $token_params = $params['token_params'];
        $token_params['code'] = $code;
        $token = post($token_url ,$token_params, true);
        if (isset($token['access_token'])) {
            $user_params = array(
                'oauth_token'    => $token['access_token'],
            );
            $userInfo = get('https://www.googleapis.com/oauth2/v1/userinfo',$user_params, true);
            if (isset($userInfo['id'])) {
                $user['email'] = (isset($userInfo['email']))?$userInfo['email']:'';
                $user['name'] = in1251($userInfo['given_name']);
                $user['family'] = in1251($userInfo['family_name']);
                $user['soc_id'] = $userInfo['id'];
                if (isset($userInfo['gender'])){
                    $user['sex'] = ($userInfo['gender']=='male')?1:2;
                }
                return $user;
            }
        }
    }
    return false;
}


function userRecovery()
{
	global $_GLOBALS;

	if (empty($_POST['email']))
    {
		jQuery::evalScript('login.result(5)');
		jQuery::getResponse();
	}

	$email = filter(trim(in1251($_POST['email'])), 'nohtml');

    $is_email = preg_match("/^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-]+)*\.([a-zA-Z]{2,6})$/", $email);

    $is_phone = false; //

    if ($is_email)
    {
	    $result = db_query('SELECT * FROM `sw_users` WHERE `email` = '.quote($email));
	    if (mysql_num_rows($result) == 0)
        {
		    jQuery::evalScript('login.result(6)');
		    jQuery::getResponse();
	    }
    }
    else
    {
        // ��������� ���������, ��� ��� �������. ������ +7 � 8 �� ������, ���� ��� ���� �������, � ������ �� ������, ������� � 9...
        $sysPhone = trim(preg_replace("/[^0-9]/", '', $email));
        $first_letter  = substr($sysPhone, 0, 1);
        if (in_array($first_letter, array('7', '8')))
        {
            $sysPhone = substr($sysPhone, 1);
        }

        // ��������� � �� ����������� ��������
        // ���� ���� - ����������� �����
        $result = db_query("SELECT * FROM sw_users WHERE `sys-phone` LIKE '%".$sysPhone."%' LIMIT 1" );
        if (mysql_num_rows($result) == 0)
        {
            jQuery::evalScript('login.result(61)');
            jQuery::getResponse();
        }
        // ������� � ���� ����, ����������
        $is_phone = true;
    }

    // ������������ ������, �� �������� ��� �� e-mail
	$user = mysql_fetch_array($result, MYSQL_ASSOC);
	$activation = md5($user['id'].time()._SITEKEY);

    // �������� ��������� � user-record
	db_query('UPDATE `sw_users` SET `active` = 1, `activation` = '.quote($activation).' WHERE `id` = '.quote($user['id']));

    $time = time()+(60*60*24*365);

    if (is_string($activation) && $activation!=='' && $activation!==null)
    {
        //setcookie('member', $activation, $time,'/',_CROSS_DOMAIN);
    }

    define('_IS_USER', is_user());

    // ����� ���� �����: ������������ �� ����-����� �����������
    // ������, ���� ���� ��� �������������� �� ������ ���������� e-mail (������������������ � ����� ��)
    // �� ������� ��� ������
    if ($is_email && !empty($user['email']))
    {
        $host_name = $_SERVER['SERVER_NAME'];
        $msg = '<p style="text-align:center"><img src="http://'.$host_name.'/i/logo-studio-floristic-green.png" alt="'.$_GLOBALS['v_sitename'].'"></p>'.PHP_EOL;
        $msg.= '<h3>������������, '.$user['username'].'!</h3>'.PHP_EOL;
        $msg.= '<p>��� �����: <b>'.$user['email'].'</b></p>'.PHP_EOL;
        $msg.= '<p>������� ������ �� ������ � ������ ��������.</p>'.PHP_EOL;
        $msg.= '<p>��� ����� �� ���� ��������� �� ������: <a href="http://'.$host_name.'/login/op=restoration&key='.$activation.'&lost=1" target="_blank">��������������</a></p>'.PHP_EOL;
        $msg.= '<p>� ���������, ������� ��������-�������� <a href="http://'.$host_name.'" target="_blank">Studio Floristic</a></p>'.PHP_EOL;

        $headers = "Content-type: text/html; charset=windows-1251 \r\nFrom: ".$host_name."<".$_GLOBALS['v_email_admin'].">\r\n";
        $theme = "�������������� ������ �� ����� �".$_GLOBALS['v_sitename']."�";

        $mail = new PHPMailer;
        try {
            $mail->setFrom($_GLOBALS['v_email_admin'], $host_name);
            $mail->addAddress($email);
            $mail->Subject = $theme;
            $mail->isHTML(true);
            $mail->CharSet = 'windows-1251';
            $mail->Body = '<html>'.$msg.'</html>';
            $mail->AltBody = strip_tags($msg);
            $mail->send();            
        } catch (Exception $e) {
            unset($e);
        }

        $smsQueue = new SmsQueue();
        $smsQueue->storeMailToFile(array('subject' => $theme,'body' => $msg, 'headers' => $headers));

        jQuery::evalScript('login.result(8)');
        jQuery::getResponse();
    }
    elseif($is_phone) // ���� �� �� ������ ����� �������� ��� �������������� ������, ������ ��� SMS � �������
    {
        if(!class_exists('SmsQueue'))
        {
            include_once '../../system/smsqueue.php';
        }

        $params = array(
            'msg_type' => SmsQueue::USER_PASSWORD_RECOVERY,
            'uid'=> $user['id']
        );

        $smsQueue = new SmsQueue();
        $smsQueue->addMessageInQueue($params);

        // � ����������� ��� � ���, ��� ��� ������� SMS
        jQuery::evalScript('login.result(81)');
        jQuery::getResponse();
    }
}

function userRestoration()
{
	global $_GLOBALS;
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if (!empty($_POST['new-pass']) || !empty($_POST['ret-pass']))
        {
            $newPass = filter(trim(in1251($_POST['new-pass'])), 'nohtml');
            $retPass = filter(trim(in1251($_POST['ret-pass'])), 'nohtml');
            $md5Pass = md5($newPass);
            $uid = abs(intval($_SESSION['uid']));

            db_query('UPDATE `sw_users` SET `pass` = '.quote($md5Pass).' WHERE `id` = '.quote($uid));

            $u = query_new('SELECT * FROM sw_users WHERE id = '.quote($uid),1);
            $_SESSION['user'] = array('id'=>$uid, 'login'=>$_SESSION['user']['login'], 'pass'=>$md5Pass, 'activation'=>$u['activation']);
            $time = time()+(60*60*24*365);
            if (is_string($u['activation']) && $u['activation']!=='' && $u['activation']!==null)
            {
                setcookie('member', $u['activation'], $time,'/',_CROSS_DOMAIN);
            }

            $activation = md5($u['id'].time()._SITEKEY);

            // �������� ��������� � user-record
            db_query('UPDATE `sw_users` SET `active` = 1, `activation` = '.quote($activation).' WHERE `id` = '.quote($u['id']));
            header('Location: /');
        }
    }

	if (empty($_GET['key'])) HeaderPage(_PAGE_ERROR404);

	$key = filter(trim($_GET['key']), 'nohtml');

	$r = db_query('SELECT * FROM `sw_users` WHERE `activation` = '.quote($key));
	if (mysql_num_rows($r) != 0)
    {
		$u = mysql_fetch_array($r, MYSQL_ASSOC);
		set_user_session($u);
        $time = time()+(60*60*24*365);
        set_user_member($u,$time);
		db_query('UPDATE `sw_users` SET `active` = 1 WHERE `id` = '.quote($u['id']));

        if ($_GET['lost'] == 1) {
            $html = '<style>#passRestore{padding:16px;}label{width:120px;margin-top:5px;}.msg{color:red;font-size:16px;margin:0 15px;}input[type="password"]{width:235px!important;font-size:12px!important;padding:5px!important;margin-top:4px;}input[type="submit"]{margin-top:10px;}</style><div class="msg"></div><form method="POST" id="passRestore"><label for="new-pass">����� ������*:</label><br><input type="password" id="new-pass" name="new-pass" required><br><label for="ret-pass">��������� ������*:</label><br><input type="password" id="ret-pass" name="ret-pass" required><br><input type="submit" value="���������"></form><script>function submitForm(){$("#content-data form").submit()}$("#content-data form input[type=\'submit\']").on("click",function(e){e.preventDefault();if($(this).siblings("#new-pass").val()==""||$(this).siblings("#ret-pass").val()==""){$(".msg").text("������� ������")}else{if($(this).siblings("#new-pass").val()!=$(this).siblings("#ret-pass").val()){$(".msg").text("������ �� ���������");}else{login.result(12);setTimeout(submitForm, 1000);}}});</script>';
        } else {
            $html = '<p>�� ������������, ����� 5 ������ ���������� ��������������� � ������ �������.<meta http-equiv="refresh" content="5; url=/office/op=my"></p>';
        }
	}
    else
    {
        $html = '<p>���� �� ������</p>';
    }

	$_GLOBALS['title'] = '�������������� ������';
	$_GLOBALS['text'] = $html;
	$_GLOBALS['html_title'] = $_GLOBALS['title'].' - '.$_GLOBALS['v_sitename'];
}


function SendRegisterCodeSMS($recipient_phone)
{
    $smsCode = mt_rand(1111, 9999);
    $_SESSION['smscode'] = $smsCode;

    if (!class_exists('SmsQueue')) {
        include_once 'system/smsqueue.php';
    }
    $smsQueue = new SmsQueue();

    $params = array(
        'text' => 'StudioFloristic.ru ��� ��� ������������� '.$smsCode.'. ���� ������ - 2% (������������ - 23%). ����� ����������! 88003331291',
        'msg_type' => SmsQueue::REGISTRATION_EVENT,
        'sender' => htmlspecialchars(stripslashes('SF')),
        'destination' => $recipient_phone
    );

    $smsQueue->addMessageInQueue($params);

    # ��������� HTML-����� ��� ������ � ��������� ������, ���� ����� ����� ������ ��� ������������� �����������
    $html = '<script language="JavaScript">
                function showResendBtn (){
                    jQuery("#resend_sms").fadeIn("fast");
                }
                function hideResendBtn(){
                    jQuery("#resend_sms").hide();
                }
                function countDown(second,endMinute,endHour,endDay,endMonth,endYear) {
                var now = new Date();
                second = (arguments.length == 1) ? second + now.getSeconds() : second;
                endYear =  typeof(endYear) != \'undefined\' ?  endYear : now.getFullYear();            
                endMonth = endMonth ? endMonth - 1 : now.getMonth();  //����� ������ ���������� � 0   
                endDay = typeof(endDay) != \'undefined\' ? endDay :  now.getDate();         
                endHour = typeof(endHour) != \'undefined\' ?  endHour : now.getHours();
                endMinute = typeof(endMinute) != \'undefined\' ? endMinute : now.getMinutes();   
                //��������� ������� � �������� ���� (������ ���������� ����� ��� ������ 1�.) 
                var endDate = new Date(endYear,endMonth,endDay,endHour,endMinute,second+1); 
                var interval = setInterval(function() { //��������� ������ � ���������� 1 �������
                    var time = endDate.getTime() - now.getTime();
                    if (time < 0) {                      //���� �������� ���� ������ �������
                        clearInterval(interval);
                        alert("�������� ����!");            
                    } else {            
                        var days = Math.floor(time / 864e5);
                        var hours = Math.floor(time / 36e5) % 24; 
                        var minutes = Math.floor(time / 6e4) % 60;
                        var seconds = Math.floor(time / 1e3) % 60;  
                        
                        document.getElementById(\'afss_mins_bv\').innerHTML = "<span id=\'m_min\'>" +minutes+ \' ���. </span>\'+ "<span id=\'m_sec\'>" +seconds+\' ���. </span>\';
                        if (!seconds && !minutes) {              
                            clearInterval(interval);
                             jQuery("#m_min").hide();       
                             jQuery("#m_sec").hide();
                             jQuery("#m_text").hide();
                             showResendBtn();
                        }           
                     }
                                    now.setSeconds(now.getSeconds() + 1); //����������� ������� ����� �� 1 �������
                    }, 1000);
                }
                countDown(120);
                 jQuery("#resend_sms").click(function(){
                     jQuery("#m_text").show();
                      countDown(120);
                      jQuery("#resend_sms").hide();
                 });
            </script>'
        .  '<div id="register_cont" class="tb_to_body  just_btn_close  window_info"><div class="register_text_info">'
        . '<div class="closeNew abort "></div>'
        . '<div class="text_block_cont"><p>����������� �����������.<br>�� ��� ������� ��������� ��� �������������.'
        . '<br/>����������, ������� ��� �����:</p><br/>'
        . '<form action="/login/op=activation" onsubmit="formSubmit(this); return false;">'
        . '<input name="code_sms" type="text" />&nbsp;&nbsp;'
        . '<input name="sysPhone" type="hidden" value="'.$recipient_phone.'"/>'
        . '<input class="green-grd" type="submit" /><p><br/>���������� ���������. �������� ��� ����� ������ �� 2� �����.<br/> <span id="m_text">��������� ��� �� ������ ��������� ����� </span><span id="afss_mins_bv"></span>&nbsp;&nbsp;'
        . '<input id="resend_sms" style="display: none;" class="green-grd" type="button" onclick="initGET(\'/login?op=activation&task=sendsms\')" value="��������" style="background:none; border:1px solid #eee; color:#777;" /></p>'
        . '</form></div>'
        . '</div></div>';

    return($html);
}

function userActivation()
{
    global $_GLOBALS;

    if (isset($_GET['task']) && $_GET['task'] == 'sendsms')
    {
        $html = SendRegisterCodeSMS($_SESSION['sysPhone']);
        jQuery::evalScript('jQuery("#register_cont").remove()');
        jQuery('body')->append(inUTF8($html));
        jQuery::evalScript('windows_info(\'register_cont\',\'closeNew\')');
        jQuery::getResponse();
    }

    if (empty($_GET['key']) && !isset($_POST['code_sms']))
    {
        HeaderPage(_PAGE_ERROR404);
    }

    $time = time()+(60*60*24*365);

    if (!$_POST['code_sms'])
    {
        $key = filter(trim($_GET['key']), 'nohtml');
        $u   = query_new('SELECT * FROM `sw_users` WHERE `activation` IS NOT NULL AND `activation` = ' . quote($key), 1);
    }
    else
    {
        if (!empty($_SESSION['smscode']) && !empty($_POST['code_sms']))
        {
            if (trim($_SESSION['smscode']) != trim($_POST['code_sms']))
            {
                $html = '<div class="register_text_info">'
                        . '<script language="JavaScript">
                          
                                function chTimer(){
                                    if (jQuery("#afss_mins_bv").html() == ""){
                                        countDown(120);
                                    }
                                }         
                           </script>
                        '
                        . '<div class="closeNew abort "></div>'
                        . '<div class="text_block_cont"><p>�������� ���.<br>'
                        . '<br/>����������, ������� ��� ������ ��� ��������� ��������� �������� ����</p><br/>'
                        . '<form action="/login/op=activation" onsubmit="formSubmit(this); return false;">'
                        . '<input name="code_sms" type="text" />&nbsp;&nbsp;'
                        . '<input name="sysPhone" type="hidden" value="' . $_POST['sysPhone'] . '"/>'
                        . '<input class="green-grd" type="submit" onclick="chTimer();" /><p><br/><span id="m_text"> ��������� �������� ��� � ����� ��������� �������� �����</span>&nbsp;<span id="afss_mins_bv"></span>'
                        . '<input id="resend_sms" class="green-grd" type="button" style="display: none;" onclick="initGET(\'/login?op=activation&task=sendsms\')" value="��������" style="background:none; border:1px solid #eee; color:#777;" /></p>'
                        . '</form></div>'
                        . '</div>';
                jQuery('#register_cont')->html(inUTF8($html));
                jQuery::evalScript('windows_info(\'register_cont\',\'closeNew\')');
                jQuery::getResponse();
            }
            else
            {
                $u    = query_new('SELECT * FROM `sw_users` WHERE `sys-phone` = ' . $_POST['sysPhone'], 1);
                query_new('UPDATE `sw_users` SET `active` = 1, `reg_url` = "" WHERE `id` = ' . quote($u['id']));
                set_user_session($u);
                set_user_member($u, $time);
                $html = '<div class="register_text_info">'
                        . '<div class="closeNew abort "></div>'
                        . '<div class="text_block_cont"><p>��������� ������ ������!<br>'
                        . '<br/>����������, ������� �� ���� ��� ����� ������ � �������.</p><br/>'
                        . '</div>'
                        . '</div>';

                addUserOfSendDiscount($u);

                jQuery('#register_cont')->html(inUTF8($html));
                jQuery::evalScript('windows_info(\'register_cont\',\'closeNew\')');
                jQuery::evalScript('window.location = "/office/op=my"');
                jQuery::getResponse();
            }
        }
    }

    if ($u!==false)
    {
        addUserOfSendDiscount($u);

        set_user_session($u);
        set_user_member($u,$time);
        query_new('UPDATE `sw_users` SET `active` = 1, `reg_url` = "" WHERE `id` = '.quote($u['id']));
        $to = ($u['reg_url']!='')
                ? '�� �������� � ������� ����������� �����������<meta http-equiv="refresh" content="5; '.$u['reg_url'].'">'
                : '� ���� ������ �������<meta http-equiv="refresh" content="5; url=/office/op=my">';

        $_GLOBALS['text'] = '<p>������� ������ �������������. ����� 5 ������ �� ������ �������������� '.$to.'</p>';
    }
    else
    {
        $_GLOBALS['text'] = '<p>���� �� ������</p>';
    }

    $_GLOBALS['title'] = '��������� ������� ������';
    $_GLOBALS['html_title'] = $_GLOBALS['title'].' - '.$_GLOBALS['v_sitename'];
}

function userRegistration()
{
    global $_GLOBALS;

    // �������� ���������� ������������ �����
    if (empty($_POST['name']) || empty($_POST['sex']) || empty($_POST['email']) || empty($_POST['password']) || empty($_POST['pass-ret']))
    {
        jQuery::evalScript('login.result(1)');
        jQuery::getResponse();
    }

    // �������� ���������� �������
    if ($_POST['password'] != $_POST['pass-ret'])
    {
        jQuery::evalScript('login.result(2)');
        jQuery::getResponse();
    }

    // �������� �������� ����/�������� � ��������(���� ����)
    $email_phone = filter(trim(in1251($_POST['email'])), 'nohtml');
    $phone = filter(trim(in1251($_POST['phone'])), 'nohtml');

    // ��������� ��������� �� ����
    $is_Email = preg_match("/^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-]+)*\.([a-zA-Z]{2,6})$/", $email_phone);

    // ���� ���� ��-�����-�� �������� �� �������� ������ �����������, � �� ����������� ���, ��
    // ��� ��������� �������� ����� e-mail ������ �� ��������, � �.�. � ������, �� �� ����� ���� ������ ���������
    $requestForReActivationByEmail = false;

    if($is_Email)// ���� ���� ��
    {
        // ��������� �� ������� ������������ ���� � ��
        // ���� ���� ��� ���������������� - �����������

        // ��� ����� ��������� ��-�������.
        // ���� ���� �������� ������������ � ����� e-mail, �� ����� ���������� ��� ��������������
        // ��� ����������� �� �������� �������������� ������

        // ��������, ���� �� �������� ������������ � ����� e-mail
        if (dbone('COUNT(`id`)', '`email` = '.quote($email_phone).' AND `active` = 1', 'users') > 0)
        {
            // �������� ��� � javascript ��� ����� �������� ���� ��� ����������. ���� ��� ��� e-mail, �� �� ������
            // ��������������. ���� �� �� ������� ������, �� �� ������ ������������ ��� �� �����-�� ��������

            jQuery::evalScript('login.result(30)');
            jQuery::getResponse();
        }

        // ��������, ���� �� ���������� ������������ � ����� e-mail

        if (dbone('COUNT(`id`)', '`email` = '.quote($email_phone).' AND `active` = 0', 'users') > 0)
        {
            $requestForReActivationByEmail = true;
            $email = $email_phone;
        }
        else // ����� ������
        {
            $email = $email_phone;
        }
    }
    else //���� ��� �� ����, �� ��� ����� ��������(�� �� ��� ������):)
    {
        $sysPhone = trim(preg_replace("/[^0-9]/", '', $email_phone));
        $first_letter  = substr($sysPhone, 0, 1);
        if (in_array($first_letter, array('7', '8')))
        {
            $sysPhone = substr($sysPhone, 1);
        }

        // ��������� � �� ����������� ��������
        // ���� ���� - ����������� �����
        $activeUs = query_new("SELECT `active` FROM sw_users WHERE `sys-phone`=".quote($sysPhone,false),1);
        if(!empty($activeUs))
        {
            if($activeUs['active'] == 1)
            {
                jQuery::evalScript('login.result(11)');
                jQuery::getResponse();
            }
            else
            {
                $html = SendRegisterCodeSMS($sysPhone); // �������� �� SMS ��� ������������� �����������
                jQuery('body')->append(inUTF8($html));
                jQuery('div#darkBack')->trigger('click');
                jQuery('div#window_info_bg')->trigger('click');
                jQuery::evalScript('windows_info(\'register_cont\',\'closeNew\')');
                jQuery::getResponse();
            }
        }
        else // ���� ��� - ������ ������� � ���������� ���� �������� ��� ��, ���� ������ ������(��� ��� - ������ ���� �������)
        {
            $phone =  $email_phone;
            $email = '';
        }
        $requestForReActivationByEmail = false;
    }

    $name = filter(trim(in1251($_POST['name'])), 'nohtml');
    $family = filter(trim(in1251($_POST['family'])), 'nohtml');
    $patronymic = filter(trim(in1251($_POST['patronymic'])), 'nohtml');
    $sex = abs(intval($_POST['sex']));
    $password = filter(trim(in1251($_POST['password'])), 'nohtml');
    $reg_url = $_SERVER['HTTP_REFERER'];
    $subscription = !empty($_POST['subscription']) ? 1 : 0;
    $sms = !empty($_POST['sms']) ? 1 : 0;
    $index = filter(trim(in1251($_POST['index'])), 'nohtml');
    $city = filter(trim(in1251($_POST['city'])), 'nohtml');
    $home = filter(trim(in1251($_POST['home'])), 'nohtml');
    $housing = filter(trim(in1251($_POST['housing'])), 'nohtml');
    $hkey = filter(trim(in1251($_POST['hkey'])), 'nohtml');
    $country = filter(trim(in1251($_POST['country'])), 'nohtml');
    $street = filter(trim(in1251($_POST['street'])), 'nohtml');
    $structure = filter(trim(in1251($_POST['structure'])), 'nohtml');
    $apartment = filter(trim(in1251($_POST['apartment'])), 'nohtml');
    $username = trim($family.' '.$name.' '.$patronymic);

    if($is_Email)
    {
        $sysPhone = preg_replace("/[^0-9]/", '', $phone);
        $first_letter  = substr($sysPhone, 0, 1);
        if (in_array($first_letter, array('7', '8')))
        {
            $sysPhone = substr($sysPhone, 1);
        }
    }

    if (empty($_SESSION['send_mail_registration'])) {
        $pass = md5($password);
        $activation = md5(session_id().time());

        if ($requestForReActivationByEmail == false)
        {
            db_query('INSERT INTO `sw_users` (`username`, `email`, `name`, `family`, `patronymic`, `sex`, `pass`, `phone`, `sys-phone`, `subscription`, `sms`, `index`, `city`, `home`, `housing`, `hkey`, `country`, `street`, `structure`, `apartment`, `activation`, `reg_url`) '
                . 'VALUES ('.quote($username).', '.quote($email).', '.quote($name).', '.quote($family).', '.quote($patronymic).', '.quote($sex).', '.quote($pass).', '.quote($phone,false).', '.quote($sysPhone,false).', '.quote($subscription).', '.quote($sms).', '.quote($index).', '.quote($city).', '.quote($home).', '.quote($housing).', '.quote($hkey).', '.quote($country).', '.quote($street).', '.quote($structure).', '.quote($apartment).', '.quote($activation).', '.quote($reg_url).')');
        }
        else
        {
            // �������� ��������� � ������� ��� ���� ������ �� email
            db_query('UPDATE `sw_users` SET `activation` = '.quote($activation).' WHERE `email` = '.quote($email));
        }

        $host_name = $_SERVER['SERVER_NAME'];

        if($is_Email)// ������������� ������ �� ����
        {
            $link = 'http://'.$host_name.'/login/op=activation&key='.$activation;
            $msg = '<p style="text-align:center"><img src="http://'.$host_name.'/i/logo-studio-floristic-green.png" alt="'.$_GLOBALS['v_sitename'].'"></p>'.PHP_EOL;
            $msg.= '<h3>������������, '.$username.'!</h3>'.PHP_EOL;
            $msg.= '<p>���������� ��� �� ����������� � �������� ������� � �������� <a href="http://'.$host_name.'/" target="_blank">StudioFloristic.ru</a></p>'.PHP_EOL;
            $msg.= '<p><b>��� ������������� ����������� �������� �� ���� ������:</b></p>'.PHP_EOL;
            $msg.= '<p><a href="'.$link.'" target="_blank">'.$link.'</a></p>'.PHP_EOL;
            $msg.= '<p>������������������ ������������ �������� �������������� ������, ��������� � ������ ��� ���������� �������� � �������� �������!</p>'.PHP_EOL;
            $msg.= '<h4>��� e-mail ��� ����� � ������ �������: '.$email.'</h4>'.PHP_EOL;
            $msg.= '<h4>��� ������: '.$password.'</h4>'.PHP_EOL;
            $msg.= '<p>�������� ��������������� ������ �� ������ ������ � ����� <a href="http://'.$host_name.'/login/op=authorization&login='.$email.'&password='.$password.'&goto=office" target="_blank">������ ��������</a>.</p>'.PHP_EOL;
            $msg.= '<p>�� ���� �������� ����������� �� ��������: 8 800 333-12-91 (��������� �� ������), <strong>�������������</strong>.</p>'.PHP_EOL;
            $msg.= '<p><b>������ ��� � ����� ������� ������� � �����������! ������� ��� �������!</b></p>'.PHP_EOL;
            $msg.= '<p>C ���������, ��������-������� <a href="http://'.$host_name.'/" target="_blank">StudioFloristic.ru</a></p>'.PHP_EOL;

            $subject = '������������� ����������� �� ����� �'.$_GLOBALS['v_sitename'].'�';

            $mail = new PHPMailer;
            try {
                $mail->setFrom($_GLOBALS['v_email_admin'], $host_name);
                $mail->addAddress($email);
                $mail->Subject = $subject;
                $mail->isHTML(true);
                $mail->CharSet = 'windows-1251';
                $mail->Body = '<html>'.$msg.'</html>';
                $mail->AltBody = strip_tags($msg);
                $mail->send();
            } catch (Exception $e) {
                unset($e);
            }
                                   
            $smsQueue = new SmsQueue();
            $smsQueue->storeMailToFile(array('subject' => $subject,'body' => $msg, 'headers' => $headers));

            $html = '<div id="register_cont" class="tb_to_body"><div class="register_text_info">'
                . '<div class="closeNew abort "></div>'
                . '<div class="text_block_cont"><h3>����������� �����������</h3>'
                . '<p>�� ��� email ���� ���������� ������ ��� ������������� �����������.</p>'
                . '<p>����������, �������� �� ������, ��������� � ������</p></div>'
                . '<h3>���������� ��� :)</h3>'
                . '</div></div>';
        } else {
            $html = SendRegisterCodeSMS($sysPhone);
            $_SESSION['sysPhone'] = $sysPhone;
        }

        $_SESSION['send_mail_registration'] = 1;
    }

    jQuery('body')->append(inUTF8($html));
    jQuery('div#darkBack')->trigger('click');
    jQuery('div#window_info_bg')->trigger('click');
    jQuery::evalScript('windows_info(\'register_cont\',\'closeNew\')');
    jQuery::getResponse();

}


function get($url, $params, $parse = true){
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url . '?' . urldecode(http_build_query($params)));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    $result = curl_exec($curl);
    curl_close($curl);

    return ($parse)? json_decode($result, true) :$result;
}
function post($url, $params, $parse = true){
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    $result = curl_exec($curl);
    curl_close($curl);

    return ($parse)? json_decode($result, true) :$result;
}

function addUserOfSendDiscount($u)
{
    $query = "REPLACE INTO `sw_notification_discount` (`user_id`, `created_record`) VALUES (".$u['id'].", NOW())";
    db_query($query);
}