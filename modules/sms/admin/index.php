<?
if (!defined('_SWS_ADMIN_CORE')) exit();

if (!_IS_ADMIN) jQuery::getResponse();

switch ($op) {
	case 'remove': removeSMS(); break;
	case 'send': {
		include_once('../../system/sms.php');
		sendSMS();
	} break;
	case 'load-module': initSMS(); break;
}

function removeSMS() {
	if (!_IS_ADMIN || empty($_GET['id'])) jQuery::getResponse();
	$id = abs(intval($_GET['id']));
	
	db_delete('sms', '`id` = '.quote($id));
	
	jQuery::getResponse();
}

function sendSMS() {
	global $SMS4B;
	
	if (empty($_POST['text'])) {
		jQuery::evalScript('sms.error(1)');
		jQuery::getResponse();
	}
	
	$title = !empty($_POST['title']) ? filter(trim(in1251($_POST['title'])), 'nohtml') : '��� ��������';
	$text = filter(trim(in1251($_POST['text'])), 'nohtml');
	$numbers = array();
	$where = null;
	
	if (!empty($_POST['numers'])) {
		foreach (explode("\n", $_POST['numers']) as $num) {
			$num = abs(intval(trim($num)));
			if ($num != 0) $numbers[] = $num;
		}
	}
	
	if (!empty($_POST['type']) && $_POST['type'] == 2) $where = ' AND `subscription` = 1';
	
	$r = db_query('SELECT `sys-phone` FROM `sw_users` WHERE `active` = 1 AND `sys-phone` != \'\''.$where);
	while ($u = mysql_fetch_array($r, MYSQL_ASSOC)) $numbers[] = $u['sys-phone'];
	
	foreach ($numbers as $number) {
		$SMS4B->GetSOAP("AccountParams",array("SessionID" => $SMS4B->GetSID()));
		$SMS4B->SendSmsPack($text, $SMS4B->parse_numbers($number), htmlspecialchars(stripslashes('SF')), $SMS4B->GetFormatDateForSmsForm(date("d-m-Y H:i:s")), '', '');
	}
	
	db_query('INSERT INTO `sw_sms` (`title`, `text`) VALUES ('.quote($title).', '.quote($text).')');
	
	$sid = dbone('MAX(`id`)', false, 'sms');
	
	jQuery::evalScript(inUTF8('sms.insert({id:'.$sid.',title:"'.htmlspecialchars($title).'",text:"'.htmlspecialchars($text).'"})'));
	jQuery::getResponse();
}

function initSMS() {
	$r = db_query('SELECT (SELECT COUNT(`id`) FROM `sw_users` WHERE `active` = 1 AND `sys-phone` != \'\') as `all`, (SELECT COUNT(`id`) FROM `sw_users` WHERE `active` = 1 AND `sys-phone` != \'\' AND `subscription` = 1) as `subscription`');
	$c = mysql_fetch_array($r, MYSQL_ASSOC);
	
	$html = '<h1><a href="javascript:module.index()">SMS ��������</a></h1>';
	$html.= '<form id="fSendSMS" action="/go/m=sms&op=send" onsubmit="formSubmit(this); return false;">';
	$html.= '<table class="form-data"><tr><th width="80">���� �����:</th><td><p><label><input type="radio" name="type" value="1" checked> ���� <span class="hint">('.$c['all'].')</span></label></p><p><label><input type="radio" name="type" value="2"> ������������� <span class="hint">('.$c['subscription'].')</span></label></p><p id="my-numers"><a href="javascript:sms.myNumbers(true)" class="insert-type">�������� ������</a><br/></p></td></tr></table>';
	$html.= '<p class="item"><label>��������: <span class="hint">(��� �������)</span></label><input type="text" name="title" class="f-field"></p>';
	$html.= '<p class="item"><label>����� ���������: <span class="hint">(��������: <font id="count-str">0</font>)</span></label><textarea name="text" class="f-field" rows="5" onkeyup="sms.count(this)"></textarea></p>';
	$html.= '<p class="item submit"><input type="submit" value="���������" class="b-textfield""></p>';
	$html.= '</form>';
	
	$html.= '<h2>������� ���������</h2>';
	$html.= '<table id="list-items" class="items"><tr class="title"><th class="title">��������</th><th class="text">�����</th><th width="20"></th></tr>';
	
	$r = db_query('SELECT `id`, `title`, `text` FROM `sw_sms` ORDER BY `id` DESC');
	while ($s = mysql_fetch_array($r, MYSQL_ASSOC)) {
		$html.= '<tr id="s'.$s['id'].'"><td class="title"><a href="javascript:sms.view('.$s['id'].')">'.$s['title'].'</a></td><td class="text">'.$s['text'].'</td><td class="btn"><div title="�������" class="delete" onclick="sms.remove('.$s['id'].')"></div></td></tr>';
	}
	
	$html.= '</table>';
	
	jQuery('#content-data')->html(inUTF8($html));
	jQuery::getResponse();	
}