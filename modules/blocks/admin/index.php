<?
	if (!defined('_SWS_ADMIN_CORE')) exit();
	
	$_GLOBALS['create_dir'] = true;
	$_GLOBALS['add'] = true;
	$_GLOBALS['search'] = true;
	$_GLOBALS['on-page'] = 20;
	$_GLOBALS['tiny'] = true;
	$_GLOBALS['date'] = true;
	
	$_GLOBALS['field']['title']['id'] = 'ID';
	$_GLOBALS['field']['title']['sid'] = '������/���������';
	$_GLOBALS['field']['title']['active'] = '���./����.';
	$_GLOBALS['field']['title']['pos'] = '�������';
	$_GLOBALS['field']['title']['url'] = '������';
	$_GLOBALS['field']['title']['title'] = '���������';
	$_GLOBALS['field']['title']['text'] = '�����';
	$_GLOBALS['field']['title']['html_title'] = '��������� �������� (TITLE)';
	$_GLOBALS['field']['title']['keywords'] = '�������� ����� (KEYWORDS)';
	$_GLOBALS['field']['title']['description'] = '�������� (DESCRIPTION)';
	
	$_GLOBALS['field']['type']['sid'] = 'sid';
	$_GLOBALS['field']['type']['active'] = 'checkbox';
	$_GLOBALS['field']['type']['pos'] = 'position';
	$_GLOBALS['field']['type']['url'] = 'text';
	$_GLOBALS['field']['type']['title'] = 'title';
	$_GLOBALS['field']['type']['text'] = 'html';
	$_GLOBALS['field']['type']['html_title'] = 'textarea';
	$_GLOBALS['field']['type']['keywords'] = 'textarea';
	$_GLOBALS['field']['type']['description'] = 'textarea';
	
	$_GLOBALS['field']['db']['sid'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['active'] = "int(1) NOT NULL default '0'";
	$_GLOBALS['field']['db']['pos'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['url'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['title'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['text'] = "longtext";
	$_GLOBALS['field']['db']['html_title'] = "text";
	$_GLOBALS['field']['db']['keywords'] = "text";
	$_GLOBALS['field']['db']['description'] = "text";
	$_GLOBALS['field']['db']['insert'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['system'] = "int(1) NOT NULL default '0'";
	
	$_GLOBALS['field']['table']['id'] = 'text';
	$_GLOBALS['field']['table']['title'] = 'title';
	
	$_GLOBALS['field']['value']['active'] = 1;
	
	$_GLOBALS['edit-field'] = array('active', 'title', 'url', 'text', 'html_title', 'keywords', 'description');
	$_GLOBALS['add-field'] = $_GLOBALS['edit-field'];
?>