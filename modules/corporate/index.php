<?
if (!defined('SW')) die('���� ������� �� ���������');
error_reporting(E_ERROR);

include_once('include/jquery/jQuery.php');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

switch($op) {
	case 'insert': insertPhone(); break;
	default: getCorporate(); break;
}

function insertPhone() {
	global $_GLOBALS, $SMS4B;
	
	if (empty($_POST['firmname']) || empty($_POST['username']) || empty($_POST['phone']) || empty($_POST['email'])) {
		if (empty($_POST['firmname'])) jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#firmname"),t:"����� ������ ��������� ��������",left:280,top:jQuery("#firmname input").offset().top-260})'));
		if (empty($_POST['username'])) jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#username"),t:"����� ������ ���λ",left:280,top:jQuery("#username input").offset().top-260})'));
		if (empty($_POST['phone'])) jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#phone"),t:"����� ������ ���������",left:280,top:jQuery("#phone input").offset().top-260})'));
		if (empty($_POST['email'])) jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#email"),t:"����� ������ �E-mail�",left:280,top:jQuery("#email input").offset().top-260})'));
		jQuery::getResponse();
	}
	
	$firmname = filter(trim(in1251($_POST['firmname'])), 'nohtml');
	$username = filter(trim(in1251($_POST['username'])), 'nohtml');
	$phone = filter(trim(in1251($_POST['phone'])), 'nohtml');
	$email = filter(trim(in1251($_POST['email'])), 'nohtml');
	$comment = filter(trim(in1251($_POST['comment'])), 'nohtml');
	
	db_query('INSERT INTO `sw_corporate` (`active`, `title`, `username`, `phone`, `email`, `comment`, `insert`) VALUES (1, '.quote($firmname).', '.quote($username).', '.quote($phone).', '.quote($email).', '.quote($comment).', UNIX_TIMESTAMP())');
	
	$msg = '<p>����� ������ �� ������������� ������������.</p>';
	$msg.= '<p>&nbsp;</p>';
	if (!empty($firmname)) $msg.= '<p><b>�������� ��������:</b> '.$firmname.'</p>';
	if (!empty($username)) $msg.= '<p><b>���:</b> '.$username.'</p>';
	if (!empty($phone)) $msg.= '<p><b>�������:</b> '.$phone.'</p>';
	if (!empty($email)) $msg.= '<p><b>E-mail:</b> '.$email.'</p>';
	if (!empty($comment)) $msg.= '<p><b>���������:</b><br />'.$comment.'</p>';
	
	$headers = "Content-type: text/html; charset=windows-1251 \r\nFrom: robot@studiofloristic.ru\r\n";
	$theme = '����� ������ �� �'.$_GLOBALS['v_sitename'].'�';

	$mail = new PHPMailer;
	try {
		$mail->setFrom('robot@studiofloristic.ru');
		$mail->addAddress($_GLOBALS['v_email_admin']);
		$mail->Subject = $theme;
		$mail->isHTML(true);
		$mail->CharSet = 'windows-1251';
		$mail->Body = '<html>'.$msg.'</html>';
		$mail->AltBody = strip_tags($msg);
		$mail->send();		
	} catch (Exception $e) {
	    unset($e);
	}
		
	jQuery::evalScript(inUTF8('corporate.created()'));
	jQuery::getResponse();
}

function getCorporate() {
	global $_GLOBALS;
	
	$r = db_query('SELECT `sid`, `title`, `text`, `html_title`, `keywords`, `description` FROM `sw_pages` WHERE `link` = '.quote('corporate').' AND `active` = 1');
	if (mysql_num_rows($r) == 0) HeaderPage(_PAGE_ERROR404);
	$p = mysql_fetch_array($r, MYSQL_ASSOC);
	
	$html = stripslashes($p['text']);
//	$html.= '<div class="corpForm blockWithTopLine"><h2>����� ������ �� ������������� ������������</h2><form action="/corporate/op=insert" onsubmit="formSubmit(this); return false;">';
//	$html.= '<p id="firmname">�������� ��������: <span class="red">*</span><br /><input name="firmname" type="text" /></p>';
//	$html.= '<p id="username">���: <span class="red">*</span><br /><input name="username" type="text" /></p>';
//	$html.= '<p id="phone">�������: <span class="red">*</span><br /><input name="phone" type="text" /></p>';
//	$html.= '<p id="email">E-mail: <span class="red">*</span><br /><input name="email" type="text" /></p>';
//	$html.= '<p id="comment">�����������:<br /><textarea name="comment"></textarea></p>';
//	$html.= '<p><span class="red hint">����, ���������� *, ����������� ��� ����������.</span></p>';
//	$html.= '<p><div align="right"><input type="submit" value="���������" /></div></p>';
//	$html.= '</form></div>';
//	


	//Breadcrumbs
	$bcms = bcNav($p);
	
	if (isset($bcms)) {
		$_GLOBALS['bcms'] = $bcms;
	}

	$_GLOBALS['section'] = 'corporate';
	$_GLOBALS['template'] = 'default';
	
	$_GLOBALS['pages'] = array($link);
	$sid = $p['sid'];
	while ($sid != 0) {
		$r = db_query('SELECT `id`, `sid`, `link` FROM `sw_pages` WHERE `id` = '.$sid);
		$ps = mysql_fetch_array($r, MYSQL_ASSOC);
		$sid = $ps['sid'];
		$_GLOBALS['pages'][] = $ps['link'];
	}
	$_GLOBALS['pages'] = array_reverse($_GLOBALS['pages']);
	
	$_GLOBALS['menu'] = $_GLOBALS['pages'][0];
	
	$_GLOBALS['title'] = $p['title'];
	$_GLOBALS['text'] = $html;
	$_GLOBALS['html_title'] = !empty($p['html_title']) ? $p['html_title'] : $p['title'].' - '.$_GLOBALS['v_sitename'];
	if (!empty($p['description'])) $_GLOBALS['description'] = $p['description'];
	if (!empty($p['keywords'])) $_GLOBALS['keywords'] = $p['keywords'];
}

function bcNav($p)
{
	global $_GLOBALS;

	$res = '<div class="bc_cont" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="'.$_GLOBALS['abs_url_host'].'" class=""><span itemprop="title">�������</span></a><span class="text_color_orange"> <i class="fontico-angle-right"></i></span></div><div class="bc_cont" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">'.$p['title'].'</span></div>';

	return $res;
}

?>