<?
	if (!defined('_SWS_ADMIN_CORE')) exit();
	
	$_GLOBALS['create_dir'] = false;
	$_GLOBALS['add'] = true;
	$_GLOBALS['search'] = true;
	$_GLOBALS['on-page'] = 20;
	$_GLOBALS['tiny'] = false;
	$_GLOBALS['date'] = true;
	
	$_GLOBALS['field']['title']['id'] = 'ID';
	$_GLOBALS['field']['title']['sid'] = '������/���������';
	$_GLOBALS['field']['title']['active'] = '���./����.';
	$_GLOBALS['field']['title']['pos'] = '�������';
	$_GLOBALS['field']['title']['title'] = '� �����';
	$_GLOBALS['field']['title']['value'] = '������ � %';
	$_GLOBALS['field']['title']['activation'] = '������';
	
	$_GLOBALS['field']['type']['sid'] = 'sid';
	$_GLOBALS['field']['type']['active'] = 'checkbox';
	$_GLOBALS['field']['type']['pos'] = 'position';
	$_GLOBALS['field']['type']['title'] = 'title';
	$_GLOBALS['field']['type']['value'] = 'text';
	$_GLOBALS['field']['type']['activation'] = 'checkbox';
	
	$_GLOBALS['field']['db']['sid'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['active'] = "int(1) NOT NULL default '0'";
	$_GLOBALS['field']['db']['pos'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['title'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['value'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['activation'] = "int(1) NOT NULL default '0'";
	$_GLOBALS['field']['db']['insert'] = "timestamp NOT NULL default CURRENT_TIMESTAMP";
	$_GLOBALS['field']['db']['system'] = "int(1) NOT NULL default '0'";
	
	$_GLOBALS['field']['table']['id'] = 'text';
	$_GLOBALS['field']['table']['title'] = 'title';
	
	$_GLOBALS['field']['value']['active'] = 1;
	
	$_GLOBALS['edit-field'] = array('active', 'activation', 'title', 'value');
	$_GLOBALS['add-field'] = $_GLOBALS['edit-field'];
	
	switch ($op) {
		case 'load-module': initCards(); break;
		case 'remove-discount': discountRemove(); break;
	}
	
	function discountRemove() {
		if (empty($_GET['id'])) jQuery::getResponse();
		$id = abs(intval($_GET['id']));
		
		$r = db_query('SELECT `user-discount`, `user-id` FROM `sw_cards` WHERE `id` = '.quote($id));
		if (mysql_num_rows($r) != 0) {
			$d = mysql_fetch_array($r, MYSQL_ASSOC);
			if (!empty($d['user-id'])) {
				db_query('UPDATE `sw_cards` SET `activation` = 0, `activation-date` = NULL, `user-discount` = 0, `user-id` = 0 WHERE `id` = '.quote($id));
				db_query('UPDATE `sw_users` SET `discount` = '.quote($d['user-discount']).' WHERE `id` = '.quote($d['user-id']));
				jQuery::evalScript(inUTF8('module.index();initSysDialog("������ ��������");'));
			}
		}
		
		jQuery::getResponse();
	}
	
	function initCards() {
		global $_GLOBALS, $m, $module, $op;
		
		$sid = 0;
		$pg = 1;
		$search = null;
		
		if (!empty($_GET['query'])) $search = filter(trim(in1251($_GET['query'])), 'nohtml');
		if (!empty($_GET['pg'])) $pg = abs(intval($_GET['pg']));
		if (!empty($_GET['sid'])) $sid = abs(intval($_GET['sid']));
		
		$on_page = !empty($_GLOBALS['on-page']) ? $_GLOBALS['on-page'] : 10;
		$min = $on_page * $pg - $on_page;
		
		if (!empty($search)) {
			if (substr($search, 0, 2) != 'id') {
				if (!empty($_GLOBALS['field']['search'])) {
					$item = array();
					foreach ($_GLOBALS['field']['search'] as $f) $item[] = 'i.`'.$f.'` LIKE \'%'.mysql_real_escape_string($search).'%\'';
					$where = implode(' OR ', $item);
				} else $where = 'i.`title` LIKE \'%'.mysql_real_escape_string($search).'%\'';
			} else {
				$where = 'i.`id` = '.quote(abs(intval(substr($search, 2))));
			}
		} else {
			$where = 'i.`sid` = '.$sid;
		}
		
		$sDateV = !empty($_GET['sDate']) ? filter(trim(in1251($_GET['sDate'])), 'nohtml') : null;
		$eDateV = !empty($_GET['eDate']) ? filter(trim(in1251($_GET['eDate'])), 'nohtml') : null;
		$tValue = !empty($_GET['type']) ? abs(intval($_GET['type'])) : 0;
		
		if (!empty($_GET['sDate']) || !empty($_GET['eDate'])) {
			$dField = !empty($_GET['type']) && $_GET['type'] != 1 ? 'activation-date' : 'insert';
			
			if (!empty($_GET['sDate']) && !empty($_GET['sDate'])) {
				if (preg_match("/^([0-9]{2})\.([0-9]{2})\.([0-9]{4})$/i", $_GET['sDate'], $sd) && preg_match("/^([0-9]{2})\.([0-9]{2})\.([0-9]{4})$/i", $_GET['eDate'], $ed)) {
					$sDate = $sd[3].'-'.$sd[2].'-'.$sd[1];
					$eDate = $ed[3].'-'.$ed[2].'-'.$ed[1];
					$where.= ' AND (DATE(`'.$dField.'`) >= '.quote($sDate).' AND DATE(`'.$dField.'`) < '.quote($eDate).')';
				}
			} elseif (!empty($_GET['sDate'])) {
				if (preg_match("/^([0-9]{2})\.([0-9]{2})\.([0-9]{4})$/i", $_GET['sDate'], $date)) {
					$sDate = $date[3].'-'.$date[2].'-'.$date[1];
					$where.= ' AND DATE(`'.$dField.'`) >= '.quote($sDate);
				}
			} elseif (!empty($_GET['sDate'])) {
				if (preg_match("/^([0-9]{2})\.([0-9]{2})\.([0-9]{4})$/i", $_GET['eDate'], $date)) {
					$eDate = $date[3].'-'.$date[2].'-'.$date[1];
					$where.= ' AND DATE(`'.$dField.'`) < '.quote($sDate);
				}
			}
			
		}
		
		$html = '<h1><a href="javascript:module.index()">'.$m['title'].'</a>'.(!empty($_GLOBALS['add']) ? ' <img src="/sf123sf123sf/i/add.png" title="��������" align="absmiddle" onclick="initGET(\'/go/m='.$module.'&op=item\')">' : null).'</h1>';
		$html.= '<form action="/go/m='.$module.'&op=load-module" onsubmit="getForm(this); return false;">';
		$html.= '<p class="item">������ �� ����: � <input id="start-date" type="text" class="f-field date" name="sDate" value="'.$sDateV.'"> �� <input id="end-date" type="text" class="f-field date" name="eDate" value="'.$eDateV.'"></p>';
		$html.= '<p class="item">�� ����: <label><input type="radio" name="type" value="1"'.($tValue == 1 ? ' checked' : ($tValue == 0 ? ' checked' : null)).'> ����������</label> <label><input type="radio" name="type" value="2"'.($tValue == 2 ? ' checked' : null).'> ���������</label></p>';
		$html.= '<div class="search"><input type="text" class="query" name="query" value="'.htmlspecialchars($search).'"><p class="info">��� ������ �� id �������: id2</p></div>';
		
		if ($sid != 0) {
			$pach = array();
			$s = $sid;
			$back = 0;
			while ($s != 0) {
				$r = db('`id`, `sid`, `title`', '`id` = '.$s, $module);
				$p = mysql_fetch_array($r);
				$s = $p['sid'];
				if ($back == 0) $back = $p['sid'];
				$sid != $p['id'] ? $pach[] = '<a onclick="initGET(\'/go/m='.$module.'&op=load-module&sid='.$p['id'].'\')">'.$p['title'].'</a>' : $pach[] = $p['title'];
			}
			$pach[] = '<a onclick="initGET(\'/go/m='.$module.'&op=load-module\')">'.$m['title'].'</a>';
			krsort($pach);
			$html.= '<div class="pach">'.implode('<span>></span>', $pach).'</div>';
		}
	
		$html.= '<p class="item submit"><input type="submit" class="b-textfield" value="�����"></p>';
		$html.= '</form>';
		$html.= '<table id="list-items" class="items" style="margin-top:15px"><tr class="title">';
		
		foreach ($_GLOBALS['field']['table'] as $name=>$value) {
			!isset($fields) ? $fields = 'i.`'.$name.'`' : $fields.= ', i.`'.$name.'`';
			$html.= '<th class="'.$name.'">'.$_GLOBALS['field']['title'][$name].'</th>';
		}
		
		$tr = count($_GLOBALS['field']['table']);
		
		$html.= '<th class="activation">������</th>';
		$html.= '<th class="datetime">������������</th>';
		$html.= '<th class="datetime">���������</th>';
		$html.= '<th class="options">��������</th>';
		$html.= '</tr>';
		
		if ($sid != 0) $html.= '<tr><td class="id"></td><td colspan="'.$tr.'" class="title dir" onclick="initGET(\'/go/m='.$module.'&op=load-module&sid='.$back.'\')">..</td></tr>';
		
		$r = db_query('SELECT COUNT(i.`id`) FROM `sw_'.$module.'` as i WHERE '.$where);
		$c = mysql_fetch_array($r, MYSQL_NUM);
		
		$q = 'SELECT '.$fields.', UNIX_TIMESTAMP(i.`activation-date`) as `activation-date`, UNIX_TIMESTAMP(i.`insert`) as `insert`, i.`activation`, i.`active`, i.`system` FROM `sw_cards` as i WHERE '.$where.' ORDER BY i.`id` DESC LIMIT '.$min.', '.$on_page;
		$r = db_query($q);
		while ($i = mysql_fetch_array($r, MYSQL_ASSOC)) {
			$html.= '<tr id="i'.$i['id'].'">';
			
			foreach ($_GLOBALS['field']['table'] as $name=>$value) {
				switch ($value) {
					case 'title': $html.= '<td class="title'.($i['dir'] != 0 ? ' dir" onclick="initGET(\'/go/m='.$module.'&op=load-module&sid='.$i['id'].'\')"' : '" onclick="initGET(\'/go/m='.$module.'&op=item&id='.$i['id'].'&pg='.$pg.'&query='.$search.'\')"').'>'.$i[$name].'</td>'; break;
					#case 'viewOrder': $html.= '<td class="title"><a onclick="initGET(\'/go/m='.$module.'&op=item&id='.$i['id'].'&pg='.$pg.'&query='.$search.'\')">'.$i[$name].'</a> | <a href="javascript:order.view('.$i['id'].')">MINI</a></td>'; break;
					case 'viewOrder': $html.= '<td class="title"><a href="javascript:order.view('.$i['id'].')">'.$i[$name].'</a></td>'; break;
					case 'checkbox': $html.= '<td class="'.$name.'"><span onclick="item.checked('.$i['id'].',this)" class="jLink'.(!empty($i[$name]) ? ' off">���������' : '">��������').'</span></td>'; break;
					case 'active': $html.= '<td class="'.$name.'"><b style="color:'.(!empty($i[$name]) ? 'green">������������' : 'red">�� ������������').'</b></td>'; break;
					case 'status': $html.= '<td class="'.$name.'"><b style="color:'.(!empty($i[$name]) ? 'green">�������' : 'red">�� �������').'</b></td>'; break;
					case 'link': $html.= '<td class="'.$name.'"><a href="'.$i[$name].'" target="_blank">'.$i[$name].'</a></td>'; break;
					default: $html.= '<td class="'.$name.'">'.$i[$name].'</td>'; break;
				}
			}
			
			$html.= '<td><b style="color:'.(!empty($i['activation']) ? 'green">������������ | <a href="javascript:void(0)" onclick="initGET(\'/go/m=cards&op=remove-discount&id='.$i['id'].'\')">��������</a>' : 'red">�� ������������').'</b></td><td>'.date('d.m.Y � H:i', $i['activation-date']).'</td><td>'.date('d.m.Y � H:i', $i['insert']).'</td><td class="btn"><div class="active'.($i['active'] != 0 ? '" title="�������"' : ' active-off" title="��������"').' onclick="initGET(\'/go/m='.$module.'&op=active&id='.$i['id'].'\')"></div><div class="edit" title="�������������" onclick="initGET(\'/go/m='.$module.'&op=item&id='.$i['id'].'&pg='.$pg.'&query='.$search.'\')"></div><div title="�������" class="delete'.($i['system'] != 0 ? '-off"' : '" onclick="items.remove('.$i['id'].')"').'></div></td></tr>';
		}
		
		$html.= '</table>';
		
		if ($c[0] > $on_page) $html.= paging($op, $c[0], $pg, $on_page, '/go/m='.$module.'&op=load-module&query='.$search);
		
		jQuery('#content-data')->html(inUTF8($html));
		jQuery::evalScript(inUTF8('module.set('.$pg.',"'.htmlspecialchars($search).'")'));
		if (!empty($_GLOBALS['tiny'])) jQuery::evalScript('htmlEditor.init()');
		jQuery::evalScript('module.search()');
		jQuery::evalScript('date.init(function(){date.load(jQuery("input#start-date"));date.load(jQuery("input#end-date"));})');
		jQuery::getResponse();
	}
?>