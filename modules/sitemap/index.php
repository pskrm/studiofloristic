<?php
if (!defined('SW')) {die('���� ������� �� ���������');}

switch($op){
    case 'getfile': get_sitemap(); break;
    case 'genfile': gen_sitemap(); break;
    default : HeaderPage(_PAGE_ERROR404);
}

function gen_sitemap(){
    global $sitemap_querys,$sm_q_order;
    
    $host = $_SERVER['HTTP_HOST'];
    if (!empty($_GET['auth_key']) && md5($host.'sitemap_genfile')==$_GET['auth_key']){
        $gen_time = time();
        $xml_file = 'sitemap.xml';
        $http = 'http'.(isset($_SERVER['HTTPS'])?'s':'').'://';
        query_new('UPDATE `sw_sitemap_options` SET `pages_ingen`=0, `status`=1, `lastmod`=CURRENT_TIMESTAMP, `pages_zero`=0,'
                . ' `pages_50x`=0, `pages_40x`=0, `pages_30x`=0, `pages_20x`=0, `gentime`=0 WHERE `id`=1');
        query_new('UPDATE `sw_sitemap` SET `status`=0, `active`=0');
        $s_xml = new sitemap_core($host,$http,$xml_file);
        $s_xml->ch_stat = empty($_GET['check_status']) ? false : true;
        foreach ($sm_q_order as $ord){
            $sm_cur = $sitemap_querys[$ord];
            $urls = query_new($sm_cur['query']);
            foreach ($urls as $urow){
                $s_xml->cur_stat=0;
                $lmod = !empty($urow['lmod']) ? strtotime($urow['lmod']) : time();
                $loc = $sm_cur['glue'].$urow['url'].$sm_cur['end'];
                $act = $s_xml->quick_addblock(array('loc'=>$s_xml->full_host.$loc,'lmod'=>date('c',$lmod),'ch'=>$sm_cur['ch'],'pr'=>$sm_cur['pr']));
                if (empty($urow['id'])){
                    query_new('INSERT INTO `sw_sitemap` (`active`,`modid`,`mod`,`url`,`status`,`lastmod`) VALUES ('
                        . $act.','.$urow['modid'].','.quote($urow['mod']).','.quote($loc).','.$s_xml->cur_stat.','.quote(date('Y-m-d H:i:s',$lmod)).')');
                }else{
                    query_new('UPDATE `sw_sitemap` SET `active`='.$act.', `status`='.$s_xml->cur_stat.', '
                            . '`lastmod`='.quote(date('Y-m-d H:i:s',$lmod)).', `url`='.quote($loc).' WHERE id='.$urow['id']);
                }
                $stat = strval($s_xml->cur_stat);
                $up = ($s_xml->cur_stat<200) ? 'zero':$stat[0].'0x';
                query_new('UPDATE `sw_sitemap_options` SET `pages_ingen`=`pages_ingen`+1, `gentime`='.(time()-$gen_time).', `pages_'.$up.'`=`pages_'.$up.'`+1 WHERE `id`=1');
            }
        }
        $s_xml->generate_file();
        query_new('UPDATE `sw_sitemap_options` SET `status`=0, `gentime`='.(time()-$gen_time).', `lastmod`=CURRENT_TIMESTAMP, '
                . '`pages_all`=(SELECT COUNT(`id`) FROM `sw_sitemap`) WHERE `id`=1');
    }
    HeaderPage(_PAGE_ERROR404);
}

function get_sitemap(){
    $xml_file = 'sitemap.xml';
    if (file_exists($xml_file)){
        return_file($xml_file);
    } else {
        HeaderPage(_PAGE_ERROR404);
    }
}

