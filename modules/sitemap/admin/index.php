<?php
    if (!defined('_SWS_ADMIN_CORE')) { exit(); }
	
	$_GLOBALS['create_dir'] = false;
	$_GLOBALS['add'] = false;
	$_GLOBALS['search'] = false;
	$_GLOBALS['on-page'] = 0;
	$_GLOBALS['tiny'] = false;
	$_GLOBALS['date'] = false;
        
    $tabl_flds = array();
	$tabl_flds['sid'] = "int(11) NOT NULL default '0'";
	$tabl_flds['active'] = "int(1) NOT NULL default '0'";
	$tabl_flds['pos'] = "int(11) NOT NULL default '0'";
    $tabl_flds['modid'] = "int(11) NOT NULL default '0'";
	$tabl_flds['mod'] = "varchar(255) default NULL";
    $tabl_flds['url'] = "TEXT NOT NULL";
    $tabl_flds['status'] = "int(11) NOT NULL default '0'";
	$tabl_flds['lastmod'] = "timestamp NOT NULL default CURRENT_TIMESTAMP";
        
	$tabl_flds_opt = array();
    $tabl_flds_opt['pages_all'] = "int(11) NOT NULL default '0'";
    $tabl_flds_opt['pages_ingen'] = "int(11) NOT NULL default '0'";
    $tabl_flds_opt['pages_20x'] = "int(11) NOT NULL default '0'";
    $tabl_flds_opt['pages_30x'] = "int(11) NOT NULL default '0'";
    $tabl_flds_opt['pages_40x'] = "int(11) NOT NULL default '0'";
    $tabl_flds_opt['pages_50x'] = "int(11) NOT NULL default '0'";
    $tabl_flds_opt['pages_zero'] = "int(11) NOT NULL default '0'";
    $tabl_flds_opt['status'] = "int(11) NOT NULL default '0'";
    $tabl_flds_opt['gentime'] = "int(11) NOT NULL default '0'";
    $tabl_flds_opt['lastmod'] = "timestamp NOT NULL default CURRENT_TIMESTAMP";
    
	switch ($op) {
		case 'load-module': initSitemap(); break;
		case 'file-create': fileCreate(); break;
        case 'get-status': getStatus(); break;
	}
	
    function initSitemap(){
        global $m, $module,$tabl_flds,$tabl_flds_opt;
		
        check_tbl(array($module=>$tabl_flds,$module.'_options'=>$tabl_flds_opt));
        
        $mod_opt = query_new("SELECT *, (SELECT count(`active`) FROM "._MYSQL_PREFIX.$module." WHERE `active`=1) AS `active_urls` "
                . "FROM "._MYSQL_PREFIX.$module.'_options', 1);
        if (empty($mod_opt)){
            query_new('INSERT INTO '._MYSQL_PREFIX.$module.'_options (`status`) VALUES (0)');
            $mod_opt = query_new("SELECT * FROM "._MYSQL_PREFIX.$module.'_options', 1);
        }
		$html = '<div class="h1-cont"><h1><a href="javascript:module.index()">'.$m['title'].'</a></h1></div>';
        $html.= '<table id="sitemap_tbl" class="items">'.PHP_EOL
                . '<tr class="title">'.PHP_EOL
                    . '<th>����� URL</th>'.PHP_EOL
                    . '<th>�����</br>����������</th>'.PHP_EOL
                    . '<th>������ 200</th>'.PHP_EOL
                    . '<th>������ 30x<br>(���������������)</th>'.PHP_EOL
                    . '<th>������ 40x<br>(�������� �� �������)</th>'.PHP_EOL
                    . '<th>������ 50x<br>(������ �������)</th>'.PHP_EOL
                    . '<th>������ �� ��������<br>��� �����������</th>'.PHP_EOL
                    . '<th>������������<br>��������</th>'.PHP_EOL
                    . '<th>��������� ����������:<br>�� ������ �� �� <hr>�� ��������� sitemap.xml</th>'.PHP_EOL
                    . '<th>����� /<br>������ sitemap.xml</th></tr>'.PHP_EOL.sm_opts_tr($mod_opt,true). '</table>';
		if (!empty($mod_opt['status'])){
            jQuery::evalScript('sitemap.runCheck()');
        }
        jQuery('#content-data')->html(inUTF8($html));
		jQuery::getResponse();
    }
    
    function getStatus($ret=false){
        global $module;
        
        $mod_opt = query_new("SELECT *, (SELECT count(`active`) FROM "._MYSQL_PREFIX.$module." WHERE `active`=1) AS `active_urls` "
                . "FROM "._MYSQL_PREFIX.$module.'_options', 1);
        $http = 'http'.(isset($_SERVER['HTTPS'])?'s':'').'://';
        $chfile = empty($mod_opt['status']) ? true : false;
        if (!empty($mod_opt['status'])){
            jQuery('td#sm_s>span')->replaceWith(inUTF8('<span style="color:#999;">���� ������ �������� ����� sitemap.xml</span>'));
            jQuery('td#sm_s form input[type="button"]')->val(inUTF8("���� ��������\nsitemap.xml"));
        }else{
            jQuery::evalScript('sitemap.stopCheck()');
            jQuery('td#sm_s>span')->replaceWith(inUTF8((file_exists('../../sitemap.xml')
                    ? '<span><a href="'.$http.$_SERVER['HTTP_HOST'].'/sitemap/op=getfile">C������ <b>sitemap.xml</b></a></span>'
                    : '<span style="color:#f00">���� <b>sitemap.xml</b> �����������</span>'))); 
            jQuery('td#sm_s form input[type="button"]')->val(inUTF8((file_exists('../../sitemap.xml')?'��������':'�������')."\nsitemap.xml"));
        }
        $lmodfile = (file_exists('../../sitemap.xml') && !empty($chfile)) ? date('Y-m-d H:i:s',filemtime('../../sitemap.xml')):'����������';
        $lmoddb = !empty($mod_opt['lastmod']) ? $mod_opt['lastmod']:'����������';
        jQuery('td#sm_lmod')->html(inUTF8('<div>'.$lmoddb.'</div><hr><div>'.$lmodfile.'</div>'));
        jQuery('td#sm_g')->html(inUTF8(date('i:s',$mod_opt['gentime']).'<br>(���:���)'));
        jQuery('td#sm_pz')->html(inUTF8($mod_opt['pages_zero']));
        jQuery('td#sm_p5')->html(inUTF8($mod_opt['pages_50x']));
        jQuery('td#sm_p4')->html(inUTF8($mod_opt['pages_40x']));
        jQuery('td#sm_p3')->html(inUTF8($mod_opt['pages_30x']));
        jQuery('td#sm_p2')->html(inUTF8($mod_opt['pages_20x']));
        jQuery('td#sm_pi')->html(inUTF8($mod_opt['pages_ingen']));
        jQuery('td#sm_pa')->html(inUTF8('� ���� ������:<br>'.$mod_opt['pages_all'].'<hr>�� ��� � sitemap.xml:<br>'.$mod_opt['active_urls']));
		if (!$ret){
            jQuery::getResponse();
        }
    }
    
    function fileCreate(){
        global $module;
        
        $key = md5($_SERVER['HTTP_HOST'].'sitemap_genfile');
        $chstate = empty($_REQUEST['check_status']) ? '':'&check_status=on';
        $http = 'http'.(isset($_SERVER['HTTPS'])?'s':'').'://';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $http.$_SERVER['HTTP_HOST'].'/'.$module.'/op=genfile&auth_key='.$key.$chstate);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        curl_exec($ch);
        curl_close($ch);
        
        jQuery::evalScript('sitemap.runCheck()');
        getStatus(true);
		jQuery::getResponse();
        
    }
    
    function sm_opts_tr($mod_opt=false,$chfile=false){
        global $module;
        $http = 'http'.(isset($_SERVER['HTTPS'])?'s':'').'://';
        if (!empty($mod_opt['status'])){
            $status = '<span style="color:#999;">���� ������ �������� ����� sitemap.xml</span>';
            $sub_text = "���������\nsitemap.xml";
        }else{
            $status = (file_exists('../../sitemap.xml')) ? '<span><a href="'.$http.$_SERVER['HTTP_HOST'].'/sitemap/op=getfile">C������ <b>sitemap.xml</b></a></span>'
                    : '<span style="color:#f00">���� <b>sitemap.xml</b> �����������</span>';
            $sub_text = (file_exists('../../sitemap.xml')?'��������':'�������')."\nsitemap.xml";
        }
        $lmodfile = (file_exists('../../sitemap.xml') && !empty($chfile)) ? date('Y-m-d H:i:s',filemtime('../../sitemap.xml')):'����������';
        $lmoddb = !empty($mod_opt['lastmod']) ? $mod_opt['lastmod']:'����������';
        $inp_dis = empty($mod_opt['status'])?'':'disabled';
        $status.= '<div><form action="/go/m='.$module.'&op=file-create" onsubmit="getForm(this); return false;">'
                . '<p><input class="b-textfield" type="button" onclick="sitemap.fileCreate()" value="'.$sub_text.'" '.$inp_dis.'></p>'
                . '<p><label><input type="checkbox" value="on" name="check_status" checked '.$inp_dis.' style="vertical-align:middle;">'
                . '<span style="line-height:16px;">��������� ������ �������</span></label></p></form>';
        return '<tr id="sm_opts"><td id="sm_pa">� ���� ������:<br>'.$mod_opt['pages_all'].'<hr>�� ��� � sitemap.xml:<br>'.$mod_opt['active_urls'].'</td>'.PHP_EOL
                . '<td id="sm_pi">'.$mod_opt['pages_ingen'].'</td>'.PHP_EOL
                . '<td id="sm_p2">'.$mod_opt['pages_20x'].'</td>'.PHP_EOL
                . '<td id="sm_p3">'.$mod_opt['pages_30x'].'</td>'.PHP_EOL
                . '<td id="sm_p4">'.$mod_opt['pages_40x'].'</td>'.PHP_EOL
                . '<td id="sm_p5">'.$mod_opt['pages_50x'].'</td>'.PHP_EOL
                . '<td id="sm_pz">'.$mod_opt['pages_zero'].'</td>'.PHP_EOL
                . '<td id="sm_g">'.date('i:s',$mod_opt['gentime']).'<br>(���:���)</td>'.PHP_EOL
                . '<td id="sm_lmod"><div>'.$lmoddb.'</div><hr><div>'.$lmodfile.'</div></td>'.PHP_EOL
                . '<td id="sm_s" class="'.(empty($mod_opt['status'])?'off':'on').'"><div class="sm_loading"></div>'.$status.'</td></tr>';
    }