<?php
$sitemap_querys = array(
    array('query'=>'SELECT cs.`url`,cs.`cid` as `modid`,'.quote('catalog').' as `mod`, sm.`id`, sm.`lastmod` as `lmod` FROM `sw_catalog_section` cs'
        . ' LEFT JOIN `sw_catalog` c ON c.`id`=cs.`cid` LEFT OUTER JOIN `sw_sitemap` sm ON sm.`modid`=cs.`cid` AND sm.`mod`='.quote('catalog')
        . ' WHERE cs.`first` = 1 AND c.`active` = 1  ORDER BY cs.`cid` DESC',
        'ch'=>2,'pr'=>'0.7','glue'=>'','end'=>''),
    array('query'=>'SELECT s.`pach` as `url`, s.`id` as `modid`,'.quote('section').' as `mod`, sm.`id`, sm.`lastmod` as `lmod` FROM `sw_section` s'
        . ' LEFT OUTER JOIN `sw_sitemap` sm ON sm.`modid`=s.`id` AND sm.`mod`='.quote('section').' WHERE s.`pach` IS NOT NULL AND s.`active` = 1',
        'ch'=>2,'pr'=>'0.7','glue'=>'','end'=>''),
    array('query'=>'SELECT p.`link` as `url`, p.`id` as `modid`,'.quote('pages').' as `mod`, sm.`id`, sm.`lastmod` as `lmod` FROM `sw_pages` p'
        . ' LEFT OUTER JOIN `sw_sitemap` sm ON sm.`modid`=p.`id` AND sm.`mod`='.quote('pages')
        . ' WHERE p.`active` = 1 AND p.`link` NOT IN ('.quote('error404').','.quote('index').')',
        'ch'=>3,'pr'=>'0.5','glue'=>'/','end'=>'.html'),
    array('query'=>'SELECT b.`url`,b.`id` as `modid`,'.quote('blocks').' as `mod`, sm.`id`, sm.`lastmod` as `lmod` FROM `sw_blocks` b'
        . ' LEFT OUTER JOIN `sw_sitemap` sm ON sm.`modid`=b.`id` AND sm.`mod`='.quote('blocks').' WHERE b.`active` = 1',
        'ch'=>3,'pr'=>'0.3','glue'=>'','end'=>''),
    array('query'=>'SELECT a.`link` as `url`,a.`id` as `modid`,'.quote('articles').' as `mod`, sm.`id`, sm.`lastmod` as `lmod` FROM `sw_articles` a'
        . ' LEFT OUTER JOIN `sw_sitemap` sm ON sm.`modid`=a.`id` AND sm.`mod`='.quote('articles').' WHERE a.`active` = 1',
        'ch'=>4,'pr'=>'0.3','glue'=>'/articles/','end'=>'.html'),
    array('query'=>'SELECT n.`link` as `url`,n.`id` as `modid`,'.quote('news').' as `mod`, sm.`id`, sm.`lastmod` as `lmod` FROM `sw_news` n'
        . ' LEFT OUTER JOIN `sw_sitemap` sm ON sm.`modid`=n.`id` AND sm.`mod`='.quote('news').' WHERE n.`active` = 1',
        'ch'=>4,'pr'=>'0.3','glue'=>'/news/','end'=>'.html'),
    array('query'=>'(SELECT `id`,`url`,`modid`,`mod`,`lastmod` FROM `sw_sitemap` WHERE `modid`=1 AND `mod`='.quote('index_page').') UNION '
        . '(SELECT NULL AS `id`,'.quote('/').' as `url`,1 as `modid`,'.quote('index_page').' as `mod`, CURRENT_TIMESTAMP() as `lastmod`) '
        . 'ORDER BY  `id` DESC LIMIT 1',
        'ch'=>2,'pr'=>'1.0','glue'=>'','end'=>'')
);
$sm_q_order = array(6,1,0,2,5,4,3);

class sitemap_core { 
    var $lines = array(
        '<?xml version="1.0" encoding="UTF-8"?>',
        '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">',
        '</urlset>');
    var $xml_fn = 'sitemap.xml';
    var $t = "\t";
    var $t2 = "\t\t";
    var $full_host,$host,$http,$url_count,$ch_stat,$cur_stat;
    var $ch = array('always','hourly','daily','weekly','monthly','yearly','never');
    var $abr = array('loc'=>'loc','lmod'=>'lastmod','ch'=>'changefreq','pr'=>'priority',
        'lastmod'=>'lastmod','changefreq'=>'changefreq','priority'=>'priority');
    
    public function __construct($host='',$http='http://',$filename='') {
        $host = empty($host) ? $_SERVER['HTTP_HOST'] : $host;
        $http = (empty($http)) ? (!isset($_SERVER['HTTPS'])?'http://':'https://') : $http;
        if (!empty($filename)){$this->xml_fn = $filename;}
        $this->full_host = $http.$host;
		$this->host = $host;
		$this->http = $http;
		$this->url_count = 0;
//        $this->quick_addblock(array('loc'=>$this->full_host.'/','lmod'=>date('c'),'ch'=>2,'pr'=>'1.0'));
    }
    
    function generate_file (){
        if (file_put_contents($this->xml_fn, implode(PHP_EOL,$this->lines))!==false){
            return true;
        }
        return false;
    }
    
    function add_block($tag='',$var=''){
        $pop = array_pop($this->lines);
        $this->lines[] = $this->t.'<'.$tag.'>';
        foreach ($var as $key=>$row){
            $this->lines[] = $this->t2.'<'.$key.'>'.$row.'</'.$key.'>';
        }
        $this->lines[] = $this->t.'</'.$tag.'>';
        $this->lines[] = $pop;
    }
    
    function quick_addblock($loc=array()){
        if (!empty($loc['loc'])){
            $to_add = array();
            foreach ($loc as $key=>$var){
                if (isset($this->abr[$key])){
                    $key = $this->abr[$key];
                    if (($key=='changefreq') && !empty($this->ch[$var])){
                        $var = $this->ch[$var];
                    }
                    if ($key == 'loc'){ $var = $this->mask($var); }
                    if (strval($var)!=''){ $to_add[$key] = strval($var); }
                }
            }
            if (!empty($to_add['loc'])){
                $this->cur_stat = empty($this->ch_stat) ? 0 : intval(url_resp_code($loc['loc']));
                if (empty($this->ch_stat) || (!empty($this->ch_stat) && $this->cur_stat==200)){
                    $this->url_count++;
                    $this->add_block('url',$to_add);
                    return 1;
                }
            }
        }
        return 0;
    }
    
    function mask($url=''){
        if (is_string($url)){
            return htmlspecialchars($url,ENT_QUOTES,'UTF-8');
        }
        return '';
    }
}

