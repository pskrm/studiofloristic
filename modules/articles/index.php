<?
if (!defined('SW')) die('���� ������� �� ���������');

switch($op) {
	case 'view': viewArticle(); break;
	default: getArticles(); break;
}

function viewArticle() {
	global $_GLOBALS, $module,$cur;
	
	if (empty($_GET['link'])) HeaderPage(_PAGE_ERROR404);
	$link = filter(trim($_GET['link']), 'nohtml');
	$db_flds = array('sc.*','scs_url.`url`','scs_url.`sid`');
	$where = array();
	$data = PageData($module);
	
	$r = db_query('SELECT `id`, `cid`, `date`, `title`, `text`, `html_title`, `keywords`, `description` FROM `sw_articles` WHERE `link` = '.quote($link).' AND `active` = 1');
	if (mysql_num_rows($r) == 0) HeaderPage(_PAGE_ERROR404);
	$n = mysql_fetch_array($r, MYSQL_ASSOC);
	
	//$html = '<h1 class="firsth1">'.$data['title'].'</h1>';
	$html = '<h1 class="firsth1">'.$n['title'].'</h1>';

	$where[] = !empty($n['cid'])? ' AND scs.`sid`='.$n['cid'].' ':' AND sc.`index`=1 ';
	$where[] = 'GROUP BY sc.`id` ORDER BY RAND() LIMIT 0,3';
	$items = catalog_items($db_flds, $where, false);
	if (!empty($items)){
		$html.= '<div class="catalog">';
		foreach ($items as $item) { $html .= dirItem($item);}
		$html.= '<div class="viewAllCatalog"><a href="/catalog/bouquet/">����������� ��� ��������</a></div><div class="clear"></div></div>';
	}
	
	//$html.= '<div class="contentBlock blockWithTopLine"><div class="mainTextInner"><span class="h3_alt">'.$n['title'].'</span>';
	$html.= '<div class="contentBlock blockWithTopLine"><div class="mainTextInner">';
	$html.= stripslashes($n['text']);
	
	$html.= '<br clear="all"/><div class="pagging" style="padding-top: 15px;">';
	
	$r = db_query('SELECT `link`, `title` FROM `sw_articles` WHERE `id` < '.$n['id'].' AND `active` = 1 ORDER BY `date`, `id` DESC');
	if (mysql_num_rows($r) != 0) {
		$i = mysql_fetch_array($r, MYSQL_ASSOC);
		if (strlen($i['title']) > 20) {
			$title = substr($i['title'], 0, 20);
			$title = substr($title, 0, strrpos($title, ' ')).'...';
		} else $title = $i['title'];
		
		$html.= '<a href="/articles/'.$i['link'].'.html" class="prevLink" title="'.$i['title'].'">&lt; '.$title.'</a>';
	}
	
	$html.= '<a href="/articles.html">��� ������</a>';
	
	$r = db_query('SELECT `link`, `title` FROM `sw_articles` WHERE `id` > '.$n['id'].' AND `active` = 1 ORDER BY `date`, `id` DESC');
	if (mysql_num_rows($r) != 0) {
		$i = mysql_fetch_array($r, MYSQL_ASSOC);
		if (strlen($i['title']) > 25) {
			$title = substr($i['title'], 0, 25);
			$title = substr($title, 0, strrpos($title, ' ')).'...';
		} else $title = $i['title'];
		
		$html.= '<a href="/articles/'.$i['link'].'.html" class="nextLink" title="'.$i['title'].'">'.$title.' &gt;</a>';
	}
	
	$html.= '</div>';
		
	$html.= '</div></div>';
	
	$_GLOBALS['text'] = $html;
	$_GLOBALS['html_title'] = !empty($n['html_title']) ? $n['html_title'] : $n['title'].' - �������';
	if (!empty($n['description'])) $_GLOBALS['description'] = $n['description'];
	if (!empty($n['keywords'])) $_GLOBALS['keywords'] = $n['keywords'];
	$_GLOBALS['template'] = 'primary';
	$_GLOBALS['section'] = 'articles';
}

function getArticles() {
	global $_GLOBALS, $module;
	
	$on_page = 10;
	$pg = !empty($_GET['pg']) ? abs(intval($_GET['pg'])) : 1;
	$min = $on_page * $pg - $on_page;
	$count = dbone('COUNT(`id`)', '`active` = 1 AND `date` <= CURDATE()', 'articles');
	$html = null;
	
	$data = PageData($module);
	
	$html.= '<div class="articles"><h1>'.$data['title'].'</h1>';
	
	if ($pg > 1) {
		$rs = db_query('SELECT `text`, `html_title`, `keywords`, `description` FROM `sw_seo` WHERE `active` = 1 AND `title` = '.quote(filter(trim($_SERVER['REQUEST_URI']), 'nohtml')).' LIMIT 1');
		if (mysql_num_rows($rs) != 0) $seo = mysql_fetch_array($rs, MYSQL_ASSOC);
		if (!empty($seo['text'])) $data['text'] = $seo['text'];
		if (!empty($seo['html_title'])) $data['html_title'] = $seo['html_title'];
		if (!empty($seo['keywords'])) $data['keywords'] = $seo['keywords'];
		if (!empty($seo['description'])) $data['description'] = $seo['description'];
	}
	
	if (!empty($data['text'])) $html.= '<div class="mainTextInner">'.stripslashes($data['text']).'</div>';
	
	$r = db_query('SELECT UNIX_TIMESTAMP(`date`) as `date`, `link`, `title`, `notice` FROM `sw_articles` WHERE `active` = 1 AND `date` <= CURDATE() ORDER BY `id` DESC LIMIT '.$min.', '.$on_page);
	while ($n = mysql_fetch_array($r, MYSQL_ASSOC)) {
		$html.= '<div class="newsBlock blockWithTopLine"><div class="newsTitle"><a href="/articles/'.$n['link'].'.html">'.$n['title'].'</a><span class="date">'.date('d.m.Y', $n['date']).'</span></div><div class="newsText">'.$n['notice'].'</div><div class="newsLink"><a href="/articles/'.$n['link'].'.html">�����</a></div></div>';
	}
	
	$html.= '<div class="blockWithTopLine"></div></div>';
	
	if ($count > $on_page) $html.= PageNavigator($count, $pg, $on_page, '/articles/op=all&pg=', '/articles.html');

	$title_addition = ($pg > 1) ? ' (�������� �'.$pg.') / �������� �Studio Floristic�' : ' / �������� �Studio Floristic�';
	
	$_GLOBALS['title'] = $data['title'];
	$_GLOBALS['text'] = $html;
	$_GLOBALS['html_title'] = !empty($data['html_title']) ? $data['html_title'].$title_addition : $data['title'].$title_addition;
	if (!empty($data['description'])) $_GLOBALS['description'] = $data['description'];
	if (!empty($data['keywords'])) $_GLOBALS['keywords'] = $data['keywords'];
	$_GLOBALS['template'] = 'primary';
	$_GLOBALS['section'] = 'articles';
}

?>