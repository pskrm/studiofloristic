<?php
if (!defined('SW')) die('���� ������� �� ���������');

include_once('include/jquery/jQuery.php');

switch($op) {
    case 'main': main_blocks($_GET); break;
    default: default_task(); break;
}

function default_task(){
    jQuery::getResponse();
}

function main_blocks($sel_blocks = array(),$ret=0){
    unset($sel_blocks['op']);
    unset($sel_blocks['m']);
    $select = implode("' OR `id_name`='", array_keys($sel_blocks));
    $blocks = query_new("SELECT * FROM sw_text_blocks WHERE `active`=1 AND (`id_name`='".$select."')",0,'id_name');
    if (is_array($blocks)){
        foreach ($blocks as $key=>$block){
            $params = $sel_blocks[$key];
            $params = get_block_params($params);
            $type = $params['type'];
            switch ($type){
                case 'to_body':
                    $btn_close = (in_array('just_btn_close', $params['others']))? ' just_btn_close ':'';
                    $html = '<div id="'.$key.'_cont" class="tb_'.$type.' '.$btn_close.'">'
                        . '<div class="'.$key.'_info">'
                            . '<div class="closeNew abort "></div>'
                            . (($params['titles']!='')?$params['titles']:'<span class="title">'.$block['title'].'</span>')
                            . '<div class="text_block_cont">'.$block['text'].'</div>'
                            . '<div>'
                                . (($params['buttons']!='')?'<p class="btns" style="padding: 11px 0 0 0;">'.$params['buttons'].'</p>':'')
                        . '</div></div></div>';
                    $html = str_replace('<img', '<img nopin="nopin" ', $html);
                    if ($ret==1){
                        return $html;
                    }
                    if (in_array('onclick',$params['others'])){
                        jQuery("#".$key)->attr('onclick',"windows_info('".$key."_cont','closeNew')");
                    }
                    jQuery('body')->append(inUTF8($html));
                    if (isset($params['others']['show'])){
                        jQuery('.'.$key.'_info')->append(inUTF8($params['others']['show']['append']));
                        jQuery(".".$key."_info span.".$params['others']['show']['btn']."_btn")->attr('onclick',"text_blocks.showMore(this,'.window_info .".$key."_info #".$params['others']['show']['show']."')");
                    }
                    break;
                case 'hover_popup':
                    $html = '<div '.(empty($params['others']['multiple'])?'id="'.$key.'_cont"':'').' class="tb_hover_popup">
                                <div class="triagle"></div>
                                <div class="text_block_cont">'.$block['text'].'</div>
                            </div>';
                    $html = str_replace('<img', '<img nopin="nopin" ', $html);
                    if ($ret==1){
                        return $html;
                    }
                    $key = !empty($params['others']['multiple'])?'.'.$params['others']['multiple']:'#'.$key;
                    if (in_array('append',$params['others'])){
                        jQuery($key)->append(inUTF8($html));
                    }elseif(in_array('after',$params['others'])){
                        jQuery($key)->after(inUTF8($html));
                    }else{
                        jQuery($key)->html(inUTF8($html));
                    }
                    break;
                case 'plane_text':
                    $html = str_replace('<img', '<img nopin="nopin" ', $block['text']);
                    if ($ret==1){
                        return $html;
                    }
                    jQuery('#'.$key)->html(inUTF8($html));
                    break;
            }
        }
    }
    if ($ret==0){
        jQuery::getResponse();
    }

}
function get_block_params($params){
    $ret = array('type'=>'to_body');
    $buttons = array();
    $titles = array();
    $others = array();

    $def_btns = array(
        'ok'=>array('text'=>'��'),
        'no'=>array('text'=>'���'),
        'cancel'=>array('text'=>'������'),
        'yes'=>array('text'=>'��������'),
        'more'=>array('text'=>'���������')
    );
    $def_titles = array(
        'attention'=>array('text'=>'��������'),
        'warning'=>array('text'=>'��������������')
    );
    if (is_string($params)){
        $arr = explode('--', $params);
        foreach ($arr as $row){
            if (strpos($row,'TYPE_')===0){
                $ret['type'] = str_replace('TYPE_', '', $row);
            }elseif (strpos($row,'BTN_')===0){
                $btn = str_replace('BTN_', '', $row);
                if (strpos($btn,'_TBT')!==false){
                    $btn = str_replace('_TBT', '', $btn);
                    $btn_sel = query_new("SELECT * FROM sw_text_blocks WHERE `active`=1 AND `id_name`='".$btn."')",1);
                    $buttons[$btn]['text'] = str_replace('p', 'span', $btn_sel['text']);
                }elseif(isset($def_btns[$btn])){
                    $buttons[$btn]['text'] = $def_btns[$btn]['text'];
                }
            }elseif (strpos($row,'_BTN_CLASS_')!==false){
                $btn_vars = explode('_BTN_CLASS_', $row);
                $buttons[$btn_vars[0]]['class'] = $btn_vars[1];
            }elseif (strpos($row,'TIT_')===0){
                $tit = str_replace('TIT_', '', $row);
                if (strpos($tit,'_TBT')!==false){
                    $tit = str_replace('_TBT', '', $tit);
                    $tit_sel = query_new("SELECT * FROM sw_text_blocks WHERE `active`=1 AND `id_name`='".$tit."')",1);
                    $titles[$tit]['text'] = str_replace('p', 'span', $tit_sel['text']);
                }elseif(isset($def_titles[$tit])){
                    $titles[$tit]['text'] = $def_titles[$tit]['text'];
                }else{
                    $titles[] = array('text'=>  urldecode(in1251($tit)));
                }
            }elseif (strpos($row,'_TIT_CLASS_')!==false){
                $tit_vars = explode('_TIT_CLASS_', $row);
                $titles[$tit_vars[0]]['class'] = $tit_vars[1];
            }elseif(strpos($row,'BTNSHOW_')===0){
                $vars = explode('->',str_replace('BTNSHOW_', '', $row));
                $show_text = main_blocks(array($vars[1]=>'TYPE_plane_text'), 1);
                if (is_string($show_text) && $show_text!=''){
                    $others['show'] = array(
                        'btn'=>$vars[0],
                        'append'=>'<div id="'.$vars[1].'">'.$show_text.'</div>',
                        'show'=>$vars[1]
                    );
                }
            }elseif(strpos($row,'MULTIPLE_')===0){
                $others['multiple'] = str_replace('MULTIPLE_', '', $row);
			}elseif(strpos($row,'PAGE_')===0){
			$others['page'] = str_replace('PAGE_', '', $row);
            }else{
                $others[]=$row;
            }
        }

        $btn_dis = '';
        foreach ($buttons as $key=>$btn){
            if (isset($btn['text']) && $btn['text']!=''){
                $class = (isset($btn['class']))?$btn['class']:'butn_green';
                $btn_dis .= '<span class="'.$class.' '.$key.'_btn">'.$btn['text'].'</span>';
            }
        }
        $ret['buttons'] = $btn_dis;

        $tit_dis = '';
        foreach ($titles as $key=>$tit){
            if (isset($tit['text']) && $tit['text']!=''){
                $class = (isset($tit['class']))?$tit['class']:'';
                $tit_dis .= '<span class="'.$class.' '.$key.'_tit title">'.$tit['text'].'</span>';
            }
        }
        $ret['titles'] = $tit_dis;
        $ret['others'] = $others;
        return $ret;
    }
    return false;
}
