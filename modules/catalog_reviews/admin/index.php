<?
	if (!defined('_SWS_ADMIN_CORE')) exit();
	
	$_GLOBALS['create_dir'] = false;
	$_GLOBALS['add'] = true;
	$_GLOBALS['search'] = true;
	$_GLOBALS['on-page'] = 20;
	$_GLOBALS['tiny'] = false;
	$_GLOBALS['date'] = true;
	
	$_GLOBALS['field']['title']['id'] = 'ID';
	$_GLOBALS['field']['title']['sid'] = '������/���������';
	$_GLOBALS['field']['title']['active'] = '���./����.';
	$_GLOBALS['field']['title']['pos'] = '�������';
	$_GLOBALS['field']['title']['name'] = '���';
	$_GLOBALS['field']['title']['email'] = 'E-mail';
	$_GLOBALS['field']['title']['text'] = '�����';
	$_GLOBALS['field']['title']['response'] = '�����';
	$_GLOBALS['field']['title']['url'] = '�����';
	$_GLOBALS['field']['title']['date'] = '���� ����������';
	
	$_GLOBALS['field']['type']['sid'] = 'sid';
	$_GLOBALS['field']['type']['active'] = 'checkbox';
	$_GLOBALS['field']['type']['pos'] = 'position';
	$_GLOBALS['field']['type']['name'] = 'text';
	$_GLOBALS['field']['type']['email'] = 'text';
	$_GLOBALS['field']['type']['text'] = 'textarea';
	$_GLOBALS['field']['type']['response'] = 'textarea';
	$_GLOBALS['field']['type']['url'] = 'link';
	$_GLOBALS['field']['type']['date'] = 'date2';
	
	$_GLOBALS['field']['db']['sid'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['active'] = "int(1) NOT NULL default '0'";
	$_GLOBALS['field']['db']['pos'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['name'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['email'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['text'] = "text";
	$_GLOBALS['field']['db']['response'] = "text";
	$_GLOBALS['field']['db']['insert'] = "timestamp NOT NULL default CURRENT_TIMESTAMP";
	$_GLOBALS['field']['db']['system'] = "int(1) NOT NULL default '0'";
	
	$_GLOBALS['field']['table']['id'] = 'text';
	$_GLOBALS['field']['table']['name'] = 'title';
	$_GLOBALS['field']['table']['text'] = 'text';
	$_GLOBALS['field']['table']['response'] = 'text';
	$_GLOBALS['field']['table']['url'] = 'link';
	$_GLOBALS['field']['table']['date'] = 'text';
	
	$_GLOBALS['field']['value']['active'] = 1;
	$_GLOBALS['field']['search'] = array('name', 'text', 'response');
	
	$_GLOBALS['edit-field'] = array('active', 'name', 'email', 'date', 'text', 'response', 'url');
	$_GLOBALS['add-field'] = $_GLOBALS['edit-field'];
        
        $check_tbls_cols = array(
            $module=>array(
                'stars' => "int(11) NOT NULL default '0'"
            )
        );
        check_tbl($check_tbls_cols);
?>