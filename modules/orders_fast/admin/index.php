<?
	if (!defined('_SWS_ADMIN_CORE')) exit();
	
	$_GLOBALS['create_dir'] = false;
	$_GLOBALS['add'] = false;
	$_GLOBALS['search'] = true;
	$_GLOBALS['on-page'] = 20;
	$_GLOBALS['tiny'] = false;
	$_GLOBALS['date'] = true;
	
	$_GLOBALS['field']['title']['id'] = 'ID';
	$_GLOBALS['field']['title']['sid'] = '������/���������';
	$_GLOBALS['field']['title']['active'] = '��������./�� ��������.';
	$_GLOBALS['field']['title']['pos'] = '�������';
	$_GLOBALS['field']['title']['title'] = '������������';
	$_GLOBALS['field']['title']['price'] = '���� �� 1��.<br>(���.)';
	$_GLOBALS['field']['title']['count'] = "���-��<br>(��.)";
	$_GLOBALS['field']['title']['uName'] = '��������';
	$_GLOBALS['field']['title']['uPhone'] = '����� ��������';
	$_GLOBALS['field']['title']['uComment'] = '�����������';
	$_GLOBALS['field']['title']['comment'] = '����������� ���������';
	$_GLOBALS['field']['title']['insert'] = '��������';
	$_GLOBALS['field']['title']['delivery_date'] = '���� ��������';
	
	$_GLOBALS['field']['type']['sid'] = 'sid';
	$_GLOBALS['field']['type']['active'] = 'checkbox';
	$_GLOBALS['field']['type']['pos'] = 'position';
	$_GLOBALS['field']['type']['title'] = 'text';
	$_GLOBALS['field']['type']['price'] = 'text';
	$_GLOBALS['field']['type']['count'] = 'text';
	$_GLOBALS['field']['type']['uName'] = 'text';
	$_GLOBALS['field']['type']['uPhone'] = 'text';
	$_GLOBALS['field']['type']['uComment'] = 'textarea';
	$_GLOBALS['field']['type']['comment'] = 'textarea';
	$_GLOBALS['field']['type']['delivery_date'] = 'date';
	
	$_GLOBALS['field']['db']['sid'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['active'] = "int(1) NOT NULL default '0'";
	$_GLOBALS['field']['db']['pos'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['title'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['price'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['count'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['uName'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['uPhone'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['uComment'] = "text";
	$_GLOBALS['field']['db']['comment'] = "text";
	$_GLOBALS['field']['db']['insert'] = "timestamp NOT NULL default CURRENT_TIMESTAMP";
	$_GLOBALS['field']['db']['system'] = "int(1) NOT NULL default '0'";
	$_GLOBALS['field']['db']['delivery_date'] = 'date';
    $_GLOBALS['field']['db']['structure'] = 'text';
	
	$_GLOBALS['field']['table']['id'] = 'text';
	$_GLOBALS['field']['table']['title'] = 'viewOrder';
    $_GLOBALS['field']['table']['uName'] = 'uid_link';
	$_GLOBALS['field']['table']['price'] = 'text';
	$_GLOBALS['field']['table']['count'] = 'text';
	$_GLOBALS['field']['table']['delivery_date'] = 'text';
	$_GLOBALS['field']['table']['insert'] = 'datetime';
    $_GLOBALS['field']['table']['uid'] = 'nop';
	
	$_GLOBALS['field']['value']['active'] = 1;
	
	$_GLOBALS['edit-field'] = array('active', 'title', 'price', 'count', 'uName', 'uPhone','delivery_date', 'uComment', 'comment');
	$_GLOBALS['add-field'] = $_GLOBALS['edit-field'];
	
	$check_tbls_cols = array(
        'orders_fast'=>$_GLOBALS['field']['db']
    );
    check_tbl($check_tbls_cols);