<?php
if (!defined('SW')) die('���� ������� �� ���������');

include_once('include/jquery/jQuery.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

switch($op) {
    case 'init': init(); break;
    case 'get': get_send_question(); break;
    case 'email': send_answer(); break;
    default: default_task(); break;
}

function default_task(){
    jQuery::getResponse();
}

function send_answer(){
    global  $_GLOBALS;
    $text = isset($_GET['txt']) ? filter(trim(in1251($_GET['txt'])), 'nohtml') : "";
    $id = isset($_GET['id']) ? filter(trim(in1251($_GET['id'])), 'nohtml') : "";
    $sql = sprintf("
    SELECT * FROM sw_questions WHERE id = %d
    ", mysql_real_escape_string($id ));

    $question = query_new($sql,1);
    $headers = "Content-type: text/html; charset=windows-1251 \r\nFrom: "._HOSTNAME."<".$_GLOBALS['v_email_admin'].">\r\n";
    $theme = "������ �� ����� �"._HOSTNAME."�";
    if (!empty($question['email'])){
        $msg = '<p>������������, '.$question['name'].'</p>';
        $msg.= '<p>'.$question['date'].' �� �������� ���� ��������� �� ����� studiofloristic.ru</p>'
            . '<p>���� ��������� ����������� ��� ��� ��������� �����. ���� � ��� �������� �������, ������� 8 800 333-12-91 (��������� �� ������)</p><hr/>'
            . '<p>'.$text.'</p><hr/>';
        $msg.= '<br/><p>������� ������� �� ������� � ������ ��������!</p>';

        $mail = new PHPMailer;
        try {
            $mail->setFrom($_GLOBALS['v_email_admin'], _HOSTNAME);
            $mail->addAddress($question['email']);
            $mail->Subject = $theme;
            $mail->isHTML(true);
            $mail->CharSet = 'windows-1251';
            $mail->Body = '<html>'.$msg.'</html>';
            $mail->AltBody = strip_tags($msg);  
            $mail->send();            
        } catch (Exception $e) {
            unset($e);
        }
    }

    query_new("UPDATE sw_questions SET status = 1 WHERE id =  " . $id);

    jQuery::getResponse();
}

function init(){
    if ( $_SESSION['question_send'] == 1) {

        $html = '<div id="send_question_form" class=" tb_to_body">'
            . '<div class="send_question_info">'
            . '<div class="closeNew"></div>'
            . '<span class="title">��� ������ ��� ��������������<br/>���������� ���������.</span>'
            . '<br/>'
            . '<div><p>����� �� ������ �������� ������ �� ����� <a href="mailto:info@studiofloristic.ru">info@studiofloristic.ru</a></p></div>'
            . '</div>'
            . '</div>';
    }else{
        $html = '<div id="send_question_form" class=" tb_to_body">'
            . '<div class="send_question_info">'
            . '<div class="closeNew"></div>'
            . '<span class="title">�������� �������?<br/>�������� ���!</span>'
            . '<table>'
            . '<tr><td></td><td><div class="text_block_cont" id="top_text" style="margin-bottom:10px"></div></td></tr>'
            . '<tr><td><span class="input_label"><strong>���� ���*:</strong></span></td><td><input type="text" id="id_sq_name" name="sq_name" onclick="sendQuestion.refresh(this);" value=""><span id="sq_name_after"></span></td></tr>'
            . '<tr><td><span class="input_label"><strong>��� �������*:</strong></span></td><td><input type="text" id="id_sq_phone" name="sq_phone" onclick="sendQuestion.refresh(this);" value=""><span id="sq_phone_after"></span></td></tr>'
            . '<tr><td><span class="input_label"><strong>��� email:</strong></span></td><td><input type="text" id="id_sq_mail" name="sq_mail" value=""></td></tr>'
            . '<tr><td><span class="input_label"><strong>����� �������:</strong></span></td><td><textarea id="id_sq_text" name="sq_text" rows="5"></textarea></td></tr>'
            . '</table>'
            . '<br/>'
            . '<div><p>����� �� ������ �������� ������ �� ����� <a href="mailto:info@studiofloristic.ru">info@studiofloristic.ru</a></p></div>'
            . '<div><p class="color-gray">����, ���������� *, <strong>�����������</strong> � ����������.</p></div>'
            . '<div><p class="btns" ><span class="butn_green" onclick="sendQuestion.completeRequest();">��������� ������</span></p></div>'
            . '</div>'
            . '</div>';
    }
    jQuery('body')->append(inUTF8($html));
    jQuery::getResponse();
}
function get_send_question(){
    global  $_GLOBALS,$cur;

    $name = isset($_GET['name']) ? filter(trim(in1251($_GET['name'])), 'nohtml') : "";
    $phone = isset($_GET['phone']) ? filter(trim(in1251($_GET['phone'])), 'nohtml') : "";
    $email = isset($_GET['email']) ? filter(trim(in1251($_GET['email'])), 'nohtml') : "";
    $text = isset($_GET['text']) ? filter(trim(in1251($_GET['text'])), 'nohtml') : "";

    if (empty($name)){

        jQuery::getResponse();
    }
    if (empty($phone) && empty($email)){

        jQuery::getResponse();
    }
    $uid = (_IS_USER && !empty($_SESSION['uid']))? $_SESSION['uid']:0;

    // ������� � �� � �������� ������, ���� ���� �����

    query_new("INSERT INTO `sw_questions` (`name`, `phone`, `email`, `text`, `status`, `date`, `uid`) 
                    VALUES (". quote($name).", ". quote($phone).", ". quote($email).", ". quote($text).", '0', NOW(), ".quote($uid )." ) ;
               ");
    $_SESSION['question_send'] = 1;
    // �������� ������ ���� ����

    $headers = "Content-type: text/html; charset=windows-1251 \r\nFrom: "._HOSTNAME."<".$_GLOBALS['v_email_admin'].">\r\n";
    $theme = "������ �� ����� �"._HOSTNAME."�";
    if (!empty($email)){
        $msg = '<p>������������!</p>';
        $msg.= '<p>�� �������� ���� ��������� �� ����� studiofloristic.ru</p>'
            . '<p>���: '.$name.'</p>'
            . '<p>�����: '.$text.'</p>';
        $msg.= '<br/><p>� ��������� ����� ���� ��������� �������� � ����. ������� ������� �� ������� � ������ ��������!</p>';

        $mail = new PHPMailer;
        try {
            $mail->setFrom($_GLOBALS['v_email_admin'], _HOSTNAME);
            $mail->addAddress($email);
            $mail->Subject = $theme;
            $mail->isHTML(true);
            $mail->CharSet = 'windows-1251';
            $mail->Body = '<html>'.$msg.'</html>';
            $mail->AltBody = strip_tags($msg);
            $mail->send();            
        } catch (Exception $e) {
            unset($e);
        }
    }

    // �������� ������ ������

    $msg_admin = "<p>������������!</p>";
    $msg_admin.= '<p>������� ������ �� ������������</p>'
        . '<p>���: '.$name.'</p>'
        . '<p>�������: '.$phone.'</p>'
        . '<p>Email:'.$email.'</p><hr/>'
        . '<p>����� �������:'.$text.'</p><br/>';
    $msg_admin.= '<p>�������� �� ������ ����� � ���������������� ���������� � ����� <a href="http://'._HOSTNAME.'/sf123sf123sf/"><b>"�������� �������"</b></a><br/>';
    $msg_admin.= '����� <a href="http://'._HOSTNAME.'">'.$_GLOBALS['v_sitename'].'</a>';

    $mail = new PHPMailer;
    try {
        $mail->setFrom($_GLOBALS['v_email_admin'], _HOSTNAME);
        $mail->addAddress($_GLOBALS['v_email_admin']);
        $mail->Subject = $theme;
        $mail->isHTML(true);
        $mail->CharSet = 'windows-1251';
        $mail->Body = '<html>'.$msg_admin.'</html>';
        $mail->AltBody = strip_tags($msg);
        $mail->send();
    } catch (Exception $e) {
            unset($e);
        }

    jQuery::getResponse();
    /*
    global  $_GLOBALS,$cur;
    $get = $_GET;
    $name = filter(trim(in1251($get['fb_name'])), 'nohtml');
    $phone = filter(trim(in1251($get['fb_phone'])), 'nohtml');
    $call_now = filter(trim(in1251($get['call_now'])), 'nohtml');
    $current_time = filter(trim(in1251($get['current_time_h'])), 'nohtml').':'.filter(trim(in1251($get['current_time_m'])), 'nohtml');
    $call_time = filter(trim(in1251($get['call_time_h'])), 'nohtml').':'.filter(trim(in1251($get['call_time_m'])), 'nohtml');
    $call_time_end = (intval(filter(trim(in1251($get['call_to'])), 'nohtml'))==1) ? filter(trim(in1251($get['call_time_end_h'])), 'nohtml').':'.filter(trim(in1251($get['call_time_end_m'])), 'nohtml') : '00:00';
    $uid = (_IS_USER && !empty($_SESSION['uid']))? $_SESSION['uid']:0;
	
    $cur_tmp = $cur;
    $cur = set_currency('rub');
    $basket_info = basket_info();

    $cur = set_currency($cur_tmp['name']);
    $products = $basket_info['products'];
    unset($basket_info['products']);
    $itemsMail = '';
    foreach ($products as $tovar){
        $itemsMail.= '<li><a href="http://'._HOSTNAME.$tovar['url'].'" target="_blank">'.$tovar['title'].'</a> - '.$tovar['count'].' ��.</li>';
    }
    query_new("INSERT INTO sw_fast_baying (`name`,`phone`,`call_now`,`call_time`,`call_time_end`,`current_time`,`tovar_list`,`uid`,`amount`,`basket_info`) VALUES ("
            .  quote($name).","
            .  quote($phone).","
            .  quote($call_now).","
            .  quote($call_time).","
            .  quote($call_time_end).","
            .  quote($current_time).","
            .  quote(base64_encode(serialize($products))).","
            .  quote($uid).","
            .  quote($basket_info['discount_summ']).","
            .  quote(base64_encode(serialize($basket_info)))
            . ")");
    $row = query_new("SELECT * FROM sw_fast_baying WHERE id IN (SELECT MAX(id) FROM sw_fast_baying)",1);
    if ($row !==false){
        $time = $row['insert'];
    }else{
        $time = date('Y-m-d H:i:s');
    }

    $oid = intval($row['id']); // id bp ������� ������� �������

    $msg = '<p>������������!</p>';
    $msg.= '<p>�������� "������� �������" �� '.$time.'</p>'
            . '<p>��������: '.$name.'</p>'
            . '<p>�������: '.$phone.'</p>'
            . '<p>������� ����� � ���������:'.$current_time.'</p>';
    if (intval($call_now)>0){
        $msg .= '<p><b>����������� ������!</b></p>';
    }else{
        $msg .= '<p>����������� �: '.$call_time.'</p>';
        if (intval(filter(trim(in1251($get['call_to'])), 'nohtml'))==1){
            $msg .= '<p>����������� ��: '.$call_time_end.'</p>';
        }
    }
    $msg.= '<hr />';
    $msg.= '<p>������ ������:</p><ul>'.$itemsMail.'</ul>';
    $msg.= '<br /><br />';
    $msg.= '--<br />';
    $msg.= '����� <a href="http://'._HOSTNAME.'">'.$_GLOBALS['v_sitename'].'</a>';

    $headers = "Content-type: text/html; charset=windows-1251 \r\nFrom: "._HOSTNAME."<".$_GLOBALS['v_email_admin'].">\r\n";
    $theme = "������� ������� �� �"._HOSTNAME."�";
    mail($_GLOBALS['v_email_admin'], $theme, $msg, $headers);

    // �������� ������� sms � ��� ��� � ��� � ��������� ����� ��������

    $params = array(
        'msg_type' => SmsQueue::ORDER_FAST_BUY_RECEIVED,
        'oid' => $oid,
        'phone' => $phone
    );
    $smsQueue = new SmsQueue();
    $smsQueue->addMessageInQueue($params);

    // ����� �������� ���������

    jQuery::evalScript(inUTF8('initSysDialog("�������� �������� � ���� � ��������� �����")'));
    jQuery('#window_info_bg')->fadeOut(200);
    jQuery('.window_info')->hide();
    jQuery('.window_info')->removeClass('window_info');
    jQuery::getResponse();
    */
}