<?
if (!defined('_SWS_ADMIN_CORE')) exit();

$_GLOBALS['create_dir'] = false;
$_GLOBALS['add'] = false;
$_GLOBALS['search'] = true;
$_GLOBALS['on-page'] = 20;
$_GLOBALS['tiny'] = false;
$_GLOBALS['date'] = true;

$_GLOBALS['field']['title']['id'] = 'ID';
$_GLOBALS['field']['title']['name'] = '��� ������������';
$_GLOBALS['field']['title']['phone'] = '�������';
$_GLOBALS['field']['title']['email'] = 'E-mail';
$_GLOBALS['field']['title']['text'] = '����� �������';
$_GLOBALS['field']['title']['status'] = '������ (�������/�� �������)';
$_GLOBALS['field']['title']['date'] = '���� �������� �������';


$_GLOBALS['field']['type']['name'] = 'text';
$_GLOBALS['field']['type']['status'] = 'checkbox';
$_GLOBALS['field']['type']['pos'] = 'position';
$_GLOBALS['field']['type']['phone'] = 'text';
$_GLOBALS['field']['type']['email'] = 'text';
$_GLOBALS['field']['type']['text'] = 'textarea';

$_GLOBALS['field']['db']['id'] = 'int(11) NOT NULL';
$_GLOBALS['field']['db']['name'] = 'varchar(255) NOT NULL';
$_GLOBALS['field']['db']['phone'] = 'varchar(20) NOT NULL';
$_GLOBALS['field']['db']['email'] = 'varchar(255) NULL';
$_GLOBALS['field']['db']['text'] = 'text NULL';
$_GLOBALS['field']['db']['status'] = 'int(1) NULL';
$_GLOBALS['field']['db']['date'] = "datetime NULL";


$_GLOBALS['field']['table']['id'] = 'text';
$_GLOBALS['field']['table']['name'] = 'text';
$_GLOBALS['field']['table']['phone'] = 'text';
$_GLOBALS['field']['table']['email'] = 'text';
$_GLOBALS['field']['table']['text'] = 'textarea';
$_GLOBALS['field']['table']['status'] = 'text';
$_GLOBALS['field']['table']['date'] = 'datetime';


$_GLOBALS['field']['value']['active'] = 1;

$_GLOBALS['edit-field'] = array('name', 'phone', 'email', 'text', 'status');
$_GLOBALS['add-field'] = $_GLOBALS['edit-field'];

$check_tbls_cols = array(
    'questions'=>$_GLOBALS['field']['db']
);
check_tbl($check_tbls_cols);
