<?php

if (!defined('SW'))
    die('���� ������� �� ���������');
include_once('include/jquery/jQuery.php');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
switch($op) {
    case 'gifts-load': giftsLoad(); break;
    case 'interval': intervalPrice(); break;
    case 'interval2': intervalPriceWOMkad(); break;
    case 'courer': courerPrice(); break;
    case 'set': setCount(); break;
    case 'clear': clearItems(); break;
    case 'remove': removeItem(); break;
    case 'refresh': refreshItems(); break;
    case 'insert': insertItem(); break;
    case 'podarok': insertPodarok(); break;
    case 'box': initBox(); break;
    case 'vase': initVase(); break;
    case 'vase2': initVaseNew(); break;
    case 'vase3': initVaseRemove(); break;
    case 'delivery-photo': delivery_photo(); break;
    case 'set-cupon': setCupon(); break;
    case 'setgift': giftSet(); break;
    case 'issue': issue(); break;
    case 'create-order': createOrder(); break;
    case 'payment': paymentOrder(); break;
    case 'done': orderDone(); break;
    case 'sendMsg' : sendMessage(); break;
    case 'getrealprices' : getRealPrices(); break;
    case 'manual' : manualForm(); break;
    case 'getsummbyoid' : getSummByOrderPart(); break;
    case 'checkSbNeeds' : checkSberBankItem(); break; // ����� ��� ��������� ������ (����������� ��������), ����� ����� ��������� ����� ������ ��� ���
	case 'abandon': workAbandon(); break;
    default: initBasket(); break;
}

function workAbandon(){
	// ������� ��� ������
	$sessionId = session_id();
	$user_name = isset($_POST['name']) ? in1251(trim($_POST['name'])) : '';
	$user_email = isset($_POST['email']) ? in1251(trim($_POST['email'])) : '';
	$user_phone = isset($_POST['phone']) ? in1251(trim($_POST['phone'])) : '';
	$status = 0;
	$type =1; // �� ��� ����� ��� 1 - sms
	if (empty($sessionId))
	{
		// ��� ������ ��� �������
		echo "false";
		die;
	}

	// �������� ������� � email, ��� ������ ����
	if (((empty($user_phone )) || (mb_strlen($user_phone , "WINDOWS-1251") < 8)) && empty($user_email)) {
		echo "false #001";
		die;
	}
	// ������ �������� email, ���� email ���� �������� ���
	if (mb_strlen($user_email, "WINDOWS-1251") > 5) {
		if (filter_var($user_email, FILTER_VALIDATE_EMAIL)) {
			// ���������� email
			$type =0;// ����� ����� email
		}
	}

	$basket_info_15 = basket_info(array('user' => $_SESSION['cupon'] ));
	$summ15_1 = $basket_info_15['discount_summ'];
	$summ15_2 = $basket_info_15['mark_disc_summ'];

	// ��������� �������� ������ ������� ������ �����
	$basket_info = basket_info();


	if (count($basket_info ) > 0)
	{
		$cnt = query_new("SELECT MAX(id) AS cnt FROM `sw_abandoned_carts`;", true, 'cnt');
		$idm = ($cnt['cnt'] + 1);
		list($f, $l) =  explode(".", uniqid('s', true));
		$hash = substr($f, -6) . $idm;

		// ��� ��, ����� ������ � ��������
		// ��� ���� ������ ���� �� ��� ������ � ���� �������, ������� ������������� ����� ������ ������
		$query = sprintf("REPLACE INTO `sw_abandoned_carts` (
										  `id`,
										  `session`,
										  `dt`,
										  `email`,
										  `phone`,
										  `status`,
										  `type`,
										  `name`,
										  `order`,
										  `sid`,
										  `datas`,
										  `hash`,
										  `summ_discount`
										)
										VALUES
										  (
										    '%s',
										    '%s',
										    NOW(),
										    '%s',
										    '%s',
										    '',
										    '%s',
										    '%s',
										    '0',
										    '0',
										    %s,
										    '%s',
										    '%s'
  						);",$idm, $sessionId, $user_email, $user_phone, $type, $user_name,
			quote(serialize($basket_info)), $hash, $summ15_1 );
		db_query($query);
		echo "true";
		die;
	}


}

//<option value="sbbank">������ �� ��������� � ����� ����� ��</option>
function checkSberBankItem(){
    $uid = isset($_SESSION['uid']) ? $_SESSION['uid'] : $_SESSION['user']['id'];
    if ($uid > 0){
        echo json_encode(array('result' => 'true'));
    }else{
        echo json_encode(array('result' => 'false'));
    }
    die;
}

function getSummByOrderPart(){
    $ret = array('summ' => 0, 'text' => '0');
    $oidPart = isset($_GET['oid']) ? intval($_GET['oid']) : 0;
    $uid = isset($_SESSION['uid']) ? $_SESSION['uid'] : $_SESSION['user']['id'];
    // �������� ������ ���������� � ������
    $oData = query_new(sprintf("SELECT `id`, `itogo`, `status`, `uid` FROM `sw_orders` WHERE `id` = %d LIMIT 0,1", mysql_real_escape_string(intval($oidPart ))));
    $oData = $oData[0];
    $igogo = $oData['itogo'];
    if (($uid > 0) && ($oData['uid'] != $uid) ){
        // ������������ ���������������, �� ����� �� ���
        $ret['text'] = '1';
    }

    if (intval($oData['status']) >= 1){
        $ret['text'] = '2';
    }

    if (intval($oData['id']) <= 0){
        $ret['text'] = '3';
    }

    if (!empty($igogo)){
        $ret['summ'] = $igogo;
    }else{
        $ret['summ'] = 0;
    }

    echo json_encode($ret);
    die;
}

function manualForm(){
    $ret = array('result'=>'false');
    $oid = isset($_GET['oid']) ? $_GET['oid'] : 0;
    $otype = isset($_GET['type']) ? $_GET['type'] : 0;
    $summ = isset($_GET['summ']) ? $_GET['summ'] : 0;
    if (!empty($oid)) {
        // get order by id
        $oData = query_new(sprintf("SELECT id, itogo, uPhone, uName, email FROM `sw_orders` WHERE `id` = %d LIMIT 0,1", mysql_real_escape_string(intval($oid))));
        $oData = $oData[0];
        $itog = $amount = $summItogoRub = $oData['itogo'];
        $hash = md5($oData['id']); // ��� ��� ������ ����������
        $html = "";
        $realCurrency = "RUB";
    }else{
        // pay by summ only
        $itog = $amount = $summItogoRub = $oData['itogo'] = intval($summ);
        $noid = $oid = intval(time());
        $hash = md5($noid ); // ��� ��� ������ ����������
        $oData = array('id' => $noid , 'uPhone' => '', 'uName' => '', 'email' => '');
        $realCurrency = "RUB";
        $html = "";
    }
        if (!empty($oData['id'])){
                // ��������� ��� ��������� �����
            if ($otype === 'sbbank' || $otype === 'cash' || $otype === 'courier' || $otype === 'cards-courier')
            {


                $html.= '<FORM id="assist-form" ACTION="http://www.studiofloristic.dev/print-payment.php?id='.$hash.'" METHOD="GET">';
                $html.= '<INPUT TYPE="HIDDEN" NAME="id" VALUE="'.$hash.'" />';
                $html.= '<INPUT TYPE="HIDDEN" NAME="tp" VALUE="true" />';
                $html.= '</FORM>';
            }
            elseif (($otype === 'cards-assist') || ($otype === 'wm'))
            {
                $buttonText = '������� � ������';

                $payButton = '<input type="submit" form="assist-form" class="bigButton" id="payButton" value="'.$buttonText.'" />';
                $html.= '<FORM id="assist-form" ACTION="https://payments221.paysecure.ru/pay/order.cfm" METHOD="POST">';
                $html.= '<INPUT TYPE="HIDDEN" NAME="Merchant_ID" VALUE="662011" />';
                $html.= '<INPUT TYPE="HIDDEN" NAME="OrderNumber" VALUE="'.$oid.'" />';
                $html.= '<INPUT TYPE="HIDDEN" NAME="OrderAmount" VALUE="'.$itog.'" />';
                $html.= '<INPUT TYPE="HIDDEN" NAME="OrderCurrency" VALUE="'.$realCurrency .'">';
                $html.= '<INPUT TYPE="HIDDEN" NAME="TestMode" VALUE="0" />';
                $html.= '<INPUT TYPE="HIDDEN" NAME="OrderComment" VALUE="������ ������ �'.$oid.'" />';
                $html.= '</FORM>';

            }
            elseif ($otype === 'paypal')
            {
                $buttonText = '������� � ������';

                $payButton = '<input type="submit" form="paypal-form" class="bigButton" id="payButton" value="'.$buttonText.'" />';

                $html.= '<form id="paypal-form" method="post" action="https://www.paypal.com/cgi-bin/webscr" name="paypal">';
                $html.= '<input type="hidden" name="cmd" value="_xclick" />';
                $html.= '<input type="hidden" name="business" value="studiofloristic@gmail.com" />';
                $html.= '<input type="hidden" name="amount" value="'.$itog.'" />';
                $html.= '<input type="hidden" name="item_name" value="Order number '.$oid.'" />';
                $html.= '<input type="hidden" name="custom" value="'.$oid.'"/>';
                $html.= '<input type="hidden" name="no_shipping" value="0" />';
                $html.= '<input type="hidden" name="return" value="https://www.studiofloristic.ru/basket/?op=done" />';
                $html.= '<input type="hidden" name="cancel_return" value="https://www.studiofloristic.ru/basket/?op=issue" />';
                $html.= '<input type="hidden" name="undefined_quantity" value="0" />';
                $html.= '<input type="hidden" name="currency_code" value="RUB" />';
                $html.= '</form>';
            } elseif ($otype === 'w-all'){
                $html.= '<form id="walletone-form" action="https://wl.walletone.com/checkout/checkout/Index" method="post">';
                // ������������ ������� ��� W1
                $w1_fields = array();
                // ���������� ����� ����� � ������������� ������



                $w1_fields["WMI_MERCHANT_ID"]    = isset($_SESSION['w1id']) ? $_SESSION['w1id'] : W1_MERCHANT_ID4;

                if($realCurrency == 'USD' ){
                    $w1_fields["WMI_PAYMENT_AMOUNT"] = $summItogoRub.".00";
                }elseif($realCurrency == 'EUR'){
                    $w1_fields["WMI_PAYMENT_AMOUNT"] = $summItogoRub.".00";
                }else{
                    $w1_fields["WMI_PAYMENT_AMOUNT"] = $itog.".00";
                }



                $w1_fields["WMI_CURRENCY_ID"]    = W1_CURRENCY_ID;


                $w1_fields["WMI_PAYMENT_NO"]     = $oid;
                $w1_fields["WMI_DESCRIPTION"]    = "BASE64:".base64_encode( inUTF8("������ ������ �$oid") );
                $w1_fields["WMI_EXPIRED_DATE"]   = date('c', strtotime("+25 days"), time());
                $w1_fields["WMI_SUCCESS_URL"]    = W1_SUCCESS_URL;
                $w1_fields["WMI_FAIL_URL"]       = W1_FAIL_URL;



                if (!empty($oData['email'])) $w1_fields["WMI_CUSTOMER_EMAIL"]       = $oData['email']; // ����� ��������� �����



                // ������� ������ ������ - ���� ������, ����� �������� ��� ���������
                if (!empty($label)){
                    if (mb_strpos($label,',') !== false){
                        $labels = explode(',', $label);
                        $w1_fields["WMI_PTENABLED"] = $labels;
                    }else{
                        $w1_fields["WMI_PTENABLED"] = $label;
                    }

                }


                foreach($w1_fields as $name => $val)
                {
                    if(is_array($val))
                    {
                        usort($val, "strcasecmp");
                        $w1_fields[$name] = $val;
                    }
                }

                uksort($w1_fields, "strcasecmp");
                $w1fieldValues = "";

                foreach($w1_fields as $value)
                {
                    if(is_array($value))
                        foreach($value as $v)
                        {

                            $w1fieldValues .= $v;
                        }
                    else
                    {

                        $w1fieldValues .= $value;
                    }
                }

                $w1_fields["WMI_SIGNATURE"]  = base64_encode(pack("H*", md5($w1fieldValues. (isset($_SESSION['w1key']) ? $_SESSION['w1key']: W1_SECRET_KEY4)))); //���������� ��������� �������
                foreach($w1_fields as $w1key => $w1val){
                    if (is_array($w1val)){
                        foreach ($w1val as $pmntsRange){
                            $html .= '<input name="'.$w1key.'" type="hidden" value="'.$pmntsRange.'" />';
                        }
                    }else{
                        $html .= '<input name="'.$w1key.'" type="hidden" value="'.$w1val.'" />';
                    }

                }


                $html.= '</form>';
            }
            else
            {
                $buttonText = '������� � ������';

                $payButton = '<input type="submit" form="walletone-form" class="bigButton" id="payButton" value="'.$buttonText.'" />';
                switch ($otype) {
                    case 'cards':           $label = 'CreditCardBYR,CreditCardRUB,CreditCardUAH,CreditCardUSD,CreditCardEUR'; break;
                    case 'yandex':          $label = 'YandexMoneyRUB'; break;
                    case 'wm':              $label = 'WebMoneyRUB'; break;
                    case 'contact':         $label = 'EurosetRUB'; break; // ������� - ��� ��������
                    case 'euroset':         $label = 'SvyaznoyRUB'; break; // �������� - ��� ������� :) ���...
                    case 'terminals':       $label = 'CashTerminal'; break;
                    case 'onlinebanks':     $label = 'AlfaclickRUB,TinkoffRUB,Privat24UAH,PsbRetailRUB,SberOnlineRUB,FakturaruRUB,RsbRUB,EripBYR,SetcomSidZAR,StandardBankEftZ'; break;
                    case 'mobileapp':       $label = 'WalletOne'; break;
                    case 'mobileoperators': $label = 'MegafonRUB,BeelineRUB,MtsRUB,Tele2RUB,KievStarUAH'; break;
                    case 'emoney':          $label = 'LiqPayMoney,OkpayRUB,QiwiWalletRUB,WalletOneRUB,UniMoneyRUB,BPayMDL,GoogleWalletUSD'; break;
                }

                $html.= '<form id="walletone-form" action="https://wl.walletone.com/checkout/checkout/Index" method="post">';
                // ������������ ������� ��� W1
                $w1_fields = array();
                // ���������� ����� ����� � ������������� ������



                $w1_fields["WMI_MERCHANT_ID"]    = isset($_SESSION['w1id']) ? $_SESSION['w1id'] : W1_MERCHANT_ID4;

                if($realCurrency == 'USD' ){
                    $w1_fields["WMI_PAYMENT_AMOUNT"] = $summItogoRub.".00";
                }elseif($realCurrency == 'EUR'){
                    $w1_fields["WMI_PAYMENT_AMOUNT"] = $summItogoRub.".00";
                }else{
                    $w1_fields["WMI_PAYMENT_AMOUNT"] = $itog.".00";
                }



                $w1_fields["WMI_CURRENCY_ID"]    = W1_CURRENCY_ID;


                $w1_fields["WMI_PAYMENT_NO"]     = $oid;
                $w1_fields["WMI_DESCRIPTION"]    = "BASE64:".base64_encode( inUTF8("������ ������ �$oid") );
                $w1_fields["WMI_EXPIRED_DATE"]   = date('c', strtotime("+25 days"), time());
                $w1_fields["WMI_SUCCESS_URL"]    = W1_SUCCESS_URL;
                $w1_fields["WMI_FAIL_URL"]       = W1_FAIL_URL;
                /**
                 * ���� ����� �� �������, ����� ������� ��������� ��� ��������� ���� W1 ��������
                 */

                   // if (!empty($oData['uPhone'])) $w1_fields["WMI_CUSTOMER_PHONE"]       = $oData['uPhone']; // ����� ��������� �������



                    if (!empty($oData['email'])) $w1_fields["WMI_CUSTOMER_EMAIL"]       = $oData['email']; // ����� ��������� �����



                // ������� ������ ������ - ���� ������, ����� �������� ��� ���������
                if (!empty($label)){
                    if (mb_strpos($label,',') !== false){
                        $labels = explode(',', $label);
                        $w1_fields["WMI_PTENABLED"] = $labels;
                    }else{
                        $w1_fields["WMI_PTENABLED"] = $label;
                    }

                }

                //���������� �������� ������ �����
                foreach($w1_fields as $name => $val)
                {
                    if(is_array($val))
                    {
                        usort($val, "strcasecmp");
                        $w1_fields[$name] = $val;
                    }
                }
                // ������������ ���������, ����� ����������� �������� �����,
                // ��������������� �� ������ ������ � ������� �����������.
                uksort($w1_fields, "strcasecmp");
                $w1fieldValues = "";

                foreach($w1_fields as $value)
                {
                    if(is_array($value))
                        foreach($value as $v)
                        {
                            //����������� �� ������� ��������� (UTF-8)
                            //���������� ������ ���� ��������� �������� ������� �� Windows-1251
                            //$v = iconv("utf-8", "windows-1251", $v);
                            $w1fieldValues .= $v;
                        }
                    else
                    {
                        //����������� �� ������� ��������� (UTF-8)
                        //���������� ������ ���� ��������� �������� ������� �� Windows-1251
                        //$value = iconv("utf-8", "windows-1251", $value);
                        $w1fieldValues .= $value;
                    }
                }

                $w1_fields["WMI_SIGNATURE"]  = base64_encode(pack("H*", md5($w1fieldValues. (isset($_SESSION['w1key']) ? $_SESSION['w1key']: W1_SECRET_KEY4)))); //���������� ��������� �������
                foreach($w1_fields as $w1key => $w1val){
                    if (is_array($w1val)){
                        foreach ($w1val as $pmntsRange){
                            $html .= '<input name="'.$w1key.'" type="hidden" value="'.$pmntsRange.'" />';
                        }
                    }else{
                        $html .= '<input name="'.$w1key.'" type="hidden" value="'.$w1val.'" />';
                    }

                }

                // ������� ������ ���������
                //  if (!empty($label)) {
                //		$html.= '<input name="IncCurrLabel" type="hidden" value="'.$label.'" />';
                //	}
                $html.= '</form>';
            }
        }



    echo $html;
    die;
}

function getRealPrices(){

    echo json_encode($_SESSION['real_prices']);
    die;
}

function setCupon() {
    $cupon = filter(trim(in1251($_GET['cupon'])), 'nohtml');
    $cupon = query_new("SELECT * FROM sw_coupons WHERE `count` > 0 AND `active` = 1 AND title=" . quote($cupon), 1);
    if (empty($cupon)) {
        jQuery::evalScript(inUTF8('initSysDialog("����� �� ������ �� ������")'));
    } else {


        $u_discount = isset($_SESSION['discount']) ?  abs(intval($_SESSION['discount'])) : 0; // ������ ������������ ������

            if ($u_discount < $cupon['value'])
            {
                $_SESSION['cupon'] = $cupon['value'];
                $_SESSION['discount'] = $cupon['value'];
                db_query('UPDATE `sw_coupons` SET `count` = ( `count` - 1 )  WHERE title=' . quote($cupon['title']) . ' AND `count` > 0 ;');
                jQuery::evalScript('location="/basket.html"');
            }
            else{
                $alertStr = '���� ������� ������ ('.$u_discount .'%) ��������� ������ ������ �� ���������� ������ ('.$cupon['value'].'%). ������ �������� �������!';
                jQuery::evalScript(inUTF8('initSysDialog("'.$alertStr .'")'));
            }


    }
    jQuery::getResponse();
}

function delivery_photo($ret = false) {
    $photos = query_new('SELECT * FROM `sw_photo_delivery` WHERE `active` = 1');
    $html   = '';
    if (is_array($photos) && count($photos) > 0) {
        foreach ($photos as $item) {
            $imgs = !empty($item['delivery_photo']) && is_string($item['delivery_photo']) ? explode(':', $item['delivery_photo']) : false;
            $html .= carusel_list($imgs, $item['id']);
        }
        if ($html != '') {
            $html = '<div id="delivery_photo_cont"><div class="delivery_photo"><div class="closeNew"></div><span class="title">���� � ��������</span>'
                    . '<div id="delivery_photo_carusel"><ul class="sliderSmall jcarousel-skin-tangoSm">' . $html . '</ul></div></div></div>';
            if (empty($ret)) {
                jQuery('body')->append(inUTF8($html));
                jQuery::evalScript("deliveryPhoto.load()");
            }
        }
    }
    if (empty($ret)) {
        jQuery::getResponse();
    } else {
        return $html;
    }
}

function carusel_list($imgs, $id) {
    $dis  = '';
    $path = 'files/photo_delivery/' . $id . '/';
    if (is_array($imgs) && !empty($imgs)) {
        foreach ($imgs as $img) {
            if (file_exists($path . $img)) {
                $size = getimagesize($path . $img);
                if (($size[0] / $size[1]) > (168 / 192)) {
                    $pre      = $path . 'w_' . $img;
                    $size     = getimagesize($pre);
                    $style    = ' style="left:' . ((168 - $size[0]) / 2) . 'px;" ';
                    $over_out = ' onmouseover="deliveryPhoto.overOut(this,168,' . ($size[1] * 168 / $size[0]) . ')" onmouseout="deliveryPhoto.overOut(this,' . $size[0] . ',192)" ';
                } else {
                    $pre      = $path . 'w168_' . $img;
                    $size     = getimagesize($pre);
                    $style    = ' style="top:' . ((192 - $size[1]) / 2) . 'px;" ';
                    $over_out = ' onmouseover="deliveryPhoto.overOut(this,' . ($size[0] * 192 / $size[1]) . ',192)" onmouseout="deliveryPhoto.overOut(this,168,' . $size[1] . ')" ';
                }
                $dis .= '<li ' . $over_out . '><a href="/' . $path . 'w600_' . $img . '" ><img nopin="nopin" ' . $style . ' src="/' . $pre . '" /></a></li>';
            }
        }
    }
    return $dis;
}


function initVaseRemove() {
    global $_GLOBALS;

    if (!empty($_GET['c'])) {
        $c         = abs(intval($_GET['c']));
        $b         = abs(intval($_GET['b']));
        $vasePrice         = abs(intval($_GET['vaseprice']));
        $vaseid         = abs(intval($_GET['vaseid']));
        $box_price = array(
            0 => 0,
            1 => $_GLOBALS['v_execution-1'],
            2 => $_GLOBALS['v_execution-2'],
            3 => $_GLOBALS['v_execution-3']
        );
        $tovar     = query_new('SELECT `price`,`amount`,`box` FROM `sw_basket` WHERE `id` = ' . $c . ' AND `session` = ' . quote(session_id()), 1);
    }
    if (!empty($tovar)) {
        $tovar['amount'] = $tovar['price'] + $box_price[intval($tovar['box'])] ;
        db_query('UPDATE `sw_basket` SET `vase` = 0, `amount` = ' . $tovar['amount'] . ' WHERE `id` = ' . quote($c) . ' AND `session` = ' . quote(session_id()));
    }
    $basket_info   = basket_info();
    $_GET['price'] = $basket_info['mark_disc_summ'];
    giftsMore();
}


/*
 * ����� ����� ������� ���, ������� ���� ���������� ����� ���������� ����
 */
function initVaseNew() {
    global $_GLOBALS;

    if (!empty($_GET['c'])) {
        $c         = abs(intval($_GET['c']));
        $b         = abs(intval($_GET['b']));
        $vasePrice         = abs(intval($_GET['vaseprice']));
        $vaseid         = abs(intval($_GET['vaseid']));
        $box_price = array(
            0 => 0,
            1 => $_GLOBALS['v_execution-1'],
            2 => $_GLOBALS['v_execution-2'],
            3 => $_GLOBALS['v_execution-3']
        );
        $tovar     = query_new('SELECT `price`,`amount`,`box` FROM `sw_basket` WHERE `id` = ' . $c . ' AND `session` = ' . quote(session_id()), 1);
    }
    if (!empty($tovar)) {
        $tovar['amount'] = $tovar['price'] + $box_price[intval($tovar['box'])] + ((empty($b)) ? 0 : $vasePrice );
        db_query('UPDATE `sw_basket` SET `vase` = ' . $vaseid . ', `amount` = ' . $tovar['amount'] . ' WHERE `id` = ' . quote($c) . ' AND `session` = ' . quote(session_id()));
    }
    $basket_info   = basket_info();
    $_GET['price'] = $basket_info['mark_disc_summ'];
    giftsMore();
}

function initVase() {
    global $_GLOBALS;

    if (!empty($_GET['c'])) {
        $c         = abs(intval($_GET['c']));
        $b         = abs(intval($_GET['b']));
        $box_price = array(
            0 => 0,
            1 => $_GLOBALS['v_execution-1'],
            2 => $_GLOBALS['v_execution-2'],
            3 => $_GLOBALS['v_execution-3']
        );
        $tovar     = query_new('SELECT `price`,`amount`,`box` FROM `sw_basket` WHERE `id` = ' . $c . ' AND `session` = ' . quote(session_id()), 1);
    }
    if (!empty($tovar)) {
        $tovar['amount'] = $tovar['price'] + $box_price[intval($tovar['box'])] + (empty($b) ? 0 : abs(intval($_GLOBALS['v_vase-price'])));
        db_query('UPDATE `sw_basket` SET `vase` = ' . $b . ', `amount` = ' . $tovar['amount'] . ' WHERE `id` = ' . quote($c) . ' AND `session` = ' . quote(session_id()));
    }
    $basket_info   = basket_info();
    $_GET['price'] = $basket_info['mark_disc_summ'];
    giftsMore();
}

function initBox() {
    global $_GLOBALS;

    if (!empty($_GET['c'])) {
        $c         = abs(intval($_GET['c']));
        $b         = abs(intval($_GET['b']));
        $box_price = array(
            0 => 0,
            1 => $_GLOBALS['v_execution-1'],
            2 => $_GLOBALS['v_execution-2'],
            3 => $_GLOBALS['v_execution-3']
        );
        $tovar     = query_new('SELECT `price`,`amount`,`vase` FROM `sw_basket` WHERE `id` = ' . quote($c) . ' AND `session` = ' . quote(session_id()), 1);
    }
    if (!empty($tovar)) {
        $tovar['amount'] = $tovar['price'] + intval($box_price[$b]) + ((!empty($tovar['vase'])) ? abs(intval($_GLOBALS['v_vase-price'])) : 0);
        db_query('UPDATE `sw_basket` SET `box` = ' . quote($b) . ', `boxPrice` = ' . quote($box_price[$b]) . ', `amount` = \'' . $tovar['amount'] . '\' WHERE `id` = ' . quote($c) . ' AND `session` = ' . quote(session_id()));
    }
    $basket_info   = basket_info();
    $_GET['price'] = $basket_info['mark_disc_summ'];
    giftsMore();
}

function giftsLoad() {
    if (empty($_GET['id']))
        jQuery::getResponse();

    $id   = abs(intval($_GET['id']));
    $html = '';

    $r          = db_query('SELECT c.`id`, s.`sid`, c.`title`, c.`price`, c.`image`, s.`url` FROM `sw_catalog_section` as s LEFT JOIN `sw_catalog` as c ON s.`cid` = c.`id` WHERE s.`sid` = ' . quote($id) . ' AND c.`active` = 1 AND c.`id` IS NOT NULL AND s.basket_show=1 ORDER BY c.`views` DESC');
    $gifts_list = array();
    while ($c          = mysql_fetch_array($r, MYSQL_ASSOC)) {
        $images = explode(':', $c['image']);
        if (!empty($c['text'])) {
            $text = substr(strip_tags($c['text']), 0, 100);
            $text = htmlspecialchars(substr($text, 0, strrpos($text, ' ')) . '...', ENT_COMPAT | ENT_HTML401, 'cp1251');
        } else
            $text         = null;
        $gift         = '<li>'
                . '<a href="javascript:fast.info(' . $c['id'] . ',' . $c['sid'] . ')" class="haveTooltip" title=\'<span class="h2_alt">' . htmlspecialchars($c['title'], ENT_COMPAT | ENT_HTML401, 'cp1251') . '</span>\'>'
                . '<img class="dimage-show-item" dimage_src="/files/catalog/' . $c['id'] . '/w128_' . $images[0] . '" width="128" height="116" />'
                . '<span>' . $c['price'] . '�</span>'
                . '</a></li>';
        $gifts_list[] = $gift;
    }

    $gifts_list = array_chunk($gifts_list, 15);
    $nav_bar    = '<div class="new_slider_nav_bar">';
    $new_slider = '<div class="page_slider">';
    foreach ($gifts_list as $key => $page) {
        if (count($gifts_list) > 1) {
            $nav_bar .= '<span class="nav_bar_switch nav_bar_' . $key . '" onclick="gifts.switchPage(' . $key . ')"></span>';
        }

        $rows = array_chunk($page, 4);
        $new_slider .= '<div id="new_slider_page_' . $key . '" class="new_slider_page"><ul class="dimage-show-cont new_slider_line ">';
        foreach ($rows as $row_key => $item) {
            //TODO: �������� ������ ������ ����� UL � ������� ������ ����
           // $new_slider .= '<ul class="dimage-show-cont new_slider_line line_' . $row_key . '">' . implode('', $item) . '</ul>';
            $new_slider .= '' . implode('', $item) . '';
        }
        $new_slider .='</ul></div>';
    }
    $html .= $nav_bar . '</div>' . $new_slider . '</div>' . $nav_bar . '</div>';

    jQuery('div#tabs-data')->html(inUTF8($html));
    jQuery::evalScript('gifts.carusel()');
    jQuery::evalScript('gifts.goSwipe()');
    jQuery::evalScript('gifts.switchPage()');
    jQuery::getResponse();
}

function intervalPrice() {
    global $cur, $COOKIE_REPLACEMENT;

    if (empty($_GET['id'])) {
        jQuery::getResponse();
    }
    $COOKIE_REPLACEMENT = true;
    $basket_info        = basket_info(array(), '', array('info' => 'full'));
    $datetime           = $basket_info['datetime'];
    $deliveres          = $basket_info['deliveres'];
    $addes              = $basket_info['addes_prices'];
    $summ_sell          = (holl_markup($datetime['date']) === false) ? 'discount' : 'mark_disc';

    jQuery('#del_info_price')->html(inUTF8($basket_info[$summ_sell . '_summ'] . '<span class="reduction">' . $cur['reduction'] . '</span>'));


    $del_radios = array();
    for ($i = 1; $i < 6; $i++) {
        $val                    = $deliveres['delivery_' . $i]['value'];
        $off                    = ($val === 'disabled') ? $val : '';
        $del_radios[$i]['off']  = $off;
        $val                    = intval($val) > 0 ? $val : 0;
        $del_radios[$i]['val']  = $val;
        $del_radios[$i]['span'] = (($val + $addes['pay_tochdos']) == 0) ? '<span class="text_color_orange"><b>���������</b></span>' : '+' . ($val + $addes['pay_tochdos']) . $cur['reduction'];
    }
    if (empty($addes['pay_tochdos'])) {
        jQuery('#ov_mkad_tochdos')->hide();
    } else {
        jQuery('#ov_mkad_tochdos')->show();
    }
    jQuery('#diff_days')->attr('totp_value', $basket_info['discounts']['diff_days']);
    if ($basket_info['deliveres']['del_active'] === false) {
        jQuery('#del_2')->prop('checked', true);
        jQuery('#del_2')->trigger('click');
        jQuery('#del_1')->attr('disabled', true);
        jQuery('#del_5')->attr('disabled', true);
        jQuery('#del_1_cont')->addClass('disabled');
        jQuery('#del_5_cont')->addClass('disabled');
        jQuery('#del_3')->attr('disabled', true);
        jQuery('#del_3_cont')->addClass('disabled');
        jQuery('#del_4, .mkad_over_input>input')->attr('disabled', true);
        jQuery('#del_4_cont')->addClass('disabled');
        jQuery('#mkad_pass_lenght')->attr('disabled', true);
        jQuery('.delivery_off')->show();
    } else {
        jQuery('input[name="delivery"], .mkad_over_input>input')->attr('disabled', false);
        jQuery('#del_1_cont')->removeClass('disabled');
        jQuery('#del_5_cont')->removeClass('disabled');
        jQuery('#del_1')->attr('totp_value', $del_radios[1]['val']);
        jQuery('#del_5')->attr('totp_value', $del_radios[5]['val']);
        jQuery('span#courer')->html(inUTF8($del_radios[1]['span']));
        jQuery('#del_3_cont')->removeClass('disabled');
        jQuery('#del_3')->attr('totp_value', $del_radios[3]['val']);
        jQuery('span#courer-okr')->html(inUTF8($del_radios[3]['span']));
        jQuery('span.courer-okr')->html(inUTF8($deliveres['mkad_mkad']));
        jQuery::evalScript('basket.mkad_mkad = ' . $deliveres['mkad_mkad']);
        jQuery('#del_4_cont')->removeClass('disabled');
        jQuery('#del_4')->attr('totp_value', $del_radios[4]['val']);
        jQuery('.delivery_off')->hide();
    }
    jQuery('input[name=date]')->attr('disabled', false);
    jQuery('input[name=date]')->blur();
    jQuery::evalScript("basket.mkadOver()");
    jQuery::getResponse();
}


function intervalPriceWOMkad() {
    global $cur, $COOKIE_REPLACEMENT;

    if (empty($_GET['id'])) {
        jQuery::getResponse();
    }
    $COOKIE_REPLACEMENT = true;
    $basket_info        = basket_info(array(), '', array('info' => 'full'));
    $datetime           = $basket_info['datetime'];
    $deliveres          = $basket_info['deliveres'];
    $addes              = $basket_info['addes_prices'];
    $summ_sell          = (holl_markup($datetime['date']) === false) ? 'discount' : 'mark_disc';

    jQuery('#del_info_price')->html(inUTF8($basket_info[$summ_sell . '_summ'] . '<span class="reduction">' . $cur['reduction'] . '</span>'));


    $del_radios = array();
    for ($i = 1; $i < 6; $i++) {
        $val                    = $deliveres['delivery_' . $i]['value'];
        $off                    = ($val === 'disabled') ? $val : '';
        $del_radios[$i]['off']  = $off;
        $val                    = intval($val) > 0 ? $val : 0;
        $del_radios[$i]['val']  = $val;
        $del_radios[$i]['span'] = (($val + $addes['pay_tochdos']) == 0) ? '<span class="text_color_orange"><b>���������</b></span>' : '+' . ($val + $addes['pay_tochdos']) . $cur['reduction'];
    }
    if (empty($addes['pay_tochdos'])) {
        jQuery('#ov_mkad_tochdos')->hide();
    } else {
        jQuery('#ov_mkad_tochdos')->show();
    }
    jQuery('#diff_days')->attr('totp_value', $basket_info['discounts']['diff_days']);
    if ($basket_info['deliveres']['del_active'] === false) {
        jQuery('#del_2')->prop('checked', true);
        jQuery('#del_2')->trigger('click');
        jQuery('#del_1')->attr('disabled', true);
        jQuery('#del_5')->attr('disabled', true);
        jQuery('#del_1_cont')->addClass('disabled');
        jQuery('#del_5_cont')->addClass('disabled');
        jQuery('#del_3')->attr('disabled', true);
        jQuery('#del_3_cont')->addClass('disabled');
        jQuery('#del_4, .mkad_over_input>input')->attr('disabled', true);
        jQuery('#del_4_cont')->addClass('disabled');
        jQuery('#mkad_pass_lenght')->attr('disabled', true);
        jQuery('.delivery_off')->show();
    } else {
        jQuery('input[name="delivery"], .mkad_over_input>input')->attr('disabled', false);
        jQuery('#del_1_cont')->removeClass('disabled');
        jQuery('#del_5_cont')->removeClass('disabled');
        jQuery('#del_5')->attr('totp_value', $del_radios[5]['val']);
        jQuery('#del_1')->attr('totp_value', $del_radios[1]['val']);
        jQuery('span#courer')->html(inUTF8($del_radios[1]['span']));
        jQuery('#del_3_cont')->removeClass('disabled');
        jQuery('#del_3')->attr('totp_value', $del_radios[3]['val']);
        jQuery('span#courer-okr')->html(inUTF8($del_radios[3]['span']));
        jQuery('span.courer-okr')->html(inUTF8($deliveres['mkad_mkad']));
       // jQuery::evalScript('basket.mkad_mkad = ' . $deliveres['mkad_mkad']);
        jQuery('#del_4_cont')->removeClass('disabled');
        jQuery('#del_4')->attr('totp_value', $del_radios[4]['val']);
        jQuery('.delivery_off')->hide();
    }
    jQuery('input[name=date]')->attr('disabled', false);
    jQuery('input[name=date]')->blur();
  //  jQuery::evalScript("basket.mkadOver()");
    jQuery::getResponse();
}

function courerPrice() {
    global $cur, $COOKIE_REPLACEMENT;

    if (!empty($_GET['time'])) {
        $time = filter(trim($_GET['time']), 'nohtml');
        if (!preg_match("/^([0-9]{2})\:([0-9]{2})$/i", $time, $t) || $t[1] >= 24 || $t[2] >= 60) {
            jQuery::evalScript('basket.noDelivery()');
            jQuery::getResponse();
        }
        $interval = query_new("SELECT * FROM sw_delivery sd WHERE sd.start <= '" . $time . "' AND sd.end > '" . $time . "'", 1);
        if ($interval === false) {
            $interval = query_new("SELECT * FROM sw_delivery sd WHERE sd.title REGEXP '" . $time . "'", 1);
            if ($interval === false) {
                jQuery::evalScript('basket.noDelivery()');
                jQuery::getResponse();
            }
        }
        $_COOKIE['issue_interval'] = $interval['id'];
        setcookie('issue_interval', $interval['id'], 0, '/basket/', _CROSS_DOMAIN);

        $COOKIE_REPLACEMENT = true;
        $basket_info        = basket_info(array(), '', array('info' => 'full'));
        $datetime           = $basket_info['datetime'];
        $deliveres          = $basket_info['deliveres'];
        $addes              = $basket_info['addes_prices'];
        $summ_sell          = (holl_markup($datetime['date']) === false) ? 'discount' : 'mark_disc';

        jQuery('#del_info_price')->html(inUTF8($basket_info[$summ_sell . '_summ'] . '<span class="reduction">' . $cur['reduction'] . '</span>'));
        $del_radios = array();
        for ($i = 1; $i < 6; $i++) {
            $val                    = $deliveres['delivery_' . $i]['value'];
            $off                    = ($val === 'disabled') ? $val : '';
            $del_radios[$i]['off']  = $off;
            $val                    = intval($val) > 0 ? $val : 0;
            $del_radios[$i]['val']  = $val;
            $del_radios[$i]['span'] = (($val + $addes['pay_tochdos']) == 0) ? '<span class="text_color_orange"><b>���������</b></span>' : '+' . ($val + $addes['pay_tochdos']) . $cur['reduction'];
        }
        jQuery('#diff_days')->attr('totp_value', $basket_info['discounts']['diff_days']);
        if (empty($addes['pay_tochdos'])) {
            jQuery('#ov_mkad_tochdos')->hide();
        } else {
            jQuery('#ov_mkad_tochdos')->show();
        }
        if ($basket_info['deliveres']['del_active'] === false) {
            jQuery('#del_2')->prop('checked', true);
            jQuery('#del_2')->trigger('click');
            jQuery('#del_1')->attr('disabled', true);
            jQuery('#del_5')->attr('disabled', true);
            jQuery('#del_1_cont')->addClass('disabled');
            jQuery('#del_5_cont')->addClass('disabled');
            jQuery('#del_3')->attr('disabled', true);
            jQuery('#del_3_cont')->addClass('disabled');
            jQuery('#del_4')->attr('disabled', true);
            jQuery('#del_4_cont')->addClass('disabled');
            jQuery('#mkad_pass_lenght')->attr('disabled', true);
            jQuery('.delivery_off')->show();
        } else {
            jQuery('input[name="delivery"]')->attr('disabled', false);
            jQuery('#del_1_cont')->removeClass('disabled');
            jQuery('#del_5_cont')->removeClass('disabled');
            jQuery('#del_1')->attr('totp_value', $del_radios[1]['val']);
            jQuery('#del_5')->attr('totp_value', $del_radios[5]['val']);
            jQuery('span#courer')->html(inUTF8($del_radios[1]['span']));
            jQuery('#del_3_cont')->removeClass('disabled');
            jQuery('#del_3')->attr('totp_value', $del_radios[3]['val']);
            jQuery('span#courer-okr')->html(inUTF8($del_radios[3]['span']));
            jQuery('span.courer-okr')->html(inUTF8($deliveres['mkad_mkad']));
            jQuery::evalScript('basket.mkad_mkad = ' . $deliveres['mkad_mkad']);
            jQuery('#del_4_cont')->removeClass('disabled');
            jQuery('#del_4')->attr('totp_value', $del_radios[4]['val']);
            jQuery('.delivery_off')->hide();
        }
        jQuery('select[name="interval"] option[value="' . $interval['id'] . '"]')->attr("selected", "selected");
    }
    jQuery::evalScript("basket.mkadOver()");
    jQuery::getResponse();
}

function orderDone() {
    global $_GLOBALS, $cur;

    if (!empty($_SESSION['order_hash1'])) {
        $o = query_new('SELECT * FROM `sw_orders` WHERE `hash` = ' . quote($_SESSION['order_hash1']) . ' LIMIT 1', 1);
    }
    if (empty($o) || (empty($o['status']) && $_REQUEST['type'] !== 'cccs')) {
        HeaderPage('/basket/op=issue');
    }
    $oid  = $o['id'];
    $hash = md5($oid);

    if ($o['type'] == 'sbbank') {
        $title = '�������! ��� ����� ������.';
        $html  = '<div style="padding:0 16px;"><p>�� ������� ������ ������ ����� ��������. � ��� ���� 2 ��������:</p><ol>';
        $html .= '<li>� ������ �������� ���������� �� ���� � ����� ��������� ��������� � ����������� � ������������� ��� ����� (������� ����� 2-3 ���������� ���, ������� ��� �������������� ���������� ������ ������ ������ ��� ��������� ���� (��� ����) ������� �� �����: <a href="mailto:' . $_GLOBALS['v_email_admin'] . '">' . $_GLOBALS['v_email_admin'] . '</a>).<br></li>';
        $html .= '<li>���� �� ��������� ���������� ��������� ����� ���������, �� �� ������ �������� ����� ����� �������� ���������, ��������� ���������, ��������� � ���������. (������� ����� 2-3 ���������� ���, ������� ��� �������������� ���������� ������ ������ ������ ��� ��������� ���� (��� ����) ���� �� �����: <a href="mailto:' . $_GLOBALS['v_email_admin'] . '">' . $_GLOBALS['v_email_admin'] . '</a>).</li>';
        $html .= '</ol>';
        $html .= '<p align="center"><a class="zakazButton" href="/print-payment.php?id=' . $hash . '" target="_blank">������ ���������� ���������</a></p>';
        $html .= '<p style="text-align: center;">
                    <div id="content-data">
                    <div class="text-align-center">
                    <div class="giant_title">���������� ��� �� �����!</div>
                    <p>&nbsp;</p>
                    <div class="h2_alt">����� ������ ������: '.$oid.'</div>
                    <p>���� ��������� �������� � ���� � ��������� �����, �� ���������� �������.</p>
                    <p>&nbsp;</p>
                    <p style="padding-left: 30px;padding-right: 30px;"><img class="full-width border-radius" src="/i/smski-3-new.jpg" alt="" /></p>
                    </div>
                    </div>
                  </p></div>';
    } else {
        $title = '';
        $html  = '<p style="text-align: center;"><div id="content-data">
                    <div class="text-align-center">
                    <div class="giant_title">���������� ��� �� �����!</div>
                    <p>&nbsp;</p>
                    <div class="h2_alt">����� ������ ������: '.$oid.'</div>
                    <p>���� ��������� �������� � ���� � ��������� �����, �� ���������� �������.</p>
                    <p>&nbsp;</p>
                    <p style="padding-left: 30px;padding-right: 30px;"><img class="full-width border-radius" src="/i/smski-3-new.jpg" alt="" /></p>
                    </div>
                    </div>
                   </p>';
    }

    clearCart();

    $_GLOBALS['title']      = $title;
    $_GLOBALS['text']       = $html;
    $_GLOBALS['html_title'] = '����� �������� - ' . $_GLOBALS['v_sitename'];
    $_GLOBALS['template']   = 'default';
}

function clearCart() {
    db_delete('basket_order', '`session` = ' . quote(session_id()));
    db_delete('basket', '`session` = ' . quote(session_id()));

    setcookie('order_hash1', null, null, '/', _CROSS_DOMAIN);
	$_SESSION['order_hash1'] = null;
	unset($_SESSION['order_hash1']);
    cookie_fields_clear('issue_', '/basket/');
    cookie_fields_clear('issue_', '/');
}

function paymentOrder() {
    global $_GLOBALS, $SMS4B, $cur;

    unset($_GLOBALS['pre_closes_includes']['counters']);
    unset($_GLOBALS['pre_closes_includes']['rambler-counter']);
    unset($_GLOBALS['pre_closes_includes']['googleads']);
    unset($_GLOBALS['pre_closes_includes']['googleadservices']);
    unset($_GLOBALS['pre_closes_includes']['pinit']);
    unset($_GLOBALS['pre_closes_includes']['passadvice']);
    unset($_GLOBALS['pre_closes_includes']['saletex']);
    unset($_GLOBALS['pre_closes_includes']['jivosite']);
    unset($_GLOBALS['pre_closes_includes']['yandex-metrika']);
    #unset($_GLOBALS['pre_closes_includes']['google-analytics']);
    #unset($_GLOBALS['pre_opens_includes']['google-analytics']);

    if (!empty($_SESSION['order_hash1'])){
        $o = query_new('SELECT * FROM `sw_orders` WHERE `hash` = '.quote($_SESSION['order_hash1']).' LIMIT 1',1);
        if (!empty($o) && !empty($o['status'])){
            HeaderPage('/basket/op=done');
        }
    }

    if (empty($o)){
        HeaderPage('/basket/op=issue');
    }
    $oid = $o['id'];

    $basket_info = basket_info(array(),'',array('info'=>'full'));
    $discounts = $basket_info['discounts'];
    $datetime = $basket_info['datetime'];
    $deliveres = $basket_info['deliveres'];
    $addes = $basket_info['addes_prices'];
    $b = $basket_info['basket_order'];
    $basket_items = $basket_info['products'];

    if ($b === false){
        HeaderPage('/basket.html');
    }
    $box = array(
        0=>'����� (���������)',
        1=>'�������� ����� ('.ceil($_GLOBALS['v_execution-1']/$cur['value']).$cur['reduction'].')',
        2=>'����������� �������� ('.ceil($_GLOBALS['v_execution-2']/$cur['value']).$cur['reduction'].')',
        3=>'���������� �������� ('.ceil($_GLOBALS['v_execution-3']/$cur['value']).$cur['reduction'].')',
    );

    $holi_text = '';
    $itog = $basket_info['discount_summ_full'];
    $holiday = holl_markup($datetime['date']);
    if ($holiday !==false){
        $itog = $basket_info['mark_disc_summ_full'];
        $tmp = explode('|', $holiday['title']);
        foreach ($tmp as $key=>$var){
            $tmp[$key] = '<span class="tit_text">'.$var.'</span>';
        }
        $holiday['title'] = implode('',$tmp);
        $holi_text =  '<div class="tbb_row holiday_order_row"><table><tr><td><div class="tbb_text_item">'
        . '<div class="bigBlocktext">'.$holiday['title'].'</div></div></td>'
        . '</tr></table></div>';
    }
    $html = '<div class="oplataBlock blockWithTopLine">'
            . '<h2>��� ����� � '. $oid .' ������!</h2><br><br>'
            . '<span class="h2_alt">������ ������:</span>'
            . '<table border="0" cellpadding="0" cellspacing="0"><tbody>';

    if (is_array($basket_items) && count($basket_items)>0) {
        foreach ($basket_items as $c) {
            $amount = ($holiday!==false)? $c['markup_amount']:$c['amount'];
            $image = null;
            switch ($c['type']) {
                case 'my-bouquet': $image = '/i/myBouquet-70.gif'; break;
                case 'my-order': $image = '/i/individ.jpg'; break;
                default:
                    if (!empty($c['image'])) {
                        $image = explode(':', $c['image']);
                        $image = '/files/catalog/'.$c['cid'].'/w70_'.$image[0];
                    }
            }
            // ������� ��� ����������� ����� ���

            if ($c['vase'] > 0){
                // ������� ������ ����
                $curvase = query_new("SELECT * FROM sw_vases WHERE `status` = 1 AND id = ". intval($c['vase']), 1);
                if ($curvase['id'] > 0){
                    $html.= '<tr>'
                        . '<td>'
                        . '<div class="liBlock">'
                        . '<a href="'.$c['url'].'" target="_blank" class="pLine haveTooltip" title="'.htmlspecialchars($c['text'], ENT_COMPAT | ENT_HTML401, 'cp1251').'">'.$c['title'].'</a>'
                        . '<div><img src="'.$image.'" /></div>'
                        . '</div>'
                        .(!empty($c['box']) ? '<p>+ '.$box[$c['box']].'</p>' : null)
                        .(!empty($c['vase']) ? '<p class="haveTooltip" title="<img src=\'/files/vases/'.$curvase['id'].'/'.$curvase['picture'].'\' align=\'left\' width=200 />">+ ���� "'.$curvase['title'].'" ('.ceil($curvase['price']/$cur['value']).$cur['reduction'].')</p>' : null)
                        .'</td>'
                        . '<td class="tableCost">'
                        .$amount.'<span class="reduction">'.$cur['reduction'].'</span>'.($c['count'] > 1 ? ' x '.$c['count'].'�� = '.($amount * $c['count']).'<span class="reduction">'.$cur['reduction'].'</span>' : null)
                        .'</td></tr>';
                }else{
                    $html.= '<tr>'
                        . '<td>'
                        . '<div class="liBlock">'
                        . '<a href="'.$c['url'].'" target="_blank" class="pLine haveTooltip" title="'.htmlspecialchars($c['text'], ENT_COMPAT | ENT_HTML401, 'cp1251').'">'.$c['title'].'</a>'
                        . '<div><img src="'.$image.'" /></div>'
                        . '</div>'
                        .(!empty($c['box']) ? '<p>+ '.$box[$c['box']].'</p>' : null)
                        .(!empty($c['vase']) ? '<p class="haveTooltip" title="<img src=\'/i/vaza.jpg\' align=\'left\' />">+ ���� ('.ceil($_GLOBALS['v_vase-price']/$cur['value']).$cur['reduction'].')</p>' : null)
                        .'</td>'
                        . '<td class="tableCost">'
                        .$amount.'<span class="reduction">'.$cur['reduction'].'</span>'.($c['count'] > 1 ? ' x '.$c['count'].'�� = '.($amount * $c['count']).'<span class="reduction">'.$cur['reduction'].'</span>' : null)
                        .'</td></tr>';
                }
            }else{
                $html.= '<tr>'
                    . '<td>'
                    . '<div class="liBlock">'
                    . '<a href="'.$c['url'].'" target="_blank" class="pLine haveTooltip" title="'.htmlspecialchars($c['text'], ENT_COMPAT | ENT_HTML401, 'cp1251').'">'.$c['title'].'</a>'
                    . '<div><img src="'.$image.'" /></div>'
                    . '</div>'
                    .(!empty($c['box']) ? '<p>+ '.$box[$c['box']].'</p>' : null)
                    .(!empty($c['vase']) ? '<p class="haveTooltip" title="<img src=\'/i/vaza.jpg\' align=\'left\' />">+ ���� ('.ceil($_GLOBALS['v_vase-price']/$cur['value']).$cur['reduction'].')</p>' : null)
                    .'</td>'
                    . '<td class="tableCost">'
                    .$amount.'<span class="reduction">'.$cur['reduction'].'</span>'.($c['count'] > 1 ? ' x '.$c['count'].'�� = '.($amount * $c['count']).'<span class="reduction">'.$cur['reduction'].'</span>' : null)
                    .'</td></tr>';
            }

        }
    }
    $g = giftsInBasket($basket_info['mark_disc_summ']);
    if (!empty($g)){
        $image = '';
        if (!empty($g['image'])) {
            $image = explode(':', $g['image']);
            $image = '/files/gifts/'.$g['gid'].'/w70_'.$image[0];
        }
        $html.= '<tr><td>'
                . '<div class="liBlock">'
                    . '<a href="javascript: void(0)" class="pLine haveTooltip" title="'.htmlspecialchars($g['text'], ENT_COMPAT | ENT_HTML401, 'cp1251').'">'.$g['title'].'</a>'
                    . '<div><img src="'.$image.'" /></div>'
                . '</div>'
            .'</td><td class="tableCost"><span class="text_color_orange">���������</span><br>(�������)</td></tr>';
    }
    $del_arr = array(
            1 => '� �������� ����',
            3 => '�� ���� (�� '.$deliveres['mkad_boundary'].' ��)',
            4 => '�� ���� (�� '.$deliveres['mkad_boundary'].' ��) / ���������� '.$deliveres['mkad_lenght'].' ��',
            5 => '�� ������� ����� "' . iconv("UTF-8", "WINDOWS-1251//IGNORE",$_SESSION['delivery_metro__']  ) . '"'
        );
    if ($b['delivery']!=2 && isset($del_arr[$deliveres['delivery']])){
        $dost_text = (!empty($addes['delivery']))? ($addes['pay_tochdos']+$addes['delivery']).'<span class="reduction">'.$cur['reduction'].'</span>': '<span class="text_color_orange"><b>���������</b></span>';
        $html.= '<tr><td><div class="liBlock">�������� '.$del_arr[$deliveres['delivery']].'</div></td><td class="tableCost">'.$dost_text.'</td></tr>';
    }
    if (!empty($b['foto'])) {
        $html.= '<tr><td><div class="liBlock">���� ��������� �������</div></td><td class="tableCost">'.(!empty($_GLOBALS['v_photo'])?ceil($_GLOBALS['v_photo'] / $cur['value']).$cur['reduction']:'<span class="text_color_orange"><b>���������</b></span>').'</td></tr>';
    }
    if ($b['type'] == 'courier') {
        $html.= '<tr><td><div class="liBlock">����� ������� �� �������</div></td><td class="tableCost">'.ceil($_GLOBALS['v_pay-curer'] / $cur['value']).$cur['reduction'].'</td></tr>';
    }
    if (array_sum($discounts) > 0){
        $html.= '<tr><td><div class="liBlock">���� ������</div></td><td class="tableCost">'.array_sum($discounts).'%</td></tr>';
    }

    $html.= '</tbody></table></div>';
    $html.= '<div class="oplataBlock blockWithTopLine"><span class="h2_alt">��������:</span>';
    $html.= '<p>'.trim($b['family'].' '.$b['name'].' '.$b['patronymic']).'</p>';
    if (!empty($b['city'])) $html.= '<p>�. '.$b['city'];
    if (!empty($b['street'])) $html.= ', ��. '.$b['street'];
    if (!empty($b['home'])) $html.= ', ��� '.$b['home'];
    if (!empty($b['housing'])) $html.= ', ������ '.$b['housing'];
    if (!empty($b['apartment'])) $html.= ', �������� '.$b['apartment'];
    if (!empty($b['hkey'])) $html.= ', ��� �������� '.$b['hkey'];
    $html.= '</p>';
    if (!empty($b['phone'])) $html.= '<p>�������: '.$b['phone'].($b['extra_call']==1?' <span class="grey">(�� ���������� �� ��������)</span>':'').'</p>';
    if (!empty($b['email'])) $html.= '<p>E-mail: '.$b['email'].'</p>';
    if (!empty($b['comment'])) $html.= '<p>�����������:<br /><i>'.$b['comment'].'</i></p>';
    $html.= '</div>';

    $html.= '<div class="oplataBlock blockWithTopLine"><span class="h2_alt">����������:</span>';
    $html.= '<p>'.trim($b['recipient_family'].' '.$b['recipient_name'].' '.$b['recipient_patronymic']).'</p>';
    if (!empty($b['recipient_city'])) $html.= '<p>�. '.$b['recipient_city'];
    if (!empty($b['recipient_street'])) $html.= ', ��. '.$b['recipient_street'];
    if (!empty($b['recipient_home'])) $html.= ', ��� '.$b['recipient_home'];
    if (!empty($b['recipient_housing'])) $html.= ', ������ '.$b['recipient_housing'];
    if (!empty($b['recipient_apartment'])) $html.= ', �������� '.$b['recipient_apartment'];
    if (!empty($b['recipient_hkey'])) $html.= ', ��� �������� '.$b['recipient_hkey'];
    $html.= '</p>';
    if (!empty($b['message'])) {
      if (!empty($b['signature'])) $html.= '<p><b>������� ��� ��������:</b> '.$b['signature'].'</p>';
      $html.= '<p><b>����� ��� ��������:</b> '.$b['message'].'</p>';
    }
    if (!empty($b['recipient_phone'])) $html.= '<p>�������: '.$b['recipient_phone'].($b['by_phone_only']==1?' <span class="grey">(��� �������� �� ������ ��������)</span>':'').'</p>';
    if ($b['by_phone_only']==1 && $b['by_phone_only_inf']!=''){
        $html .= '<p class="by_phone_only_order"><span>�������������� ����������:</span><span style="padding-left:10px; color:#555;">"'.$b['by_phone_only_inf'].'"</span></p>';
    }
    if (!empty($b['incog'])) $html.= '<p>� �� �������� �� ���� �����</p>';
    if (!empty($b['foto'])) $html.= '<p>� ������� ���� ���������� � ������ ��������</p>';
    if (!empty($b['neighborhood'])) $html.= '<p>� ����� �������� ����� �������, �������������</p>';
    if (!empty($b['nocall'])) $html.= '<p>� ��������� ����� ��� ������ ����������</p>';
    $html.= '</div>';
    $types = array(
        'cards'=>'��������� ������: WalletOne (������ �����)',
        'cards-assist'=>'��������� ������: Assist',
        'cash'=>'���������',
        'yandex'=>'������ ������',
        'wm'=>'Web money',
        'paypal'=>'PayPal',
        'sbbank'=>'��������',
        'contact'=>'��������',
        'euroset'=>'�������',
        'courier'=>'����� ������� �� �������',
        'terminals'=>'��������� ���������',
        'onlinebanks'=>'�������� ����',
        'mobileapp'=>'��������� ����������',
        'mobileoperators'=>'��������� ���������',
        'emoney'=>'����������� ������',
        'cards-courier'=>'��������� ������ �������'
    );
    if ($b['type'] == 'courier') {
      $types[$b['type']].= ' <span class="hint">('.date('d.m.Y � H:i', $b['curData']).' �� ������: '.$b['curAdres'].')</span>';
    }
    $html.= '<div class="oplataBlock blockWithTopLine"><span class="h2_alt">����� ������:</span><p>'.$types[$b['type']].'</p></div>';
    if (!empty($b['datetime'])) {
        $del_date = ($b['timeship']!='')? date('d.m.Y � ', $b['datetime']).$b['timeship'] :date('d.m.Y � H:i', $b['datetime']);
        $html.= '<div class="oplataBlock blockWithTopLine"><span class="h2_alt">���� � ����� ��������:</span><p>'.$del_date.'</p></div>';
    }
    if ($deliveres['delivery']==2){
        unset($addes['pay_tochdos']);
    }
    $itog += array_sum($addes);


//    $hash = md5($oid);

    //
    $otype = in1251($o['type']);

    $data_attr = 'data-source="'.$oid.'"';
    $realCurrency = mb_strtoupper($cur['name']);
    if ($realCurrency == 'EURO') $realCurrency = 'EUR';
    // ��� ��������� ������ ����� ������ � ������
    if ($realCurrency !== 'RUB'){
        $summItogoRub__ = query_new("SELECT `itogo`, `amount`, `currencySumm` FROM sw_orders WHERE id = '" . quote($oid) ."' LIMIT 0,1;");
        $summItogoRub = $summItogoRub__[0]['itogo'];
        $summAmountRub = $summItogoRub__[0]['amount'];
        $summCurRub = $summItogoRub__[0]['currencySumm'];
        $crc = md5(_ROBO_LOGIN.':'.$summItogoRub .':'.$oid.':' ._ROBO_PASS_1);
    }else{
        $crc = md5(_ROBO_LOGIN.':'.$itog.':'.$oid.':' ._ROBO_PASS_1);
    }
    //$crc = md5(_ROBO_LOGIN.':'.$itog.':'.$oid.':' . $realCurrency  . ':'._ROBO_PASS_1);


    if ($otype === 'sbbank' || $otype === 'cash' || $otype === 'courier' || $otype === 'cards-courier')
    {

        $buttonText = '';
        if ($otype == 'sbbank')
        {
            $buttonText = '����������� ���������';
        }
        else
        {
            $buttonText = '���������';
        }
        $payButton = '<a href="/basket/op=done&type=cccs" class="bigButton" '.$data_attr.'>'.$buttonText.'</a>';

    }
    elseif (($otype === 'cards-assist') || ($otype === 'wm'))
    {
        $buttonText = '������� � ������';

/*
        if ($realCurrency == 'RUB' ){
           // set_currency('euro');
            $basket_info = basket_info(array(),'',array('info'=>'full'));
            $amount = round( $basket_info['discount_summ_full']);
            set_currency('rub');
            $basket_info = basket_info(array(),'',array('info'=>'full'));
        }elseif($realCurrency == 'USD'){
            set_currency('rub');
            $basket_info = basket_info(array(),'',array('info'=>'full'));
            $amount = round( $basket_info['discount_summ_full']);
            set_currency('usd');
            $basket_info = basket_info(array(),'',array('info'=>'full'));
        }elseif($realCurrency == 'EUR'){
           set_currency('rub');
            $basket_info = basket_info(array(),'',array('info'=>'full'));
            $amount = round( $basket_info['discount_summ_full']);
            set_currency('euro');
            $basket_info = basket_info(array(),'',array('info'=>'full') );
        }
*/
	    $sItog = convert_currency($itog, $realCurrency , 'RUB' );

        $payButton = '<input type="submit" form="assist-form" class="bigButton" id="payButton" value="'.$buttonText.'" />';
        $html.= '<FORM id="assist-form" ACTION="https://payments221.paysecure.ru/pay/order.cfm" METHOD="POST">';
        $html.= '<INPUT TYPE="HIDDEN" NAME="Merchant_ID" VALUE="662011" />';
        $html.= '<INPUT TYPE="HIDDEN" NAME="OrderNumber" VALUE="'.$oid.'" />';
        $html.= '<INPUT TYPE="HIDDEN" NAME="OrderAmount" VALUE="'.$sItog.'" />';
        $html.= '<INPUT TYPE="HIDDEN" NAME="OrderCurrency" VALUE="RUB">';
        $html.= '<INPUT TYPE="HIDDEN" NAME="TestMode" VALUE="0" />';
        $html.= '<INPUT TYPE="HIDDEN" NAME="OrderComment" VALUE="������ ������ �'.$oid.'" />';
        $html.= '</FORM>';

    }
    elseif ($otype === 'paypal')
    {
        $buttonText = '������� � ������';

        $payButton = '<input type="submit" form="paypal-form" class="bigButton" id="payButton" '.$data_attr.' value="'.$buttonText.'" />';
	    $sItog = convert_currency($itog, $realCurrency , 'RUB' );

        $html.= '<form id="paypal-form" method="post" action="https://www.paypal.com/cgi-bin/webscr" name="paypal">';
        $html.= '<input type="hidden" name="cmd" value="_xclick" />';
        $html.= '<input type="hidden" name="business" value="studiofloristic@gmail.com" />';
        $html.= '<input type="hidden" name="amount" value="'.$sItog.'" />';
        $html.= '<input type="hidden" name="item_name" value="Order number '.$oid.'" />';
        $html.= '<input type="hidden" name="custom" value="'.$oid.'"/>';
        $html.= '<input type="hidden" name="no_shipping" value="0" />';
        $html.= '<input type="hidden" name="return" value="https://www.studiofloristic.ru/basket/?op=done" />';
        $html.= '<input type="hidden" name="cancel_return" value="https://www.studiofloristic.ru/basket/?op=issue" />';
        $html.= '<input type="hidden" name="undefined_quantity" value="0" />';
        $html.= '<input type="hidden" name="currency_code" value="RUB" />';
        $html.= '</form>';
    }
    else
    {
        $buttonText = '������� � ������';

        $payButton = '<input type="submit" form="walletone-form" class="bigButton" id="payButton" '.$data_attr.' value="'.$buttonText.'" />';
        switch ($otype) {
            case 'cards':           $label = 'CreditCardBYR,CreditCardRUB,CreditCardUAH,CreditCardUSD,CreditCardEUR'; break;
            case 'yandex':          $label = 'YandexMoneyRUB'; break;
            case 'wm':              $label = 'WebMoneyRUB'; break;
            case 'contact':         $label = 'EurosetRUB'; break; // ������� - ��� ��������
            case 'euroset':         $label = 'SvyaznoyRUB'; break; // �������� - ��� ������� :) ���...
            case 'terminals':       $label = 'CashTerminal'; break;
            case 'onlinebanks':     $label = 'AlfaclickRUB,TinkoffRUB,Privat24UAH,PsbRetailRUB,SberOnlineRUB,FakturaruRUB,RsbRUB,EripBYR,SetcomSidZAR,StandardBankEftZ'; break;
            case 'mobileapp':       $label = 'WalletOne'; break;
            case 'mobileoperators': $label = 'MegafonRUB,BeelineRUB,MtsRUB,Tele2RUB,KievStarUAH'; break;
            case 'emoney':          $label = 'LiqPayMoney,OkpayRUB,QiwiWalletRUB,WalletOneRUB,UniMoneyRUB,BPayMDL,GoogleWalletUSD'; break;
        }

        $html.= '<form id="walletone-form" action="https://wl.walletone.com/checkout/checkout/Index" method="post">';
        // ������������ ������� ��� W1
        $w1_fields = array();
        // ���������� ����� ����� � ������������� ������



        $w1_fields["WMI_MERCHANT_ID"]    = isset($_SESSION['w1id']) ? $_SESSION['w1id'] : W1_MERCHANT_ID4;

        if($realCurrency == 'USD' ){
            $w1_fields["WMI_PAYMENT_AMOUNT"] = $summItogoRub.".00";
        }elseif($realCurrency == 'EUR'){
            $w1_fields["WMI_PAYMENT_AMOUNT"] = $summItogoRub.".00";
        }else{
            $w1_fields["WMI_PAYMENT_AMOUNT"] = $itog.".00";
        }



        $w1_fields["WMI_CURRENCY_ID"]    = W1_CURRENCY_ID;


        $w1_fields["WMI_PAYMENT_NO"]     = $oid;
        $w1_fields["WMI_DESCRIPTION"]    = "BASE64:".base64_encode( inUTF8("������ ������ �$oid") );
        $w1_fields["WMI_EXPIRED_DATE"]   = date('c', strtotime("+25 days"));
        $w1_fields["WMI_SUCCESS_URL"]    = W1_SUCCESS_URL;
        $w1_fields["WMI_FAIL_URL"]       = W1_FAIL_URL;
        /**
         * ���� ����� �� �������, ����� ������� ��������� ��� ��������� ���� W1 ��������
         */
       // if (array_key_exists('phone',$b)){
       //     if (!empty($b['phone'])) $w1_fields["WMI_CUSTOMER_PHONE"]       = $b['phone']; // ����� ��������� �������
       // }

       // if (array_key_exists('email',$b)){
       //    if (!empty($b['email'])) $w1_fields["WMI_CUSTOMER_EMAIL"]       = $b['email']; // ����� ��������� �����
       // }

        /*
         * ��� ������ W1 ���� ���������� ��� ���������
         */
        if (array_key_exists('family',$b)){
            //if (!empty($b['family'])) $w1_fields["WMI_CUSTOMER_LASTNAME"]       = $b['family']; // ����� ��������� �������
        }

        if (array_key_exists('name',$b)){
            //if (!empty($b['name'])) $w1_fields["WMI_CUSTOMER_FIRSTNAME"]       = $b['name']; // ����� ��������� ���
        }


        // ������� ������ ������ - ���� ������, ����� �������� ��� ���������
        if (!empty($label) ){
            if (mb_strpos($label,',') !== false){
                $labels = explode(',', $label);
                $w1_fields["WMI_PTENABLED"] = $labels;
            }else{
                $w1_fields["WMI_PTENABLED"] = $label;
            }

        }

        //���������� �������� ������ �����
        foreach($w1_fields as $name => $val)
        {
            if(is_array($val))
            {
                usort($val, "strcasecmp");
                $w1_fields[$name] = $val;
            }
        }
        // ������������ ���������, ����� ����������� �������� �����,
        // ��������������� �� ������ ������ � ������� �����������.
        uksort($w1_fields, "strcasecmp");
        $w1fieldValues = "";

        foreach($w1_fields as $value)
        {
            if(is_array($value))
                foreach($value as $v)
                {
                    //����������� �� ������� ��������� (UTF-8)
                    //���������� ������ ���� ��������� �������� ������� �� Windows-1251
                    //$v = iconv("utf-8", "windows-1251", $v);
                    $w1fieldValues .= $v;
                }
            else
            {
                //����������� �� ������� ��������� (UTF-8)
                //���������� ������ ���� ��������� �������� ������� �� Windows-1251
                //$value = iconv("utf-8", "windows-1251", $value);
                $w1fieldValues .= $value;
            }
        }

        $w1_fields["WMI_SIGNATURE"]  = base64_encode(pack("H*", md5($w1fieldValues. (isset($_SESSION['w1key']) ? $_SESSION['w1key']: W1_SECRET_KEY4)))); //���������� ��������� �������
        foreach($w1_fields as $w1key => $w1val){
            if (is_array($w1val)){
                foreach ($w1val as $pmntsRange){
                    $html .= '<input name="'.$w1key.'" type="hidden" value="'.$pmntsRange.'" />';
                }
            }else{
                $html .= '<input name="'.$w1key.'" type="hidden" value="'.$w1val.'" />';
            }

        }

        // ������� ������ ���������
      //  if (!empty($label)) {
   	//		$html.= '<input name="IncCurrLabel" type="hidden" value="'.$label.'" />';
	//	}
        $html.= '</form>';
    }

    $html.= '<div class="blockWithTopLine" style="padding:15px;">';
    $html.= '<div class="bigBlockWithButtonOk"><table><tr><td class="v-align" ><div class="text_block_basket"><table><tr><td style="vertical-align: middle;">'
            . '<div class="tbb_row"><table><tr><td><div class="tbb_text_item">'
                . '<div class="bigBlocktext">�� �������� ����� �� �����:</div>'
                . '<div class="dis_text">(� ������ <span>'.array_sum($discounts).'%</span> ������)</div>'
                . '<div id="discInfo_inBok" class="diff_disc centered_hover_popup text_block" tb="TYPE_hover_popup--append"><div class="dis_text"><span><i class="fontico-info-circled-alt"></i></span> - �������������� ���������� �� ������</div></div>'
            . '</div></td>'
            . '<td align="right"><div class="bigBlockCost">'.number_format($itog, 0, '.' , ' ').'<span class="reduction">'.$cur['reduction'].'</span></div></td></tr></table></div>'
            . $holi_text
            . '</td></tr></table></div></td>'
            . '<td><div class="button_basket">'.$payButton.'</div></td></tr></table>'
        . '</div></div>';
    $html.= (file_exists('system/templates/ratingBlock.tpl.html')?file_get_contents('system/templates/ratingBlock.tpl.html'):'');
    logevent(6, $o['uid'], 1, array('order_id' => $oid, 'itog' => $itog, 'comment' =>$o['recipientSMS'], 'type' => $otype, 'is_payed' => 0) ); // ��������
    $_GLOBALS['title'] = '������ ������';
    $_GLOBALS['text'] = $html;
    $_GLOBALS['html_title'] = $_GLOBALS['title'].' - '.$_GLOBALS['v_sitename'];
    $_GLOBALS['template'] = 'default';
}

function createOrder() {
    global $_GLOBALS,$COOKIE_REPLACEMENT, $SMS4B, $cur, $payment_arr;

    $numberPaymentType = intval(filter(trim(in1251($_POST['type'])), 'nohtml'));
    $type = '';
    if ($numberPaymentType > 0)
    {
        foreach($payment_arr as $payment)
        {
            if ($payment['nCode'] === $numberPaymentType)
            {
                $type = $payment['type'];
                break;
            }
        }
    }

    // ���������� ��� ������� (������)
	if (empty($_POST['date']) || empty($_POST['name']) || empty($_POST['phone']) || empty($_POST['type']) || empty($_POST['delivery'])) {
        jQuery::evalScript('$("div.button_basket.loading").removeClass("loading");');
        $erro = array();
        if (empty($_POST['name'])) {
            $erro[] = '<strong style=\"color:red\">*</strong> �� ������� ���';
            $scroll = 'jQuery("body,html").animate({scrollTop:0},500);';
            jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#z-field-name:first"),t:"����� ������ �����",top:10})'));
        }
        if (empty($_POST['phone'])) {
            $erro[] = '<strong style=\"color:red\">*</strong> �� ������ �������';
            $scroll = 'jQuery("body,html").animate({scrollTop:0},500);';
            jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#z-field-phone:first"),t:"����� ������ ���������",top:10})'));
        }
        if (empty($_POST['delivery'])) {
            $erro[] = '<strong style=\"color:red\">*</strong> �� ��������� ���� ��������';
        }
        if (empty($_POST['date']) || !preg_match("/^([0-9]{2})\.([0-9]{2})\.([0-9]{4})$/i", $_POST['date']) || (!empty($_POST['time']) && preg_match("/^([0-9]{2})\:([0-9]{2})$/i", $_POST['time']) == 0)) {
            $erro[] = '<strong style=\"color:red\">*</strong> �� ������� ���� ��������';
            jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#z-field-data:first"),t:"����� ������ ���� ��������",top:10})'));
        }
        if (empty($type)) {
            $erro[] = '<strong style=\"color:red\">*</strong> �� ������ ����� ������';
        }

        if (!empty($scroll)) {
            jQuery::evalScript($scroll);
        }

        jQuery::evalScript(inUTF8('initSysDialog("' . implode('<br /><br />', $erro) . '",7000)'));
        jQuery::getResponse();
    }

    #if (filter(trim(in1251($_POST['type'])), 'nohtml') == 'courier') {
    // ���������� ������� � ������� �������, ����� ���������, ��������� �� ����������� ����
    if ($type == 'courier') {
        if (empty($_POST['cur-adres']) || empty($_POST['cur-time']) || empty($_POST['cur-date'])) {
            jQuery::evalScript('$("div.button_basket.loading").removeClass("loading");');
            jQuery::evalScript(inUTF8('initSysDialog("����� ��������� ��� ���� ��� ������ �������")'));
            jQuery::getResponse();
        }
        if (!preg_match("/^([0-9]{2})\.([0-9]{2})\.([0-9]{4})\s([0-9]{2})\:([0-9]{2})\:([0-9]{2})$/i", $_POST['cur-date'] . ' ' . $_POST['cur-time'] . ':00', $ctime)) {
            jQuery::evalScript('$("div.button_basket.loading").removeClass("loading");');
            jQuery::evalScript(inUTF8('initSysDialog("����� ��������� ��� ���� ��� ������ �������")'));
            jQuery::getResponse();
        }
        $curData  = mktime($ctime[4], $ctime[5], $ctime[6], $ctime[2], $ctime[1], $ctime[3]);
        $curAdres = filter(trim(in1251($_POST['cur-adres'])), 'nohtml');
    } else {
        $curAdres = null;
        $curData  = null;
    }

    $cur_ret = $cur;
    set_currency('rub');
    $COOKIE_REPLACEMENT = true;

    $CART      = basket_info(array(), '', array('info' => 'full'));
    $datetime  = $CART['datetime'];
    $deliveres = $CART['deliveres'];
    $discounts = $CART['discounts'];
    $addes     = $CART['addes_prices'];
    $u         = $CART['user'];
    $delivery  = ($deliveres['delivery'] == 4) ? $deliveres['delivery'] + $deliveres['mkad_lenght'] : $deliveres['delivery'];
    $dostavka  = !empty($addes['delivery']) ? $addes['delivery'] + $addes['pay_tochdos'] : $addes['pay_tochdos'];

    //���������� ������ �����
    $geoData = getUserCity();
    if ($geoData != false){
        $geoCity = $geoData['city']['name_ru'] ;
        $geoCountry = $geoData['country']['name_ru'] ;
    }else{
        $geoCity = "Unknown";
        $geoCountry = "Unknown";
    }

    if ($deliveres['delivery'] == 2) {
        unset($addes['pay_tochdos']);
        $dostavka = 0;
    }

    if ($deliveres['delivery'] == 5){
       $dostavka_metro = iconv("UTF-8", "WINDOWS-1251//IGNORE", ((!empty($_SESSION['delivery_metro__']  ) )? $_SESSION['delivery_metro__']   : $_COOKIE['metro_deliver'])) ;
    }

    // ���������� ������ � ������� ������� �� �������
    // ��������� ������ ������� ������ � �������������� ������� � �������
    $courier_payment = 0;
    if ($type == 'courier')
    {
        $addes['pay_for_courier'] = $_GLOBALS['v_pay-curer']; // ��������� ������ ������� ������ � ����
        $courier_payment = $addes['pay_for_courier']; // ���������� ��� ������ � ��������� ������� ��������� ������ ������� �� �������
    }

    $itemsMail = null;
    $holiday   = holl_markup($datetime['date']);
    $itog      = (empty($holiday) ? $CART['discount_summ_full'] : $CART['mark_disc_summ_full']) + array_sum($addes);
    $uid       = _IS_USER ? abs(intval($_SESSION['uid'])) : 0;
    $dels      = deliveres(false, $datetime['interval'], $datetime['date']);
    $timeship  = (!empty($datetime['time'])) ? '' : $dels['dels']['title'];
    $unix_time = strtotime($datetime['date'] . ' ' . $datetime['time']);
    $c         = array('amount' => $CART['clear_summ'], 'count' => $CART['count']);
    if ($unix_time === false) {
        jQuery::evalScript('$("div.button_basket.loading").removeClass("loading");');
        jQuery::evalScript('basket.result(4)');
        jQuery::getResponse();
    }

    if (!empty($timeship)){
        $tm_parts = explode(" - ", $timeship); // 13:0016:00
        $starttime = date("H:i:s",strtotime($tm_parts[0]));
        $endtime = date("H:i:s",strtotime($tm_parts[1]));
    }elseif(isset($datetime['time'])){
        if (!empty($datetime['time'])){
            $endtime = date("H:i:s",strtotime($datetime['time']));
            $starttime = $endtime ;

        }else{
            $starttime = "00:00:00";
            $endtime = "00:00:00";
        }
    }else{
        $starttime = "00:00:00";
        $endtime = "00:00:00";
    }

    $ORDER = array();
    #$type         = filter(trim(in1251($_POST['type'])), 'nohtml');
    $name         = filter(trim(in1251($_POST['name'])), 'nohtml');
    $patronymic   = filter(trim(in1251($_POST['patronymic'])), 'nohtml');
    $family       = filter(trim(in1251($_POST['family'])), 'nohtml');
    $phone        = filter(trim(in1251($_POST['phone'])), 'nohtml');
    $email        = filter(trim(in1251($_POST['email'])), 'nohtml');
    $city         = filter(trim(in1251($_POST['city'])), 'nohtml');
    $street       = filter(trim(in1251($_POST['street'])), 'nohtml');
    $home         = filter(trim(in1251($_POST['home'])), 'nohtml');
    $housing      = filter(trim(in1251($_POST['housing'])), 'nohtml');
    $apartment    = filter(trim(in1251($_POST['apartment'])), 'nohtml');
    $structure    = filter(trim(in1251($_POST['structure'])), 'nohtml');
    $hkey         = filter(trim(in1251($_POST['hkey'])), 'nohtml');
    $comment      = filter(trim(in1251($_POST['comment'])), 'nohtml');
    $subscription = !empty($_POST['subscription']) ? 1 : 0;

    $incog             = !empty($_POST['incog']) ? 1 : 0;
    $foto              = !empty($_POST['foto']) ? 1 : 0;
    $neighborhood      = !empty($_POST['neighborhood']) ? 1 : 0;
    $nocall            = !empty($_POST['nocall']) ? 1 : 0;
    $message           = filter(trim(in1251($_POST['message'])), 'nohtml');
    $signature         = filter(trim(in1251($_POST['signature'])), 'nohtml');
    $by_phone_only     = !empty($_POST['by_phone_only']) ? 1 : 0;
    $by_phone_only_inf = filter(trim(in1251($_POST['by_phone_only_inf'])), 'nohtml');
    $extra_call        = !empty($_POST['extra_call']) ? 1 : 0;
    $phoneSYS          = preg_replace("/\D+/", "", $phone);

    if (empty($subscription)) {
        if (empty($_POST['recipient-phone']) && $by_phone_only == 1) {
            jQuery::evalScript('$("div.button_basket.loading").removeClass("loading");');
            jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#field-recipient-phone:first"),t:"����� ������ ���������",top:10,left:180})'));
            jQuery::evalScript('jQuery("body,html").animate({scrollTop:600},500);');
            jQuery::getResponse();
        }
        $recipient_name       = filter(trim(in1251($_POST['recipient-name'])), 'nohtml');
        $recipient_patronymic = filter(trim(in1251($_POST['recipient-patronymic'])), 'nohtml');
        $recipient_family     = filter(trim(in1251($_POST['recipient-family'])), 'nohtml');
        $recipient_phone      = filter(trim(in1251($_POST['recipient-phone'])), 'nohtml');
        $recipient_city       = filter(trim(in1251($_POST['recipient-city'])), 'nohtml');
        $recipient_street     = filter(trim(in1251($_POST['recipient-street'])), 'nohtml');
        $recipient_home       = filter(trim(in1251($_POST['recipient-home'])), 'nohtml');
        $recipient_housing    = filter(trim(in1251($_POST['recipient-housing'])), 'nohtml');
        $recipient_apartment  = filter(trim(in1251($_POST['recipient-apartment'])), 'nohtml');
        $recipient_structure  = filter(trim(in1251($_POST['recipient-structure'])), 'nohtml');
        $recipient_hkey       = filter(trim(in1251($_POST['recipient-hkey'])), 'nohtml');
        $recipient_phoneSYS   = preg_replace("/\D+/", "", $recipient_phone);
    } else {
        $recipient_name       = $name;
        $recipient_patronymic = $patronymic;
        $recipient_family     = $family;
        $recipient_phone      = $phone;
        $recipient_city       = $city;
        $recipient_street     = $street;
        $recipient_home       = $home;
        $recipient_housing    = $housing;
        $recipient_apartment  = $apartment;
        $recipient_structure  = $structure;
        $recipient_hkey       = $hkey;
        $recipient_phoneSYS   = preg_replace("/\D+/", "", $recipient_phone);
    }

    $inCart = query_new('SELECT `session` FROM `sw_basket_order` WHERE `session` = ' . quote(session_id()).' LIMIT 1',1 );
    if ( !empty($inCart)) {
        query_new('UPDATE `sw_basket_order` SET '
                . '`datetime` = '               . quote($unix_time) . ', '
                . '`timeship` = '               . quote($timeship) . ', '
                . '`date` = '                   . quote(date('Y-m-d', $unix_time)) . ', '
                . '`delivery` = '               . quote($delivery) . ', '
                . '`dostavka` = '               . quote($dostavka) . ', '
                . '`type` = '                   . quote($type) . ', '
                . '`subscription` = '           . quote($subscription) . ', '
                . '`incog` = '                  . quote($incog) . ', '
                . '`foto` = '                   . quote($foto) . ', '
                . '`neighborhood` = '           . quote($neighborhood) . ', '
                . '`nocall` = '                 . quote($nocall) . ', '
                . '`message` = '                . quote($message) . ', '
                . '`signature` = '              . quote($signature) . ', '
                . '`name` = '                   . quote($name) . ', '
                . '`patronymic` = '             . quote($patronymic) . ', '
                . '`family` = '                 . quote($family,false) . ', '
                . '`phone` = '                  . quote($phone) . ', '
                . '`phone-sys` = '              . quote($phoneSYS,false) . ', '
                . '`email` = '                  . quote($email) . ', '
                . '`city` = '                   . quote($city) . ', '
                . '`street` = '                 . quote($street) . ', '
                . '`home` = '                   . quote($home) . ', '
                . '`housing` = '                . quote($housing) . ', '
                . '`apartment` = '              . quote($apartment) . ', '
                . '`structure` = '              . quote($structure) . ', '
                . '`hkey` = '                   . quote($hkey) . ', '
                . '`recipient_name` = '         . quote($recipient_name) . ', '
                . '`recipient_patronymic` = '   . quote($recipient_patronymic) . ', '
                . '`recipient_family` = '       . quote($recipient_family) . ', '
                . '`recipient_phone` = '        . quote($recipient_phone,false) . ', '
                . '`recipient_city` = '         . quote($recipient_city) . ', '
                . '`recipient_street` = '       . quote($recipient_street) . ', '
                . '`recipient_home` = '         . quote($recipient_home) . ', '
                . '`recipient_housing` = '      . quote($recipient_housing) . ', '
                . '`recipient_apartment` = '    . quote($recipient_apartment) . ', '
                . '`recipient_structure` = '    . quote($recipient_structure) . ', '
                . '`recipient_hkey` = '         . quote($recipient_hkey) . ', '
                . '`comment` = '                . quote($comment) . ', '
                . '`curData` = '                . quote($curData) . ', '
                . '`curAdres` = '               . quote($curAdres) . ', '
                . '`by_phone_only` = '          . quote($by_phone_only) . ', '
                . '`by_phone_only_inf` = '      . quote($by_phone_only_inf) . ', '
                . '`markup` = '                 . quote($CART['markup']) . ', '
                . '`extra_call` = '             . quote($extra_call) . ' '
                . ' WHERE `session` = '         . quote(session_id()));
    } else {
        query_new('INSERT INTO `sw_basket_order` ('
                . '`session`, `type`, `datetime`, '
                . '`timeship`, `date`, `delivery`, '
                . '`dostavka`, `subscription`, `incog`, '
                . '`foto`, `neighborhood`, `nocall`, '
                . '`message`, `signature`, `name`, '
                . '`patronymic`, `family`, `phone`, '
                . '`phone-sys`, `email`, `city`, '
                . '`street`, `home`, `housing`, '
                . '`apartment`, `structure`, `hkey`, '
                . '`recipient_name`, `recipient_patronymic`, `recipient_family`, '
                . '`recipient_phone`, `recipient_city`, `recipient_street`, '
                . '`recipient_home`, `recipient_housing`, `recipient_apartment`, '
                . '`recipient_structure`, `recipient_hkey`, `comment`, '
                . '`curAdres`, `curData`,`by_phone_only`,'
                . '`by_phone_only_inf`,`extra_call`,`markup`) VALUES ('
                . quote(session_id())           . ', ' . quote($type)                       . ', ' . quote($unix_time)              . ', '
                . quote($timeship)              . ', ' . quote(date('Y-m-d', $unix_time))   . ', ' . quote($delivery)               . ', '
                . quote($dostavka)              . ', ' . quote($subscription)               . ', ' . quote($incog)                  . ', '
                . quote($foto)                  . ', ' . quote($neighborhood)               . ', ' . quote($nocall)                 . ', '
                . quote($message)               . ', ' . quote($signature)                  . ', ' . quote($name)                   . ', '
                . quote($patronymic)            . ', ' . quote($family)                     . ', ' . quote($phone,false)            . ', '
                . quote($phoneSYS,false)        . ', ' . quote($email)                      . ', ' . quote($city)                   . ', '
                . quote($street)                . ', ' . quote($home)                       . ', ' . quote($housing)                . ', '
                . quote($apartment)             . ', ' . quote($structure)                  . ', ' . quote($hkey)                   . ', '
                . quote($recipient_name)        . ', ' . quote($recipient_patronymic)       . ', ' . quote($recipient_family)       . ', '
                . quote($recipient_phone,false) . ', ' . quote($recipient_city)             . ', ' . quote($recipient_street)       . ', '
                . quote($recipient_home)        . ', ' . quote($recipient_housing)          . ', ' . quote($recipient_apartment)    . ', '
                . quote($recipient_structure)   . ', ' . quote($recipient_hkey)             . ', ' . quote($comment)                . ', '
                . quote($curAdres)              . ', ' . quote($curData)                    . ', ' . quote($by_phone_only)          . ', '
                . quote($by_phone_only_inf)     . ', ' . quote($extra_call)                 . ', ' . quote($CART['markup']) . ')');
    }


    $uAdres = array();
    if (!empty($city))      $uAdres[] = '�. '       .$city;
    if (!empty($street))    $uAdres[] = ''      .$street;
    if (!empty($home))      $uAdres[] = '�. '       .$home;
    if (!empty($housing))   $uAdres[] = '������ '   .$housing;
    if (!empty($structure)) $uAdres[] = '�������� ' .$structure;
    if (!empty($apartment)) $uAdres[] = '��./���� ' .$apartment;

    $uAdres = implode(', ',$uAdres);
    $username = trim($family.' '.$name.' '.$patronymic);
    $uData = array('<b>��������:</b> '.$username,'<b>�����:</b> '.$uAdres);

    if (!empty($hkey))      $uData[] = '<b>�������:</b> '.$hkey;
    if (!empty($phone))     $uData[] = '<b>�������:</b> '.$phone.(!empty($extra_call)?' (�� ���������� �� ��������)':'');
    if (!empty($email))     $uData[] = '<b>E-mail:</b> <a href="mailto:'.$email.'">'.$email.'</a>';
    if (!empty($comment))   $uData[] = '<b>�����������:</b> <i>'.$comment.'</i>';

    $rAdres = array();
    if (!empty($recipient_city))        $rAdres[] = '�. '       .$recipient_city;
    if (!empty($recipient_street))      $rAdres[] = ''      .$recipient_street;
    if (!empty($recipient_home))        $rAdres[] = '�. '       .$recipient_home;
    if (!empty($recipient_housing))     $rAdres[] = '������ '   .$recipient_housing;
    if (!empty($recipient_structure))   $rAdres[] = '�������� ' .$recipient_structure;
    if (!empty($recipient_apartment))   $rAdres[] = '��./���� ' .$recipient_apartment;

    $rAdres = implode(', ',$rAdres);
    $rData = array('<b>����������:</b> '.trim($recipient_family.' '.$recipient_name.' '.$recipient_patronymic),'<b>�����:</b> '.$rAdres);
    $aData = $rData;

    if (!empty($recipient_phone))   $rData[] = '<b>�������:</b> '.$recipient_phone.(!empty($by_phone_only)?' (�������� �� ������ ��������)':'');
    if (!empty($by_phone_only_inf) &&
        !empty($by_phone_only))     $rData[] = '<b>�������������� ����������</b> (��� �������� �� ������ ��������):<br>'.$by_phone_only_inf;
    if (!empty($recipient_hkey))    $rData[] = '<b>�������:</b> '.$recipient_hkey;
    if (!empty($signature))         $rData[] = '<b>������� ��� ��������:</b> '.$signature;
    if (!empty($message))           $rData[] = '<b>����� ��� ��������:</b> '.$message;
    if (!empty($recipient_phone))   $aData[] = '<b>�������:</b> '.$recipient_phone;
    if (!empty($recipient_hkey))    $aData[] = '<b>�������:</b> '.$recipient_hkey;

    $rData[] = '<b>' . (!empty($incog) ? '�� ��������' : '��������') . ' �� ����</b>';
    $rData[] = '<b>����:</b> ' . (!empty($foto) ? '����' : '�� ����');
    $rData[] = '<b>�������� ����� �������, �������������:</b> ' . (!empty($neighborhood) ? '��' : '���');
    $rData[] = '<b>��� ������ ����������:</b> ' . (!empty($nocall) ? '��' : '���');

    if ($type == 'courier') $rData[] = '<b>����� ������� �� �������:</b> '.date('d.m.Y � H:i', $curData).' �� ������ '.$curAdres.' (<b>+ '.$_GLOBALS['v_pay-curer'].$cur['reduction'].'</b>)';

    $extra   = array('<b>' . (!empty($incog) ? '�� ��������' : '��������') . ' �� ����</b>');
    $extra[] = '<b>����:</b> ' . (!empty($foto) ? '����' : '�� ����');
    $extra[] = '<b>�������� ����� �������, �������������:</b> ' . (!empty($neighborhood) ? '��' : '���');
    $extra[] = '<b>��� ������ ����������:</b> ' . (!empty($nocall) ? '��' : '���');

    $order_title  = !empty($timeship) ? date('Y-m-d', $unix_time) . ' ' . $timeship : date('Y-m-d H:i', $unix_time);
    $recipientSMS = substr($order_title, 5) . ' ' . $_GLOBALS['payments'][$type] . ', ' . $name . ' ' . $phoneSYS . ', ' . $recipient_city;

    if (!empty($recipient_street))      $recipientSMS.= ', '.$recipient_street;
    if (!empty($recipient_home))        $recipientSMS.= ' '.$recipient_home;
    if (!empty($recipient_housing))     $recipientSMS.= '/'.$recipient_housing;
    if (!empty($recipient_apartment))   $recipientSMS.= '�'.$recipient_apartment;

    if ($delivery==2)   $order_title = '���������: '.$order_title;
    $uData = '<p>'.implode('</p><p>',$uData).'</p>';
    $rData = '<p>'.implode('</p><p>',$rData).'</p>';
    $aData = '<p>'.implode('</p><p>',$aData).'</p>';
    $extra = '<p>'.implode('</p><p>',$extra).'</p>';

    # patch 18.03.2016

    unset ($_COOKIE['order_hash1']);
    unset ($_SESSION['order_hash1']);
    $_SESSION['�rder_hash1'] = null;
    # end of patch 18.03.2016

	// ������� ������ � ��������� �������, ���� ����
	db_query("DELETE FROM `sw_abandoned_carts` WHERE `session` = " . quote(session_id()). ";");

    if (!empty($_SESSION['order_hash1'])){
        $oid = query_new('SELECT `id` FROM `sw_orders` WHERE `hash` = '.quote($_SESSION['order_hash1']).' LIMIT 1',1);
    }

    $realCurrency = mb_strtoupper($cur['name']);
    if ($realCurrency == 'EURO') $realCurrency = 'EUR';
    $w1pay = w1LimitCheck();
    if (!empty($w1pay['w1id'])){
        $_SESSION['w1id'] = $w1pay['w1id'];
        $_SESSION['w1key'] = $w1pay['w1key'];
        $w1code = $w1pay['type'];
    }
    if (empty($_SESSION['order_hash1']) || empty($oid['id'])){
        $oid = query_new('INSERT INTO `sw_orders` ('
            . '`active`, `uid`, `title`, `itogo`, '
            . '`recipientSMS`, `phone-sys`, `incog`, `foto`, '
            . '`neighborhood`, `nocall`, `uName`, `uPhone`, '
            . '`uAdres`, `uData`, `addressee`, `extra`, '
            . '`amount`, `count`, `dostavka`, `delivery`, '
            . '`recipient`, `timeDelivery`, `session`, `discount`,'
            . '`by_phone_only`, `by_phone_only_inf`, `extra_call`, `markup`, `type`, '
            . '`recipient-phone-sys`, `courier_payment`,  `email`, `currencyStr`, `currencySumm`, `walletoneid`, `region`, `s_start_time`, `s_end_time`'
            . ') VALUES ('
            . '1, ' // ���������� ������
            .quote($uid).', ' // user ID
            .quote($order_title).', ' // ���� � ����� ������
            .quote($itog).', ' // �����
            .quote($recipientSMS . "[$$ ".(!empty($dostavka_metro ) ? $dostavka_metro  : "" )." $$]" ).', '
            .quote($phoneSYS,false).', '
            .quote($incog).', '
            .quote($foto).', '
            .quote($neighborhood).', '
            .quote($nocall).', '
            .quote($username).', '
            .quote($phone,false).', '
            .quote($uAdres).', '
            .quote($uData).', '
            .quote($aData).', '
            .quote($extra).', '
            .quote($c['amount']).', ' // � ������ �� ������
            .quote($c['count']).', ' // ���������� �������
            .quote($dostavka).', ' // ��������� �������� (��������, ����� ���������, ���� ���� ��������� ��������� ������ ������� �� �������
            .quote($delivery).', '
            .quote($rData).', '
            .quote(str_replace('���������: ','',$order_title)).', '
            .quote(session_id()).', '
            .quote(array_sum($discounts)).', '
            .quote($by_phone_only).', '
            .quote($by_phone_only_inf).', '
            .quote($extra_call).', '
            .quote($CART['markup']).', '
            .quote($type).', '
            .quote($recipient_phoneSYS).', '
            .quote($courier_payment).', '
            .quote($email) . ', '
            .quote($realCurrency). ', '
            .quote( round( $itog / $cur['value'])). ", "
            .quote( (!empty($w1code)) ? $w1code : 1 ). ","
            .quote( $geoCity . "/" . $geoCountry ). ","
            .quote( $starttime). ","
            .quote( $endtime). " " .
            ')',1);
        logevent(5, $uid, 1, array('order_id' => $oid, 'itog' => $itog, 'comment' =>$recipientSMS , 'type' => $type, 'is_payed' => 0) ); // ��������
        $hash = md5($oid);
        query_new('UPDATE `sw_orders` SET `hash` = '.quote($hash).' WHERE `id` = '.quote($oid));
        setcookie('order_hash1', $hash , time()+60*15,'/',_CROSS_DOMAIN);
	    $_SESSION['order_hash1'] = $hash ;
    }else{
        $w1pay = w1LimitCheck();
        if (!empty($w1pay['w1id'])){
            $_SESSION['w1id'] = $w1pay['w1id'];
            $_SESSION['w1key'] = $w1pay['w1key'];
            $w1code = $w1pay['type'];
        }

        if (empty($starttime) && empty($endtime)){
            if (!empty($timeship)){
                $tm_parts = explode(" - ", $timeship); // 13:0016:00
                $starttime = date("H:i:s",strtotime($tm_parts[0]));
                $endtime = date("H:i:s",strtotime($tm_parts[1]));
            }elseif(isset($datetime['time'])){
                if (!empty($datetime['time'])){
                    $endtime = date("H:i:s",strtotime($datetime['time']));
                    $starttime = $endtime ;

                }else{
                    $starttime = "00:00:00";
                    $endtime = "00:00:00";
                }
            }else{
                $starttime = "00:00:00";
                $endtime = "00:00:00";
            }
        }

        $oid = $oid['id'];
        query_new('UPDATE `sw_orders` SET'
                . '`active` = 1, '
                . '`uid` = '.quote($uid).', '
                . '`title` = '.quote($order_title).', '
                . '`itogo` = '.quote($itog).', '
                . '`recipientSMS` = '.quote($recipientSMS).', '
                . '`phone-sys` = '.quote($phoneSYS,false).', '
                . '`incog` = '.quote($incog).', '
                . '`foto` = '.quote($foto).', '
                . '`neighborhood` = '.quote($neighborhood).', '
                . '`nocall` = '.quote($nocall).', '
                . '`uName` = '.quote($username).', '
                . '`uPhone` = '.quote($phone,false).', '
                . '`uAdres` = '.quote($uAdres).', '
                . '`uData` = '.quote($uData).', '
                . '`addressee` = '.quote($aData).', '
                . '`extra` = '.quote($extra).', '
                . '`amount` = '.quote($c['amount']).', '
                . '`count` = '.quote($c['count']).', '
                . '`dostavka` = '.quote($dostavka).', '
                . '`delivery` = '.quote($delivery).', '
                . '`recipient` = '.quote($rData).', '
                . '`timeDelivery` = '.quote(str_replace('���������: ','',$order_title)).', '
                . '`session` = '.quote(session_id()).', '
                . '`discount` = '.quote(array_sum($discounts)).', '
                . '`by_phone_only` = '.quote($by_phone_only).', '
                . '`by_phone_only_inf` = '.quote($by_phone_only_inf).', '
                . '`extra_call` = '.  quote($extra_call).', '
                . '`markup` = '.quote($CART['markup']).', '
                . '`type` = '.quote($type).', '
                . '`walletoneid` = '.quote( (!empty($w1code)) ? $w1code : 1 ).', '
                . '`s_start_time` = '.quote( $starttime ).', '
                . '`s_end_time` = '.quote( $endtime).' '
                . ' WHERE `id` = '.quote($oid));
        query_new('DELETE FROM `sw_orders_item` WHERE `oid` = '.quote($oid));
        logevent(7, $uid, 1, array('order_id' => $oid, 'itog' => $itog, 'comment' =>$recipientSMS , 'type' => $type, 'is_payed' => 0) ); // ��������
    }

    $time = time();
    $box  = array(
        0 => '����� (���������)',
        1 => '�������� ����� (' . $_GLOBALS['v_execution-1'] . $cur['reduction'] . ')',
        2 => '����������� �������� (' . $_GLOBALS['v_execution-2'] . $cur['reduction'] . ')',
        3 => '���������� �������� (' . $_GLOBALS['v_execution-3'] . $cur['reduction'] . ')',
    );

    $itemsMail = array();
    $itmsToOrd = array_merge($CART['products'],$CART['gifts']);
    foreach ($itmsToOrd as $i){
        query_new('INSERT INTO `sw_orders_item` (`oid`, `cid`, `sid`, `pid`, `vase`, `box`, `boxPrice`, `count`, `type`, `title`, `color`, `text`, `structure`, `price`, `amount`) VALUES ('
                .quote($oid).', '.quote($i['cid']).', '.quote($i['sid']).', '.quote($i['pid']).', '.quote($i['vase']).', '.quote($i['box']).', '.quote($i['boxPrice']).', '.quote($i['count']).', '.quote($i['type']).', '.quote($i['title']).', '.quote($i['color']).', '.quote($i['text']).', '.quote($i['structure']).', '.quote($i['price']).', '.quote($i['amount']).')');
//        query_new('UPDATE sw_catalog sc SET sc.`views` = (SELECT COUNT(soi.id) as `views` FROM sw_orders_item soi WHERE soi.`cid`='.$i['cid'].') where sc.`id`='.$i['cid']);
        $itemsMail[] = '<li><a href="http://'._HOSTNAME.$i['url'].'" target="_blank">'.$i['title'].'</a> - '.$i['count'].' ��. �� '.$i['amount'].' ���.'.(!empty($i['box']) ? ' + '.$box[$i['box']] : null).(!empty($i['vase']) ? ' + ���� '.$_GLOBALS['v_vase-price'].' �.' : null).'</li>';
    }

    // ������ ��� ����� �������� SMS - ���������. �������� ��������� ����� ��������� � ��������, ������������
    // �� ����� �������� � ������� - ������ ��������� � ���������� ID ������, � ��� ���������
    // ��� ��������� ������ ��������� ����� SmsQueue.php

    if (!class_exists('SmsQueue'))
    {
        include_once ('system/smsqueue.php');
    }

//    if ( in_array($type, array('cash', 'cards-courier', 'terminals')) )
//    {
        // ���������� ��������� ������� �� �������, ��� ��� ����� ������
        $params = array(
            'msg_type' => SmsQueue::ORDER_ONLINE_CASH,
            'oid' => $oid
            );
        $smsQueue = new SmsQueue();
        $smsQueue->addMessageInQueue($params);
//    }
    // ����� �������� ���������
    $del_arr = array(
        1 => '� �������� ����',
        3 => '�� ���� (�� '.$deliveres['mkad_boundary'].' ��)',
        4 => '�� ���� (�� '.$deliveres['mkad_boundary'].' ��) / ���������� '.$deliveres['mkad_lenght'].' ��',
        5 => '�� ������� ����� "' . iconv("UTF-8", "WINDOWS-1251//IGNORE",$_SESSION['delivery_metro__']  ) . '"'
    );
    $dost_text = (!empty($addes['delivery']))? ($addes['pay_tochdos']+$addes['delivery']).'<span class="reduction">'.$cur['reduction'].'</span>': '<span class="text_color_orange"><b>���������</b></span>';
    $dost_string = '<li>�������� '.$del_arr[$deliveres['delivery']]. " - " . $dost_text .'</li>';
    // ������� ��������� ������ �������
    if ($type == 'courier'){
        $dost_string .= '<li>����� ������� �� ������� - 250�.</li>';
    }
    /*
     *   $del_arr = array(
            1 => '� �������� ����',
            3 => '�� ���� (�� '.$deliveres['mkad_boundary'].' ��)',
            4 => '�� ���� (�� '.$deliveres['mkad_boundary'].' ��) / ���������� '.$deliveres['mkad_lenght'].' ��',
            5 => '�������� �� ������� ����� "' . iconv("UTF-8", "WINDOWS-1251//IGNORE",$_SESSION['delivery_metro__']  ) . '"'
        );
    if ($b['delivery']!=2 && isset($del_arr[$deliveres['delivery']])){
        $dost_text = (!empty($addes['delivery']))? ($addes['pay_tochdos']+$addes['delivery']).'<span class="reduction">'.$cur['reduction'].'</span>': '<span class="text_color_orange"><b>���������</b></span>';
        $html.= '<tr><td><div class="liBlock">�������� '.$del_arr[$deliveres['delivery']].'</div></td><td class="tableCost">'.$dost_text.'</td></tr>';
    }
     * */


    $msg = '<p>������������!</p>'.PHP_EOL;
    $includs = array();
    if(!empty($dostavka)){
        $includs[] = '�������� '.$dostavka.' '.$cur['reduction'];
    }
    $del_date = !empty($unix_time)?'<p><b>����� ��������:</b> '.$order_title.'</p>'.PHP_EOL:''.PHP_EOL;
    $msg.= '<p>����� �'.$oid.' �� '.date('d.m.Y H:i', $time).' �� ����� '.$itog.' '.$cur['reduction'].(!empty($includs) ? ', � ������ '.   implode(', ', $includs): null) . (($type == 'courier') ? " + ����� ������� �� ������� 250 �." : ""). '</p>'.PHP_EOL;
    $msg.= (array_sum($discounts)>0)?'<p><b>����� ������:</b> '.array_sum($discounts).'%</p>'.PHP_EOL:''.PHP_EOL;
    $msg.= $del_date.'<p><b>��� ������:</b> '.$_GLOBALS['payments'][$type].'</p><hr />'.PHP_EOL;
    $msg.= $uData.'<hr />'.$rData.'<hr />'.PHP_EOL;
    $msg.= '<p>������ ������:</p><ul>'.implode(PHP_EOL,$itemsMail). $dost_string.'</ul><br /><br />--<br />'.PHP_EOL;
    $msg.= '����� <a href="http://'._HOSTNAME.'">'.$_GLOBALS['v_sitename'].'</a>'.PHP_EOL;

    $headers = "Content-type: text/html; charset=windows-1251 \r\nFrom: �������� ������ <".$_GLOBALS['v_email_admin'].">\r\n";
    $theme = "����� ����� �� �"._HOSTNAME."�";

    $mail = new PHPMailer;
    try {
        $mail->setFrom($_GLOBALS['v_email_admin'], '�������� ������');
        $mail->addAddress($_GLOBALS['v_email_admin']);
        $mail->Subject = $theme;
        $mail->isHTML(true);
        $mail->CharSet = 'windows-1251';
        $mail->Body = '<html>'.$msg.'</html>';
        $mail->AltBody = strip_tags($msg);
        $mail->send();        
    } catch (Exception $e) {
        unset($e);
    }

    $user_email = false;
    if ( _IS_USER && !empty($u['email']))
    {
        if (!empty($email)){
            $user_email = $email;
        }else{
            $user_email = $u['email'];
        }

    }
    elseif (!empty($email))
    {
        $user_email = $email;
    }



    if ($user_email != false)
    {
        if(array_sum($discounts)>0){ $includs[] = '������ '.array_sum($discounts).'%';}
        $msg = '<p style="text-align:center"><img src="http://'._HOSTNAME.'/i/logo-studio-floristic-green.png" alt="'.$_GLOBALS['v_sitename'].'"></p>'.PHP_EOL;
        $msg.= '<p>������� ������� ����� ���, '.$username.'</p>'.PHP_EOL;
        $msg.= '<p>��� ����� � '.$oid.' �� '.date('d.m.Y', $time).' ������.</p>'.PHP_EOL;
        $msg.= !empty($unix_time)?'<p><b>����� ��������:</b> '.$order_title.'</p>'.PHP_EOL:''.PHP_EOL;
        $msg.= '<p>��������� ������: '.$itog.' '.$cur['reduction'].(!empty($includs) ? ', � ������ '.implode(', ',$includs): null). (($type == 'courier') ? " + ����� ������� �� ������� 250 �." : "") .'</p>'.PHP_EOL;
        $msg.= '<p>������ ������:</p><ul>'. str_replace("�.", " ���.", implode(PHP_EOL,$itemsMail). $dost_string) .'</ul>'.PHP_EOL; 
        $msg.= '<p>����������, ��� ��������� � ����� ���������� ����������� ���������� ����� ������ ������ - <b>'.$oid.'</b>.</p>'.PHP_EOL;

        // ��� ����� ����� �������� ���� � �������� ������, �, ��� ���� �� �����, ������� ����������� � ������� ������ ������

        $msg.= '<p>E-mail: info@studiofloristic.ru</p>'.PHP_EOL;
        $msg .= '<p>�� ���� �������� ����������� �� �������� 8-800-333-12-91, <b>�������������</b>.'.PHP_EOL;
        $msg .= '<p><b>������ ��� � ����� ������� ������� � �����������!</b></p>'.PHP_EOL;
        $msg .= '<p>����������� �������������� ����� ������������. �� ������ ������� ��  �������� ���������� ������ �� �������� � � ������ �������� ����������.</p>'.PHP_EOL;
        $msg .= '<p>����� �������� ������� ��� ������������� ��������. � ������������ ������������ ����� ������������ <a href="https://www.studiofloristic.ru/corporate.html">�����</a>.</p>'.PHP_EOL;

        $msg .= '<p><b style="color:green;">StudioFloristic.ru</b> <b>-</b> <b style="color:mediumpurple;">�����</b> <b style="color:orange">���</b> <b style="color:darkred;">�������</b></p>'.PHP_EOL;

        $theme = '��� ����� �� ����� �'.$_GLOBALS['v_sitename'].'�'.PHP_EOL;

        $mail = new PHPMailer;
        try {
            $mail->setFrom($_GLOBALS['v_email_admin'], '�������� ������');
            $mail->addAddress($user_email);
            $mail->Subject = $theme;
            $mail->isHTML(true);
            $mail->CharSet = 'windows-1251';
            $mail->Body = '<html>'.$msg.'</html>';
            $mail->AltBody = strip_tags($msg);
            $mail->send();            
        } catch (Exception $e) {
            unset($e);
        }

        $smsQueue = new SmsQueue();
        $smsQueue->storeMailToFile(array('subject' => $theme,'body' => $msg, 'headers' => $headers));
    }

    set_currency($cur_ret['name']);
    cookie_fields_clear('issue_', '/basket/');
    cookie_fields_clear('issue_', '/');


    // ��� �����, ���� ������ �������, �� ���� /basket/op=done&type=cccs
    if (in_array($type, array('sbbank', 'cash', 'courier', 'cards-courier') ) ) {
        $script = 'location.href="/basket/op=done&type=cccs"';
    } else {
        $script = 'location.href="/basket/op=payment"';
    }

    $script = 'location.href="/basket/op=payment"';

    jQuery::evalScript($script);
    jQuery::getResponse();
}

function setCount() {
    if (empty($_GET['id']) || empty($_GET['count'])) {
        jQuery::getResponse();
    }
    $count         = abs(intval($_GET['count']));
    $id            = abs(intval($_GET['id']));
    db_query('UPDATE `sw_basket` SET `count` = ' . quote($count) . ' WHERE `session` = ' . quote(session_id()) . ' AND `id` = ' . quote($id));
    $basket_info   = basket_info();
    $_GET['price'] = $basket_info['discount_summ'];
    jQuery::evalScript('basket.totPrice()');
    jQuery::evalScript('miniCart.itemsRefresh()');
    giftsMore();

}

function msg_del_min_price($price = 0, $free_del = array(), $del_holl = array(), $del_normal = array(), $cur = array()) {
    $free_del['info_block_text1'] = str_replace('del_h_min_price', $del_holl['del_opt']['min_price'] . $cur['reduction'], $free_del['info_block_text1']);
    $free_del['info_block_text1'] = str_replace('del_min_price', $del_normal['del_opt']['min_price'] . $cur['reduction'], $free_del['info_block_text1']);
    $url                          = ($free_del['info_block_url_text'] != '') ? $free_del['info_block_url_text'] : '����������';
    $url                          = ($free_del['info_block_url_link'] != '') ? '<a href="' . $free_del['info_block_url_link'] . '" target="_blank">' . $url . '</a>' : '';
    $free_del['info_block_text2'] = str_replace('url_link', $url, $free_del['info_block_text2']);
    $html                         = '<div id="del_info_cont" style="display:none;"><div class="del_info">'
            . '<div class="bott_shad">'
            . '<p><span class="etention">!</span></p>'
            . $free_del['info_block_text1']
            . '</div>'
            . '<div class="bott_shad">' . $free_del['info_block_text2'] . '</div>'
            . '<div class="price_and_actions">'
            . '<p>��������� ������ ������: <span id="del_info_price" class="red_price">' . $price . '<span class="reduction">' . $cur['reduction'] . '</span></span></p>'
            . '<p style="padding: 11px 0 0 0;"><a href="/basket.html" class="butn_green">�������� �����</a><span id="close_del_info" class="butn_green close_del_info">����������</span></p>'
            . '</div></div></div>';
    return $html;
}

function msg_metro_not($time){
    $html = "<div id='info_metro_not_working' style='display: none;'>";
    $html .= "<div class=\"del_info\"><div class=\"bott_shad\"><br/><h4>��������� ������!</h4><br/><p>� ��������� ���������� ������� <strong id='metrotime'>$time</strong> �������� �� ����� �� ������������.</p><p>����������, �������� ������ ������ �������� ��� �������������� �����������.</p><br/><p><span id=\"close_del_info\" class=\"butn_green close_del_info\">����������</span></p></div></div></div>";

    return $html;
}

function issue() {
	global $_GLOBALS,$payment_arr,$cur,$COOKIE_REPLACEMENT;

	$COOKIE_REPLACEMENT = true;
	$basket_info = basket_info(array(),'',array('info'=>'full'));
	if ($basket_info['count']==0){
		clearCart();
		HeaderPage('/basket.html');
	}
	if (_IS_USER && $basket_info['basket_order'] === false && $basket_info['user']===false) {
		HeaderPage(_PAGE_ERROR404);
	}

	unset($_GLOBALS['pre_closes_includes']['counters']);
	unset($_GLOBALS['pre_closes_includes']['rambler-counter']);
	unset($_GLOBALS['pre_closes_includes']['googleads']);
	unset($_GLOBALS['pre_closes_includes']['googleadservices']);
	unset($_GLOBALS['pre_closes_includes']['pinit']);
	unset($_GLOBALS['pre_closes_includes']['passadvice']);
	unset($_GLOBALS['pre_closes_includes']['saletex']);

	$discounts = $basket_info['discounts'];
	$u = $basket_info['basket_order_cookie'];

	$datetime = $basket_info['datetime'];
	$services = $basket_info['services'];
	$addes = $basket_info['addes_prices'];
	$deliveres = $basket_info['deliveres'];
	$del_normal = deliveres(false, 0, true);
	$del_holl = deliveres(false, 0, false);
	$holiday = $basket_info['holiday'];
	$summ_sell = (holl_markup($datetime['date'])===false)?'discount':'mark_disc';
	$free_del = query_new("SELECT * FROM sw_delivery_free",1);

	$_GLOBALS['body_pre_close'][] = delivery_photo(true);
	$_GLOBALS['body_pre_close'][] = msg_del_min_price($basket_info[$summ_sell.'_summ'],$free_del,$del_holl,$del_normal,$cur);

	if ($deliveres['del_active']===false){
		$_GLOBALS['js_onready'][] = "windows_info('del_info_cont','close_del_info')";
	}
	// ������� ����� �� ��������

	// if ((($datetime['interval'] == 7) || ($datetime['interval'] == 8)  ) &&  ($deliveres['delivery'] == 5)){
	foreach ($del_normal['dels'] as $del_row){
		if ($del_row['id'] == $datetime['interval']){
			$_GLOBALS['body_pre_close'][] = msg_metro_not($del_row['title']);
		}
	}
	//  $_GLOBALS['js_onready'][] = "windows_info('info_metro_not_working','close_del_info');";
	//  $deliveres['delivery'] = 1;
	//  $deliveres['delivery_1']['check'] = 'checked';
	//}
	$_GLOBALS['js_onready'][] = 'payCur = '.$services['pay_courier'];
	$_GLOBALS['js_onready'][] = 'basket.max_disc = '.intval($_GLOBALS['v_max-discount']);
	$_GLOBALS['js_onready'][] = 'basket.start_disc = '.$discounts['user'];
	$_GLOBALS['js_onready'][] = 'basket.wayTrafic = '.$deliveres['mkad_len_price'];
	$_GLOBALS['js_onready'][] = 'basket.start_over = '.$deliveres['mkad_boundary'];
	$_GLOBALS['js_onready'][] = 'basket.mkad_mkad = '.$deliveres['mkad_mkad'];
	$_GLOBALS['js_onready'][] = '$("input[name=date].datepicker").datepicker({numberOfMonths: 2,onSelect: function(){$(this).attr("disabled",true);basket.interval();}})';
	$_GLOBALS['js_onready'][] = '$("input[name=time]").mask("99:99")';
//    $_GLOBALS['js_onready'][] = '$("input[name=phone], input[name=recipient-phone]").mask("+7 (999) 999-99-99");})';

	if (isset($_SESSION['user']['email'])){
		if (empty($u['email'])){
			$u['email'] = $_SESSION['user']['email'];
		}
	}
	// ��������� ���������������� �������
	if (empty($u['phone'])){
		if (!empty($_SESSION['user']['phone'])){

		}else{

			$phoneSQL = dbone('phone', "`id` = ".quote($_SESSION['uid']), 'users');
			if (!empty($phoneSQL )){
				$u['phone'] = $phoneSQL ;
			}
		}
	}

	$html = '<form id="basket-issue" action="/basket/op=create-order" onsubmit="formSubmit(this, event); return false;">';
	// ORDER BLOCK 1
	$html.= '<div class="createOrderBlock blockWithTopLine"><span class="number">1</span><span class="h2_alt">��������</span><div class="clear"></div>';
	$html.= '<p class="checkcheck" style="display:table;"><label for="formeonly"><input name="subscription" id="formeonly" type="checkbox" value="1" class="polIsZak" onclick="basket.recipient(this)"'.(isset($u['subscription']) && !empty($u['subscription']) ? ' checked' : null).' /> <span style="cursor: pointer;">���! (�������� � ���������� &#151; ���� ����)</span></label></p>';
	$html.= '<div class="regBlock borderRight">';
	$html.= '<div id="z-field-name">���:*<div class="how_to_fill text_block" tb="TYPE_hover_popup" id="tb_name"></div><br /><input name="name" type="text"'.(!empty($u['name']) ? ' value="'.htmlspecialchars($u['name'], ENT_COMPAT | ENT_HTML401, 'cp1251').'"' : null).' onblur="abandoned.put();" class="customer_name" /></div>';
	//$html.= '<div>��������:<br /><input name="patronymic" type="text"'.(!empty($u['patronymic']) ? ' value="'.htmlspecialchars($u['patronymic'], ENT_COMPAT | ENT_HTML401, 'cp1251').'"' : null).' /></div>';
	//$html.= '<div>�������:<br /><input name="family" type="text"'.(!empty($u['family']) ? ' value="'.htmlspecialchars($u['family'], ENT_COMPAT | ENT_HTML401, 'cp1251').'"' : null).' /></div>';
	$html.= '<div id="z-field-phone">�������:*<div class="how_to_fill text_block" tb="TYPE_hover_popup" id="tb_phone"></div><br /><input name="phone" type="text"'.(!empty($u['phone']) ? ' value="'.htmlspecialchars($u['phone'], ENT_COMPAT | ENT_HTML401, 'cp1251').'"' : null).' onblur="abandoned.put();" class="customer_phone" /></div>';
	$html.= '<div class="extra_call_ch" ><label><input type="checkbox" name="extra_call" '.($u['extra_call']==1?' checked ':'').'><span>�� ���������� �� ��������</span></label><div class="how_to_fill text_block" tb="TYPE_hover_popup" id="extra_call"></div></div>';
	$html.= '<div>E-mail:<div class="how_to_fill text_block" tb="TYPE_hover_popup" id="tb_email"></div><br /><input name="email" type="text"'.(!empty($u['email']) ? ' value="'.htmlspecialchars($u['email'], ENT_COMPAT | ENT_HTML401, 'cp1251').'"' : null).' onblur="abandoned.put();" class="customer_email" /></div>';
	$html.= '</div><div class="regBlock" id="me_address" style="display: none;">';
	$html.= '<div>�����:<div class="how_to_fill text_block" tb="TYPE_hover_popup" id="tb_city"></div><br /><input name="city" type="text"'.(!empty($u['city']) ? ' value="'.htmlspecialchars($u['city'], ENT_COMPAT | ENT_HTML401, 'cp1251').'"' : null).' /></div>';
	$html.= '<div id="z-field-street">�����:<br /><textarea name="street" class="addr_area">'.(!empty($u['street']) ? ''.htmlspecialchars($u['street'], ENT_COMPAT | ENT_HTML401, 'cp1251').'' : null).'</textarea></div>';
	//$html.= '<div class="smallCOB"><span id="z-field-home">���:<br /><input name="home" type="text"'.(!empty($u['home']) ? ' value="'.htmlspecialchars($u['home'], ENT_COMPAT | ENT_HTML401, 'cp1251').'"' : null).' /></span><span>������:<br /><input name="housing" type="text"'.(!empty($u['housing']) ? ' value="'.htmlspecialchars($u['housing'], ENT_COMPAT | ENT_HTML401, 'cp1251').'"' : null).' /></span></div>';
	//$html.= '<div class="smallCOB"><span>��������:<br /><input name="apartment" type="text"'.(!empty($u['apartment']) ? ' value="'.htmlspecialchars($u['apartment'], ENT_COMPAT | ENT_HTML401, 'cp1251').'"' : null).' /></span><span>��������:<br /><input name="structure" type="text"'.(!empty($u['structure']) ? ' value="'.htmlspecialchars($u['structure'], ENT_COMPAT | ENT_HTML401, 'cp1251').'"' : null).' /></span><span>��� ��������:<br /><input name="hkey" type="text"'.(!empty($u['hkey']) ? ' value="'.htmlspecialchars($u['hkey'], ENT_COMPAT | ENT_HTML401, 'cp1251').'"' : null).' /></span></div>';
	$html.= '</div>';

	$html.= '';
	$html.= '';
	$html.= '<div class="anonyms text_block centered_hover_popup" tb_cont="anonyms_free" tb="TYPE_hover_popup--append--MULTIPLE_anonyms" style="width: -webkit-fit-content;width: -moz-fit-content;width: fit-content;">';
	$html.= '<span class="h3_alt dashed_uno" style="cursor: help;">�������� �����������</span>';
	$html.= '</div>';
	$html.= '';

	$html.= '<div class="clear"><p>����, ���������� *, <b>�����������</b> � ����������</p></div></div>';
	// ORDER BLOCK 2
	$html.= '<div id="recipient-block" class="createOrderBlock blockWithTopLine'.(!empty($u['subscription']) ? ' noActiveBlock' : null).'">'
		. '<div class="delivery_only_by_phone">' . "���������� ��������, ���� <br/> ������ ����� ��������." . '</div>';

	$html.= '<span class="number">2</span><span class="h2_alt">����������</span><div class="clear"></div>'
		. '<div class="regBlock borderRight">';
	$html.= '<div>���:<div class="how_to_fill text_block" tb="TYPE_hover_popup" id="tb_recipient_name"></div><br /><input name="recipient-name" type="text"'.(isset($u['recipient_name']) && empty($u['subscription']) ? ' value="'.htmlspecialchars($u['recipient_name'], ENT_COMPAT | ENT_HTML401, 'cp1251').'"' : null).' /></div>';
	//$html.= '<div>��������:<br /><input name="recipient-patronymic" type="text"'.(isset($u['recipient_patronymic']) && empty($u['subscription']) ? ' value="'.htmlspecialchars($u['recipient_patronymic'], ENT_COMPAT | ENT_HTML401, 'cp1251').'"' : null).' /></div>';
	//$html.= '<div>�������:<br /><input name="recipient-family" type="text"'.(isset($u['recipient_family']) && empty($u['subscription']) ? ' value="'.htmlspecialchars($u['recipient_family'], ENT_COMPAT | ENT_HTML401, 'cp1251').'"' : null).' /></div>';
	$html.= '<div id="field-recipient-phone">�������:<div class="how_to_fill text_block" tb="TYPE_hover_popup" id="tb_recipient_phone"></div><br /><input name="recipient-phone" type="text"'.(isset($u['recipient_phone']) && empty($u['subscription']) ? ' value="'.htmlspecialchars($u['recipient_phone'], ENT_COMPAT | ENT_HTML401, 'cp1251').'"' : null).' /></div>
       <div class="by_phone_only_main"><label for="by_phone_only_id"><input type="checkbox" id="by_phone_only_id" name="by_phone_only" onclick="basket.by_phone_only();" '.($u['by_phone_only']==1?' checked ':'').'><span style="cursor: pointer;">� ���� ������ ������� ����������</span></label><div class="how_to_fill text_block" tb="TYPE_hover_popup" id="by_phone_only"></div></div>';
	$html.= '</div><div class="regBlock ">';
	$html.= '<div>�����:<div class="how_to_fill text_block" tb="TYPE_hover_popup" id="tb_recipient_city"></div><br /><input name="recipient-city" type="text"'.(isset($u['recipient_city']) && empty($u['subscription']) ? ' value="'.htmlspecialchars($u['recipient_city'], ENT_COMPAT | ENT_HTML401, 'cp1251').'"' : null).' /></div>';
	$html.= '<div id="field-recipient-street">�����:<br /><textarea name="recipient-street" class="addr_area">'.(isset($u['recipient_street']) && empty($u['subscription']) ? ''.htmlspecialchars($u['recipient_street'] , ENT_COMPAT | ENT_HTML401, 'cp1251').'' : null).'</textarea></div>';
	//$html.= '<div class="smallCOB"><span id="field-recipient-home">���:<br /><input name="recipient-home" type="text"'.(isset($u['recipient_home']) && empty($u['subscription']) ? ' value="'.htmlspecialchars($u['recipient_home'] , ENT_COMPAT | ENT_HTML401, 'cp1251').'"' : null).' /></span><span>������:<br /><input name="recipient-housing" type="text"'.(isset($u['recipient_housing']) && empty($u['subscription']) ? ' value="'.htmlspecialchars($u['recipient_housing'], ENT_COMPAT | ENT_HTML401, 'cp1251').'"' : null).' /></span></div>';
	//<div class="smallCOB"><span>��������:<br /><input name="recipient-apartment" type="text"'.(isset($u['recipient_apartment']) && empty($u['subscription']) ? ' value="'.htmlspecialchars($u['recipient_apartment'] , ENT_COMPAT | ENT_HTML401, 'cp1251').'"' : null).' /></span><span>��������:<br /><input name="recipient-structure" type="text"'.(isset($u['recipient_structure']) && empty($u['subscription']) ? ' value="'.htmlspecialchars($u['recipient_structure'], ENT_COMPAT | ENT_HTML401, 'cp1251').'"' : null).' /></span><span>��� ��������:<br /><input name="recipient-hkey" type="text"'.(isset($u['recipient_hkey']) && empty($u['subscription']) ? ' value="'.htmlspecialchars($u['recipient_hkey'], ENT_COMPAT | ENT_HTML401, 'cp1251').'"' : null).' /></span></div>
	$html.= '</div>';
	$html.= '<div class="delivery_obp_inputs"><br/>'
	/*	. '<div><label for="by_phone_only_id"><input type="checkbox" id="by_phone_only_id" name="by_phone_only" onclick="basket.by_phone_only();" '.($u['by_phone_only']==1?' checked ':'').'><span style="cursor: pointer;">� ���� ������ ������� ����������</span></label><div class="how_to_fill text_block" tb="TYPE_hover_popup" id="by_phone_only"></div></div>' */
		. '<div class="bigInput"><span style="margin-bottom: 5px;display: inline-block;">�������������:</span><div class="how_to_fill text_block" tb="TYPE_hover_popup" id="by_phone_only_inf" style="margin: -3px 5px;"></div><br /><textarea name="by_phone_only_inf" cols="1" rows="1">'.(!empty($u['by_phone_only_inf']) ? htmlspecialchars($u['by_phone_only_inf'], ENT_COMPAT | ENT_HTML401, 'cp1251') : null).'</textarea></div>';
	$html.= '</div>';
	$html.= '</div>';
	// ORDER BLOCK 3
	$_GLOBALS['js_onload'][] = '
			 jQuery("#orderBlock3Toggler").on(\'click\', function(e) {    
                      e.preventDefault(); //prevent default behavior
                       jQuery("#orderBlock3Data").toggle();
    
			});

			 jQuery("#orderBlock4Toggler").on(\'click\', function(e) {    
                      e.preventDefault(); //prevent default behavior
                       jQuery("#orderBlock4Data").toggle();
    
			});
			 jQuery("#orderBlock5Toggler").on(\'click\', function(e) {    
                      e.preventDefault(); //prevent default behavior
                       jQuery("#orderBlock5Data").toggle();
    
			});
			';

	$photo_pay_text = ($services['photo']>0)
		?'<span class="gray">(��������� ������ '.$services['photo'].$cur['reduction'].')</span>'
		:'<span class="text_color_orange"><b>���������</b></span>';
	$html.= '<div class="createOrderBlock blockWithTopLine no-padding"><span id="orderBlock3Toggler" style="cursor: pointer;"><span class="number">3</span><span class="h2_alt">�������������� ������</span><span class="gray">(���� ��������, �������� ��� ������ � ��.)</span></span><br />';
	$html.= '<span id="orderBlock3Data" style="display: none; cursor: default;"><p class="checkcheck"><input name="incog" type="checkbox" value="1"'.(!empty($u['incog']) ? ' checked' : null).' /> <span>�� �������� �� ���� �����</span><span class="gray">(������� �������)</span></p>';
	$html.= '<div class="div-relative"><p class="checkcheck">'
		. '<input name="foto" onclick="basket.totPrice()" type="checkbox" value="1" '.(!empty($u['foto']) ? 'checked' : '').' class="totPrice" totp_value="'.$services['photo'].'" totp_action="add"/> '
		. '<span>������� ���� ���������� � ������ ��������</span>' . $photo_pay_text
		. '<span class="foto-delivery" id="foto-delivery"><img src="/i/foto-delivery.png"></span><span class="delyvery_photo_link" onclick="deliveryPhoto.init()" name="delyvery_photo">���� � ��������</span></p><div class="how_to_fill text_block" tb="TYPE_hover_popup" id="tb_foto"></div></div>';
	$html.= '<p class="checkcheck"><input name="neighborhood" type="checkbox" value="1"'.(!empty($u['neighborhood']) ? ' checked' : null).' /> <span>����� �������� ����� �������, �������������</span><span class="text_color_orange"><b>���������</b></span></p>';
	$html.= '<div class="div-relative"><p class="checkcheck text_block" tb="TYPE_to_body--just_btn_close--BTN_yes--BTN_cancel--TIT_warning" id="no_call"><input name="nocall" onclick="no_call.initShow()" type="checkbox" value="1"'.(!empty($u['nocall']) ? ' checked' : null).' /> <span>��������� ����� ��� ������ ����������</span><span class="text_color_orange"><b>���������</b></span></p><div class="how_to_fill text_block" tb="TYPE_hover_popup" id="tb_nocall"></div></div>';
	$html.= '</span></div>';
	// ORDER BLOCK 4
	//$html .= '<div class="flex align-start">';
	$html.= '<div class="createOrderBlock blockWithTopLine no-padding"><span id="orderBlock4Toggler" style="cursor: pointer;"><span class="number">4</span><span class="h2_alt" style="padding-right:10px">����� ��� ��������</span><span class="gray">(� ������� ������ ����������� ���������� ��������)</span></span>';
	$html.= '<span id="orderBlock4Data" style="display: none; cursor: default;"><p class="bigInput">���������:<br /><textarea name="message" cols="1" rows="1">'.(!empty($u['message']) ? htmlspecialchars($u['message'], ENT_COMPAT | ENT_HTML401, 'cp1251') : null).'</textarea></p>';
	$html.= '<p class="bigInput">�������:<br /><input name="signature" type="text"'.(isset($u['signature']) ? ' value="'.htmlspecialchars($u['signature'], ENT_COMPAT | ENT_HTML401, 'cp1251').'"' : null).' /></p>';
	$html.= '</span></div>';
	// ORDER BLOCK 5
	$html.= '<div class="createOrderBlock blockWithTopLine no-padding"><span id="orderBlock5Toggler" style="cursor: pointer;"><span class="number">5</span><span class="h2_alt" style="padding-right:10px">����������� � ������</span><span class="gray">(�� ����������� � ����������)</span></span>';
	$html.= '<span id="orderBlock5Data" style="display: none; cursor: default;"><p class="bigInput">�����������:<br /><textarea name="comment">'.(!empty($u['comment']) ? htmlspecialchars($u['comment'], ENT_COMPAT | ENT_HTML401, 'cp1251') : null).'</textarea></p>';
	$html.= '</span></div>';
	// ORDER BLOCK 6
	$courer_id = 'courer';
	$del_radios = array();
	for ($i=1;$i<6;$i++){
		$val = $deliveres['delivery_'.$i]['value'];
		$off = ($val==='disabled')?$val:'';
		$del_radios[$i]['off'] = $off;
		$val = intval($val)>0?$val:0;
		$del_radios[$i]['val'] = $val;
		$del_radios[$i]['span'] = (($val+$addes['pay_tochdos'])==0)? '<span class="text_color_orange"><b>���������</b></span>':'+'.($val+$addes['pay_tochdos']).$cur['reduction'];
		$del_radios[$i]['input'] = '<div id="del_'.$i.'_cont" class="'.$off.'"><label><input type="radio" id="del_'.$i.'" name="delivery" '
			. 'totp_action="'.$deliveres['delivery_'.$i]['type'].'" '
			. 'totp_value="'.$val.'" '
			. 'class="totPrice" onclick="basket.delivery(this)" value="'.$i.'"'
			. $deliveres['delivery_'.$i]['check'].' '.$off.' />';
	}

	$free_del_price1 = (!empty($free_del) && isset($free_del['free_del']))? ceil($free_del['free_del']/$cur['value']):0;
	$free_del_price2 = (!empty($free_del) && isset($free_del['free_del_h']))? ceil($free_del['free_del_h']/$cur['value']):0;
	$baner_text = (!empty($free_del) && isset($free_del['baner_text']) && $free_del['baner_text']!=='')? $free_del['baner_text']:'';
	$def_class = ($deliveres['del_type']=='holl')? '1':'0 active';
	$hol_class = ($deliveres['del_type']=='holl')? '0 active': '1';

	// ���������� ������ ��� ������ ������� ����
	$jMetro = json_decode(file_get_contents('js/metro/metro_stations_select2.json')) ;
	$placeHolderMetro = "<select id='mselect' data-placeholder=\"�������� �������...\" style=\"width:250px;\" class=\"chosen-select\" ".(($deliveres['delivery'] == 5) ? "" : "disabled=\"disabled\"" ).">";
	foreach ($jMetro as $nm => $jMetroLine){
		$placeHolderMetro .= "<optgroup style=\"background:".$jMetroLine->color.";\" id=\"".$jMetroLine->id."\" label=\"".$jMetroLine->text."\">";
		foreach ($jMetroLine->children as $jMetroItem){
			if (isset($_COOKIE['metro_deliver']) && iconv("UTF-8", "WINDOWS-1251//IGNORE", $_COOKIE['metro_deliver'])  == $jMetroItem->text){
				$placeHolderMetro .= "<option id='".$jMetroItem->id."' style=\"color:".$jMetroLine->color.";\" selected=\"selected\">".$jMetroItem->text."</option>";
			}else{
				$placeHolderMetro .= "<option id='".$jMetroItem->id."' style=\"color:".$jMetroLine->color.";\">".$jMetroItem->text."</option>";
			}

		}
		$placeHolderMetro .= "</optgroup>";
	}
	$placeHolderMetro .= "</select>";
	$html.= '<div class="createOrderBlock blockWithTopLine">'
		. '<table class="delivery_info_tbl"><tr><td>'
		. '<span class="number">6</span>'
		. '<span class="h2_alt">�������� ��� ���������</span><div class="how_to_fill text_block" tb="TYPE_hover_popup" id="tb_delivery_and_getting"></div><br />'
		. '<span class="dopInfo gray">��� ������ ������� ������! �� 7 ���� - '.$_GLOBALS['v_discount-7-days'].'%, �� 14 ���� - '.$_GLOBALS['v_discount-14-days'].'%</span>'
		. '<input type="hidden" id="diff_days" totp_value="'.$discounts['diff_days'].'" class="totPrice" totp_action="disc">'
		. '<p class="dateTime">'
		. '<span id="z-field-data">����:<br />'
		. '<input name="date" type="text" onchange="basket.interval()" class="datepicker"'.(!empty($datetime['date']) ? ' value="'.$datetime['date'].'"' : null).' /></span>'
		. '<span class="relative">�����:<br />'
		. '<select name="interval" onchange="basket.interval()">';
	foreach ($del_normal['dels'] as $del_row){
		$html.= '<option value="'.$del_row['id'].'"'.($del_row['id'] == $datetime['interval'] ? ' selected' : null).'>'.$del_row['title'].'</option>';
	}
	$html.= '</select></span>'
		. '<span class="ili">���</span>'
		. '<span class="relative">� ������� �������:<br />'
		. '<input totp_value="'.$services['pay_tochdos'].'" totp_action="add" class="totPrice" name="time" onblur="basket.courer(this)" type="text" '
		. 'value="'.(!empty($datetime['time']) ? $datetime['time'] : '').'"/> <span class="gray">'
		.(($services['pay_tochdos']>0)? '+'.$services['pay_tochdos'].$cur['reduction'] : '') .'</span></span></p>';
	$html .= '<div class="delivery_off" '.($deliveres['del_active']===false?'style="display:block;"':'').' onclick="windows_info(\'del_info_cont\',\'close_del_info\')"><a href="javascript:windows_info(\'del_info_cont\',\'close_del_info\')">!</a></div>';

	$html .= '<div class="check2">'
		. $del_radios[1]['input'] .'<span class="deliv_spl_text">� �������� ����:</span>'
		. '<span id="'.$courer_id.'" class="gray">'.$del_radios[1]['span'].'</span></label></div>'
		// ������� ��� �������� �� ������� �����
		. " "
		. '<div id="del_5_cont" class="" '. ((($datetime['interval'] == 7) || ($datetime['interval'] == 8) ) ? 'style="opacity: 0.4; pointer-events: none; color:red;"' : "" ) .'><label id="last_metro"><input type="radio" '.(($deliveres['delivery'] == 5) ? 'checked="checked"' : '' ) .' id="del_5" name="delivery" totp_action="add" totp_value="250" class="totPrice" onclick="basket.delivery(this)" value="5"><span id="metro_text" style="">�� ������� �����:</span>&nbsp;&nbsp;&nbsp;<span id="metro_selector">'.$placeHolderMetro.'</span><span id="'.$courer_id.'" class="gray">'.(($deliveres['del_active']===false ) ? "" : $del_radios[1]['span'] ).'</span></label></div>'

		. $del_radios[3]['input'] .'<span class="deliv_spl_text">�� ���� (<span style="margin-left:2px;">�� '.$deliveres['mkad_boundary'].' ��</span>):</span>'
		. '<span id="'.$courer_id.'-okr" class="gray">'.$del_radios[3]['span'].'</span></label></div>'
		. '<div>'. $del_radios[4]['input']
		. '<span class="deliv_spl_text">�� ���� (�� '.$deliveres['mkad_boundary'].' ��):</span></label>'
		. '<span class="over_mkad_inf_cont">'
		. '<span class="mkad_over_input tableCol">'
		. '<a class="minus" href="javascript:basket.mkadOver(false)"></a>'
		. '<input '.$del_radios[4]['off'].' maxlength="3" type="text" value="'.$deliveres['mkad_lenght'].'" name="mkad_pass_lenght" onchange="basket.mkadOver();" onkeyup="basket.mkadOver(\'kup\');">'
		. '<a class="plus" href="javascript:basket.mkadOver(true)"></a>'
		. '</span>'
		. '<span class="gray">�� * '.$deliveres['mkad_len_price'].$cur['reduction'].'/�� =</span>'
		. '<span id="mkad-over" class="gray"><b>'.($del_radios[4]['val']+$addes['pay_tochdos']).$cur['reduction'].'</b></span>&nbsp;'
		. '<div id="del_over_mkad_pop" class="how_to_fill text_block ">'
		.'<div class="tb_hover_popup" id="del_over_mkad_pop_cont">'
		.'<div class="triagle"></div>'
		.'<div class="text_block_cont"><table>'
		. '<tr><td>� ��������� �������� ������:</td><td></td></tr>'
		. '<tr><td>�������� �� ���� (�� '.$deliveres['mkad_boundary'].' ��) </td>'
		. '<td align="right"><span class="'.$courer_id.'-okr">'.$deliveres['mkad_mkad'].'</span>'.$cur['reduction'].'</td></tr>'
		. '<tr><td>'.$deliveres['mkad_len_price'].$cur['reduction'].'/�� ����� '.$deliveres['mkad_boundary'].'��'
		. '<span class="gray" id="pop_calc"> ('.$deliveres['mkad_lenght'].'-'.$deliveres['mkad_boundary'].')*'.$deliveres['mkad_len_price'].'</span> </td>'
		. '<td align="right" id="pop_calc_res">'.$deliveres['mkad_over_clean'].$cur['reduction'].'</td></tr>'
		. '<tr id="ov_mkad_tochdos" '.($addes['pay_tochdos']>0?'':'style="display:none;"').'"><td>�������� � ������� ������� </td>'
		. '<td align="right">'.$services['pay_tochdos'].$cur['reduction'].'</td></tr>'
		. '</table></div>'
		.'</div></div>'
		. '</span>'
		. '</div></div>'
		. $del_radios[2]['input'] . '<span>���������</span><span class="gray">(������ '.$deliveres['delivery_2']['value'].'%)</span></label>'
		. '</div></div>';

	$delFree = query_new("SELECT * FROM sw_modules WHERE link='delivery_free' AND active!='0'",1);
	$bnrClss = empty($delFree)? ' baner_off ':'';

	$btmBlck = (!empty($delFree) && $delFree['active']== 1) ? '<div class=""><span>� ����������� ���:</span><span class="holiday_text">'.$baner_text.'</span><span class="free_price">�� '.$free_del_price2.' '.$cur['reduction'].'</span><span class="text_star">* � �������� ����</span></div>':'';

	$html.= '</td><td align="right"><div class="delivery_free_baner ' . $bnrClss . ' green-grd-light"><div class="title_baner bott_shad"><span class="h2_alt">���������� ��������*</span></div>'
		. '<div class="bott_shad"><span>� ������� ���:</span><span class="free_price">�� ' . $free_del_price1 . ' ' . $cur['reduction'] . '</span></div>' . $btmBlck . '</div></td></tr></table>';

	if ($del_holl['del_opt']['active'] == 1) {
		$html .= '<ul class="sw-tab">'
			. '<li id="default" class="' . $def_class . '">�������� � ������� ����</li>'
			. '<li id="holiday" class="' . $hol_class . '">' . $del_holl['del_opt']['tab'] . '</li></ul>'
			. delivery_tabl_simple($del_normal['dels'], $del_normal['del_opt'], 'default', intval($def_class))
			. delivery_tabl_simple($del_holl['dels'], $del_holl['del_opt'], 'holiday', intval($hol_class));
	}else{
		$html .= delivery_tabl_simple($del_normal['dels'],$del_normal['del_opt'],'default',intval($def_class));
	}
	$html.= '</div>';
	// ORDER BLOCK 7
	$set_payments = array(10,11,4,20,3,5,12,9,17,18,8,13,/*19,*/16,15);

	$html.= '<div class="createOrderBlock changePayment blockWithTopLine">'
		. '<span class="number">7</span><span class="h2_alt">����� ������</span>';
	//$payment_dis = '<table border="0" cellpadding="0" cellspacing="0" width="90%" align="center">';
	$payment_dis = '<div class="payment_types">';
	$count_to_row = 0;
	$count_rows = 0;
	$pay_row = '';
	$p_row_class='';
	foreach ($set_payments as $o_key=>$o_pay){
		if (isset($payment_arr[$o_pay]) && isset($payment_arr[$o_pay]['type']) && $payment_arr[$o_pay]['type']!=''){
			if ($payment_arr[$o_pay]['img']!=''){
				$img = '<span><img src="'.$payment_arr[$o_pay]['img'].'" alt="" /></span>';
			}else{
				$img = '<span><img src="/i/paymentVariant'.$o_pay.'.jpg" alt="" /></span>';
			}
			$add_attr = '';
			switch ($payment_arr[$o_pay]['type']){
				case 'courier':
					$add_attr .= ' totp_value="'.$services['pay_courier'].'" ';

					break;
				case 'cards':
					$p_row_class='bigPayment';
					break;
				case 'cards-assist':
					$p_row_class='bigPayment';
					break;
				case 'cards-courier':
					$p_row_class='bigPayment';
					break;
				case 'cash':
					$p_row_class='bigPayment';
					break;
				case 'emoney':
					$p_row_class = '';
					break;
				case 'mobileoperators':
					$p_row_class = '';
					break;
				default:
					$p_row_class='';
					break;
			}
			/*
			$pay_row .= '<td><label>'.$img
				.'<b><input type="radio" name="type" data-type="'.$payment_arr[$o_pay]['type'].'" value="'.$payment_arr[$o_pay]['nCode'].'" onclick="basket.payment(this)"'
				.((isset($u['type']) && $u['type'] == $payment_arr[$o_pay]['nCode']) ? ' checked ' : null)
				. (isset($payment_arr[$o_pay]['attr']) ? $payment_arr[$o_pay]['attr'] : '').$add_attr
				.' /><span>'.$payment_arr[$o_pay]['text'].'</span></b></label></td>'; */
			$pay_row .= '<div class="'.$p_row_class.'" rel="'.$o_pay.'"><label>'.$img
				.'<b><input type="radio" name="type" data-type="'.$payment_arr[$o_pay]['type'].'" value="'.$payment_arr[$o_pay]['nCode'].'" onclick="basket.payment(this)"'
				.((isset($u['type']) && $u['type'] == $payment_arr[$o_pay]['nCode']) ? ' checked ' : null)
				. (isset($payment_arr[$o_pay]['attr']) ? $payment_arr[$o_pay]['attr'] : '').$add_attr
				.' /><span>'.$payment_arr[$o_pay]['text'].'</span></b></label></div>';

			$count_to_row++;

			/* if ($count_to_row==3){
				 $payment_dis .= $pay_row;
				 $count_rows++;
				 if ($count_rows==4 && count($set_payments)>($o_key+1)){
					 $payment_dis .= '<div class="more-methods-wrapper"><div class="middleButton moremathods">��� ������ ������</div></div>';
					 $p_row_class = 'hiddenmethods';
				 }
				 $pay_row = '';
				 $count_to_row = 0;
			 }*/
		}
	}
	if ($count_to_row>0){
		//$payment_dis .= '<tr class="'.$p_row_class.'">'.$pay_row.'</tr>';
		$payment_dis .= $pay_row;
	}
	//$html .= $payment_dis.'</table>';
	$html .= $payment_dis.'</div>';
	// $u['type'] = 'courier';
	$show_additional_fields = (isset($u['type']) && intval($u['type']) == 1008) ? 'block' : 'none';

	$cur_time_select = '<select name="cur-time">';
	$cur_time_select .= '<option value="8:00-10:00">8:00-10:00</option>';
	$cur_time_select .= '<option value="10:00-12:00">10:00-12:00</option>';
	$cur_time_select .= '<option value="12:00-14:00">12:00-14:00</option>';
	$cur_time_select .= '<option value="14:00-16:00">14:00-16:00</option>';
	$cur_time_select .= '<option value="16:00-18:00">16:00-18:00</option>';
	$cur_time_select .= '<option value="18:00-20:00">18:00-20:00</option>';
	$cur_time_select .= '<option value="19:00-21:00">19:00-21:00</option>';
	$cur_time_select .= '</select>';


	$html.= '<div id="additional-fields" style="display: '.$show_additional_fields.'">'
		. '<p>��� ������ ������� �� ������� ��������� �������������� ����:</p>'
		. '<p class="dateTime"><span>����:<br /><input name="cur-date" type="text" class="datepicker" value="'.(!empty($u['cur-date']) ? $u['cur-date'] : null).'" /></span>'
		. '<span> �����:<br /><input name="cur-time" type="text" value="'.(!empty($u['cur-time']) ? $u['cur-time'] : null).'" /></span></p>'
		//   . '<span> �����:<br />'.$cur_time_select .'</span></p>'
		. '<p class="adress">�����:<br /><input name="cur-adres" type="text" value="'.(!empty($u['cur-adres']) ? $u['cur-adres'] : null).'"/></p>'
		. '<p class="gray">����� ������� �� �������� ������� �� ������ +'.$services['pay_courier'].$cur['reduction'].' (� 8:00 �� 21:00 �����) � �������� ���� <br />'
		. '(���� ��������� �������� � ��������� ���� �����)</p></div>';
	$html .= '</div>';
	$block_cost = ($holiday!==false)? '' : 'big';
	if ($deliveres['delivery']==2){
		unset($addes['pay_tochdos']);
	}
	if ($holiday!==false){
		$hlt = explode('|', $holiday['title']);
		foreach ($hlt as $key=>$var){
			$hlt[$key] = '<span class="tit_text">'.$var.'</span>';
		}
		$holiday['title'] = implode('', $hlt);
		$holi_text = '<div id="hol_text" class="tbb_row holiday_order_row"><table><tr>'
			. '<td><div class="tbb_text_item"><div>'.$holiday['title'].'</div></div></td>'
			. '<td align="right" class="v-align"><div class="BlockCost"><span id="itog2-items">'.($basket_info['mark_disc_summ_full']+array_sum($addes)).'<span class="reduction">'.$cur['reduction'].'</span></span></div></td>'
			. '</tr></table></div>';
	}else{
		$holi_text = '<div id="hol_text"></div>';
	}
	// ���� ����������� �������� ���������� � ������. ���� ������ � ������
	/*
    $html.='<div class="blockWithTopLine" style="padding:15px;">'
        . '<div class="bigBlockWithButtonOk"><table><tr><td>'
            . '<div class="text_block_basket">
						<table><tr><td class="v-align-m">'
                . '<div class="tbb_row"><table><tr>'
                    . '<td><div class="tbb_text_item">'
                        . '<div class="bigBlocktext">� ������� <span id="count-items">'.strItems($basket_info['count']).'</span> �� �����:</div>'
                        . '<div class="dis_text">(� ������ <span id="cur_disc_text">'.  dis_in_one($discounts).'%</span> ������<span id="with_adds" '.(array_sum($addes)>0?'style="display:inline;"':'').'>, � ����� ���������� �����</span> )</div>'
                     // . '<div id="discInfo_inBok" class="diff_disc centered_hover_popup text_block" tb="TYPE_hover_popup--append"><div class="dis_text"><span><i class="fontico-info-circled-alt"></i></span> - �������������� ���������� �� ������</div></div>'
                    . '</div></td>'
                    . '<td align="right" class="v-align"><div class="'.$block_cost.'BlockCost"><span id="itog-items">'.(number_format($basket_info['discount_summ_full']+array_sum($addes), 0, '.', ' ')).'<span class="reduction">'.$cur['reduction'].'</span></span></div></td></tr></table>'
                . '</div>'
                . $holi_text
            . '</td></tr></table></div></td>'
            . '<td><div class="button_basket"><input type="submit" id="issue-button" value="����������� �����" onclick="animatIt(this)" class="bigButton"/></div></td></tr></table>'
        . '</div>'
    . '</div>';
*/
	// ����� ���� ��� ���������� �������
	//��������� ���� ������� �� �����
	$blago_summ = (1 / 100) * ($basket_info['discount_summ_full']+array_sum($addes));

	$html .= '
	<div class="blockWithTopLine" style="padding:15px; text-align: center;">
		<div class="itogo-price"><span class="roboto">�������� ����:</span> <span id="itog-items" class="greenPrice">'.(number_format($basket_info['discount_summ_full']+array_sum($addes), 0, '.', ' ')).'<span class="reduction">'.$cur['reduction'].'</span></span></div>
		<div class="dis_text">(� ������ <span id="cur_disc_text">'.  dis_in_one($discounts).'%</span> ������<span id="with_adds" '.(array_sum($addes)>0?'style="display:inline;"':'').'>, � ����� ���������� �����</span> )</div>
		<div id="discInfo_inBok" class="diff_disc centered_hover_popup text_block" tb="TYPE_hover_popup--append"><div class="dis_text"><span><i class="fontico-info-circled-alt"></i></span> - �������������� ���������� �� ������</div></div>
		<br/>
		<div class="button_basket"><input type="submit" id="issue-button" value="����������� �����" onclick="animatIt(this)" class="bigButton fatButton"/></div>
		<div class="itogo-additional">
			<div class="itogo-additional-line1">���� �������? �������!</div>
			<div class="itogo-additional-line2">���������� ������ �� ������: 8-800-333-12-91</div>
			<div class="itogo-additional-line3">������� � ������ +7 (495) 255-12-91</div>
		</div>
	</div>
	';

	$_GLOBALS['js_onready'][] = '$("input[name=cur-date].datepicker").datepicker({numberOfMonths: 2,onSelect: function(){$("input[name=cur-date]").blur();}})';
	// $_GLOBALS['js_onready'][] = '$("input[name=cur-time]").mask("99:99")';

	$html .= '</form>'.(file_exists('system/templates/ratingBlock.tpl.html')?file_get_contents('system/templates/ratingBlock.tpl.html'):'');
	$_GLOBALS['title'] = '���������� ������';
	$_GLOBALS['text'] = $html;
	$_GLOBALS['html_title'] = $_GLOBALS['title'].' - '.$_GLOBALS['v_sitename'];
	$_GLOBALS['template'] = 'issue';
}

function removeItem() {
    if (empty($_GET['id'])) { HeaderPage(_PAGE_ERROR404); }
    db_delete('basket', '`session` = '.quote(session_id()).' AND `id` = '.quote(abs(intval($_GET['id']))));
    $basket_info = basket_info();
    if ($basket_info['count']==0){
        clearItems(true);
        jQuery::evalScript('href.go("/basket.html");');
        jQuery::getResponse();
    }else{
        jQuery('#item-'.$_GET['id'])->remove();
        jQuery('#mc_item_cont_'.$_GET['id'])->remove();
        jQuery::evalScript('miniCart.itemsRefresh();');
        jQuery::evalScript('basket.totPrice();');
        $_GET['price'] = $basket_info['mark_disc_summ'];
        giftsMore();
    }
}

function clearItems($ret=false) {
    db_delete('basket_order', '`session` = '.quote(session_id()));
    db_delete('basket', '`session` = '.quote(session_id()));
    setcookie('order_hash1', null, null, '/',_CROSS_DOMAIN);
    $_SESSION['order_hash1'] = null;
    unset($_SESSION['order_hash1']);
    cookie_fields_clear('issue_', '/basket/');
    cookie_fields_clear('issue_', '/');
    if (empty($ret)){
        HeaderPage('/basket.html');    
    }
}

function refreshItems() {
    $html = mini_cart();
    jQuery('div.howMuchItem')->html(inUTF8($html));
    jQuery::evalScript('miniCart.hoverRefresh();');
    jQuery::getResponse();
}

function insertItem() {
    if (empty($_GET['id']) || !isset($_GET['sid'])) jQuery::getResponse();

    $pid = 0;
    $id = abs(intval($_GET['id']));
    $sid = abs(intval($_GET['sid']));
    $color = empty($_GET['color'])?0:$_GET['color'];

    $count = !empty($_GET['count']) ? intval($_GET['count']) : 1;
    if ($count == 0) $count = 1;

    if (empty($_GET['pid'])) {
        $p = query_new('SELECT `id` FROM `sw_catalog_price` WHERE `cid` = '.quote($id).' ORDER BY `price` ASC LIMIT 1',1);
        if ($p!==false) {
            $pid = abs(intval($p['id']));
        }
    } else $pid = abs(intval($_GET['pid']));

    $c = ($pid != 0)
        ? query_new('SELECT c.`id`, c.`title`, c.`text`, c.`min`, p.`composition` as `structure`, c.`image`, p.`title` as `type`, p.`price`, s.`url` FROM `sw_catalog_price` as p LEFT JOIN `sw_catalog` as c ON p.`cid` = c.`id` LEFT JOIN `sw_catalog_section` as s ON s.`cid` = c.`id` AND s.`first` = 1 WHERE p.`id` = '.quote($pid).' AND p.`cid` = '.quote($id),1)
        : query_new('SELECT c.`id`, c.`title`, c.`price`, c.`min`, c.`text`, IF(p.`id` IS NULL,c.`structure`,p.`composition`) as `structure`, c.`image`, s.`url` FROM `sw_catalog` as c LEFT JOIN `sw_catalog_price` as p ON p.`cid` = c.`id` LEFT JOIN `sw_catalog_section` as s ON s.`cid` = c.`id` WHERE s.`first` = 1 AND p.`first` = 1 AND c.`id` = '.quote($id),1);
    if ($c===false) {
        $c = query_new('SELECT c.`id`, c.`title`, c.`price`, c.`min`, c.`text`, c.`structure`, c.`image`, s.`url` FROM `sw_catalog` as c LEFT JOIN `sw_catalog_section` as s ON s.`cid` = c.`id` WHERE s.`first` = 1 AND c.`id` = '.quote($id),1);
        if ($c===false){
            jQuery::getResponse();
        }
    }
    if ($pid != 0){
        $c['title'].= ' ('.$c['type'].')';
    }
    if ($count < $c['min']){
        $count = $c['min'];
    }
    $c['text'] = strip_tags($c['text']);
    if (strlen($c['text']) > 200) {
        $c['text'] = substr($c['text'], 0, 200).'...';
    }
    if (dbone('COUNT(`id`)', '`session` = '.quote(session_id()).' AND `cid` = '.quote($id).' AND `pid` = '.quote($pid), 'basket') != 0) {
        db_query('UPDATE `sw_basket` SET `count` = `count` + '.$count.' WHERE `session` = '.quote(session_id()).' AND `cid` = '.quote($id).' AND `pid` = '.quote($pid));
//        refreshItems();
    } else {
        db_query('INSERT INTO `sw_basket` (`cid`, `sid`, `pid`, `count`, `session`, `title`, `text`, `structure`, `price`, `amount`, `color`) 
  VALUES ('.quote($id).', '.quote($sid).', '.quote($pid).', '.quote($count).', '.quote(session_id()).', '.quote($c['title']).', '.quote($c['text']).', '.quote($c['structure']).', '.quote($c['price']).', '.quote($c['price']).', '.quote($color).')');
    }
    jQuery::evalScript('location.href = "/basket.html"');
    jQuery::getResponse();
}


function insertItemEx($id, $sid, $pid, $color, $count) {

	if (empty($id) || !isset($sid)) return false;

	$id = abs(intval($id));
	$sid = abs(intval($sid));
	$color = empty($color)?0:$color;

	$count = !empty($count) ? intval($count) : 1;
	if ($count == 0) $count = 1;

	if (empty($pid)) {
		$p = query_new('SELECT `id` FROM `sw_catalog_price` WHERE `cid` = '.quote($id).' ORDER BY `price` ASC LIMIT 1',1);
		if ($p!==false) {
			$pid = abs(intval($p['id']));
		}
	} else $pid = abs(intval($pid));

	$c = ($pid != 0)
		? query_new('SELECT c.`id`, c.`title`, c.`text`, c.`min`, p.`composition` as `structure`, c.`image`, p.`title` as `type`, p.`price`, s.`url` FROM `sw_catalog_price` as p LEFT JOIN `sw_catalog` as c ON p.`cid` = c.`id` LEFT JOIN `sw_catalog_section` as s ON s.`cid` = c.`id` AND s.`first` = 1 WHERE p.`id` = '.quote($pid).' AND p.`cid` = '.quote($id),1)
		: query_new('SELECT c.`id`, c.`title`, c.`price`, c.`min`, c.`text`, IF(p.`id` IS NULL,c.`structure`,p.`composition`) as `structure`, c.`image`, s.`url` FROM `sw_catalog` as c LEFT JOIN `sw_catalog_price` as p ON p.`cid` = c.`id` LEFT JOIN `sw_catalog_section` as s ON s.`cid` = c.`id` WHERE s.`first` = 1 AND p.`first` = 1 AND c.`id` = '.quote($id),1);
	if ($c===false) {
		$c = query_new('SELECT c.`id`, c.`title`, c.`price`, c.`min`, c.`text`, c.`structure`, c.`image`, s.`url` FROM `sw_catalog` as c LEFT JOIN `sw_catalog_section` as s ON s.`cid` = c.`id` WHERE s.`first` = 1 AND c.`id` = '.quote($id),1);
		if ($c===false){
			jQuery::getResponse();
		}
	}
	if ($pid != 0){
		$c['title'].= ' ('.$c['type'].')';
	}
	if ($count < $c['min']){
		$count = $c['min'];
	}
	$c['text'] = strip_tags($c['text']);
	if (strlen($c['text']) > 200) {
		$c['text'] = substr($c['text'], 0, 200).'...';
	}
	if (dbone('COUNT(`id`)', '`session` = '.quote(session_id()).' AND `cid` = '.quote($id).' AND `pid` = '.quote($pid), 'basket') != 0) {
		db_query('UPDATE `sw_basket` SET `count` = `count` + '.$count.' WHERE `session` = '.quote(session_id()).' AND `cid` = '.quote($id).' AND `pid` = '.quote($pid));
//        refreshItems();
	} else {
		db_query('INSERT INTO `sw_basket` (`cid`, `sid`, `pid`, `count`, `session`, `title`, `text`, `structure`, `price`, `amount`, `color`) 
  VALUES ('.quote($id).', '.quote($sid).', '.quote($pid).', '.quote($count).', '.quote(session_id()).', '.quote($c['title']).', '.quote($c['text']).', '.quote($c['structure']).', '.quote($c['price']).', '.quote($c['price']).', '.quote($color).')');
	}
	return true;
}


function insertPodarok() {
    if (empty($_GET['cid']) || empty($_GET['sid'])) jQuery::getResponse();

    $cid = abs(intval($_GET['cid']));
    $sid = abs(intval($_GET['sid']));

    if (!empty($_POST['pid'])) {
        $pid = abs(intval($_POST['pid']));
        $c = query_new('SELECT c.`title`, c.`price`, c.`min`, p.`composition` as `structure`, s.`url` FROM `sw_catalog_price` as p LEFT JOIN `sw_catalog` as c ON p.`cid` = c.`id` LEFT JOIN `sw_catalog_section` as s ON s.`cid` = c.`id` LEFT JOIN `sw_section` as d ON s.`sid` = d.`id` WHERE p.`id` = '.quote($pid).' AND s.`sid` = '.quote($sid).' AND c.`id` = '.quote($cid).' AND c.`active` = 1',1);
        if ($c===false) jQuery::getResponse();
    } else {
        $pid = 0;
        $c = query_new('SELECT c.`title`, c.`price`, c.`min`, c.`structure`, s.`url` FROM `sw_catalog_section` as s LEFT JOIN `sw_catalog` as c ON s.`cid` = c.`id` LEFT JOIN `sw_section` as d ON s.`sid` = d.`id` WHERE s.`sid` = '.quote($sid).' AND c.`id` = '.quote($cid).' AND c.`active` = 1',1);
        if ($c===false) jQuery::getResponse();
    }

    $c['structure'] = str_replace("\n", '<br />', $c['structure']);
    $count = (abs(intval($_POST['count']))<$c['min']) ? $c['min'] : abs(intval($_POST['count']));
    $item = query_new('SELECT * FROM sw_basket WHERE `session` = '.quote(session_id()).' AND `cid` = '.quote($cid).' AND `pid` = '.quote($pid),1);
    if ($item!== false){
        db_query('UPDATE `sw_basket` SET `count` = `count` + '.$count.' WHERE `session` = '.quote(session_id()).' AND `cid` = '.quote($cid));
        jQuery('#'.$item['id'])->val($item['count']+$count);
        jQuery::evalScript('basket.set('.$item['id'].',"")');
    }else{
        db_query('INSERT INTO `sw_basket` (`cid`, `sid`, `pid`, `count`, `session`, `title`, `structure`, `price`, `amount`) VALUES ('.quote($cid).', '.quote($sid).', '.quote($pid).', '.quote($count).', '.quote(session_id()).', '.quote($c['title']).', '.quote($c['structure']).', '.quote($c['price']).', '.quote($c['price']).')');
        $item = query_new('SELECT b.`id`, c.`id` as cat_id , b.`vase`, b.`box`, b.`cid`, s.`url`, b.`title`, b.`type`, b.`amount`, b.`price`, b.`text`, b.`count`, c.`image`, b.`structure` FROM `sw_basket` as b LEFT JOIN `sw_catalog` as c ON b.`cid` = c.`id` LEFT JOIN `sw_catalog_section` as s ON s.`cid` = b.`cid` AND s.`sid` = b.`sid` WHERE b.`session` = '.quote(session_id()).' AND b.`cid` = '.quote($cid).' AND b.`pid` = '.quote($pid),1);
        if ($item!==false){
            $bitem = basket_item($item);
            jQuery('.howMuchItem')->html(inUTF8(mini_cart()));
            jQuery('#basket_items_cont')->append(inUTF8($bitem));
            jQuery::evalScript('basket.totPrice()');
            jQuery::evalScript('miniCart.itemsRefresh()');
        }
    }
    jQuery('#quickOrderBlock .close')->trigger('click');
    jQuery::getResponse();
}
function basket_item($c=array()){
    global $_GLOBALS,$cur;
    $box = array(
        0=>'����� (���������)',
        1=>'<b style="border-bottom: 1px dashed #333; padding-bottom: 2px;font-weight: normal;">�������� �����</b> ('.ceil($_GLOBALS['v_execution-1']/$cur['value']).$cur['reduction'].')',
        2=>'<b style="border-bottom: 1px dashed #333; padding-bottom: 2px;font-weight: normal;">����������� ��������</b> ('.ceil($_GLOBALS['v_execution-2']/$cur['value']).$cur['reduction'].')',
        3=>'<b style="border-bottom: 1px dashed #333; padding-bottom: 2px;font-weight: normal;">���������� ��������</b> ('.ceil($_GLOBALS['v_execution-3']/$cur['value']).$cur['reduction'].')',
    );
    $boxPrice = array(
        0=>0,
        1=>ceil($_GLOBALS['v_execution-1']/$cur['value']),
        2=>ceil($_GLOBALS['v_execution-2']/$cur['value']),
        3=>ceil($_GLOBALS['v_execution-3']/$cur['value'])
    );
    $boxImage = array(
        0=>'',
        1=>'<img src=\'/i/variant1.jpg\'>',
        2=>'<img src=\'/i/variant2.jpg\'>',
        3=>'<img src=\'/i/variant3.jpg\'>'
    );
    $html = '';
    if (is_array($c) && count($c)>0){
        switch ($c['type']) {
            case 'my-bouquet':
                $image = '<img width="199px" src="/i/myBouquet.gif">';
                $sostav = '������';
                break;
            case 'my-order':
                $image = '<img width="199px" src="/i/individ-100.jpg">';
                $sostav = '���� ���������';
                break;
            default:
                $icons = '<div class="item-stickers">'
                . ((!empty($c['share']))?'<div class="catalog_item_share centered_v_hover_popup shared_items text_block" tb="TYPE_hover_popup--append--MULTIPLE_shared_items" tb_cont="share_text_block"></div>':'')
                . '</div>';
                if (!empty($c['image'])) {
                    $images = explode(':', $c['image']);
                    foreach ($images as $key=>$img){
                        $style = ($key==0)?'':' style="display:none;"';
                        $image .= '<a href="/files/catalog/'.$c['cid'].'/w600_'.$img.'" '.$style.' rel="cart_item_'.$c['cid'].'" class="cart_fancybox">'.$icons.'<img src="/files/catalog/'.$c['cid'].'/w199_'.$img.'" /></a>';
                    }
                }else{
                    $image = '';
                }
                $sostav = '������';
        }
        $html.= '<div id="item-'.$c['id'].'" class="cartItem blockWithTopLine"><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'
                . '<td class="tableText"><a href="'.$c['url'].'" target="_blank" class="name">'.$c['title'].'</a></td>'
                . '<td id="count-'.$c['id'].'" class="tableCol"><a href="javascript:basket.set('.$c['id'].',\'-\')" class="minus"></a><input type="text" id="'.$c['id'].'" rel="'.$boxPrice[$c['box']].'" onchange="javascript:basket.set('.$c['id'].',\'\')" value="'.$c['count'].'"><a href="javascript:basket.set('.$c['id'].',\'+\')" class="plus"></a></td>'
                . '<td class="tableCost"><span id="price-item-'.$c['id'].'">'.$c['amount'].'</span><span class="reduction">'.$cur['reduction'].'</span></td>'
                . '<td class="closeTd"><a class="closeLi" href="javascript: basket.remove('.$c['id'].')">&nbsp;</a></td></tr><tr><td colspan="4">';
        $html.= '<table class="sw-box"><tr><td width="120" class="basket_img_cont">'. $image.'</td>';
        if (!empty($c['structure'])) {
            $html.= '<td><div class="rightInfo"><div class="title">'.$sostav.':</div><table width="100%" cellspacing="0" cellpadding="0" border="0"><tbody>';
            $br = strstr($c['structure'], '<br />') ? '<br />' : "\n";
            foreach (explode($br, $c['structure']) as $l) {
                if ($c['type'] == 'my-order' ){
                    $html .= '<tr><td colspan="2"><b>' . $l . '</b></td></tr>';
                }else {
                    $line = explode(':', $l);
                    if (!empty($line[0]) && !empty($line[1])) {
                        $html .= '<tr><td><b>' . $line[0] . ':</b></td><td>' . $line[1] . '</td></tr>';
                    } elseif (!empty($l)) {
                        $html .= '<tr><td colspan="2"><b>' . $l . '</b></td></tr>';
                    }
                }
            }
            // ������� ������� ������
	        if (!empty($c['color'])){
		        $bColor = explode('-', $c['color']);
		        $colors = query_new('SELECT `id`, `title`, `class` FROM `sw_colors` WHERE `active` = 1 AND `id` IN ('.implode(',', $bColor).') ',0,'id');
		        $spans = array();
		        foreach ($colors as $colorId => $colorItem){
			        $frameColor = '<a href="javascript:void(0);" style="cursor: default;" class="'.$colorItem['class'].'" title="'.htmlspecialchars($colorItem['title'] , ENT_COMPAT | ENT_HTML401, 'cp1251').'">'
				        . '<span>&nbsp;</span>'
				        . (stripos($colorItem['class'],'twoColors')!==false?'<span>&nbsp;</span>':'').'</a>';
			        $spans[] = $frameColor;
		        }
		        /*
		         * <span id="color-list-314" class="colorChange">
  <a href="javascript:void(0);" class="cream" title="��������"><span>&nbsp;</span></a>
  <a href="javascript:void(0);" class="red" title="�������"><span>&nbsp;</span></a>
  <a href="javascript:void(0);" class="orange" title="���������"><span>&nbsp;</span></a>
    </span>
		         */

		        $html .= '<tr><td colspan="2"><span class="colorChange">' . implode(" " , $spans) . '</span></td></tr>';
	        }

            $html.= '</tbody></table></div></td><td class="'.($c['type'] == 'item'?'box':'').'">';
            if ($c['type'] == 'item') {
                foreach ($box as $k=>$b) {
                    $html.= '<p><label class="haveTooltip" title="'.$boxImage[$k].'"><input name="box-'.$c['id'].'" value="'.$k.'" data-mc_amount="'.($c['price']+$boxPrice[$k]).'"  onchange="basket.box('.$c['id'].',this)"'
                            .($c['box'] != $k ? null : ' checked').' type="radio" class="amount_basket" /> '.$b.'</label></p>';
                }
                $html.= '<div class="hr"></div>';
                // ������� ��� ����, ������� ������� � ����� �������
                $vaseSql = "SELECT * FROM sw_vases WHERE `status` = 1;";
                $vaseHtml = '
                <script language="JavaScript">
                jQuery(function(){
                jQuery("#vasescour'.$c['id'].'").jcarousel({
                        
                    animation: "slow"
                });
               
                });
                
                </script>
                ';
                $vaseHtml .= "<div class=\"  jcarousel-skin-tangoSm\"><div class=\"jcarousel-container jcarousel-container-horizontal\" style=\"position: relative; display: block;\">
                                    <div class=\"jcarousel-clip jcarousel-clip-horizontal\" id='vasescour".$c['id']."' style=\"position: relative;\">";
                $vaseHtml .= "<ul class=\"sliderSmall jcarousel-list jcarousel-list-horizontal vases-slider\" style=\"height:156px;left:0;margin:0;overflow: hidden;padding: 0;position: relative;top: 0;\">";
                $vases = query_new($vaseSql);
                // ������ ���������, � �� ������ �� ���� ��� ����
                if ($c['vase'] > 0){
                    $curvase = query_new("SELECT * FROM sw_vases WHERE id = ". intval($c['vase']), 1);
                }
                // ���������� ���������� ��� ���
                foreach ($vases as $a => $vase){
                    $vaseHtml .= "<li style=\"float: left;list-style: outside none none;\" class=\"jcarousel-item jcarousel-item-horizontal jcarousel-item-1 jcarousel-item-1-horizontal\">";
                    $vaseHtml .= '<a href="javascript:basket.vaseappend('.$c['id'].', '.$vase['price'].', '.$vase['id'].');" class="haveTooltip" title=\'<span class="h2_alt">'.htmlspecialchars($vase['title'], ENT_COMPAT | ENT_HTML401, 'cp1251').'</span>\'>';
                    $vaseHtml .= '<img nopin="nopin" width="70" src="/files/vases/'.$vase['id'].'/'.$vase['picture'].'" /><span class="vases-price-wrapper">'.ceil($vase['price'] / $cur['value']).$cur['reduction'].'</span></a></li>';
                }
                $vaseHtml .= "</ul></div>";
                $vaseHtml .= "</div></div>";

                // �������� ���
                if (count($vases) > 0){
                    // ���� ���� ����
                    if (isset($curvase['id'])){
                        $html.= '<p class="vase"><label class="" title="">'
                            . '<input type="checkbox" name="vase-'.$c['id'].'" value="1" data-mc_amount_add="'.ceil($_GLOBALS['v_vase-price']/$cur['value']).'" onchange="basket.vasenew('.$c['id'].');" checked="checked"/> '
                            . '<b style="border-bottom: 1px dashed #333; padding-bottom: 2px;font-weight: normal;">�������� ����</b> <span id="vase-result-'.$c['id'].'" data-mc_amount_add="'.$curvase['price'].'">'.(($c['vase'] > 0) ? "(+ ".$curvase['price']. $cur['reduction'] .")" : "" ).'</span></label></p><div class="vases-block-'.$c['id'].'" style="margin-left: -20px; margin-top: -15px; display: none;">'.$vaseHtml .'</div>';
                    }else{
                        $html.= '<p class="vase"><label class="" title="">'
                            . '<input type="checkbox" name="vase-'.$c['id'].'" value="1" data-mc_amount_add="'.ceil($_GLOBALS['v_vase-price']/$cur['value']).'" onchange="basket.vasenew('.$c['id'].');"/> '
                            . '<b style="border-bottom: 1px dashed #333; padding-bottom: 2px;font-weight: normal;">�������� ����</b> <span id="vase-result-'.$c['id'].'">'.(($c['vase'] > 0) ? "(+ ".$curvase['price']. $cur['reduction'] .")" : "" ).'</span></label></p><div class="vases-block-'.$c['id'].'" style="margin-left: -20px; margin-top: -15px; display: none;">'.$vaseHtml .'</div>';
                    }


                }else{
                    // ��� ������� ��� - ������ ���������
                    $html.= '<p class="vase"><label class="haveTooltip" title="<img src=\'/i/vaza.jpg\' align=\'left\' />">'
                        . '<input type="checkbox" name="vase-'.$c['id'].'" value="1" data-mc_amount_add="'.ceil($_GLOBALS['v_vase-price']/$cur['value']).'" onchange="basket.vase('.$c['id'].',this)"'.($c['vase'] != 1 ? null : ' checked').' /> '
                        . '<b style="border-bottom: 1px dashed #333; padding-bottom: 2px;font-weight: normal;">�������� ����</b> <span>(+ '.ceil($_GLOBALS['v_vase-price']/$cur['value']).$cur['reduction'].')</span></label></p>';
                }
            }
            $html.= '</td>';
        }
        $html.= '</tr></table>';
        $html.= '</td></tr></table></div>';
    }
    return $html;
}
function gift_item($c=array(),$ch='',$en=' disabled '){
    $html = '';
	$image = '';
    if (is_array($c) && isset($c['id'])){
        if (!empty($c['image'])) {
            $images = explode(':', $c['image']);
            //TODO: �������� ������ ������ ������ � ������
			//$image .= '<img src="/files/gifts/'.$c['id'].'/'.$images[0].'" width="150" height="150"/>';
			$image .= '<img src="/files/gifts/'.$c['id'].'/'.$images[0].'"/>';
        }
        $html.= '<div id="gift-'.$c['id'].'" class="giftCartItem"><label>'
			. '<div class="gift_img">'. $image.'</div>'
			. '<div class="gift_check">'
				. '<input type="radio" name="gift" value="'.$c['id'].'" onclick="gifts.setgift('.$c['id'].')" '.$ch.' '.$en.'>'
				. '<span class="name">'.$c['title'].'</span>'
			. '</div></label>'
            .'<div class="gift_reload"><div class="loading-dot"></div></div>'
		. '</div>';
    }
    return $html;
}
function giftsInBasket($price=0,$unset=true){
    $bg = query_new('SELECT b.`id`, g.`id` as `gid`, g.`title`, g.`text`, g.`image`,g.`begin-price`,g.`end-price` FROM `sw_basket` as b LEFT JOIN `sw_gifts` as g ON b.`cid` = g.`id` WHERE b.`session` = '.quote(session_id()).' AND b.`type` = '.quote('gift'),1);
    if (!empty($bg) && !empty($unset) && $price<$bg['begin-price']){
        query_new('DELETE FROM sw_basket WHERE `session` = '.quote(session_id()).' AND `type` = "gift"');
        $bg = false;
    }
    return $bg;
}
function giftsBlock($price=0,$gid=0,$enable=false){
	global $cur;
    $gifts = query_new('SELECT * FROM sw_gifts WHERE `active` = 1 AND CAST(`begin-price` as SIGNED) <= '.quote($price).' ORDER BY `begin-price` DESC LIMIT 3');
	if (empty($gifts)){
		$html = '<div class="nogift_btn">'
			. '<div id="presents" class="btn red_orange-grd text_block" tb="TYPE_to_body--onclick--TIT_ " onclick="windows_info(\'presents_cont\',\'closeNew\')">'
				. '<span class="bg_dashed">������ �������� �������?</span>'
		. '</div></div>';
	}else{
        foreach ($gifts as $key=>$gift){
            $min_price = (empty($min_price) || $gift['begin-price']<$min_price)?$gift['begin-price']:$min_price;
    		$check = ($gift['id']==$gid)?' checked ':'';
            $en = empty($enable)?' disabled ':'';
            $gifts[$key] = gift_item($gift,$check,$en);
        }
        $more_btn = '<div><div id="presents" class="btn orange-grd text_block" tb="TYPE_to_body--onclick--TIT_ " onclick="windows_info(\'presents_cont\',\'closeNew\')"><span class="bg_dashed">���� ������� ������!</span></div></div>';
		$html = '<div class="gift_price_btn"><div><span class="green_text">�������</span> <span class="gray_text">(��� ������ �� '.floor($min_price/$cur['value']).' '.$cur['reduction'].')</span></div>'.$more_btn.'</div>'
            .'<div class="gifts">'.implode(PHP_EOL,$gifts).'</div>';
	}
    return $html;
}

function giftsMore(){
    if (!empty($_GET['price']) && is_numeric($_GET['price'])){
        $price = intval($_GET['price']);
        $gib = giftsInBasket($price,true);
        $gid = empty($gib['gid'])?0:$gib['gid'];
        $html = giftsBlock($price, $gid, true);
        jQuery('.gift_cont')->html(inUTF8($html));
    }
    jQuery::getResponse();
}

function giftSet(){
    query_new('DELETE FROM sw_basket WHERE `session` = '.quote(session_id()).' AND `type` = "gift"');
    $gifts = query_new('SELECT * FROM sw_gifts WHERE `active` = 1 AND id = '.quote($_GET['id']),1);
    if (!empty($gifts)){
        db_query('INSERT INTO `sw_basket` (`cid`, `count`, `session`, `title`, `text`, `type`) VALUES ('.quote($gifts['id']).', 1, '.quote(session_id()).', '.quote($gifts['title']).', '.quote($gifts['text']).', '.quote('gift').')');
    }
    jQuery::getResponse();
}

function initBasket()
{
    global $_GLOBALS,$cur;

    unset($_GLOBALS['pre_closes_includes']['saletex']);
    if (isset($_GET['code']))
    {

		$loadBasket = query_new(sprintf("SELECT * FROM `sw_abandoned_carts` WHERE hash = %s LIMIT 0,1", filter(quote($_GET['code']), 'nohtml')), true);
		if (!empty($loadBasket['datas']))
		{
			clearCart();
			$basket_info_new = unserialize($loadBasket['datas']);
			$products_new = $basket_info_new['products'];
			// ������� ������ 15%
			$_SESSION['cupon'] = 15;
			$_SESSION['discount'] = 15;
			foreach ($products_new as $pr)
			{
				insertItemEx($pr['cid'], $pr['sid'], $pr['pid'], $pr['color'],$pr['count']);
			}



		}
    }

	$basket_info = basket_info();


    $discount = $basket_info['discounts']['user'];
    if ($basket_info['count']>0)
    {
		$_GLOBALS['js_onload'][] = 'basket.max_disc = '.intval($_GLOBALS['v_max-discount']);
		$_GLOBALS['js_onload'][] = 'basket.start_disc = '.$discount;
        $_GLOBALS['js_onload'][] = 'fastBay.init()';
        $gib = giftsInBasket($basket_info['discount_summ']);
        $gid = empty($gib['gid'])?0:$gib['gid'];
        $html = '<div id="basket_items_cont">';
        foreach ($basket_info['products'] as $c) {
            $html .= basket_item($c);
        }

        $html.= '</div><div class="blockWithTopLine" style="padding:15px;">'
                .'<div class="gift_cont">'.giftsBlock($basket_info['discount_summ'],$gid,true). '</div>'
            . '</div>'
			.'<div class="cartItem blockWithTopLine dopItem"><span style="display:inline-block;width:75%;">+ <a href="javascript: void(0);" class="haveTooltip" title="<img src=\'/files/chrysal1.jpg\' align=\'left\' /> ��������� ��� ��������� ������ Chrysal Clear �������� ����������� �������� ��� ������������� �������� ������, � ����� ������������ �������� ����. ������������� Chrysal Clear ����������� ������ �������� ������ � ����������� ������������ ����� � ����.">�����������</a> � ������� � ������� ������, ���������</span><span class="basket_close_btn "><span class="fontico-cancel-circled-3"><a href="/basket/op=clear" >������� ���</a></span></span></div>';
        $cupon = '<div class="left_align"><span class="cupon_title">������� ����� ������:</span></div>'
                . '<div class="left_align"><span class="cupon_title_2">(���� ����)</span></div>'
                . '<div class="cupon_input left_align"><input type="text" name="cupon" ></div>'
                . '<div><span class="butn_green" onclick="cupon.Check();">��������� �����</span></div>';
        if(isset($_SESSION['cupon']) && intval($_SESSION['cupon'])>0){
            $cupon = '<span class="active_cupon">'
                . '<span class="tit_1">��� �����</span>'
                . '<span class="tit_1">�� <b>'.intval($_SESSION['cupon']).'</b>% ������</span>'
                . '<span class="tit_2">�����������<font color="#000">.</font></span>'
            . '</span>';
        }
        $disabled = _IS_USER===true?'disabled':'';
        $html.= '<div class="blockWithTopLine cup_block">'
                . '<div class="cupon_cont">'.$cupon.'</div>'
                . '<div class="fast_baying">'
                    . '<div class="title_in_page">�������?</div>'
                    . '<div style="margin:10px 0;"><span class="butn_orange" onclick="fastBay.show();" id="fast_baying">������� �������</span></div>'
                    . '<div class="popup_link"><span class="text_block centered_hover_popup" id="fast_bay_what_that" tb="TYPE_to_body--onclick--TIT_����� �� ��������">��� ���?</span></div>'
                . '</div>'
                . '<div class="basket_goon">'
                    . '<span class="title">����������</span>'
                    . '<div class="radio_cont">'
                        . '<label><input type="radio" name="goon" value="no_reg" checked '.$disabled.'><span>��� �����������</span></label>'
                        . '<label onclick="basket.basketGoon()"><input type="radio" name="goon" value="reg" '.$disabled.'><span>������������������</span></label><div><span class="text_block popup_link" id="what_that_give" tb="TYPE_to_body--onclick">��� ��� ����?</span></div>'
                        . '<label onclick="basket.basketGoon()"><input type="radio" name="goon" value="login" '.$disabled.'><span>� ���������������</span></label>'
                    . '</div>'
                . '</div>'
                . '</div>';
        $holiday = $basket_info['holiday'];
        if ($holiday!==false){
            $hlt = explode('|', $holiday['title']);
            foreach ($hlt as $key=>$var){
                $hlt[$key] = '<span class="tit_text">'.$var.'</span>';
            }
            $holiday['title'] = implode('', $hlt);
            $block_cost = '';
            $holi_text = '<div id="hol_text" class="tbb_row holiday_order_row"><table><tr>'
                   . '<td><div class="tbb_text_item">'
                            . '<div>'.$holiday['title'].'</div>'
                        . '</div></td>'
                        . '<td align="right"><div class="BlockCost"><span id="itog2-items">'.$basket_info['mark_disc_summ'].'<span class="reduction">'.$cur['reduction'].'</span></span></div></td></tr></table>'
                    . '</div>';
        }else{
            $block_cost = 'big';
            $holi_text = '<div id="hol_text"></div>';
        }
        if ($discount > 0){
            $pre_disc_linktext = ' ';
            $disc_pop_link = '������';
        }else{
            $pre_disc_linktext = ' ������, ';
            $disc_pop_link = '��� �������� ������';
        }
        $disc_pop_link = $pre_disc_linktext.'<div class="disc_pop_link text_block centered_hover_popup" tb_cont="discPop_link" tb="TYPE_hover_popup--append--MULTIPLE_disc_pop_link">'.$disc_pop_link.'</div>';

        $html.='<div class="blockWithTopLine" style="padding:15px;">'
            . '<div class="bigBlockWithButtonOk"><table><tr><td>'
                . '<div class="text_block_basket"><table><tr><td class="v-align-m">'
                    . '<div class="tbb_row"><table><tr>'
                        . '<td><div class="tbb_text_item">'
                            . '<div class="bigBlocktext">� ������� <span id="count-items">'.strItems($basket_info['count']).'</span> �� �����:</div>'
                            . '<div class="dis_text">(� ������ <span id="cur_disc_text">'.$discount.'%</span>'.$disc_pop_link.')</div>'
                        . '</div></td>'
                        . '<td align="right" class="v-align"><div class="'.$block_cost.'BlockCost"><span id="itog-items">'.number_format($basket_info['discount_summ'], 0, '.', ' ').'<span class="reduction">'.$cur['reduction'].'</span></span></div></td></tr></table>'
                    . '</div>'
                    . $holi_text
                . '</td></tr></table></div></td>'
                . '<td align="right"><div class="button_basket"><a href="javascript:basket.basketGoon(\'go\')" class="bigButton">�������� �����</a></div></td></tr></table>'
            . '</div>'
            . '<div class="popup_link" style="text-align:right; padding-top: 5px;"><span class="text_block" id="rulles_of_services" tb="TYPE_to_body--onclick--TIT_������� �������������� �����">������� �������������� �����</span></div>'
        . '</div>';

        $r = query_new('SELECT `id`, `title` FROM `sw_section` WHERE `active` = 1 AND `basket` = 1');
        if ($r !== false) {
            $html.= '<div class="addPresent blockWithTopLine" style="margin-top: 0;"><span class="h2_alt">�������� �������</span><div id="tabs" class="ui-tabs"><ul class="ui-tabs-nav">';
            $item = array();
            foreach ($r as $s) {
                $html.= '<li id="basket_gifts_tab_'.$s['id'].'"><span onclick="gifts.show('.$s['id'].')" style="cursor:pointer;" >'.$s['title'].'</span></li>';
                if (empty($item)){
                    $_GLOBALS['js_onload'][] = 'gifts.show('.$s['id'].')';
                }
                $item[] = $s['id'];
            }
            $html.= '</ul><div id="tabs-data"><p><span class="hint">���������� ���������, ��������� ������...</span></p></div></div></div>';
            $_GLOBALS['head_inc'][] = '<link type="text/css" rel="stylesheet" href="/css/ui/tabs.css" media="screen" />';
            $_GLOBALS['js_onready'][] = '$("#tabs").tabs()';
        }
        $html .= (file_exists('system/templates/ratingBlock.tpl.html')?file_get_contents('system/templates/ratingBlock.tpl.html'):'');
        $_GLOBALS['title'] = '�������';
        $_GLOBALS['template'] = 'default';
    } else {
        cookie_fields_clear('issue_', '/basket/');
        cookie_fields_clear('issue_', '/');
        $p = query_new('SELECT `title`, `text` FROM `sw_pages` WHERE `link` = '.quote('vasha-korzina-pusta'),1);
        if ($p===false) {
            HeaderPage(_PAGE_ERROR404);
        }
        $_GLOBALS['title'] = $p['title'];
        $html = stripslashes($p['text']);
    }

    $html .= '  
        <div class="bottom-link-wrap">            
            <noindex style="margin: 0 auto;display: block;">
                <a class="garkach" style="width:unset;float:none;" href="Javascript:void(window.open(\'http://clck.yandex.ru/redir/dtype=stred/pid=47/cid=2508/*http://market.yandex.ru/shop/56014/reviews\', \'_blank\'));"><img nopin="nopin" src="/i/market.png"/></a>
            </noindex>
        </div>
    ';

    $_GLOBALS['text'] = $html;
    $_GLOBALS['html_title'] = $_GLOBALS['title'].' - '.$_GLOBALS['v_sitename'];
}

// ����� �������� ��������� email (���� ������) � ������
function sendMessage()
{
    $oid = intval($_GET['oid']);

    //$smsQueue = new SmsQueue();
    //$smsQueue->addMessageInQueue(array('oid'=>$oid, 'msg_type'=>SmsQueue::ORDER_ONLINE_RECEIVED_NO_CASH));
    //$smsQueue->sendEmailWhenOrderReceived(array('oid'=>$oid));
    $p = query_new('SELECT `itogo`, `recipientSMS`, `type` FROM `sw_orders` WHERE `id` = '.$oid, 1);
    logevent(11, $_SESSION['user']['id'], 1, array('order_id' => $oid, 'itog' => $p['itogo'], 'comment' =>$p['recipientSMS'], 'type' => $p['type'], 'is_payed' =>0) );
    jQuery::getResponse();
}
