<?php
    if (!defined('_SWS_ADMIN_CORE')) {exit();}
	
	$_GLOBALS['create_dir'] = true;
	$_GLOBALS['add'] = true;
	$_GLOBALS['search'] = true;
	$_GLOBALS['on-page'] = 20;
	$_GLOBALS['tiny'] = true;
	$_GLOBALS['date'] = false;
	$_GLOBALS['nesting'] = false;
	
	$_GLOBALS['field']['title']['id'] = 'ID';
	$_GLOBALS['field']['title']['sid'] = '������/���������';
	$_GLOBALS['field']['title']['active'] = '���./����.';
	$_GLOBALS['field']['title']['pos'] = '�������';
	$_GLOBALS['field']['title']['title'] = '��������';
	$_GLOBALS['field']['title']['link'] = '��������� ���';
	$_GLOBALS['field']['title']['text'] = '�������� ������';
	$_GLOBALS['field']['title']['colors'] = '����� ������';
	$_GLOBALS['field']['title']['min'] = '����������� ���-��';
	$_GLOBALS['field']['title']['price'] = '����';
	$_GLOBALS['field']['title']['price-old'] = '������ ����';
	$_GLOBALS['field']['title']['art'] = '�������';
	$_GLOBALS['field']['title']['size'] = '������';
	$_GLOBALS['field']['title']['structure'] = '������';
	$_GLOBALS['field']['title']['index'] = '�� �������';
    $_GLOBALS['field']['title']['index_pos'] = '������� �� �������';
	$_GLOBALS['field']['title']['hit'] = '���';
	$_GLOBALS['field']['title']['season'] = '����� ������';
	$_GLOBALS['field']['title']['image'] = '�����������';
    $_GLOBALS['field']['title']['share'] = '������ �� �����';
	$_GLOBALS['field']['title']['html_title'] = '��������� �������� (TITLE)';
	$_GLOBALS['field']['title']['keywords'] = '�������� ����� (KEYWORDS)';
	$_GLOBALS['field']['title']['description'] = '�������� (DESCRIPTION)';
	
	$_GLOBALS['field']['type']['sid'] = 'catalog';
	$_GLOBALS['field']['type']['active'] = 'checkbox';
	$_GLOBALS['field']['type']['pos'] = 'position';
	$_GLOBALS['field']['type']['title'] = 'title';
	$_GLOBALS['field']['type']['link'] = 'text';
	$_GLOBALS['field']['type']['text'] = 'html';
	$_GLOBALS['field']['type']['colors'] = 'colors';
	$_GLOBALS['field']['type']['min'] = 'text';
	$_GLOBALS['field']['type']['price'] = 'text';
	$_GLOBALS['field']['type']['price-old'] = 'text';
	$_GLOBALS['field']['type']['art'] = 'text';
	$_GLOBALS['field']['type']['size'] = 'text';
	$_GLOBALS['field']['type']['structure'] = 'textarea';
	$_GLOBALS['field']['type']['index'] = 'checkbox';
    $_GLOBALS['field']['type']['index_pos'] = 'position';
	$_GLOBALS['field']['type']['hit'] = 'checkbox';
    $_GLOBALS['field']['type']['share'] = 'checkbox';
	$_GLOBALS['field']['type']['season'] = 'checkbox';
	$_GLOBALS['field']['type']['image'] = 'image';
	$_GLOBALS['field']['type']['html_title'] = 'textarea';
	$_GLOBALS['field']['type']['keywords'] = 'textarea';
	$_GLOBALS['field']['type']['description'] = 'textarea';
	
	$_GLOBALS['field']['db']['sid'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['active'] = "int(1) NOT NULL default '0'";
	$_GLOBALS['field']['db']['pos'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['title'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['link'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['text'] = "text";
	$_GLOBALS['field']['db']['colors'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['min'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['price'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['price-old'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['index'] = "int(1) NOT NULL default '0'";
    $_GLOBALS['field']['db']['index_pos'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['hit'] = "int(1) NOT NULL default '0'";
    $_GLOBALS['field']['db']['share'] = "int(1) NOT NULL default '0'";
	$_GLOBALS['field']['db']['size'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['structure'] = "text";
	$_GLOBALS['field']['db']['season'] = "int(1) NOT NULL default '0'";
	$_GLOBALS['field']['db']['art'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['image'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['html_title'] = "text";
	$_GLOBALS['field']['db']['keywords'] = "text";
	$_GLOBALS['field']['db']['description'] = "text";
	$_GLOBALS['field']['db']['insert'] = "timestamp NOT NULL default CURRENT_TIMESTAMP";
	$_GLOBALS['field']['db']['system'] = "int(1) NOT NULL default '0'";
	
	$_GLOBALS['field']['table']['id'] = 'text';
	$_GLOBALS['field']['table']['title'] = 'title';
	$_GLOBALS['field']['table']['index'] = 'checkbox';
	
	$_GLOBALS['field']['value']['active'] = 1;
	$_GLOBALS['field']['value']['min'] = 1;
    $_GLOBALS['field']['value']['index_pos'] = 0;
	
	$_GLOBALS['field']['settings']['image']['count'] = 10;
	$_GLOBALS['field']['settings']['image']['type'] = '"�����������": "*.jpg; *.jpeg; *.gif; *.png"';
	$_GLOBALS['field']['settings']['image']['size'] = array(array('width'=>70, 'height'=>70, 'ratio'=>true), array('width'=>120, 'height'=>90, 'ratio'=>true), array('width'=>128, 'height'=>116, 'ratio'=>true), array('width'=>168, 'height'=>192, 'ratio'=>true), array('width'=>199, 'height'=>287, 'ratio'=>true), array('width'=>600, 'height'=>400, 'ratio'=>true, 'watermark'=>true));
	
	$_GLOBALS['edit-field'] = array('active', 'index', 'index_pos', 'hit', 'season', 'share', 'sid', 'title', 'link', 'text', 'colors', 'min', 'price', 'price-old', 'art', 'structure', 'size', 'html_title', 'keywords', 'description', 'image');
	$_GLOBALS['add-field'] = $_GLOBALS['edit-field'];
        
    $check_tbls_cols = array(
        'catalog_section'=>array(
            'cid_pos' => "int(11) NOT NULL default '0'"
        ),
        'catalog'=>$_GLOBALS['field']['db']
    );
    check_tbl($check_tbls_cols);