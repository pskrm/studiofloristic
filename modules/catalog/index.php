<?php
if (!defined('SW')) {die('���� ������� �� ���������');}

switch($op) {
    case 'podarok': winPodarok(); break;
    case 'fast': fastOrder(); break;
    case 'dir': dirItems(); break;
    case 'create': createItem(); break;
    case 'filter': filterItems(); break;
    case 'bestsellers': bestsellers(); break;
    case 'season': season(); break;
    case 'item': viewItem(); break;
    case 'cur-go': curGo(); break;
    case 'viewed': viewedItems();break;
    case 'instagram': instagram_block();break;
    case 'delinf': deliveryInfo(); break;
    default: initCatalog(); break;
}

if ($_REQUEST['op'] != 'dir') {
    $_SESSION['cf'] = '';
    $_SESSION['cfp'] = '';
}

function deliveryInfo() {
    global $op, $module, $_GLOBALS,$cur;
    include_once('include/jquery/jQuery.php');

    $fname = _MODULS_PACH . $module . '/tpl/' . $op . '.tpl.xhtml';
    if (file_exists($fname)) {
        $data     = array('d' => 'data-d');
        $dwTpl    = array();
        $dTpl     = array();
        $tTpl     = array();
        $free     = array();
        $m        = 'min_';
        $tpl      = file_get_contents($fname);
        $ctime    = strtotime(date('Y-m-d'));
        $dls['d'] = query_new('SELECT * FROM `sw_delivery` WHERE `active` = 1 ORDER BY `start`', 0, 'start');
        $ops['d'] = query_new('SELECT * FROM `sw_delivery_ships` LIMIT 1', 1);
        $ops['d'][$m . 'omkad'] = $ops['d'][$m . 'mkad'];

        $dels_hOp = query_new('SELECT * FROM `sw_delivery_holiday_options` WHERE `active` = 1 AND `start` IS NOT NULL AND `end` IS NOT NULL LIMIT 1', 1);
        if (!empty($dels_hOp)) {
            $tpl = str_replace('<!--[%HDAY_TEXT%]-->', (!empty($_GLOBALS['v_dcal_holl_info'])?$_GLOBALS['v_dcal_holl_info']:$dels_hOp['tab']), $tpl);
            $dels_hOp[$m.'omkad'] = $dels_hOp[$m.'mkad'];
            $data['dh']='data-dh';
            $ops['dh'] = $dels_hOp;
            $dls['dh'] = query_new('SELECT * FROM `sw_delivery_holiday` WHERE `active` = 1 ORDER BY `start`', 0, 'start');
        }

        foreach ($_GLOBALS['dlryWay'] as $dwn => $dw) {
            $mn = $m . $dwn;
            foreach ($data as $k => $dd) {
                $ops[$k][$mn] = floor($ops[$k][$mn] / $cur['value']);
                $dw[]         = $dd . $dwn . '="' . $ops[$k][$mn] . '"';
            }
            $mn      = $dw['text'];
            unset($dw['text']);
            $dwTpl[] = '<option value="' . $dwn . '" ' . implode(' ', $dw) . '>' . $mn . '</option>';
        }

        for ($i = 0; $i < 40; $i++) {
            $t      = $ctime + $i * 86400;
            $v      = (!empty($ops['dh']) && $t >= strtotime($ops['dh']['start']) && $t <= strtotime($ops['dh']['end'])) ? 'dh' : 'd';
            $tText  = ($i > 1 ? '' : ($i == 0 ? '�������, ' : '������, '))
                    . strtolower($_GLOBALS['date'][date('D', $t)]) . ', ' . date('d', $t) . ' '
                    . strtolower($_GLOBALS['date'][date('M', $t)]) . ', ' . date('Y') . '�.';
            $dTpl[] = '<option class="' . strtolower(date('D', $t)) . '" value="' . $v . '">' . $tText . '</option>';
            if ($i == 0) {
                $tpl = str_replace('<!--[%CITY%]-->', ($v == 'd' ? $ops['d'][$m . 'city'] : $ops['dh'][$m . 'city']) . $cur['reduction'], $tpl);
            }
        }

        $hip = array('city'=>array(),'mkad'=>array(),'hcity'=>array(),'hmkad'=>array());
        foreach ($dls['d'] as $k => $d) {
            $oAtrr  = array();
            $oClass = array('citydlp','citydhip','mkaddlp','mkaddhip','citydhlp','citydhhip','mkaddhlp','mkaddhhip','omkadd','omkaddh');
            foreach ($_GLOBALS['dlryWay'] as $dwn => $dw) {
                foreach ($data as $kk => $dd) {
                    $price   = $dwn == 'city' ? 'center' : 'okrayna';
                    $price   = floor($dls[$kk][$k][$price] / $cur['value']);
                    $oAtrr[] = $dd . $dwn . '="' . $price . '"';
                }
            }
            if (!empty($d[$m.'city'])){
                $hip['city'][] = array('start'=>$d['start'],'end'=>$d['end']);
                unset($oClass[array_search('citydhip', $oClass)]);
            }elseif(empty($d[$m.'city']) && !empty($hip['city'])){
                $tmp = array_shift($hip['city']);
                $tmp = explode(':',$tmp['start']);
                $free[] = $tmp[0].':'.$tmp[1];
                $tmp = array_pop($hip['city']);
                $tmp = explode(':',$tmp['end']);
                $free[] = $tmp[0].':'.$tmp[1];
                $tTpl[] = '<option value="0" class="citydhip" data-dcity="0">' . implode(' - ',$free) .$addtext.'</option>';
                if (!empty($_GLOBALS['v_addtext_city'])){
                    $opts = explode("\n", $_GLOBALS['v_addtext_city']);
                    foreach ($opts as $v) {
                        $tTpl[] = '<option class="citydhip info" disabled="disabled">'.$v.'</option>';
                    }
                }
                $free = array();
                $hip['city'] = array();
            }
            if (!empty($d[$m.'mkad'])){
                $hip['mkad'][] = array('start'=>$d['start'],'end'=>$d['end']);
                unset($oClass[array_search('mkaddhip', $oClass)]);
            }elseif(empty($d[$m.'mkad']) && !empty($hip['mkad'])){
                $tmp = array_shift($hip['mkad']);
                $tmp = explode(':',$tmp['start']);
                $free[] = $tmp[0].':'.$tmp[1];
                $tmp = array_pop($hip['mkad']);
                $tmp = explode(':',$tmp['end']);
                $free[] = $tmp[0].':'.$tmp[1];
                $tTpl[] = '<option value="0" class="mkaddhip" data-dmkad="0">' . implode(' - ',$free) .'</option>';
                if (!empty($_GLOBALS['v_addtext_mkad'])){
                    $opts = explode("\n", $_GLOBALS['v_addtext_mkad']);
                    foreach ($opts as $v) {
                        $tTpl[] = '<option class="mkaddhip info" disabled="disabled">'.$v.'</option>';
                    }
                }
                $free = array();
                $hip['mkad'] = array();
            }
            if (!empty($dls['dh'][$k][$m.'city'])){
                $hip['hcity'][] = array('start'=>$dls['dh'][$k]['start'],'end'=>$dls['dh'][$k]['end']);
                unset($oClass[array_search('citydhhip', $oClass)]);
            }elseif(empty($dls['dh'][$k][$m.'city']) && !empty($hip['hcity'])){
                $tmp = array_shift($hip['hcity']);
                $tmp = explode(':',$tmp['start']);
                $free[] = $tmp[0].':'.$tmp[1];
                $tmp = array_pop($hip['hcity']);
                $tmp = explode(':',$tmp['end']);
                $free[] = $tmp[0].':'.$tmp[1];
                $tTpl[] = '<option value="0" class="citydhhip" data-dhcity="0">' . implode(' - ',$free) .'</option>';
                if (!empty($_GLOBALS['v_addtext_dh_city'])){
                    $opts = explode("\n", $_GLOBALS['v_addtext_dh_city']);
                    foreach ($opts as $v) {
                        $tTpl[] = '<option class="citydhhip info" disabled="disabled">'.$v.'</option>';
                    }
                }
                $free = array();
                $hip['hcity'] = array();
            }
            if (!empty($dls['dh'][$k][$m.'mkad'])){
                $hip['hmkad'][] = array('start'=>$dls['dh'][$k]['start'],'end'=>$dls['dh'][$k]['end']);
                unset($oClass[array_search('mkaddhhip', $oClass)]);
            }elseif(empty($dls['dh'][$k][$m.'mkad']) && !empty($hip['hmkad'])){
                $tmp = array_shift($hip['hmkad']);
                $tmp = explode(':',$tmp['start']);
                $free[] = $tmp[0].':'.$tmp[1];
                $tmp = array_pop($hip['hmkad']);
                $tmp = explode(':',$tmp['end']);
                $free[] = $tmp[0].':'.$tmp[1];
                $tTpl[] = '<option value="0" class="mkaddhhip" data-dhmkad="0">' . implode(' - ',$free) . '</option>';
                if (!empty($_GLOBALS['v_addtext_dh_mkad'])){
                    $opts = explode("\n", $_GLOBALS['v_addtext_dh_mkad']);
                    foreach ($opts as $v) {
                        $tTpl[] = '<option class="mkaddhhip info" disabled="disabled">'.$v.'</option>';
                    }
                }
                $free = array();
                $hip['hmkad'] = array();
            }
            $tTpl[] = '<option ' . implode(' ', $oAtrr) . ' class="' .implode(' ', $oClass) . '">' . $d['title'] . '</option>';
        }

        $payInfBlocks = array();
        if (!empty($_GLOBALS['v_pay_info_dh_mkad'])){
            $_GLOBALS['v_pay_info_dh_mkad'] = str_replace("\n",'</br>',$_GLOBALS['v_pay_info_dh_mkad']);
            $payInfBlocks[] = '<div class="pay_info dh_mkad">'.$_GLOBALS['v_pay_info_dh_mkad'].'</div>';
        }
        if (!empty($_GLOBALS['v_pay_info_mkad'])){
            $_GLOBALS['v_pay_info_mkad'] = str_replace("\n",'</br>',$_GLOBALS['v_pay_info_mkad']);
            $payInfBlocks[] = '<div class="pay_info d_mkad">'.$_GLOBALS['v_pay_info_mkad'].'</div>';
        }
        if (!empty($_GLOBALS['v_pay_info_dh_city'])){
            $_GLOBALS['v_pay_info_dh_city'] = str_replace("\n",'</br>',$_GLOBALS['v_pay_info_dh_city']);
            $payInfBlocks[] = '<div class="pay_info dh_city">'.$_GLOBALS['v_pay_info_dh_city'].'</div>';
        }
        if (!empty($_GLOBALS['v_pay_info_city'])){
            $_GLOBALS['v_pay_info_city'] = str_replace("\n",'</br>',$_GLOBALS['v_pay_info_city']);
            $payInfBlocks[] = '<div class="pay_info d_city">'.$_GLOBALS['v_pay_info_city'].'</div>';
        }

        $tpl = str_replace('<!--[%INTIME%]-->', '<input type="checkbox" name="inTime" data-price="'.ceil($_GLOBALS['v_pay-tochdos'] / $cur['value']).'" onchange="deliveryCalc.setSels()"/>', $tpl);
        $tpl = str_replace('<!--[%OMKAD_TEXT%]-->', '�� ���� (�� '.$_GLOBALS['v_start_over_MKAD'].' ��)', $tpl);
        $tpl = str_replace('<!--[%WAY%]-->', implode(PHP_EOL, $dwTpl), $tpl);
        $tpl = str_replace('<!--[%DAYS%]-->', implode(PHP_EOL, $dTpl), $tpl);
        $tpl = str_replace('<!--[%TIMES%]-->', implode(PHP_EOL, $tTpl), $tpl);
        $tpl = str_replace('<!--[%TIME_INT_PRICE%]-->', implode(PHP_EOL, $payInfBlocks), $tpl);
        $tpl = str_replace('<!--[%PKM%]-->', ceil($_GLOBALS['v_m_per_km'] / $cur['value']).' '.$cur['reduction'], $tpl);
        $tpl = str_replace('<!--[%IT_PRICE%]-->', '+'.ceil($_GLOBALS['v_pay-tochdos'] / $cur['value']).' '.$cur['reduction'], $tpl);
        $tpl = str_replace('<!--[%OMKADIN%]-->', '<input  type="text" name="price" maxlength="3" data-radioset="" data-km="0" data-km-price="'.$_GLOBALS['v_m_per_km'].'" data-start-omkad="'.$_GLOBALS['v_start_over_MKAD'].'" value="'.$_GLOBALS['v_start_over_MKAD'].'" onchange="deliveryCalc.oMkad()" onblur="deliveryCalc.oMkad()" onkeyup="deliveryCalc.oMkad(\'kup\')">', $tpl);

        jQuery('body')->append(inUTF8($tpl));
        jQuery::evalScript("deliveryCalc.initVars()");
        jQuery::evalScript("deliveryCalc.free()");
        jQuery::evalScript("windows_info('delcalc','closeNew')");
    } else {
        jQuery::evalScript('initSysDialog("' . inUTF8('��������������� ������ �������� �������� ����������.') . '")');
    }

    jQuery::getResponse();
}
function curGo() {
    include_once('include/jquery/jQuery.php');

    if (empty($_GET['value'])) jQuery::getResponse();

    $c = get_currency($_GET['value']);
    if ($c===false) {
        jQuery::getResponse();
    }
    jQuery::evalScript('cur.fastSet({"id":"'.$c['name'].'","value":"'.$c['value'].'"})');
    jQuery::getResponse();
}

function winPodarok() {
    global $cur;
    include_once('include/jquery/jQuery.php');

    if (empty($_GET['id']) || empty($_GET['sid'])) { jQuery::getResponse(); }

    $id = abs(intval($_GET['id']));
    $sid = abs(intval($_GET['sid']));
    $c = query_new('SELECT c.`min`, c.`id`, c.`title`, c.`text`, c.`art`, c.`size`, c.`colors`, c.`structure`, c.`price`, c.`price-old`, c.`image`, c.`html_title`, c.`keywords`, c.`description`, d.`link`, d.`sid` FROM `sw_catalog_section` as s LEFT JOIN `sw_catalog` as c ON s.`cid` = c.`id` LEFT JOIN `sw_section` as d ON s.`sid` = d.`id` WHERE s.`sid` = '.quote($sid).' AND c.`id` = '.quote($id).' AND c.`active` = 1',1);
    if ($c===false) {
        jQuery('div#quickOrderBlock')->html(inUTF8('<span class="close">&nbsp;</span><span class="h2_alt">�������� �������</span><p>����� �� �������</p>'));
        jQuery::evalScript('fast.load()');
        jQuery::getResponse();
    }
    $structure = !empty($c['structure']) ? explode("\n", $c['structure']) : array();

    $html = '<span class="close"></span><span class="h2_alt">�������� �������</span><form action="/basket/op=podarok&cid='.$id.'&sid='.$sid.'" onsubmit="formSubmit(this); return false;" class="formOrder">';
    $html.= '<div class="product">';
    if (!empty($c['image'])) {
        $images = array();
        $image = explode(':', $c['image']);
        $html.= '<div class="leftProd floatLeft"><div class="img">';
        foreach ($image as $i=>$img){
            $style = ($i==0)?'':' style="display:none;" ';
            $html .= '<a '.$style.' id="fast-list-images_'.$i.'" class="fast-list-images_all" href="/files/catalog/'.$c['id'].'/w600_'.$image[$i].'" rel="fast-product"><img src="/files/catalog/'.$c['id'].'/w199_'.$image[$i].'" /></a>';
            $images[] = '"'.$img.'"';
        }
        $html.= '</div>';
        if (count($image) > 1) {
            $html.= '<div id="fast-list-images"></div>';
            $html.= '<script type="text/javascript">'
                    . 'fast.pach="/files/catalog/'.$c['id'].'/";'
                    . 'fast.item=['.implode(',',$images).'];'
                    . 'fast.carousel(0,\'fast-list-images\')</script>';
        }
        $html.= '</div>';
    }
    $html.= '<div class="rightProd floatRight"><span class="h1_alt sn-pxg">'.$c['title'].'</span><div class="leftInfo floatLeft">';
    if (!empty($c['size'])){ $html.= '<div><b>������:</b> '.$c['size'].'</div>'; }
    if (!empty($c['art'])){ $html.= '<div><b>�������:</b> '.$c['art'].'</div>'; }
    if (intval($c['min'])) {
        $html.= '<div class="infoCol"><b>���-��:</b> <span class="tableCol">'
                . '<a href="javascript:item.minus(\'fast-count-item\',1)" class="minus"></a>'
                . '<input type="text" id="fast-count-item" value="'.$c['min'].'" name="count">'
                . '<a href="javascript:item.plus(\'fast-count-item\');" class="plus"></a>'
            . '</span><div class="clear"></div></div>';
    }
    if (!empty($c['colors'])) {
        $bColor = explode(':', $c['colors']);
        $colors = query_new('SELECT `id`, `title`, `class` FROM `sw_colors` WHERE `active` = 1 AND `id` IN ('.implode(',', $bColor).') ',0,'id');
        $html.= '
        <script>
var colors = {
    set: function (id, cid) {
        if (jQuery(\'span#color-list-\' + id + \' a#c\' + cid).hasClass(\'active\')) {
            jQuery(\'span#color-list-\' + id + \' a#c\' + cid).removeClass(\'active\');
            jQuery(\'span#color-list-\' + id + \' a#c\' + cid).children(\'input\').prop(\'checked\', false);
            
        } else {
            jQuery(\'span#color-list-\' + id + \' a#c\' + cid).addClass(\'active\');
            jQuery(\'span#color-list-\' + id + \' a#c\' + cid).children(\'input\').prop(\'checked\', true);
            
        }
    }
};
        </script>';
        $html.= '<link type="text/css" rel="stylesheet" href="/css/colors.css" media="screen" /><div class="infoColor"><b>����:</b><span id="color-list-'.$c['id'].'" class="colorChange"><input type="hidden" name="color-'.$c['id'].'" value="0">';
        foreach ($colors as $id=>$color) {
            $html.= '<a id="c'.$id.'" href="#" '.(isIphone() ? 'onctouchstart="colors.set('.$c['id'].','.$id.'); return false;"' : 'onclick="colors.set('.$c['id'].','.$id.'); return false;"').' class="'.$color['class'].'" title="'.htmlspecialchars($color['title'] , ENT_COMPAT | ENT_HTML401, 'cp1251').'"></a>';

        }
        $html.= '</span><div class="clear"></div></div>';
        $color = true;
    } else { $color = false; }
    $html.= '</div>';

    $price_list = '';
    $b = query_new('SELECT `start`, `end`, `title`, `center` FROM `sw_holiday` WHERE `active` = "1"',1);
    if ($b!==false){
        $b['title'] = str_replace('|','<br>',$b['title']);
    }
    $r = query_new('SELECT `id`, `title`, `price`, `price-old`, `size`, `composition`, `first` FROM `sw_catalog_price` WHERE `cid` = '.quote($c['id']).' ORDER BY `price` ASC');
    if (count($r)>0 && $r!==false) {
        $price_list = '<div class="title">����:</div>
        <script type="text/javascript">
            $(document).ready(function(){
                $("#chek").prev("input").click();
            });
        </script>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">';
        foreach ($r as $p) {
            $check = !empty($p['first']) ? ' checked ':'';
            $check_span = !empty($p['first']) ? ' <span id="chek" class="gray">(�� ����)</span>':'';
            if (!empty($p['composition']) && !empty($p['first'])){
                $structure = explode('<br />', $p['composition']);
            }
            if (!empty($p['size']) && !empty($p['first'])){
                $c['size'] = $p['size'];
            }
            $price_list.= '<tr><td><label><input type="radio" onchange="typeItem.check('.$c['id'].','.$p['id'].')" name="pid" value="'.$p['id'].'" '.$check.'/>'.$p['title'].$check_span.'</label></td>'
                . '<td class="newCost">'.ceil($p['price'] / $cur['value']).'<span class="reduction">'.$cur['reduction'].'</span></td>'
                . '<td class="oldCost"><span class="price">'.ceil($p['price-old'] / $cur['value']).'</span><span class="reduction">'.$cur['reduction'].'</span></td></tr>';
            if ($b!==false) {
                $cost = ceil(ceil($p['price']*(1+$b['center']/100))/$cur['value']);
                $price_list.= '<tr class="holy" id="'.$p['id'].'" '.(empty($p['first']) ? 'style="display:none;"':'').'>
                    <td style="color: #999; font-size: 12px; padding-left: 21px;">'.$b['title'].'</td>
                    <td style="color: #ff9510; font-size: 16px; font-weight: bold; font-family: Georgia;"><i>'.$cost.'</i><span class="reduction">'.$cur['reduction'].'</span></td>
                    <td></td></tr>';
            }
        }
        $price_list.= '</table>';
    } elseif (!empty($c['price']) && !empty($c['price-old'])) {
        $price_list = '<table style="margin:0;"><tr><td style="width:78px;"><b>����:</b></td>'
            .'<td align="right" style="width:80px;"><span class="oldCost"><span class="price">'.ceil($c['price-old'] / $cur['value']).'</span><span class="reduction">'.$cur['reduction'].'</span></span></td>'
            . '<td><span class="newCost"><span val="'.$c['price'].'" id="price-old fff">'.ceil($c['price'] / $cur['value']).'</span><span class="reduction">'.$cur['reduction'].'</span></span></td></tr>';
        if ($b!==false) {
            $cost = ceil(ceil($c['price']*(1+$b['center']/100))/$cur['value']);
            $price_list .= '<tr class="holy">
                <td style="color: #999; font-size: 12px; min-width:170px;" colspan="2">'.$b['title'].'</td>
                <td style="color: #ff9510; font-size: 16px; font-weight: bold; font-family: Georgia;"><i>'.$cost.'</i><span class="reduction">'.$cur['reduction'].'</span></td></tr>';
        }
        $price_list.= '</table>';
    } elseif(!empty($c['price'])) {
        $price_list = '<table style="margin:0;"><tr><td style="width:170px;"><b>����:</b></td>'
            . '<td><span val="'.$c['price'].'" id="price">'.ceil($c['price'] / $cur['value']).'</span><span class="reduction">'.$cur['reduction'].'</span></td></tr>';
        if ($b!==false) {
            $cost = ceil(ceil($c['price']*(1+$b['center']/100))/$cur['value']);
            $price_list .= '<tr class="holy">
                    <td style="color: #999; font-size: 12px; min-width:170px;" colspan="2">'.$b['title'].'</td>
                    <td style="color: #ff9510; font-size: 16px; font-weight: bold; font-family: Georgia;"><i>'.$cost.'</i><span class="reduction">'.$cur['reduction'].'</span></td></tr>';
        }
        $price_list.= '</table>';
    }

    if (!empty($structure)) {
        $html.= '<div class="rightInfo floatRight"><div class="title">������:</div><span id="composition"><table cellpadding="0" cellspacing="0" border="0" width="100%"><tbody>';
        foreach ($structure as $l) {
            $line = explode(':', $l);
            if (!empty($line[0]) && !empty($line[1])) {
                $html.= '<tr><td><b>'.$line[0].':</b></td><td>'.$line[1].'</td></tr>';
            } elseif (!empty($l)) {
                $html.= '<tr><td colspan="2">'.$l.'</td></tr>';
            }
        }
        $html.= '</tbody></table></span></div>';
    }
    $html.= '<div class="clear"></div><div class="kolvo">'.$price_list.stripslashes($c['text']).'</div></div><div class="clear"></div></div>';
    $html.= '<div class="formButton"><input type="submit" value="�������� � �������" /></div></form>';

    jQuery('div#quickOrderBlock')->html(inUTF8($html));
    jQuery::evalScript('fast.load()');
    jQuery::getResponse();
}

function fastOrder() {
    $_SESSION['fast_bay_id'] = null;
    global $cur;
    include_once('include/jquery/jQuery.php');

    if (empty($_GET['id']) || empty($_GET['sid'])) jQuery::getResponse();

    $id = abs(intval($_GET['id']));
    $sid = abs(intval($_GET['sid']));

    $c = catItem_url($id);
    if (empty($c)) {
        jQuery('div#quickOrderBlock')->html(inUTF8('<span class="close">&nbsp;</span><span class="h2_alt">������� �����</span><p>����� �� �������</p>'));
        jQuery::evalScript('fast.load()');
        jQuery::getResponse();
    }

    $br = strstr($c['structure'], '<br />') ? '<br />' : "\n";
    $structure = !empty($c['structure']) ? explode($br, $c['structure']) : array();

    $html = '<span class="close">&nbsp;</span><span class="h2_alt">������� �����</span><form action="/orders_fast/op=order&cid='.$id.'&sid='.$sid.'" onsubmit="formSubmit(this); return false;" class="formOrder">';
    $html.= '<div class="product">';
    if (!empty($c['image'])) {
        $images = array();
        $image = explode(':', $c['image']);
        $icons = '<div class="item-stickers">'
                . ((!empty($c['hit']))?'<div class="catalog_item_hit"></div>':'')
                . ((!empty($c['season']))?'<div class="catalog_item_bouquet"></div>':'')
                . ((!empty($c['share']))?'<div class="catalog_item_share centered_v_hover_popup shared_items text_block" tb="TYPE_hover_popup--append--MULTIPLE_shared_items" tb_cont="share_text_block"></div>':'')
                . '</div>';
        $html.= '<div class="leftProd floatLeft">'
                . '<div class="img"><div class="freshness"></div>'
                . '<div class="pinit"><a  href="//www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" /></a></div>';
        foreach ($image as $i=>$img){
            $style = ($i==0)?'':' style="display:none;" ';
            $html .= '<a '.$style.' id="fast-list-images_'.$i.'" class="fast-list-images_all" href="/files/catalog/'.$c['id'].'/w600_'.$image[$i].'" rel="fast-product">'.$icons.'<img src="/files/catalog/'.$c['id'].'/w199_'.$image[$i].'" /></a>';
            $images[] = '"'.$img.'"';
        }
        $html.= '</div>';
        if (!empty($c['share'])){
            $share = query_new('SELECT `text` FROM `sw_text_blocks` WHERE `active`=1 AND `id_name`='.quote('share_text_block'),1);
            $html .= !empty($share['text'])?'<div class="item_cart_share">'.$share['text'].'</div>' : '';
        }
        if (count($image) > 1) {
            $html.= '<div id="fast-list-images"></div>';
            jQuery::evalScript('fast.pach="/files/catalog/'.$c['id'].'/"');
            jQuery::evalScript('fast.item=['.implode(',',$images).']');
            jQuery::evalScript('fast.carousel(0,\'fast-list-images\')');
        }
        $html.= '</div>';
    }
    $html.= '<div class="rightProd floatRight"><span class="h1_alt">'.$c['title'].'</span>'
            . '<div class="mix-info"><div style="width: 100%; padding-right:5px;"><div class="leftInfo">';
    if (!empty($c['size'])) $html.= '<div ><b>������:</b> <span id="fast-item-size">'.$c['size'].'</span></div>';
    if (!empty($c['art'])) $html.= '<div><b>�������:</b> '.$c['art'].'</div>';
    $html.= '<div class="infoCol"><b>���-��:</b> <span class="tableCol">'
            . '<a href="javascript:item.minus(\'fast-count-item\',1);" class="minus"></a>'
            . '<input type="text" id="fast-count-item" value="1" name="count">'
            . '<a href="javascript:item.plus(\'fast-count-item\');" class="plus"></a></span><div class="clear"></div></div>';

    $curs = array('euro'=>'ico1', 'usd'=>'ico2', 'rub'=>'ico3');
    $html.= '<div class="infoMoney"><b>������:</b><span>';
    foreach ($curs as $k=>$v) $html.= '<a id="'.$k.'" href="javascript:void(0)" '.(isIphone() ? 'ontouchstart="cur.fast(this)" class="'.$v.($k != $_COOKIE['cur'] ? null : 'active').'"' : 'onclick="cur.fast(this)" class="'.$v.($k != $_COOKIE['cur'] ? null : 'active').'"').'></a>';
    $html.= '</span><div class="clear"></div></div>';

    if (!empty($c['colors'])) {
        $bColor = explode(':', $c['colors']);
        $colors = query_new('SELECT `id`, `title`, `class` FROM `sw_colors` WHERE `active` = 1 AND `id` IN ('.implode(',', $bColor).') ',0,'id');
        $html.= '<div class="infoColor"><b>����:</b><span id="fastOrder_colorList_'.$c['id'].'" class="colorChange">';
        foreach ($colors as $id=>$color) {
            $html.= '<a id="fo_c'.$id.'" href="javascript: fast.colorSet('.$c['id'].','.$id.')" class="'.$color['class'].'" title="'.htmlspecialchars($color['title'] , ENT_COMPAT | ENT_HTML401, 'cp1251').'">'
                    . '<input type="checkbox" style="display:none;" name="fast_color_'.$c['id'].'[]" value="'.$id.'"><span>&nbsp;</span>'
                    . (stripos($color['class'],'twoColors')!==false?'<span>&nbsp;</span>':'').'</a>';
        }
        $html.= '</span><div class="clear"></div></div>';
    }
    $b = query_new('SELECT `start`, `end`, `title`, `center` FROM `sw_holiday` WHERE `active` = "1"',1);
    if ($b!==false){
        $b['title'] = str_replace('|','<br>',$b['title']);
    }
    $html.= '</div></div><div>';

    $price = '';
    $r = db_query('SELECT `id`, `title`, `price`, `price-old`, `composition`, `first`, `size` FROM `sw_catalog_price` WHERE `cid` = '.quote($c['id']).' ORDER BY `price` ASC');
    if (mysql_num_rows($r) != 0) {
        $price = '<div class="title">����:</div><table width="100%" border="0" cellpadding="0" cellspacing="0">';
        while ($p = mysql_fetch_array($r, MYSQL_ASSOC)) {
            $check = (!empty($p['first'])) ? ' <span id="fast-chek" class="gray">(�� ����)</span>':'';
            $holl_style = (!empty($p['first'])) ? '' : ' style="display: none;"';
            if (!empty($p['first']) && !empty($p['composition'])) {
                $structure = explode('<br />', $p['composition']);
            }
            $struc_data = block_nofollow('<div id="fast_subprice_struc_'.$p['id'].'" data-size="'.(empty($p['size'])?'�':$p['size']).'">'.itemTypeStruct(explode('<br />', $p['composition'])).'</div>');
            $price.= '<tr><td><label><input type="radio" '.(isIphone() ? 'ontouchstart="fastprice.check('.$p['id'].')"' : 'onclick="fastprice.check('.$p['id'].')"').' name="pid" value="'.$p['id'].'" />'.$p['title'].$check.'</label>'.$struc_data.'</td>'
                    . '<td class="newCost"><span val="'.$p['price'].'" class="fast-order-price">'.ceil($p['price'] / $cur['value']).'</span><span class="reduction">'.$cur['reduction'].'</span></td>'
                    . '<td class="oldCost"><span val="'.$p['price-old'].'" class="price fast-order-price">'.ceil($p['price-old'] / $cur['value']).'</span><span class="reduction">'.$cur['reduction'].'</span></td></tr>';
            if ($b!==false) {
                $cost = ceil(ceil($c['price']*(1+$b['center']/100))/$cur['value']);
                $price.= '<tr class="holy-fast" id="holy-'.$p['id'].'" '.$holl_style.'>
                    <td style="color: #999; font-size: 12px; padding-left: 21px;">'.$b['title'].'</td>
                    <td style="color: #ff9510; font-size: 16px; font-weight: bold; font-family: Georgia;"><span val="'.$cost.'" class="fast-order-price">'.ceil($cost/$cur['value']).'</span><span class="reduction">'.$cur['reduction'].'</span></td>
                    <td></td>
                </tr>';
            }
        }
        $price.= '</table>';
    } elseif (!empty($c['price']) && !empty($c['price-old'])) {
        $price = '<table style="margin:0;"><tr><td style="width:78px;"><b>����:</b></td>'
                . '<td style="width:80px;" align="right" class="oldCost"><span class="price fast-order-price" val="'.$c['price-old'].'">'.ceil($c['price-old'] / $cur['value']).'</span><span class="reduction">'.$cur['reduction'].'</span></td>'
                . '<td class="newCost"><span val="'.$c['price'].'" class="fast-order-price">'.ceil($c['price'] / $cur['value']).'</span><span class="reduction">'.$cur['reduction'].'</span></td></tr>';
        if ($b!==false) {
            $cost = ceil(ceil($c['price']*(1+$b['center']/100))/$cur['value']);
                $price .= '<tr class="holy">
                        <td style="color: #999; font-size: 12px; min-width:170px;" colspan="2">'.$b['title'].'</td>
                        <td style="color: #ff9510; font-size: 16px; font-weight: bold; font-family: Georgia;"><span val="'.$cost.'" class="fast-order-price">'.ceil($cost/$cur['value']).'</span><span class="reduction">'.$cur['reduction'].'</span></td></tr>';
        }
        $price.= '</table>';
    } elseif(!empty($c['price'])) {
        $price = '<table style="margin:0;"><tr><td style="width:170px;"><b>����:</b></td>'
                . '<td><span val="'.$c['price'].'" class="fast-order-price">'.ceil($c['price'] / $cur['value']).'</span><span class="reduction">'.$cur['reduction'].'</span></td></tr>';
        if ($b!==false) {
            $cost = ceil(ceil($c['price']*(1+$b['center']/100))/$cur['value']);
            $price .= '<tr class="holy">'
                . '<td style="color: #999; font-size: 12px; min-width:170px;" colspan="2">'.$b['title'].'</td>'
                . '<td style="color: #ff9510; font-size: 16px; font-weight: bold; font-family: Georgia;">'
                    . '<span val="'.$cost.'" class="fast-order-price">'.ceil($cost/$cur['value']).'</span>'
                    . '<span class="reduction">'.$cur['reduction'].'</span></td></tr>';
        }
        $price.= '</table>';
    }

    if (!empty($structure)) {
        $style_tr = '';
        $struct_tbl = array();
        $html.= '<div class="rightInfo"><div class="title">������:</div><span id="fast-composition"><table cellpadding="0" cellspacing="0" border="0" width="100%"><tbody>';
        foreach ($structure as $struc_key=>$l) {
            $line = explode(':', $l);
            $struct_tbl[] = (!empty($line[0]) && !empty($line[1]) ? '<tr '.$style_tr.'><td><b>'.$line[0].':</b></td><td>'.$line[1].'</td></tr>' : '<tr '.$style_tr.'><td colspan="2"><b>'.$l.'</b></td></tr>');
            if (count($struct_tbl)==3 && $struc_key<count($structure)-1){
                $style_tr = ' style="display:none;" ';
                $struct_tbl[] = '<tr class="min_max_struct full_struct struct_show_hide"><td colspan="2"><span '.(isIphone() ? 'ontouchstart' : 'onclick').'="$(this).parent().parent().hide().nextAll(\'tr\').show();">������ ������</span></td></tr>';
                $last_tr = '<tr  '.$style_tr.' class="struct_show_hide" ><td colspan="2"><span onclick="$(this).parent().parent().parent().find(\'tr.min_max_struct\').show().nextAll(\'tr\').hide();">�������� ������</span></td></tr>';
            }
        }
        if (!empty($last_tr)) { $struct_tbl[] = $last_tr; }
        $html.= implode(PHP_EOL,$struct_tbl);
        $html.= '</tbody></table></span></div>';
    }
    $uname = (!empty($_SESSION['user']['username']))? $_SESSION['user']['username'] : '';
    $uphone = (!empty($_SESSION['user']['phone']))? $_SESSION['user']['phone'] : '';
    $html.= '</div></div>';
    $html.= '<div class="clear"></div><div class="kolvo">'.$price.stripslashes($c['text']).'</div></div><div class="clear"></div></div><hr />';
    $html.= '<p>������� ���������� ���������� � �� ��� ����������. <br />���� ��������� �������� 24 ���� � �����</p>';

    $html.= '<div id="oName" class="formMediumInputBlock floatLeft">���� ���*:<br /><input name="name" type="text" value="'.$uname.'"/></div>';
    $html.= '<div id="oPhone" class="formMediumInputBlock floatLeft">�������*:<br /><input name="phone" type="text" value="'.$uphone.'"/></div>';
    $html.= '<div class="clear"></div>'
            . '<div id="oText" class="formBigTextarea">�����������:<br /><textarea name="text" cols="1" rows="1" ></textarea><p>����, ���������� *, <b>�����������</b> � ����������.</p></div>'
            . '<div class="formButton"><input type="submit" value="��������" '.(isIphone() ? 'ontouchstart' : 'onclick').'="yaCounter10652362.reachGoal(\'zakaz\'); return true;" /></div></form>';

    jQuery('div#quickOrderBlock')->html(inUTF8($html));
    jQuery::evalScript('$("#fast-chek").prev("input").click()');
    jQuery::evalScript('fast.load()');
    jQuery::getResponse();
}

function dirItems($where = array(),$props=array()) {
    global $_GLOBALS, $module, $cur, $op;

    include_once('include/jquery/jQuery.php');

    if (empty($_SERVER['REQUEST_URI'])) {
        HeaderPage(_PAGE_ERROR404);
    }

    clearFilter();

    $_GLOBALS['sections'] = array();
    $bc_text = array();
    $db_flds = array('sc.*', 'scs_url.`url`');
    $db_joins = array();
    $cf_unsets = array();
    $pg = !empty($_REQUEST['pg']) ? $_REQUEST['pg'] : 1;
    $pp = !empty($_SESSION['per_page']) ? $_SESSION['per_page'] : 12;
    $cf = !empty($_SESSION['cf']) ? $_SESSION['cf'] : array();
    $ot = !empty($_SESSION['order_type']) ? $_SESSION['order_type'] : 1;
    $cfp = !empty($_SESSION['cfp']) ? $_SESSION['cfp'] : array();
    $utm = !isset($_REQUEST['utm_source']) && !isset($_REQUEST['gclid']) ? false : true;
    $sep = '<span class="text_color_orange"> <i class="fontico-angle-right"></i></span>';
    $url = filter(trim($_SERVER['REQUEST_URI']), 'nohtml');
    $url = stripos($url, '?', 0) !== false ? substr($url, 0, strrpos($url, '?')) : $url;
    $seo_url = $url;
    $cfu = false;
    $ajax = isset($_REQUEST['ajax_ret']) ? true : false;
    $get = stripos($url, '?', 0) === false ? false : true;

    if ((isset($_SESSION['last_pg_loaded'])) &&  (!$ajax) && ($_SESSION['last_catalog_loaded'] ==  strtok($_SERVER["REQUEST_URI"],'?')) ){
        $howmany = $_SESSION['last_pg_loaded'] - $pg; // ���������� ������� ��� �������� �� ������

        $scr = '
            <script language="JavaScript">
           jQuery(function(){
                for (var i = '.$pg.'; i <= '.$_SESSION['last_pg_loaded'].'; i++) {
                    if (i == 1) {
                    continue;
                    }else{
                        pageNav.loadEx(i);
                    }
                    
                } 
           });
          
            </script>
            ';
    }else{
        $scr = '';
    }

    $reqst = $_REQUEST;
    foreach ($reqst as $rkey=>$req){
        switch ($rkey){
            case 'cfp':
                $cfp = explode(':',$req);
                $cfp['url'] = parse_url($_SERVER['REQUEST_URI']);
                $cfp['url'] = $cfp['url']['path'];
                break;
            case 'order_type': $ot=intval($req);break;
            case 'per_page': $pp = intval($req);break;
            default : unset($reqst[$rkey]);
        }
    }
    if (isset($_REQUEST['set_fltrs'])){
        $cf = array();
        if (isset($_REQUEST['cf'])){
            $cf = is_array($_REQUEST['cf'])? $_REQUEST['cf']:explode(':',$_REQUEST['cf']);
            foreach ($cf as $nfkey=>$nfvar){
                $nfvar = trim((string)$nfvar);
                if (!empty($nfvar)) { $cf[$nfkey] = intval($nfvar);}
                else { unset($cf[$nfkey]);}
            }

            $cf['url'] = parse_url($_SERVER['REQUEST_URI']);
            $cf['url'] = $cf['url']['path'];
        }
        $reqst[] = true;
    }
    if (!empty($_REQUEST['unset_fltr'])){
        $req = $_REQUEST['unset_fltr'];
        $cfp = ($req == 'all' || $req == 'price')? array():$cfp;
        $cf = $req == 'all' ? $cfp : $cf;
        if (array_search($req, $cf) !== false) {
            unset($cf[array_search($req, $cf)]);
        }
        $reqst[] = true;
    }

    $_SESSION['per_page'] = $pp;
    $_SESSION['cfp'] = $cfp;
    $_SESSION['cf'] = $cf;
    $_SESSION['order_type'] = $ot;

    $cfp = !empty($_REQUEST['cfp_preview']) ? explode(':', $_REQUEST['cfp_preview']) : $cfp;
    switch ($op){
        case 'dir':
            if (substr($url, -1) != '/' && empty($_REQUEST['pg'])) {
                $url .= '/';
                if (!empty($ajax) || !empty($get) || !empty($_POST)) {
                    $reqst[] = true;
                } else {
                    HeaderPage($url,301);
                }
            }
            $url = substr($url, 0, strripos($url, '/') + 1);
            if (stripos($url,'/catalog/bouquet/',0)!==false){
                $cfu = true;
            }
            break;
        default :
            if (!empty($props)){
                $nav_dir = (isset($props['nav_dir']))? $props['nav_dir']:$nav_dir;
                $bc_text = (isset($props['bc_text']))? $props['bc_text']:$bc_text;
                $cfu = (isset($props['cfu']))? $props['cfu']:$cfu;
            }
    }

    if (!empty($reqst)){
        jQuery::evalScript('location.href = "' . inUTF8($url) . '";');
        jQuery::getResponse();
    }
    if ($ajax===false && $get && empty($_REQUEST['cfp_preview']) && empty($utm)){
        HeaderPage($url);
    }

    if ($cfu){
        $_GLOBALS['cf_url'] = $url;
        $seo_dir = query_new('SELECT * FROM `sw_seo_section` WHERE `pach` = '.quote($url).' AND `active` = 1 ORDER BY `pos`',1);
        $filter_dirs = query_new('SELECT sws.*, ss.`title` as "sid_title" FROM `sw_section` ss LEFT JOIN `sw_section` sws ON sws.`sid`=ss.`id` WHERE ss.`active`=1 AND sws.`active`=1 AND ss.`filter`=1 ORDER BY ss.`pos`,sws.`title`',0,'sid',1);
    }
    if ($op == 'dir'){
        $dir = query_new('SELECT * FROM `sw_section` WHERE '.(empty($seo_dir)? '`pach` = '.quote($url) : '`id` = '.$seo_dir['sectid']).' AND `active` = 1',1);
        if (empty($dir)){
            if ($ajax){
                jQuery::evalScript(isset($_REQUEST['cfp_preview'])?'filterPrice.noResults()':'location.href = "'.inUTF8($url).'";');
                jQuery::getResponse();
            }
            HeaderPage(_PAGE_ERROR404);
        }
        else { $where[] = empty($where)? ' AND scs.`sid`='.$dir['id'].' ':'';}

        $_GLOBALS['section'] = $dir['link'];
        $_GLOBALS['cfp_path'] = $dir['pach'];
    }

    if (!empty($filter_dirs)){
        $seo_dir_cf = !empty($seo_dir['filters'])?explode(':',$seo_dir['filters']):array();
        foreach ($filter_dirs as $fd_key=>$f_dir){
            $fc_sql = array();
            foreach ($f_dir as $fs_key=>$f_sect){
                if (in_array($f_sect['id'], $cf) || in_array($f_sect['id'], $seo_dir_cf)) {
                    $filter_dirs[$fd_key][$fs_key]['fltr_checked'] = in_array($f_sect['id'], $cf);
                    if (!empty($filter_dirs[$fd_key][$fs_key]['fltr_checked']) && empty($_REQUEST['cfp_preview'])){
                        $cf_unsets[] = '<div class="filter_item"><a href="javascript: catalog.unset_fltr('.$f_sect['id'].');"><span class="text">'.$f_sect['title'].'</span><span class="btn"></span></a></div>';
                    }
                    $fc_sql[] = intval($f_sect['id']);
                }
            }
            if (!empty($fc_sql)){
                $where[] = 'AND scs'.$fd_key.'.`sid` IN ('.implode(',',$fc_sql).')';
                $db_joins[] = 'LEFT JOIN sw_catalog_section scs'.$fd_key.' ON sc.`id`=scs'.$fd_key.'.`cid`';
            }
        }
        $_GLOBALS['cf_list'] = $filter_dirs;
    }

    if (!empty($cfp) && intval($cfp[0])<intval($cfp[1])){
        $db_joins[] = 'LEFT JOIN sw_catalog_price scp ON scp.`cid`=sc.`id`';
        $db_flds[] = 'scp.`price` AS `min_sub_price`';
        $db_flds[] = 'scp.`price-old` AS `min_sub_price_old`';
        $where[] = 'AND (IFNULL(scp.`price`,sc.`price`) BETWEEN '.  intval($cfp[0]).' AND '.  intval($cfp[1]).')';
        $_GLOBALS['filter_price_set'] = $cfp;
    }
    $where[] = 'GROUP BY sc.`id`';

    $count = catalog_items($db_flds, $where, true, $db_joins);
    $count = (empty($count['count']))?0:intval($count['count']);

    if (!empty($_REQUEST['cfp_preview'])){
        empty($count)? jQuery::evalScript('filterPrice.noResults()') : jQuery::evalScript('filterPrice.haveResults()');
        jQuery('#fppi_results')->html($count);
        jQuery::getResponse();
    }

    $_GLOBALS['cfp_count'] = $count;

    if ($op=='dir'){
        $nav_dir = $dir;

        if (!empty($seo_dir)){
            $_GLOBALS['sections'][] = $seo_dir['link'];
            $bc_text[] = array('title'=>$seo_dir['title'],'class'=>'tcol-orange','url'=>$seo_dir['pach']);
        }else{
            $seo_links = query_new('SELECT sss.`title`,sss.`h1_title`,sss.`pach`,sss.`filters`,ss.`sid` FROM sw_seo_section sss LEFT JOIN sw_section ss ON sss.`filters`=ss.`id` WHERE sss.`active`=1  AND ss.`sid` IS NOT NULL AND sss.`sectid`='.$nav_dir['id'].' ORDER BY ss.`sid`,sss.`pos`',0,'sid',1);
            if (!empty($seo_links)){
                $seo_html = '<div class="seo_links blockWithTopLine">';
                foreach ($seo_links as $key => $sl_sid){
                    $filterTitleQuery = query_new('SELECT title FROM sw_section WHERE id='.$key.'');
                    $fiflterTtile = $filterTitleQuery[0]['title'] ? $filterTitleQuery[0]['title'] : '�������' ;
                    $seo_html .='<div><span>'.$fiflterTtile.':</span>';
                    foreach ($sl_sid as $sl_val){
                        $seo_html .= '<a href="'.$sl_val['pach'].'">'.$sl_val['title'].'</a>';
                    }
                    $seo_html .='</div>';
                }
                $seo_html .= '</div>';
            }
        }
        while (!empty($dir)){
            $_GLOBALS['sections'][] = $dir['link'];
            $bc_text[] = array(
                'title'=>$dir['title'],
                'class'=>(empty($bc_text)?'tcol-orange':''),
                'url'=>$dir['pach'],
                'fltr'=>(!empty($cfu) && $dir['id']==55?$filter_dirs:array()));
            $dir = query_new('SELECT * FROM sw_section WHERE `active` = 1 AND `id` = '.$dir['sid'],1);
        }
        $bc_text[] = array('title'=>'�������','class'=>'','url'=>'/');

        $_GLOBALS['sections'] = array_reverse($_GLOBALS['sections']);
        $_GLOBALS['menu'] = $_GLOBALS['sections'][0];
    }
    $bc_text = bcNav($bc_text, false, $sep);

    if (($ot == 3 || $ot == 4) && !empty($cfp) && intval($cfp[0])<intval($cfp[1])){
        $route = $ot == 3 ? 'ASC' : 'DESC';
        $where[] = 'ORDER BY min_sub_price '.$route;
    } else {
        $where[] = 'ORDER BY sc.'.catalog_vars::$order_type_names[$ot][1];
    }

    $where[] = 'LIMIT '.($pp * ($pg-1)).','.$pp;

    $order_list = '';
    foreach (catalog_vars::$order_type_sequence as $key){
        $order_list .= '<div class="text_cont'.($ot==$key?' selected ':'').'"><a href="javascript: catalog.set_otype('.$key.')">'.catalog_vars::$order_type_names[$key][0].'</a></div>';
    }

    $limit_list = '';
    foreach (catalog_vars::$per_page as $lim_num){
        $limit_list .= '<div class="text_cont'.($lim_num==$pp ? ' selected ' : '').'"><a href="javascript: catalog.set_ppage('.$lim_num.')">�� '.$lim_num.'</a></div>';
    }

    $html = '<div class="setable_filters"><table><tr><td>'
        . '<div class="breadcrumbs">'.$bc_text.'</div></td><td align="right" style="vertical-align:top;">'
        . '<div class="order_and_limits">'
            . '<div class="order_view popup_shower">'
                . '<div class="text_cont selected"><span>'.catalog_vars::$order_type_names[$ot][0].'</span></div>'
                . '<div id="order_select" class="fw_popup">'. $order_list. '</div>'
            . '</div>'
            . '<div class="limits_view">'
                . '<span style="color: #000; padding-right:5px;">����������:</span>'
                . '<div class="popup_shower">'
                    . '<div class="text_cont selected"><span>�� '.$pp.'</span></div>'
                    . '<div id="limit_select" class="fw_popup">'. $limit_list. '</div>'
                . '</div>'
            . '</div>'
        . '</div>'
    . '</td></tr></table><div class="tit_count_items"><b>�������:</b> '.strItems_new($count).'</div></div>';

    $html .= empty($seo_html)?'':$seo_html;

    if (!empty($cf) || !empty($cfp)){
        $html .= '<div class="seton_filters blockWithTopLine"><div class="tit_filters"><span>������� ������:</span></div>'
                . '<div class="floatRight basket_close_btn"><span class="fontico-cancel-circled-3"><a href="javascript: catalog.unset_fltr(\'all\');">������� ���</a></span></div>';
        $html .= !empty($cfp)? '<div class="filter_item"><a href="javascript: catalog.unset_fltr(\'price\');"><span class="text">�� '.$cfp[0].$cur['reduction'].' �� '.$cfp[1].$cur['reduction'].'</span><span class="btn"></span></a></div>':'';
        $html .= !empty($cf_unsets)?implode(PHP_EOL, $cf_unsets):'';
        $html .= '</div>';
    }
    $db_flds[] = 'scs_url.`sid`';
    $items = catalog_items($db_flds, $where, false, $db_joins);
    if (!empty($items)) {
        $html.= '<div class="catalog blockWithTopLine">';
        foreach ($items as $ikey=>$item) { $items[$ikey] = dirItem($item);}
        $items[] = '<div class="clear"></div>';
        if ($count > $pp && ($pg * $pp)<$count){
            $ppText = ($count - ($pg * $pp))>$pp ? $pp : $count - ($pg * $pp);
            // ��� ������� ���������
               // file_put_contents('mlog.txt', "LP:" .$_SESSION['last_pg_loaded'] . " AJA:" .$ajax . " SESCAT:" . $_SESSION['last_catalog_loaded'] . " RURI:" . strtok($_SERVER["REQUEST_URI"],'?') . PHP_EOL   , FILE_APPEND);


            $items[] = $scr.'<div class="load-more"><span class="green-grd" id="loader" '.(isIphone() ? 'ontouchstart' : 'onclick').'="pageNav.loadMore(this,'.($pg + 1).')"><span>��������� ��� '.$ppText.'</span><i class="fontico-spin1 animate-spin"></i></span></div>';
        }
        if ($_REQUEST['ajax_ret']=='load-more'){
            // ��������� ������ ���������, �������� ��������� ������������ ���������
            if (!isset($_REQUEST['dontsetpage'])) {
                $_SESSION['last_pg_loaded'] = $pg;
                $_SESSION['last_catalog_loaded'] =  strtok($_SERVER["REQUEST_URI"],'?');
            }

            jQuery('div.load-more')->replaceWith(inUTF8(implode(PHP_EOL, $items)));
            jQuery('div.pages_nav li > a[href$="pg'.$pg.'.html"]')->replaceWith(inUTF8('<span>'.$pg.'</span>'));
            jQuery::getResponse();
        }else{
            unset($_SESSION['last_pg_loaded']);
        }

        $html .= implode(PHP_EOL, $items);
    } else {


        $html .= '<div class="catalog blockWithTopLine zero_found">'.PHP_EOL
                . '<div class="msg">'.PHP_EOL
                    . '<p><span class="etention">!</span></p>'.PHP_EOL
                    . '<p class="titl">��������������� ������� �� �������</p>'.PHP_EOL
                    . '<p class="text">�������, ��������������� �������� ���������, �� �������.<br>'.PHP_EOL
                    . '���������� ������ ������� ������ ���<br>'.PHP_EOL
                    . '<a href="javascript: catalog.unset_fltr(\'all\');">�������� ������� ������</a>.</p>'.PHP_EOL
                . '</div>'.PHP_EOL;
    }
    $html.= '</div>';
    $first = $url;
    $end = true;
    if ($count > $pp ){
        if (!empty($props)){
            $url = isset($props['url'])?$props['url']:$url;
            $first = isset($props['first'])?$props['first']:$first;
            $end = isset($props['end'])?$props['end']:$end;
        }
        $html.= htmlNavigator($count, $pg, $pp, $url, $first, $end);
    }

    $seo_page = query_new('SELECT `text`, `html_title`, `keywords`, `description` FROM `sw_seo` WHERE `active` = 1 AND `title` = '.quote(filter(trim($seo_url), 'nohtml')).' LIMIT 1', 1);

    if (isset($_COOKIE['ban-dest-id'])){
        $bann_id = intval($_COOKIE['ban-dest-id']);
        $baner = query_new('SELECT * FROM `sw_main_baner` WHERE `active`=1 AND `id` = '.quote($bann_id).' LIMIT 1',1);
        unset($_COOKIE['ban-dest-id']);
        setcookie('ban-dest-id', null, -1, '/');
    }else{
        $baner = query_new('SELECT * FROM `sw_main_baner` WHERE `active`=1 AND `url` REGEXP '.quote($url.'$').' LIMIT 1',1);
    }


    if (!empty($baner) && stripos($baner['banner'],':')!==false){
        $imgs = explode(':',$baner['banner']);
        if (file_exists('files/main_baner/'.$baner['id'].'/'.$imgs[1])){
            $html = '<div id="baner_slider" class="ramka"><img nopin="nopin" src="/files/main_baner/'.$baner['id'].'/'.$imgs[1].'" width="100%" height="auto" /></div>'.$html;
        }
    }    

    $title_levels = '';
    foreach (array_reverse($_GLOBALS['bc_levels']) as $l) {
        if ($l !== '�������' && $l !== '') {
            $title_levels .= $l.' / ';
        }        
    }

    if (empty($seo_page) && $_SERVER['REQUEST_URI'] == $seo_dir['pach']) {
        $seo_page = $seo_dir;
    }    

    //$data = PageData($module,true);

    if ($pg > 1) {
        $html_title = substr($title_levels, 0, -2).' (�������� �������� �'.$pg.')';
    } else {
        $html_title = (empty($seo_page['html_title'])?substr($title_levels, 0, -2):$seo_page['html_title']);
    }

    
    $_GLOBALS['title'] = empty($seo_page['h1_title'])?$nav_dir['title']:$seo_page['h1_title'];
    $_GLOBALS['text'] = $html.(empty($seo_page['text'])?'':'<div class="mainTextInner blockWithTopLine">'.stripslashes($seo_page['text']).'</div>');
    $_GLOBALS['html_title'] = $html_title.' / �������� �Studio Floristic�';
    $_GLOBALS['description'] = empty($seo_page['description'])?'':$seo_page['description'];
    $_GLOBALS['template'] = 'default';

}
function bcNav($bc_arr=array(),$ret=false,$sep=''){
    global $_GLOBALS;

    if (!empty($bc_arr) && is_array($bc_arr)){
        $html = array();
        $set_sep = '';
        foreach($bc_arr as $bckey=>$bcval){
            $dis = '<div class="bc_cont" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">';
            $link = (($bckey > 0) ? '<a itemprop="url" href="'.$_GLOBALS['abs_url_host'].$bcval['url'].'" class="'.$bcval['class'].'">':'').'<span itemprop="title">'.$bcval['title'].'</span>'.(($bckey > 0) ? '</a>':'').(empty($bcval['fltr'])?'':'<i class="fontico-menu-2 bc_fltr"></i>');
            $dis .= $link.$set_sep.(empty($bcval['fltr'])?'':block_nofollow(cf_html($bcval['fltr'],$link))).'</div>';
            if (empty($ret)){ $html[] = $dis;}
            else { $bc_arr[$bckey] = $dis;}
            $set_sep = $sep;
            $_GLOBALS['bc_levels'][] = $bcval['title'];
        }
        if (empty($ret)){ $bc_arr = implode(PHP_EOL, array_reverse($html));}
        return $bc_arr;
    }
    return false;
}

function catBreadcrumbs($cat_arr=array(),$filter_tabs=true,$item=array(),$sep=''){
    global $_GLOBALS;

    $parent = $cat_arr['parent'];
    unset($cat_arr['parent']);
    $name = $parent['title'];
    $url = $parent['pach'];
    $parent_id = $item['cat_id'];
    $filter_cat_list = '';
    $del = (count($cat_arr)>4)?2:1;
    $arr = array_chunk($cat_arr, ceil(count($cat_arr)/$del));
    $border_class = array(
        0 => (count($arr)>1?'border-right':''),
        1 => 'border-left'
    );

    foreach ($arr as $ul_key=>$ul){
        $filter_cat_list .= '<ul class="'.$border_class[$ul_key].'">';
        foreach ($ul as $row){
            $filter_catalogs = isset($_COOKIE['filter_catalog']) ? explode('_', $_COOKIE['filter_catalog']) : array();
            $children_class = 'children_'.$row['title'];
            $checked = (in_array($row['id'],$filter_catalogs))? ' checked ':'';
            $filter_cat_list .= '<li id="li_'.$row['id'].'" class="'.$children_class.$checked.'"><div>'
                    . '<input id="check_'.$row['id'].'" type="checkbox" '.$checked.' value="'.$row['id'].'" '.(isIphone() ? 'ontouchstart' : 'onclick').'="breadcrumbs.checkItem('.$row['id'].',true)">'
                    . '<a href="'.$row['pach'].'" class="'.$children_class.'">'.$row['title'].'</a>'
                    . '<span class="uncheck_filter" '.(isIphone() ? 'ontouchstart' : 'onclick').'="breadcrumbs.checkItem('.$row['id'].',false)"></span></div>'
                    . '</li>';
        }
        $filter_cat_list .='</ul>';
    }
    $mover = 'onmouseover="breadcrumbs.showPopup('.$parent['id'].',this)"';
    if (intval($parent['id'])<1 || $filter_tabs===false){
        $filter_cat_list = '';
        $mover = '';
        $name = (!isset($item['title']))?$name:$item['title'];
    }
    if ($filter_cat_list!=''){
        $filter_cat_list = '<div class="bc_popup_cont" id="parent_'.$parent['id'].'">'
                . '<div class="bc_body">'
                    . '<div class="bc_head"><a class="popup_link '.$item['class'].'" href="'.$url.'">'.$name.'</a></div>'
                    . '<div class="ul_cont">'.$filter_cat_list.'</div>'
                    . '<div class="bc_submit_btn"><span '.(isIphone() ? 'ontouchstart' : 'onclick').'="breadcrumbs.submitFilters('.$parent_id.')" class="butn_green">���������</span></div>'
                . '</div>'
            . '</div>';
    }
    $bc = (!isset($item['title']))
            ? '<a itemprop="url" class="simple '.$item['class'].'" href="'.$_GLOBALS['abs_url_host'].$url.'" ><span itemprop="title" >'.$name.'</span></a>'
            :'<a itemprop="url" href="'.$_GLOBALS['abs_url_host'].$item['url'].'" class="text_color_orange" ><span itemprop="title" >'.$name.'</span></a>';
    $html = '<div class="bc_cont" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" '.$mover.'>'.$bc.$sep.$filter_cat_list.'</div>';// itemprop="item"

    return $html;
}

function createItem() {
    global $_GLOBALS,$cur;

    $colors = query_new('SELECT `id`, `title`, `class` FROM `sw_colors` WHERE `active` = 1',0,'id');
    if (!empty($_POST)) {
        include_once('include/jquery/jQuery.php');

        $item = array();
        $where = '';
        foreach ($_POST as $key=>$val){
            if (stripos($key,'item_count_')!==false && intval($val)>0){
                $id = intval(str_replace('item_count_', '', $key));
                $where .= ($where!='')? ','.$id:$id;
                $item[$id]=array('count'=>$val);
                if (!empty($_POST['colors_id_'.$id])) {
                    foreach ($_POST['colors_id_'.$id] as $cur_coll){
                        $item[$id]['color'][] = $colors[$cur_coll];
                        $item[$id]['color_titles'][] = $colors[$cur_coll]['title'];
                    }
                }else{
                    jQuery::evalScript('designer.error(3)');
                    jQuery::getResponse();
                }
            }
        }
        if ($where!=''){
            $where = ' AND id IN ('.$where.')';
        }
        $r = query_new('SELECT `id`, `title`, `price` FROM `sw_designer` WHERE `active` = 1 '.$where.' ORDER BY `pos`',0,'id');
        if (count($item) == 0 || count($item)!=count($r)) {
            jQuery::evalScript('designer.error(2)');
            jQuery::getResponse();
        }
        if (empty($_POST['execution'])) {
            jQuery::evalScript('designer.error(1)');
            jQuery::getResponse();
        }
        $executionType = array(1=>'�������� �����', 2=>'����������� ����������', 3=>'���������� ��������', 4=>'��������� ��������');
        $execution = abs(intval($_POST['execution']));
        $structure = null;
        $itog = 0;
        $data = '<p>�����:</p>';
        $data.= '<ol>';
        foreach ($item as $id=>$value) {
            $summ = $r[$id]['price'] * $value['count'];
            $data.= '<li>'.$r[$id]['title'].(!empty($value['color']) ? ' (����: '.implode(', ',$value['color_titles']).')' : null).': '.$r[$id]['price'].' x '.$value['count'].' / '.$summ.'<span clas="reduction">�.</span></li>';
            if (!empty($structure)) $structure.= '<br />';
            $structure.= $r[$id]['title'].(!empty($value['color']) ? ' ('.implode(', ',$value['color_titles']).')' : null).': '.$value['count'].'��.';
            $itog+= $summ;
        }
        $data.= '</ol>';
        $itog+= $_GLOBALS['v_execution-'.$execution];
        $data.= '<p>������� ����������: '.$executionType[$execution].'</p>';
        $structure.= '<br />������� ����������: '.$executionType[$execution];

        db_query('INSERT INTO `sw_basket` (`session`, `type`, `title`, `text`, `structure`, `price`, `amount`) VALUES ('.quote(session_id()).', '.quote('my-bouquet').', '.quote('��� ��������� �����').', '.quote($data).', '.quote($structure).', '.quote($itog).', '.quote($itog).')');

        $html = mini_cart();
        jQuery('div.howMuchItem')->html(inUTF8($html));
        $cart_link = '<a href="'.(empty($_GLOBALS['in_cart'])?'javascript: void(0);" style="cursor:default;text-decoration:none;"':'/basket.html"').'>���� �������</a>';
        jQuery('div.cart')->html(inUTF8($cart_link));
        jQuery::evalScript('miniCart.hoverRefresh();');
        jQuery::evalScript('designer.result(1);');
        jQuery::getResponse();
    }

    $html = '<form id="create-bouquet" action="/catalog/op=create" onsubmit="formSubmit(this); return false;">';
    $html.= '
        <script>
var colors = {
    set: function (id, cid) {
        if (jQuery(\'span#color-list-\' + id + \' a#c\' + cid).hasClass(\'active\')) {
            jQuery(\'span#color-list-\' + id + \' a#c\' + cid).removeClass(\'active\');
            jQuery(\'span#color-list-\' + id + \' a#c\' + cid).children(\'input\').prop(\'checked\', false);
            
        } else {
            jQuery(\'span#color-list-\' + id + \' a#c\' + cid).addClass(\'active\');
            jQuery(\'span#color-list-\' + id + \' a#c\' + cid).children(\'input\').prop(\'checked\', true);
            
        }
    }
};
        </script>';
    $r = query_new('SELECT `id`, `title`, `price`, `colors`, `image` FROM `sw_designer` WHERE `active` = 1 ORDER BY `pos`');
    foreach ($r as $d) {
        $html.= '<div id="item-'.$d['id'].'" class="createBlock"><input type="hidden" id="create_item_'.$d['id'].'" name="item_count_'.$d['id'].'" value="0"><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>';
        $html.= '<td width="132" rowspan="4"><img src="'.(!empty($d['image']) ? '/files/designer/'.$d['id'].'/w132_'.$d['image'] : '/i/132x105.jpg').'" width="132" height="105" /></td>';
        $html.= '<td class="tableText" colspan="3"><div class="name">'.$d['title'].'</div></td></tr>';
        $html.= '<tr><td class="tableText" rowspan="3">';

        $bColor = !empty($d['colors']) ? explode(':', $d['colors']) : array();
        if (!empty($bColor)) {
            $clr = null;
            foreach ($bColor as $id) {
                if (isset($colors[$id])) {
                        $clr.= '<a id="c'.$id.'" href="#" '.(isIphone() ? 'ontouchstart' : 'onclick').'="colors.set('.$d['id'].','.$id.'); return false;" class="'.$colors[$id]['class'].'" title="'.htmlspecialchars($colors[$id]['title'] , ENT_COMPAT | ENT_HTML401, 'cp1251').'"><span>&nbsp;</span>'
                    . '<input type="checkbox" style="display:none;" name="colors_id_'.$d['id'].'[]" value="'.$id.'">'.(stripos($colors[$id]['class'],'twoColors')!==false?'<span>&nbsp;</span>':'').'</a>';
                }
            }
            $html.= '����:<br /><div class="colorChange"><span id="color-list-'.$d['id'].'">'.$clr.'</span></div>';
        }
        $html.= '</td><td width="100">���� �� 1��:</td><td width="100" class="tableCost"><span id="item_price_'.$d['id'].'">'.ceil($d['price']/$cur['value']).'</span><span class="reduction">'.$cur['reduction'].'</span></td></tr>';
        $html.= '<tr><td>���-��:</td><td class="tableCol"><a href="javascript:designer.minus('.$d['id'].')" class="minus"></a><span class="count" id="item_count_'.$d['id'].'">0</span><a href="javascript:designer.plus('.$d['id'].')" class="plus"></a></td></tr>';
        $html.= '<tr><td>�����:</td><td class="tableCost"><span class="summa" id="item_summa_'.$d['id'].'">0</span><span class="reduction">'.$cur['reduction'].'</span></td></tr></table></div>';
    }
    //TODO: �������� ������ ������
    /*
    $html.= '<div class="changeVariant blockWithTopLine"><span class="h2_alt">�������� ������� ���������� ������</span><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>';
    $html.= '<td><label><img src="/i/variant1.jpg" width="130" height="154" /><b><input type="radio" price="'.ceil($_GLOBALS['v_execution-1']/$cur['value']).'" name="execution" value="1" onclick="designer.execution(this)" /><span>�������� �����</span></b></label></td>';
    $html.= '<td><label><img src="/i/variant2.jpg" width="140" height="154" /><b><input type="radio" price="'.ceil($_GLOBALS['v_execution-2']/$cur['value']).'" name="execution" value="2" onclick="designer.execution(this)" /><span>����������� ����������</span></b></label></td>';
    $html.= '<td><label><img src="/i/variant3.jpg" width="130" height="154" /><b><input type="radio" price="'.ceil($_GLOBALS['v_execution-3']/$cur['value']).'" name="execution" value="3" onclick="designer.execution(this)" /><span>���������� ��������</span></b></label></td>';
    $html.= '<td><label><img src="/i/variant4.jpg" width="130" height="154" /><b><input type="radio" price="'.ceil($_GLOBALS['v_execution-4']/$cur['value']).'" name="execution" value="4" onclick="designer.execution(this)" /><span>��������� ��������</span></b></label></td>';
    $html.= '</tr><tr><td class="tableCost"><span>'.ceil($_GLOBALS['v_execution-1']/$cur['value']).'</span><span class="reduction">'.$cur['reduction'].'</span></td>'
            . '<td class="tableCost"><span>'.ceil($_GLOBALS['v_execution-2']/$cur['value']).'</span><span class="reduction">'.$cur['reduction'].'</span></td>'
            . '<td class="tableCost"><span>'.ceil($_GLOBALS['v_execution-3']/$cur['value']).'</span><span class="reduction">'.$cur['reduction'].'</span></td>'
            . '<td class="tableCost"><span>'.ceil($_GLOBALS['v_execution-4']/$cur['value']).'</span><span class="reduction">'.$cur['reduction'].'</span></td></tr>';
    */

    $html.= '<div class="changeVariant blockWithTopLine"><span class="h2_alt">�������� ������� ���������� ������</span><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>';
    $html.= '<td><label><img src="/i/variant1.jpg" /><b><input type="radio" price="'.ceil($_GLOBALS['v_execution-1']/$cur['value']).'" name="execution" value="1" '.(isIphone() ? 'ontouchstart' : 'onclick').'="designer.execution(this)" /><span>�������� �����</span></b><span class="preprice"><span>'.ceil($_GLOBALS['v_execution-1']/$cur['value']).'</span><span class="reduction">'.$cur['reduction'].'</span></span></label></td>';
    $html.= '<td><label><img src="/i/variant2.jpg" /><b><input type="radio" price="'.ceil($_GLOBALS['v_execution-2']/$cur['value']).'" name="execution" value="2" '.(isIphone() ? 'ontouchstart' : 'onclick').'="designer.execution(this)" /><span>����������� ����������</span></b><span class="preprice"><span>'.ceil($_GLOBALS['v_execution-2']/$cur['value']).'</span><span class="reduction">'.$cur['reduction'].'</span></span></label></td>';
    $html.= '<td><label><img src="/i/variant3.jpg" /><b><input type="radio" price="'.ceil($_GLOBALS['v_execution-3']/$cur['value']).'" name="execution" value="3" '.(isIphone() ? 'ontouchstart' : 'onclick').'="designer.execution(this)" /><span>���������� ��������</span></b><span class="preprice"><span>'.ceil($_GLOBALS['v_execution-3']/$cur['value']).'</span><span class="reduction">'.$cur['reduction'].'</span></span></label></td>';
    $html.= '<td><label><img src="/i/variant4.jpg" /><b><input type="radio" price="'.ceil($_GLOBALS['v_execution-4']/$cur['value']).'" name="execution" value="4" '.(isIphone() ? 'ontouchstart' : 'onclick').'="designer.execution(this)" /><span>��������� ��������</span></b><span class="preprice"><span>'.ceil($_GLOBALS['v_execution-4']/$cur['value']).'</span><span class="reduction">'.$cur['reduction'].'</span></span></label></td>';
    $html.= '</tr>';

    $html.= '</table></div><div class="bigBlockWithButtonOk"><div class="bigBlocktext" style="padding-left: 15px;">���� ������ � ���������:</div><div class="bigBlockCost"><span id="create_itog">0</span><span class="reduction">'.$cur['reduction'].'</span></div><a class="bigButton" href="javascript: designer.order()">��������</a></div></form>';

    $p = query_new('SELECT `title`, `text`, `html_title`, `description`, `keywords` FROM `sw_pages` WHERE `link` = '.quote('create').' AND `active` = 1',1);
    if ($p===false) HeaderPage(_PAGE_ERROR404);
    if (!empty($p['text'])) $html.= '<div class="mainTextInner">'.stripslashes($p['text']).'</div>';

    $_GLOBALS['title'] = $p['title'];
    $_GLOBALS['text'] = $html;
    $_GLOBALS['html_title'] = !empty($p['html_title']) ? $p['html_title'] : $p['title'].' - '.$_GLOBALS['v_sitename'];
    if (!empty($p['description'])) $_GLOBALS['description'] = $p['description'];
    if (!empty($p['keywords'])) $_GLOBALS['keywords'] = $p['keywords'];
    $_GLOBALS['template'] = 'default';
}

function filterItems() {
    $where = array(' AND scs.`url` REGEXP "/catalog/bouquet/"');
    $props = array(
        'cfu'=>true,
        'nav_dir'=>array('title'=>'������� �������'),
        'bc_text'=>array(
            array('title'=>'������� �������','class'=>'tcol-orange','url'=>'/catalog/filter.html'),
            array('title'=>'�������','class'=>'','url'=>'/')),
        'url'=>'/catalog/',
        'first'=>'/catalog/filter.html'
    );
    dirItems($where,$props);
}
function bestsellers() {
    global $_GLOBALS;
    $where = array(' AND sc.`hit`=1 ');
    $props = array(
        'cfu'=>true,
        'nav_dir'=>array('title'=>'���� ������'),
        'bc_text'=>array(
            array('title'=>'���� ������','class'=>'tcol-orange','url'=>'/catalog/op=viewed'),
            array('title'=>'�������','class'=>'','url'=>'/')),
        'url'=>'/catalog/op=bestsellers',
        'first'=>'/catalog/op=bestsellers',
        'end'=>false
    );
    $_GLOBALS['cfp_path'] = '/catalog/op=bestsellers';
    dirItems($where,$props);
}
function season() {
    global $_GLOBALS;
    $where = array(' AND sc.`season` = 1');
    $props = array(
        'cfu'=>true,
        'nav_dir'=>array('title'=>'����� ������'),
        'bc_text'=>array(
            array('title'=>'����� ������','class'=>'tcol-orange','url'=>'/catalog/op=season'),
            array('title'=>'�������','class'=>'','url'=>'/')),
        'url'=>'/catalog/op=season',
        'first'=>'/catalog/op=season',
        'end'=>false
    );
    $_GLOBALS['cfp_path'] = '/catalog/op=season';
    dirItems($where,$props);
}
function viewedItems(){
    global $_GLOBALS;
    $where = array(' AND sc.`id` IN('.implode(',',$_SESSION['items']).') ');
    $props = array(
//      'cfu'=>true,
        'nav_dir'=>array('title'=>'������������� ������'),
        'bc_text'=>array(
            array('title'=>'������������� ������','class'=>'tcol-orange','url'=>'/catalog/op=viewed'),
            array('title'=>'�������','class'=>'','url'=>'/')),
        'url'=>'/catalog/op=viewed',
        'first'=>'/catalog/op=viewed',
        'end'=>false
    );
    $_GLOBALS['cfp_path'] = '/catalog/op=viewed';
    dirItems($where,$props);
}
function viewItem() {
    global $cur, $_GLOBALS;

    if (empty($_SERVER['REQUEST_URI'])) { HeaderPage(_PAGE_ERROR404);}
    $sep = '<span class="text_color_orange"> <i class="fontico-angle-right"></i></span>';
    $curs = array('euro'=>'ico1', 'usd'=>'ico2', 'rub'=>'ico3');
    $bc_text = array();
    $price_list = '';

    $url = filter(trim($_SERVER['REQUEST_URI']), 'nohtml');
    $url = (stripos($url,'?',0)!==false)? substr($url, 0,stripos($url,'?',0)):$url;

    $c = catItem_url($url);
    if (empty($c)) { HeaderPage(_PAGE_ERROR404);}
    if ($c['url'] !== $url)
    {
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: " . $c['url']);
        exit();
    }

    $_GLOBALS['product'] = $c['url'];

    if (empty($c['first'])){ $_GLOBALS['head_inc'][] = '<link rel="canonical" href="http'.(isset($_SERVER['HTTPS'])?'s':'').'://'.$_SERVER['HTTP_HOST'].$c['url'].'" />';}

    $bc_text[] = array('title'=>$c['title'],'class'=>'tcol-orange','url'=>$c['url']);
    $dir = query_new('SELECT * FROM sw_section WHERE `active` = 1 AND `id` = '.$c['sid'].' LIMIT 1',1);

    while (!empty($dir)){
        $_GLOBALS['sections'][] = $dir['link'];
        $bc_text[] = array(
            'title'=>$dir['title'],
            'class'=>(empty($bc_text)?'tcol-orange':''),
            'url'=>$dir['pach']);
        $dir = query_new('SELECT * FROM sw_section WHERE `active` = 1 AND `id` = '.$dir['sid'].' LIMIT 1',1);
    }
    $bc_text[] = array('title'=>'�������','class'=>'','url'=>'/');
    $bc_text = bcNav($bc_text, false, $sep);

    $_GLOBALS['sections'] = array_reverse($_GLOBALS['sections']);
    $_GLOBALS['menu'] = $_GLOBALS['sections'][0];
    $_GLOBALS['section'] = $c['ss_link'];
//  $_GLOBALS['sections'] = array($c['link']);

    $b = query_new('SELECT `start`, `end`, `title`, `center` FROM `sw_holiday` WHERE `active` = "1"',1);
    if ($b!==false){ $b['title'] = str_replace('|','<br>',$b['title']);}
    $structure = !empty($c['structure']) ? explode("<br>", $c['structure']) : array();

    $html = '<div class="breadcrumbs item" >'.PHP_EOL.$bc_text.PHP_EOL.'</div>'.PHP_EOL
            . '<div class="product" itemscope itemtype="http://schema.org/Product">'.PHP_EOL;
    if (!empty($c['image'])) {
        $images = array();
        $image = explode(':', $c['image']);
        $icons = '<div class="item-stickers">'
                . ((!empty($c['hit']))?'<div class="catalog_item_hit"></div>':'')
                . ((!empty($c['season']))?'<div class="catalog_item_bouquet"></div>':'')
                . ((!empty($c['share']))?'<div class="catalog_item_share centered_v_hover_popup shared_items text_block" tb="TYPE_hover_popup--append--MULTIPLE_shared_items" tb_cont="share_text_block"></div>':'')
                . '</div>';
        $html.= '<div class="leftProd floatLeft">'
                . '<div class="img"><div class="freshness text_block" id="freshness_text" tb="TYPE_to_body--onclick--TIT_������ ������ �����"></div>'
                . '<div class="pinit"><a  href="//www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" /></a></div>';
        foreach ($image as $i=>$img){
            $style = ($i==0)?'style="pointer-events: none;"':' style="display:none;" ';
            $html .= '<a '.$style.' id="list-images_'.$i.'" class="list-images_all" itemprop="image" href="/files/catalog/'.$c['id'].'/w600_'.$image[$i].'" rel="product_page">'.$icons.'<img src="/files/catalog/'.$c['id'].'/w199_'.$image[$i].'" /></a>';
            $images[] = '"'.$img.'"';
        }
        $html.= '</div>';
        if (!empty($c['share'])){
            $share = query_new('SELECT `text` FROM `sw_text_blocks` WHERE `active`=1 AND `id_name`='.quote('share_text_block'),1);
            $html .= !empty($share['text'])?'<div class="item_cart_share">'.$share['text'].'</div>' : '';
        }


        if (count($image) > 1) {
            $html.= '<div id="list-images"></div>';
            $_GLOBALS['js_onready'][] = 'imgcar.pach = "/files/catalog/'.$c['id'].'/"';
            $_GLOBALS['js_onready'][] = 'imgcar.item=['.implode(',',$images).']';
            $_GLOBALS['js_onready'][] = 'imgcar.view(0,\'list-images\')';
        }
        $html.= '<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script><div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="none" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir" style="text-align: center"></div>'
                    . '<div style="text-align: center; padding-bottom:10px;"><noindex><a href="http://www.imobzor.ru/control-purchase/pokupka-buketa-cvetov-siyalo-solnce-v-internet-magazine-studiofloristic-ru" rel="nofollow" target="_blank"><img src="/i/zakup.jpg" /></a></noindex></div>';
        $html.='<div class="tovar_delivery_date"><span class="text block_text">��������� ���� ��������:</span>'
                . '<span class="text text_weight_bold text_color_orange" id="tovar_del_date">�������, '.date('d.m.Y').'</span></div>';
        //$html.='<div class="tovar_delivery_calc"><span '.(isIphone() ? 'ontouchstart' : 'onclick').'="deliveryCalc.init(\'delcalc\')">������� ����� ��������?</span></div>';
        // $html.='<div class="tovar_to_compare"><span>�������� �</span> <span class="text_color_green link">���������</span></div>';
        $html.= '</div>';
    }
    $html.= '<div class="rightProd floatRight">';
    $html.= '<h1 itemprop="name">'.$c['title'].'</h1>';

    $r = query_new('SELECT `id`, `title`, `price`, `price-old`, `size`, `composition`, `first` FROM `sw_catalog_price` WHERE `cid` = '.quote($c['id']).' ORDER BY `price` ASC');
    if (!empty($r)) {
        $price_list = '<div class="title">����:</div><table width="100%" border="0" cellpadding="0" cellspacing="0" itemprop="offers" itemscope itemtype="http://schema.org/AggregateOffer">';
        foreach ($r as $p_key=>$p) {
            $check = !empty($p['first']) ? ' checked="checked" ':'';
            $check_span = !empty($p['first']) ? ' <span id="chek" class="gray">(�� ����)</span>':'';
            if (!empty($p['composition']) && !empty($p['first'])){
                $structure = explode('<br />', $p['composition']);
            }
            if (!empty($p['size']) && !empty($p['first'])){
                $c['size'] = $p['size'];
            }
            if ($p_key==0){
                $prop_price = ' itemprop="lowPrice" content="'.ceil($p['price'] / $cur['value']).'" ';
            }elseif ($p_key==count($r)-1){
                $prop_price = ' itemprop="highPrice" content="'.ceil($p['price'] / $cur['value']).'" ';
            }elseif((!empty($p['first']) && $p_key!=0 && $p_key==count($r)-1) || count($r)==1){
                $prop_price = ' itemprop="price" content="'.ceil($p['price'] / $cur['value']).'" ';
            }else{
                $prop_price = '';
            }
            $struc_data = block_nofollow('<div id="subprice_struc_'.$p['id'].'" data-size="'.(empty($p['size'])?'�':$p['size']).'">'.itemTypeStruct(explode('<br />', $p['composition'])).'</div>');
            $price_list.= '<tr><td><label><input type="radio" onchange="typeItem.check('.$p['id'].')" name="pid" value="'.$p['id'].'" '.$check.'/>'.$p['title'].$check_span.'</label>'. $struc_data.'</td>'
                . '<td class="newCost" '.$prop_price.'>'.ceil($p['price'] / $cur['value']).'<span class="reduction" >'.$cur['reduction'].'</span></td>'
                . '<td class="oldCost"><span class="price">'.ceil($p['price-old'] / $cur['value']).'</span><span class="reduction">'.$cur['reduction'].'</span></td></tr>';
                
            if ($b!==false) {
                $cost = ceil(ceil($p['price']*(1+$b['center']/100))/$cur['value']);
                $price_list.= '<tr class="holy" id="'.$p['id'].'" '.(empty($p['first']) ? 'style="display:none;"':'').'>
                    <td style="color: #999; font-size: 12px; padding-left: 21px;">'.$b['title'].'</td>
                    <td style="color: #ff9510; font-size: 16px; font-weight: bold; font-family: Georgia;"><i>'.$cost.'</i><span class="reduction">'.$cur['reduction'].'</span></td>
                    <td></td></tr>';
            }
        }
        $_GLOBALS['js_onready'][] = '$("#chek").prev("input").trigger("click")'; 
        $price_list.= '<tr><td><meta itemprop="priceCurrency" content="'.$cur['ISO'].'" ><meta itemprop="offerCount" content="'.count($r).'"></td></tr></table>';
    } elseif (!empty($c['price']) && !empty($c['price-old'])) {
        $price_list = '<table style="margin:0;"><tr><td style="width:78px;"><b>����:</b></td>'
            .'<td align="right" style="width:80px;"><span class="oldCost"><span class="price">'.ceil($c['price-old'] / $cur['value']).'</span><span class="reduction">'.$cur['reduction'].'</span></span></td>'
            . '<td><span class="newCost"><span val="'.$c['price'].'" id="price-old fff">'.ceil($c['price'] / $cur['value']).'</span><span class="reduction">'.$cur['reduction'].'</span></span></td></tr>';
        if ($b!==false) {
            $cost = ceil(ceil($c['price']*(1+$b['center']/100))/$cur['value']);
            $price_list .= '<tr class="holy">
                <td style="color: #999; font-size: 12px; min-width:170px;" colspan="2">'.$b['title'].'</td>
                <td style="color: #ff9510; font-size: 16px; font-weight: bold; font-family: Georgia;"><i>'.$cost.'</i><span class="reduction">'.$cur['reduction'].'</span></td></tr>';
        }
        $price_list.= '</table>';
    } elseif(!empty($c['price'])) {
        $price_list = '<table style="margin:0;"><tr><td style="width:170px;"><b>����:</b></td>'
            . '<td><span val="'.$c['price'].'" id="price">'.ceil($c['price'] / $cur['value']).'</span><span class="reduction">'.$cur['reduction'].'</span></td></tr>';
        if ($b!==false) {
            $cost = ceil(ceil($c['price']*(1+$b['center']/100))/$cur['value']);
            $price_list .= '<tr class="holy">
                    <td style="color: #999; font-size: 12px; min-width:170px;" colspan="2">'.$b['title'].'</td>
                    <td style="color: #ff9510; font-size: 16px; font-weight: bold; font-family: Georgia;"><i>'.$cost.'</i><span class="reduction">'.$cur['reduction'].'</span></td></tr>';
        }
        $price_list.= '</table>';
    }

    $html.= '<div class="mix-info"><div style="width: 100%; padding-right:5px;"><div class="leftInfo">';
    $html.= !empty($c['size']) ? '<div id="size-data"><b>������:</b> <span id="item-size">'.$c['size'].'</span></div>' : '';
    $html.= !empty($c['art']) ? '<div><b>�������:</b> '.$c['art'].'</div>' : '';
    $html.= '<div class="infoCol"><b>���-��:</b> <span class="tableCol">'
            . '<a class="minus" href="javascript:item.minus(\'item-count-'.$c['id'].'\','.$c['min'].')"></a>'
            . '<input type="text" id="item-count-'.$c['id'].'" value="'.$c['min'].'">'
            . '<a class="plus" href="javascript:item.plus(\'item-count-'.$c['id'].'\')"></a></span><div class="clear"></div></div>';
    $html.= '<div class="infoMoney"><b>������:</b><span>';
    foreach ($curs as $k=>$v) {
        $html.= '<a href="javascript:cur.'.$k.'()" class="'.$v.($k != $_COOKIE['cur'] ? null : 'active').'"></a>';
    }
    $html.= '</span><div class="clear"></div></div>';
    if (!empty($c['colors'])) {
        $bColor = explode(':', $c['colors']);
        $colors = query_new('SELECT `id`, `title`, `class` FROM `sw_colors` WHERE `active` = 1 AND `id` IN ('.implode(',', $bColor).') ',0,'id');

        $html.= '<div class="infoColor"><b>����:</b><span id="color-list-'.$c['id'].'" class="colorChange">';
        foreach ($colors as $id=>$color) {
            $html.= '<a id="c'.$id.'" href="#" '.(isIphone() ? 'ontouchstart="colors.set('.$c['id'].','.$id.'); return false;"' : 'onclick="colors.set('.$c['id'].','.$id.'); return false;"').' class="'.$color['class'].'" title="'.htmlspecialchars($color['title'] , ENT_COMPAT | ENT_HTML401, 'cp1251').'">'
                    . '<input type="checkbox" style="display:none;" name="colors_id_'.$c['id'].'[]" value="'.$id.'"><span>&nbsp;</span>'
                    . (stripos($color['class'],'twoColors')!==false?'<span>&nbsp;</span>':'').'</a>';
        }
        $html.= '</span><div class="clear"></div></div>';
        $color = true;
    }
    $html.= '</div></div><div>';
    if (!empty($structure)) {
        $html.= '<div class="rightInfo"><div class="title">������:</div><span id="composition">'.PHP_EOL. itemTypeStruct($structure).PHP_EOL.'</span></div>';
    }
    $html.= '</div></div>';
    $html.= '<div class="mix-info" style="margin: 15px 0 5px;"><div style="width: 100%; padding-right:5px;"><div class="leftInfo">'
                .'<div class="catItem_freeBlock">'
                    . '<span class="title">���������:</span>'
                    . '<div><span class="text_block popup_link" id="catItem_freeBlock_delivery" tb="TYPE_hover_popup--after">��������</span></div>'
                    . '<div><span class="text_block popup_link" id="bouquet_long_life_1part" tb="TYPE_to_body--onclick--BTN_more--more_BTN_CLASS_btn_hide butn_green--BTNSHOW_more->bouquet_long_life_2part">���������� �� �����</span></div>'
                    . '<div><span class="text_block popup_link" id="catItem_freeBlock_podkormka" tb="TYPE_hover_popup--after">��������� ��� ������</span></div>'
                . '</div></div></div>'
            . '<div style="vertical-align: middle;"><div style="width: 160px;"></div></div></div>';
    if (isIphone())
    {
        $html.= '<div class="clear"></div><div class="kolvo">'.$price_list.'<p align="center">
             <a href="#" ontouchstart="yaCounter10652362.reachGoal(\'ya7send\'); fast.init('.$c['id'].','.$c['sid'].'); return false;" class="zakazButton quickOrder"><span>������� �����</span></a>&nbsp;&nbsp;<a href="#" ontouchstart="javascript:yaCounter10652362.reachGoal(\'ya7send\'); basket.insert('.$c['id'].','.$c['sid'].',0); return false;" class="zakazButton">� �������</a></p>'
            . '<div class="opisanie" itemprop="description">'.stripslashes($c['text']).'</div></div></div><div class="clear"></div></div>';
    }
    else
    {
        $html.= '<div class="clear"></div><div class="kolvo">'.$price_list.'<p align="center">
             <a href="javascript:yaCounter10652362.reachGoal(\'ya7send\'); fast.init('.$c['id'].','.$c['sid'].')" class="zakazButton quickOrder"><span>������� �����</span></a>&nbsp;&nbsp;<a href="javascript:yaCounter10652362.reachGoal(\'ya7send\'); basket.insert('.$c['id'].','.$c['sid'].',0)" class="zakazButton">� �������</a></p>'
            . '<div class="opisanie" itemprop="description">'.stripslashes($c['text']).'</div></div></div><div class="clear"></div></div>';
    }

    $instPhotos = instagram_media(query_new('SELECT * FROM sw_instagram_photos WHERE `cid`='.$c['id'].' AND `active`=1 ORDER BY `id` DESC'));
    if (!empty($instPhotos)){
        $html.='<div class="someSlideBlock blockWithTopLine instBlock" style="display:none;"><div id="instagram_carusel" class="jcarousel-skin-tango"><ul>'.$instPhotos['html'].'</ul></div></div>';
        $_GLOBALS['js_onload'][] = '$(".instBlock").slideDown();';
        $_GLOBALS['js_onload'][] = '$("#instagram_carusel").jcarousel({wrap:null,scroll:1,animation:"slow"});';
    }
    $html.= '<div id="inst_photos"></div>';
    $countItems = 0;
    if (empty($_SESSION['items'])) {
        $_SESSION['items'] = array();
        $_SESSION['items'][] = $c['id'];
    }else{
        $countItems = count($_SESSION['items']);
        if (!in_array($c['id'], $_SESSION['items'])) {
            $_SESSION['items'][] = $c['id'];
        }
    }
    if ($countItems > 1) {
        $r = query_new('SELECT c.`id`, c.`title`, c.`text`, c.`price`, c.`image`, s.`url` FROM `sw_catalog` as c LEFT JOIN `sw_catalog_section` as s ON s.`cid` = c.`id` AND s.`first` = 1 WHERE c.`active` = 1 AND c.`id` != '.quote($c['id']).' AND c.`id` IN ('.implode(',', $_SESSION['items']).') LIMIT 16');
        if (count($r) > 0 && $r!==false) {
            $html .= '<div class="someSlideBlock blockWithTopLine">';
            $html.= '<span class="h2_alt">�� ��� �� ��������</span><div class=" jcarousel-skin-tango"><div class="jcarousel-container jcarousel-container-horizontal" style="position: relative; display: block;">'
                    . '<div class="jcarousel-clip jcarousel-clip-horizontal" style="position: relative;">'
                    . '<ul class="mycarousel jcarousel-skin-tango" style="height:116px;left:0;margin:0;overflow: hidden;padding: 0;position: relative;top: 0;">';
            foreach ($r as $s) {
                $image = explode(':', $s['image']);
                if (!empty($s['text'])) {
                    $text = substr(strip_tags($s['text']), 0, 60);
                    $text = htmlspecialchars(substr($text, 0, strrpos($text, ' ')).'...', ENT_COMPAT | ENT_HTML401, 'cp1251');
                }
                $html.= '<li style="float: left;list-style: outside none none;" class="jcarousel-item jcarousel-item-horizontal jcarousel-item-1 jcarousel-item-1-horizontal">'
                        . '<a href="'.$s['url'].'" class="haveTooltip" title=\'<span class="h2_alt">'.htmlspecialchars($s['title'], ENT_COMPAT | ENT_HTML401, 'cp1251').'</span>'.(!empty($text)?$text:'').'\'>'
                        . '<img nopin="nopin" src="/files/catalog/'.$s['id'].'/w128_'.$image[0].'" /><span>'.ceil($s['price'] / $cur['value']).$cur['reduction'].'</span></a></li>';
            }
            $html.= '</ul></div><div class="jcarousel-prev jcarousel-prev-horizontal" style="display: block;"></div>'
                    . '<div class="jcarousel-next jcarousel-next-horizontal" style="display: block;"></div>'
                    . '</div></div><div class="viewed_btn"><a href="/catalog/op=viewed" class="butn_green">��� ������������� ������</a></div></div>';
        }
    }

    $frst = ($c['price']>=500) ? $c['price'] - 500 : 0;
    $second = $c['price'] + 500;
    $where = 'c.`price` >= '.$frst.' AND c.`price` < '.$second.'';
    $r = db_query('SELECT c.`id`, c.`price`, c.`title`, c.`text`, c.`image`, s.`url` FROM `sw_catalog` as c LEFT JOIN `sw_catalog_section` as s ON s.`cid` = c.`id` AND s.`first` = 1 WHERE s.`sid` = '.quote($c['sid']).' AND c.`active` = 1 AND c.`id` != '.quote($c['id']).' AND '.$where.' ORDER BY RAND() LIMIT 10');
    if (mysql_num_rows($r) != 0) {
            $html .= '<div class="someSlideBlock blockWithTopLine">';
            $html.= '<span class="h2_alt">������� ������</span><div class=" jcarousel-skin-tango"><div class="jcarousel-container jcarousel-container-horizontal" style="position: relative; display: block;">'
                    . '<div class="jcarousel-clip jcarousel-clip-horizontal" style="position: relative;">'
                    . '<ul class="mycarousel jcarousel-skin-tango" style="height:116px;left:0;margin:0;overflow: hidden;padding: 0;position: relative;top: 0;">';
            while ($s = mysql_fetch_array($r, MYSQL_ASSOC)) {
                $image = explode(':', $s['image']);
                if (!empty($s['text'])) {
                    $text = substr(strip_tags($s['text']), 0, 100);
                    $text = htmlspecialchars(substr($text, 0, strrpos($text, ' ')).'...', ENT_COMPAT | ENT_HTML401, 'cp1251');
                } else $text = null;
                $html.= '<li style="float: left;list-style: outside none none;" class="jcarousel-item jcarousel-item-horizontal jcarousel-item-1 jcarousel-item-1-horizontal">'
                        . '<a href="'.$s['url'].'" class="haveTooltip" title=\'<span class="h2_alt">'.htmlspecialchars($s['title'], ENT_COMPAT | ENT_HTML401, 'cp1251').'</span>'.$text.'\'>'
                        . '<img nopin="nopin" src="/files/catalog/'.$s['id'].'/w128_'.$image[0].'" /><span>'.ceil($s['price'] / $cur['value']).$cur['reduction'].'</span></a></li>';
            }
            $html.= '</ul></div><div class="jcarousel-prev jcarousel-prev-horizontal" style="display: block;"></div>'
                    . '<div class="jcarousel-next jcarousel-next-horizontal" style="display: block;"></div></div></div></div>';
    }

    $html.= (file_exists('system/templates/ratingBlock.tpl.html')?file_get_contents('system/templates/ratingBlock.tpl.html'):'');

    $r = query_new('SELECT * FROM `sw_catalog_reviews` WHERE `cid` = '.quote($c['id']).' AND `active` = 1 ORDER BY `id` DESC');
    $html.= '<div class="item-reviews-list'.($r!==false && count($r)>0 ? null : ' hidden').' blockWithTopLine" style="padding-top:10px;"><span class="h2_alt">������:</span>';
    foreach ($r as $key=>$rev) {
                $stars = '<div class="reviews_stars">';
                for ($i=0;$i<5;$i++){
                    $class = ($rev['stars']>$i)?'active_star':'';
                    $stars .= '<span class="'.$class.'"></span>';
                }
                $stars .= '</div>';
                $topLine = ($key>0)?'blockWithTopLine':'';
        $html.= '<div class="otzivsBlock '.$topLine.'" id="review_'.$rev['id'].'"><span class="date">'.$rev['date'].'</span>'.$stars.'<i>'.$rev['text'].'<b class="name">'.$rev['name'].'</b></i>'.(!empty($rev['response']) ? '<p>- '.$rev['response'].'</p>' : null).'</div>';
    }
    $html.= '</div><div class="blockWithTopLine" style="text-align:center; padding:15px 0;"><span class="butn_green review_add" '.(isIphone() ? 'ontouchstart' : 'onclick').'="reviews.showForm('.$c['id'].')">�������� ���� �����</span></div>';
    $_GLOBALS['js_onready'][] = 'reviews.initForm();';
    // SEO asked shit
    /*
    if (stripos($url,'/catalog/bouquet/',0)===0){
        $c['html_title'] = '������ ����� '.$c['title'];
        $c['description'] = "������ ����� ".$c['title']." &#x2740;";
        $structure = !empty($c['structure']) ? explode("\n", $c['structure']) : array();
        if (!empty($structure)){
            $struc = implode(', ',array_slice($structure, 0,3));
            $c['html_title'] .= ' ('.$struc.')';
            $struc = strlen($struc) > 55 ? implode(', ', array_slice($structure,0,2)).' � ��.' : $struc;
            $c['description'] .= ' ('.$struc.')';
        }
        $c['html_title'] .= ' � ��������� �� ��� - StudioFloristic.ru';
        $c['description'] .= ' �������� &#x2713; ������� �������� �� ������ &#9742; 8-800-333-12-91';
    }
    */
    // end SEO asked shit
    // ������� ����� SEO ������ ��� ������
    $seoData = getSeoForItem($c['id'], true);

    $_GLOBALS['text'] = $html;
//  $_GLOBALS['html_title'] = !empty($c['html_title']) ? $c['html_title'] : $c['title'].' - '.$_GLOBALS['v_sitename'];
//  $_GLOBALS['html_title'] = !empty($c['html_title']) ? $c['html_title'] : $seoData->title;
    $_GLOBALS['html_title'] =  $seoData->title;

//  if (!empty($c['description'])) $_GLOBALS['description'] = $c['description'];
    /*
    if (!empty($c['description'])){
        $_GLOBALS['description'] = $c['description'];
    } else {
        $_GLOBALS['description'] = $seoData->description;
    }
    */
    $_GLOBALS['description'] = $seoData->description;
//  $_GLOBALS['keywords'] = !empty($c['keywords']) ? $c['keywords'] : $c['title'];
//  $_GLOBALS['keywords'] = !empty($c['keywords']) ? $c['keywords'] : $seoData->keywords;
    $_GLOBALS['keywords'] =  $seoData->keywords;
    $_GLOBALS['template'] = 'catalog';
}

function initCatalog() {
    HeaderPage(_PAGE_ERROR404);
    // global $module, $_GLOBALS;

    // $data = PageData($module);
    // $_GLOBALS['title'] =  $data['title'];
    // $_GLOBALS['text'] =  stripslashes($data['text']);
    // $_GLOBALS['html_title'] = strlen(trim($data['html_title'])) != 0 ? $data['html_title'] : $_GLOBALS['v_title'];
    // $_GLOBALS['description'] = isset($data['description']) && strlen($data['description']) != 0 ? $data['description'] : $_GLOBALS['v_description'];
    // $_GLOBALS['keywords'] = isset($data['keywords']) && strlen($data['keywords']) != 0 ? $data['keywords'] : $_GLOBALS['v_keywords'];
}

function instagram_media($data=array()){
    if (empty($data)){
        return false;
    }
    foreach ($data as $photo){
        $carusel_arr[] = '<li><iframe class="inst-frame" src="'.$photo['link'].'embed/" width="261" height="315" frameborder="0" scrolling="no" allowtransparency="true"></iframe></li>';
    }
    return array('count'=>count($carusel_arr),'html'=>  implode('', $carusel_arr));
}

function itemTypeStruct($structure){
    $html = '';
    if ((strpos($structure[0], "\n") !== false) ){
        $structure = explode("\n", $structure[0]);
    }

    if (!empty($structure)) {
        $style_tr = '';
        $struct_tbl = array();
        foreach ($structure as $struc_key=>$l) {
            $line = explode(':', $l);
            $struct_tbl[] = (!empty($line[0]) && !empty($line[1]) ? '<tr '.$style_tr.'><td><b>'.$line[0].':</b></td><td>'.$line[1].'</td></tr>' : '<tr '.$style_tr.'><td colspan="2"><b>'.$l.'</b></td></tr>');
            if (count($struct_tbl)==3 && $struc_key<count($structure)-1){
                $style_tr = ' style="display:none;" ';
                $struct_tbl[] = '<tr class="min_max_struct full_struct struct_show_hide"><td colspan="2"><span '.(isIphone() ? 'ontouchstart' : 'onclick').'="$(this).parent().parent().hide().nextAll(\'tr\').show();">������ ������</span></td></tr>';
                $last_tr = '<tr '.$style_tr.' class="struct_show_hide" ><td colspan="2"><span '.(isIphone() ? 'ontouchstart' : 'onclick').'="$(this).parent().parent().parent().find(\'tr.min_max_struct\').show().nextAll(\'tr\').hide();">�������� ������</span></td></tr>';
            }
        }
        if (!empty($last_tr)) { $struct_tbl[] = $last_tr; }
        $html = '<table cellpadding="0" cellspacing="0" border="0" width="100%"><tbody>'.PHP_EOL.implode(PHP_EOL,$struct_tbl).PHP_EOL.'</tbody></table>';
    }
    return $html;
}

function clearFilter()
{
    $url = parse_url($_SERVER['REQUEST_URI']);
    $url = $url['path'];

    $sessionUrl = !empty($_SESSION['cf']['url']) ? $_SESSION['cf']['url'] : '';
    $sessionCfpUrl = !empty($_SESSION['cfp']['url']) ? $_SESSION['cfp']['url'] : '';

    $parserUrl = explode('/', $url);
    if (strpos($parserUrl[count($parserUrl) - 1], '.html') !== false) {
        unset($parserUrl[count($parserUrl) - 1]);
        $url = implode('/', $parserUrl).'/';
    }

    $posUrl = strpos($url, $sessionUrl);
    $posCfpUrl = strpos($url, $sessionCfpUrl);

    if ($sessionUrl !== $url) {
        $_SESSION['cf'] = '';
    }

    if ($sessionCfpUrl !== $url) {
        $_SESSION['cfp'] = '';
    }
}