<?php
if (!defined('_SWS_ADMIN_CORE')) exit();
	
    $_GLOBALS['create_dir'] = true;
    $_GLOBALS['add'] = true;
    $_GLOBALS['search'] = false;
    $_GLOBALS['on-page'] = 20;
    $_GLOBALS['tiny'] = false;
    $_GLOBALS['date'] = true;

    $_GLOBALS['field']['title']['id'] = 'ID';
    $_GLOBALS['field']['title']['active'] = '���./����.';
    $_GLOBALS['field']['title']['main'] = '�������� ���';
    $_GLOBALS['field']['title']['title'] = '��������';
    $_GLOBALS['field']['title']['background'] = '���';
    $_GLOBALS['field']['title']['pos'] = '�������';
    $_GLOBALS['field']['title']['date_start'] = '������ ������ ����';
    $_GLOBALS['field']['title']['date_end'] = '����� ������ ����';

    $_GLOBALS['field']['type']['sid'] = 'sid';
    $_GLOBALS['field']['type']['active'] = 'checkbox';
    $_GLOBALS['field']['type']['main'] = 'checkbox';
    $_GLOBALS['field']['type']['title'] = 'text';
    $_GLOBALS['field']['type']['pos'] = 'position';
    $_GLOBALS['field']['type']['background'] = 'file';
    $_GLOBALS['field']['type']['date_start'] = 'date';
    $_GLOBALS['field']['type']['date_end'] = 'date';

    $_GLOBALS['field']['db']['sid'] = "int(11) NOT NULL default '0'";
    $_GLOBALS['field']['db']['active'] = "int(1) NOT NULL default '0'";
    $_GLOBALS['field']['db']['main'] = "int(1) NOT NULL default '0'";
    $_GLOBALS['field']['db']['title'] = "varchar(255) default NULL";
    $_GLOBALS['field']['db']['pos'] = "int(11) NOT NULL default '0'";
    $_GLOBALS['field']['db']['background'] = "text";
    $_GLOBALS['field']['db']['date_start'] = 'date';
    $_GLOBALS['field']['db']['date_end'] = 'date';
    $_GLOBALS['field']['db']['insert'] = "timestamp NOT NULL default CURRENT_TIMESTAMP";
    $_GLOBALS['field']['db']['system'] = "int(1) NOT NULL default '0'";

    check_tbl(array($module=>$_GLOBALS['field']['db']));
    
    $_GLOBALS['field']['table']['id'] = 'text';
    $_GLOBALS['field']['table']['pos'] = 'pos';
    $_GLOBALS['field']['table']['title'] = 'title';
    $_GLOBALS['field']['table']['main'] = 'radio';
    $_GLOBALS['field']['table']['date_start'] = 'text';
    $_GLOBALS['field']['table']['date_end'] = 'text';

    $_GLOBALS['field']['settings']['background']['count'] = 1;
    $_GLOBALS['field']['settings']['background']['type'] = '"�������": "*.jpg; *.jpeg; *.gif; *.png"';
    $_GLOBALS['field']['settings']['background']['extension'] = array('jpg', 'jpeg', 'gif', 'png');

    $_GLOBALS['edit-field'] = array('active', 'title','pos', 'date_start','date_end', 'background');
    $_GLOBALS['add-field'] = $_GLOBALS['edit-field'];