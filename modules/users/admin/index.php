<?php
	if (!defined('_SWS_ADMIN_CORE')) exit();
	
	$_GLOBALS['create_dir'] = false;
	$_GLOBALS['add'] = true;
	$_GLOBALS['search'] = true;
	$_GLOBALS['on-page'] = 20;
	$_GLOBALS['tiny'] = false;
	$_GLOBALS['date'] = false;
	
	$_GLOBALS['field']['title']['id'] = 'ID';
	$_GLOBALS['field']['title']['sid'] = '������/���������';
	$_GLOBALS['field']['title']['active'] = '���./����.';
	$_GLOBALS['field']['title']['pos'] = '�������';
	$_GLOBALS['field']['title']['email'] = '�����';
	$_GLOBALS['field']['title']['pass'] = '������';

    $_GLOBALS['field']['title']['insert'] = '���� �����������';

	$_GLOBALS['field']['title']['username'] = '���';
	$_GLOBALS['field']['title']['name'] = '���';
	$_GLOBALS['field']['title']['family'] = '�������';
	$_GLOBALS['field']['title']['patronymic'] = '��������';
	$_GLOBALS['field']['title']['sex'] = '���';
	$_GLOBALS['field']['title']['phone'] = '�������';
	$_GLOBALS['field']['title']['city'] = '�����';
	$_GLOBALS['field']['title']['home'] = '���';
	$_GLOBALS['field']['title']['housing'] = '������';
	$_GLOBALS['field']['title']['hkey'] = '������';
	$_GLOBALS['field']['title']['country'] = '������';
	$_GLOBALS['field']['title']['street'] = '�����';
	$_GLOBALS['field']['title']['apartment'] = '��������';
    $_GLOBALS['field']['title']['user_card'] = '����� �������';
    $_GLOBALS['field']['title']['discount'] = '������';
    $_GLOBALS['field']['title']['soc_nw'] = '���. ����';
	
	$_GLOBALS['field']['type']['sid'] = 'sid';
	$_GLOBALS['field']['type']['active'] = 'checkbox';
	$_GLOBALS['field']['type']['pos'] = 'position';
	$_GLOBALS['field']['type']['email'] = 'text';
	$_GLOBALS['field']['type']['pass'] = 'password';

    $_GLOBALS['field']['type']['insert'] = 'text';

	$_GLOBALS['field']['type']['username'] = 'text';
	$_GLOBALS['field']['type']['name'] = 'text';
	$_GLOBALS['field']['type']['family'] = 'text';
	$_GLOBALS['field']['type']['patronymic'] = 'text';
	$_GLOBALS['field']['type']['sex'] = 'select';
	$_GLOBALS['field']['type']['phone'] = 'text';
	$_GLOBALS['field']['type']['city'] = 'text';
	$_GLOBALS['field']['type']['home'] = 'text';
	$_GLOBALS['field']['type']['housing'] = 'text';
	$_GLOBALS['field']['type']['hkey'] = 'text';
	$_GLOBALS['field']['type']['country'] = 'text';
	$_GLOBALS['field']['type']['street'] = 'text';
	$_GLOBALS['field']['type']['apartment'] = 'text';
    $_GLOBALS['field']['type']['user_card'] = 'text';
    $_GLOBALS['field']['type']['discount'] = 'text';
	
	$_GLOBALS['field']['db']['sid'] = "int(11) NOT NULL default 0";
	$_GLOBALS['field']['db']['active'] = "int(1) NOT NULL default 0";
	$_GLOBALS['field']['db']['pos'] = "int(11) NOT NULL default 0";
	$_GLOBALS['field']['db']['email'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['pass'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['username'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['name'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['family'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['patronymic'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['sex'] = "int(11) NOT NULL default 0";
	$_GLOBALS['field']['db']['phone'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['city'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['home'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['housing'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['hkey'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['country'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['street'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['apartment'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['insert'] = "timestamp NOT NULL default CURRENT_TIMESTAMP";
	$_GLOBALS['field']['db']['system'] = "int(1) NOT NULL default 0";
	$_GLOBALS['field']['db']['vk'] = "text NOT NULL";
	$_GLOBALS['field']['db']['vk_scope'] = "text NOT NULL";
	$_GLOBALS['field']['db']['odnoklassniki'] = "text NOT NULL";
	$_GLOBALS['field']['db']['odnoklassniki_scope'] = "text NOT NULL";
	$_GLOBALS['field']['db']['mailru'] = "text NOT NULL";
	$_GLOBALS['field']['db']['mailru_scope'] = "text NOT NULL";
	$_GLOBALS['field']['db']['yandex'] = "text NOT NULL";
	$_GLOBALS['field']['db']['yandex_scope'] = "text NOT NULL";
	$_GLOBALS['field']['db']['google'] = "text NOT NULL";
	$_GLOBALS['field']['db']['google_scope'] = "text NOT NULL";
	$_GLOBALS['field']['db']['facebook'] = "text NOT NULL";
	$_GLOBALS['field']['db']['facebook_scope'] = "text NOT NULL";
	$_GLOBALS['field']['db']['reg_url'] = "text NOT NULL";
	$_GLOBALS['field']['db']['user_card'] = "text NOT NULL";

	// ������� ��� ���������� ���� ������
	$_GLOBALS['field']['table']['id'] = 'text';
	$_GLOBALS['field']['table']['email'] = 'title';
	$_GLOBALS['field']['table']['username'] = 'text';
    $_GLOBALS['field']['table']['soc_nw'] = 'soc_nw';
    $_GLOBALS['field']['table']['phone'] = 'text';
    $_GLOBALS['field']['table']['discount'] = 'user-disc';
    $_GLOBALS['field']['table']['insert'] = 'text';

	$_GLOBALS['field']['value']['active'] = 1;
    $_GLOBALS['field']['value']['sex'] = array(1=>'�������', 2=>'�������');
	
    $_GLOBALS['field']['search'] = array('email', 'user_card' , 'username', 'phone');

    // edit-field - ���� ��� ��������������
    // add-field - ���� ��� ����������
	$_GLOBALS['edit-field'] = array('active', 'email', 'pass', 'insert', 'user_card' , 'discount', 'username', 'name', 'family', 'patronymic', 'sex', 'phone', 'city', 'home', 'housing', 'hkey', 'country', 'street', 'apartment');
    // ��� ���������� ����� �� ������������� ����� �����, ��� ��� ����� ������������� � ID �������
    // � ������, ����������, ��� �����, ��� ��� ����� ���������� ������� ��������� ������ �������
	$_GLOBALS['add-field'] = array('active', 'email', 'pass', 'insert', 'discount', 'username', 'name', 'family', 'patronymic', 'sex', 'phone', 'city', 'home', 'housing', 'hkey', 'country', 'street', 'apartment');
    // ��� ����� ���������� ������ �������
        
    check_tbl(array($module=>$_GLOBALS['field']['db']));