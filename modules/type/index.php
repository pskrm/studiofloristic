<?
if (!defined('SW')) die('���� ������� �� ���������');

include_once('include/jquery/jQuery.php');

switch($op) {
	case 'set': setComposition(); break;
	default: jQuery::getResponse(); break;
}

function setComposition() {
	if (empty($_GET['cid']) || empty($_GET['id'])) jQuery::getResponse();
	$cid = abs(intval($_GET['cid']));
	$id = abs(intval($_GET['id']));

	$r = db_query('SELECT `composition`, `size` FROM `sw_catalog_price` WHERE `cid` = '.$cid.' AND `id` = '.quote($id));
	if (mysql_num_rows($r) == 0) jQuery::getResponse();
	$c = mysql_fetch_array($r, MYSQL_ASSOC);

	if (!empty($c['composition'])) {
		$html = '<table cellpadding="0" cellspacing="0" border="0" width="100%"><tbody>';

		$structure = explode('<br />', $c['composition']);
		foreach ($structure as $line) {
			if (!empty($line)) {
				if (strstr($line,':')) {
					$line = explode(':', $line);
					$html.= '<tr><td><b>'.$line[0].':</b></td><td>'.$line[1].'</td></tr>';
				} else $html.= '<tr><td colspan="2"><b>'.$line.'</b></td></tr>';
			}
		}

		$html.= '</tbody></table>';
	} else $html = null;
	$fast = (isset($_GET['fast']))?'fast-':'';
	jQuery('span#'.$fast.'composition')->html(inUTF8($html));
	jQuery('#'.$fast.'item-size')->html(inUTF8(!empty($c['size']) ? $c['size'] : '�'));
	jQuery::getResponse();
}

function insertReview() {
	if (empty($_POST['name']) || empty($_POST['email']) || empty($_POST['key']) || empty($_POST['text'])) {
		jQuery::evalScript('reviews.error(1)');
		jQuery::getResponse();
	}

	if (empty($_SESSION['key']) || $_SESSION['key'] != $_POST['key']) {
		jQuery::evalScript('reviews.error(2)');
		jQuery::getResponse();
	}

	$name = filter(trim(in1251($_POST['name'])), 'nohtml');
	$email = filter(trim(in1251($_POST['email'])), 'nohtml');
	$text = filter(trim(in1251($_POST['text'])), 'nohtml');
	$time = time();

	$text = str_replace("\n", '<br />', $text);

	db_query('INSERT INTO `sw_reviews` (`active`, `title`, `email`, `text`, `insert`) VALUES (1, '.quote($name).', '.quote($email).', '.quote($text).', '.quote($time).')');

	jQuery::evalScript(inUTF8('reviews.insert({name:"'.htmlspecialchars($name).'",text:"'.htmlspecialchars($text).'",date:"'.date('d.m.Y', $time).'"})'));
	jQuery::getResponse();
}

function getReviews() {
	global $_GLOBALS, $module;

	$data = PageData($module);
	$html = null;

	$v1 = rand(1,9);
	$v2 = rand(1,9);
	$_SESSION['key'] = $v1 + $v2;

	if (!empty($data['text'])) $html.= '<div class="mainTextInner">'.stripslashes($data['text']).'</div>';

	$html.= '<div id="list-reviews">';
	$r = db_query('SELECT `title`, `text`, `reply`, `insert` FROM `sw_reviews` WHERE `active` = 1 ORDER BY `id` DESC');
	while ($o = mysql_fetch_array($r, MYSQL_ASSOC)) $html.= '<div class="otzivsBlock blockWithTopLine"><span class="date">'.date('d.m.Y', $o['insert']).'</span><i>'.$o['text'].'<b class="name">'.$o['title'].'</b></i>'.(!empty($o['reply']) ? '<p>- '.$o['reply'].'</p>' : null).'</div>';
	$html.= '</div>';

	$html.= '<div class="createOtviz blockWithTopLine"><form action="/reviews/op=insert" onsubmit="formSubmit(this); return false;"><span class="h2_alt">�������� ���� �����</span><div class="leftPart floatLeft"><p>���� ���:<br /><input name="name" type="text" /></p><p>E-mail<br /><input name="email" type="text" /></p><p class="code">'.$v1.'+'.$v2.' = <input name="key" type="text" /></p></div><div class="rightPart floatRight"><p>����� ���������:<br /><textarea name="text" cols="1" rows="1"></textarea></p></div><div class="clear"></div><div class="floatRight"><span class="gray">����� ��������� ��� ����!</span> <input type="submit" value="���������" /></div><div class="clear"></div></form></div>';

	$_GLOBALS['title'] = $data['title'];
	$_GLOBALS['text'] = $html;
	$_GLOBALS['html_title'] = !empty($data['html_title']) ? $data['html_title'] : $data['title'].' - '.$_GLOBALS['v_sitename'];
	if (!empty($data['keywords'])) $_GLOBALS['keywords'] = $data['keywords'];
	if (!empty($data['description'])) $_GLOBALS['description'] = $data['description'];
	$_GLOBALS['template'] = 'default';
	$_GLOBALS['menu'] = 'contact';
}

?>