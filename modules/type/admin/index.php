<?php
if (!defined('_SWS_ADMIN_CORE')) exit();

switch ($op) {
	case 'refresh': refreshType(); break;
	case 'edit': editType(); break;
	case 'insert': insertType(); break;
	case 'remove': removeType(); break;
	case 'first': firstType(); break;
}

function refreshType($cid) {
	$html = '<tr><th class="title">������������</th><th class="composition">������</th><th class="price">����</th><th class="price-old">������ ����</th><th class="size">������</th><th class="options">&nbsp;</th></tr>';
	
	$r = db_query('SELECT `id`, `title`, `price`, `price-old`, `size`, `composition`, `first` FROM `sw_catalog_price` WHERE `cid` = '.quote($cid));
	while ($t = mysql_fetch_array($r, MYSQL_ASSOC)) {
		$html.= '<tr id="type-'.$t['id'].'"><td class="title"><a href="javascript:void(0)" onclick="initGET(\'/go/m=type&op=edit&id='.$t['id'].'\')">'.$t['title'].'</a></td><td>'.(!empty($t['composition']) ? str_replace("\n", '<br>', $t['composition']) : '�').'</td><td>'.$t['price'].' ���.</td><td>'.$t['price-old'].' ���.</td><td>'.$t['size'].'</td><td class="options">'.(!empty($t['first']) ? '�� ���������' : '<a href="javascript:type.first('.$cid.','.$t['id'].')">�� ���������</a>').' | <a href="javascript:type.remove('.$cid.','.$t['id'].')">�������</a></td></tr>';
	}
	
	jQuery('table#types-list')->html(inUTF8($html));
	jQuery::getResponse();
}

function editType() {
	if (empty($_GET['id'])) jQuery::getResponse();
	$id = abs(intval($_GET['id']));
	
	if (!empty($_POST)) {
		if (empty($_POST['title']) || empty($_POST['price'])) {
			jQuery::evalScript(inUTF8('initSysDialog("����, ���������� * ����������� ��� ����������")'));
			jQuery::getResponse();
		}
		
		$r = db_query('SELECT `cid` FROM `sw_catalog_price` WHERE `id` = '.quote($id));
		if (mysql_num_rows($r) == 0) jQuery::getResponse();
		$p = mysql_fetch_array($r, MYSQL_ASSOC);
		
		$composition = str_replace("\n", '<br />', filter(trim(in1251($_POST['composition'])), 'nohtml'));
		$title = filter(trim(in1251($_POST['title'])), 'nohtml');
		$price = abs(intval($_POST['price']));
		$price_old = !empty($_POST['price-old']) ? abs(intval($_POST['price-old'])) : 0;
		$size = !empty($_POST['size']) ? filter(trim(in1251($_POST['size'])), 'nohtml') : null;
		
		db_query('UPDATE `sw_catalog_price` SET `title` = '.quote($title).', `price` = '.quote($price).', `price-old` = '.quote($price_old).', `size` = '.quote($size).', `composition` = '.quote($composition).' WHERE `id` = '.quote($id));
		
		jQuery::evalScript(inUTF8('jQuery("table#edit-type input#cancel").trigger("click");initSysDialog("������� ���������");'));
		
		refreshType($p['cid']);
	}
	
	$r = db_query('SELECT `title`, `price`, `price-old`, `price-start`, `size`, `composition` FROM `sw_catalog_price` WHERE `id` = '.quote($id));
	if (mysql_num_rows($r) != 0) {
		$v = mysql_fetch_array($r, MYSQL_ASSOC);
		jQuery::evalScript(inUTF8('type.edit({id:'.$id.',title:"'.$v['title'].'",size:"'.$v['size'].'",composition:"'.str_replace('<br />', '\n', $v['composition'])).'",price:"'.$v['price'].'",priceOld:"'.$v['price-old'].'"})');
	}
	
	jQuery::getResponse();
}

function firstType() {
	if (!_IS_ADMIN || empty($_GET['id']) || empty($_GET['cid'])) jQuery::getResponse();
	$id = abs(intval($_GET['id']));
	$cid = abs(intval($_GET['cid']));
	
	db_query('UPDATE `sw_catalog_price` SET `first` = 0 WHERE `cid` = '.quote($cid));
	db_query('UPDATE `sw_catalog_price` SET `first` = 1 WHERE `id` = '.quote($id).' AND `cid` = '.quote($cid));
	
	$html = '<tr><th class="title">������������</th><th class="composition">������</th><th class="price">����</th><th class="price-old">������ ����</th><th class="size">������</th><th class="options">&nbsp;</th></tr>';
	
	$r = db_query('SELECT `id`, `title`, `price`, `price-old`, `size`, `composition`, `first` FROM `sw_catalog_price` WHERE `cid` = '.quote($cid));
	while ($t = mysql_fetch_array($r, MYSQL_ASSOC)) {
		$html.= '<tr id="type-'.$t['id'].'"><td class="title"><a href="javascript:void(0)" onclick="initGET(\'/go/m=type&op=edit&id='.$t['id'].'\')">'.$t['title'].'</a></td><td>'.(!empty($t['composition']) ? str_replace("\n", '<br>', $t['composition']) : '�').'</td><td>'.$t['price'].' ���.</td><td>'.$t['price-old'].' ���.</td><td>'.$t['size'].'</td><td class="options">'.(!empty($t['first']) ? '�� ���������' : '<a href="javascript:type.first('.$cid.','.$t['id'].')">�� ���������</a>').' | <a href="javascript:type.remove('.$cid.','.$t['id'].')">�������</a></td></tr>';
	}
	
	jQuery('table#types-list')->html(inUTF8($html));
	jQuery::getResponse();
}

function removeType() {
	if (!_IS_ADMIN || empty($_GET['id']) || empty($_GET['tid'])) jQuery::getResponse();
	$tid = abs(intval($_GET['tid']));
	$id = abs(intval($_GET['id']));
	
	db_delete('catalog_price', '`id` = '.quote($tid).' AND `cid` = '.quote($id));
	
	jQuery::getResponse();
}

function insertType() {
	if (!_IS_ADMIN || empty($_GET['id'])) jQuery::getResponse();
	
	if (empty($_POST['title']) || empty($_POST['price'])) {
		jQuery::evalScript(inUTF8('initSysDialog("����, ���������� * ����������� ��� ����������")'));
		jQuery::getResponse();
	}
	
	$composition = str_replace("\n", '<br />', filter(trim(in1251($_POST['composition'])), 'nohtml'));
	$title = filter(trim(in1251($_POST['title'])), 'nohtml');
	$price = abs(intval($_POST['price']));
	$price_old = !empty($_POST['price-old']) ? abs(intval($_POST['price-old'])) : 0;
	$size = !empty($_POST['size']) ? filter(trim(in1251($_POST['size'])), 'nohtml') : null;
	$id = abs(intval($_GET['id']));
	
	$count = dbone('COUNT(`id`)', '`cid` = '.quote($id), 'catalog_price');
	
	if ($count != 0) {
		db_query('INSERT INTO `sw_catalog_price` (`cid`, `title`, `price`, `price-old`, `price-start`, `size`, `composition`) VALUES ('.quote($id).', '.quote($title).', '.quote($price).', '.quote($price_old).', '.quote($price).', '.quote($size).', '.quote($composition).')');
	} else {
		db_query('INSERT INTO `sw_catalog_price` (`cid`, `title`, `price`, `price-old`, `price-start`, `size`, `composition`, `first`) VALUES ('.quote($id).', '.quote($title).', '.quote($price).', '.quote($price_old).', '.quote($price).', '.quote($size).', '.quote($composition).', 1)');
	}
	
	$tid = dbone('MAX(`id`)', '`cid` = '.quote($id), 'catalog_price');
	
	jQuery::evalScript(inUTF8('type.create({id:'.$tid.',cid:'.$id.',title:"'.$title.'",size:"'.$size.'",composition:"'.$composition.'",price:'.$price.',priceOld:'.$price_old.',first:'.($count!=0?0:1).'})'));
	jQuery::getResponse();
}