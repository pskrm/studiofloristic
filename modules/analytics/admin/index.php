<?php


if (!defined('_SWS_ADMIN_CORE')) exit();

$_GLOBALS['create_dir'] = false;
$_GLOBALS['add'] = false;
$_GLOBALS['search'] = false;
$_GLOBALS['on-page'] = 20;
$_GLOBALS['tiny'] = false;
$_GLOBALS['date'] = false;


function moduleAnalyticsGetAnalytics($tpl, &$html)
{
    $page = !empty($_REQUEST['page']) ? filter(trim($_REQUEST['page']), 'nohtml') : 'startpage';
    if ($page == 'startpage'){
        $adds = '<p></p><p></p>';
        $adds .= '<a onclick="initGET(\'/go/m=analytics&op=load-module&page=most-popular\')">����� ����� ����������� ������</a>';
        $adds .= '<p></p><a onclick="initGET(\'/go/m=analytics&op=load-module&page=users-stats\')">���������� �� ��������</a>';
        $adds .= '<p></p><a onclick="initGET(\'/go/m=analytics&op=load-module&page=order-stats\')">���������� �� �������</a>';
        $adds .= '<p></p><a onclick="initGET(\'/go/m=analytics&op=load-module&page=region-stats\')">���������� �� ��������</a>';
        $adds .= '<p></p><a onclick="initGET(\'/go/m=analytics&op=load-module&page=user-base\')">���� ������������������ �������������</a>';
        $adds .= '<p></p><a onclick="initGET(\'/go/m=analytics&op=load-module&page=user-unreg\')">�������������������� ������������</a>';

    } elseif($page == 'most-popular'){
        $adds = moduleAnalyticsGetMostPopular();
    } elseif($page == 'users-stats'){
        $adds = moduleAnalyticsGetUsersStats();
    } elseif($page == 'users-stats-xl'){
        $adds = moduleAnalyticsGetUsersStatsXL();
    }elseif($page == 'order-stats'){
        $adds = moduleAnalyticsGetOrderStats();
    }elseif ($page == 'region-stats'){
        $adds = moduleAnalyticsGetOrderStatsRegion();
    }elseif($page == 'user-base'){
        $adds = moduleGetUserDataBase();
    }elseif ($page == 'user-unreg'){
        $adds = moduleGetUnregUsersList();
    }

    $html .= $adds;

    return $html;
}

/**
 * ���������� ������ ���� �� ���������� ������ � ��
 */

function moduleGetUnregUsersList(){

    $sql = "SELECT `uData`, `uName`, `uPhone`, `email`, `uAdres`, `insert` FROM `sw_orders` WHERE `uid` = 0 GROUP BY `phone-sys` ORDER BY `id` DESC LIMIT 0, 99999999;";
    $orders = query_new($sql);
    if (!count($orders))
    {
        return '<h1>� ��������� ������ ��� ������ �����������</h1>';
    }
    $res = array();
    $caption = "���� ������ ������������������ ������������� �� ��� �����.";
    $resTable = '<br/><h2>'.$caption.'</h2><br/><h3>��� �� ����������������: <strong>'.count($orders).'</strong> </h3>';
    $resTable .= '<table width="100%" border="1">
    	<tr>
    	<th align="left">��� �������</th>
    	<th align="left">Email</th>
    	<th align="left">�������</th>
    	<th align="left">����� (���� ������)</th>
    	<th align="left">���� ���������� ������</th>
    	</tr>
    	';


    $path = __DIR__ . '/Classes/PHPExcel.php';

    if (!file_exists($path)){
        return $path;
    }

    include_once 'Classes/PHPExcel.php';

    $objPHPExcel = new PHPExcel();

    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', _i('���') )
        ->setCellValue('B1', _i('E-mail') )
        ->setCellValue('C1', _i('�������') )
        ->setCellValue('D1', _i('����� (���� ������)') )
        ->setCellValue('E1', _i('���� ���������� ������') ) ;

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);


    $row = 2;

    $exists = array();
    foreach ($orders as $order){


        if (mb_strpos($order['uPhone'], ',') != false)
        {
            $phones = explode(",", $order['uPhone']);
            $nphones = array();
            foreach ($phones as $ph)
            {
                $nphones[] = is_phone($ph);
            }
            $phones =  implode(", " , $nphones);
        }
        elseif (mb_strpos($order['uPhone'], ';') != false)
        {
            $phones = explode(";", $order['uPhone']);
            $nphones = array();
            foreach ($phones as $ph)
            {
                $nphones[] = is_phone($ph);
            }
            $phones =  implode(", " , $nphones);
        }
        else
        {
            $phones = is_phone($order['uPhone']);
        }



        $resTable .= '<tr>';
        $resTable .= '<td><b>'.strip_tags_content($order['uName']) .'</b></td>';
        $resTable .= '<td>'.$order['email'].'</td>';
        $resTable .= '<td>'. (( strlen($phones ) < 11 ) ? "<span style='color: #aa0000;'>(". $order['uPhone'].")</span>" :  $phones ).'</td>';
        $resTable .= '<td>'. $order['uAdres']  .'</td>';
        $resTable .= '<td>'. $order['insert'] .'</td>';
        $resTable .= '</tr>';
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValueExplicit('A'.$row, _i( strip_tags_content($order['uName']) ), PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValueExplicit('B'.$row, _i($order['email']), PHPExcel_Cell_DataType::TYPE_STRING )
            ->setCellValueExplicit('C'.$row, _i((( strlen($phones ) < 11 ) ? "(". $order['uPhone'].")" :  $phones )), PHPExcel_Cell_DataType::TYPE_STRING )
            ->setCellValueExplicit('D'.$row, _i($order['uAdres']), PHPExcel_Cell_DataType::TYPE_STRING )
            ->setCellValueExplicit('E'.$row, _i($order['insert']), PHPExcel_Cell_DataType::TYPE_STRING );

        $row++;
    }

    $objPHPExcel->setActiveSheetIndex(0);
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save($_SERVER['DOCUMENT_ROOT'].'/files/users_unger.xlsx');

    $path = '/files/users_unger.xlsx';
    $resTable .= '</table>';
    $resTable = '<a href="'.$path.'">������� � ������� Excel</a><p></p><p></p>' . $resTable;

    return $resTable ;

}

/**
 * ���������� ������ ���� ���������� ������ � ��, ���� � ���� ��� � ������� ������ "������������"
 */

function moduleGetUserDataBase(){
    $sql = "SELECT * FROM `sw_users` ORDER BY id DESC LIMIT 0,9999999;";
    $orders = query_new($sql);
    if (!count($orders))
    {
        return '<h1>� ��������� ������ ��� ������ �����������</h1>';
    }
    $res = array();
    $caption = "���� ������ ������������������ ������������� �� ��� �����.";
    $resTable = '<br/><h2>'.$caption.'</h2><br/><h3>��� ����������������: <strong>'.count($orders).'</strong> </h3>';
    $resTable .= '<table width="100%" border="1">
    	<tr>
    	<th align="left">��� �������</th>
    	<th align="left">Email</th>
    	<th align="left">�������</th>
    	<th align="left">����� (���� ������)</th>
    	</tr>
    	';


    $path = __DIR__ . '/Classes/PHPExcel.php';

    if (!file_exists($path)){
        return $path;
    }

    include_once 'Classes/PHPExcel.php';

    $objPHPExcel = new PHPExcel();

    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', _i('���') )
        ->setCellValue('B1', _i('E-mail') )
        ->setCellValue('C1', _i('�������') )
        ->setCellValue('D1', _i('����� (���� ������)') ) ;

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);


    $row = 2;

    $exists = array();
    foreach ($orders as $order){

       // if (in_array(strip_tags_content($order['email']), $exists)) continue;
        // ������ ��� ���� ���� ��� ���������
     //   if (empty($order['sys-phone']) ) continue;

        $exists[] = strip_tags_content($order['email']);
        $str = $order['city'] . '/' . $order['country']  ;
        if (mb_substr($str,0, 1) == '/'){
            $str = "";
        }
        $resTable .= '<tr>';
        $resTable .= '<td><b>'.str_replace("<p>", "", strip_tags_content($order['username'])) .'</b></td>';
        $resTable .= '<td>'.$order['email'].'</td>';
        $resTable .= '<td>'. (( strlen($order['sys-phone']) < 10 ) ? "" :  is_phone($order['sys-phone'])).'</td>';
        $resTable .= '<td>'. $str  .'</td>';
        $resTable .= '</tr>';
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValueExplicit('A'.$row, _i( trim( str_replace("<p>", "", strip_tags_content($order['username']))) ), PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValueExplicit('B'.$row, _i($order['email']), PHPExcel_Cell_DataType::TYPE_STRING )
            ->setCellValueExplicit('C'.$row, _i(is_phone($order['sys-phone'])), PHPExcel_Cell_DataType::TYPE_STRING )
            ->setCellValueExplicit('D'.$row, _i(strip_tags_content($str)), PHPExcel_Cell_DataType::TYPE_STRING );

        $row++;
    }

    $objPHPExcel->setActiveSheetIndex(0);
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save($_SERVER['DOCUMENT_ROOT'].'/files/users_bd.xlsx');

    $path = '/files/users_bd.xlsx';
    $resTable .= '</table>';
    $resTable = '<a href="'.$path.'">������� � ������� Excel</a><p></p><p></p>' . $resTable;

    return $resTable ;
}

/** ���������� ������� ���� �������������/��������, ������� ���� ��� ������ ������ (�.�. ��������� ����� ������)
 * @return string
 */
function moduleAnalyticsGetOrderStats(){
    //1. �������� ��� ������ �� ��
    $sql = "SELECT * FROM `sw_orders` WHERE `active` = 1 ORDER BY id DESC LIMIT 0,999999;";
    $orders = query_new($sql);
    if (!count($orders))
    {
        return '<h1>� ��������� ������ ��� ������ �����������</h1>';
    }

    $res = array();
    $caption = "������ ���� ���������� ��������-�������� �� ��� �����";
    $resTable = '<br/><h2>'.$caption.'</h2>';
    $resTable .= '<table width="100%" border="1">
    	<tr>
    	<th align="left">��� �������</th>
    	<th align="left">Email</th>
    	<th align="left">�������</th>
    	<th align="left">����� (�� ������)</th>
    	</tr>
    	';
    //2. ��������� ������ �������. ��� ������ ��� ���� ������� ���� ������ � ������, ���� ������ ���������

    //3. �������� ��������

    $path = __DIR__ . '/Classes/PHPExcel.php';

    if (!file_exists($path)){
        return $path;
    }

    include_once 'Classes/PHPExcel.php';

    $objPHPExcel = new PHPExcel();

    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', _i('���') )
        ->setCellValue('B1', _i('E-mail') )
        ->setCellValue('C1', _i('�������') )
        ->setCellValue('D1', _i('����� (�� ������)') ) ;

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);


    $row = 2;

    $exists = array();
    foreach ($orders as $order){
        $uData = explode("</p>", $order['uData']);
        if (in_array(strip_tags_content($uData[0]), $exists)) continue;
        // ������ ��� ���� ���� ��� ���������
        if (empty($order['phone-sys']) ) continue;
        if (strlen($order['phone-sys']) < 10 ) continue;
        if (empty(is_phone($order['phone-sys']) )) continue;
        $exists[] = strip_tags_content($uData[0]);
        $resTable .= '<tr>';
        $resTable .= '<td><b>'.str_replace("<p>", "", strip_tags_content($uData[0])) .'</b></td>';
        $resTable .= '<td>'.$order['email'].'</td>';
        $resTable .= '<td>'. (( strlen($order['phone-sys']) < 10 ) ? "" :  is_phone($order['phone-sys'])).'</td>';
        $resTable .= '<td>'. $order['uAdres'] .'</td>';
        $resTable .= '</tr>';

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValueExplicit('A'.$row, _i( trim( str_replace("<p>", "", strip_tags_content($uData[0]))) ), PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValueExplicit('B'.$row, _i($order['email']), PHPExcel_Cell_DataType::TYPE_STRING )
            ->setCellValueExplicit('C'.$row, _i(is_phone($order['phone-sys'])), PHPExcel_Cell_DataType::TYPE_STRING )
            ->setCellValueExplicit('D'.$row, _i(strip_tags_content($order['uAdres'])), PHPExcel_Cell_DataType::TYPE_STRING );

        $row++;
    }

    $objPHPExcel->setActiveSheetIndex(0);
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save($_SERVER['DOCUMENT_ROOT'].'/files/buyers.xlsx');

    $path = '/files/buyers.xlsx';
    $resTable .= '</table>';
    $resTable = '<a href="'.$path.'">������� � ������� Excel</a><p></p><p></p>' . $resTable;

    return $resTable ;
}

function moduleAnalyticsGetOrderStatsRegion(){
    $sql = "SELECT region as `name`, COUNT(region) as `c`
FROM sw_orders WHERE NOT ISNULL(region) 
GROUP BY region ORDER BY COUNT(region) ;";
    $regions = query_new($sql);
    if (!count($regions))
    {
        return '<h1>� ��������� ������ ��� ������ �����������</h1>';
    }

    $res = array();
    $caption = "������ �������� �� ������� ��������� ������";
    $resTable = '<br/><h2>'.$caption.'</h2>';
    $resTable .= '<table width="100%" border="1">
    	<tr>
    	<th align="left">������</th>
    	<th align="left">���-�� �������</th>
    	</tr>
    	';
    foreach ($regions as $region){
        if (mb_substr($region['name'], 0, 1) == "/"){
            $region['name']= ltrim($region['name'], '/');
        }

        $resTable .= '<tr>';
        $resTable .= '<td><b>' .$region['name']. '</b></td>';
        $resTable .= '<td>'.$region['c']. '</td>';
        $resTable .= '</tr>';

    }
    $resTable .= '</table>';
    return $resTable ;
}


function moduleAnalyticsGetMostPopular()
{
    $params = 'Get params';
    # ��������� ���������: ����, ����(������)
    $date_from = null;
    $date_to = null;

    # ���������� �� ����: select * from tbl WHERE ods.insert < '2009-05-01'
    #$date_from = '2016-02-01';
    #$date_to = '2016-03-15';
    $caption = "����� ����������� ������ (�� ��� �����)";

    $query = "
		SELECT oi.*,
         cat.title AS cat_title, cat.structure AS cat_structure,
         ods.insert AS ins,
         prc.composition AS composition
         FROM sw_orders_item AS oi
         LEFT JOIN sw_catalog 		cat ON oi.cid = cat.id
         LEFT JOIN sw_orders 		ods ON oi.oid = ods.id
         LEFT JOIN sw_catalog_price prc ON oi.pid = prc.id
		WHERE oi.type='item' AND ods.uid NOT IN(1,2,3)
           ";
    if (!is_null($date_from) && !is_null($date_to))
    {
        $query .= " AND ods.insert BETWEEN '".$date_from."' AND '".$date_to."'";
        $caption = "����� ����������� ������ (�� ������ � ".$date_from." �� ".$date_to.")";
    }


    $items = query_new($query);

    if (!count($items))
    {
        return '������ �� �������';
    }

    $res = array();

    foreach($items as $item)
    {
        // ���������� hash ������
        $hash = md5($item['cid'].$item['pid']);

        if ( isset($res[$hash]) )
        {
            $res[$hash]['count'] = $res[$hash]['count'] + 1;
        }
        else
        {
            $res[$hash] = $item;
            $res[$hash][count] = 1;
        }
    }

    $res = array_values($res);
    usort( $res, "moduleAnalyticsMostPopularSort");
    $res = array_reverse($res);


    // ������ ������� ��� ��� � html-�������
    // ��������� �������� �� �������
    $resTable = '<h2>'.$caption.'</h2>';
    $resTable .= '<table width="100%" border="1">
    	<tr>
    	<th align="left">ID ������</th>
    	<th align="left">������������ ������</th>
    	<th align="left">��� ������</th>
    	<th align="left">���������� �������</th>
    	</tr>
    	';

    foreach ($res as $item) {
        $resTable .= '<tr>
			<td>'.$item['cid'].'</td>
			<td>'.$item['cat_title'].'</td>
			<td>'.$item['title'].'</td>
			<td>'.$item['count'].'</td>
		</tr>';
    }

    $resTable .= '</table>';

    return $resTable;

}

function moduleAnalyticsMostPopularSort($a, $b)
{
    if ($a['count'] == $b['count']){
        return 0;
    }

    return ($a['count'] < $b['count']) ? -1 : 1;
}


function moduleAnalyticsGetUsersStats()
{
    $users = query_new("
   	SELECT u.*, o.id AS oid, o.itogo, o.uPhone
    		FROM sw_users u
    		LEFT JOIN sw_orders o ON u.id = o.uid
    		WHERE o.uid > 0 AND u.id NOT IN (1,2,3)
           ");

    if (!count($users))
    {
        return 'nothing';
    }

    $res = array();

    foreach ($users as $user)
    {
        if ( !isset( $res[$user['id']] ) )
        {
            $user['his_orders'] = array($user['oid']);
            $user['orders_count'] = 1;
            $res[$user['id']] = $user;
        }
        else
        {
            $res[$user['id']]['his_orders'][] = $user['oid'];
            $res[$user['id']]['orders_count']++;
            $res[$user['id']]['uphone'] = $user['uPhone'];
        }
    }

    $res = array_values($res);
    usort( $res, "moduleAnalyticsUsersSort" );
    $res = array_reverse($res);

    $resTable = '<h2>���������� �� ����������� </h2>';
    $resTable .= '<h3>���������� �� ���������� ������� (�� ��������) </h3>';
    $resTable .= '<table width="100%" border="1">
    	<tr>
    	<th align="left" width="5%"> ID </th>
    	<th align="left" width="20%"> ��� </th>
    	<th align="left" width="5%"> ��� </th>
		<th align="left" width="20%"> E-mail </th>
		<th align="left" width="10%"> ������� (������)</th>
		<th align="left" width="10%"> ������� (����.�����)</th>
		<th align="left" width="30%"> ������ </th>
		</tr>
    ';

    $path = __DIR__ . '/Classes/PHPExcel.php';

    if (!file_exists($path)){
        return $path;
    }

    include_once 'Classes/PHPExcel.php';

    $objPHPExcel = new PHPExcel();

    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', _i('ID') )
        ->setCellValue('B1', _i('���') )
        ->setCellValue('C1', _i('���') )
        ->setCellValue('D1', _i('E-mail') )
        ->setCellValue('E1', _i('������� (������') )
        ->setCellValue('F1', _i('������� (����. �����') );

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

    $row = 2;

    foreach ($res as $user) {

        switch ($user['sex']) {
            case 1: $sex =  '�'; break;
            case 2: $sex =  '�'; break;
            default: $sex = ''; break;
        }

        $orders = array();
        if ( count($user['his_orders']) )
        {
            foreach ($user['his_orders'] as $oid) {
                $orders[] = '<a href="javascript:order.view('.$oid.'); void(0);">'.$oid.'</a>';
            }
        }
        $orders = implode(', ', $orders);
        $orders .= $user['orders_count'] == 1 ? '' : '( ����� - ' . $user['orders_count'] . ')';

        $resTable .= '
    		<tr>
    		<td>'.$user['id'].'</td>
    		<td>'.$user['username'].'</td>
    		<td>'.$sex.'</td>
    		<td>'.$user['email'].'</td>
    		<td>'.$user['phone'].'</td>
    		<td>'.$user['uphone'].'</td>
    		<td>'.$orders.'</td>
    		</tr>
    	';

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValueExplicit('A'.$row, $user['id'], PHPExcel_Cell_DataType::TYPE_STRING)
            ->setCellValueExplicit('B'.$row, _i($user['username']), PHPExcel_Cell_DataType::TYPE_STRING )
            ->setCellValueExplicit('C'.$row, _i($sex), PHPExcel_Cell_DataType::TYPE_STRING )
            ->setCellValueExplicit('D'.$row, _i($user['email']), PHPExcel_Cell_DataType::TYPE_STRING )
            ->setCellValueExplicit('E'.$row, _i($user['phone']), PHPExcel_Cell_DataType::TYPE_STRING )
            ->setCellValueExplicit('F'.$row, _i($user['uphone']), PHPExcel_Cell_DataType::TYPE_STRING );

        $row++;
    }

    $resTable .= '</table>';

    $objPHPExcel->setActiveSheetIndex(0);
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save($_SERVER['DOCUMENT_ROOT'].'/files/clients.xlsx');

    $path = '/files/clients.xlsx';

    $resTable = '<a href="'.$path.'">������� � ������� Excel</a><p></p><p></p>' . $resTable;

    return $resTable;
}

function moduleAnalyticsUsersSort($a, $b){
    if ($a['orders_count'] == $b['orders_count']){
        return 0;
    }
    return ($a['orders_count'] < $b['orders_count']) ? -1 : 1;
}

function _i($arg) {
    return iconv("windows-1251", "utf-8", $arg);
}

function strip_tags_content($text, $tags = '', $invert = FALSE) {

    preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($tags), $tags);
    $tags = array_unique($tags[1]);

    if(is_array($tags) AND count($tags) > 0) {
        if($invert == FALSE) {
            return preg_replace('@<(?!(?:'. implode('|', $tags) .')\b)(\w+)\b.*?>.*?</\1>@si', '', $text);
        }
        else {
            return preg_replace('@<('. implode('|', $tags) .')\b.*?>.*?</\1>@si', '', $text);
        }
    }
    elseif($invert == FALSE) {
        return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text);
    }
    return $text;
}

function is_phone($destination_numbers)
{
    $destination_number = trim($destination_numbers);

    $arInd = trim($destination_numbers);
    $arInd = trim($destination_numbers,"+");

    $symbol = false;
    $spec_sym = array("+", "(", ")", " ", "-","_");
    for($i = 0; $i < strlen($arInd); $i++)
    {
        if (!is_numeric($arInd[$i]) && !in_array($arInd[$i],$spec_sym))
        {
            $symbol = true;
        }
    }

    if ($symbol)
    {
        return false;
    }
    else
    {
        $arInd = str_replace($spec_sym, "", $arInd);

        if (strlen($arInd) < 4 || strlen($arInd) > 11)
        {
            return false;
        }
        else
        {
            if (strlen($arInd) == 10)
            {
                $arInd = "7".$arInd;
            }

            if (strlen($arInd) == 11 && $arInd[0] == '8')
            {
                $arInd[0] = "7";
            }
            return $arInd;
        }
    }

    $numbers = array_unique($numbers);

    return $numbers;
}
