<?php
if (!defined('SW')) die('���� ������� �� ���������');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

include_once('include/jquery/jQuery.php');

switch($op) {
    case 'init': init(); break;
    case 'get': get_fast_bay(); break;
    default: default_task(); break;
}

function default_task(){
    jQuery::getResponse();
}

function init(){
    $name = '';
    $phone = '';
    if (_IS_USER && !empty($_SESSION['user'])) {
        $name = $_SESSION['user']['username'];
        $phone = $_SESSION['user']['phone'];
    }
    $blocks = query_new("SELECT * FROM sw_text_blocks WHERE `active`=1 AND (`id_name`='fast_baying_top_text' OR `id_name`='fast_baying_bottom_text')", 0, "id_name");
    $top_text = (isset($blocks['fast_baying_top_text']['text']))?$blocks['fast_baying_top_text']['text']:'';
    $bottom_text = (isset($blocks['fast_baying_bottom_text']['text']))?$blocks['fast_baying_bottom_text']['text']:'';
    $html =   '<div id="fast_baying_form" class=" tb_to_body">'
                . '<div class="fast_baying_info">'
                    . '<div class="closeNew"></div>'
                    . '<span class="title">������� �������</span>'
                    . '<table>'
                        . '<tr><td></td><td><div class="text_block_cont" id="top_text" style="margin-bottom:10px">'.$top_text.'</div></td></tr>'
                        . '<tr><td><span class="input_label">���� ���</span></td><td><input type="text" name="fb_name" value="'.$name.'"></td></tr>'
                        . '<tr><td><span class="input_label">��� �������</span></td><td><input type="text" name="fb_phone" value="'.$phone.'"></td></tr>'
                        . '<tr class="times_cont call_now"><td><span class="input_label">��������� �</span></td><td>'
                            . '<table><tr>'
                                . '<td><div class="counter_cont">'
                                    . '<input type="button" value="+" onclick="fastBay.setTime(this)">'
                                    . '<input type="text" value="00" class="fb_h" onchange="fastBay.changeTime(this);" name="call_time_h" maxlength="2">'
                                    . '<input type="button" value="-" onclick="fastBay.setTime(this)">'
                                . '</div></td>'
                                . '<td><span class="input_label doted">:</span></td>'
                                . '<td><div class="counter_cont">'
                                    . '<input type="button" value="+" onclick="fastBay.setTime(this)">'
                                    . '<input type="text" value="00" class="fb_m" onchange="fastBay.changeTime(this);" name="call_time_m" maxlength="2">'
                                    . '<input type="button" value="-" onclick="fastBay.setTime(this)">'
                                . '</div></td>'
                                . '<td><label><input type="checkbox" name="call_to" opt="true"><span class="input_label">��</span></label></td>'
                                . '<td><div class="counter_cont call_to">'
                                    . '<input type="button" value="+" onclick="fastBay.setTime(this)">'
                                    . '<input type="text" value="00" class="fb_h" onchange="fastBay.changeTime(this);" name="call_time_end_h" maxlength="2">'
                                    . '<input type="button" value="-" onclick="fastBay.setTime(this)">'
                                . '</div></td>'
                                . '<td><span class="input_label doted">:</span></td>'
                                . '<td><div class="counter_cont call_to">'
                                    . '<input type="button" value="+" onclick="fastBay.setTime(this)">'
                                    . '<input type="text" value="00" class="fb_m" onchange="fastBay.changeTime(this);" name="call_time_end_m" maxlength="2">'
                                    . '<input type="button" value="-" onclick="fastBay.setTime(this)">'
                                . '</div></td>'
                                . '<td><span class="input_label cur_time">����� � ����� �������:</span></td>'
                                . '<td><div class="counter_cont">'
                                    . '<input type="button" value="+" onclick="fastBay.setTime(this)">'
                                    . '<input type="text" value="00" class="fb_h" onchange="fastBay.changeTime(this);" name="current_time_h" maxlength="2">'
                                    . '<input type="button" value="-" onclick="fastBay.setTime(this)">'
                                . '</div></td>'
                                . '<td><span class="input_label doted">:</span></td>'
                                . '<td><div class="counter_cont">'
                                    . '<input type="button" value="+" onclick="fastBay.setTime(this)">'
                                    . '<input type="text" value="00" class="fb_m" onchange="fastBay.changeTime(this);" name="current_time_m" maxlength="2">'
                                    . '<input type="button" value="-" onclick="fastBay.setTime(this)">'
                                . '</div></td>'
                            . '</tr></table>'
                        . '</td></tr>'
                        . '<tr><td></td><td style="padding-left:3px;"><label><input type="checkbox" name="call_now" opt="false" hide="bottom_text"><span class="call_now_span">��������� ����� ������</span></label>'
                        . '<tr><td></td><td><div class="text_block_cont" id="bottom_text">'.$bottom_text.'</div></td></tr>'
                    . '</table>'
                    .'<div><p class="btns" ><span class="butn_green" onclick="ga(\'send\', \'event\', \'send7UA\', \'Click7UA\'); yaCounter10652362.reachGoal(\'ya7send\'); fastBay.getCall()">�������� ������</span></p></div>'
                . '</div>'
            . '</div>';

    jQuery('body')->append(inUTF8($html));
    jQuery::evalScript("fastBay.initTime();");
    jQuery::getResponse();
}
function get_fast_bay(){
    global  $_GLOBALS,$cur;
    $get = $_GET;
    $name = filter(trim(in1251($get['fb_name'])), 'nohtml');
    $phone = filter(trim(in1251($get['fb_phone'])), 'nohtml');
    $call_now = filter(trim(in1251($get['call_now'])), 'nohtml');
    $current_time = filter(trim(in1251($get['current_time_h'])), 'nohtml').':'.filter(trim(in1251($get['current_time_m'])), 'nohtml');
    $call_time = filter(trim(in1251($get['call_time_h'])), 'nohtml').':'.filter(trim(in1251($get['call_time_m'])), 'nohtml');
    $call_time_end = (intval(filter(trim(in1251($get['call_to'])), 'nohtml'))==1) ? filter(trim(in1251($get['call_time_end_h'])), 'nohtml').':'.filter(trim(in1251($get['call_time_end_m'])), 'nohtml') : '00:00';
    $uid = (_IS_USER && !empty($_SESSION['uid']))? $_SESSION['uid']:0;
	
    $cur_tmp = $cur;
    $cur = set_currency('rub');
    $basket_info = basket_info();

    $cur = set_currency($cur_tmp['name']);
    $products = $basket_info['products'];
    unset($basket_info['products']);
    $itemsMail = '';
    foreach ($products as $tovar){
        $itemsMail.= '<li><a href="http://'._HOSTNAME.$tovar['url'].'" target="_blank">'.$tovar['title'].'</a> - '.$tovar['count'].' ��.</li>';
    }
    query_new("INSERT INTO sw_fast_baying (`name`,`phone`,`call_now`,`call_time`,`call_time_end`,`current_time`,`tovar_list`,`uid`,`amount`,`basket_info`) VALUES ("
            .  quote($name).","
            .  quote($phone).","
            .  quote($call_now).","
            .  quote($call_time).","
            .  quote($call_time_end).","
            .  quote($current_time).","
            .  quote(base64_encode(serialize($products))).","
            .  quote($uid).","
            .  quote($basket_info['discount_summ']).","
            .  quote(base64_encode(serialize($basket_info)))
            . ")");
    $row = query_new("SELECT * FROM sw_fast_baying WHERE id IN (SELECT MAX(id) FROM sw_fast_baying)",1);
    if ($row !==false){
        $time = $row['insert'];
    }else{
        $time = date('Y-m-d H:i:s');
    }

    $oid = intval($row['id']); // id bp ������� ������� �������
	$_SESSION['fast_bay_id'] = $oid ;

    $msg = '<p>������������!</p>';
    $msg.= '<p>�������� "������� �������" �� '.$time.'</p>'
            . '<p>��������: '.$name.'</p>'
            . '<p>�������: '.$phone.'</p>'
            . '<p>������� ����� � ���������:'.$current_time.'</p>';
    if (intval($call_now)>0){
        $msg .= '<p><b>����������� ������!</b></p>';
    }else{
        $msg .= '<p>����������� �: '.$call_time.'</p>';
        if (intval(filter(trim(in1251($get['call_to'])), 'nohtml'))==1){
            $msg .= '<p>����������� ��: '.$call_time_end.'</p>';
        }
    }
    $msg.= '<hr />';
    $msg.= '<p>������ ������:</p><ul>'.$itemsMail.'</ul>';
    $msg.= '<br /><br />';
    $msg.= '--<br />';
    $msg.= '����� <a href="http://'._HOSTNAME.'">'.$_GLOBALS['v_sitename'].'</a>';

    $headers = "Content-type: text/html; charset=windows-1251 \r\nFrom: "._HOSTNAME."<".$_GLOBALS['v_email_admin'].">\r\n";
    $theme = "������� ������� �� �"._HOSTNAME."�";

    $mail = new PHPMailer;
    try {
        $mail->setFrom($_GLOBALS['v_email_admin'], _HOSTNAME);
        $mail->addAddress($_GLOBALS['v_email_admin']);
        $mail->Subject = $theme;
        $mail->isHTML(true);
        $mail->CharSet = 'windows-1251';
        $mail->Body = '<html>'.$msg.'</html>';
        $mail->AltBody = strip_tags($msg);
        $mail->send();
    } catch (Exception $e) {
        unset($e);
    }

    // �������� ������� sms � ��� ��� � ��� � ��������� ����� ��������

    $params = array(
        'msg_type' => SmsQueue::ORDER_FAST_BUY_RECEIVED,
        'oid' => $oid,
        'phone' => $phone
    );
    $smsQueue = new SmsQueue();
    $smsQueue->addMessageInQueue($params);

    // ����� �������� ���������

    jQuery::evalScript(inUTF8('initSysDialog("�������� �������� � ���� � ��������� �����")'));
    jQuery('#window_info_bg')->fadeOut(200);
    jQuery('.window_info')->hide();
    jQuery('.window_info')->removeClass('window_info');
    jQuery::getResponse();
}