<?php
if (!defined('_SWS_ADMIN_CORE')) exit();

	$_GLOBALS['create_dir'] = false;
	$_GLOBALS['add'] = false;
	$_GLOBALS['search'] = false;
	$_GLOBALS['on-page'] = 20;
	$_GLOBALS['tiny'] = true;
	$_GLOBALS['date'] = true;
	$_GLOBALS['nesting'] = false;

	$_GLOBALS['field']['title']['id'] = 'ID';
	$_GLOBALS['field']['title']['active'] = '���./����.';
	$_GLOBALS['field']['title']['pos'] = '�������';
	$_GLOBALS['field']['title']['name'] = '��������';
	$_GLOBALS['field']['title']['phone'] = '�������';
    $_GLOBALS['field']['title']['insert'] = '���������';
    $_GLOBALS['field']['title']['call'] = '���������';
    $_GLOBALS['field']['title']['call_now'] = '��������� ������';
    $_GLOBALS['field']['title']['call_time'] = '��������� �(�)';
    $_GLOBALS['field']['title']['call_time_end'] = '��������� ��';
    $_GLOBALS['field']['title']['current_time'] = '����� � ����������';
    $_GLOBALS['field']['title']['text'] = '���������� ���������';
    $_GLOBALS['field']['title']['tovar_list'] = '������ �������';
	$_GLOBALS['field']['title']['delivery_date'] = '���� ��������';
    $_GLOBALS['field']['title']['amount'] = '����� ������<br>(���.)';

	$_GLOBALS['field']['type']['name'] = 'text';
	$_GLOBALS['field']['type']['active'] = 'checkbox';
	$_GLOBALS['field']['type']['pos'] = 'position';
	$_GLOBALS['field']['type']['call_now'] = 'checkbox';
    $_GLOBALS['field']['type']['call_time'] = 'time';
    $_GLOBALS['field']['type']['current_time'] = 'time';
    $_GLOBALS['field']['type']['call_time_end'] = 'time';
    $_GLOBALS['field']['type']['phone'] = 'text';
    $_GLOBALS['field']['type']['text'] = 'textarea';
    $_GLOBALS['field']['type']['tovar_list'] = 'tovar_list';
    $_GLOBALS['field']['type']['insert'] = 'text';
	$_GLOBALS['field']['type']['delivery_date'] = 'date';
    $_GLOBALS['field']['type']['amount'] = 'text';

	$_GLOBALS['field']['db']['name'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['phone'] = "varchar(255) default NULL";
    $_GLOBALS['field']['db']['call_now'] = "int(1) NOT NULL default '0'";
    $_GLOBALS['field']['db']['call_time'] = "time NOT NULL";
    $_GLOBALS['field']['db']['call_time_end'] = "time NOT NULL";
    $_GLOBALS['field']['db']['current_time'] = "time NOT NULL";
    $_GLOBALS['field']['db']['insert'] = "timestamp NOT NULL default CURRENT_TIMESTAMP";
    $_GLOBALS['field']['db']['sid'] = "int(11) NOT NULL default '0'";
    $_GLOBALS['field']['db']['system'] = "int(1) NOT NULL default '0'";
    $_GLOBALS['field']['db']['active'] = "int(1) NOT NULL default '0'";
	$_GLOBALS['field']['db']['pos'] = "int(11) NOT NULL default '0'";
    $_GLOBALS['field']['db']['text'] = "text NOT NULL";
    $_GLOBALS['field']['db']['tovar_list'] = "text";
    $_GLOBALS['field']['db']['uid'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['delivery_date'] = 'date';
    $_GLOBALS['field']['db']['amount'] = "double NOT NULL default '0'";
    $_GLOBALS['field']['db']['basket_info'] = "text";

	$_GLOBALS['field']['table']['id'] = 'text';
    $_GLOBALS['field']['table']['tovar_list'] = 'viewOrder';
    $_GLOBALS['field']['table']['amount'] = 'text';
    $_GLOBALS['field']['table']['name'] = 'uid_link';
	$_GLOBALS['field']['table']['delivery_date'] = 'text';
    $_GLOBALS['field']['table']['phone'] = 'text';
    $_GLOBALS['field']['table']['call_now'] = 'yes/no';
    $_GLOBALS['field']['table']['call_time'] = 'text';
    $_GLOBALS['field']['table']['call_time_end'] = 'text';
    $_GLOBALS['field']['table']['insert'] = 'insert';
    $_GLOBALS['field']['table']['uid'] = 'nop';

	$_GLOBALS['field']['value']['active'] = 1;

	$_GLOBALS['edit-field'] = array('active', 'name','phone','call_now','current_time','delivery_date','call_time','call_time_end','tovar_list','text');
	$_GLOBALS['add-field'] = $_GLOBALS['edit-field'];

    $check_tbls_cols = array(
        'fast_baying'=>$_GLOBALS['field']['db']
    );
    check_tbl($check_tbls_cols);

	if ($op=='item' && !empty($_POST) && !empty($_GET['id'])){
		$id = abs(intval($_GET['id']));
		$fb_order = query_new('SELECT * FROM sw_fast_baying WHERE `id`='.$id.' LIMIT 1',1);
		if (!empty($fb_order)){
			$bi = unserialize(base64_decode($fb_order['basket_info']));
			if (!empty($_POST['delivery_date'])
				&& strtotime($bi['holiday']['start']) <= strtotime($_POST['delivery_date'])
				&& strtotime($bi['holiday']['end']) >= strtotime($_POST['delivery_date'])){
				query_new('UPDATE sw_fast_baying SET `amount`='.quote($bi['mark_disc_summ']).' WHERE id='.$_GET['id']);
			}else{
				query_new('UPDATE sw_fast_baying SET `amount`='.quote($bi['discount_summ']).' WHERE id='.$_GET['id']);
			}
		}
	}