<?
if (!defined('SW')) die('���� ������� �� ���������');

switch($op) {
	case 'insert': insertOrder(); break;
	default: getOrder(); break;
}

function insertOrder() {
	include_once('include/jquery/jQuery.php');
	
	if (empty($_POST['amount'])) {
		jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#my-amount"),t:"����� ������ ��������� �����",top:jQuery("#my-amount input").offset().top-270,left:130})'));
		jQuery::getResponse();
	}
	
	$amount = abs(intval($_POST['amount']));
	$message = filter(trim(in1251($_POST['message'])), 'nohtml');
	
	db_query('INSERT INTO `sw_basket` (`session`, `type`, `title`, `text`, `structure`, `price`, `amount`) VALUES ('.quote(session_id()).', '.quote('my-order').', '.quote('�������������� �����').', '.quote($message).', '.quote($message).', '.quote($amount).', '.quote($amount).')');
	
	
	jQuery::evalScript('miniCart.reloadMC(); location.href = "/basket.html";');
	jQuery::getResponse();
}

function getOrder() {
	global $_GLOBALS, $module,$cur;
	
	$data = PageData($module);
	$html = null;
	$free_del = query_new("SELECT * FROM sw_delivery_free",1);
    $del_normal = deliveres(false, 0, true);
    $del_holl = deliveres(false, 0, false);
    $free_del['info_block_text1'] = str_replace('del_h_min_price', $del_holl['del_opt']['min_price'].$cur['reduction'], $free_del['info_block_text1']);
    $free_del['info_block_text1'] = str_replace('del_min_price', $del_normal['del_opt']['min_price'].$cur['reduction'], $free_del['info_block_text1']);
    $url = ($free_del['info_block_url_text']!='')? $free_del['info_block_url_text']:'����������';
    $url = ($free_del['info_block_url_link']!='')?'<a href="'.$free_del['info_block_url_link'].'" target="_blank">'.$url.'</a>':'';
    $free_del['info_block_text2'] = str_replace('url_link', $url, $free_del['info_block_text2']);
    $popup = '<div id="ind_ord_pop" class="how_to_fill text_block " style="top:2px;">'
                            .'<div class="tb_hover_popup" id="ind_ord_pop_cont">'
                                .'<div class="triagle"></div>'
                                .'<div class="text_block_cont del_info">'
            . '<div class="bott_shad">'
            	. '<p><span class="etention">!</span></p>'
            	. $free_del['info_block_text1']
            . '</div>'
            . '<div>'. $free_del['info_block_text2']. '</div></div></div></div>';
	if (!empty($data['text'])) $html.= '<div class="mainTextInner">'.stripslashes($data['text']).'</div>';
	
	$html.= '<div class="mainTextInner list">';
	$html.= '<div class="listItem"><span class="number">1</span> <p>��������� ��� �� �������� '.$_GLOBALS['v_phone-magazin'].' ��� �������� �� '.$_GLOBALS['v_email_admin'].'</p></div>';
	$html.= '<div class="listItem"><span class="number">2</span> <p>���������� ��������� ���� ���������</p></div>';
	$html.= '<div class="listItem"><span class="number">3</span> <p>������� ��������� ������ ������</p></div>';
	$html.= '<div class="listItem"><span class="number">4</span> <p>������� ����� ������ ��������������� ������ � ���������� � ���������� ������, ����� ������ "�������� �����":</p></div>';
	$html.= '</div>';
	
	$html.= '<div id="individual-order" class="mainTextInner" style="background: none"><form action="/individual-order/op=insert" onsubmit="formSubmit(this); return false;">'
            . '<div id="my-amount" >�������� ����� <span class="red">*</span>:<br /><input name="amount" type="text" />'.$popup.'</div>'
            . '<p>���� ���������:<br /><textarea name="message" cols="1" rows="1" class="bigTextarea"></textarea></p><p class="floatRight"><input type="submit" value="�������� �����" onclick="ga(\'send\', \'event\', \'send7UA\', \'Click7UA\'); yaCounter10652362.reachGoal(\'ya7send\'); return true;" /></p><p><span class="red hint">����, ���������� *, ����������� ��� ����������.</span></p></form></div>';
	
	$_GLOBALS['title'] = $data['title'];
	$_GLOBALS['text'] = $html;
	$_GLOBALS['html_title'] = !empty($data['html_title']) ? $data['html_title'] : $data['title'].' - '.$_GLOBALS['v_sitename'];
	if (!empty($data['description'])) $_GLOBALS['description'] = $data['description'];
	if (!empty($data['keywords'])) $_GLOBALS['keywords'] = $data['keywords'];
	$_GLOBALS['template'] = 'default';
	$_GLOBALS['section'] = 'individual-order';
}

?>