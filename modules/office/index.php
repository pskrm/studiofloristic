<?php

if (!defined('SW')) die('���� ������� �� ���������');

include_once('include/jquery/jQuery.php');

switch($op)
{
	case 'change-password':  // ������� ��������� ������ � ������ �������� ������������
        changePassword();
        break;

	case 'edit': // ������� ��������� ������������ ������ ������������ (���, email, �������, �����, ���, � �������� �� �������?)
        editData();
        break;

	case 'order': // ������� ������ ������ ������� �������
        initOrder();
        break;

	case 'my':  // ��� ������� ������ ������� ��� �������� ������� ��������
        myOffice();
        break;

	case 'activation': // ��������� ������ (��� ���������� �����) � ������ �������� ������������
        Activation();
        break;
    case 'cupcards':
        getCardsData(); // ���������� ������ �����
	default:
        jQuery::getResponse();
        break;
}

function initOrder()
{
	global $_GLOBALS, $cur;

	if (!_IS_USER || empty($_GET['id']))
    {
        jQuery::getResponse(); // ������ ��������, ���� ������������ �� �����������
    }

	$id = abs(intval($_GET['id']));
	$uid = abs(intval($_SESSION['uid']));

    // ������� ������ ������� ��� ����� ������� (��� ��� ���� �����?) ���� ��������
	$r = db_query('SELECT UNIX_TIMESTAMP(`insert`) as `insert`, `itogo`,`foto`,`markup`, `count`, `dostavka`, `discount`, `delivery` FROM `sw_orders` WHERE `id` = '.quote($id).' AND `uid` = '.quote($uid));

	if (mysql_num_rows($r) == 0)
    {
        jQuery::getResponse();
    }

	$o = mysql_fetch_array($r, MYSQL_ASSOC);

    $box = array(
        0=>array('text'=>'�����','price'=>'<span class="text_color_orange">���������</span>'),
        1=>array('text'=>'�������� �����','price'=>ceil($_GLOBALS['v_execution-1']/$cur['value']).$cur['reduction']),
        2=>array('text'=>'����������� ��������','price'=>ceil($_GLOBALS['v_execution-2']/$cur['value']).$cur['reduction']),
        3=>array('text'=>'���������� ��������','price'=>ceil($_GLOBALS['v_execution-3']/$cur['value']).$cur['reduction'])
    );

    $html = '<span class="close">&nbsp;</span><span class="h1_alt">����� �� '.date('d.m.Y', $o['insert']).'</span>';

	$r = db_query('SELECT i.`cid`, i.`title`, i.`type`, i.`structure`, i.`price`, i.`amount`, i.`box`, i.`vase`, i.`count`, c.`image`, s.`url` FROM `sw_orders_item` as i LEFT JOIN `sw_catalog` as c ON i.`cid` = c.`id` LEFT JOIN `sw_catalog_section` as s ON s.`cid` = i.`cid` AND s.`sid` = i.`sid` WHERE i.`oid` = '.quote($id).' AND i.`type`!='.  quote('gift'));

	while ($c = mysql_fetch_array($r, MYSQL_ASSOC))
    {
		switch ($c['type'])
        {
			case 'my-bouquet': $image = '/i/myBouquet-70.gif'; break;
			case 'my-order': $image = '/i/individ.jpg'; break;
			default:
				if (!empty($c['image']))
                {
					$image = explode(':', $c['image']);
					$image = '/files/catalog/'.$c['cid'].'/w70_'.$image[0];
				}
                else
                {
                    $image = null;
                }
			    break;
		}

        $a_target = 'target="_blank"';
        if (empty($c['url']))
        {
            $c['url'] = 'javascript: void(0);';
            $a_target = '';
        }

        if (empty($c['price']))
        {
            $price_text = '<span class="text_color_orange">���������</span>';
        }
        else
        {
            $price_text = ceil($c['price']/$cur['value']).'<span class="reduction">'.$cur['reduction'].'</span>';
        }

		$html.= '<div class="cartItem blockWithTopLine"><table width="100%" cellspacing="0" cellpadding="0" border="0"><tbody><tr>'
                . '<td width="70" rowspan="2"><a href="'.$c['url'].'" '.$a_target.'><img src="'.$image.'"></a></td>'
                . '<td class="tableText"><a '.$a_target.' class="name" href="'.$c['url'].'">'.$c['title'].'</a></td>'
                . '<td style="text-align:right;min-width:60px;"><span class="tableCost" style="font-weight:normal;">'.$price_text.'</span></td></tr>'
                . '<tr><td colspan="2">';
		$html.= '<table style="width:100%;"><tr><td>';

		if (!empty($c['structure']))
        {
			$html.= '<div class="rightInfo"><div class="title">������:</div><table width="100%" cellspacing="0" cellpadding="0" border="0"><tbody>';
			$br = strstr($c['structure'], '<br />') ? '<br />' : "\n";
			foreach (explode($br, $c['structure']) as $l)
            {
				$line = explode(':', $l);
				if (!empty($line[0]) && !empty($line[1]))
                {
					$html.= '<tr><td><b>'.$line[0].':</b></td><td>'.$line[1].'</td></tr>';
				}
                elseif (!empty($l))
                {
					$html.= '<tr><td colspan="2"><b>'.$l.'</b></td></tr>';
				}
			}

			$html.= '</tbody></table></div>';
		}

        $html.='</td><td align="right"><table class="office_addes">';

        if (!empty($c['box']))
        {
            $html.= '<tr><td align="right"><p>'.$box[$c['box']]['text'].'</p></td><td class="value"><p>+ '.$box[$c['box']]['price'].'</p></td></tr>';
        }

        if (!empty($c['vase']))
        {
            $html.= '<tr><td align="right"><p>����</p></td><td class="value"><p>+ '.ceil($_GLOBALS['v_vase-price']/$cur['value']).$cur['reduction'].'</p></td></tr>';
        }

        if (!empty($o['markup']))
        {
            $html.= '<tr><td align="right"><p>������� �� 1 ��. ������</p></td><td class="value"><p>+ '.$o['markup'].'%</p></td></tr>';
            $mark = intval($o['markup']);
        }
        else
        {
            $mark = 0;
        }

        if (!empty($o['discount']))
        {
            $html.= '<tr><td align="right"><p>������ �� 1 ��. ������</p></td><td class="value"><p>- '.$o['discount'].'%</p></td></tr>';
            $disc = intval($o['discount']);
        }
        else
        {
            $disc = 0;
        }

        $amount = ceil(ceil(ceil($c['amount']*(1+$mark/100))*(1-$disc/100))/$cur['value']);
        $html .='</table></td></tr>'
                . '<tr><td class="tableCost" colspan="2" style="text-align:right;"><span class="gray">�����:</span> '
                .($c['count'] > 1 ? '<span style="font-weight:normal;">'.$c['count'].'��. x '.$amount.'<span class="reduction">'.$cur['reduction'].'</span> = </span>' : null)
                .($c['count']*$amount).'<span class="reduction">'.$cur['reduction']
            .'</span></td></tr></table>';

		$html.= '</tbody></table></div>';
	}

    $html.= gift_in_basket($id);

    $del_arr = array(
        1 => '� �������� ����',
        3 => '�� ���� (�� '.$_GLOBALS['v_start_over_MKAD'].' ��)',
        4 => '�� ���� '
    );

    if (intval($o['delivery'])>4)
    {
        $dest = intval($o['delivery'])-4;
        $del_arr[4] = $del_arr[4].'('.$dest.' ��)';
        $o['delivery'] = 4;
    }

    $services = array();
    if (!empty($o['dostavka']))
    {
        $services[] = '<span class="gray" style="fint-size:12px; display:block;">�������� '.(($o['delivery']!=2 && isset($del_arr[$o['delivery']]))?$del_arr[$o['delivery']]:'').' + '.ceil($o['dostavka']/$cur['value']).$cur['reduction'].'</span>';
    }

    if (!empty($o['foto']) && !empty($_GLOBALS['v_photo']))
    {
        $services[] = '<span class="gray" style="fint-size:12px;  display:block;">���������� ������� �������� + '.ceil($_GLOBALS['v_photo']/$cur['value']).$cur['reduction'].'</span>';
    }

    $add_text = (count($services)>0)? ' (� ������ �����)'.  hover_popup('office_services', $services):'';
	$html.= '<div class="bigBlockWithButtonOk">'
                . '<table style="height:45px;"><tr><td style="vertical-align:middle;">'
                    . '<div class="bigBlocktext">����� ������'.$add_text.':</div>'
                    . '</td><td style="vertical-align:middle;">'
                    . '<div class="bigBlockCost"><span id="itog-items">'.$o['itogo'].'<span class="reduction">'.$cur['reduction'].'</span></span></div>'
                . '</td></tr></table>'
                . '</div>';

	jQuery('div#popupHistory')->html(inUTF8($html));
    jQuery::evalScript('text_blocks.hoverPop()');
	jQuery::evalScript('order.load()');
	jQuery::getResponse();
}

/**
 * @param array $c - ������ � ������� � ������� � ������
 * @return string - html-��� ��� ����������� �������
 */
function gift_item( $c = array() )
{
    $html = '';
    $image = '';
    if (is_array($c) && isset($c['id']))
    {
        if (!empty($c['image']))
        {
            $images = explode(':', $c['image']);
            $image .= '<img src="/files/gifts/'.$c['gid'].'/w70_'.$images[0].'" />';
        }

        $html.= '<div id="item-'.$c['id'].'" class="cartItem blockWithTopLine"><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>'
                . '<td width="70" rowspan="2">'. $image.'</td>'
                . '<td class="tableText"><span class="name">'.$c['title'].'</span></td>'
                . '<td class="tableCost"><span class="text_color_orange">���������</span></td>'
                . '</tr>'
                . '<tr><td colspan="3"><table class="sw-box" style="margin-bottom:10px;"><tr><td><div class="rightInfo"><div class="title">�������</div><div class="gift_text">'.$c['text'].'</div></td>'
                . '</tr></table></td></tr></table></div>';
    }
    return $html;
}


/**
 * @param int $id - ID ������
 * @param bool $ret - ��������, ����, ��������������� ������ �������� ������. true - ��������� �������, false - html-���
 * @return array|bool|string
 */
function gift_in_basket($id = 0, $ret = false)
{
    $html = '';
    $gifts = query_new('SELECT b.`id`, g.`id` as `gid`, g.`title`, g.`text`, g.`image` FROM `sw_orders_item` as b LEFT JOIN `sw_gifts` as g ON b.`cid` = g.`id` WHERE b.`oid` = '.quote($id).' AND b.`type` = '.quote('gift'));

    if ($ret === true)
    {
        return $gifts;
    }

    foreach ($gifts as $gift)
    {
        $html .= gift_item($gift);
    }
    return $html;
}

/**
 *  ������� ������������ POST-������ �� ��������� ������ �� ������� �������� ������������������� ������������
 */
function changePassword()
{
	if (!_IS_USER)
    {
        jQuery::getResponse();
    }

    //
	if (empty($_POST['new-pass']) || empty($_POST['ret-pass']) || empty($_POST['old-pass']))
    {
		if (empty($_POST['new-pass'])) jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#new-pass"),t:"���� �������� ����� ������� ����� ���������",top:550,left:200})'));
		if (empty($_POST['ret-pass'])) jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#ret-pass"),t:"���� ���������� ���� ����� ���������",top:590,left:200})'));
		if (empty($_POST['old-pass'])) jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#old-pass"),t:"���� �������� ������ ������� ����� ���������",top:550,left:480})'));
		jQuery::getResponse();
	}

	$newPass = filter(trim(in1251($_POST['new-pass'])), 'nohtml');
	$retPass = filter(trim(in1251($_POST['ret-pass'])), 'nohtml');
	$oldPass = md5(filter(trim(in1251($_POST['old-pass'])), 'nohtml'));
	$md5Pass = md5($newPass);
	$uid = abs(intval($_SESSION['uid']));

	if ($newPass != $retPass)
    {
		jQuery::evalScript('office.result(8)');
		jQuery::getResponse();
	}

	if (dbone('COUNT(`id`)', '`id` = '.quote($uid).' AND `pass` = '.quote($oldPass), 'users') != 1)
    {
		jQuery::evalScript('office.result(9)');
		jQuery::getResponse();
	}

    // ������ �������� � �������� ����, ��� �����, ������� ��. �� ������� ���, ��� ������ ����� �� ���� �� �����
	db_query('UPDATE `sw_users` SET `pass` = '.quote($md5Pass).' WHERE `id` = '.quote($uid));

	$u = query_new('SELECT * FROM sw_users WHERE id = '.quote($uid),1);
	$_SESSION['user'] = array('id'=>$uid, 'login'=>$_SESSION['user']['login'], 'pass'=>$md5Pass, 'activation'=>$u['activation']);
    $time = time()+(60*60*24*365);
    if (is_string($u['activation']) && $u['activation']!=='' && $u['activation']!==null)
    {
        setcookie('member', $u['activation'], $time,'/',_CROSS_DOMAIN);
    }
	jQuery::evalScript('office.result(10)');
	jQuery::getResponse();
}

/**
 * ������� ������������ POST-������ �� ��������� ������ ������ ������������
 */

function editData()
{
	if (!_IS_USER)
    {
        jQuery::getResponse();
    }

	$error = false;

	if (empty($_POST['name']))
    {
		$error = true;
		jQuery('div#field-name')->append(inUTF8('<span class="alert">�� ������� ���</span>'));
	}
    else
    {
        jQuery::evalScript('jQuery("div#field-name span").remove()');
    }

	if (empty($_POST['email']))
    {
		$error = true;
		jQuery('div#field-email')->append(inUTF8('<span class="alert">�� ������ �-mail</span>'));
	}
    else
    {
        jQuery::evalScript('jQuery("div#field-email span").remove()');
    }

	if ($error)
    {
        jQuery::getResponse();
    }

	$email = filter(trim(in1251($_POST['email'])), 'nohtml');
	$name = filter(trim(in1251($_POST['name'])), 'nohtml');
	$patronymic = filter(trim(in1251($_POST['patronymic'])), 'nohtml');
	$family = filter(trim(in1251($_POST['family'])), 'nohtml');
	$phone = filter(trim(in1251($_POST['phone'])), 'nohtml');
	$sex = abs(intval($_POST['sex']));
	$index = filter(trim(in1251($_POST['index'])), 'nohtml');
	$country = filter(trim(in1251($_POST['country'])), 'nohtml');
	$city = filter(trim(in1251($_POST['city'])), 'nohtml');
	$street = filter(trim(in1251($_POST['street'])), 'nohtml');
	$home = filter(trim(in1251($_POST['home'])), 'nohtml');
	$housing = filter(trim(in1251($_POST['housing'])), 'nohtml');
	$apartment = filter(trim(in1251($_POST['apartment'])), 'nohtml');
	$structure = filter(trim(in1251($_POST['structure'])), 'nohtml');
	$hkey = filter(trim(in1251($_POST['hkey'])), 'nohtml');
	$subscription = !empty($_POST['subscription']) ? 1 : 0;
	$sms = !empty($_POST['sms']) ? 1 : 0;
	$sex = $sex > 1 ? 2 : 1;
	$username = trim($family.' '.$name.' '.$patronymic);



    // ������������� ������������ email, ���������� � ������ ������
	if (dbone('COUNT(`id`)', '`email` = '.quote($email).' AND `id` != '.quote($_SESSION['uid']), 'users') != 0)
    {
		jQuery::evalScript('office.result(6)');
		jQuery::getResponse();
	}

    // ������������� ������������ ������ ��������, ���������� � ������ ������
    // ��������� ���������, ��� ��� �������. ������ +7 � 8 �� ������, ���� ��� ���� �������, � ������ �� ������, ������� � 9...
    $sysPhone = preg_replace("/[^0-9]/", '', $phone);
    $first_letter  = substr($sysPhone, 0, 1);
    if (in_array($first_letter, array('7', '8')))
    {
        $sysPhone = substr($sysPhone, 1);
    }

    // ��������� � �� ����������� ��������
    // ���� ���� - ����������� �����
    if (dbone('COUNT(`id`)', "`sys-phone` LIKE '%".$sysPhone."%' AND `id` != ".quote($_SESSION['uid']), 'users') != 0)
    {
        jQuery::evalScript('office.result(61)');
        jQuery::getResponse();
    }

	db_query('UPDATE `sw_users` SET `email` = '.quote($email).', `username` = '.quote($username).', `name` = '.quote($name).', `patronymic` = '.quote($patronymic).', `family` = '.quote($family).', `phone` = '.mysql_real_escape_string($phone).', `sys-phone` = \''.mysql_real_escape_string($sysPhone).'\', `sex` = '.quote($sex).', `index` = '.quote($index).', `country` = '.quote($country).', `city` = '.quote($city).', `street` = '.quote($street).', `home` = '.quote($home).', `housing` = '.quote($housing).', `apartment` = '.quote($apartment).', `structure` = '.quote($structure).', `hkey` = '.quote($hkey).', `subscription` = '.quote($subscription).', `sms` = '.quote($sms).', `distribution` = '.quote($subscription).' WHERE `id` = '.quote($_SESSION['uid']));
    $_SESSION['user']['email'] = $email;
	jQuery::evalScript('office.result(5)');
	jQuery::getResponse();
}

/**
 * ������� ������������ ������� ��� ������� ��������
 */

function myOffice()
{
	global $_GLOBALS;

	if (!_IS_USER)
    {
        HeaderPage(_PAGE_NO_ACCESS); // ������ ������ ��� ���������������� �������������
    }

    // ID ��������������� ������������ �������� � $_SESSION['id'], ��������� ������ �� sw_users
	$r = db_query('SELECT `email`, `name`, `family`, `patronymic`, `sex`, `phone`, `subscription`, `sms`, `index`, `city`, `home`, `housing`, `country`, `street`, `structure`, `hkey`, `apartment` FROM `sw_users` WHERE `id` = '.quote($_SESSION['uid']));

	if (mysql_num_rows($r) == 0)
    {
        HeaderPage(_PAGE_ERROR404); // ��� ������������ - ������ 404
    }

	$u = mysql_fetch_array($r, MYSQL_ASSOC);

	$sex = array(1=>'�������', 2=>'�������');
   
	$html= '<div class="createOrderBlock"><form action="/office/op=edit" onsubmit="formSubmit(this); return false;"><div class="regBlock floatLeft borderRight"><span class="h3_alt">������ ������</span>';
	$html.= '<div id="field-name">���*:<br /><input name="name" type="text" value="'.htmlspecialchars(stripslashes($u['name']), ENT_COMPAT | ENT_HTML401, 'cp1251').'" /></div>';
	$html.= '<div>��������:<br /><input name="patronymic" type="text" value="'.htmlspecialchars(stripslashes($u['patronymic']), ENT_COMPAT | ENT_HTML401, 'cp1251').'" /></div>';
	$html.= '<div>�������:<br /><input name="family" type="text" value="'.htmlspecialchars(stripslashes($u['family']), ENT_COMPAT | ENT_HTML401, 'cp1251').'" /></div>';
	$html.= '<div id="field-phone">�������:<br /><input name="phone" type="text" value="'.htmlspecialchars(stripslashes($u['phone']), ENT_COMPAT | ENT_HTML401, 'cp1251').'" /></div>';
	$html.= '<div id="field-email">E-mail*:<br /><input name="email" type="text" value="'.htmlspecialchars(stripslashes($u['email']), ENT_COMPAT | ENT_HTML401, 'cp1251').'" /></div>';
	$html.= '<div style="padding:5px 0px 7px 0px">��� ���:';
	foreach ($sex as $k=>$v)
    {
        $html.= '<label><input type="radio" name="sex" value="'.$k.'"'.($u['sex'] != $k ? null : ' checked').' /> '.$v.'</label>';
    }
	$html.= '</div>';
	$html.= '<div style="padding:6px 0;"><input type="checkbox" name="sms" value="1"'.(!empty($u['sms']) ? ' checked' : null).' /> ����������� �� SMS-����������</div>';
	$html.= '<div style="padding:6px 0;"><input type="checkbox" name="subscription" value="1"'.(!empty($u['subscription']) ? ' checked' : null).' /> ����������� �� ������� (e-mail)</div>';
    $html.= '</div>';
	$html.= '<div class="floatRight"><a id="bg_color_style_logout" href="javascript:login.exit()">�����</a></div>';

    $html.= '<div class="regBlock floatLeft"><span class="h3_alt">�����</span>';

	$html.= '<div>������:<br /><input name="index" type="text" value="'.(!empty($u['index']) ? htmlspecialchars(stripslashes($u['index']), ENT_COMPAT | ENT_HTML401, 'cp1251') : null).'" /></div>';
	$html.= '<div>������:<br /><input name="country" type="text" value="'.htmlspecialchars(stripslashes($u['country']), ENT_COMPAT | ENT_HTML401, 'cp1251').'" /></div>';
	$html.= '<div>�����:<br /><input name="city" type="text" value="'.htmlspecialchars(stripslashes($u['city']), ENT_COMPAT | ENT_HTML401, 'cp1251').'" /></div>';
	$html.= '<div>�����:<br /><input name="street" type="text" value="'.htmlspecialchars(stripslashes($u['street']), ENT_COMPAT | ENT_HTML401, 'cp1251').'" /></div>';
	$html.= '<div class="smallCOB"><span>���:<br /><input name="home" type="text" value="'.htmlspecialchars(stripslashes($u['home']), ENT_COMPAT | ENT_HTML401, 'cp1251').'" /></span><span>������:<br /><input name="housing" type="text" value="'.htmlspecialchars(stripslashes($u['housing'])).'" /></span></div>';
	$html.= '<div class="smallCOB"><span>��������:<br /><input name="apartment" type="text" value="'.htmlspecialchars(stripslashes($u['apartment']), ENT_COMPAT | ENT_HTML401, 'cp1251').'" /></span><span>��������:<br /><input name="structure" type="text" value="'.htmlspecialchars(stripslashes($u['structure'])).'" /></span><span>��� ��������:<br /><input name="hkey" type="text" value="'.htmlspecialchars(stripslashes($u['hkey'])).'" /></span></div>';
	$html.= '</div><div class="clear"></div><p><input type="submit" value="���������" /></p>';
	$html.= '</div></form>';

	$html.= '<form id="change-password" action="/office/op=change-password" onsubmit="formSubmit(this); return false;">
                <div class="changePassword blockWithTopLine cardActiv">
                    <div><span class="h2_alt">��������� ������</span></div>
                    <div class="regBlock floatLeft borderRight">
                        <div id="new-pass">������� ����� ������:<br /><input name="new-pass" type="password" /></div>
                        <div id="ret-pass">��������� ����:<br /><input name="ret-pass" type="password" /></div>
                    </div>
                    <div id="old-pass" class="regBlock floatLeft">
                        <div>������� ������ ������:<br /><input name="old-pass" type="password" /></div>
                    </div>
                    <div class="clear"></div>
                    <p><input name="11" type="submit" value="�������� ������" /></p>
                </div>
            </form>';

	$html.= '<div class="skidki cardActiv blockWithTopLine">
            <span class="h2_alt">��������� ���������� ����� / ������</span>
            <form id="cardActiv" action="/office/op=activation" onsubmit="formSubmit(this); return false;">
            <p>� ����� / ������: <input name="key" type="text"/><input type="submit" value="������������" /></p><br/>
            <span style="display:none;" id="cardResult"></span>
            </form></div>';

	$html.= '<div class="lCabBlock yourOrder blockWithTopLine"><span class="h2_alt">���� ������ � ������������� ������</span>';

    // ��������� ���������� � �����
/*
    $uid = abs(intval($_SESSION['uid'])); // ID ������������
    $result = db_query('SELECT * FROM sw_cards WHERE `user-id` = '.$uid.' LIMIT 1');
    if (mysql_num_rows($result) != 0)
    {
        $card = mysql_fetch_array($result, MYSQL_ASSOC);

        $r = db_query('SELECT `discount` FROM `sw_users` WHERE `id` = '.quote($_SESSION['uid']));
        $user = mysql_fetch_array($r, MYSQL_ASSOC);

        if ($card['value'] >= $user['discount'])
        {
            $html.= '<span class="h3_alt">��������� ������ � ������� '.$card['value'].'% �� ����� � '.$card['title'].'</span>';
        }
        elseif($user['discount'] > $card['value'])
        {
            $html.= '<span class="h3_alt">��������� ������ � ������� '.$user['discount'].'</span>';
        }
    }
    else
    {
        if ((isset($_SESSION['cupon'])))
        {
            if (intval($_SESSION['cupon']) > 0)
            {
                $html.= '<span class="h3_alt">����������� ����� �� ������� '.$_SESSION['cupon'].'% </span>';
            }
        }
    }
*/
    // ��������� �� �� ������ ����� ������� � ������� �������
	$r = db_query('SELECT `id`, `amount`, `recipient`, UNIX_TIMESTAMP(`insert`) as `insert` FROM `sw_orders` WHERE `uid` = '.quote($_SESSION['uid']).' AND `status` = 1 ORDER BY `id` DESC');

	if (mysql_num_rows($r) != 0)
    {
		$summ = 0;
		$html.= '<script>office.cards();</script><table width="100%" border="0" cellspacing="0" cellpadding="0" class="data"><tbody><tr><th>���� ������</th><th>�����</th><th>����������</th></tr>';

		while ($o = mysql_fetch_array($r, MYSQL_ASSOC))
        {
			$html.= '<tr><td><a href="javascript:order.view('.$o['id'].')">'.date('d.m.Y � H:i', $o['insert']).'</a></td><td>'.$o['amount'].'�</td><td style="text-align:left;">'.$o['recipient'].'</td></tr>';
			$summ+= $o['amount']; // ��������� ����� �����
		}

        // ������, �� ����� ������ �� ����� ������������
		$r = db_query('SELECT `value`, `min` FROM `sw_discounts` WHERE `min` > '.quote($summ).' ORDER BY `min` ASC LIMIT 1');
		$d = mysql_fetch_array($r, MYSQL_ASSOC);

		$r = db_query('SELECT `discount` FROM `sw_users` WHERE `id` = '.quote($_SESSION['uid']));
		$u = mysql_fetch_array($r, MYSQL_ASSOC);

		$ost = $d['min'] - $summ;

		$html.= '</tbody></table><table border="0" cellpadding="10" cellspacing="0" class="gray" width="100%"><tbody><tr><td>����� �����: <span>'.$summ.'�</span></td><td>���� ������������� ������: <span>'.getIncrementalDiscount($_SESSION['uid']) .'%</span></td><td>'.($d['value'] > 0 ? '�� ��������� ������ '.$d['value'].'%: <span>'.$ost.'�</span>' : null).'</td></tr></tbody></table>';
	}
    else
    {
		$html.= '<span class="h3_alt">�� ��� ������ �� ��������.</span>';
	}

	$html.= '</div>';

	$time = array();
    for($i = 0; $i < 25; $i++)
    {
        $time[$i] = str_pad($i, 2, '0', STR_PAD_LEFT).':00'; // ������ ����� ��� ����������� ������
    }

	$html.= '<div class="lCabBlock blockWithTopLine"><div id="alerts-list">';

	$r = db_query('SELECT a.`id`, a.`name`, a.`family`, UNIX_TIMESTAMP(a.`datetime`) as `alert`, a.`text`, r.`title` FROM `sw_alerts` as a LEFT JOIN `sw_reminder` as r ON a.`cause` = r.`id` WHERE a.`uid` = '.quote($_SESSION['uid']));

	$html.= '<span class="h2_alt">�����������</span><div class="gray" style="padding-bottom:8px;">��� ���� ��� �� ��� ��������� sms ����������� ��� ����� �������� ����� ���������� ��������. ����������� �� e-mail ����� ��������� �������������.</div>';

	if (mysql_num_rows($r) != 0)
    {
		while ($a = mysql_fetch_array($r, MYSQL_ASSOC))
        {
            $html.= '<div class="napomLi"><span class="name">'.(trim($a['name'].' '.$a['family'])).'</span><span class="date">'.date('d.m.Y', $a['alert']).'</span><span class="info">'.(!empty($a['title']) ? '<b>'.$a['title'].':</b> ' : null).$a['text'].'</span><a href="/alerts/op=remove&id='.$a['id'].'" class="closeLi" title="�������">&nbsp;</a></div>';
        }
	}

	$html.= '</div><p></p><span class="h2_alt">�������� �����������</span><form action="/alerts/op=insert" onsubmit="formSubmit(this); return false;" class="addNap">';
	$html.= '<p id="aName">���*:<br /><input name="name" type="text" /></p>';
	$html.= '<p class="dateTime"><span id="aDate">����*:<br /><input type="text" class="datepicker" name="date"></span><span>�����*:<br /><select name="time">';
	foreach ($time as $k=>$v) $html.= '<option value="'.$k.'"'.($k != 9 ? null : ' selected').'>'.$v.'</option>';
	$html.= '</select></span></p>';
	$html.= '<p>�����:<br /><select name="cause"><option value="0">�� �������</option>';

	$r = db_query('SELECT `id`, `title` FROM `sw_reminder` WHERE `active` = 1 ORDER BY `title` ASC');
	while ($p = mysql_fetch_array($r, MYSQL_ASSOC)) $html.= '<option value="'.$p['id'].'">'.$p['title'].'</option>';

	$html.= '</select></p>';
	$html.= '<p id="aText">���� ���������*: <span id="message-char">�������� ��������: <font>30</font></span><br /><textarea onkeyup="reminder.count(this)" name="text" cols="1" rows="1" class="bigTextarea"></textarea></p>';
	$html.= '<p class="floatRight"><input type="submit" value="���������" /></p>';
	$html.= '<div class="clear"></div></form></div><script type="text/javascript">alerts.init()</script>';

	$_GLOBALS['title'] = '������ �������';
	$_GLOBALS['text'] = $html;
	$_GLOBALS['html_title'] = $_GLOBALS['title'].' - '.$_GLOBALS['v_sitename'];
	$_GLOBALS['template'] = 'office';
}

/**
 *  ������� ������������ POST-������ �� ��������� ������ ��� ����� � ������ ��������
 *  ����� (�����) �������������
 */
function Activation()
{
    global  $_GLOBALS; // ��� �������� ������������ ������
  //  intval($_GLOBALS['v_max-discount'])
    $retArray = array(); // ������ ��� �������� ������������, ����� �������� ����������� ���� �����/����� ������������
    // ������������ �� ����������� - �� ��������
	if (!_IS_USER)
    {
		jQuery::evalScript('office.result(1)');
		jQuery::getResponse();
	}

    // �� ������ ��� �����/������ - �� ��������
	if (empty($_POST['key']))
    {
		jQuery::evalScript('office.result(2)');
		jQuery::getResponse();
	}

	$key = filter(trim(in1251($_POST['key'])), 'nohtml'); // ��� �����
	$uid = abs(intval($_SESSION['uid'])); // ID ������������

	$r = db_query('SELECT `discount` FROM `sw_users` WHERE `id` = '.mysql_real_escape_string($uid));
	if (mysql_num_rows($r) == 0)
    {
        jQuery::getResponse();
    }
	$u = mysql_fetch_array($r, MYSQL_ASSOC); // ��������� ������ ������������

    // ������ ����� �� ���������� ���� ($key), ������� �������, �� �� ������������?
    // ���� ����� �����, �� ����������
	$r = db_query('SELECT `id`, `value` FROM `sw_cards` WHERE `title` = \''.mysql_real_escape_string($key).'\' AND `active` = 1 AND `activation` = 0 LIMIT 1');
	if (mysql_num_rows($r) != 0)
    {
		$card = mysql_fetch_array($r, MYSQL_ASSOC);

		if (($u['discount'] < $card['value']) && ($card['value'] <= intval($_GLOBALS['v_max-discount'])) )
        {
			db_query('UPDATE `sw_users` SET `discount` = '.mysql_real_escape_string($card['value']).' WHERE `id` = '.mysql_real_escape_string($uid));
		}
		else
        {
            jQuery::evalScript('office.result(11, {"disc_uno" : "'.$u['discount'] .'%", "disc_dos" : "'.$card['value'].'%"})');
            jQuery::getResponse();
        }

		db_query('UPDATE `sw_cards` SET `activation` = 1, `activation-date` = NOW(), `user-discount` = '.quote($u['discount']).', `user-id` = '.quote($uid).' WHERE `id` = '.mysql_real_escape_string($card['id']));
        $retArray = array('type'=> 'card', 'val' => $card['value'], 'name' => htmlspecialchars($key, ENT_COMPAT | ENT_HTML401, 'cp1251'), 'dt' => date("d-m-Y H:i") );
        $_SESSION['user']['cupcards'] = json_encode($retArray);
		jQuery::evalScript(inUTF8('initSysDialog("������ '.$card['value'].'% ������������."); jQuery("form#cardActiv").resetForm(); jQuery("span#discount-value").html("'.$card['value'].'%"); office.cards();'));
		jQuery::getResponse();
	}
    // ���� �� �����, �� ������ �� ����������

    // ���� �������� �����, � ������ ���������, ������������� �� �� ����� ���� �������������
	$r = db_query('SELECT c.`id`, c.`value`, c.`count`, IF(a.`uid` IS NULL, 0, 1) as `active` FROM `sw_coupons` as c LEFT JOIN `sw_coupons_users` as a ON a.`cid` = c.`id` AND a.`uid` = '.mysql_real_escape_string($uid).' WHERE c.`title` = \''.mysql_real_escape_string($key).'\' AND c.`active` = 1 LIMIT 1');

	if (mysql_num_rows($r) != 0)
    {
		$d = mysql_fetch_array($r, MYSQL_ASSOC);

		if (!empty($d['active']))
        {
			jQuery::evalScript(inUTF8('initSysDialog("�� ��� ������������ ���� �����"); jQuery("form#cardActiv").resetForm();'));
			jQuery::getResponse();
		}

        // count - ����� ���������� �������, ������� ����������� ��� ���������, value - ��������� ������� ������
        // ������������ ����� ������: ������ ���, ���������� ��������� ������� � ������� ������ � ���������

		if ($d['count'] > 0) // ���� ������ ��� ��������
        {
            // ���� ������ ������������ ������ ������ ������, �������� ������ ������������ ����� ��������� ������
			if (($u['discount'] < $d['value']) && ($d['value'] <= intval($_GLOBALS['v_max-discount']) ) )
            {
				db_query('UPDATE `sw_users` SET `discount` = '.mysql_real_escape_string($d['value']).' WHERE `id` = '.mysql_real_escape_string($uid));
				$_SESSION['cupon'] = $d['value'];
			}
            else
            {
                jQuery::evalScript('office.result(11, {"disc_uno" : "'.$u['discount'] .'%", "disc_dos" : "'.$d['value'].'%"})');
                jQuery::getResponse();
            }

            // ������ ������ � ������� ������������� ������� (couponID, userID)
			db_query('INSERT INTO `sw_coupons_users` (`cid`, `uid`) VALUES ('.mysql_real_escape_string($d['id']).', '.mysql_real_escape_string($uid).')');

            // ��������� ���������� �������� ������� �� 1
			db_query('UPDATE `sw_coupons` SET `count` = `count` - 1 WHERE `id` = '.mysql_real_escape_string($d['id']));
            $retArray = array('type'=> 'cupon', 'val' => $d['value'], 'name' => htmlspecialchars($key, ENT_COMPAT | ENT_HTML401, 'cp1251'), 'dt' => date("d-m-Y H:i") );
            $_SESSION['user']['cupcards'] = json_encode($retArray);
			jQuery::evalScript(inUTF8('initSysDialog("������ '.$d['value'].'% ������������."); jQuery("form#cardActiv").resetForm(); jQuery("span#discount-value").html("'.$d['value'].'%"); office.cards();'));
			jQuery::getResponse();
		}
        else // ������ ������� �� ������
        {
			jQuery::evalScript(inUTF8('initSysDialog("������ �� ���� ����� �����������")'));
			jQuery::getResponse();
		}
	}

	jQuery::evalScript('office.result(3)');
	jQuery::getResponse();
}

function getCardsData(){

    if (_IS_USER) {
        $uid = isset($_SESSION['user']['id']) ?$_SESSION['user']['id'] : $_SESSION['uid'];
        if ($uid <= 0){
            echo json_encode(array());
            die;
        }

        if (isset($_SESSION['user']['cupcards'])) {
            if (count($_SESSION['user']['cupcards']) > 1) {
                echo json_encode($_SESSION['user']['cupcards']);
            } else {
                // ��������� �������� ������ ����� �� ��
                $sql = sprintf("SELECT * FROM sw_cards WHERE `user-id` = %d AND active = 1 AND activation > 0 LIMIT 0,1;", mysql_real_escape_string($uid) );
                $r = db_query($sql);
                if (mysql_num_rows($r) != 0){
                    // ����� ����
                    $d = mysql_fetch_array($r, MYSQL_ASSOC);
                    $retArray = array('type'=> 'card', 'val' => $d['value'], 'name' => htmlspecialchars($d['title'], ENT_COMPAT | ENT_HTML401, 'cp1251'), 'dt' => date("d-m-Y H:i", strtotime($d['activation-date'])) );
                    echo json_encode($retArray);
                }else{
                    //������� �����
                    $sql = sprintf("SELECT * FROM sw_coupons_users cu LEFT JOIN sw_coupons c ON cu.`CID` = c.`id` WHERE cu.`UID` = %d AND c.`active` = 1 ORDER BY ID DESC LIMIT 0,1;", mysql_real_escape_string($uid) );
                    $r = db_query($sql);
                    if (mysql_num_rows($r) != 0){
                        $d = mysql_fetch_array($r, MYSQL_ASSOC);
                        $retArray = array('type'=> 'cupon', 'val' => $d['value'], 'name' => htmlspecialchars($d['title'], ENT_COMPAT | ENT_HTML401, 'cp1251'), 'dt' => date("d-m-Y H:i", strtotime($d['insert'])) );
                        echo json_encode($retArray);
                    }else{
                        // ��� ������� ����/�������
                        echo json_encode(array());
                    }
                }

            }
        } else {
            $sql = sprintf("SELECT * FROM sw_cards WHERE `user-id` = %d AND active = 1 AND activation > 0 LIMIT 0,1;", mysql_real_escape_string($uid) );
            $r = db_query($sql);
            if (mysql_num_rows($r) != 0){
                // ����� ����
                $d = mysql_fetch_array($r, MYSQL_ASSOC);
                $retArray = array('type'=> 'card', 'val' => $d['value'], 'name' => htmlspecialchars($d['title'], ENT_COMPAT | ENT_HTML401, 'cp1251'), 'dt' => date("d-m-Y H:i", strtotime($d['activation-date'])) );
                echo json_encode($retArray);
            }else{
                //������� �����
                $sql = sprintf("SELECT * FROM sw_coupons_users cu LEFT JOIN sw_coupons c ON cu.`CID` = c.`id` WHERE cu.`UID` = %d AND c.`active` = 1 ORDER BY ID DESC LIMIT 0,1;", mysql_real_escape_string($uid) );
                $r = db_query($sql);
                if (mysql_num_rows($r) != 0){
                    $d = mysql_fetch_array($r, MYSQL_ASSOC);
                    $retArray = array('type'=> 'cupon', 'val' => $d['value'], 'name' => htmlspecialchars($d['title'], ENT_COMPAT | ENT_HTML401, 'cp1251'), 'dt' => date("d-m-Y H:i", strtotime($d['insert'])) );
                    echo json_encode($retArray);
                }else{
                    // ��� ������� ����/�������
                    echo json_encode(array());
                }
            }
        }
    }else{
        echo json_encode(array());
    }
    die;
}

