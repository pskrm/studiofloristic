<?
if (!defined('SW')) die('���� ������� �� ���������');

include_once('include/jquery/jQuery.php');

switch($op) {
	case 'change-password': changePassword(); break;
	case 'edit': editData(); break;
	case 'order': initOrder(); break;
	case 'my': myOffice(); break;
	case 'activation': Activation(); break;
	default: jQuery::getResponse(); break;
}

function initOrder() {
	global $_GLOBALS;
	
	if (!_IS_USER || empty($_GET['id'])) jQuery::getResponse();
	$id = abs(intval($_GET['id']));
	$uid = abs(intval($_SESSION['uid']));
	
	$r = db_query('SELECT UNIX_TIMESTAMP(`insert`) as `insert`, `itogo`, `count`, `dostavka` FROM `sw_orders` WHERE `id` = '.quote($id).' AND `uid` = '.quote($uid));
	if (mysql_num_rows($r) == 0) jQuery::getResponse();
	$o = mysql_fetch_array($r, MYSQL_ASSOC);
	
	$box = array(
		0=>'����� (���������)',
		1=>'�������� ����� (+ 50 ���.)',
		2=>'����������� �������� (+ 200 ���.)',
		3=>'���������� �������� (+ 250 ���.)',
	);
	
	$html = '<span class="close">&nbsp;</span><h1>����� �� '.date('d.m.Y', $o['insert']).'</h1>';
	
	$r = db_query('SELECT i.`cid`, i.`title`, i.`type`, i.`structure`, i.`price`, i.`box`, i.`vase`, i.`count`, c.`image`, s.`url` FROM `sw_orders_item` as i LEFT JOIN `sw_catalog` as c ON i.`cid` = c.`id` LEFT JOIN `sw_catalog_section` as s ON s.`cid` = i.`cid` AND s.`sid` = i.`sid` WHERE i.`oid` = '.quote($id));
	while ($c = mysql_fetch_array($r, MYSQL_ASSOC)) {
		switch ($c['type']) {
			case 'my-bouquet': $image = '/i/myBouquet-70.gif'; break;
			case 'my-order': $image = '/i/individ.jpg'; break;
			default: {
				if (!empty($c['image'])) {
					$image = explode(':', $c['image']);
					$image = '/files/catalog/'.$c['cid'].'/w70_'.$image[0];
				} else $image = null;
			} break;
		}
		
		$html.= '<div class="cartItem blockWithTopLine"><table width="100%" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td width="70"><a href="'.$c['url'].'"><img src="'.$image.'"></a></td><td class="tableText"><a class="name" href="'.$c['url'].'">'.$c['title'].'</a>';
		
		if (!empty($c['structure'])) {
			$html.= '<br /><div class="rightInfo"><div class="title">������:</div><table width="100%" cellspacing="0" cellpadding="0" border="0"><tbody>';
			$br = strstr($c['structure'], '<br />') ? '<br />' : "\n";
			foreach (explode($br, $c['structure']) as $l) {
				$line = explode(':', $l);
				if (!empty($line[0]) && !empty($line[1])) {
					$html.= '<tr><td><b>'.$line[0].':</b></td><td>'.$line[1].'</td></tr>';
				} elseif (!empty($l)) {
					$html.= '<tr><td colspan="2"><b>'.$l.'</b></td></tr>';
				}
			}
			$html.= '</tbody></table></div>';
			if (!empty($c['box'])) $html.= '<p>+ '.$box[$c['box']].'</p>';
			if (!empty($c['vase'])) $html.= '<p>+ ���� '.$_GLOBALS['v_vase-price'].' �.</p>';
		}
		
		$html.= '</td><td class="tableCost">'.($c['count'] > 1 ? $c['count'].' x ' : null).$c['price'].' �</td></tr></tbody></table></div>';
	}
	
	$html.= '<div class="bigBlockWithButtonOk"><div class="bigBlocktext">����� ������'.(!empty($o['dostavka']) ? ' (� ������ �������� + '.$o['dostavka'].'�)' : null).':</div><div class="bigBlockCost"><span id="itog-items">'.$o['itogo'].'</span>�</div></div>';
	
	jQuery('div#popupHistory')->html(inUTF8($html));
	jQuery::evalScript('order.load()');
	jQuery::getResponse();
}

function changePassword() {
	if (!_IS_USER) jQuery::getResponse();
	
	if (empty($_POST['new-pass']) || empty($_POST['ret-pass']) || empty($_POST['old-pass'])) {
		if (empty($_POST['new-pass'])) jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#new-pass"),t:"���� �������� ����� ������� ����� ���������",top:550,left:200})'));
		if (empty($_POST['ret-pass'])) jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#ret-pass"),t:"���� ���������� ���� ����� ���������",top:590,left:200})'));
		if (empty($_POST['old-pass'])) jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#old-pass"),t:"���� �������� ������ ������� ����� ���������",top:550,left:480})'));
		jQuery::getResponse();
	}
	
	$newPass = filter(trim(in1251($_POST['new-pass'])), 'nohtml');
	$retPass = filter(trim(in1251($_POST['ret-pass'])), 'nohtml');
	$oldPass = md5(filter(trim(in1251($_POST['old-pass'])), 'nohtml'));
	$md5Pass = md5($newPass);
	$uid = abs(intval($_SESSION['uid']));
	
	if ($newPass != $retPass) {
		jQuery::evalScript('office.result(8)');
		jQuery::getResponse();
	}
	
	if (dbone('COUNT(`id`)', '`id` = '.quote($uid).' AND `pass` = '.quote($oldPass), 'users') != 1) {
		jQuery::evalScript('office.result(9)');
		jQuery::getResponse();
	}
	
	db_query('UPDATE `sw_users` SET `pass` = '.quote($md5Pass).', `password` = '.quote($newPass).' WHERE `id` = '.quote($uid));
	
	$_SESSION['user'] = array('id'=>$uid, 'login'=>$_SESSION['user']['login'], 'pass'=>$md5Pass);
	
	jQuery::evalScript('office.result(10)');
	jQuery::getResponse();
}

function editData() {
	if (!_IS_USER) jQuery::getResponse();
	$error = false;
	
	if (empty($_POST['name'])) {
		$error = true;
		jQuery('div#field-name')->append(inUTF8('<span class="alert">�� ������� ���</span>'));
	} else jQuery::evalScript('jQuery("div#field-name span").remove()');
	
	if (empty($_POST['email'])) {
		$error = true;
		jQuery('div#field-email')->append(inUTF8('<span class="alert">�� ������ E-mail</span>'));
	} else jQuery::evalScript('jQuery("div#field-email span").remove()');
	
	if ($error) jQuery::getResponse();
	
	$email = filter(trim(in1251($_POST['email'])), 'nohtml');
	$name = filter(trim(in1251($_POST['name'])), 'nohtml');
	$patronymic = filter(trim(in1251($_POST['patronymic'])), 'nohtml');
	$family = filter(trim(in1251($_POST['family'])), 'nohtml');
	$phone = filter(trim(in1251($_POST['phone'])), 'nohtml');
	$sex = abs(intval($_POST['sex']));
	$index = filter(trim(in1251($_POST['index'])), 'nohtml');
	$country = filter(trim(in1251($_POST['country'])), 'nohtml');
	$city = filter(trim(in1251($_POST['city'])), 'nohtml');
	$street = filter(trim(in1251($_POST['street'])), 'nohtml');
	$home = filter(trim(in1251($_POST['home'])), 'nohtml');
	$housing = filter(trim(in1251($_POST['housing'])), 'nohtml');
	$apartment = filter(trim(in1251($_POST['apartment'])), 'nohtml');
	$structure = filter(trim(in1251($_POST['structure'])), 'nohtml');
	$hkey = filter(trim(in1251($_POST['hkey'])), 'nohtml');
	$subscription = !empty($_POST['subscription']) ? 1 : 0;
	$sms = !empty($_POST['sms']) ? 1 : 0;
	$sex = $sex > 1 ? 2 : 1;
	$username = trim($family.' '.$name.' '.$patronymic);
	
	$sysPhone = preg_replace("/[^0-9]/", '', $phone);
	
	if (dbone('COUNT(`id`)', '`email` = '.quote($email).' AND `id` != '.quote($_SESSION['uid']), 'users') != 0) {
		jQuery::evalScript('office.result(6)');
		jQuery::getResponse();
	}
	
	db_query('UPDATE `sw_users` SET `email` = '.quote($email).', `username` = '.quote($username).', `name` = '.quote($name).', `patronymic` = '.quote($patronymic).', `family` = '.quote($family).', `phone` = '.quote($phone).', `sys-phone` = \''.mysql_real_escape_string($sysPhone).'\', `sex` = '.quote($sex).', `index` = '.quote($index).', `country` = '.quote($country).', `city` = '.quote($city).', `street` = '.quote($street).', `home` = '.quote($home).', `housing` = '.quote($housing).', `apartment` = '.quote($apartment).', `structure` = '.quote($structure).', `hkey` = '.quote($hkey).', `subscription` = '.quote($subscription).', `sms` = '.quote($sms).', `distribution` = '.quote($subscription).' WHERE `id` = '.quote($_SESSION['uid']));
	
	jQuery::evalScript('office.result(5)');
	jQuery::getResponse();
}

function myOffice() {
	global $_GLOBALS;
	
	if (!_IS_USER) HeaderPage(_PAGE_NO_ACCESS);
	
	$r = db_query('SELECT `email`, `name`, `family`, `patronymic`, `sex`, `phone`, `subscription`, `sms`, `index`, `city`, `home`, `housing`, `country`, `street`, `structure`, `hkey`, `apartment` FROM `sw_users` WHERE `id` = '.quote($_SESSION['uid']));
	if (mysql_num_rows($r) == 0) HeaderPage(_PAGE_ERROR404);
	$u = mysql_fetch_array($r, MYSQL_ASSOC);
	
	$sex = array(1=>'�������', 2=>'�������');
	
	$html = '<div class="createOrderBlock"><form action="/office/op=edit" onsubmit="formSubmit(this); return false;"><div class="regBlock floatLeft borderRight"><h3>������ ������</h3>';
	$html.= '<div id="field-name">���*:<br /><input name="name" type="text" value="'.htmlspecialchars(stripslashes($u['name'])).'" /></div>';
	$html.= '<div>��������:<br /><input name="patronymic" type="text" value="'.htmlspecialchars(stripslashes($u['patronymic'])).'" /></div>';
	$html.= '<div>�������:<br /><input name="family" type="text" value="'.htmlspecialchars(stripslashes($u['family'])).'" /></div>';
	$html.= '<div id="field-phone">�������:<br /><input name="phone" type="text" value="'.htmlspecialchars(stripslashes($u['phone'])).'" /></div>';
	$html.= '<div id="field-email">E-mail*:<br /><input name="email" type="text" value="'.htmlspecialchars(stripslashes($u['email'])).'" /></div>';
	$html.= '<div style="padding:5px 0px 7px 0px">��� ���:';
	foreach ($sex as $k=>$v) $html.= '<label><input type="radio" name="sex" value="'.$k.'"'.($u['sex'] != $k ? null : ' checked').' /> '.$v.'</label>';
	$html.= '</div>';
	$html.= '<div style="padding:6px 0;"><input type="checkbox" name="sms" value="1"'.(!empty($u['sms']) ? ' checked' : null).' /> ����������� �� sms ����������</div>';
	$html.= '<div style="padding:6px 0;"><input type="checkbox" name="subscription" value="1"'.(!empty($u['subscription']) ? ' checked' : null).' /> ����������� �� ������� (e-mail)</div>';
	
	$html.= '</div><div class="regBlock floatLeft"><h3>�����</h3>';
	
	$html.= '<div>������:<br /><input name="index" type="text" value="'.(!empty($u['index']) ? htmlspecialchars(stripslashes($u['index'])) : null).'" /></div>';
	$html.= '<div>������:<br /><input name="country" type="text" value="'.htmlspecialchars(stripslashes($u['country'])).'" /></div>';
	$html.= '<div>�����:<br /><input name="city" type="text" value="'.htmlspecialchars(stripslashes($u['city'])).'" /></div>';
	$html.= '<div>�����:<br /><input name="street" type="text" value="'.htmlspecialchars(stripslashes($u['street'])).'" /></div>';
	$html.= '<div class="smallCOB"><span>���:<br /><input name="home" type="text" value="'.htmlspecialchars(stripslashes($u['home'])).'" /></span><span>������:<br /><input name="housing" type="text" value="'.htmlspecialchars(stripslashes($u['housing'])).'" /></span></div>';
	$html.= '<div class="smallCOB"><span>��������:<br /><input name="apartment" type="text" value="'.htmlspecialchars(stripslashes($u['apartment'])).'" /></span><span>��������:<br /><input name="structure" type="text" value="'.htmlspecialchars(stripslashes($u['structure'])).'" /></span><span>��� ��������:<br /><input name="hkey" type="text" value="'.htmlspecialchars(stripslashes($u['hkey'])).'" /></span></div>';
	$html.= '</div><div class="clear"></div><p><input type="submit" value="���������" /></p>';
	$html.= '</div></form>';
	
	$html.= '<form id="change-password" action="/office/op=change-password" onsubmit="formSubmit(this); return false;"><div class="changePassword blockWithTopLine cardActiv"><div><h2>��������� ������</h2></div><div class="regBlock floatLeft borderRight"><div id="new-pass">������� ����� ������:<br /><input name="new-pass" type="password" /></div><div id="ret-pass">��������� ����:<br /><input name="ret-pass" type="password" /></div></div><div id="old-pass" class="regBlock floatLeft"><div>������� ������ ������:<br /><input name="old-pass" type="password" /></div></div><div class="clear"></div><p><input name="11" type="submit" value="�������� ������" /></p></div></form>';
	
	$html.= '<div class="skidki cardActiv blockWithTopLine"><h2>��������� ���������� ����� / ������</h2><form id="cardActiv" action="/office/op=activation" onsubmit="formSubmit(this); return false;"><p>� ����� / ������: <input name="key" type="text"/><input type="submit" value="������������" /></p></form></div>';
	
	$html.= '<div class="lCabBlock yourOrder blockWithTopLine"><h2>���� ������ � ������������� ������</h2>';
	$r = db_query('SELECT `id`, `amount`, `recipient`, UNIX_TIMESTAMP(`insert`) as `insert` FROM `sw_orders` WHERE `uid` = '.quote($_SESSION['uid']).' AND `status` = 1 ORDER BY `id` DESC');
	if (mysql_num_rows($r) != 0) {
		$summ = 0;
		$html.= '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="data"><tbody><tr><th>���� ������</th><th>�����</th><th>� ������</th></tr>';
		while ($o = mysql_fetch_array($r, MYSQL_ASSOC)) {
			$html.= '<tr><td><a href="javascript:order.view('.$o['id'].')">'.date('d.m.Y � H:i', $o['insert']).'</a></td><td>'.$o['amount'].'�</td><td style="text-align:left;">'.$o['recipient'].'</td></tr>';
			$summ+= $o['amount'];
		}
		
		$r = db_query('SELECT `value`, `min` FROM `sw_discounts` WHERE `min` > '.quote($summ).' ORDER BY `min` ASC LIMIT 1');
		$d = mysql_fetch_array($r, MYSQL_ASSOC);
		
		$r = db_query('SELECT `discount` FROM `sw_users` WHERE `id` = '.quote($_SESSION['uid']));
		$u = mysql_fetch_array($r, MYSQL_ASSOC);
		
		$ost = $d['min'] - $summ;
		
		$html.= '</tbody></table><table border="0" cellpadding="10" cellspacing="0" class="gray" width="100%"><tbody><tr><td>����� �����: <span>'.$summ.'�</span></td><td>���� ������: <span>'.$u['discount'].'%</span></td><td>'.($d['value'] > 0 ? '�� ��������� ������ '.$d['value'].'%: <span>'.$ost.'�</span>' : null).'</td></tr></tbody></table>';
	} else {
		$html.= '<h3>�� ��� ������ �� �������.</h3>';
	}
	$html.= '</div>';
	
	$time = array(
		0=> '00:00',
		1=> '01:00',
		2=> '02:00',
		3=> '03:00',
		4=> '04:00',
		5=> '05:00',
		6=> '06:00',
		7=> '07:00',
		8=> '08:00',
		9=> '09:00',
		10=> '10:00',
		11=> '11:00',
		12=> '12:00',
		13=> '13:00',
		14=> '14:00',
		15=> '15:00',
		16=> '16:00',
		17=> '17:00',
		18=> '18:00',
		19=> '19:00',
		20=> '20:00',
		21=> '21:00',
		22=> '22:00',
		23=> '23:00'
	);
	
	$html.= '<div class="lCabBlock blockWithTopLine"><div id="alerts-list">';
	$r = db_query('SELECT a.`id`, a.`name`, a.`family`, UNIX_TIMESTAMP(a.`datetime`) as `alert`, a.`text`, r.`title` FROM `sw_alerts` as a LEFT JOIN `sw_reminder` as r ON a.`cause` = r.`id` WHERE a.`uid` = '.quote($_SESSION['uid']));
	$html.= '<h2>�����������</h2><div class="gray" style="padding-bottom:8px;">��� ���� ��� �� ��� ��������� sms ����������� ��� ����� �������� ����� ���������� ��������. ����������� �� Email � ����� ������ ���� ���������.</div>';
	if (mysql_num_rows($r) != 0) {
		
		while ($a = mysql_fetch_array($r, MYSQL_ASSOC)) $html.= '<div class="napomLi"><span class="name">'.(trim($a['name'].' '.$a['family'])).'</span><span class="date">'.date('d.m.Y', $a['alert']).'</span><span class="info">'.(!empty($a['title']) ? '<b>'.$a['title'].':</b> ' : null).$a['text'].'</span><a href="/alerts/op=remove&id='.$a['id'].'" class="closeLi" title="�������">&nbsp;</a></div>';
	}
	$html.= '</div><p></p><h2>�������� �����������</h2><form action="/alerts/op=insert" onsubmit="formSubmit(this); return false;" class="addNap">';
	$html.= '<p id="aName">���*:<br /><input name="name" type="text" /></p>';
	$html.= '<p class="dateTime"><span id="aDate">����*:<br /><input type="text" class="datepicker" name="date"></span><span>�����*:<br /><select name="time">';
	foreach ($time as $k=>$v) $html.= '<option value="'.$k.'"'.($k != 9 ? null : ' selected').'>'.$v.'</option>';
	$html.= '</select></span></p>';
	$html.= '<p>�����:<br /><select name="cause"><option value="0">�� �������</option>';
	
	$r = db_query('SELECT `id`, `title` FROM `sw_reminder` WHERE `active` = 1 ORDER BY `title` ASC');
	while ($p = mysql_fetch_array($r, MYSQL_ASSOC)) $html.= '<option value="'.$p['id'].'">'.$p['title'].'</option>';
	
	$html.= '</select></p>';
	$html.= '<p id="aText">���� ���������*: <span id="message-char">�������� ��������: <font>30</font></span><br /><textarea onkeyup="reminder.count(this)" name="text" cols="1" rows="1" class="bigTextarea"></textarea></p>';
	$html.= '<p class="floatRight"><input type="submit" value="���������" /></p>';
	$html.= '<div class="clear"></div></form></div><script type="text/javascript">alerts.init()</script>';
	
	$_GLOBALS['title'] = '������ �������';
	$_GLOBALS['text'] = $html;
	$_GLOBALS['html_title'] = $_GLOBALS['title'].' - '.$_GLOBALS['v_sitename'];
	$_GLOBALS['template'] = 'office';
}

function Activation() {
	if (!_IS_USER) {
		jQuery::evalScript('office.result(1)');
		jQuery::getResponse();
	}
	
	if (empty($_POST['key'])) {
		jQuery::evalScript('office.result(2)');
		jQuery::getResponse();
	}
	
	$key = filter(trim(in1251($_POST['key'])), 'nohtml');
	$uid = abs(intval($_SESSION['uid']));
	
	$r = db_query('SELECT `discount` FROM `sw_users` WHERE `id` = '.mysql_real_escape_string($uid));
	if (mysql_num_rows($r) == 0) jQuery::getResponse();
	$u = mysql_fetch_array($r, MYSQL_ASSOC);
	
	$r = db_query('SELECT `id`, `value` FROM `sw_cards` WHERE `title` = \''.mysql_real_escape_string($key).'\' AND `active` = 1 AND `activation` = 0 LIMIT 1');
	if (mysql_num_rows($r) != 0) {
		$d = mysql_fetch_array($r, MYSQL_ASSOC);
		
		if ($u['discount'] < $d['value']) {
			db_query('UPDATE `sw_users` SET `discount` = '.mysql_real_escape_string($d['value']).' WHERE `id` = '.mysql_real_escape_string($uid));
		}
		
		db_query('UPDATE `sw_cards` SET `activation` = 1, `activation-date` = NOW(), `user-discount` = '.quote($u['discount']).', `user-id` = '.quote($uid).' WHERE `id` = '.mysql_real_escape_string($d['id']));
		
		jQuery::evalScript(inUTF8('initSysDialog("������ ������ '.$d['value'].'% �����������."); jQuery("form#cardActiv").resetForm(); jQuery("span#discount-value").html("'.$d['value'].'%")'));
		jQuery::getResponse();
	}
	
	$r = db_query('SELECT `id`, `value`, `count` FROM `sw_coupons` WHERE `title` = \''.mysql_real_escape_string($key).'\' AND `active` = 1 LIMIT 1');
	if (mysql_num_rows($r) != 0) {
		$d = mysql_fetch_array($r, MYSQL_ASSOC);
		
		if ($d['count'] > 0) {
			if ($u['discount'] < $d['value']) {
				db_query('UPDATE `sw_users` SET `discount` = '.mysql_real_escape_string($d['value']).' WHERE `id` = '.mysql_real_escape_string($uid));
			}
			
			db_query('UPDATE `sw_coupons` SET `count` = `count` - 1 WHERE `id` = '.mysql_real_escape_string($d['id']));
			
			jQuery::evalScript(inUTF8('initSysDialog("������ ������ '.$d['value'].'% �����������."); jQuery("form#cardActiv").resetForm(); jQuery("span#discount-value").html("'.$d['value'].'%")'));
			jQuery::getResponse();
		} else {
			jQuery::evalScript(inUTF8('initSysDialog("������ �� ���� ����� �����������")'));
			jQuery::getResponse();
		}
	}
	
	jQuery::evalScript('office.result(3)');
	jQuery::getResponse();
}

?>