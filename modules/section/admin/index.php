<?
	if (!defined('_SWS_ADMIN_CORE')) exit();

	$_GLOBALS['create_dir'] = false;
	$_GLOBALS['add'] = true;
	$_GLOBALS['search'] = true;
	$_GLOBALS['on-page'] = 20;
	$_GLOBALS['tiny'] = false;
	$_GLOBALS['date'] = false;
	$_GLOBALS['nesting'] = true;

	$_GLOBALS['field']['title']['id'] = 'ID';
	$_GLOBALS['field']['title']['sid'] = '������/���������';
	$_GLOBALS['field']['title']['active'] = '���./����.';
	$_GLOBALS['field']['title']['pos'] = '�������';
	$_GLOBALS['field']['title']['title'] = '��������';
	$_GLOBALS['field']['title']['link'] = '��������� ���';
	$_GLOBALS['field']['title']['pach'] = '����';
	$_GLOBALS['field']['title']['items'] = '���-�� ��������� �� ��������';
	$_GLOBALS['field']['title']['basket'] = '���������� � �������';
    $_GLOBALS['field']['title']['item_list'] = '������������ ������ � �������';
    $_GLOBALS['field']['title']['filter'] = '������������ ��� ������';

	$_GLOBALS['field']['type']['sid'] = 'sid';
	$_GLOBALS['field']['type']['active'] = 'checkbox';
	$_GLOBALS['field']['type']['pos'] = 'position';
	$_GLOBALS['field']['type']['title'] = 'title';
	$_GLOBALS['field']['type']['link'] = 'text';
	$_GLOBALS['field']['type']['pach'] = 'pach';
	$_GLOBALS['field']['type']['items'] = 'text';
	$_GLOBALS['field']['type']['basket'] = 'checkbox';
    $_GLOBALS['field']['type']['item_list'] = 'section_items';
    $_GLOBALS['field']['type']['filter'] = 'checkbox';

	$_GLOBALS['field']['db']['sid'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['active'] = "int(1) NOT NULL default '0'";
	$_GLOBALS['field']['db']['pos'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['title'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['link'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['pach'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['items'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['basket'] = "int(1) NOT NULL default '0'";
	$_GLOBALS['field']['db']['insert'] = "timestamp NOT NULL default CURRENT_TIMESTAMP";
	$_GLOBALS['field']['db']['system'] = "int(1) NOT NULL default '0'";
    $_GLOBALS['field']['db']['filter'] = "int(1) NOT NULL default '0'";


	$_GLOBALS['field']['table']['id'] = 'text';
	$_GLOBALS['field']['table']['title'] = 'title';
	$_GLOBALS['field']['table']['pach'] = 'link';
    $_GLOBALS['field']['table']['filter'] = 'yes/no';

	$_GLOBALS['field']['value']['active'] = 1;
	$_GLOBALS['field']['value']['items'] = 12;

	$_GLOBALS['edit-field'] = array('active', 'basket','filter', 'sid', 'title', 'link', 'pach', 'items');
	$_GLOBALS['add-field'] = $_GLOBALS['edit-field'];

    if ($op=="item" && isset($_GET['id'])){
        $section_row = query_new('SELECT * FROM sw_section WHERE id='.$_GET['id'],1);
        if (isset($section_row['basket']) && intval($section_row['basket'])==1){
            $_GLOBALS['edit-field'][] = 'item_list';
        }
    }

    $check_tbls_cols = array(
        'catalog_section'=>array(
            'cid_pos' => "int(11) NOT NULL default '0'",
            'basket_show'=> "int(1) NOT NULL default '1'"
        ),
        'section'=>$_GLOBALS['field']['db']
    );
    check_tbl($check_tbls_cols);
?>