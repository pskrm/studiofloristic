<?
if (!defined('SW')) die('���� ������� �� ���������');

switch($op) {
	default: LoadModule(); break;
}

function LoadModule() {
	global $_GLOBALS;

	if (empty($_GET['link']) || stripos(_PAGE_ERROR404,$_GET['link'])!==false) HeaderPage(_PAGE_ERROR404);
	$link = filter(trim($_GET['link']), 'nohtml');

	$_GLOBALS['menu'] = $link;

	$r = db_query('SELECT `sid`, `title`, `text`, `html_title`, `keywords`, `description`, `link` FROM `sw_pages` WHERE `link` = '.quote($link).' AND `active` = 1');
	if (mysql_num_rows($r) == 0) HeaderPage(_PAGE_ERROR404);
	$p = mysql_fetch_array($r, MYSQL_ASSOC);

	$html = stripslashes($p['text']);

	$_GLOBALS['section'] = $link;

	switch ($link) {

		case 'payment': {
			//Breadcrumbs
		    $bcms = bcNav($p);
			$_GLOBALS['template'] = 'default';
		} break;

		case 'contact': {
			//Breadcrumbs
		    $bcms = bcNav($p);
			$_GLOBALS['template'] = 'default';
		} break;

		case 'budget-wedding': {
			$_GLOBALS['template'] = 'default';
		} break;

		case 'order-adopted':
			//[%ORDER_ID%]
			if (!empty($_SESSION['fast_bay_id'])){
				$html = str_replace('[%ORDER_ID%]',  $_SESSION['fast_bay_id'] , $html);
				$_GLOBALS['text'] = $html;
				$_SESSION['fast_bay_id'] =null;
				unset($_SESSION['fast_bay_id']);
			}

			break;
		case 'unsubscribed':
			$hash = filter(trim($_REQUEST['hash']), 'nohtml');
			if (empty($hash)){
				HeaderPage(_PAGE_ERROR404);
			}

			if (mb_strlen($hash ) != 64){
				HeaderPage(_PAGE_ERROR404);
			}

			$exists = query_new("SELECT * FROM sw_notify WHERE crc = " . quote($hash ) . " LIMIT 0,1");
			if ((!is_array($exists )) || (!isset($exists[0])) || (count($exists) == 0) ){
				HeaderPage(_PAGE_ERROR404);
			}

			$exists = $exists[0];

			if (filter_var($exists['email'], FILTER_VALIDATE_EMAIL) === false){
				HeaderPage(_PAGE_ERROR404);
			}
			$text = "<p>����� ����������� ����� <b>".$exists['email']."</b> ������� �������� �� ��������!</p>";
			$exists2 = query_new("SELECT * FROM sw_notify_unsubscribe WHERE email = " . quote($exists['email']) . " LIMIT 0,1");
			if ((is_array($exists2 )) || (isset($exists2[0])) || (count($exists2) > 0) ){
				$text = "<p>����� ����������� ����� <b>".$exists['email']."</b> ��� ��� �������� �� �������� �����: <b>".date("d-m-Y H:i:s", strtotime($exists2[0]['added']))."</b>.</p>";
			}else{
				db_query("INSERT INTO `sw_notify_unsubscribe` (`email`, `added`) VALUES  ('".$exists['email']."', NOW());");
			}

			$html = str_replace('[%UNSUBSCRIBE%]',  $text , $html);
			$_GLOBALS['template'] = 'default';
			$_GLOBALS['title'] = '���������� �� ��������';
			$_GLOBALS['text'] = $html;
			$_GLOBALS['html_title'] = '���������� �� �������� | ������ �� ����� � ������ - '.$_GLOBALS['v_sitename'];
			break;

		case 'bestsellers':

			$url = filter(trim($_SERVER['REQUEST_URI']), 'nohtml');
			$data = '';
			// ��� ������ ��������� �������, ������
			$sql = "SELECT sc.*, scs_url.`url` FROM sw_catalog sc LEFT JOIN sw_catalog_section scs ON sc.`id`=scs.`cid` LEFT JOIN sw_catalog_section scs_url ON scs.`cid`=scs_url.`cid` WHERE sc.`active`=1 AND scs_url.`first`=1 AND sc.`share`=1";

			$items = query_new($sql);
			$items= array_map("unserialize", array_unique( array_map("serialize", $items) ));
			// var_dump($items); exit();

			if (isset($_COOKIE['ban-dest-id'])){
				$bann_id = intval($_COOKIE['ban-dest-id']);
				$baner = query_new('SELECT * FROM `sw_main_baner` WHERE `active`=1 AND `id` = '.quote($bann_id).' LIMIT 1',1);
				unset($_COOKIE['ban-dest-id']);
				setcookie('ban-dest-id', null, -1, '/');
			}else{
				$baner = query_new('SELECT * FROM `sw_main_baner` WHERE `active`=1 AND `url` REGEXP '.quote($url.'$').' LIMIT 1',1);
			}


			if (!empty($baner) && stripos($baner['banner'],':')!==false){
				$imgs = explode(':',$baner['banner']);
				if (file_exists('files/main_baner/'.$baner['id'].'/'.$imgs[1])){
					$data = '<div id="baner_slider" class="ramka"><img nopin="nopin" src="/files/main_baner/'.$baner['id'].'/'.$imgs[1].'" width="100%" height="auto" /></div>'.$html;
				}
			}
			$data.= '<div class="catalog blockWithTopLine">';
			foreach ($items as $item)
			{
				$data.= dirItem($item);
			}

			$data.= '<div class="clear"></div></div>';

			$html = str_replace('[%CATALOG%]', $data , $html);
			$_GLOBALS['template'] = 'default';
			$_GLOBALS['title'] = '���� ������';
			$_GLOBALS['text'] = $html;
			$_GLOBALS['html_title'] = '���� ������ | ������ �� ����� � ������ - '.$_GLOBALS['v_sitename'];
			break;

		case 'delivery': {

			//Breadcrumbs
		    $bcms = bcNav($p);

			$delivery = query_new('SELECT * FROM `sw_delivery` WHERE `active` = 1 ORDER BY `start` ASC');
            $del_opt = query_new("SELECT * FROM `sw_delivery_ships`",1);
            $del_hol_opt = query_new('SELECT * FROM `sw_delivery_holiday_options` WHERE `active` = 1',1);
            $set_tab = preg_match('/.*\/catalog\/bouquet.*html/',$_SERVER['HTTP_REFERER']) && !empty($del_hol_opt['active'])?1:0;
            $dis_del = delivery_tabl_simple($delivery,$del_opt,'default',$set_tab);
            if (!empty($del_hol_opt['active'])) {
                $dis_del = '<ul class="sw-tab"><li id="default" class="'.($set_tab?'':'active').'">�������� � ������� ����</li><li id="holiday" class="'.(!$set_tab?'':'active').'">'.$del_hol_opt['tab'].'</li></ul>'.$dis_del;
                $delivery = query_new('SELECT * FROM `sw_delivery_holiday` WHERE `active` = 1 ORDER BY `start` ASC');
                $set_tab = empty($set_tab)?1:0;
                $dis_del .= delivery_tabl_simple($delivery,$del_hol_opt,'holiday',$set_tab);
            }
			$html = str_replace('[%DELIVERY%]', $dis_del, $html);
			$html = str_replace('[%CUR-NALIK%]', $_GLOBALS['v_pay-curer'], $html);
			/*
			 * ������� ������� ��� ����������� ��������
			 * */
			$_GLOBALS['js_onload'][] = '
			 
			';
            $delSliders = '
            
            <style type="text/css">
            .ramka {
            margin: 0 0 0 0 !important;
            }
			</style>
            
            <div id="baner_slider" class="ramka swiper-container">
            <div class="swiper-wrapper">
            <div class="swiper-slide">
            <a href="javascript:;"><img nopin="nopin" src="/files/delivery_page/1.jpg"/></a>
            </div>
            <div class="swiper-slide">
            <a href="javascript:;"><img nopin="nopin" src="/files/delivery_page/2.jpg"/></a>
            </div>
            <div class="swiper-slide">
            <a href="javascript:;"><img nopin="nopin" src="/files/delivery_page/3.jpg"/></a>
            </div>
            <div class="swiper-slide">
            <a href="javascript:;"><img nopin="nopin" src="/files/delivery_page/4.jpg"/></a>
            </div>
            </div>
            <div class="swiper-button-next"></div><div class="swiper-button-prev"></div>
            </div>
            <div class="swiper-pagination"></div>
            <script type="text/javascript">
            var swiper = new Swiper(".swiper-container", {
                          pagination: ".swiper-pagination",
                          paginationClickable: true,
                          autoplay: 5000,
                          autoplayDisableOnInteraction: false,
                          nextButton: ".swiper-button-next",
                          prevButton: ".swiper-button-prev",
                       });
            </script>
            ';
            $html = str_replace('[%DELIVERYSLIDERS%]', $delSliders , $html);
		} break;

		case 'discounts': {
		    // �������� ������

			//Breadcrumbs
		    $bcms = bcNav($p);

            $sql = "SELECT sc.*, scs_url.`url` FROM sw_catalog sc LEFT JOIN sw_catalog_section scs ON sc.`id`=scs.`cid` LEFT JOIN sw_catalog_section scs_url ON scs.`cid`=scs_url.`cid` WHERE sc.`active`=1 AND scs_url.`first`=1 AND sc.`share`=1";
            $items = query_new($sql);
            $items= array_map("unserialize", array_unique( array_map("serialize", $items) ));
            // var_dump($items); exit();
            $ahtml= '<div class="h1_alt">�����</div><div class="catalog blockWithTopLine">';
            foreach ($items as $item)
            {
                $ahtml .= dirItem($item);
            }
            $ahtml.= '<div class="clear"></div></div>';

			$html = str_replace('[%ACTIVATION%]', '<div class="skidki cardActiv blockWithTopLine"><span class="h2_alt">��������� ���������� ����� / ������</span><form action="/office/op=activation" onsubmit="formSubmit(this); return false;"><p>� ����� / ������: <input name="key" type="text"/><input type="submit" value="������������" /></p>'.(_IS_USER ? null : '<p>��������� ����� �������� ������ ������������������ �������������. ���������� �����������������. ���� �� ��� ���������������� - ������ �� ����.</p>').'</form></div>', $html);
			$html = str_replace('[%ACTIONS%]', $ahtml , $html);
			$_GLOBALS['template'] = 'default';
		} break;

	}

	$_GLOBALS['pages'] = array($link);
	$sid = $p['sid'];
	while ($sid != 0) {
		$r = db_query('SELECT `id`, `sid`, `link` FROM `sw_pages` WHERE `id` = '.$sid);
		$ps = mysql_fetch_array($r, MYSQL_ASSOC);
		$sid = $ps['sid'];
		$_GLOBALS['pages'][] = $ps['link'];
	}
	$_GLOBALS['pages'] = array_reverse($_GLOBALS['pages']);

	$_GLOBALS['menu'] = $_GLOBALS['pages'][0];

	if (isset($bcms)) {
		$_GLOBALS['bcms'] = $bcms;
	}

	$_GLOBALS['title'] = $p['title'];
	$_GLOBALS['text'] = $html;
	$_GLOBALS['html_title'] = !empty($p['html_title']) ? $p['html_title'] : $p['title'].' - '.$_GLOBALS['v_sitename'];
	if (!empty($p['description'])) $_GLOBALS['description'] = $p['description'];
	if (!empty($p['keywords'])) $_GLOBALS['keywords'] = $p['keywords'];
}

function bcNav($p)
{
	global $_GLOBALS;

	$res = '<div class="bc_cont" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="'.$_GLOBALS['abs_url_host'].'" class=""><span itemprop="title">�������</span></a><span class="text_color_orange"> <i class="fontico-angle-right"></i></span></div><div class="bc_cont" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">'.$p['title'].'</span></div>';

	return $res;
}
?>