<?
if (!defined('SW')) die('���� ������� �� ���������');

switch($op) {
    case 'fsearch': fast_search(); break;
	default: initSearch(); break;
}
function search_sql($s='',$t=0,$b=false,$l=false){
    $srat_arr = array(
        1=>'IF(scid.`section_title` LIKE \'%'.quote_like($s).'%\',1,0)',
        3=>'IF(sc.`structure` LIKE \'%'.quote_like($s).'%\',3,0)',
        5=>'IF(sc.`title` LIKE \'%'.quote_like($s).'%\',5,0)'
    );
    $srat = isset($srat_arr[$t]) ? $srat_arr[$t] : implode('+', $srat_arr);
    $fealds = 'sc.`id`,scs.`sid`, sc.`title`, sc.`structure`, sc.`art`, scs.`url`, sc.`views`, sc.`image`,sc.`price`,sc.`price-old`, sc.`hit`, sc.`season`,sc.`share`, IF(sc.`art`='.quote($s).',20,0)+'.$srat.' AS `srat`';
    $tbls = ($t==1||$t==0?"SELECT scs.`cid` AS `id`,ss.`title` AS `section_title` FROM `sw_section` ss LEFT JOIN `sw_catalog_section` scs ON ss.`id` = scs.`sid` WHERE ss.`active`=1 AND ss.`title` LIKE '%".quote_like($s)."%'":'').
            (!isset($srat_arr[$t])?' UNION ':'').
            ($t>1||$t==0?" SELECT sc.`id`,NULL AS `section_title` FROM `sw_catalog` sc WHERE sc.`art` = ".quote($s).
                ($t==5||$t==0?" OR sc.`title` LIKE '%".quote_like($s)."%' ":'').
                ($t==3||$t==0?" OR sc.`structure` LIKE '%".quote_like($s)."%' ":'')
            :'');
    $sql = 'SELECT '.$fealds.' FROM ('.$tbls.') scid LEFT JOIN sw_catalog sc ON sc.`id`=scid.`id` LEFT JOIN sw_catalog_section scs ON scs.`cid` = scid.`id` WHERE sc.`active`=1 AND scs.`first`=1 GROUP BY scid.`id` ORDER BY `srat` DESC, sc.`views` DESC';
    $sql_count = 'SELECT COUNT(*) as `count` FROM ('.$sql.') as ctbl';
    if ($b!==false && $l!==false){
        $lim = ' LIMIT '.$b.','.$l;
    }
    $sql_lim = $sql.(empty($lim)?'':$lim);
    return array('sql'=>$sql,'sql_lim'=>$sql_lim,'sql_count'=>$sql_count);
}
function fast_search(){
    global $cur;
    include_once('include/jquery/jQuery.php');

    $search = !empty($_GET['query']) ? in1251(strtolower(filter(trim($_GET['query']), 'nohtml'))) : null;
    if (!empty($search)) {
        if (!preg_match('#^[ ��"�_a-zA-Z�-��-�0-9-]+$#i', $search)) {
            jQuery::evalScript(inUTF8('initSysDialog("� ������� ������������ ����������� �������")'));
        } else {
            include_once('system/stem.php');
            $s = preg_replace('/ +/', ' ', $search);
            $t = !empty($_REQUEST['search_type'])?intval($_REQUEST['search_type']):0;
            $sql = search_sql($s,$t,0,5);

            $products = query_new($sql['sql_lim']);
            $count = query_new($sql['sql_count'],1);
            $count = empty($count['count'])?0:$count['count'];
            if (is_array($products)) {
                $collspan = $count? 'colspan="4"' :'';
                $html = '<div class="fast_search_popup"><table>'
                    . '<tr><td '.$collspan.' align="right">'
                        . '<div class="butn_red" onclick="fastsearch.closeList(\'.fast_search_popup\')">'
                        . '<span class="cross"></span>'
                        . '<span class="text">������� �����</span>'
                    . '</div></td></tr>';
                if (count($products)>0){
                    $b = query_new('SELECT `start`, `end`, `title`, `center` FROM `sw_holiday` WHERE `active` = "1"',1);
                    $html	.= '<tr class="search_tbl_header bott_bord">'
                                . '<td colspan="2"><span>�����</span></td>'
                                . '<td><span>����</span></td><td></td></tr>';
                    foreach ($products as $key=>$c){
                        if (!empty($c['image'])) {
                            $image = '';
                            $images = explode(':', $c['image']);
                            foreach ($images as $key=>$img){
                                $style = ($key==0)?'':' style="display:none;"';
                                $image .= '<a href="/files/catalog/'.$c['id'].'/w600_'.$img.'" '.$style.' rel="fastsearch_item_'.$c['id'].'" class="fastsearch_fancybox"><img src="/files/catalog/'.$c['id'].'/w70_'.$img.'" /></a>';
                            }
                        }
                        $b_text = ($b===false)? '' : '<div><span class="holl_price" onmouseover="fastsearch.hoverHolly(this,true)" onmouseout="fastsearch.hoverHolly(this,false)">'
                                . ceil(($c['price'])/(100)*($b['center'])+($c['price'])).'<span class="reduction">'.$cur['reduction'].'</span></span>'
                                . '<div class="tb_hover_popup">'
                                    . '<div class="triagle"></div>'
                                    . '<div class="text_block_cont"><p>'.implode('</p><p>',explode('|',$b['title'])).'</p></div>'
                                . '</div>'
                            . '</div>';
                        $cons_list = '';
                        if ($c['structure']!=''){
                            $cons_list = preg_split("/\r\n|\n|\r/", $c['structure']);
                            foreach ($cons_list as $cl_key=>$cl_row){
                                $cl_tmp = explode(':',$cl_row);
                                if (count($cl_tmp)>1){
                                    $cl_tmp[0] = '<b>'.$cl_tmp[0].'</b>';
                                    $cons_list[$cl_key] = implode(':',$cl_tmp);
                                }else{
                                    $cons_list[$cl_key] = '<b>'.$cl_row.'</b>';
                                }

                            }
                            $cons_list = '<div class="consist_list"><span class="consist">������</span>'
                                . '<div>'.implode("\n",$cons_list).'</div>'
                                . '</div>';
                        }
                        $title = '<span>'.implode('</span><span>',explode(' ',$c['title'])).'</span>';
                        $html .= '<tr class="bott_bord cat_item">'
                                    . '<td class="fast_search_res_img"><div>'.$image.'</div></td>'
                                    . '<td class="description">'
                                        . '<div><a href="'.$c['url'].'" target="_blank">'.$title.'</a></div>'
                                        . $cons_list
                                    .'</td>'
                                    . '<td class="fast_search_prices">'
                                        . '<span class="current_price">'.(countSizes($c['id']) > 1 ? '�� ' : '').ceil($c['price']/$cur['value']).'<span class="reduction">'.$cur['reduction'].'</span></span>'
                                        . ((intval($c['price-old'])>0)?'<span class="old_price">'.ceil($c['price-old']/$cur['value']).'<span class="reduction">'.$cur['reduction'].'</span></span>':'')
                                        . $b_text
                                    . '</td>'
                                    . '<td class="fast_search_actions">'
                                        . '<div><span class="butn_green" onclick="basket.insert('.$c['id'].','.$c['sid'].',0)">� �������</span></div>'
                                        . '<div><span class="quickOrder" onclick="fast.init('.$c['id'].','.$c['sid'].')">������� �����</span></div>'
                                    . '</td>'
                            . '</tr>';
                    }
                    $html.= '<tr class="bottom_btn"><td '.$collspan.' align="right"><span class="butn_green" onclick="fastsearch.showConsist()">����� ������� �������: <b>'.$count.'</b></span></td></tr>';
                }else{
                    $search_msg = query_new('SELECT * FROM sw_pages WHERE `link` = '.quote('search_not_found'),1);
                    $html .= '<tr class="no_results_text"><td>'.(empty($search_msg['text'])?'<p>� ���������, ������ �� �������</p>':$search_msg['text']).'</td></tr>';
                    // $html .= '<tr class="no_results_text bott_bord top_bord"><td align="center" style="padding:20px;"><span>�� ������� ������� ������ �� ��������.</span><br><span>������� ������ ��� ��������� �� <a href="/">�������</a>.</span></td></tr>';
                }
                $html.= '</table></div>';
                jQuery('.fast_search_popup')->remove();
                jQuery('.find')->append(inUTF8($html));
                jQuery::evalScript('fastsearch.init()');
            } else {
                jQuery::evalScript(inUTF8('initSysDialog("����� ������� (�������� ��������� ������)")'));
            }
        }
    }else{
        jQuery::evalScript(inUTF8('initSysDialog("������ ����������� �������")'));
    }
    jQuery::getResponse();
}

function initSearch() {
	global $_GLOBALS;

	$search = !empty($_GET['query']) ? strtolower(filter(trim($_GET['query']), 'nohtml')) : null;

	if (!empty($search)) {
		if (!preg_match('#^[ ��"�_a-zA-Z�-��-�0-9-]+$#i', $search)) {
			$html = '<div class="mainTextInner"><p>� ������� ������������ ����������� �������</p></div>';
		} elseif ($search == '"' || $search == '�' || $search == '�') {
			$html = '<div class="mainTextInner"><p>����� ������ ��������� ������</p></div>';
		} elseif (strlen($search) < 2) {
			$html = '<div class="mainTextInner"><p>� �������� ������ ����� ����� 2-� ����</p></div>';
		} else {
			include_once('system/stem.php');
            $s = preg_replace('/ +/', ' ', $search);
            $t = !empty($_REQUEST['search_type'])?intval($_REQUEST['search_type']):0;
            $sql = search_sql($s,$t,0,1000);

            $items = query_new($sql['sql_lim']);
            if (!empty($items)) {
                $html.= '<div class="catalog blockWithTopLine">';
                foreach ($items as $item) { $html .= dirItem($item);}
                $html.= '<div class="clear"></div></div>';
            } else {
                $search_msg = query_new('SELECT * FROM sw_pages WHERE `link` = '.quote('search_not_found'),1);
                $html = '<div class="mainTextInner">'.(empty($search_msg['text'])?'<p>� ���������, ������ �� �������</p>':$search_msg['text']).'</div>';
            }
		}
	} else {
        $search_msg = query_new('SELECT * FROM sw_pages WHERE `link` = '.quote('search_bad_word'),1);
        $html = '<div class="mainTextInner">'.(empty($search_msg['text'])?'<p>� ���������, �� �� ������� ��������� ������</p>':$search_msg['text']).'</div>';
    }
	$_GLOBALS['title'] = '����� �'.$search.'�';
	$_GLOBALS['text'] = $html;
	$_GLOBALS['html_title'] = $_GLOBALS['title'].' - '.$_GLOBALS['v_sitename'];
	$_GLOBALS['template'] = 'default';
}

?>