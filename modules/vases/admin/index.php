<?
if (!defined('_SWS_ADMIN_CORE')) exit();

$_GLOBALS['create_dir'] = true;
$_GLOBALS['add'] = true;
$_GLOBALS['search'] = true;
$_GLOBALS['on-page'] = 20;
$_GLOBALS['tiny'] = false;
$_GLOBALS['date'] = true;

$_GLOBALS['field']['title']['id'] = 'ID';
$_GLOBALS['field']['title']['title'] = '�������� ����';
$_GLOBALS['field']['title']['price'] = '����';
$_GLOBALS['field']['title']['status'] = '������';


$_GLOBALS['field']['type']['id'] = 'text';
$_GLOBALS['field']['type']['title'] = 'text';
$_GLOBALS['field']['type']['price'] = 'text';
$_GLOBALS['field']['type']['status'] = 'checkbox';
$_GLOBALS['field']['type']['picture'] = 'file';

$_GLOBALS['field']['db']['id'] = 'int(11) NOT NULL';
$_GLOBALS['field']['db']['title'] = 'varchar(255) NOT NULL';
$_GLOBALS['field']['db']['price'] = 'int(11) NULL';
$_GLOBALS['field']['db']['picture'] = 'varchar(255) NULL';
$_GLOBALS['field']['db']['status'] = 'int(1) NULL';


$_GLOBALS['field']['table']['id'] = 'text';
$_GLOBALS['field']['table']['title'] = 'text';
$_GLOBALS['field']['table']['price'] = 'text';
$_GLOBALS['field']['table']['status'] = 'text';


$_GLOBALS['field']['value']['active'] = 1;

$_GLOBALS['field']['settings']['picture']['count'] = 1;
$_GLOBALS['field']['settings']['picture']['type'] = '"����������� ����": "*.jpg; *.jpeg; *.gif; *.png"';
$_GLOBALS['field']['settings']['picture']['extension'] = array('jpg', 'jpeg', 'gif', 'png');

$_GLOBALS['edit-field'] = array('title', 'price', 'status', 'picture');
$_GLOBALS['add-field'] = $_GLOBALS['edit-field'];

$check_tbls_cols = array(
    'vases'=>$_GLOBALS['field']['db']
);
check_tbl($check_tbls_cols);
