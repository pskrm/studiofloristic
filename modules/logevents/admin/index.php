<?
if (!defined('_SWS_ADMIN_CORE')) exit();

$_GLOBALS['create_dir'] = false;
$_GLOBALS['add'] = false;
$_GLOBALS['search'] = true;
$_GLOBALS['on-page'] = 20;
$_GLOBALS['tiny'] = false;
$_GLOBALS['date'] = true;

$_GLOBALS['field']['title']['id'] = 'ID';
$_GLOBALS['field']['title']['code'] = '�������';
$_GLOBALS['field']['title']['uid'] = '������������';
$_GLOBALS['field']['title']['oid'] = 'ID ������';
$_GLOBALS['field']['title']['date'] = '���� �������';
$_GLOBALS['field']['title']['status'] = '������ ������';
$_GLOBALS['field']['title']['place'] = '�����';
$_GLOBALS['field']['title']['itogo'] = '�����';
$_GLOBALS['field']['title']['type'] = '���';

$_GLOBALS['field']['type']['code'] = 'text';
$_GLOBALS['field']['type']['active'] = 'checkbox';
$_GLOBALS['field']['type']['pos'] = 'position';
$_GLOBALS['field']['type']['place'] = 'text';
$_GLOBALS['field']['type']['status'] = 'checkbox';
$_GLOBALS['field']['type']['itogo'] = 'text';
$_GLOBALS['field']['type']['type'] = 'text';

$_GLOBALS['field']['db']['id'] = 'int(11) NOT NULL';
$_GLOBALS['field']['db']['code'] = 'int(11) NULL';
$_GLOBALS['field']['db']['uid'] = 'int(11) NOT NULL';
$_GLOBALS['field']['db']['data'] = 'text NULL';
$_GLOBALS['field']['db']['date'] = 'datetime NULL';
$_GLOBALS['field']['db']['info'] = 'text NULL';
$_GLOBALS['field']['db']['pos'] = "int(11) NOT NULL default '0'";
$_GLOBALS['field']['db']['system'] = "int(1) NOT NULL default '0'";
$_GLOBALS['field']['db']['active'] = "int(1) NOT NULL default '0'";

/*
$_GLOBALS['field']['table']['id'] = 'text';
$_GLOBALS['field']['table']['code'] = 'text';
$_GLOBALS['field']['table']['uid'] = 'uid_link';
$_GLOBALS['field']['table']['data'] = 'text';
$_GLOBALS['field']['table']['date'] = 'datetime';
$_GLOBALS['field']['table']['info'] = 'text';
*/

$_GLOBALS['field']['table']['id'] = 'text';
$_GLOBALS['field']['table']['code'] = 'text';
$_GLOBALS['field']['table']['uid'] = 'uid_link';
$_GLOBALS['field']['table']['oid'] = 'text';
$_GLOBALS['field']['table']['date'] = 'datetime';
$_GLOBALS['field']['table']['status'] = 'text';
$_GLOBALS['field']['table']['place'] = 'text';
$_GLOBALS['field']['table']['itogo'] = 'text';
$_GLOBALS['field']['table']['type'] = 'text';


$_GLOBALS['field']['value']['active'] = 1;

$_GLOBALS['edit-field'] = array('active', 'code', 'uid', 'data', 'date');
$_GLOBALS['add-field'] = $_GLOBALS['edit-field'];

$check_tbls_cols = array(
    'adm_events'=>$_GLOBALS['field']['db']
);
check_tbl($check_tbls_cols);
