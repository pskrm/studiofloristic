<?
	if (!defined('_SWS_ADMIN_CORE')) exit();
	
	$_GLOBALS['create_dir'] = true;
	$_GLOBALS['add'] = true;
	$_GLOBALS['search'] = true;
	$_GLOBALS['on-page'] = 20;
	$_GLOBALS['tiny'] = true;
	$_GLOBALS['date'] = false;
	$_GLOBALS['nesting'] = false;
	
	$_GLOBALS['field']['title']['id'] = 'ID';
	$_GLOBALS['field']['title']['active'] = '���./����.';
	$_GLOBALS['field']['title']['pos'] = '�������';
	$_GLOBALS['field']['title']['title'] = '��������';
        $_GLOBALS['field']['title']['delivery_photo'] = '���������� �������� ������';
	
	$_GLOBALS['field']['type']['active'] = 'checkbox';
	$_GLOBALS['field']['type']['pos'] = 'position';
	$_GLOBALS['field']['type']['title'] = 'title';
        $_GLOBALS['field']['type']['delivery_photo'] = 'image';
	
        $_GLOBALS['field']['db']['sid'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['active'] = "int(1) NOT NULL default '0'";
	$_GLOBALS['field']['db']['pos'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['title'] = "varchar(255) default NULL";
        $_GLOBALS['field']['db']['delivery_photo'] = "text NOT NULL";
	$_GLOBALS['field']['db']['insert'] = "timestamp NOT NULL default CURRENT_TIMESTAMP";
	$_GLOBALS['field']['db']['system'] = "int(1) NOT NULL default '0'";
        
	
	$_GLOBALS['field']['table']['id'] = 'text';
        $_GLOBALS['field']['table']['pos'] = 'pos';
	$_GLOBALS['field']['table']['title'] = 'title';
	
	$_GLOBALS['field']['value']['active'] = 1;
	
        $_GLOBALS['field']['settings']['delivery_photo']['count'] = 100;
	$_GLOBALS['field']['settings']['delivery_photo']['type'] = '"�����������": "*.jpg; *.jpeg; *.gif; *.png"';
	$_GLOBALS['field']['settings']['delivery_photo']['size'] = array(array('height'=>192, 'ratio'=>true), array('width'=>168, 'ratio'=>true), array('width'=>600, 'height'=>400, 'ratio'=>true, 'watermark'=>true));
        
	$_GLOBALS['edit-field'] = array('active', 'title', 'pos','delivery_photo');
	$_GLOBALS['add-field'] = $_GLOBALS['edit-field'];
        
        $check_tbls_cols = array(
            'photo_delivery'=>$_GLOBALS['field']['db']
        );
        check_tbl($check_tbls_cols);
?>