<?
	if (!defined('_SWS_ADMIN_CORE')) exit();
	
	$_GLOBALS['create_dir'] = true;
	$_GLOBALS['add'] = true;
	$_GLOBALS['search'] = true;
	$_GLOBALS['on-page'] = 20;
	$_GLOBALS['tiny'] = false;
	$_GLOBALS['date'] = false;
	
	$_GLOBALS['field']['title']['id'] = 'ID';
	$_GLOBALS['field']['title']['sid'] = '������/���������';
	$_GLOBALS['field']['title']['active'] = '���./����.';
	$_GLOBALS['field']['title']['pos'] = '�������';
	$_GLOBALS['field']['title']['title'] = '��������';
	$_GLOBALS['field']['title']['colors'] = '�����';
	$_GLOBALS['field']['title']['price'] = '����';
	$_GLOBALS['field']['title']['image'] = '�����������';
	
	$_GLOBALS['field']['type']['sid'] = 'sid';
	$_GLOBALS['field']['type']['active'] = 'checkbox';
	$_GLOBALS['field']['type']['pos'] = 'position';
	$_GLOBALS['field']['type']['title'] = 'text';
	$_GLOBALS['field']['type']['colors'] = 'colors';
	$_GLOBALS['field']['type']['price'] = 'text';
	$_GLOBALS['field']['type']['image'] = 'image';
	
	$_GLOBALS['field']['db']['sid'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['active'] = "int(1) NOT NULL default '0'";
	$_GLOBALS['field']['db']['pos'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['title'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['colors'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['price'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['image'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['insert'] = "timestamp NOT NULL default CURRENT_TIMESTAMP";
	$_GLOBALS['field']['db']['system'] = "int(1) NOT NULL default '0'";
	
	$_GLOBALS['field']['table']['id'] = 'text';
	$_GLOBALS['field']['table']['pos'] = 'pos';
    $_GLOBALS['field']['table']['title'] = 'title';
	
	$_GLOBALS['field']['value']['active'] = 1;
	
	$_GLOBALS['field']['settings']['image']['count'] = 1;
	$_GLOBALS['field']['settings']['image']['type'] = '"�����������": "*.jpg; *.jpeg; *.gif; *.png"';
	$_GLOBALS['field']['settings']['image']['size'] = array(array('width'=>120, 'height'=>90), array('width'=>132, 'height'=>105));
	
	$_GLOBALS['edit-field'] = array('active', 'title', 'colors', 'price', 'image');
	$_GLOBALS['add-field'] = $_GLOBALS['edit-field'];
?>