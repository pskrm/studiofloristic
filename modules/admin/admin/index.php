<?
	if (!defined('_SWS_ADMIN_CORE')) exit();
	
	$_GLOBALS['create_dir'] = false;
	$_GLOBALS['add'] = true;
	$_GLOBALS['search'] = true;
	$_GLOBALS['on-page'] = 20;
	$_GLOBALS['tiny'] = false;
	$_GLOBALS['date'] = false;
	
	$_GLOBALS['field']['title']['id'] = 'ID';
	$_GLOBALS['field']['title']['sid'] = '������/���������';
	$_GLOBALS['field']['title']['active'] = '���./����.';
	$_GLOBALS['field']['title']['pos'] = '�������';
	$_GLOBALS['field']['title']['email'] = 'E-mail';
	$_GLOBALS['field']['title']['title'] = '�����';
	$_GLOBALS['field']['title']['password'] = '������';
	$_GLOBALS['field']['title']['group'] = '������ ������������';
    $_GLOBALS['field']['title']['notification'] = '�������� ����������� � ����� �������';
	
	$_GLOBALS['field']['type']['sid'] = 'sid';
	$_GLOBALS['field']['type']['active'] = 'checkbox';
	$_GLOBALS['field']['type']['pos'] = 'position';
	$_GLOBALS['field']['type']['email'] = 'text';
	$_GLOBALS['field']['type']['title'] = 'text';
	$_GLOBALS['field']['type']['password'] = 'password';
	$_GLOBALS['field']['type']['group'] = 'group';
    $_GLOBALS['field']['type']['notification'] = 'checkbox';

	$_GLOBALS['field']['db']['sid'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['active'] = "int(1) NOT NULL default '0'";
	$_GLOBALS['field']['db']['pos'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['email'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['title'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['password'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['insert'] = "timestamp NOT NULL default CURRENT_TIMESTAMP";
	$_GLOBALS['field']['db']['system'] = "int(1) NOT NULL default '0'";
    $_GLOBALS['field']['db']['notification'] = "int(1) NOT NULL default '1'";
	
	$_GLOBALS['field']['table']['id'] = 'text';
	$_GLOBALS['field']['table']['title'] = 'title';
	
	$_GLOBALS['field']['value']['active'] = 1;
    $_GLOBALS['field']['value']['notification'] = 1;
	
	$_GLOBALS['edit-field'] = array('active', 'group', 'title', 'password', 'email', 'notification');
	$_GLOBALS['add-field'] = $_GLOBALS['edit-field'];
	
	function setGroup($value = 0) {
		$option = null;
		$group = array(1=>'�������������', 2=>'��������');
		foreach ($group as $k=>$v) $option.= '<option value="'.$k.'"'.(!empty($value) && $value == $k ? ' selected' : null).'>'.$v.'</option>';
		return $option;
	}
?>