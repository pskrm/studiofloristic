<?php

if (!defined('_SWS_ADMIN_CORE'))
    exit();

$_GLOBALS['create_dir'] = false;
$_GLOBALS['add']        = true;
$_GLOBALS['search']     = false;
$_GLOBALS['on-page']    = 100;
$_GLOBALS['tiny']       = true;
$_GLOBALS['date']       = false;
$_GLOBALS['nesting']    = false;

$_GLOBALS['field']['title']['id']          = 'ID';
$_GLOBALS['field']['title']['active']      = '���./����.';
$_GLOBALS['field']['title']['pos']         = '�������';
$_GLOBALS['field']['title']['sectid']      = '�������� ������';
$_GLOBALS['field']['title']['filters']     = '������������ ������� ��� ������';
$_GLOBALS['field']['title']['title']       = '��������';
$_GLOBALS['field']['title']['h1_title']    = '��������� (H1)';
$_GLOBALS['field']['title']['html_title']  = '��������� �������� (TITLE)';
$_GLOBALS['field']['title']['text']        = '���������� ��������';
$_GLOBALS['field']['title']['link']        = '��������� ���';
$_GLOBALS['field']['title']['pach']        = '���� URL';
$_GLOBALS['field']['title']['description'] = '�������� (DESCRIPTION)';

$_GLOBALS['field']['type']['sectid']      = 'sectionList_seoSections';
$_GLOBALS['field']['type']['active']      = 'checkbox';
$_GLOBALS['field']['type']['pos']         = 'position';
$_GLOBALS['field']['type']['title']       = 'title';
$_GLOBALS['field']['type']['h1_title']    = 'text';
$_GLOBALS['field']['type']['html_title']  = 'textarea';
$_GLOBALS['field']['type']['link']        = 'text';
$_GLOBALS['field']['type']['pach']        = 'text';
$_GLOBALS['field']['type']['filters']     = 'filter_sellist';
$_GLOBALS['field']['type']['text']        = 'html';
$_GLOBALS['field']['type']['description'] = 'textarea';

$_GLOBALS['field']['db']['sid']         = "int(11) NOT NULL default '0'";
$_GLOBALS['field']['db']['sectid']      = "int(11) NOT NULL default '0'";
$_GLOBALS['field']['db']['active']      = "int(1) NOT NULL default '0'";
$_GLOBALS['field']['db']['pos']         = "int(11) NOT NULL default '0'";
$_GLOBALS['field']['db']['title']       = "text";
$_GLOBALS['field']['db']['h1_title']    = 'text';
$_GLOBALS['field']['db']['html_title']  = 'text';
$_GLOBALS['field']['db']['link']        = "text";
$_GLOBALS['field']['db']['pach']        = "text";
$_GLOBALS['field']['db']['filters']     = 'text';
$_GLOBALS['field']['db']['text']        = 'longtext';
$_GLOBALS['field']['db']['description'] = 'text';
$_GLOBALS['field']['db']['insert']      = "timestamp NOT NULL default CURRENT_TIMESTAMP";
$_GLOBALS['field']['db']['system']      = "int(1) NOT NULL default '0'";


$_GLOBALS['field']['table']['id']    = 'text';
$_GLOBALS['field']['table']['pos']   = 'pos';
$_GLOBALS['field']['table']['title'] = 'title';
$_GLOBALS['field']['table']['pach']  = 'link';

$_GLOBALS['field']['value']['active'] = 1;

$_GLOBALS['edit-field'] = array('active', 'sectid', 'filters', 'link', 'pach', 'title', 'h1_title', 'html_title', 'text', 'description');
$_GLOBALS['add-field']  = $_GLOBALS['edit-field'];

check_tbl(array('seo_section' => $_GLOBALS['field']['db']));

switch($op){
    case 'pos': seo_pos(); break;
    case 'load-module': seo_initModule(); break;
}

function seo_pos(){
	global $module;

	if (empty($_GET['id'])) jQuery::getResponse();

	$id = abs(intval($_GET['id']));
	$sid = !empty($_GET['sectid']) ? abs(intval($_GET['sectid'])) : 0;

	$items = query_new('SELECT `id` FROM `sw_'.$module.'` WHERE `sectid` = '.quote($sid).' ORDER BY `pos`,`id` ASC');
	if (!empty($items) && isset($_GET['top'])){
		$top = intval($_GET['top']);
		foreach ($items as $npos => $item) {
			if (($top==0 && $item['id']==$id) || ($top==1 && $items[$npos+1]['id']==$id)){
				$npos++;
			}elseif(($top==0 && $items[$npos-1]['id']==$id) || ($top==1 && $item['id']==$id)){
				$npos--;
			}
			db_query('UPDATE `sw_'.$module.'` SET `pos` = '.quote($npos).' WHERE `id` = '.quote($item['id']));
		}
	}
	$html = seo_initModule(true, $sid);
    jQuery('#tablist_'.$sid)->html(inUTF8($html));
    jQuery::getResponse();
}

function seo_initModule($ret = false, $sectid = false) {
    global $_GLOBALS, $m, $module, $op;

    $tr = count($_GLOBALS['field']['table']);

    $html = '<h1 style="margin-right: 13px;"><a href="javascript:module.index()">' . $m['title'] . '</a>' . (!empty($_GLOBALS['add']) ? ' <img src="/sf123sf123sf/i/add.png" title="��������" align="absmiddle" onclick="initGET(\'/go/m=' . $module . '&op=item\')">' : null) . '</h1>';
    $html .= '<div class="seo_links_cont"><ul>';
    $w = empty($sectid)?'':' WHERE seo.`sectid` = '.$sectid.' ';
    $seo_links = query_new('SELECT seo.`id`,seo.`pos`,seo.`active`,seo.`system`,seo.`sectid`,ss.`title` AS "sect_title",seo.`title`,seo.`pach` FROM sw_seo_section seo LEFT JOIN sw_section ss ON ss.`id`=seo.`sectid` '.$w.' ORDER BY seo.`sectid`,seo.`pos`', 0, 'sectid', 1);
    if (!empty($seo_links)) {
        $tabHead = '<table class="items"><tr class="title">';
        foreach ($_GLOBALS['field']['table'] as $name => $value) {
            $tabHead.= '<th class="' . $name . '">' . $_GLOBALS['field']['title'][$name] . '</th>';
        }
        $tabHead.= '<th class="options">��������</th></tr>';

        foreach ($seo_links as $skey => $sl_sid) {
            $c        = count($sl_sid) - 1;
            $tbl_rows = array();
            $html .= '<li class="seo_links">';
            foreach ($sl_sid as $ind => $i) {
                $td = '<tr id="i' . $i['id'] . '">';
                foreach ($_GLOBALS['field']['table'] as $name => $value) {
                    switch ($value) {
                        case 'title': $td.= '<td class="title" onclick="initGET(\'/go/m=' . $module . '&op=item&id=' . $i['id'] . '&pg=' . $pg . '\')"' . '>' . $i[$name] . '</td>';
                            break;
                        case 'pos': $td.= '<td class="' . $name . '">'
                                    . (empty($ind) ? '<img src="/sf123sf123sf/i/top-block.gif">' : '<a href="javascript:void(0)" onclick="initGET(\'/go/m=' . $module . '&op=pos&id=' . $i['id'] . '&top=1&sectid=' . $skey . '\')"><img src="/sf123sf123sf/i/top.gif"></a>')
                                    . ($c == $ind ? '<img src="/sf123sf123sf/i/bottom-block.gif">' : '<a href="javascript:void(0)" onclick="initGET(\'/go/m=' . $module . '&op=pos&id=' . $i['id'] . '&top=0&sectid=' . $skey . '\')"><img src="/sf123sf123sf/i/bottom.gif"></a>')
                                    . '</td>';
                            break;
                        case 'link': $td.= '<td class="' . $name . '"><a href="' . $i[$name] . '" target="_blank">' . $i[$name] . '</a></td>';
                            break;
                        default: $td.= '<td class="' . $name . '">' . $i[$name] . '</td>';
                            break;
                    }
                }
                $tbl_rows[] = $td . '<td class="btn">'
                        . '<div class="active' . (!empty($i['active']) ? '" title="�������"' : ' active-off" title="��������"') . ' onclick="initGET(\'/go/m=' . $module . '&op=active&id=' . $i['id'] . '\')"></div>'
                        . '<div class="edit" title="�������������" onclick="initGET(\'/go/m=' . $module . '&op=item&id=' . $i['id'] . '&pg=' . $pg . '&query=' . htmlspecialchars(urlencode($search)) . '\')"></div>'
                        . '<div title="�������" class="delete' . ($i['system'] != 0 ? '-off"' : '" onclick="items.remove(' . $i['id'] . ')"') . '></div>'
                        . '</td></tr>';
            }
            $tablist = $tabHead . PHP_EOL . implode(PHP_EOL, $tbl_rows) . '</table>';
            $html .= '<div class="catTitle" onclick="seo_links(this)"><span>' . $i['sect_title'] . '</span></div><div id="tablist_' . $skey . '" class="tablist">' . PHP_EOL . $tablist . PHP_EOL . '</div></li>';
        }
    }

    if (!empty($ret)) {
        return $tablist;
    }
    $file = '../../modules/' . $module . '/admin/css.css';
    $html = (file_exists($file) ? PHP_EOL . '<style  type="text/css">' . PHP_EOL . file_get_contents($file) . PHP_EOL . '</style>' . PHP_EOL : '') . $html.'</ul></div>';

    jQuery('#content-data')->html(inUTF8($html));
    jQuery::evalScript('items.odd("table#list-items.items");');
    jQuery::evalScript('jQuery.getScript("/modules/seo_section/admin/js.js")');
    jQuery::getResponse();
}
