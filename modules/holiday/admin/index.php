<?
	if (!defined('_SWS_ADMIN_CORE')) exit();
	
	$_GLOBALS['create_dir'] = true;
	$_GLOBALS['add'] = false;
	$_GLOBALS['search'] = true;
	$_GLOBALS['on-page'] = 20;
	$_GLOBALS['tiny'] = true;
	$_GLOBALS['date'] = true;
	
	$_GLOBALS['field']['title']['id'] = 'ID';
	$_GLOBALS['field']['title']['sid'] = '������/���������';
	$_GLOBALS['field']['title']['active'] = '���./����.';
	$_GLOBALS['field']['title']['pos'] = '�������';
	$_GLOBALS['field']['title']['title'] = '������������';
	$_GLOBALS['field']['title']['start'] = '���� ������';
	$_GLOBALS['field']['title']['end'] = '���� ���������';
	$_GLOBALS['field']['title']['center'] = '������� ���������� ����';
	
	$_GLOBALS['field']['type']['sid'] = 'sid';
	$_GLOBALS['field']['type']['active'] = 'checkbox';
	$_GLOBALS['field']['type']['pos'] = 'position';
	$_GLOBALS['field']['type']['title'] = 'text';
	$_GLOBALS['field']['type']['start'] = 'date';
	$_GLOBALS['field']['type']['end'] = 'date';
	$_GLOBALS['field']['type']['center'] = 'text';
	
	$_GLOBALS['field']['db']['sid'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['active'] = "int(1) NOT NULL default '0'";
	$_GLOBALS['field']['db']['pos'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['title'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['start'] = "time default NULL";
	$_GLOBALS['field']['db']['end'] = "time default NULL";
	$_GLOBALS['field']['db']['center'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['insert'] = "timestamp NOT NULL default CURRENT_TIMESTAMP";
	$_GLOBALS['field']['db']['system'] = "int(1) NOT NULL default '0'";
	
	$_GLOBALS['field']['table']['id'] = 'text';
	$_GLOBALS['field']['table']['title'] = 'text';
	$_GLOBALS['field']['table']['center'] = 'text';
	
	$_GLOBALS['field']['value']['active'] = 1;
	
	$_GLOBALS['edit-field'] = array('title', 'start', 'end', 'center');
	$_GLOBALS['add-field'] = array('title', 'start', 'end', 'center');
?>