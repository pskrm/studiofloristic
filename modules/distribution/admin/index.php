<?
if (!defined('_SWS_ADMIN_CORE')) exit();

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$_GLOBALS['tiny'] = true;

switch ($op) {
	case 'load-module': initDistribution(); break;
	case 'send': initSend(); break;
	case 'view': initView(); break;
	case 'remove': initRemove(); break;
	case 'insert': initInsert(); break;
}

function initRemove() {
	if (empty($_GET['id'])) jQuery::getResponse();
	$id = abs(intval($_GET['id']));
	
	db_delete('distribution', '`id` = '.quote($id));
	
	jQuery::getResponse();
}

function initView() {
	if (empty($_GET['id'])) jQuery::getResponse();
	$id = abs(intval($_GET['id']));
	
	$r = db_query('SELECT `title`, `text` FROM `sw_distribution` WHERE `id` = '.quote($id));
	if (mysql_num_rows($r) != 0) {
		$d = mysql_fetch_array($r, MYSQL_ASSOC);
		jQuery('input#message-title')->val(inUTF8($d['title']));
		jQuery('textarea#tiny-distribution')->html(inUTF8($d['text']));
	}
	
	jQuery::getResponse();
}

function initInsert() {
	global $_GLOBALS;
	
	$html = null;
	$title = null;
	
	if (!empty($_POST['item'])) {
		foreach ($_POST['item'] as $id) {
			$html.= '<ul>';
			
			$id = abs(intval($id));
			if (!empty($id)) {
				$r = db_query('SELECT `link`, `title`, `notice` FROM `sw_news` WHERE `id` = '.quote($id));
				if (mysql_num_rows($r) != 0) {
					$n = mysql_fetch_array($r, MYSQL_ASSOC);
					$html.= '<li><a href="http://'._HOSTNAME.'/news/'.$n['link'].'.html" target="_blank">'.$n['title'].'</a>'.stripslashes($n['notice']).'</li>';
				}
			}
			
			$html.= '</ul>';
		}
		
		$r = db_query('SELECT `value` FROM `sw_variables` WHERE `name` = '.quote('sitename').' LIMIT 1');
		$v = mysql_fetch_array($r, MYSQL_ASSOC);
		
		$title = '������� ����� �'.$v['value'].'�';
	}
	
	jQuery('input#message-title')->val(inUTF8($title));
	jQuery('textarea#tiny-distribution')->html(inUTF8($html));
	jQuery::getResponse();
}

function initSend() {
	

	if (!isset($_POST['text'])) jQuery::getResponse();
	$html = trim(in1251($_POST['text']));
	$noHTML = filter($html, 'nohtml');
	
	if (empty($noHTML)) {
		jQuery::evalScript(inUTF8('initSysDialog("��� ������ ��� ��������")'));
		jQuery::getResponse();
	}
	
	if (empty($_POST['title'])) {
		jQuery::evalScript(inUTF8('initSysDialog("�� �� �������� ���� ������")'));
		jQuery::getResponse();
	}
	
	$where = null;
	$time = time();
	$theme = filter(trim(in1251($_POST['title'])), 'nohtml');
	
	$r = db_query('SELECT `value` FROM `sw_variables` WHERE `name` = '.quote('sitename').' LIMIT 1');
	$v = mysql_fetch_array($r, MYSQL_ASSOC);
	
	db_query('INSERT INTO `sw_distribution` (`title`, `text`, `insert`) VALUES ('.quote($theme).', '.quote($html).', '.quote($time).')');
	$sid = dbone('MAX(`id`)', false, 'distribution');
	
	if (!empty($_POST['type']) && $_POST['type'] == 2) $where = ' AND `subscription` = 1';
	
	$r = db_query('SELECT `email`, `username` FROM `sw_users` WHERE `active` = 1'.$where);
	while ($u = mysql_fetch_array($r, MYSQL_ASSOC)) {
		$msg = '<p>������������, '.$u['username'].'</p>'.$html.'<p>���������� �� �������� �� ������ � ����� ������ ��������.</p>';

		$mail = new PHPMailer(true);
		try {
			$mail->setFrom('robot@'._HOSTNAME, _HOSTNAME);
			$mail->addAddress($u['email']);
			$mail->Subject = $theme;
			$mail->isHTML(true);
			$mail->CharSet = 'windows-1251';
			$mail->Body = '<html>'.$msg.'</html>';
			$mail->AltBody = strip_tags($msg);
			$mail->send();
		} catch (Exception $e) {
		    continue;
		}
	}
	
	jQuery::evalScript(inUTF8('email.insert({id:'.$sid.',title:"'.htmlspecialchars($theme).'",date:"'.date('d.m.Y � H:i', $time).'"})'));
	jQuery::getResponse();
}

function initDistribution() {
	$html = null;
	
	$r = db_query('SELECT (SELECT COUNT(`id`) FROM `sw_users` WHERE `active` = 1) as `all`, (SELECT COUNT(`id`) FROM `sw_users` WHERE `active` = 1 AND `subscription` = 1) as `subscription`');
	$c = mysql_fetch_array($r, MYSQL_ASSOC);
	
	$html.= '<h1><a href="javascript:module.index()">E-mail ��������</a></h1>';
	
	$r = db_query('SELECT `id`, `link`, `title`, UNIX_TIMESTAMP(`insert`) as `datetime` FROM `sw_news` WHERE `active` = 1 AND `date` <= CURDATE() ORDER BY `date` DESC, `id` DESC');
	if (mysql_num_rows($r) != 0) {
		$html.= '<form id="news-insert" action="/go/m=distribution&op=insert" onsubmit="formSubmit(this); return false;">';
		$html.= '<p>������� ��� ��������:<br /><div style="float:left;width:100%;height:100px;overflow:auto;margin-bottom:10px;"><table id="list-items" class="items"><tr class="title">';
		$html.= '<th class="id"></th><th class="title">���������</th><th class="title">���� � �����</th>';
		while ($n = mysql_fetch_array($r, MYSQL_ASSOC)) {
			$html.= '<tr><td><input type="checkbox" name="item[]" value="'.$n['id'].'" onchange="formSubmit(jQuery(\'form#news-insert\'))"></td><td class="title"><a href="/news/'.$n['link'].'.html" target="_blank">'.$n['title'].'</a></td><td class="datetime">'.date('d.m.Y � H:i', $n['datetime']).'</td></tr>';
		}
		$html.= '</table></div></p>';
		$html.= '</form>';
	}
	
	$html.= '<form id="distribution-form" action="/go/m=distribution&op=send" onsubmit="formSubmit(this); return false;">';
	$html.= '<table class="form-data" style="float:left"><tr><th width="80">���� �����:</th><td><p><label><input type="radio" name="type" value="1" checked> ���� <span class="hint">('.$c['all'].')</span></label></p><p><label><input type="radio" name="type" value="2"> ������������� <span class="hint">('.$c['subscription'].')</span></label></p><p id="my-numers"><a href="javascript:email.myNumbers(true)" class="insert-type">�������� ������</a><br/></p></td></tr></table>';
	$html.= '<p>���� ������:<br /><input id="message-title" type="text" name="title" class="f-field width-100"></p>';
	$html.= '<p>����� ������:<br /><textarea id="tiny-distribution" name="text" class="width-100"></textarea></p>';
	$html.= '<p class="submit"><input class="submit" type="submit" value="���������"></p>';
	$html.= '</form>';
	
	$html.= '<h2>������� ���������</h2>';
	$html.= '<table id="list-history" class="items"><tr class="title"><th class="title">��������</th><th class="date">���� � ����� ��������</th><th width="20"></th></tr>';
	
	$r = db_query('SELECT `id`, `title`, `insert` FROM `sw_distribution` ORDER BY `id` DESC');
	while ($d = mysql_fetch_array($r, MYSQL_ASSOC)) {
		$html.= '<tr id="d'.$d['id'].'"><td class="title"><a href="javascript:email.view('.$d['id'].')">'.$d['title'].'</a></td><td class="date">'.date('d.m.Y � H:i', $d['insert']).'</td><td class="btn"><div title="�������" class="delete" onclick="email.remove('.$d['id'].')"></div></td></tr>';
	}
	
	$html.= '</table>';
	
	jQuery('#content-data')->html(inUTF8($html));
	jQuery::evalScript('htmlEditor.load("distribution","/files/distribution/")');
	jQuery::getResponse();
}

?>