<?php
	if (!defined('_SWS_ADMIN_CORE')) exit();

	$_GLOBALS['create_dir'] = true;
	$_GLOBALS['add'] = true;
	$_GLOBALS['search'] = true;
	$_GLOBALS['on-page'] = 20;
	$_GLOBALS['tiny'] = true;
	$_GLOBALS['date'] = false;
	$_GLOBALS['nesting'] = false;

	$_GLOBALS['field']['title']['id'] = 'ID';
	$_GLOBALS['field']['title']['active'] = '���./����.';
	$_GLOBALS['field']['title']['pos'] = '�������';
	$_GLOBALS['field']['title']['title'] = '��������';
	$_GLOBALS['field']['title']['text'] = '�������� ������';
	$_GLOBALS['field']['title']['begin-price'] = '��������� ����';
	$_GLOBALS['field']['title']['end-price'] = '�������� ����';
	$_GLOBALS['field']['title']['image'] = '�����������';

	$_GLOBALS['field']['type']['sid'] = 'catalog';
	$_GLOBALS['field']['type']['active'] = 'checkbox';
	$_GLOBALS['field']['type']['pos'] = 'position';
	$_GLOBALS['field']['type']['title'] = 'title';
	$_GLOBALS['field']['type']['text'] = 'textarea';
	$_GLOBALS['field']['type']['begin-price'] = 'text';
	$_GLOBALS['field']['type']['end-price'] = 'text';
	$_GLOBALS['field']['type']['image'] = 'image';

	$_GLOBALS['field']['db']['sid'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['active'] = "int(1) NOT NULL default '0'";
	$_GLOBALS['field']['db']['pos'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['title'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['text'] = "text";
	$_GLOBALS['field']['db']['begin-price'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['end-price'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['image'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['insert'] = "timestamp NOT NULL default CURRENT_TIMESTAMP";
	$_GLOBALS['field']['db']['system'] = "int(1) NOT NULL default '0'";

	$_GLOBALS['field']['table']['id'] = 'text';
	$_GLOBALS['field']['table']['pos'] = 'pos';
	$_GLOBALS['field']['table']['title'] = 'title';
    $_GLOBALS['field']['table']['begin-price'] = "text";

	$_GLOBALS['field']['value']['active'] = 1;
    $_GLOBALS['field']['value']['index_pos'] = 0;

	$_GLOBALS['field']['settings']['image']['count'] = 1;
	$_GLOBALS['field']['settings']['image']['type'] = '"�����������": "*.jpg; *.jpeg; *.gif; *.png"';
	$_GLOBALS['field']['settings']['image']['size'] = array(array('width'=>70, 'height'=>70, 'ratio'=>true), array('width'=>120, 'height'=>90, 'ratio'=>true), array('width'=>128, 'height'=>116, 'ratio'=>true), array('width'=>168, 'height'=>192, 'ratio'=>true), array('width'=>199, 'height'=>287, 'ratio'=>true), array('width'=>600, 'height'=>400, 'ratio'=>true, 'watermark'=>true));

	$_GLOBALS['edit-field'] = array('active', 'title', 'text', 'begin-price', 'image');
	$_GLOBALS['add-field'] = $_GLOBALS['edit-field'];

    check_tbl(array($module=>$_GLOBALS['field']['db']));