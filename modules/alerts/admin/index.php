<?
	if (!defined('_SWS_ADMIN_CORE')) exit();
	
	$_GLOBALS['create_dir'] = true;
	$_GLOBALS['add'] = true;
	$_GLOBALS['search'] = true;
	$_GLOBALS['on-page'] = 20;
	$_GLOBALS['tiny'] = true;
	$_GLOBALS['date'] = true;
	
	$_GLOBALS['field']['title']['id'] = 'ID';
	$_GLOBALS['field']['title']['sid'] = '������/���������';
	$_GLOBALS['field']['title']['active'] = '���./����.';
	$_GLOBALS['field']['title']['pos'] = '�������';
	$_GLOBALS['field']['title']['name'] = '���';
	$_GLOBALS['field']['title']['family'] = '�������';
	$_GLOBALS['field']['title']['datetime'] = '���� � �����';
	$_GLOBALS['field']['title']['text'] = '�����';
	
	$_GLOBALS['field']['type']['sid'] = 'sid';
	$_GLOBALS['field']['type']['active'] = 'checkbox';
	$_GLOBALS['field']['type']['pos'] = 'position';
	$_GLOBALS['field']['type']['name'] = 'text';
	$_GLOBALS['field']['type']['family'] = 'text';
	$_GLOBALS['field']['type']['datetime'] = 'text';
	$_GLOBALS['field']['type']['text'] = 'textarea';
	
	$_GLOBALS['field']['db']['sid'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['active'] = "int(1) NOT NULL default '0'";
	$_GLOBALS['field']['db']['pos'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['name'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['family'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['datetime'] = "datetime default NULL";
	$_GLOBALS['field']['db']['text'] = "text";
	$_GLOBALS['field']['db']['insert'] = "timestamp NOT NULL default CURRENT_TIMESTAMP";
	$_GLOBALS['field']['db']['system'] = "int(1) NOT NULL default '0'";
	
	$_GLOBALS['field']['table']['id'] = 'text';
	$_GLOBALS['field']['table']['name'] = 'title';
	$_GLOBALS['field']['table']['family'] = 'text';
	$_GLOBALS['field']['table']['text'] = 'text';
	
	$_GLOBALS['field']['value']['active'] = 1;
	$_GLOBALS['field']['value']['datetime'] = date('Y-m-d H:i:s');
	
	$_GLOBALS['edit-field'] = array('active', 'name', 'family', 'datetime', 'text');
	$_GLOBALS['add-field'] = $_GLOBALS['edit-field'];
?>