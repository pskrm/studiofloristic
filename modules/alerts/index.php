<?
if (!defined('SW')) die('���� ������� �� ���������');

include_once('include/jquery/jQuery.php');

switch($op) {
	case 'remove': removeAlert(); break;
	case 'insert': insertAlerts(); break;
	default: jQuery::getResponse(); break;
}

function removeAlert() {
	if (!_IS_USER || empty($_GET['id'])) jQuery::getResponse();
	$id = abs(intval($_GET['id']));
	
	db_delete('alerts', '`id` = '.quote($id).' AND `uid` = '.quote($_SESSION['uid']));
	
	HeaderPage('/office/op=my#alerts-list');
}

function insertAlerts() {
	if (!_IS_USER) jQuery::getResponse();
	
/*	if (empty($_POST['name']) || empty($_POST['date']) || !isset($_POST['time']) || empty($_POST['text'])) {
		jQuery::evalScript(inUTF8('alerts.result(1)'));
		jQuery::getResponse();
	}*/
	
	if (empty($_POST['name']) || empty($_POST['date']) || empty($_POST['text'])) {
		if (empty($_POST['name'])) jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#aName"),t:"���� ����� ����� ���������",top:0,left:0})'));
		if (empty($_POST['date'])) jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#aDate"),t:"���� ����� ����� ���������",top:0,left:0})'));
		if (empty($_POST['text'])) jQuery::evalScript(inUTF8('skyweb.alert({e:jQuery("#aText"),t:"���� ����� ��������� ����� ���������",top:0,left:0})'));
		jQuery::getResponse();
	}
	
	$uid = abs(intval($_SESSION['uid']));
	
	$timeValue = array(
		0=> '00:00',
		1=> '01:00',
		2=> '02:00',
		3=> '03:00',
		4=> '04:00',
		5=> '05:00',
		6=> '06:00',
		7=> '07:00',
		8=> '08:00',
		9=> '09:00',
		10=> '10:00',
		11=> '11:00',
		12=> '12:00',
		13=> '13:00',
		14=> '14:00',
		15=> '15:00',
		16=> '16:00',
		17=> '17:00',
		18=> '18:00',
		19=> '19:00',
		20=> '20:00',
		21=> '21:00',
		22=> '22:00',
		23=> '23:00'
	);
	
	if (dbone('COUNT(`id`)', '`uid` = '.quote($uid), 'alerts') >= 20) {
		jQuery::evalScript(inUTF8('initSysDialog("����� �������� �������� 20 �����������")'));
		jQuery::getResponse();
	}
	
	$name = filter(trim(in1251($_POST['name'])), 'nohtml');
	$date = filter(trim(in1251($_POST['date'])), 'nohtml');
	$time = abs(intval($_POST['time']));
	$cause = abs(intval($_POST['cause']));
	$text = filter(trim(in1251($_POST['text'])), 'nohtml');
	
	if (strlen($text) > 30) {
		jQuery::evalScript(inUTF8('alerts.result(4)'));
		jQuery::getResponse();
	}
	
/*	if (!preg_match("/^([0-9]{2})\:([0-9]{2})$/i", $time, $t)) {
		jQuery::evalScript(inUTF8('alerts.result(2)'));
		jQuery::getResponse();
	}*/

	$time = !empty($timeValue[$time]) ? $timeValue[$time] : '09:00';
	
	if (!preg_match("/^([0-9]{2})\.([0-9]{2})\.([0-9]{4})$/i", $date, $d)) {
		jQuery::evalScript(inUTF8('alerts.result(3)'));
		jQuery::getResponse();
	}
	
	db_query('INSERT INTO `sw_alerts` (`uid`, `name`, `cause`, `datetime`, `text`, `insert`) VALUES ('.quote($_SESSION['uid']).', '.quote($name).', '.quote($cause).', '.quote($d[3].'-'.$d[2].'-'.$d[1].' '.$time.':00').', '.quote($text).', UNIX_TIMESTAMP(NOW()))');
	
	$aid = dbone('MAX(`id`)', '`uid` = '.quote($_SESSION['uid']), 'alerts');
	
	if ($cause != 0) {
		$r = db_query('SELECT `title` FROM `sw_reminder` WHERE `id` = '.quote($cause));
		if (mysql_num_rows($r) != 0) {
			$c = mysql_fetch_array($r, MYSQL_ASSOC);
			$text = '<b>'.$c['title'].'</b>: '.$text;
		}
	}
	
	jQuery::evalScript(inUTF8('alerts.insert({id:'.$aid.',name:"'.htmlspecialchars(trim($name)).'",alert:"'.($d[1].'.'.$d[2].'.'.$d[3]).'",text:"'.htmlspecialchars($text).'"})'));
	jQuery::getResponse();
}

?>