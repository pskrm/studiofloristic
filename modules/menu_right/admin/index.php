<?
	if (!defined('_SWS_ADMIN_CORE')) exit();
	
	$_GLOBALS['create_dir'] = false;
	$_GLOBALS['add'] = true;
	$_GLOBALS['search'] = true;
	$_GLOBALS['on-page'] = 20;
	$_GLOBALS['tiny'] = false;
	$_GLOBALS['date'] = false;
	$_GLOBALS['nesting'] = true;
	
	$_GLOBALS['field']['title']['id'] = 'ID';
	$_GLOBALS['field']['title']['sid'] = '������/���������';
	$_GLOBALS['field']['title']['active'] = '���./����.';
	$_GLOBALS['field']['title']['pos'] = '�������';
	$_GLOBALS['field']['title']['title'] = '��������';
	$_GLOBALS['field']['title']['name'] = '��������� ���';
	$_GLOBALS['field']['title']['url'] = '������';
	$_GLOBALS['field']['title']['class'] = '�����';
	$_GLOBALS['field']['title']['bg_style'] = '���� ����';
	
	$_GLOBALS['field']['type']['sid'] = 'sid';
	$_GLOBALS['field']['type']['active'] = 'checkbox';
	$_GLOBALS['field']['type']['pos'] = 'position';
	$_GLOBALS['field']['type']['title'] = 'text';
	$_GLOBALS['field']['type']['name'] = 'text';
	$_GLOBALS['field']['type']['url'] = 'text';
	$_GLOBALS['field']['type']['class'] = 'text';
	$_GLOBALS['field']['type']['bg_style'] = 'collist';
	
	$_GLOBALS['field']['db']['sid'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['active'] = "int(1) NOT NULL default '0'";
	$_GLOBALS['field']['db']['pos'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['title'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['name'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['url'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['class'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['insert'] = "timestamp NOT NULL default CURRENT_TIMESTAMP";
	$_GLOBALS['field']['db']['system'] = "int(1) NOT NULL default '0'";
	$_GLOBALS['field']['db']['bg_style'] = "int(11) NOT NULL default '0'";
	
	$_GLOBALS['field']['table']['id'] = 'text';
	$_GLOBALS['field']['table']['pos'] = 'pos';
	$_GLOBALS['field']['table']['title'] = 'title';
	
	$_GLOBALS['field']['value']['active'] = 1;
	
	$_GLOBALS['edit-field'] = array('active', 'sid', 'title', 'name', 'url', 'class', 'bg_style');
	$_GLOBALS['add-field'] = $_GLOBALS['edit-field'];
	
	check_tbl(array($module=>$_GLOBALS['field']['db']));
?>