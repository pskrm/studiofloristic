<?php
    define('_INSTAGRAM_USERNAME','sfloristic');
    define('_INSTAGRAM_USERID','1474740454');

//    $_GLOBALS['add'] = true;
    $_GLOBALS['search'] = false;
    $_GLOBALS['on-page'] = 20;
    $_GLOBALS['tiny'] = false;
	$_GLOBALS['top_paging'] = true;

    $_GLOBALS['field']['title']['id'] = 'ID';
    $_GLOBALS['field']['title']['active'] = '���./����.';
    $_GLOBALS['field']['title']['pos'] = '�������';
    $_GLOBALS['field']['title']['cid'] = '������� ������';
    $_GLOBALS['field']['title']['link'] = '������ �� ����';
    $_GLOBALS['field']['title']['tags'] = '����';
    $_GLOBALS['field']['title']['inst_id'] = 'ID ����';
	$_GLOBALS['field']['title']['struct'] = '����';
    $_GLOBALS['field']['title']['created'] = '<div class="insta_task"><span>���� ���������� �</span> <img src="/sf123sf123sf/i/insta.png"></div>';

    $_GLOBALS['field']['type']['sid'] = 'sid';
    $_GLOBALS['field']['type']['active'] = 'checkbox';
    $_GLOBALS['field']['type']['pos'] = 'position';
    $_GLOBALS['field']['type']['cid'] = 'text';
    $_GLOBALS['field']['type']['link'] = 'text';
    $_GLOBALS['field']['type']['tags'] = 'text';
    $_GLOBALS['field']['type']['inst_id'] = 'text';
    $_GLOBALS['field']['type']['struct'] = 'text';
    $_GLOBALS['field']['type']['created'] = 'text';

    $_GLOBALS['field']['db']['sid'] = "int(11) NOT NULL default 0";
    $_GLOBALS['field']['db']['active'] = "int(1) NOT NULL default 1";
    $_GLOBALS['field']['db']['pos'] = "int(11) NOT NULL default 0";
    $_GLOBALS['field']['db']['cid'] = "int(11) NOT NULL default 0";
    $_GLOBALS['field']['db']['link'] = "varchar(255) NOT NULL default ''";
    $_GLOBALS['field']['db']['tags'] = "TEXT NOT NULL";
    $_GLOBALS['field']['db']['inst_id'] = "varchar(255) NOT NULL default ''";
    $_GLOBALS['field']['db']['struct'] = "TEXT NOT NULL";
    $_GLOBALS['field']['db']['insert'] = "timestamp NOT NULL default CURRENT_TIMESTAMP";
    $_GLOBALS['field']['db']['system'] = "int(1) NOT NULL default '0'";
    $_GLOBALS['field']['db']['created'] = "timestamp NOT NULL default 0";

    check_tbl(array($module=>$_GLOBALS['field']['db']));

    $_GLOBALS['field']['table']['id'] = 'text';
    $_GLOBALS['field']['table']['struct'] = 'inst_smalls';
    $_GLOBALS['field']['table']['cid'] = 'cid_name';
    $_GLOBALS['field']['table']['tags'] = 'inst_tags';
    $_GLOBALS['field']['table']['created'] = 'text';


    $_GLOBALS['edit-field'] = array('active','cid', 'link');
    $_GLOBALS['add-field'] = $_GLOBALS['edit-field'];

    // $_GLOBALS['mod_filds'] = 'CAST(MID(`struct`,INSTR(`struct`,\'"link"\') - 16,10) AS SIGNED) AS `created_get`';
    // $_GLOBALS['mod_order_by'] = ' `created_get` DESC, `created` DESC ';
    $_GLOBALS['mod_order_by'] = ' `created` DESC ';

    if ($op=='inst_photo_update'){
        instagram_update();
    }

function instagram_update(){
    global $op;

    ifAccess();

    $conf = [
        'apiKey'=>INSTAGRAM_API_KEY,
        'apiSecret'=>INSTAGRAM_API_SECRET,
        'apiCallback'=>INSTAGRAM_API_CALLBACK
    ];

    $instagram = new Instagram($conf);

    $data = (object)[
        'access_token' => $_SESSION['instagram_access_token'],
        'user' => (object)[
            'id' => $_SESSION['instagram_user_id']
        ]
    ];

    $instagram->setAccessToken($data);
    $media = $instagram->getUserMedia($data->user->id, 20, true);
    $imgs_data = array();
    while (is_object($media) && count($media->data)==20){
        $imgs_data = array_merge($imgs_data,$media->data);
        $media = $instagram->pagination($media,20);
    }

    foreach ($imgs_data as $photo){
        $photo->tags = formatTags($photo->tags);
        $forSelect = formatTags($photo->tags,1);
        $item = query_new('SELECT `id`,`title` FROM sw_catalog WHERE `art` IN ('.  implode(",",$forSelect).') OR `link` IN ('.  implode(",",$forSelect).') OR `title` IN ('.  implode(",",$forSelect).') GROUP BY `id`',1);
        $photo->cat_item = $item;
        $inHave = query_new('SELECT * FROM sw_instagram_photos WHERE `inst_id` = '.quote($photo->id),1);
        if (empty($inHave)){
            query_new('INSERT INTO sw_instagram_photos (`link`,`tags`,`inst_id`,`struct`,`created`) VALUES '
                . '('.quote($photo->link).','.quote('#'.implode('#',$photo->tags).'#').','. quote($photo->id).','.quote(serialize($photo)).',FROM_UNIXTIME('.$photo->created_time.'))');
        }elseif(empty($inHave['active'])){
            query_new('UPDATE sw_instagram_photos SET `tags`='.quote('#'.implode('#',$photo->tags).'#').', `struct`='.quote(serialize($photo)).', `created`=FROM_UNIXTIME('.$photo->created_time.') WHERE `inst_id` = '.quote($photo->id));
        }
    }

    $op = 'load-module';
}

function ifAccess()
{
    if (empty($_SESSION['instagram_access_token'])) {
        echo '�������� Access Token';
        die;
    }

    if (empty($_SESSION['instagram_user_id'])) {
        echo '�� ������ �������� Access Token ��� userID';
        die;
    }
}

function formatTags($tags,$q=false){
    if (!empty($tags) && is_array($tags)){
        foreach ($tags as $key=>$tag){
			if ($q){
				$tags[$key] = is_numeric($tag)?$tag:quote(iconv("UTF-8","Windows-1251",$tag));
			}else{
				$tags[$key] = is_numeric($tag)?$tag:iconv("UTF-8","Windows-1251",$tag);
			}
        }
    }
    return $tags;
}