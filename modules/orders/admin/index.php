<?
	if (!defined('_SWS_ADMIN_CORE')) exit();

	$_GLOBALS['create_dir'] = false;
	$_GLOBALS['add'] = true;
	$_GLOBALS['search'] = true;
	$_GLOBALS['on-page'] = 40;
	$_GLOBALS['tiny'] = true;
	$_GLOBALS['date'] = false;

	$_GLOBALS['field']['title']['id'] = 'ID';
	$_GLOBALS['field']['title']['sid'] = '������/���������';
	$_GLOBALS['field']['title']['active'] = '���./����.';

    $_GLOBALS['field']['title']['status_verified'] = '�����������';
    $_GLOBALS['field']['title']['status-printed'] = '������';
	$_GLOBALS['field']['title']['status_sended'] = '���������';
	$_GLOBALS['field']['title']['status_ready'] = '������';
    $_GLOBALS['field']['title']['status_delivered'] = '���������';



	$_GLOBALS['field']['title']['pos'] = '�������';
	$_GLOBALS['field']['title']['uid'] = '������������';
	$_GLOBALS['field']['title']['title'] = '����� ��������';
	$_GLOBALS['field']['title']['itogo'] = '����� � ������ (���)';
	$_GLOBALS['field']['title']['incog'] = '�� �������� �� ���� �����';
	$_GLOBALS['field']['title']['foto'] = '���� ��������� �������';
	$_GLOBALS['field']['title']['amount'] = '����� ������';
	$_GLOBALS['field']['title']['count'] = '���������� �������';
	$_GLOBALS['field']['title']['dostavka'] = '����� ��������';
	$_GLOBALS['field']['title']['recipient'] = '����������';
	$_GLOBALS['field']['title']['uData'] = '������ ���������';
	$_GLOBALS['field']['title']['uName'] = '��������';
	$_GLOBALS['field']['title']['uPhone'] = '������� ���������';
	$_GLOBALS['field']['title']['uAdres'] = '����� ���������';
	$_GLOBALS['field']['title']['timeDelivery'] = '����� ��������';
	$_GLOBALS['field']['title']['status'] = '������ ������';

	$_GLOBALS['field']['title']['type'] = '������ ������';

	$_GLOBALS['field']['title']['payment'] = '��� �������';
	$_GLOBALS['field']['title']['comment'] = '�����������';
	$_GLOBALS['field']['title']['insert'] = '��������';
	$_GLOBALS['field']['title']['courier'] = '������';
	//$_GLOBALS['field']['title']['region'] = 'GeoIP';


	$_GLOBALS['field']['type']['sid'] = 'sid';
	$_GLOBALS['field']['type']['active'] = 'checkbox';

    $_GLOBALS['field']['type']['status_verified'] = 'checkbox';
    $_GLOBALS['field']['type']['status-printed'] = 'checkbox';
    $_GLOBALS['field']['type']['status_sended'] = 'checkbox';
    $_GLOBALS['field']['type']['status_delivered'] = 'checkbox';
    $_GLOBALS['field']['type']['status_ready'] = 'checkbox';


	$_GLOBALS['field']['type']['pos'] = 'position';
	$_GLOBALS['field']['type']['uid'] = 'text';
	$_GLOBALS['field']['type']['title'] = 'text';
	$_GLOBALS['field']['type']['amount'] = 'text';
	$_GLOBALS['field']['type']['count'] = 'text';
	$_GLOBALS['field']['type']['dostavka'] = 'text';
    $_GLOBALS['field']['type']['itogo'] = 'text';
    $_GLOBALS['field']['type']['courier'] = 'text';
	$_GLOBALS['field']['type']['recipient'] = 'html';
	$_GLOBALS['field']['type']['uData'] = 'html';
	$_GLOBALS['field']['type']['uName'] = 'text';
	$_GLOBALS['field']['type']['uPhone'] = 'text';
	$_GLOBALS['field']['type']['uAdres'] = 'text';
	$_GLOBALS['field']['type']['timeDelivery'] = 'text';
	$_GLOBALS['field']['type']['status'] = 'checkbox';

	$_GLOBALS['field']['type']['type'] = 'payment_list';

	$_GLOBALS['field']['type']['payment'] = 'text';
	$_GLOBALS['field']['type']['comment'] = 'textarea';

	$_GLOBALS['field']['db']['sid'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['active'] = "int(1) NOT NULL default '0'";

    $_GLOBALS['field']['db']['status_verified'] = "int(1) NOT NULL default '0'";
    $_GLOBALS['field']['db']['status-printed'] = "int(1) default '0'";
    $_GLOBALS['field']['db']['status_sended'] = "int(1) NOT NULL default '0'";
    $_GLOBALS['field']['db']['status_delivered'] = "int(1) NOT NULL default '0'";
    $_GLOBALS['field']['db']['status_ready'] = "int(1) NOT NULL default '0'";


	$_GLOBALS['field']['db']['pos'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['title'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['amount'] = "double NOT NULL default '0'";
	$_GLOBALS['field']['db']['count'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['dostavka'] = "double NOT NULL default '0'";
	$_GLOBALS['field']['db']['uData'] = "text";
	$_GLOBALS['field']['db']['uName'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['courier'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['uPhone'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['uAdres'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['timeDelivery'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['status'] = "tinyint(1) NOT NULL default '0'";
	$_GLOBALS['field']['db']['type'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['payment'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['insert'] = "timestamp NOT NULL default CURRENT_TIMESTAMP";
	$_GLOBALS['field']['db']['system'] = "int(1) NOT NULL default '0'";
	//$_GLOBALS['field']['db']['region'] = "varchar(100) default NULL";

	$_GLOBALS['field']['table']['id'] = 'text';
	$_GLOBALS['field']['table']['title'] = 'viewOrder';
    $_GLOBALS['field']['table']['uName'] = 'uid_link';
    $_GLOBALS['field']['table']['uid'] = 'nop';
	$_GLOBALS['field']['table']['itogo'] = 'text';

    $_GLOBALS['field']['table']['status_verified'] = 'status_verified';
    $_GLOBALS['field']['table']['status-printed'] = 'status_printed';
    $_GLOBALS['field']['table']['status_ready'] = 'status_ready';
    $_GLOBALS['field']['table']['status_sended'] = 'status_sended';
    $_GLOBALS['field']['table']['status_delivered'] = 'status_delivered';


    $_GLOBALS['field']['table']['status'] = 'status';
	$_GLOBALS['field']['table']['type'] = 'payment-type';
	$_GLOBALS['field']['table']['insert'] = 'text';
	//$_GLOBALS['field']['table']['region'] = 'text';

    $_GLOBALS['field']['search']['uName'] = 'uName';

	$_GLOBALS['field']['value']['active'] = 1;

	$_GLOBALS['edit-field'] = array('active', 'status_verified', 'status_sended', 'status_ready', 'status_delivered', 'status', 'title', 'amount', 'count', 'dostavka','itogo', 'type', 'recipient', 'uData', 'uName', 'uPhone', 'uAdres', 'comment', 'courier');
	$_GLOBALS['add-field'] = $_GLOBALS['edit-field'];

        $by_phone_recipient = array(
            'by_phone_only'=>"tinyint(1) NOT NULL default '0'",
            'extra_call'=>"tinyint(1) NOT NULL default '0'",
            'by_phone_only_inf'=>"text NOT NULL",
            'markup'=>'int(11) NOT NULL default 0'
        );
        $check_tbls_cols = array('basket_order'=>$by_phone_recipient);
//        $by_phone_recipient['order_mod'] = 'int(11) NOT NULL default 0';
        $check_tbls_cols['orders'] = $by_phone_recipient;
        check_tbl($check_tbls_cols);
?>