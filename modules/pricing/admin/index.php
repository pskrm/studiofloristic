<?
switch ($op) {
	case 'load-module': initPricing(); break;
	case 'set': initPricingSet(); break;
	case 'pricing-remove': pricingRemove(); break;
	case 'start-price': pricingStart(); break;
	case 'start': setStart(); break;
}

function setStart() {
	if (!_IS_ADMIN) jQuery::getResponse();
	
	db_query('UPDATE `sw_catalog` SET `price-start` = `price`');
	db_query('UPDATE `sw_catalog_price` SET `price-start` = `price`');
	
	jQuery::evalScript(inUTF8('initSysDialog("�������� ���������")'));
	jQuery::getResponse();
}

function pricingStart() {
	if (!empty($_POST['item'])) {
		foreach ($_POST['item'] as $id) {
			$id = abs(intval($id));
			$r = db_query('SELECT c.`id` FROM `sw_catalog_section` as s LEFT JOIN `sw_catalog` as c ON s.`cid` = c.`id` AND s.`first` = 1 WHERE s.`sid` = '.quote($id).' AND c.`id` IS NOT NULL');
			if (mysql_num_rows($r) != 0) {
				while ($c = mysql_fetch_array($r, MYSQL_ASSOC)) {
					$res1 = db_query('UPDATE `sw_catalog` SET `price` = `price-start`, `price-old` = ROUND(`price` + (`price` * 10 / 100)) WHERE `id` = '.quote($c['id']));
					$res2 = db_query('UPDATE `sw_catalog_price` SET `price` = `price-start`, `price-old` = ROUND(`price` + (`price` * 10 / 100)) WHERE `cid` = '.quote($c['id']));

				}
			}
			db_query('INSERT INTO `sw_pricing` (`sid`, `insert`) VALUES ('.quote($id).', UNIX_TIMESTAMP())');
		}
	}
	
	initPricing();
}

function pricingRemove() {
	if (empty($_GET['id'])) jQuery::getResponse();
	db_delete('pricing', '`id` = '.quote(abs(intval($_GET['id']))));
	jQuery::getResponse();
}

function initPricingSet() {
	if (empty($_POST['value']) || empty($_POST['type']) || empty($_POST['item'])) {
		jQuery::evalScript(inUTF8('initSysDialog("����� ��������� ��� ����")'));
		jQuery::getResponse();
	}
	
	$value = abs(intval($_POST['value']));
	
	if (empty($value)) {
		jQuery::evalScript(inUTF8('initSysDialog("� ���� ��������� ������ ���� ������ �����!")'));
		jQuery::getResponse();
	}
	
	$type = abs(intval($_POST['type']));
	$type = $type != 1 ? 2 : 1;
	
	foreach ($_POST['item'] as $id) {
		$id = abs(intval($id));
		if (!empty($id)) {
			$r = db_query('SELECT c.`id`, c.`price` FROM `sw_catalog_section` as s LEFT JOIN `sw_catalog` as c ON s.`cid` = c.`id` AND s.`first` = 1 WHERE s.`sid` = '.quote($id).' AND c.`id` IS NOT NULL');
			if (mysql_num_rows($r) != 0) {
				while ($c = mysql_fetch_array($r, MYSQL_ASSOC)) {
					db_query('UPDATE `sw_catalog` SET `price` = `price` '.($type != 2 ? '+' : '-').' ROUND(`price` * '.$value.' / 100), `price-old` = ROUND(`price` + ROUND(`price` * 10 / 100)) WHERE `id` = '.quote($c['id']));
					db_query('UPDATE `sw_catalog_price` SET `price` = `price` '.($type != 2 ? '+' : '-').' ROUND(`price` * '.$value.' / 100), `price-old` = ROUND(`price` + ROUND(`price` * 10 / 100)) WHERE `cid` = '.quote($c['id']));
				}
			}
			db_query('INSERT INTO `sw_pricing` (`value`, `type`, `sid`, `insert`) VALUES ('.quote($value).', '.quote($type).', '.quote($id).', UNIX_TIMESTAMP())');
		}
	}
	
	jQuery::evalScript(inUTF8('initSysDialog("���� ������� ��������")'));
	
	initPricing();
}

function initPricing() {
	global $level, $sValue;
	
	$html = '<p><a href="javascript:pricing.start()">������������� ��������� ����</a> - ������� ���� ����� ������������� ��� ��������� �� ������ ���������� (��� �������� � �������������� ����).</p><form id="pricing-form" action="/go/m=pricing&op=set" onsubmit="formSubmit(this); return false;">';
	$html.= '<p><select name="item[]" class="f-field width-100" size="10" multiple>'.sections(0, 0).'</select></p>';
	$html.= '<p>��������(%): <input type="text" name="value" class="f-field"></p>';
	$html.= '<p>��� ��������: <select name="type" class="f-field"><option value="1">���������</option><option value="2">���������</option></select></p>';
	$html.= '<p class="hint">��������! ���� � ��������� �������� �������� ���� ��� ��� �������, � ������� ���������� ������ �������� ��������!</p>';
	$html.= '<p class="submit"><input class="submit" type="button" value="��������" onclick="jQuery(\'form#pricing-form\').resetForm()">&nbsp;&nbsp;&nbsp;<input class="submit" type="submit" value="��������"></p>';
	$html.= '</form>';
	
	$level = 0;
	$where = null;
	$sValue = array();
	$type = array(0=>'������� ����', 1=>'<font color="green">����������</font>', 2=>'<font color="red">����������</font>');
	
	if (!empty($_POST['item'])) {
		foreach ($_POST['item'] as $id) {
			$id = abs(intval($id));
			if (!empty($id)) {
				!empty($where) ? $where.= ' OR p.`sid` = '.quote($id) : $where = ' WHERE p.`sid` = '.quote($id);
				$sValue[] = $id;
			}
		}
		
		$filter = true;
	} else $filter = false;
	
	$html.= '<h2>������� ���������������</h2>';
	$html.= '<p>������ �� ��������:'.($filter ? ' <a href="javascript:pricing.clear()">��������</a>' : null).'<br /><form id="filter-pricing" action="/go/m=pricing&op=load-module" onsubmit="formSubmit(this); return false;"><select name="item[]" class="f-field width-100" size="12" onchange="formSubmit(this.form)" multiple>'.sections(0, 0).'</select></form></p>';
	$html.= '<table id="list-items" class="items"><tr class="title"><th class="title">��������</th><th class="date">���� � �����</th><th width="20"></th></tr>';
	
	$r = db_query('SELECT p.`id`, p.`value`, p.`type`, s.`title`, p.`insert` FROM `sw_pricing` as p LEFT JOIN `sw_section` as s ON p.`sid` = s.`id`'.$where.' ORDER BY p.`id` DESC');
	while ($p = mysql_fetch_array($r, MYSQL_ASSOC)) {
		$html.= '<tr id="p'.$p['id'].'"><td class="title">�'.$p['title'].'� '.(!empty($p['type']) ? $type[$p['type']].' �� <b>'.$p['value'].'%</b>' : '<b>'.$type[$p['type']].'</b>').'</td><td class="date">'.date('d.m.Y � H:i', $p['insert']).'</td><td class="btn"><div title="�������" class="delete" onclick="pricing.remove('.$p['id'].')"></div></td></tr>';
	}
	
	$html.= '</table>';
	
	$level = 0;
	$sValue = array();
	
	$html.= '<p><h2>������� � �������������� ����</h2></p>';
	$html.= '<form id="back-pricing" action="/go/m=pricing&op=start-price" onsubmit="formSubmit(this); return false;">';
	$html.= '<p><select name="item[]" class="f-field width-100" size="14" multiple>'.sections(0, 0).'</select></p>';
	$html.= '<p class="submit"><input class="b-textfield" type="button" onclick="pricing.back()" value="���������"></p>';
	$html.= '</form>';
	
	jQuery('#content-data')->html(inUTF8($html));
	jQuery::getResponse();
}

function sections($sid = 0, $id = 0, $level = 0, $h = null) {
	global $level, $sValue;
	
	$level++;
	$line = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	$pref = null;
	
	for ($i=1;$i<$level;$i++) {
		$pref.= $line;
	}
	
	$r = db_query('SELECT s.`id`, s.`title`, (IF(d.`id` IS NULL,0,1)) as `dir` FROM `sw_section` as s LEFT JOIN `sw_section` as d ON d.`sid` = s.`id` WHERE s.`sid` = '.quote($sid).' GROUP BY s.`id` ORDER BY s.`title` ASC');
	while ($p = mysql_fetch_array($r, MYSQL_ASSOC)) {
		$h.= '<option value="'.$p['id'].'"'.(in_array($p['id'], $sValue) ? ' selected' : null).'>'.$pref.$p['title'].'</option>';
		if (!empty($p['dir'])) {
			$h.= sections($p['id'], $id, $level);
			$level--;
		}
	}
	
	return $h;
}

?>