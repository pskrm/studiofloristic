<?
	if (!defined('_SWS_ADMIN_CORE')) exit();
	
	$_GLOBALS['create_dir'] = false;
	$_GLOBALS['add'] = true;
	$_GLOBALS['search'] = true;
	$_GLOBALS['on-page'] = 20;
	$_GLOBALS['tiny'] = false;
	$_GLOBALS['date'] = false;
	
	$_GLOBALS['field']['title']['id'] = 'ID';
	$_GLOBALS['field']['title']['sid'] = '������/���������';
	$_GLOBALS['field']['title']['active'] = '���./����.';
	$_GLOBALS['field']['title']['pos'] = '�������';
	$_GLOBALS['field']['title']['title'] = '��������';
	$_GLOBALS['field']['title']['value'] = '������ � %';
	$_GLOBALS['field']['title']['min'] = '��������� �����';
	$_GLOBALS['field']['title']['max'] = '�������� �����';
	
	$_GLOBALS['field']['type']['sid'] = 'sid';
	$_GLOBALS['field']['type']['active'] = 'checkbox';
	$_GLOBALS['field']['type']['pos'] = 'position';
	$_GLOBALS['field']['type']['title'] = 'interval';
	$_GLOBALS['field']['type']['value'] = 'text';
	$_GLOBALS['field']['type']['min'] = 'text';
	$_GLOBALS['field']['type']['max'] = 'text';
	
	$_GLOBALS['field']['db']['sid'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['active'] = "int(1) NOT NULL default '0'";
	$_GLOBALS['field']['db']['pos'] = "int(11) NOT NULL default '0'";
	$_GLOBALS['field']['db']['title'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['value'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['min'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['max'] = "varchar(255) default NULL";
	$_GLOBALS['field']['db']['insert'] = "timestamp NOT NULL default CURRENT_TIMESTAMP";
	$_GLOBALS['field']['db']['system'] = "int(1) NOT NULL default '0'";
	
	$_GLOBALS['field']['table']['id'] = 'text';
	$_GLOBALS['field']['table']['title'] = 'title';
	$_GLOBALS['field']['table']['value'] = 'text';
	
	$_GLOBALS['field']['value']['active'] = 1;
	
	$_GLOBALS['edit-field'] = array('active', 'title', 'min', 'max', 'value');
	$_GLOBALS['add-field'] = $_GLOBALS['edit-field'];
?>