<?php
/**
 * Created by PhpStorm.
 * User: sshaitan
 * Date: 27.07.17
 * Time: 18:18
 *
 * ������ ���������� � �������� ����������� ������������� �� email
 * ��������� ���� 1 ��� � ���� �� ������� �� ����� � ���������� prepare - �� ������� ������ �������� �� ������� ����
 * ��� �������� ����� ���� �� ������ ���� ��������� �� ������� � ���������� send. �� �������� ��������������� ������ �� ���������� ������ ��������
 * ������� �������� ����� ����� ���� �����. ������ ���������� ������ ���� � ����.
 *
 */
ini_set('max_execution_time', 0);
error_reporting(E_ALL);
ini_set("display_errors", "on");

$path = realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..') . DIRECTORY_SEPARATOR;
@include_once($path . 'system/google.php');
$shortLink = generatorShorLink('https://www.studiofloristic.ru');

$params                     = new stdClass();
$params->days_notify        = 7; // ���������� ������������ ������ ��� �� 7 ����
$params->days_notify_second = 2; // ������ ����������� ������� ������������ �� 2 ���
$params->days_moderated     = 2; // ���� �� ���������
$params->years              = 5; // ������� ��� ���������� � �����
$params->first_template     = 'directmail7.html'; // ������ ������ �� 7
$params->first_template_admin     = 'adminDirectmail7.html'; // ������ ������ �� 7 ��� ������
$params->second_template    = 'directmail2.html'; // ������ ������ �� 2 ���. ��� ������� ����� � ����� tmpl ������ notify
$params->path               = $path; // ������ ������ �� 2 ���. ��� ������� ����� � ����� tmpl ������ notify
$params->sms                = '������������! {ORDER_DATE} �� ����� '.$shortLink.' �� ���������� ����� ������. ���� {ORDER_DAY_MONTH} - ���� ��������� ��� ��� �������, �� �� � ������������� ����������� ��� ������ 15% �� ��������� �����. ����������� ��� god15 ��� ���������� ������. ������ ��� ������� � �����������! 8 800 333-12-91'; // ������ SMS ��� ������������
$params->sms_days           = 3; // ��� ������������ ����, �� ������� ���� �� ������ ���������� sms
$params->sms_user           = "studiofloristic";
$params->sms_pass           = "Srloqj2098ed";
$params->sms_sender         = htmlspecialchars(stripslashes('SFloristic'));
$params->debug              = false;
$params->wait_check_status  = 1;

@include_once($path . 'system/config.php');
@include_once($path . 'system/db.php');
@include_once($path . 'system/filter.php');
@include_once($path . 'system/mainfile.php');
include($path . 'include/PHPMailer/src/PHPMailer.php');
include($path . 'include/PHPMailer/src/Exception.php');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

if (php_sapi_name() == "cli")
{
	if (!isset($argv[1]))
	{
		showUsage();
	}
	else
	{
		switch ($argv[1])
		{
			case 'prepare':
				prepareMails($params);
				break;
			case 'send':
				sendMails($params);
				break;
			case 'smsprepare':
				prepareSMS($params);
				break;
			case 'smssend':
				sendnotifySMS($params);
				break;
			case 'abandon':
				sendAbandon($params);
				break;
            case 'sendNotification':
                sendNotificationOfDiscount();
				break;
			default:
				showUsage("ERROR: unknown command - " . $argv[1]);
				break;
		}

	}
} else {
    // ���� ������������ ����� ��������� ��� ������ �� �������� - ���� �� ���
	header('Location: https://www.studioflorisitc.ru');
}

function sendNotificationOfDiscount()
{
    global $_GLOBALS;

    require_once __DIR__ . '/../system/google.php';

    _s("Statrt sendNotificationOfDiscount()");

    $vars    = query_new("SELECT * FROM `sw_variables` WHERE `name` = " . quote('send_message_registr_discount') . ' LIMIT 0,1');
    $vars    = $vars[0];
    $notificationSendTime = $vars['value'];

    $query = sprintf("SELECT `snd`.`id`, `snd`.`user_id`, `su`.`email`, `su`.`phone` FROM `sw_notification_discount` snd 
        LEFT JOIN `sw_users` su ON `snd`.`user_id` = `su`.`id` 
        WHERE (`created_record` + INTERVAL %s MINUTE) < NOW() AND `status` = ''", $notificationSendTime);

    $r = db_query($query );
    while ($u = mysql_fetch_array($r, MYSQL_ASSOC)) {
        if (empty($u['email']) || empty($u['user_id'])) {
            if (!empty($u['id'])) {
                $query = "DELETE FROM `sw_notification_discount` WHERE id = ".$u['id'];
                db_query($query);
            }

            continue;
        }

        $query = 'UPDATE `sw_notification_discount` SET `status` = 1 WHERE user_id = '. $u['user_id'];
        db_query($query);

        if (!class_exists('SmsQueue')) {
            include_once __DIR__ . '/../system/smsqueue.php';
        }
        $smsQueue = new SmsQueue();

        $link = 'https://studiofloristic.ru/reg-discount.html';
        $link = generatorShorLink($link);

        $isEmail = preg_match("/^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-]+)*\.([a-zA-Z]{2,6})$/", $u['email']);

        if (!$isEmail) {
           $u['phone'] = $u['email'];
        }

        $params = array(
            'text' => '������������! �� ������������������ � �� ����� studiofloristic.ru � �������� ����� ���������� ��� ����� 10% ������: reg10 ������� ��� � �������. ������ ��� ������� � �����������! 8-800-333-12-91. ��������� - '.$link,
            'msg_type' => SmsQueue::REGISTRATION_EVENT_DISCOUNT,
            'sender' => htmlspecialchars(stripslashes('SF')),
            'destination' => $u['phone'],
        );

        $smsQueue->addMessageInQueue($params);

        query_new('INSERT INTO `sw_sms_register_discount` (`user_id`) VALUES ('.quote($u['user_id']).')');

        _s('SMS Send');

        $sqlVariable = db_query('SELECT `value` FROM `sw_variables` WHERE `name` = \'message_registartion_discount\' LIMIT 1');
        $messageVariable = mysql_fetch_assoc($sqlVariable);
        $messageVariable = $messageVariable['value'];

        if (!empty($messageVariable) && !empty($u['email'])) {
            $mail = new PHPMailer;

            try {
                $mail->setFrom('info@studiofloristic.ru', 'studiofloristic.ru');
                $mail->addAddress($u['email']);
                $mail->Subject = '��� ����� 10% ������';
                $mail->isHTML(true);
                $mail->CharSet = 'windows-1251';
                $mail->AddBCC("navka_s@mail.ru", "studiofloristic");
                $mail->Body = $messageVariable;
                $mail->AltBody = strip_tags($messageVariable);
                $mail->send();

                _s('Email send');
            } catch (Exception $e) {
                unset($e);
            }
        }
    }
}

function sendAbandon($params)
{
	$sessionEmails = array();
	$sessionSms = array();
    include 'CSms4bBase.php';

	_s("Working abandon carts");
	$vars    = query_new("SELECT * FROM `sw_variables` WHERE `name` = " . quote('abandoned_cart_time') . ' LIMIT 0,1');
	$vars    = $vars[0];
	$abandonTime = $vars['value'];

	$query = sprintf("SELECT * FROM `sw_abandoned_carts` WHERE (`dt` + INTERVAL %s MINUTE) < NOW() AND `status` = ''", $abandonTime);
	$r = db_query($query );
    while ($p = mysql_fetch_array($r, MYSQL_ASSOC)) {
        _s("Working abandon id: ". $p['id']);

        $bounces = query_new("SELECT * FROM `sw_notify_unsubscribe` WHERE `email` = " . quote($p['email']) . " LIMIT 0,1");
        $bounces = @$bounces[0];
        if (is_array($bounces) && count($bounces) > 1) {
            _s('Cant send email for abandon ' . $p['id'] . ' on email ' . $p['email'] . ' - mail blacklisted! Deleting notify');
            db_query("DELETE FROM `sw_abandoned_carts`  WHERE `session` = " . quote($p['session']));

            return false;
        }

        // ���� email
        if(empty($p['email']) && filter_var($p['email'], FILTER_VALIDATE_EMAIL) === false) {
            _s("Cant send email - email not found");
        } else {
            $mailObj = new stdClass();
            $mailObj->hash = $p['hash'];
            $mailObj->email = $p['email'];
            $mailObj->name = $p['name'];
            $datas = unserialize($p['datas']);
            $mailObj->summ = $datas['discount_summ'];
            $mailObj->summ_discount = $p['summ_discount'];
            $mailObj->discount = $datas['discounts']['user'];
            $mailObj->type = 0;

            // ��������� ��� ������
            foreach ($datas['products'] as $pr) {
                $prod = new stdClass();
                $prod->id = $pr['id'];
                $prod->�id = $pr['cid'];
                $prod->title = $pr['title'];
                $prod->text = $pr['text'];
                $prod->count = $pr['count'];
                $prod->amount = $pr['mark_disc'];
                $prod->price = $pr['price'];
                $prod->url = $pr['url'];
                $prod->image = $pr['image'];
                $prod->type = $pr['type'];
                $mailObj->products[] = $prod;
            }

            if (!empty($mailObj->email)) {
                // ������� ����� ������
                $headers = "Content-type: text/html; charset=windows-1251 \r\n";
                $headers .= "From: �������� ������ <" . $vars['value'] . ">\r\n";
                $theme         = "StudioFloristic.ru - ���������� ����� �� ������� 15%!";
                $msg = renderAbandonedMail($mailObj);

                // �������� ��� � ���� �������� ��� � ������� ������� email-a
                if (in_array($mailObj->email, $sessionEmails)) {
                    db_query("UPDATE `sw_abandoned_carts` SET `status` = 1  WHERE `session` = " . quote($p['session']));
                    _s('Aready send email to this email [' . $mailObj->email. ']');
                } else {
                    $mail = new PHPMailer;
                    $mail->setFrom('info@studiofloristic.ru', 'studiofloristic.ru');
                    $mail->addAddress($mailObj->email);
                    $mail->Subject = $theme;
                    $mail->isHTML(true);
                    $mail->CharSet = 'windows-1251';
                    $mail->AddBCC("navka_s@mail.ru", "studiofloristic");
                    $mail->Body = '<html>'.$msg.'</html>';
                    $mail->AltBody = strip_tags($msg);
                    $res = $mail->send();

                    if ($res === true) {
                        $sessionEmails[] = $mailObj->email;
                        db_query("UPDATE `sw_abandoned_carts` SET `status` = 0  WHERE `session` = " . quote($p['session']));
                        _s('Done send email for abandone basket id: ' . $p['id'] . ' on email ' . $mailObj->email);
                    } else {
                        if (empty($p['phone'])) {
                            db_query("DELETE FROM `sw_abandoned_carts` WHERE `session` = " . quote($p['session']));
                        }
                        _s('Cant send email for abandone basket id: ' . $p['id'] . ' on email ' . $mailObj->email);
                    }
                }
            }
        }
    }

    $query = sprintf("SELECT * FROM `sw_abandoned_carts` WHERE (`dt` + INTERVAL %s MINUTE) < NOW() AND (`status` = '' OR `status` = '0')", $abandonTime);
    $r = db_query($query );
    while ($p = mysql_fetch_array($r, MYSQL_ASSOC)) {
        //���� sms
        if (!empty($p['phone'])) {
            if ($p['send_sms_error'] == '1') {
                continue;
            }
            $mailObj = new stdClass();
            $mailObj->hash = generatorShorLink('https://www.studiofloristic.ru/basket.html?code='.$p['hash']);
            $mailObj->phone = $p['phone'];
            $mailObj->name = $p['name'];
            $datas = unserialize($p['datas']);
            $mailObj->summ = $datas['discount_summ'];
            $mailObj->discount = $datas['discounts']['user'];
            $mailObj->type = 1;

            // ��������� ��� ������
            foreach ($datas['products'] as $pr) {
                $prod = new stdClass();
                $prod->id = $pr['id'];
                $prod->title = $pr['title'];
                $prod->text = $pr['text'];
                $prod->count = $pr['count'];
                $prod->amount = $pr['mark_disc'];
                $prod->price = $pr['price'];
                $prod->url = $pr['url'];
                $prod->image = $pr['image'];
                $mailObj->products[] = $prod;
            }

            $msg = renderAbandonedSMS($mailObj);
            if (!empty($mailObj->phone)) {
                $SMS4B = new CSms4bBase($params->sms_user, $params->sms_pass);
                $phone = $mailObj->phone;

                $sqlAbandoned = "SELECT * FROM `sw_abandoned_carts` WHERE `session` = " . quote($p['session']) . " LIMIT 1";
                $rowAbandoned = db_query($sqlAbandoned);
                while ($pAbandoned = mysql_fetch_array($r, MYSQL_ASSOC)) {
                    _s($pAbandoned['status']);
                }

                if (in_array($phone, $sessionSms)) {
                    _s('Already send sms for phone ' . $phone);
                    db_query("UPDATE `sw_abandoned_carts` SET `status` = 1  WHERE `session` = " . quote($p['session']));
                    continue;
                }

                if ($params->debug !== true) {
                    if ($SMS4B->SendSMS($msg, $phone, $params->sms_sender)) {
                        _s('DONE send sms for phone ' . $phone);
                        if ($p['status'] == '') {
                            $status = 1;
                        } elseif ($p['status'] == 0) {
                            $status = 2;
                        }
                        db_query("UPDATE `sw_abandoned_carts` SET `status` = ". $status ."  WHERE `session` = " . quote($p['session']));
                    } else {
                        db_query("UPDATE `sw_abandoned_carts` SET `send_sms_error` = 1  WHERE `session` = " . quote($p['session']));
                        _s('Cant send sms for phone ' . $p['phone'] . '. Send error! See sms4b error log. Last error: ' . $SMS4B->LastError);
                        continue;
                    }
                } else {
                    // � ������ ������ ������ ������� ��� ��������� � ������� + ��������� ��
                    echo '--------------------------------------' . PHP_EOL;
                    echo 'PHONE: ' . $phone . PHP_EOL;
                    echo 'MSG: '. PHP_EOL . iconv("WINDOWS-1251", "UTF-8", $msg ) . PHP_EOL;
                    echo '--------------------------------------' . PHP_EOL;
                    db_query("UPDATE `sw_abandoned_carts` SET `status` = 1  WHERE `session` = " . quote($p['session']));
                }

                $sessionSms[] = $phone;
            }
        }
    }
}

function sendMails($params)
{
	_s("Start sending user mails");
	//1. ������� ��� ������, ������� ���� ��������� ���� �� 7 ���� ��� ����� ������ :)

	$firstQuery = "SELECT * FROM sw_notify WHERE DATE(first_send_dt )= DATE(NOW()) AND send_count = 0 AND status = 0;";
	$r          = db_query($firstQuery);
	_s("Sending first mails");
	while ($p = mysql_fetch_array($r, MYSQL_ASSOC))
	{
		if (($p['first_send_dt'] == date("Y-m-d")) && ($p['send_count'] == 0))
		{
			sendFirstEmail($p, $params);
		}
		else
		{
			_s('Cant send email. See it manualy notify_id ' . $p['id']);
		}

	}
	_s("Sending second mails");
	$secondQuery = "SELECT * FROM sw_notify WHERE DATE(second_send_dt )= DATE(NOW()) AND send_count = 1 AND status = 2;";
	$r           = db_query($secondQuery);
	while ($p = mysql_fetch_array($r, MYSQL_ASSOC))
	{
		if (($p['second_send_dt'] == date("Y-m-d")) && ($p['send_count'] == 1))
		{
			sendSecondEmail($p, $params);
		}
		else
		{
			_s('Cant send email. See it manualy notify_id ' . $p['id']);
		}

	}

	_s("DONE sending user mails");

}

function sendnotifySMS($params)
{
	if (!class_exists('CSms4bBase'))
	{
		include 'CSms4bBase.php';
		include 'sms4bChecker.php';

	}

	_s("Start sending user notify SMS");
	//1. ������� ��� SMS, ������� ���� ��������� ���� �� 3 ���� �� ������

	$firstQuery = "SELECT * FROM sw_notify_sms WHERE DATE(first_send_dt )= DATE(NOW()) AND status = 0;";
	$r          = db_query($firstQuery);

	while ($p = mysql_fetch_array($r, MYSQL_ASSOC))
	{
		$SMS4B = new CSms4bBase($params->sms_user, $params->sms_pass);
		_s("Checking params ...");
		// ��� ���� �������� �� ���� �� ���������� ��� ��� ��� ����� ����� - 1 ��� � �����
		$is_sended_q = query_new("SELECT `when` FROM `sw_notify_sms_log` WHERE `phone` = " . quote($p['phone']) . " ORDER BY id DESC LIMIT 0,1", true);
		$is_sended   = $is_sended_q['when'];
		$month_diff  = (int) abs((strtotime(date('Y-m-d')) - strtotime($is_sended)) / (60 * 60 * 24 * 30));
		if ($month_diff <= 1)
		{
			_s("Cant send sms, already send in current month on phone " . $p['phone']);
			db_query("UPDATE `sw_notify_sms` SET `status` = 2 WHERE `id` = " . quote($p['id']));
			continue;
		}


		if (($p['first_send_dt'] == date("Y-m-d")) && ($p['status'] == 0))
		{
			// ��� �������, ���������� sms ����� � ���������� � ���

			$orderMonthTxt = getRusMonth(date("m", strtotime($p['order_dt'])));
			$orderDay      = date("d", strtotime($p['order_dt']));
			$orderYear     = date("Y", strtotime($p['order_dt']));
			$msg           = str_replace('{ORDER_DATE}', date("d-m-Y", strtotime($p['order_dt'])), $params->sms);
			$msg           = str_replace('{ORDER_DAY_MONTH}', $orderDay . ' ' . $orderMonthTxt, $msg);
			$phone         = is_phone($p['phone']);
			if ($params->debug !== true)
			{
				if ($SMS4B->SendSMS($msg, $phone, $params->sms_sender))
				{
					_s('DONE send sms for phone ' . $phone);
					db_query("UPDATE `sw_notify_sms` SET `status` = 1, `send_dt` = NOW() WHERE `id` = " . quote($p['id']));
					db_query("INSERT INTO `sw_notify_sms_log` (`phone`, `when`, `gid`)
								VALUES
  								(" . quote($phone) . ", " . quote(date("Y-m-d")) . ", " .quote($SMS4B->current_guid). ");
								");
					// �������� ���� ������� � �������� ���������� ��� ���
					sleep($params->wait_check_status);
					$checker = new sms4bChecker($params->sms_user, $params->sms_pass);
					$resCheck = $checker->checkSMS($SMS4B->current_guid);
					if ($resCheck === true)
					{
						db_query("UPDATE `sw_notify_sms` SET `opened_count` = 1 WHERE `id` = " . quote($p['id']));
					}


				}
				else
				{
					_s('Cant send sms for phone ' . $p['phone'] . '. Send error! See sms4b error log. Last error: ' . $SMS4B->LastError);
					db_query("UPDATE `sw_notify_sms` SET `status` = 3 WHERE `id` = " . quote($p['id']));
					continue;
				}
			}
			else
			{
				// � ������ ������ ������ ������� ��� ��������� � ������� ��� ��������� ��
				echo '--------------------------------------' . PHP_EOL;
				echo 'PHONE: ' . $phone . PHP_EOL;
				echo 'MSG: '. PHP_EOL . iconv("WINDOWS-1251", "UTF-8", $msg ) . PHP_EOL;
				echo '--------------------------------------' . PHP_EOL;
			}

		}
		else
		{
			_s('Cant send sms for phone ' . $p['phone'] . '. Status or date error. See it manualy notify_id ' . $p['id']);
		}

	}

	_s("DONE sending user mails");

}


function prepareMails($params)
{
	$resData = array();
	_s("Starting preparation for MANY_YEAR_MAILS");
	$comp = $params->days_notify + $params->days_moderated;

	// ������ ����� �������� �����

	for ($i = 1; $i <= $params->years; $i++)
	{
		$s_time_str = sprintf('now -%s year + %s days', $i, $comp);

		$timestamp_start = date("Y-m-d 00:00:00", strtotime($s_time_str));
		$timestamp_end   = date("Y-m-d 23:59:59", strtotime($s_time_str));
		_s("Using timestamp: " . $timestamp_start . " to " . $timestamp_end);
		/*	$query   = sprintf("
								SELECT
								  *
								FROM
								  `sw_orders`
								WHERE `active` = 1
								  AND `status_verified` = 1
								  AND `status_delivered` = 1
								  AND `status` = 1
								  AND `email` <> ''
								  AND `insert` BETWEEN '%s'
								  AND '%s'
								GROUP BY `session` ORDER BY id DESC;", $timestamp_start, $timestamp_end);
		*/
		$query = sprintf("
							SELECT
							  *
							FROM
							  `sw_orders`
							WHERE 
							  `status` = 1
							  AND `email` <> '' 
							  AND `delivery_dt` BETWEEN '%s'
							  AND '%s'
							GROUP BY `session` ORDER BY id DESC;", $timestamp_start, $timestamp_end);

		if ($params->debug)
		{
			echo $query . PHP_EOL;
		}
		$r       = db_query($query);

		while ($p = mysql_fetch_array($r, MYSQL_ASSOC))
		{
			//TODO: ���� ���� �������� �������� ��� ���� ���� ��� ����� ���� ����, �� ��� �� ���������
			//TODO: � ��� ���� ��������� � ������ ������������
			$resData[] = $p;
		}
	}
	// ����� ����� �������� �����

	$total = count($resData);
	_s("Found " . $total . " uniq completed orders");
	_s("Preparing mails...");
	foreach ($resData as $order)
	{
		$insert = array('name' => $order['uName'], 'order' => $order['id'], 'order_date' => $order['delivery_dt'],
		                'hash' => $order['session'], 'email' => $order['email'], 'phone' => $order['phone-sys'],
		                'crc'  => hash('sha256', $order['email'] . $order['id']));
		$result = insertMail($insert, $params);
		if ($result !== false)
		{
			_s("Added mail with id: " . $result);
		}

	}
	_s("Preparation complete! Exiting with code 0");

}

function prepareSMS($params)
{

	_s("Starting preparation for MANY_YEAR_SMSs");
	$comp = $params->sms_days + $params->days_moderated;

	// ������ ����� �������� �����
	$resData = array();
	for ($i = 1; $i <= $params->years; $i++)
	{
		$s_time_str = sprintf('now -%s year + %s days', $i, $comp);

		$timestamp_start = date("Y-m-d 00:00:00", strtotime($s_time_str));
		$timestamp_end   = date("Y-m-d 23:59:59", strtotime($s_time_str));
		_s("Using timestamp: " . $timestamp_start . " to " . $timestamp_end);
		/*	$query   = sprintf("
								SELECT
								  *
								FROM
								  `sw_orders`
								WHERE
								  `active` = 1
								  AND `status_verified` = 1
								  AND `status_delivered` = 1
								  AND `status` = 1
								  AND `email` = ''
								  AND `phone-sys` <> ''
								  AND `insert` BETWEEN '%s'
								  AND '%s'
								GROUP BY `session` ORDER BY id DESC;", $timestamp_start, $timestamp_end);
		*/
		$query = sprintf("
							SELECT
							  *
							FROM
							  `sw_orders`
							WHERE
							  `status` = 1
							  AND `phone-sys` <> ''
							  AND `delivery_dt` BETWEEN '%s'
							  AND '%s'
							GROUP BY `session` ORDER BY id DESC;", $timestamp_start, $timestamp_end);
		$r     = db_query($query);

		while ($p = mysql_fetch_array($r, MYSQL_ASSOC))
		{
			//TODO: ���� ���� �������� �������� ��� ���� ���� ��� ����� ���� ����, �� ��� �� ���������
			//TODO: � ��� ���� ��������� � ������ ������������
			if (!$p) continue;
			$resData[] = $p;
		}
	}
	// ����� ����� �������� �����

	$total = count($resData);
	_s("Found " . $total . " uniq completed orders");
	_s("Preparing smss...");
	foreach ($resData as $order)
	{
		$insert = array('name' => $order['uName'], 'order' => $order['id'], 'order_date' => $order['delivery_dt'],
		                'hash' => $order['session'], 'email' => '', 'phone' => $order['phone-sys'],
		                'crc'  => hash('sha256', $order['phone-sys'] . $order['id']));
		$result = insertSMS($insert, $params);
		if ($result !== false)
		{
			_s("Added sms with id: " . $result);
		}

	}
	_s("Preparation complete! Exiting with code 0");

}

function showUsage($append)
{
	if (!empty($append))
	{
		echo "Notify user by mail helper script" . PHP_EOL;
		echo $append . PHP_EOL;
		echo "Usage: php notify.php prepare  - its for prepare nails in DB" . PHP_EOL;
		echo "php notify.php send - its for send mails" . PHP_EOL;
		echo "php notify.php smsprepare - its for prepare sms" . PHP_EOL;
		echo "php notify.php smssend - its for send sms" . PHP_EOL;
	}
	else
	{
		echo "Notify user by mail helper script" . PHP_EOL;
		echo "Usage: php notify.php prepare  - its for prepare nails in DB" . PHP_EOL;
		echo "php notify.php send - its for send mails" . PHP_EOL;
		echo "php notify.php smsprepare - its for prepare sms" . PHP_EOL;
		echo "php notify.php smssend - its for send sms" . PHP_EOL;

	}


	die;
}

function _s($str)
{
    echo $str . PHP_EOL;
}

function insertMail($data, $params)
{
	if (!is_array($data))
	{
		trigger_error("Failed add mail to table!", E_USER_WARNING);

		return false;

	}
	else
	{
		if (!isset($data['name']) || empty($data['name']))
		{
			trigger_error("Empty name! Cant insert mail", E_USER_WARNING);

			return false;
		}
		if (!isset($data['order']) || empty($data['order']))
		{
			trigger_error("Empty order! Cant insert mail", E_USER_WARNING);

			return false;
		}
		if (!isset($data['order_date']) || empty($data['order_date']))
		{
			trigger_error("Empty order insert date! Cant insert mail", E_USER_WARNING);

			return false;
		}
		if (!isset($data['hash']) || empty($data['hash']))
		{
			trigger_error("Empty order hash ! Cant insert mail", E_USER_WARNING);

			return false;
		}

		if (!isset($data['email']) || empty($data['email']))
		{
			trigger_error("Empty order email! Cant insert mail", E_USER_WARNING);

			return false;
		}

		/**
		 * simple php mail validation
		 */
		if (filter_var($data['email'], FILTER_VALIDATE_EMAIL) === false)
		{
			trigger_error("Not valid email! Cant insert mail", E_USER_WARNING);

			return false;
		}

		$query = sprintf("
						SELECT id FROM `sw_notify`
						WHERE hash = '%s'
						", $data['hash']);
		$res   = db_query($query);
		$value = mysql_fetch_object($res);
		if ($value !== false)
		{
			trigger_error("Cant insert mail, already exists with id " . $value->id, E_USER_WARNING);

			return false;
		}
		// check crc
		$query = sprintf("
						SELECT id FROM `sw_notify`
						WHERE crc = '%s'
						", $data['crc']);
		$res   = db_query($query);
		$value = mysql_fetch_object($res);
		if ($value !== false)
		{
			trigger_error("Cant insert mail, already exists with id " . $value->id, E_USER_WARNING);

			return false;
		}

		if (!isset($data['order_date']) || empty($data['order_date']))
		{
			trigger_error("Empty order DATE! Cant insert mail", E_USER_WARNING);

			return false;
		}
		$first_send_date  = date("Y-m-d", strtotime($data['order_date'] . ' -' . $params->days_notify . ' days'));
		$second_send_date = date("Y-m-d", strtotime($data['order_date'] . ' -' . $params->days_notify_second . ' days'));;

		$years_diff = round((time() - strtotime($data['order_date'])) / (3600 * 24 * 365.25));

		$first_send_date  = date("Y-m-d", strtotime($first_send_date . ' + ' . $years_diff . ' year'));
		$second_send_date = date("Y-m-d", strtotime($second_send_date . ' + ' . $years_diff . ' year'));


		$query = sprintf("
						INSERT INTO `sw_notify` (
						  `name`,
						  `order`,
						  `message`,
						  `status`,
						  `cr_dt`,
						  `mod_dt`,
						  `send_dt`,
						  `opened`,
						  `order_dt`,
						  `type`,
						  `hash`,
						  `email`,
						  `phone`,
						  `crc`,
						  `first_send_dt`,
						  `second_send_dt`
						)
						VALUES
						  (
						    '%s',
						    '%s',
						    '',
						    '0',
						    '%s',
						    '%s',
						    '0000:00:00 00:00:00',
						    '0',
						    '%s',
						    'years',
						    '%s',
						    '%s',
						    '%s',
						    '%s',
						    '%s',
						    '%s'
						  );

		", $data['name'], $data['order'], date("Y-m-d H:i:s"), date("Y-m-d H:i:s"), date("Y-m-d H:i:s", strtotime($data['order_date'])),
			$data['hash'], $data['email'], $data['phone'], hash('sha256', $data['email'] . $data['order']), $first_send_date, $second_send_date);
		db_query($query);
		$res   = db_query("SELECT LAST_INSERT_ID() as id");
		$value = mysql_fetch_object($res);

		return $value->id;

	}

}


function insertSMS($data, $params)
{
	if (!is_array($data))
	{
		trigger_error("Failed add sms to table!", E_USER_WARNING);

		return false;

	}
	else
	{

		if (!isset($data['order']) || empty($data['order']))
		{
			trigger_error("Empty order! Cant insert sms", E_USER_WARNING);

			return false;
		}
		if (!isset($data['order_date']) || empty($data['order_date']))
		{
			trigger_error("Empty order insert date! Cant insert sms", E_USER_WARNING);

			return false;
		}
		if (!isset($data['hash']) || empty($data['hash']))
		{
			trigger_error("Empty order hash ! Cant insert sms", E_USER_WARNING);

			return false;
		}

		if (!isset($data['phone']) || empty($data['phone']))
		{
			trigger_error("Empty order phone! Cant insert sms", E_USER_WARNING);

			return false;
		}


		$query = sprintf("
						SELECT id FROM `sw_notify_sms`
						WHERE hash = '%s'
						", $data['hash']);
		$res   = db_query($query);
		$value = mysql_fetch_object($res);
		if ($value !== false)
		{
			trigger_error("Cant insert sms, already exists with id " . $value->id, E_USER_WARNING);

			return false;
		}
		// check crc
		$query = sprintf("
						SELECT id FROM `sw_notify_sms`
						WHERE crc = '%s'
						", $data['crc']);
		$res   = db_query($query);
		$value = mysql_fetch_object($res);
		if ($value !== false)
		{
			trigger_error("Cant insert sms, already exists with id " . $value->id, E_USER_WARNING);

			return false;
		}

		if (!isset($data['order_date']) || empty($data['order_date']))
		{
			trigger_error("Empty order DATE! Cant insert sms", E_USER_WARNING);

			return false;
		}
		// �������� ������� � �����
		$years_diff = round((time() - strtotime($data['order_date'])) / (3600 * 24 * 365.25));

		$first_send_date = date("Y-m-d", strtotime($data['order_date'] . ' -' . $params->sms_days . ' days'));
		$first_send_date = date("Y-m-d", strtotime($first_send_date . ' + ' . $years_diff . ' year'));


		$query = sprintf("
						INSERT INTO `sw_notify_sms` (
						  `name`,
						  `order`,
						  `status`,
						  `cr_dt`,
						  `send_dt`,
						  `order_dt`,
						  `hash`,
						  `phone`,
						  `crc`,
						  `first_send_dt`
						)
						VALUES
						  (
						    '%s',
						    '%s',
						    '0',
						    '%s',
						    '0000-00-00 00:00:00',
						    '%s',
						    '%s',
						    '%s',
						    '%s',
						    '%s'
						  );

		", $data['name'], $data['order'], date("Y-m-d H:i:s"), date("Y-m-d H:i:s", strtotime($data['order_date'])),
			$data['hash'], $data['phone'], hash('sha256', $data['phone'] . $data['order']), $first_send_date);
		db_query($query);
		$res   = db_query("SELECT LAST_INSERT_ID() as id");
		$value = mysql_fetch_object($res);

		return $value->id;

	}

}

function sendFirstEmail($preparedData, $params)
{
	$vars    = query_new("SELECT * FROM `sw_variables` WHERE name = " . quote('email_admin') . ' LIMIT 0,1');
	$vars    = $vars[0];
	$headers = "Content-type: text/html; charset=windows-1251 \r\n";
	$headers .= "From: �������� ������ <" . $vars['value'] . ">\r\n";

	$tmpl = $params->path . 'modules/notify/tmpl/' . $params->first_template;
	if (!file_exists($tmpl))
	{
		trigger_error("Error template file not found! " . $tmpl, E_USER_ERROR);
		exit;
	}

	$tmplAdmin = $params->path . 'modules/notify/tmpl/' . $params->first_template_admin;
	if (!file_exists($tmplAdmin))
	{
		trigger_error("Error template file not found! " . $tmplAdmin, E_USER_ERROR);
		exit;
	}

	if (filter_var($preparedData['email'], FILTER_VALIDATE_EMAIL) === false)
	{
		trigger_error("Failed email " . $preparedData['email'], E_USER_WARNING);

		return false;
	}

	$email         = $preparedData['email'];
	$template      = file_get_contents($tmpl);
	$templateAdmin = file_get_contents($tmplAdmin);
	$discount      = '10';
	$kupon         = 'god10';
	$orderDay      = date("d", strtotime($preparedData['order_dt']));
	$orderMonthTxt = getRusMonth(date("m", strtotime($preparedData['order_dt'])));

	$template      = str_replace('{%ORDER_DAY_MONTH_TXT%}', $orderDay . ' ' . $orderMonthTxt, $template);
	$template      = str_replace('{%ORDER_DATE%}', date("d-m-Y", strtotime($preparedData['order_dt'])), $template);
	$template      = str_replace('{%DISCOUNT%}', $discount, $template);
	$template      = str_replace('{%KUPON%}', $kupon, $template);
	$template      = str_replace('{%CRC%}', $preparedData['crc'], $template);
	$template      = str_replace('{%ORDER_DAY_MONTH_TXT%}', $orderDay . ' ' . $orderMonthTxt, $template);

    $templateAdmin      = str_replace('{%ORDER_DAY_MONTH_TXT%}', $orderDay . ' ' . $orderMonthTxt, $templateAdmin);
    $templateAdmin      = str_replace('{%ORDER_DATE%}', date("d-m-Y", strtotime($preparedData['order_dt'])), $templateAdmin);
    $templateAdmin      = str_replace('{%DISCOUNT%}', $discount, $templateAdmin);
    $templateAdmin      = str_replace('{%KUPON%}', $kupon, $templateAdmin);
    $templateAdmin      = str_replace('{%CRC%}', $preparedData['crc'], $templateAdmin);
    $templateAdmin      = str_replace('{%ORDER_DAY_MONTH_TXT%}', $orderDay . ' ' . $orderMonthTxt, $templateAdmin);

	$theme         = "StudioFloristic.ru - ������ " . $discount . "%";
	// �������� ��� ���� �� � ������ ������
	$bounces = query_new("SELECT * FROM sw_notify_unsubscribe WHERE email = " . quote($email) . " LIMIT 0,1");
	$bounces = @$bounces[0];
	if (is_array($bounces) && count($bounces) > 1)
	{
		_s('Cant send email for order ' . $preparedData['order'] . ' on email ' . $email . ' - mail blacklisted! Deleting notify');
		db_query("DELETE FROM `sw_notify`  WHERE id = " . quote($preparedData['id']));

		return false;
	}
	// �������� � �� ���������� �� �� ��� �� ���� ����� � ��������� 30 ���� ���� ��������


	error_reporting(E_ALL);
	ini_set('display_errors', 'on');


	$mail = new PHPMailer;
	$mail->setFrom('info@studiofloristic.ru', 'studiofloristic.ru');
	$mail->addAddress($email);
	$mail->Subject = $theme;
	$mail->isHTML(true);
	$mail->CharSet = 'windows-1251';
	$mail->Body = '<html>'.$template.'</html>';
	$mail->AltBody = strip_tags($msg);
	$res = $mail->send();

    $mail = new PHPMailer;
    $mail->setFrom('info@studiofloristic.ru', 'studiofloristic.ru');
    $mail->addAddress('navka_s@mail.ru');
    $mail->Subject = $theme;
    $mail->isHTML(true);
    $mail->CharSet = 'windows-1251';
    $mail->Body = '<html>'.$templateAdmin.'</html>';
    $mail->AltBody = strip_tags($msg);
    $res = $mail->send();

	if ($res)
	{
		db_query("UPDATE `sw_notify` SET send_count = 1, send_dt = NOW(), status = 2  WHERE id = " . quote($preparedData['id']));
		_s('Done send first email for order ' . $preparedData['order'] . ' on email ' . $email);
		db_query("INSERT INTO `sw_notify_log` (`email`, `sended`, `params`) VALUES   ('" . $email . "', NOW(), '" . serialize($preparedData) . "');");

		return true;
	}
	else
	{
		_s('Cant send email for order ' . $preparedData['order'] . ' on email ' . $email . ' - mail will be deleted');
		db_query("DELETE FROM `sw_notify`  WHERE id = " . quote($preparedData['id']));

		return false;
	}


}

function sendSecondEmail($preparedData, $params)
{

	$vars    = query_new("SELECT * FROM `sw_variables` WHERE name = " . quote('email_admin') . ' LIMIT 0,1');
	$vars    = $vars[0];
	$headers = "Content-type: text/html; charset=windows-1251 \r\n";
	$headers .= "From: �������� ������ <" . $vars['value'] . ">\r\n";

	$tmpl = $params->path . 'modules/notify/tmpl/' . $params->second_template;
	if (!file_exists($tmpl))
	{
		trigger_error("Error template file not found! " . $tmpl, E_USER_ERROR);
		exit;

	}

	if (filter_var($preparedData['email'], FILTER_VALIDATE_EMAIL) === false)
	{
		trigger_error("Failed email " . $preparedData['email'], E_USER_WARNING);

		return false;
	}

	if ($preparedData['send_count'] != 1)
	{
		trigger_error("Failed email " . $preparedData['email'] . " failed send_count. Send_count must be 1", E_USER_WARNING);

		return false;
	}

	if ($preparedData['status'] != 2)
	{
		trigger_error("Failed email " . $preparedData['email'] . " failed status. STATUS must be 2", E_USER_WARNING);

		return false;
	}

	$email         = $preparedData['email'];
	$template      = file_get_contents($tmpl);
	$discount      = '15';
	$kupon         = 'god15';
	$orderDay      = date("d", strtotime($preparedData['order_dt']));
	$orderMonthTxt = getRusMonth(date("m", strtotime($preparedData['order_dt'])));
	$template      = str_replace('{%ORDER_DAY_MONTH_TXT%}', $orderDay . ' ' . $orderMonthTxt, $template);
	$template      = str_replace('{%DISCOUNT%}', $discount, $template);
	$template      = str_replace('{%ORDER_DATE%}', date("d-m-Y", strtotime($preparedData['order_dt'])), $template);
	$template      = str_replace('{%KUPON%}', $kupon, $template);
	$template      = str_replace('{%CRC%}', $preparedData['crc'], $template);
	$theme         = "StudioFloristic.ru - ������ " . $discount . "%";
	// �������� ��� ���� �� � ������ ������
	$bounces = query_new("SELECT * FROM sw_notify_unsubscribe WHERE email = " . quote($email) . " LIMIT 0,1");
	$bounces = @$bounces[0];
	if (is_array($bounces) && count($bounces) > 1)
	{
		_s('Cant send email for order ' . $preparedData['order'] . ' on email ' . $email . ' - mail blacklisted! Deleting notify');
		db_query("DELETE FROM `sw_notify`  WHERE id = " . quote($preparedData['id']));

		return false;
	}
	error_reporting(E_ALL);
	ini_set('display_errors', 'on');

	$mail = new PHPMailer;
	$mail->setFrom('info@studiofloristic.ru', 'studiofloristic.ru');
	$mail->addAddress($email);
	$mailer->AddBCC("navka_s@mail.ru", "studiofloristic");
	$mail->Subject = $theme;
	$mail->isHTML(true);
	$mail->CharSet = 'windows-1251';
	$mail->Body = '<html>'.$template.'</html>';
	$mail->AltBody = strip_tags($msg);		
	$res = $mail->send();

	if ($res)
	{
		db_query("UPDATE `sw_notify` SET send_count = 2, send_dt = NOW(), status = 3  WHERE id = " . quote($preparedData['id']));
		_s('Done send second email for order ' . $preparedData['order'] . ' on email ' . $email);
		db_query("INSERT INTO `sw_notify_log` (`email`, `sended`, `params`) VALUES   ('" . $email . "', NOW(), '" . serialize($preparedData) . "');");

		return true;
	}
	else
	{
		_s('Cant send email for order ' . $preparedData['order'] . ' on email ' . $email . ' - mail will be deleted');
		db_query("DELETE FROM `sw_notify`  WHERE id = " . quote($preparedData['id']));

		return false;
	}


}

function getRusMonth($month_num)
{
	switch ($month_num)
	{
		case 1:
			$m = '������';
			break;
		case 2:
			$m = '�������';
			break;
		case 3:
			$m = '�����';
			break;
		case 4:
			$m = '������';
			break;
		case 5:
			$m = '���';
			break;
		case 6:
			$m = '����';
			break;
		case 7:
			$m = '����';
			break;
		case 8:
			$m = '�������';
			break;
		case 9:
			$m = '��������';
			break;
		case 10:
			$m = '�������';
			break;
		case 11:
			$m = '������';
			break;
		case 12:
			$m = '�������';
			break;
		default:
			return false;
			break;
	}

	return $m;
}

function is_phone($destination_numbers)
{
	$destination_number = trim($destination_numbers);

	$arInd = trim($destination_numbers);
	$arInd = trim($destination_numbers, "+");

	$symbol   = false;
	$spec_sym = array("+", "(", ")", " ", "-", "_");
	for ($i = 0; $i < strlen($arInd); $i++)
	{
		if (!is_numeric($arInd[$i]) && !in_array($arInd[$i], $spec_sym))
		{
			$symbol = true;
		}
	}

	if ($symbol)
	{
		return false;
	}
	else
	{
		$arInd = str_replace($spec_sym, "", $arInd);

		if (strlen($arInd) < 4 || strlen($arInd) > 11)
		{
			return false;
		}
		else
		{
			if (strlen($arInd) == 10)
			{
				$arInd = "7" . $arInd;
			}

			if (strlen($arInd) == 11 && $arInd[0] == '8')
			{
				$arInd[0] = "7";
			}

			return $arInd;
		}
	}

	$numbers = array_unique($numbers);

	return $numbers;
}