<?php
/**
 * Created by PhpStorm.
 * User: sshaitan
 * Date: 15.09.17
 * Time: 15:20
 *
 * ����� ��� �������� ������� SMS ��� sms4b
 */

class sms4bChecker
{
	public $login;
	public $password;
	public $guid;

	const url = 'https://sms4b.ru/ws/sms.asmx/';
	const gmt = 3;
	private $session_id;

	/**
	 * sms4bChecker constructor.
	 */
	public function __construct($sms4bLogin, $sms4bPassword)
	{
		if ((empty($sms4bLogin)) || (empty($sms4bPassword)) )
		{
			throw new Exception('Login or password SMS4B not set');
		}

		$this->login = $sms4bLogin;
		$this->password = $sms4bPassword;

	}


	/**
	 * ��������� ������ � ���������� �� �������������
	 * @return int
	 * @throws Exception
	 */
	private function startSession()
	{
		$sid = strip_tags($this->SendPOST(
			array(
				'Login' => $this->login,
				'Password' => $this->password,
				'Gmt' => self::gmt
			),
			'StartSession',
			self::url
		));

		$this->session_id = 0;
		if((int)$sid <= 0)
		{
			throw new Exception('Cant start session. Session is empty');
		}
		$this->session_id = (int)$sid;
		return (int) $sid;
	}


	/**
	 * ������� �������� ������ SMS
	 * ���������� ������
	 * true - ����������
	 * false - �� ���������� :)
	 *
	 * @param $smsId
	 *
	 * @return bool
	 */
	public function checkSMS($smsId)
	{
		$this->guid = $smsId;
		$arRes = $this->checkSMSStatus();
		if((int)$arRes['R'] > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}


	/**
	 * ���������� ������ �������� ��� SMS
	 * @param $smsId
	 *
	 * @return array
	 */
	public function getSMSStatus($smsId)
	{
		$this->guid = $smsId;
		return $this->checkSMSStatus();
	}

	/**
	 * ��������� ������ sms
	 * @return array
	 * @throws Exception
	 */
	public function checkSMSStatus()
	{
		if (empty($this->guid))
		{
			throw new Exception('SMS id is empty');
			return array();
		}
		$this->startSession();
		$status = $this->SendPOST(
			array(
				'SessionId' => $this->session_id,
				'Guids' => $this->guid
			),
			'CheckSMS', self::url);

		$obStat = simplexml_load_string($status);
		$arRes = (array)$obStat->List->CheckSMSList;
		$this->endSession();
		return $arRes;
	}


	/** �������� ������
	 * @return bool
	 * @throws Exception
	 */
	private function endSession()
	{
		$resultCloseSid = (int)$this->SendPOST(
			array('SessionID' => $this->session_id),
			'CloseSession',
			self::url
		);

		if($resultCloseSid !== 0)
		{
			throw new Exception('Cant close session. Error code : ' . $resultCloseSid);
			return false;
		}

		return true;
	}


	/**
	 * �������� POST ������� � API sms4b
	 * @param $arParam ������ POST ����������
	 * @param $methodName ��� API �������
	 * @param $url URL ��� API
	 *
	 * @return bool|string
	 */
	private function SendPOST($arParam, $methodName, $url = self::url)
	{
		    $postdata = http_build_query($arParam);

		    $opts = array(
		        'http' =>
		            array(
		                'method' => 'POST',
		                'header' => 'Content-type: application/x-www-form-urlencoded' . PHP_EOL,
		                'content' => $postdata
		            )
		    );

		    $context = stream_context_create($opts);
		    return file_get_contents($url . $methodName, false, $context);
	}
}