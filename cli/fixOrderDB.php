<?php
/**
 * Created by PhpStorm.
 * User: sshaitan
 * Date: 22.09.17
 * Time: 14:50
 */

ini_set('max_execution_time', 0);
error_reporting(E_ALL);
ini_set("display_errors", "on");


$path = realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..') . DIRECTORY_SEPARATOR;


@include_once($path . 'system/config.php');
@include_once($path . 'system/db.php');
@include_once($path . 'system/filter.php');
@include_once($path . 'system/mainfile.php');

/*
_s("Gathering all orders for patching emails");

$r = db_query("SELECT * FROM `sw_orders` WHERE `email` = '' AND `uData` LIKE '%mailto%';");

_s("Found " . mysql_num_rows($r) . " orders without email");
_s("=====================================================");
while ($order = mysql_fetch_object($r))
{
	_s("Working order:" . $order->id);
	//$email  =  strip_tags('<p><a href="'. mb_substr($order->uData, strpos($order->uData, 'mailto', 0), mb_strpos($order->uData, '</a>', strpos($order->uData, 'mailto', 0) ) ) ) ;
	$email  =  strip_tags('<a href="mailto'.get_string_between($order->uData, 'mailto', '/a') . '</a>') ;
	_s("Found email:" . $email);
	$email = filter_var($email, FILTER_VALIDATE_EMAIL);
	if ($email !== false)
	{

		_s($email . " checked OK!");
		$updateQuery = sprintf("UPDATE `sw_orders` SET `email` = '%s' WHERE `id` = %d;", $email, (int)$order->id);
		$res = db_query($updateQuery );
		if ($res){
			_s("Email fixed for id: [" . $order->id."]->[$email]");
	}
	else
	{
		_s("Error in email [" . $email . "]");
	}
_s("-----------------------------------------------------");
	}
	else
	{
		_s("Error in email [" . $email . "]");
	}

}
*/
_s("Gathering all orders for patching time delivery");

//SELECT * FROM `sw_orders` WHERE `delivery_dt` = '' OR  ISNULL( `delivery_dt` ) ;
$r = db_query("SELECT * FROM `sw_orders` WHERE `delivery_dt` = '' OR  ISNULL( `delivery_dt` ) ;");

_s("Found " . mysql_num_rows($r) . " orders without delivery_dt");
_s("=====================================================");

while ($order = mysql_fetch_object($r))
{
	_s("Working order:" . $order->id);

	$timeDelivery  = str_replace("  ", " ", trim(str_replace(array("���������", ":"), "", $order->title))) ;
	if (empty($timeDelivery))
	{
		_s("Delivery datetime problem for order: [" . $order->id. "]" );
		$delivery_dt = date("Y-m-d H:i:s", strtotime($order->insert));
	}
	else
	{
		// ���������� ������
		$parts = explode(" ", $timeDelivery);
		if (isset($parts[1]))
		{
			if (strpos($parts[1], "-") !== false)
			{
				// ���������� ��������
				list($startTime, $endTime) = explode(" - ", $parts[1]);
				$hours = substr($startTime, 0, 2);
				$minut = substr($startTime, 2, 4);
				$delivery_dt = date("Y-m-d H:i:s", strtotime(trim($parts[0]) . " " . $hours . ":" . $minut . ":" . "00" ));
			}
			else
			{
				// � ��� ��������� ����
				$hours = substr($parts[1], 0, 2);
				$minut = substr($parts[1], 2, 4);
				$delivery_dt = date("Y-m-d H:i:s", strtotime(trim($parts[0]) . " " . $hours . ":" . $minut . ":" . "00" ));
			}
		}
		else
		{
				$delivery_dt = date("Y-m-d H:i:s", $parts[0]);
		}

	}
	$updatus = sprintf("UPDATE `sw_orders` SET `delivery_dt` = '%s' WHERE id = %d;",$delivery_dt , (int) $order->id );
	$res = db_query($updatus );
	if ($res)
	{
		_s("Delivery fixed for id: [" . $order->id."]->[$delivery_dt]");
	}
	else
	{
		_s("Delivery fixed FAILED id: [" . $order->id."]->[$delivery_dt]");
	}


}


/** UTILS */

function _s($str)
{
	echo $str . PHP_EOL;

}

function get_string_between($string, $start, $end){
	$string = ' ' . $string;
	$ini = strpos($string, $start);
	if ($ini == 0) return '';
	$ini += strlen($start);
	$len = strpos($string, $end, $ini) - $ini;
	return substr($string, $ini, $len);
}
