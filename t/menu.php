<?

if (empty($_GLOBALS['menu'])) $_GLOBALS['menu'] = 'bouquet';

$mmenus = query_new('SELECT `title`, `url`, `name`, `bg_style` FROM `sw_menu` WHERE `sid` = 0 AND `active` = 1 ORDER BY `pos` ASC');
foreach ($mmenus as $m)
{
    $bg_style = false;
    if (isset($bg_colors[$m['bg_style']]))
    {
        $bg_style = $bg_colors[$m['bg_style']]['class'];
    }

    if ($m['url']=='/catalog/bouquet/' && empty($_GLOBALS['cf_list']))
    {
        cf_get_list();
    }

    $gm = ($m['url']=='/catalog/bouquet/')?'<i class="fontico-menu-2"></i>':'';

    $mmcf = ($m['url']=='/catalog/bouquet/') ? block_nofollow(cf_html($_GLOBALS['cf_list'],'','mm_cf')) : '';

    $mmcf_class = ($m['url']=='/catalog/bouquet/') ? ' class="bc_cont" ':'';

    $link_class = $m['name'] == $_GLOBALS['menu'] ? 'active ' : '';

    #TODO: �������� CSS-����� ��� ��������� ������
    if ($bg_style == 'orange')
    {
        $link_class .= 'menu_orange';
    }

    $link_class = strlen($link_class) > 0 ? ' class = "'.$link_class.'"' : '';

    $menu[] = '<div '.$mmcf_class.'><a href="'.$m['url'].'"'.$link_class.'>'
            . $m['title'].($m['name'] == $_GLOBALS['menu'] ? '<span></span>' : '').$gm.'</a>'.($m['url']=='/catalog/bouquet/'?'':'')
            . $mmcf.'</div>';
}
$frst = array_shift($menu);
//var_dump($frst );
//die;
$mobile_start = "<div class='menu-mobile-link'><a href='javascript:void(0);'>����</a></div><div class='menu-mobile'>";
$dopMenu = array();
$dopMenu[] = '<div class="for-mobile"><a href="/individual-order.html">���. �����</a></div><span class="razd for-mobile">&nbsp;</span>';
$dopMenu[] = '<div class="for-mobile"><a href="/catalog/create.html">���� �����</a></div><span class="razd for-mobile">&nbsp;</span>';
$dopMenu[] = '<div class="for-mobile"><a href="/catalog/op=season">����� ������</a></div><span class="razd for-mobile">&nbsp;</span>';

$menu = '<div class="topMenuIn">'. $frst . '<span class="razd">&nbsp;</span>' . $mobile_start .     implode('<span class="razd">&nbsp;</span>', $menu). implode("\n", $dopMenu) .'</div></div>';

$top_menu = query_new('SELECT m.`id`, m.`title`, m.`url`, m.`name`, m.`bg_style` FROM `sw_menu` as m LEFT JOIN `sw_menu` as r ON m.`sid` = r.`id` WHERE r.`name` = '.quote($_GLOBALS['menu']).' AND m.`active` = 1 ORDER BY m.`pos` ASC',0,'id');
if ((is_array($top_menu)) ) {
    if (count($top_menu) > 0){
        $menu.= '<div class="topMenu2"><ul class="sub_level_0">';
        foreach ($top_menu as $row){ $menu .= sub_menu($row);}
        $menu.= '</ul></div>';
    }else{
        $top_menu = query_new('SELECT m.`id`, m.`title`, m.`url`, m.`name`, m.`bg_style` FROM `sw_menu` as m LEFT JOIN `sw_menu` as r ON m.`sid` = r.`id` WHERE r.`name` = "bouquet" AND m.`active` = 1 ORDER BY m.`pos` ASC',0,'id');
        $menu.= '<div class="topMenu2"><ul class="sub_level_0">';
        foreach ($top_menu as $row){ $menu .= sub_menu($row);}
        $menu.= '</ul></div>';
    }

}
// � ����� ����� ������� ������ ������ ��� ��������� ������
$menu .= '<div class="top-search for-mobile"><div class="actions-mobile"><a class="butn_orange" href="/actions.html">�����</a></div><div class="find find-mobile"><form action="/?m=search" method="GET"><input type="hidden" name="m" value="search"><input name="query"  onkeyup="fastsearch.goSearch(this);" type="text" value="'.(!empty($_GET['query']) ? strtolower(filter(trim($_GET['query']), 'nohtml')) : null).'" placeholder="����� �� �������� ��� ��������" /><input type="button" onclick="this.form.submit()" /></form></div></div>';

function sub_menu($row=array(),$level=0){
    global $bg_colors;

    $level++;
    $html = '';
    if (isset($row['id'])){
        $html .= '<li class="sub_level_'.$level.'" onmouseover="top_Menu2.showSub(this);" zlevel="'.$level.'">'
        . '<a class="main '.(isset($bg_colors[$row['bg_style']])?' bg_color_style bg_'.$bg_colors[$row['bg_style']]['class'].' ':'').'" href="'.$row['url'].'" >'.$row['title'].'</a>';
        $sub_menu = query_new('SELECT `id`, `title`, `url`, `name`, `bg_style` FROM `sw_menu` WHERE `sid` = '.$row['id'].' AND `active` = 1 ORDER BY `pos` ASC');
        if (is_array($sub_menu) && count($sub_menu)>0){
            $html .= '<a class="hovered" href="'.$row['url'].'">'.$row['title'].'</a><div class="sub_cont">';
            $del = (count($sub_menu)>4)?2:1;
            $arr = array_chunk($sub_menu, ceil(count($sub_menu)/$del));
            $border_class = array(0 => '');
            if (count($arr)>1){
                $border_class = array(
                    0 => 'border-right',
                    1 => 'border-left'
                );
            }
            foreach ($arr as $key=>$cell){
                $html .= '<ul class="sub_level_'.$level.' '.$border_class[$key].'">';
                foreach ($cell as $sub_row){
                    $html .= sub_menu($sub_row, $level);
                }
                $html .= '</ul>';
            }
            $html .= '</div><div class="box_shadow"></div>';
        }else{
            $html .='</li>';
        }
    }
    return $html;
}


$html = str_replace('[%MENU%]', isset($menu) ? $menu : null, $html);
?>